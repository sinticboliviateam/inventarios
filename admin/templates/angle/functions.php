<?php
use SinticBolivia\SBFramework\Classes\SB_BaseTheme;
use SinticBolivia\SBFramework\Classes\SB_Request;

class Beetle_Templates_Angle extends SB_BaseTheme
{
    protected function __construct()
    {
        $this->languageDomain   = 'angle';
        $this->languagePath     = dirname(__FILE__) . SB_DS . 'locale';
        
        parent::__construct();
    }
	public function AddActions()
	{
		$task = SB_Request::getTask();
		parent::AddActions();
		if( lt_is_admin() )
		{
			if( $task == 'angle_set_colors' )
			{
				$theme = basename(SB_Request::getString('theme'));
				sb_update_user_meta(sb_get_current_user()->user_id, '_angle_colors', $theme);
				die();
			}
		}
	}
}
Beetle_Templates_Angle::GetInstance();
