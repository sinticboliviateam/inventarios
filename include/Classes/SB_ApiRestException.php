<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Classes;
use Exception;
use SinticBolivia\SBFramework\Classes\SB_ApiRest;

class SB_ApiRestException extends Exception
{
    /**
     *
     * @var SB_ApiRest 
     */
    public $controller;
    
    public function __construct($error, $ctrl, $code = null)
    {
        $this->controller = $ctrl;
        parent::__construct($error, $code);
    }
    public function ResponseError()
    {
        if( !$this->controller )
            throw new Exception(__('API ERROR: Invalid api rest controller'));
        $this->controller->ResponseError($this->getMessage());
    }
}

