<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Classes;
use SinticBolivia\SBFramework\Classes\SB_Attachment;
class SB_AttachmentImage extends SB_Attachment
{
	protected 	$thumbnails 	= null;
	protected	$size			= null;
	protected	$cropFit		= false;
	protected	$forceResize	= false;
	
	public function GetDbThumbnails()
	{
		$this->thumbnails = array();
		if( !(int)$this->attachment_id )
			return false;
			
		$query = "SELECT * FROM attachments WHERE parent = {$this->attachment_id} AND type = '{$this->type}'";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$thumb = new SB_AttachmentImage();
			$thumb->SetDbData($row);
			$this->thumbnails[] = $thumb;
		}
	}
	/**
	 * Get image thumbnail
	 * 
	 * Size example: full|55x55|150x150|330x330|500x500
	 * @param string $size 
	 * @return  SB_AttachmentImage
	 */
	public function GetThumbnail($size, $forceResize = false, $cropFit = false)
	{
		$thumb = null;
		if( !$this->thumbnails || !count($this->thumbnails) )
			$this->GetDbThumbnails();
		if( !count($this->thumbnails) )
			return $this;
		//##check for forceResize
		if( $size != 'full' && $forceResize && is_file(UPLOADS_DIR . SB_DS . $this->file) )
		{
			$thumb 				= $this;
			$thumb->forceResize = $forceResize;
			$thumb->size 		= $size;
			$thumb->cropFit 	= $cropFit;
			return $thumb;
		}
		foreach($this->thumbnails as $_thumb)
		{
			
			if( stristr($_thumb->file, $size) )
			{
				$thumb = $_thumb;
				break;
			}
		}
		
		//##if thumbnail not found full size exists, return full size
		if( !$thumb && is_file(UPLOADS_DIR . SB_DS . $this->file) )
		{
			$thumb = $this;
		}
		return $thumb != null ? $thumb : new SB_AttachmentImage();
	}
	public function GetThumbnails()
	{
		if( $this->thumbnails == null )
			$this->GetDbThumbnails();
		return $this->thumbnails;
	}
	/**
	 * Get image url
	 * 
	 * @return string 
	 */
	public function GetUrl()
	{
		if( !$this->forceResize )
			return UPLOADS_URL . '/' . $this->file;
		
		$img = new SB_Image(UPLOADS_DIR . SB_DS . $this->file);
		list($w, $h) = explode('x', $this->size);
		if( !$this->cropFit )
			$img->resize($w, $h);
		else
			$img->crop_to_fit($w, $h);
		$buffer = $img->GetBase64();
		return "data:{$this->mime};base64,$buffer";
	}
	public function jsonSerialize()
	{
		return [
				'attachent_id'	=> $this->attachment_id,
				'object_type'	=> $this->object_type,
				'object_id'		=> $this->object_id,
				'type'			=> $this->type,
				'mime'			=> $this->mime,
				'file'			=> $this->file,
				'url'			=> $this->GetUrl(),
				'thumbnailUrl'	=> $this->GetThumbnail('150x150')->GetUrl()
		];
	}
}