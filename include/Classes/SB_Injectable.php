<?php
namespace SinticBolivia\SBFramework\Classes;

class SB_Injectable extends SB_Object
{
	protected $models = [];
	
	/**
	 * Load controller registered models
	 *
	 */
	protected function LoadModels()
	{
		$self = new \ReflectionObject($this);
		foreach($self->getProperties(\ReflectionProperty::IS_PROTECTED) as $prop)
		{
			if( !strstr($prop->getName(), 'Model') ) continue;
			$ns 	= '';
			$class 	= '';
			if( $res = preg_match('/@namespace\s+(.*)\s+?/', $prop->getDocComment(), $matches) )
			{
				$class 	= ucfirst(trim($prop->getName()));
				$ns 	= trim($matches[1]);
			}
			elseif( preg_match('/@var\s+(.*)\s+?/', $prop->getDocComment(), $matches) && $matches[1] )
			{
				$class	= trim($matches[1]);
				$ns		= '';
			}
			
			$this->models[] = array('class' => $class, 'ns' => $ns);
		}
	
		foreach($this->models as $model)
		{
			if( is_array($model) )
				$this->LoadModel($model['class'], $model['ns']);
			else
				$this->LoadModel($model);
		}
	}
	/**
	 * Load a model instance from class and namespace
	 *
	 * @param string $model
	 * @param string $ns
	 * @return SB_Model
	 */
	protected function LoadModel($model, $ns = null)
	{
		$varname 	= sb_normalize_type_name(basename(str_replace('\\', '/', $model)));
	
		//check if model is already instanced
		if( isset($this->$varname) && $this->$varname )
			return $this->$varname;
	
		$class_name = ucfirst($model);
		
		if( class_exists($class_name) )
		{
			
			$model          = new $class_name($this->dbh);
			$model->mod     = $this->mod;
			$this->$varname = $model;
		}
		elseif( $ns /*|| $this->app->moduleNamespace*/ )
		{
			$modelNamespace = $ns ? $ns : $this->app->moduleNamespace . '\\Models';
			$class_name = $modelNamespace . '\\' . $model;
			 
			if( class_exists($class_name) )
			{
				$model          = new $class_name($this->dbh);
				$model->mod     = $this->mod;
	
				$this->$varname = $model;
			}	
		}
		else
		{
			return null;
	
		}
		if( !$this->$varname )
			return null;
	
	
		return $this->$varname;
	}
}