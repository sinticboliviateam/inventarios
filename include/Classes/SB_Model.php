<?php
namespace SinticBolivia\SBFramework\Classes;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Database;

/**
 * The Model base class
 * 
 * @author J. Marcelo Aviles Paco
 * @copyright Sintic Bolivia
 * @namespace
 */
class SB_Model extends SB_Injectable
{
	/**
	 * 
	 * @var \SinticBolivia\SBFramework\Database\SB_Database
	 */
	protected	$dbh;
	public      $mod;
	/**
	 * The request instance
	 * @var SB_Request
	 */
    protected	$request;
    
	public function __construct($dbh = null)
	{
		$this->dbh 		= $dbh ? $dbh : SB_Factory::getDbh();
		$this->request  = SB_Request::GetInstance();
		$this->LoadModels();
	}
    /**
     * Get a translated text from po files
     * 
     * @param string $str
     * @param string $domain
     * @return string The translated text
     */
    public function __($str, $domain = null)
    {
        
        return SB_Text::_($str, $domain ? $domain : $this->mod);
    }
	/**
	 * Load registered models
	 * 
	 */
    /*
	protected function LoadModels()
	{
        $self = new \ReflectionObject($this);
		foreach($self->getProperties(\ReflectionProperty::IS_PROTECTED) as $prop)
		{
			if( !strstr($prop->getName(), 'Model') ) continue;
			$res = preg_match('/@namespace\s+(.*)\s+?/', $prop->getDocComment(), $matches);
			$class_name = ucfirst($prop->getName());
			$namespace	= $res ? $matches[1] : null;
			if( $namespace )
				$class_name = $namespace . '\\' . $class_name;
			if( class_exists($class_name) )
			{
				$varname        = $prop->getName();
				$model          = new $class_name($this->dbh);
                $model->mod     = $this->mod;
                $this->$varname = $model;
			}
			
		}
		
	}
	*/
}
