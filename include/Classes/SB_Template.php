<?php
namespace SinticBolivia\SBFramework\Classes;

class SB_Template
{
	protected	$basePath;
	protected	$baseUrl;
	protected	$request;
	protected	$vars			= [];
	protected	$templateFile 	= 'index.php';
	protected	$htmlDocument;
	protected	$app;
	
	public function __construct($path = null, $url = null)
	{
		$this->basePath 	= $path;
		$this->baseUrl		= $url;
		$this->request		= SB_Request::GetInstance();
		$this->htmlDocument = new SB_HtmlDoc();
		
	}
	public function SetApplication($app)
	{
		$this->app = $app;
	}
	public function SetPath($path)
	{
		$this->basePath = $path;
	}
	public function SetUrl($url)
	{
		$this->baseUrl = $url;
	}
	/**
	 * Set the template file
	 * 
	 * @param string $tplFile
	 */
	public function SetFile($tplFile)
	{
		$this->templateFile = $tplFile;
		
		if( !strstr($this->templateFile, '.php') )
			$this->templateFile .= '.php';

	}
	/**
	 * Get a template file
	 * 
	 * @param string $tpl_file
	 * @return NULL|string
	 */
	public function GetFile($tpl_file)
	{
		$file = strstr($tpl_file, '.php') ? $tpl_file : $tpl_file . '.php';
		
		if( !is_file($this->basePath . SB_DS . $file) )
			return null;
		return $this->basePath . SB_DS . $file;
	}
	/**
	 * 
	 * @return \SinticBolivia\SBFramework\Classes\SB_HtmlDoc
	 */
	public function GetHtmlDocument()
	{
		return $this->htmlDocument;
	}
	/**
	 * Include a partial template file
	 * All partials needs to be located into "partials" directory
	 * 
	 * @param string $partial The partial filename
	 * @return boolean
	 */
	public function IncludePartial($partial)
	{
		$partial		= strstr($partial, '.php') ? $partial : $partial . '.php';
		$partialFile	= $this->basePath . SB_DS . 'partials' . SB_DS . $partial;
		
		if( !is_file($partialFile) )
		{
			return false;
		}
		include $partialFile;
		return true;
	}
	/**
	 * Include a template file
	 * The search path is based on the current template directory 
	 * @param string $file
	 * @param array $args
	 * @return boolean
	 */
	public function IncludeFile($file, $args = [])
	{
		$templateFile = $this->GetFile($file);
		if( !$templateFile )
			return false;
		include $templateFile;
		return true;
	}
	/**
	 * Get the template header file
	 * 
	 * @param string $tpl The template suffix
	 * @param array $args
	 * @return boolean
	 */
	public function GetHeader($tpl = '', $args = [])
	{
		global $template_html, $view_vars, $app;
		
		$view	= $this->request->getString('view', 'default');
		isset($view_vars[$view]) && (is_array($view_vars[$view]) || is_object($view_vars[$view])) ? extract($view_vars[$view]) : '';
		
		$tpl_file = 'header' . ($tpl ? '-' . $tpl : '') . '.php';
		if( !is_file($this->basePath . SB_DS . $tpl_file) )
			return false;
		
		include $this->basePath . SB_DS . $tpl_file;
		
		return true;
	}
	public function GetFooter($tpl = '', $args = [])
	{
		global $template_html, $view_vars, $app;
		
		$view 			= $this->request->getString('view', 'default');
		isset($view_vars[$view]) && (is_array($view_vars[$view]) || is_object($view_vars[$view])) ? @extract($view_vars[$view]) : '';
		
		$tpl_file = 'footer' . ($tpl ? '-'.$tpl : '') . '.php';
		
		if( !is_file($this->basePath . SB_DS . $tpl_file)  )
			return false;
		
		include $this->basePath. SB_DS . $tpl_file;
		
		return true;
	}
	public function GetSidebar($tpl = '', $args = [])
	{
		global $template_html, $view_vars, $app;
		
		$view 			= $this->request->getString('view', 'default');
		isset($view_vars[$view]) && (is_array($view_vars[$view]) || is_object($view_vars[$view])) ? @extract($view_vars[$view]) : '';
		
		
		$tpl_file = 'sidebar' . ($tpl ? '-'.$tpl : '') . '.php';
		if( !is_file($this->basePath . SB_DS . $tpl_file) )
			return false;
		
		include $this->basePath . SB_DS . $tpl_file;
		
		return true;
	}
	public function SetVar($var, $value)
	{
		$this->vars[$var] = $value;
	}
	public function SetVars($array)
	{
		foreach($array as $var => $value)
		{
			$this->vars[$var] = $value;
		}
	}
	public function BuildHtml()
	{
		$mod = $this->request->getString('mod', null);
		//##check if template directory exists
		if( !$this->basePath || !is_dir($this->basePath) )
		{
			require_ONCE INCLUDE_DIR . SB_DS . 'template-functions.php';
			lt_template_fallback();
			return true;
		}
		if( defined('LT_ADMIN') )
		{
			if( function_exists('sb_build_admin_menu') )
				sb_build_admin_menu();
		}
		else
		{
		
		}
		if( !$mod )
		{
			$mod = defined('LT_ADMIN') ? 'dashboard' : 'content';
		}
		
		$tpl_file = $this->templateFile;
		if( lt_is_frontpage() && is_file($this->GetFile('frontpage.php')) )
		{
			$tpl_file = 'frontpage.php';
		}
		
		$tpl_file = SB_Module::do_action('template_file', $tpl_file);
		$this->htmlDocument->AddBodyClass('tpl-' . str_replace('.php', '', $tpl_file));
		$vars = $this->app->GetController() ? $this->app->GetController()->viewVars : array();
		count($vars) && extract($vars);
		ob_start();
		require_once $this->basePath . SB_DS . $tpl_file;
		return ob_get_clean();
		
	}
}