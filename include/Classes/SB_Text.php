<?php
namespace SinticBolivia\SBFramework\Classes;

class SB_Text
{
	public static function _($text, $domain = 'default')
	{
        if( defined('NO_TRANSLATION') )
            return $text;
        if( empty($text) )
			return '';
		$pre = defined('TRANSLATION_DEBUG') ? '<pre>'.$text.'</pre>' : '';
        if( defined($text) )
        {
            return constant($text);
        }
        if( defined('TRANSLATION_USE_POMO') )
        {
            $msgstr = self::_pomo($text, $domain);
            if( $text == $msgstr )
                $msgstr = self::_pomo($text, 'default');
            return $pre . $msgstr;
        }
		textdomain($domain);
		$ntext = dgettext($domain, $text);
        if( $text == $ntext )
            $ntext = dgettext('default', $text);
        
		return $pre . SB_Module::do_action('lang_text', $ntext);
	}
    public static function _pomo($text, $domain = 'default')
    {
        return isset(SB_Language::$pomoDomains[$domain]) ? 
						SB_Language::$pomoDomains[$domain]->translate($text) : $text;
    }
}
class SBText extends SB_Text{}
