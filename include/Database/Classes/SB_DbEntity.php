<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SinticBolivia\SBFramework\Database\Classes;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Database\Classes\SB_Database;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Database\SB_QueryBuilder;
use ReflectionClass;
use Exception;
use JsonSerializable;

class SB_DbEntity implements JsonSerializable
{
    private     $self;
    private     $vars           = array();
    protected   $class          = null;
    protected   $table          = null;
	/**
	 * @var SinticBolivia\SBFramework\Database\SB_Database
	 */
    protected   $dbh            = null;
    protected   $columns        = array();
    protected   $primaryKey     = null;
    protected   $queryBuilder   = null;
    protected	$relations		= array();
    
    protected   static  $entityPool = array();
    protected   static  $nativeTypes = array(
            'INT',
            'INTEGER',
            'BIGINTEGER',
            'BIGINT',
            'FLOAT',
            'DECIMAL',
            'DATE',
            'DATETIME',
            'TINYINT',
            'TEXT',
            'BLOB',
            'CHAR',
            'VARCHAR',
            'STRING'
    );
    protected   static  $currentEntity = null;
    
    /**
     * @var BOOL 
     */
    //public      $deleted;
    /**
     * @var DATETIME
     */
    //public      $deletion_date;
    /**
     * @var DATETIME
     */
    public      $last_modification_date;
    /**
     * @var DATETIME
     */
    public      $creation_date;
    
    protected   $metaTable  = null;
    protected   $cacheMeta  = array();
    
    public function __construct()
    {
        $this->class = get_called_class();
        $this->dbh = SB_Factory::getDbh();
        if( isset(self::$entityPool[$this->class]) )
        {
            $this->table        = self::$entityPool[$this->class]->table;
            $this->columns      = self::$entityPool[$this->class]->columns;
            $this->primaryKey   = self::$entityPool[$this->class]->primaryKey;
            $this->relations	= self::$entityPool[$this->class]->relations;
        }
        else
        {
            //error_log('BUILDING ENTITY ' . $this->class);
            //##build the entity
            $self           = new ReflectionClass($this);
            $this->class    = $self->getName();
            $classVars      = self::GetEntityAttributes($this, $self);
			
            if( !isset($classVars['table']) )
                throw new Exception(__('The entity class has no table name'));
            $this->table      	= $classVars['table'];
            $this->columns		= self::GetEntityColumns($this, $self);
            $this->relations	= self::GetRelations($this, $self);
            self::$entityPool[$this->class] = $this;
        }
        $this->queryBuilder = SB_Factory::getQueryBuilder();
    }
    /**
     * Bind data into entity and validate the data types
     * 
     * @param $data An object or array with entity data
     * @return bool
     */
    public function Bind($data, $exclude = [])
    {
        if( !$data )
            return false;
        $this->BeforeBind($data);
        
        foreach($data as $prop => $val)
        {
			if( in_array($prop, $exclude) ) continue;
            //##check for non entities columns
            if( (is_array($val) || is_object($val)) && property_exists($this, $prop) )
            {
                $this->$prop = $val;
                continue;
            }
            //##check if the column if defined into entity
            if( !isset($this->columns[$prop]) )
            {
                continue;
            }
            $dataType = SB_DbTable::GetType($this->columns[$prop]['type']);

            if( $dataType == SB_DbTable::TYPE_INTEGER || $dataType == SB_DbTable::TYPE_TINYINT 
                || $dataType == SB_DbTable::TYPE_BIGINT || $dataType == SB_DbTable::TYPE_BIGINTEGER )
                $this->$prop = (int)$val;
            elseif( $dataType == SB_DbTable::TYPE_DECIMAL || $dataType == SB_DbTable::TYPE_FLOAT )
            {
				//var_dump($prop);
			   $this->$prop = (float)$val;
			}
            elseif( $dataType == SB_DbTable::TYPE_CHAR || $dataType == SB_DbTable::TYPE_VARCHAR 
                    || $dataType == SB_DbTable::TYPE_TEXT || $dataType == SB_DbTable::TYPE_STRING )
            {
                $this->$prop = (string)"$val";
            }
            elseif( $dataType == SB_DbTable::TYPE_DATE || $dataType == SB_DbTable::TYPE_DATETIME )
                $this->$prop = $val;
            else
                $this->$prop = $val;
        }
        $this->AfterBind($data);
        return true;
    }
    /**
     * Get validated entity data into an standard object
     * 
     * @return object
     */
    public function GetEntityData()
    {
        $data = (object)array();
        foreach($this->columns as $column => $val)
        {
            $dataType = SB_DbTable::GetType($val['type']);
          
            if( $dataType == SB_DbTable::TYPE_INTEGER || $dataType == SB_DbTable::TYPE_TINYINT 
                || $dataType == SB_DbTable::TYPE_BIGINT || $dataType == SB_DbTable::TYPE_BIGINTEGER)
                $data->$column = (int)$this->$column;
            elseif( $dataType == SB_DbTable::TYPE_DECIMAL || $dataType == SB_DbTable::TYPE_FLOAT )
                $data->$column = (float)$this->$column;
            elseif( $dataType == SB_DbTable::TYPE_CHAR || $dataType == SB_DbTable::TYPE_VARCHAR || $dataType == SB_DbTable::TYPE_TEXT || $dataType == SB_DbTable::TYPE_STRING )
            {
                $data->$column = trim($this->$column);
            }
            elseif( $dataType == SB_DbTable::TYPE_DATE || $dataType == SB_DbTable::TYPE_DATETIME )
                $data->$column = sb_format_datetime(strtotime(trim($this->$column)), 'Y-m-d H:i:s');
            else
            {
                $data->$column = trim($this->$column);
            }
        }
        if( !(int)$data->{$this->primaryKey} )
			$data->{$this->primaryKey} = null;
        return $data;
    }
    protected function BeforeBind($data){}
    protected function AfterBind($data){}
    protected static function CheckCurrentEntity()
    {
        if( !self::$currentEntity )
        {
            self::$currentEntity = self::GetEntity(get_called_class());
        }
        return self::$currentEntity;
    }
    protected static function FreeEntity()
    {
        self::$currentEntity = null;
    }
    protected static function GetEntityAttributes($entity, $self = null)
    {
        $self   = $self ? $self : new ReflectionClass($entity);
        
        $vars   = array();
        $lines  = explode("\n", $self->getDocComment());
        
        foreach($lines as $line)
        {
            if( preg_match("/(@(.*)\s+(.*))/", $line, $matches) )
            {
                $vars[trim($matches[2])] = trim($matches[3]);
            }
        }
        return $vars;
    }
    public static function GetEntityColumns($entity, $self = null)
    {
        $self       = $self ? $self : new ReflectionClass($entity);
        $properties = $self->getProperties(\ReflectionProperty::IS_PUBLIC);
        
        $columns = array();
        foreach($properties as $prop)
        {
            $columnName = $prop->getName();
            $propertyData = array();
            $isColumn = true;
            $lines = explode("\n", $prop->getDocComment());
            foreach($lines as $line)
            {
                $type       = null;
                $varname    = null;
                $dataType   = null;
                if( preg_match("/(@(.*)\s+(\w+))/", $line, $matches) )
                {
                    $type       = trim($matches[2]);
                    $varname    = $type == 'var' ? 'type' : $type;
                    $dataType   = strtoupper(trim($matches[3]));
                }
                if( $varname == 'primaryKey' )
                {
                    $entity->primaryKey = $columnName;
                    
                    //continue;
                }
                if( empty($varname) || !$varname )
                    continue;
                $propertyData[$varname] = $dataType;
            }
            //##check if column is a native type
            if( isset($propertyData['type']) && in_array($propertyData['type'], self::$nativeTypes) )
            {
               $columns[$columnName] = $propertyData;
            }
        }
        return $columns;
    }
    /**
     * Create a new instance of entity
     * @return SB_DbEntity
     */
    public static function GetEntity($className)
    {
        $entity             = new $className();
        
        return $entity;
    }
    /**
     * Gets a entity record from database
     * 
     * @param mixed $id The entity primary key identifier
     * @return NULL|\SinticBolivia\SBFramework\Database\Classes\SB_DbEntity
     */
    public static function Get($id)
    {
        $dbh                = SB_Factory::getDbh();
        $entity             = self::GetEntity(get_called_class());
        
        $query = $entity->queryBuilder->select($entity->table, array_keys($entity->columns));
        $query->where()
				->equals($entity->primaryKey, (int)$id);
        $raw_sql 	= $entity->queryBuilder->write($query);
        $values		= $entity->queryBuilder->getValues();
        $sql 		= str_replace(array_keys($values), $values, $raw_sql);
        
        $res 		= $dbh->Query($sql);
        $resultSet 	= $dbh->GetResultSet();
        $data 		= $resultSet->GetRow();
        
        if( !$data )
            return null;
       
        $entity->Bind($data);
        
        return $entity;
    }
    public static function GetIn($ids)
    {
		if( !is_array($ids) )
			throw new Exception(__('Invalid Query IN data'));
		$dbh	= SB_Factory::getDbh();
		$class	= get_called_class();
		$entity = self::GetEntity($class);
		$query	= $entity->queryBuilder->select($entity->table, array_keys($entity->columns));
		$query->where()
				->in($entity->primaryKey, $ids);
		
		$sql 		= $entity->queryBuilder->writeWithValues($query, 1);
		$res 		= $dbh->Query($sql);
		$resultSet	= $dbh->GetResultSet();
		return $resultSet->GetRows($class);
	}
	/**
	 * 
	 * @param string $column
	 * @param string $value
	 * @param string $single
	 * @return multitype:StdClass |NULL|\SinticBolivia\SBFramework\Database\Classes\SB_DbEntity
	 */
	public static function GetBy($column, $value, $single = true)
	{
		$dbh	= SB_Factory::getDbh();
		$class	= get_called_class();
		$entity = self::GetEntity($class);
		$query	= $entity->queryBuilder->select($entity->table, array_keys($entity->columns));
		$query->where()
				->equals($column, $value);
		
		$sql 		= $entity->queryBuilder->writeWithValues($query);
		$res 		= $dbh->Query($sql);
		$resultSet	= $dbh->GetResultSet();
		if( !$single )
			return $resultSet->GetRows($class);
		$row = $resultSet->GetRow();
		if( !$row )
			return null;
			
		$entity->Bind($row);
		
		return $entity;
	}
    /**
     * 
     * @param int $limit
     * @param int $offset
     * @param array $conds
     * @return array
     */
    public static function GetRows($limit = 100, $offset = 0, $conds = array())
    {
        $dbh    	= SB_Factory::getDbh();
        $builder	= $dbh->NewQuery();
        $class		= get_called_class();
        $entity 	= self::GetEntity($class);
        
        $query 		= $builder->select($entity->table);
        if( count($conds) )
        {
        	foreach($conds as $col => $val)
        	{
        		$query->where()->equals($col, $val);
        	}
        }
        if( $limit > 0 )
        {
        	$query->limit((int)$offset, $limit);
        }
        $sql = $builder->writeWithValues($query);
        $dbh->Query($sql);
        $result = $dbh->GetResultSet($class);
        return $result->GetRows($class); 
    }
    public function Select(array $columns)
    {
        $cols = $this->dbh->SanitizeColumns($columns);
        $query = "SELECT $cols FROM {$this->table}";
        
        return self::CheckCurrentEntity();
    }
    public function LeftJoin($table, $column, $comparator, $value)
    {
    }
    public static function GroupBy()
    {
    }
    public static function OrderBy($column, $order)
    {
    }
    public static function Count()
    {
		$dbh 	= SB_Factory::getDbh();
        $entity = self::GetEntity(get_called_class());
        $entity->queryBuilder->select()
			->setTable($entity->table)
			->count();
		
		return $dbh->GetVar($entity->queryBuilder->write());
    }
    /**
     * Find a record using many conditions
     * 
     * @param array $conds
     * @return NULL|\SinticBolivia\SBFramework\Database\Classes\SB_DbEntity|multitype:StdClass
     */
    public static function Where(array $conds)
    {
		$dbh 	= SB_Factory::getDbh();
        $entity = self::GetEntity(get_called_class());
        $query 	= $entity->queryBuilder->select($entity->table, array_keys($entity->columns));
		
		foreach($conds as $col => $val)
		{
			$query->where()->equals($col, $val);
		}
		//$query->limit(0, 1);
		
		$sql 	= $entity->queryBuilder->writeWithValues($query);
		//SB_Factory::getApplication()->Log($sql);
        $dbh->Query($sql);
        $resultSet 	= $dbh->GetResultSet();
        $records	= $resultSet->GetRows(get_called_class());
        
        if( !$resultSet->totalRows )
        	return null;
        if( $resultSet->totalRows == 1 ) 
        {
        	$entity->Bind($records[0]);
        	return $entity;
        }
        
        return $records;
    }
    /**
     * Save the current entity to database
     * 
     * @param string $getNewRecord
     * @return Ambigous <unknown, NULL, \SinticBolivia\SBFramework\Database\Classes\SB_DbEntity>|Ambigous <NULL, \SinticBolivia\SBFramework\Database\Classes\SB_DbEntity>
     */
    public function Save($getNewRecord = true)
    {
        if( !$this->{$this->primaryKey} )
        {
        	$this->{$this->primaryKey} = null;
			if( property_exists($this, 'last_modification_date') )
				$this->last_modification_date = date('Y-m-d H:i:s');
			if( property_exists($this, 'creation_date') )
				$this->creation_date = date('Y-m-d H:i:s');
				
			if( method_exists($this, 'BeforeInsert') )
				call_user_func([$this, 'BeforeInsert']);
				
            $id = $this->dbh->Insert($this->table, $this->GetEntityData());
            $obj = $getNewRecord ? self::Get($id) : $id;
            
            if( method_exists($this, 'AfterInsert') )
				call_user_func([$this, 'AfterInsert'], $obj);
			return $obj;
        }
        $obj = $this->Update($getNewRecord);
		
		return $obj;
    }
    public function Update($getUpdatedRecord = true)
    {
		if( property_exists($this, 'last_modification_date') )
			$this->last_modification_date = date('Y-m-d H:i:s');
		if( method_exists($this, 'BeforeUpdate') )
			call_user_func([$this, 'BeforeUpdate']);
			
        $this->dbh->Update($this->table, $this->GetEntityData(), array($this->primaryKey => $this->{$this->primaryKey}));   
        
        if( method_exists($this, 'AfterUpdate') )
			call_user_func([$this, 'AfterUpdate']);
        
        return $getUpdatedRecord ? self::Get($this->{$this->primaryKey}) : $this->{$this->primaryKey};
    }
    public function Delete($fisically = true)
    {
        if( !$fisically )
        {
            if( isset($this->{'deleted'}) )
                $this->dbh->Update($this->table, 
                    array('deleted' => true), 
                    array($this->primaryKey => $this->{$this->primaryKey})
                );
            elseif( isset($this->{'status'}) )
                $this->dbh->Update($this->table, 
                    array('status' => 'deleted'), 
                    array($this->primaryKey => $this->{$this->primaryKey})
                );
            return true;
        }
        $this->dbh->Delete($this->table, array($this->primaryKey => $this->{$this->primaryKey}));
    }
    public function GetTableName(){return $this->table;}
    /**
     * Get entity meta data
     * 
     * @param string $metaKey The meta data key
     * @param mixed $default The default value if entity does not has the meta
     * @return mixed
     */
    public function GetMeta($metaKey, $default = null)
    {
        //if( !isset($this->meta) || !is_array($this->meta) || !count($this->meta) )
        if( !property_exists($this, 'meta') )
            return $default;
        //##find into meta array
        $exists = false;
        if( is_array($this->meta) )
        {
        	foreach($this->meta as $_meta)
        	{
        		$meta = (object)$_meta;
        		if( $meta->meta_key == $metaKey )
        		{
        			$exists = true;
        			//##store into meta cache
        			$this->cacheMeta[$metaKey] = $meta->meta_value;
        			break;
        		}
        	}
        }
        if( $exists )
        {
            return $this->cacheMeta[$metaKey];
        }
        //##try to get from database
        if( $this->{$this->primaryKey} && $this->metaTable )
        {
            return SB_Meta::getMeta($this->metaTable, $metaKey, $this->primaryKey, $this->{$this->primaryKey}, $default);
        }
        
        return $default;
    }
    /*
    public function __toString()
    {
        return print_r($this->GetEntityData(), 1);
    }
    */
    protected static function GetRelations($obj, $self)
    {
		$rels = array();
		$props = $self->getProperties(/*\ReflectionProperty::IS_PRIVATE |*/ \ReflectionProperty::IS_PROTECTED);
		foreach($props as $prop)
		{
			$columnName 	= $prop->getName();
            $propertyData 	= array();
            $isRelation		= false;
            $lines = explode("\n", $prop->getDocComment());
			
            foreach($lines as $_line)
            {
				$line = trim($_line);
				if( empty($line) || !strstr($line, '@') ) continue;
                $type       = null;
                $varname    = null;
                $dataType   = null;
                $line		= preg_replace('/\s+/', ' ', $line);
                $line		= preg_replace('/\t+/', ' ', $line);
                
                if( preg_match("/@(.*)\s+(.*)/", $line, $matches) )
                {
					$var	= trim($matches[1]);
					$data 	= trim($matches[2]);
					$var	= $var == 'var' ? 'type' : $var;
					if( in_array($var, ['type', 'belongsTo', 'hasOne', 'hasMany', 'manyToMany', 'reference']) )
					{
						$propertyData[$var] = strstr($data, ']') ? eval('return ' . $data . ';') : $data;
						if( in_array($var, ['belongsTo', 'hasOne', 'hasMany', 'manyToMany']) )
							$propertyData['relation'] = $var; 
						$isRelation = true;
					}
                }
                
            }
            if( $isRelation )
				$rels[$columnName] = $propertyData;
		}
		//print_r($rels);
		return $rels;
	}
	public function __get($var)
	{
		//##check if required properte is a relation
		if( !property_exists($this, $var) )
		{
			return null;
		}
		if( $this->$var )
			return $this->$var;
		if( isset($this->relations[$var]) )
		{
			$this->$var = $this->BuildRelation($var);
		}
		return $this->$var;
	}
	public function __set($var, $value)
	{
		if( !property_exists($this, $var) )
			return false;
		$this->$var = $value;
		return true;
	}
	public function __call($method, $argument)
	{
		if( strstr($method, 'Get') )
		{
			$var = lcfirst(str_replace('Get', '', $method));
			return $this->__get($var);
		}
	}
	protected function BuildRelation($prop)
	{
		
		$relation 	= $this->relations[$prop];
		$type		= $relation['relation'];
		
		if( $type == 'belongsTo' )
		{
			//##check reference identifier
			if( !$this->{$relation['reference'][0]} )
				return null;
			$className 	= $relation[$type];
			$callback 	= $className . '::Get';
			$obj = call_user_func($callback, $this->{$relation['reference'][0]});
			return $obj;
		}
		elseif( $type == 'hasOne' )
		{
			//##check reference identifier
			if( !$this->{$relation['reference'][0]} )
				return null;
			$className 	= $relation[$type];
			$callback 	= $className . '::Get';
			
			$obj = call_user_func($callback, $this->{$relation['reference'][0]});
			return $obj;
		}
		elseif( $type == 'hasMany' )
		{
			
		}
		return null;
	}
	public function jsonSerialize()
	{
		return $this->GetEntityData();
	}
}
