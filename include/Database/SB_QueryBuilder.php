<?php
namespace SinticBolivia\SBFramework\Database;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Object;

class SB_QueryBuilder extends SB_Object
{
    protected   $sql        = '';
    protected   $tables     = array();
    protected   $conditions = array();
    protected   $joins      = array();
    protected   $dbh        = null;
    protected   $columns    = array();
    protected   $order      = "";
    protected   $limit      = "";
    
    public function __construct($dbh = null)
    {
        $this->dbh              = $dbh ? $dbh : SB_Factory::getDbh();
        $this->Flush();
    }
    public function Flush()
    {
        $this->joins['LEFT']    = array();
        $this->joins['RIGHT']   = array();
        $this->joins['INNER']   = array();
        $this->table            = array();
        $this->conditions       = array();
        $this->columns          = array();
        $this->order            = '';
        $this->limit            = '';
        $this->sql              = '';
    }
    public function Select(array $columns)
    {
        $this->columns = $columns;
        
        return $this;
    }
    protected function ParseSelect()
    {
        $sql = $this->dbh->SanitizeColumns($this->columns);
        
        return sprintf("SELECT %s", implode(', ', $sql));
        
    }
    public function From(array $tables)
    {
        $this->tables = $tables;
        
		return $this;
    }
    protected function ParseFrom()
    {
        $sql = 'FROM ' . implode(', ', $this->tables);
        
        return $sql;		
    }
    /**
     * Add a new join
     * @var string $type The join type [LEFT|RIGHT|INNER]
     * @var string $table The main table
     * @var string $joinTable The table to join
     */
    public function AddJoin($type, $table, $joinTable, $tableColumn, $joinTableColumn)
    {
        if( !isset($this->joins[$type]) )
            return false;
            
        $this->joins[$type][] = array('table' => $table, 
                                    'joinTable' => $joinTable, 
                                    'tableColumn' => $tableColumn, 
                                    'joinTableColumn' => $joinTableColumn);
        return $this;
    }
    /**
     * Build a SQL from an array of conditions
     * Condition format
     * array(
     *      'column'    => 'column_name',
     *      'operator'  => 'AND|OR|LIKE',
     *      'value'     => 'the_value to compare',
     *      'comparator'    => '=|<>|!='
     * )
     * @return string A SQL condition
     **/
    public function ParseConditions()
    {
        $sqlCondition = '';
        if( !is_array($this->conditions) )
            return $sqlCondition;
        
        foreach($this->conditions as $cond)
        {
            $column         = trim($cond['column']);
            $val            = $this->dbh->EscapeString($cond['value']);
            $op             = trim($cond['operator']);
            $comparator     = $cond['comparator'];
            $sqlCondition   .= "$op $column $comparator $val";
        }
        
        return $sqlCondition;
    }
    public function AddCondition($leftValue, $rightValue, $operator, $comparator = '=')
    {
        $column = $leftValue;
        $value = $rightValue;
        if( strstr($leftValue, '.') )
        {
            list($table, $col) = explode('.', $leftValue);
            $column = sprintf("%s.%s", $this->dbh->WrapField($table), $this->dbh->WrapField($col));
        }
        if( strstr($value, '.') )
        {
            list($table, $col) = explode('.', $rightValue);
            $value = sprintf("%s.%s", $this->dbh->WrapField($table), $this->dbh->WrapField($col));
        }

        $this->conditions[] = array('column' => $column, 'operator' => $operator, 'value' => $value, 'comparator' => $comparator);
        
        return $this;
    }
    public function Where($column, $comparator, $value)
    {
        
        //$this->conditions[] = compact($column, $operator, $value, $comparator);
        $this->AddCondition($column, $value, 'AND', $comparator);
        return $this;
    }
    public function LeftJoin($joinTable, $tableColumn, $joinTableColumn)
    {
        $this->AddJoin('LEFT', '', $joinTable, $tableColumn, $joinTableColumn);
        return $this;
    }
    public function RightJoin($joinTable, $tableColumn, $joinTableColumn)
    {
        $this->AddJoin('RIGHT', '', $joinTable, $tableColumn, $joinTableColumn);
        return $this;
    }
    public function InnerJoin($joinTable, $tableColumn, $joinTableColumn)
    {
        $this->AddJoin('INNER', '', $joinTable, $tableColumn, $joinTableColumn);
        return $this;
    }
    public function SqlAND($leftValue, $comparator = '=', $rightValue)
    {
        $this->AddCondition($leftValue, $rightValue, 'AND', $comparator);
        return $this;
    }
    
    public function ParseJoins($joinType)
    {
        $sqlJoin = '';
        foreach($this->joins[$joinType] as $join)
        {
           
            $tableColumn    = $this->dbh->SanitizeColumns($join['tableColumn']);//sprintf("%s%s%s", $this->dbh->lcw, $join['tableColumn'], $this->dbh->rcw);
            $joinTable      = sprintf("%s%s%s", $this->dbh->lcw, $join['joinTable'], $this->dbh->rcw);
            $joinColumn     = $this->dbh->SanitizeColumns($join['joinTableColumn']);//sprintf("%s%s%s", $this->dbh->lcw, $join['joinTableColumn'], $this->dbh->rcw);
            
            $sqlJoin        .= "$joinType JOIN {$join['joinTable']} ON {$joinColumn[0]} = {$tableColumn[0]} ";
        }
        
        return $sqlJoin;
    }
    public function Order($column, $order = 'ASC')
    {
        $column = $this->dbh->SanitizeColumns($column);
        $this->order = sprintf("ORDER BY %s %s", $column[0], $order);
        return $this;
    }
    public function Limit($limit, $offset = 0)
    {
        $this->limit = sprintf("LIMIT %d", $limit);
        if( $offset > 0 )
            $this->limit .= sprintf(", %d", $offset);
    }
    public function Build()
    {
        $select         = $this->ParseSelect();
        $from           = $this->ParseFrom();
        $leftJoins      = $this->ParseJoins('LEFT');
        $rightJoins     = $this->ParseJoins('RIGHT');
        $innerJoins     = $this->ParseJoins('INNER');
        $where          = $this->ParseConditions();
        
        return "$select $from $leftJoins $rightJoins $innerJoins WHERE 1 = 1 $where {$this->order} {$this->limit}";
    }
    public function Query($flush = true)
    {
        $sql = $this->Build();
        if( $flush )
            $this->Flush();
        $this->dbh->Query($sql);
        
        return $this->dbh->GetResultSet();
    }
}
