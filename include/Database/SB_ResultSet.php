<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SinticBolivia\SBFramework\Database;

abstract class SB_ResultSet
{
    public $result;
    public $totalRows = 0;
    
    public function __construct($res)
    {
        $this->result = $res;
    }
    abstract public function GetRow();
    public function GetRows($class = null, $vars = null)
	{
		$results = array();
		$class = ( $class && class_exists($class) ) ? $class : null;
		while( $row = $this->GetRow() )
		{
			if( $class )
			{
				$obj = new $class();
				//##assign object vars for initialization
				if( is_array($vars) )
				{
					foreach($vars as $var => $value)
					{
						$obj->$var = $value;
					}
				}
				if( method_exists($obj, 'SetDbData') )
				{
					$obj->SetDbData($row);
				}
				elseif( method_exists($obj, 'Bind') )
				{
					$obj->Bind($row);
				}
				else
				{
					$obj->db_data = (object)$row;
				}
				$results[] = (object)$obj;
			}
			else
			{
				$results[] = (object)$row;
			}
		}
       
		return $results;
	}
}
