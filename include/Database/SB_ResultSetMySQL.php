<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Database;
use SinticBolivia\SBFramework\Database\SB_ResultSet;

class SB_ResultSetMySQL extends SB_ResultSet
{
    public $isMySqlI = true;
    
    public function __construct($res, $isMySqlI) 
    {
        parent::__construct($res);
        $this->isMySqlI = $isMySqlI;
        $this->totalRows = !$this->isMySqlI ? 
                                mysql_num_rows($this->result) :
                                $this->result->num_rows;
        
    }
    public function GetRow() 
    {
        $row = $this->isMySqlI ? $this->result->fetch_object() : mysql_fetch_object($this->result);
        return $row;
    }
    public function GetRows($class = null, $vars = null)
    {
        $results = parent::GetRows($class, $vars);
        //$this->isMySqlI ? $this->result->free() : mysql_free_result($this->result);
        return $results;
    }
}

