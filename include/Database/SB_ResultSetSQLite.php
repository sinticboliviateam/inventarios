<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Database;
use SinticBolivia\SBFramework\Database\SB_ResultSet;

class SB_ResultSetSQLite extends SB_ResultSet
{
    public function __construct($res) 
    {
        parent::__construct($res);
    }
    public function GetRow($class = null) 
    {
        $res = $this->result->fetchArray(SQLITE3_ASSOC);
        $this->result->finalize();
        $this->result = null;
        return (object)$res;
    }
    public function GetRows($class = null, $vars = null) 
    {
        $results = array();
		$class = ( $class && class_exists($class) ) ? $class : null;
		while( $row = $this->result->fetchArray(SQLITE3_ASSOC) )
		{
			if( $class )
			{
				$obj = new $class();
				//##assign object vars for initialization
				if( is_array($vars) )
				{
					foreach($vars as $var => $value)
					{
						$obj->$var = $value;
					}
				}
				if( method_exists($obj, 'SetDbData') )
				{
					$obj->SetDbData($row);
				}
				elseif( method_exists($obj, 'Bind') )
				{
					$obj->Bind($row);
				}
				else
				{
					$obj->db_data = (object)$row;
				}
				$results[] = (object)$obj;
			}
			else
			{
				$results[] = (object)$row;
			}
		}
       
		
        $this->result->finalize();
        $this->result = null;
        return $results;
    }
}
