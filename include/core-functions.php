<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;

function __sb_namespace2path($namespace)
{
	/*
	$namespace = str_replace(array('SinticBolivia\\SBFramework\\', 'SinticBolivia\\BeetleCMS\\'), 
						'', 
						$namespace);
	*/
	$allow = array('SinticBolivia\\SBFramework\\', 'SinticBolivia\\BeetleCMS\\');
	$path = '';
	if( !stristr($namespace, 'SinticBolivia\\SBFramework\\') && !stristr($namespace, 'SinticBolivia\\BeetleCMS\\') )
	{
		return INCLUDE_DIR . SB_DS . 'libs' . SB_DS . str_replace('\\', SB_DS, $namespace) . '.php';
	}
	$parts = explode('\\', $namespace);
	
	if( $parts[2] == 'Modules' )
	{
		$path = MODULES_DIR . SB_DS;
		unset($parts[0], $parts[1], $parts[2]);
		$parts[3] = 'mod_' . strtolower($parts[3]);
	}
	elseif( $parts[2] == 'Admin' && $parts[3] == 'Templates' )
	{
		$path = ADM_TEMPLATES_DIR . SB_DS;
		unset($parts[0], $parts[1], $parts[2], $parts[3]);
	}
	elseif( $parts[2] == 'Templates' )
	{
		$path = TEMPLATES_DIR . SB_DS;
		unset($parts[0], $parts[1], $parts[2]);
	}
	elseif( $parts[2] == 'Libs' )
	{
		$path = INCLUDE_DIR . SB_DS . 'libs' . SB_DS;
		unset($parts[0], $parts[1], $parts[2]);
	}
	elseif( $parts[2] == 'Classes' )
	{
		$path = INCLUDE_DIR . SB_DS . 'Classes' . SB_DS;
		unset($parts[0], $parts[1], $parts[2]);
	}
	else
	{
		$path = INCLUDE_DIR . SB_DS;
		unset($parts[0], $parts[1]);
	}
	/*
	$sufix = str_replace($allow, '', $namespace);
	$sufix = str_replace(array('\\', 'mod_'), array(SB_DS, ''), $sufix);
	*/
	$path	= $path . implode(SB_DS, $parts) . '.php';
	return $path;
}
function sb_namespace2path($namespace)
{
	$folder = str_replace(array('SinticBolivia\\SBFramework\\Modules\\', 
								'SinticBolivia\\BeetleCMS\\Modules\\', 
								'\\'
						),
						array(MODULES_DIR . SB_DS . 'mod_:', 
								MODULES_DIR . SB_DS . 'mod_:', 
								SB_DS
						), $namespace);
	
	list($prefix, $sufix) = explode('_:', $folder);
	$file = $prefix . '_' . lcfirst($sufix) . '.php';

	return $file;
}
function sb_autoload_class($class_name)
{
	$file = '';
	/*
	//##check for sbframework classes
	$default_class_path = BASEPATH . SB_DS . 'include' . SB_DS . 'Classes';// . SB_DS . $class_name . '.php';
	//##check if class filename exists into default class path
	if( is_file($default_class_path . SB_DS . $class_name . '.php') )
	{
		require_once $default_class_path . SB_DS . $class_name . '.php';
		return true;
	}
	*/
	/*
	 if( is_file($default_class) )
	 {
		 require_once $default_class;
		 var_dump('DEFAULT CLASS: ', $default_class);
		 return true;
	 }
	 */
	//var_dump($class_name);
	/*
	if( stristr($class_name, 'SBFramework\\Modules') )
	{
		$file = sb_namespace2path($class_name);
		//var_dump('FILE:', $file);
	}
	elseif( stristr($class_name, 'Libs') || !stristr($class_name, 'SinticBolivia\\') )
	{
		$file .= BASEPATH . SB_DS . 'include' . SB_DS . 'libs' . SB_DS .
					str_replace(['\\', 'Libs'], [SB_DS, ''], $class_name) . '.php';
	}
	else
	{
		$file .= BASEPATH . SB_DS . 'include' . str_replace(array('SinticBolivia\\SBFramework', 'SinticBolivia\\BeetleCMS', '\\'),
				array('', '', SB_DS), $class_name) . '.php';
	}
	*/
	$file = __sb_namespace2path($class_name);
	//var_dump($file);
	if( !is_file($file) )
	{
		//var_dump($file);
		return false;
	}
	
	//var_dump($file);
	require_once $file;
	return true;
}
//##define autoload function
spl_autoload_register('sb_autoload_class');
function sb_error_handler($errno, $error, $error_file, $error_line, $context)
{
	$app = SB_Factory::getApplication();
    $msg = "ERROR: $error<br/>ERROR CODE: $errno<br/>FILE: $error_file<br/>LINE: $error_line";
	//$app->Log(array('code' => $errno, 'error' => $error, 'file' => $error_file, 'line' => $error_line, 'context' => $context));
    $app->Log($msg);
	if( $errno != E_NOTICE && $errno != E_USER_WARNING && $errno != E_USER_NOTICE )
	{
		//lt_die($error . '<br/><div style="height:200px;overflow:auto;"><code><pre>' . print_r(debug_backtrace(), 1) . '</pre></code></div>');
        throw new Exception($msg);
	}
}
