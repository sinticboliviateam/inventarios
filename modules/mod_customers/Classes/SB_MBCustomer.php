<?php
namespace SinticBolivia\SBFramework\Modules\Customers\Classes;

use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;

/**
 * 
 * @author marcelo
 * @property int customer_id
 * @property int extern_id
 * @property string code
 * @property int group_id
 * @property int store_id
 * @property string first_name
 * @property string last_name
 * @property string company
 * @property string email
 * @property string address_1
 * @property string address_2
 * @property string city
 * @property string country
 * @property string country_code
 * @property string status
 * @property string last_modification_date
 * @property string creation_date
 */
class SB_MBCustomer extends SB_ORMObject
{
	public	$group;
	public	$blocked = false;
	
	public function __construct($customer_id = null)
	{
		parent::__construct();
		if($customer_id)
			$this->GetDbData($customer_id);
	}
	public function GetDbData($id)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_customers WHERE customer_id = $id LIMIT 1";
		if( !$dbh->Query($query) )
		{
			return false;
		}
		$this->_dbData = $dbh->FetchRow();
		$this->GetDbMeta();
		
		if( (int)$this->group_id > 0 )
		{
			$this->group = $this->dbh->FetchRow("SELECT * FROM mb_customer_groups WHERE id = $this->group_id LIMIT 1");
			if( $this->group )
				$this->blocked = $this->group->blocked;
		}
	}
	public function SetDbData($data)
	{
		$dbh = SB_Factory::getDbh();
		$this->_dbData = (object)$data;
		
		if( empty($data->meta) )
		{
			$this->GetDbMeta();
		}
		if( (int)$this->group_id > 0 )
		{
			$this->group = $this->dbh->FetchRow("SELECT * FROM mb_customer_groups WHERE id = $this->group_id LIMIT 1");
			$this->blocked = isset($this->group) ? (int)$this->group->blocked : 0;
		}
	}
	public function SetDbMeta($meta)
	{
		$this->meta = $meta;
	}
	public function GetDbMeta()
	{
		$dbh = SB_Factory::getDbh();
		
		$query = "SELECT * FROM mb_customer_meta WHERE customer_id = $this->customer_id";
		if( $dbh->Query($query) )
		{
			foreach($dbh->FetchResults() as $row)
			{
				$this->meta[$row->meta_key] = $row->meta_value;
			}
		}
	}
	/**
	 * Get orders history
	 * @param string $status
	 * @param string $type
	 */
	public function GetOrderHistory($status = null, $type = null)
	{
		$where = '';
		if( $status )
		{
			$where = "AND status = '$status' ";
		}
		if( $type )
		{
			$where .= "AND type = '$type' ";
		}
		$query = "SELECT * FROM mb_orders 
					WHERE 1 = 1
					AND customer_id = $this->customer_id 
					$where 
					ORDER BY order_date DESC";
		
		return $this->dbh->FetchResults($query);
	}
	public function GetPayments($status = 'complete')
	{
		$query = "SELECT * 
					FROM mb_orders 
					WHERE 1 = 1
					AND customer_id = $this->customer_id 
					AND payment_status = '$status'
					ORDER BY order_date DESC";
		return $this->dbh->FetchResults($query);
	}
	public function __get($var)
	{
		return parent::__get($var);
	}
	public function GetBillingName()
	{
		$billing_name = sprintf("%s %s", $this->first_name, $this->last_name);
		
		if( $this->_billing_name )
			$billing_name = $this->_billing_name;
		elseif( $this->company )
			$billing_name = $this->company;
			
		return $billing_name;
	}
}