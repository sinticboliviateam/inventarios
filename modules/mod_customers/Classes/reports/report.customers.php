<?php
class SB_MBReportCustomers extends SB_ORMObject
{
	public		$key = 'customers';
	public		$tabLabel;
	public 		$tabLink;

	public function __construct()
	{
		parent::__construct();
		$this->tabLabel	= __('Customers', 'customers');
		$this->tabLink	= SB_Route::_('index.php?mod=mb&view=reports.default&report=customers');
	}
	public function GetDbData($id){}
	public function SetDbData($data){}
	public function GetTabs()
	{
		$tab = SB_Request::getString('tab', 'purchases');
		?>
		<div class="navbar navbar-default hidden-prinar">
			<ul class="nav navbar-nav">
				<li class="<?php print !$tab || $tab == 'purchases' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=customers&tab=purchases'); ?>">
						<?php _e('Purchases', 'customers'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'pending_payments' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=customers&tab=pending_payments')?>">
						<?php _e('Pending Payments', 'customers'); ?>
					</a>
				</li>
				<?php SB_Module::do_action('mb_report_customers_tabs'); ?>
			</ul>
		</div>
		<?php
	}
	public function Form()
	{
		$tab = SB_Request::getString('tab', 'purchases');
		$this->GetTabs();
		$method = 'Form' . ucfirst($tab);
		if( method_exists($this, $method) )
			call_user_func(array($this, $method));
	}
	public function FormPurchases()
	{
		$user = sb_get_current_user();
		$stores = SB_Warehouse::GetUserStores($user);
		$store_id = SB_Request::getInt('store_id');
		?>
		<h3><?php _e('Report by volume of purchases', 'customers'); ?></h3>
		<form action="" method="get">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="purchases" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == $store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function FormPending_payments()
	{
		$user = sb_get_current_user();
		$stores = SB_Warehouse::GetUserStores($user);
		$store_id = SB_Request::getInt('store_id');
		?>
		<div class="hidden-print">
		<h3><?php _e('Customers Pending Payments Report', 'customers'); ?></h3>
		<form action="" method="get">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="customers" />
			<input type="hidden" name="tab" value="pending_payments" />
			<input type="hidden" name="build" value="pending_payments" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == $store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
		</form>
		</div>
		<?php 
	}
	public function Build()
	{
		$report = SB_Request::getString('build');
		if( $report == 'pending_payments' )
			$this->BuildPendingPayments();
	}
	protected function BuildPendingPayments()
	{
		$user = sb_get_current_user();
		$title = '';
		$store_id = SB_Request::getInt('store_id');
		$this->dbh->Select('o.*, c.first_name, c.last_name, SUM(o.total) pending_total, s.store_name')
					->From(array('mb_orders o', 'mb_customers c', 'mb_stores s'))
					->Where(null)
					->Join('o.customer_id = c.customer_id')
					->Join('o.store_id = s.store_id')
					->SqlAND(array('o.payment_status' => 'pending'));
		if( $store_id <= 0 && $user->can('mb_see_all_stores') )
		{
			$title = __('Customers Pending Payments Report', 'customers');
			$this->dbh->Join("o.store_id = s.store_id");
		}
		else
		{
			if( !$user->can('mb_see_all_stores') )
			{
				$store_id = (int)$user->_store_id;
			}
			$store = new SB_MBStore($store_id);
			$this->dbh->Join("o.store_id = $store_id");
			$title = sprintf(__('Customers Pending Payments Report - %s', 'customers'), $store->store_name);
		}
		
		$this->dbh->GroupBy('o.customer_id')
					->OrderBy('o.order_date', 'desc');
		//var_dump($this->dbh->builtQuery);
		$this->dbh->Query(null);
		$items = $this->dbh->FetchResults();
		$total = 0;
		$show_store = $store_id <= 0 && $user->can('mb_see_all_stores');
		?>
		<p class="hidden-print">
			<a href="javascript:window.print();" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'customers'); ?></a>
		</p>
		<h2 class="text-center"><?php print $title; ?><br/><?php print sb_format_date(time()); ?></h2>
		<table class="table table-condensed table-bordered table-hover">
		<thead>
		<tr>
			<th><?php _e('Num.', 'customers'); ?></th>
			<?php if( $show_store ): ?>
			<th><?php _e('Store', 'customers'); ?></th>
			<?php endif; ?>
			<th><?php _e('Customer', 'customers'); ?></th>
			<th><?php _e('Total Amount', 'customers'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($items as $item): $total += $item->pending_total; ?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<?php if( $show_store ): ?>
			<td><?php print $item->store_name; ?></td>
			<?php endif; ?>
			<td><?php printf("%s %s", $item->first_name, $item->last_name); ?></td>
			<td class="text-right"><?php print number_format($item->pending_total, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="<?php print $show_store ? 3 : 2; ?>"><div class="text-right"><b><?php _e('Total:'); ?></b></div></td>
			<td class="text-right"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
		</tr>
		</tfoot>
		</table>
		<?php 
	}
}