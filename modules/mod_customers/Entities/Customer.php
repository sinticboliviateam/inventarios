<?php
namespace SinticBolivia\SBFramework\Modules\Customers\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;

/**
 * @table mb_customers
 */
class Customer extends SB_DbEntity
{
    /**
     * @primaryKey true
     * @var BIGINTEGER
     */
    public  $customer_id;
    /**
     * @var BIGINTEGER
     */
    /**
     * @var BIGINTEGER
     */
    public  $extern_id;
    /**
     * @var VARCHAR
     */
    public  $code;
    /**
     * @var BIGINTEGER
     */
    public  $group_id;
    /**
     * @var BIGINTEGER
     */
    public  $store_id;
    /**
     * @var VARCHAR
     */
    public  $first_name;
    /**
     * @var VARCHAR
     */
    public  $last_name;
    /**
     * @var VARCHAR
     */
    public  $company;
    /**
     * @var DATE
     */
    public  $date_of_birth;
    /**
     * @var VARCHAR
     */
    public  $gender;
    /**
     * @var VARCHAR
     */
    public  $phone;
    /**
     * @var VARCHAR
     */
    public  $mobile;
    /**
     * @var VARCHAR
     */
    public  $fax;
    /**
     * @var VARCHAR
     */
    public  $email;
    /**
     * @var VARCHAR
     */
    public  $website;
    /**
     * @var VARCHAR
     */
    public  $address_1;
    /**
     * @var VARCHAR
     */
    public  $address_2;
    /**
     * @var VARCHAR
     */
    public  $zip_code;
    /**
     * @var VARCHAR
     */
    public  $city;
    /**
     * @var VARCHAR
     */
    public  $country;
    /**
     * @var VARCHAR
     */
    public  $country_code;
    
    public  $meta = array();
    
}
