<?php
namespace SinticBolivia\SBFramework\Modules\Customers\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Customers\Entities\Customer;
use Exception;

class CustomersModel extends SB_Model
{
	/**
	 * Get customer
	 * 
	 * @param Object $customer_id
	 */
	public function get($customer_id)
	{
		$customer_id = (int)$customer_id;
		$person_type = SB_Person::getPersonTypeBy('type', 'Customer');
		$query = "SELECT * FROM person WHERE person_type_id = $person_type->person_type_id AND person_id = $customer_id";
		$res = $this->_dbh->Query($query);
		
		return $this->_dbh->FetchRow();
	}
    public function Create(Customer $customer)
    {
		//print_r($customer);
		$customer->customer_id				= null;
        $customer->deleted                  = false;
        $customer->last_modification_date   = date('Y-m-d H:i:s');
        $customer->creation_date            = date('Y-m-d H:i:s');
        SB_Module::do_action_ref('customers_before_insert', $customer);
        
        $newCustomer = $customer->Save(1);
       
        if( isset($customer->meta) && is_array($customer->meta) )
        {
            foreach($customer->meta as $_meta)
            {
                $meta = (object)$_meta;
                mb_add_customer_meta($newCustomer->customer_id, $meta->meta_key, $meta->meta_value);
            }
        }
        return $newCustomer;
    }
    public function Update(Customer $customer)
    {
        if( !$customer->customer_id )
            throw new Exception($this->__('The customer identifier is invalid'));
            
		$customer->last_modification_date	= date('Y-m-d H:i:s');
		SB_Module::do_action_ref('customers_before_update', $customer);
        
		$updatedCustomer = $customer->Save();
        if( isset($customer->meta) && is_array($customer->meta) )
        {
            foreach($customer->meta as $_meta)
            {
                $meta = (object)$_meta;
                mb_update_customer_meta($customer->customer_id, $meta->meta_key, $meta->meta_value);
            }
        }
        
        return $updatedCustomer;
    }
	/**
	 * Get all available customers
	 */
	public function getCustomers()
	{
		$person_type = SB_Person::getPersonTypeBy('type', 'Customer');
		$query = "SELECT * FROM person WHERE person_type_id = $person_type->person_type_id";
		$res = $this->_dbh->Query($query);
		
		return $this->_dbh->FetchResults();
	}
}
