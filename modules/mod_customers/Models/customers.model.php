<?php
namespace SinticBolivia\SBFramework\Modules\Customers\Entities;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Modules\Customers\Entities\Customer;

class SB_CustomersModel extends SB_Model
{
	/**
	 * Get customer
	 * 
	 * @param Object $customer_id
	 */
	public function get($customer_id)
	{
		$customer_id = (int)$customer_id;
		$person_type = SB_Person::getPersonTypeBy('type', 'Customer');
		$query = "SELECT * FROM person WHERE person_type_id = $person_type->person_type_id AND person_id = $customer_id";
		$res = $this->_dbh->Query($query);
		
		return $this->_dbh->FetchRow();
	}
    public function Create(Customer $customer)
    {
        
    }
	/**
	 * Get all available customers
	 */
	public function getCustomers()
	{
		$person_type = SB_Person::getPersonTypeBy('type', 'Customer');
		$query = "SELECT * FROM person WHERE person_type_id = $person_type->person_type_id";
		$res = $this->_dbh->Query($query);
		
		return $this->_dbh->FetchResults();
	}
}
