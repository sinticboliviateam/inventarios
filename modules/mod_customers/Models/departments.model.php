<?php
class SB_DepartmentsModel extends SB_Model
{
	public function get($department_id)
	{
		$query = "SELECT * FROM departments WHERE department_id = $department_id";
		$res = $this->_dbh->Query($query);
		if( !$res )
			return null;
		
		return $this->_dbh->FetchRow();
	}
}