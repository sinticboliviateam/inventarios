<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Modules\Customers\Classes\SB_MBCustomer;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;

class LT_AdminControllerCustomers extends SB_Controller
{
	public function task_default()
	{
		if( !sb_is_user_logged_in() )
		{
			die(SB_Text::_('You are not logged in, please start session', 'customers'));
		}
		$user = sb_get_current_user();
		if( !$user->can('manage_customers') )
		{
			sb_redirect(SB_Route::_('index.php?mod=customers'),
					SBText::_('You dont have enough permissions view customers', 'customers'), 'error');
		}
		$keyword		= SB_Request::getString('keyword');
		$page 			= SB_Request::getInt('page', 1);
		$limit 			= SB_Request::getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
		
		$query = "SELECT COUNT(*) FROM mb_customers";
		$where = "WHERE 1 = 1 ";
		if( !$user->IsRoot() )
		{
			$where .= "AND store_id = $user->_store_id ";
		}
		if( $keyword )
		{
			$where .= "AND (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR company LIKE '%$keyword%') ";
		}
		$query = "$query $where";
		$total_rows = $this->dbh->GetVar($query);
		
		$total_pages 	= ceil($total_rows / $limit);
		$offset 		= ($page <= 1) ? 0 : ($page - 1) * $limit;
		
		$this->dbh->Select("c.*, (select cm.meta_value FROM mb_customer_meta cm WHERE cm.customer_id = c.customer_id AND cm.meta_key = '_nit_ruc_nif' LIMIT 1) AS nit_ruc_nif")
					->From(array('mb_customers c'))
					//->LeftJoin('mb_customer_meta cm', 'cm.customer_id = c.customer_id AND cm.meta_key = "_nit_ruc_nif"')
					->Where(null);
		if( !$user->IsRoot() )
		{
			$this->dbh->SqlAND("c.store_id = $user->_store_id");
		}
		if( $keyword )
		{
			$this->dbh->SqlAND(array('c.first_name' => $keyword), 'LIKE', '%', '%')
						->SqlOR(array('c.last_name' => $keyword), 'LIKE', '%', '%')
						->SqlOR(array('c.company' => $keyword), 'LIKE', '%', '%');
		}
		$this->dbh->OrderBy('creation_date')
					->Limit($limit, $offset);
		//var_dump($this->dbh->builtQuery);
		$this->dbh->Query(null);
		$customers = $this->dbh->FetchResults();
		
		$this->document->SetTitle(SB_Text::_('Customers Management', 'customers'));
		
		sb_set_view_var('customers', $customers);
		sb_set_view_var('current_page', $page);
		sb_set_view_var('total_pages', $total_pages);
	}
	public function task_new()
	{
		$user = sb_get_current_user();
		$stores = SB_Warehouse::GetUserStores($user);
		sb_set_view_var('stores', $stores);
		sb_set_view_var('page_title', __('Create new customer', 'customers'));
		$this->_groups = $this->dbh->FetchResults("SELECT * FROM mb_customer_groups ORDER BY name ASC");
	}
	public function task_delete()
	{
		if( !sb_is_user_logged_in() )
		{
			die(SB_Text::_('You are not logged in, please start session', 'mb_c'));
		}
		$user = sb_get_current_user();
		if( !$user->can('delete_customer') )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('You dont have enough permissions to delete a customer', 'mb_c'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=customers'));
		}
		$customer_id = SB_Request::getInt('id');
		if( !$customer_id )
		{
			sb_redirect(SB_Route::_('index.php?mod=customers'), SB_Text::_('The customer identifier is invalid', 'mb_c'), 'error');
		}
		$dbh = SB_Factory::getDbh();
		$dbh->Delete('mb_customers', array('customer_id' => $customer_id));
		$dbh->Delete('mb_customer_meta', array('customer_id' => $customer_id));
		SB_MessagesStack::AddMessage(SB_Text::_('The customer has been deleted', 'mb_c'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=customers'));
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		$customer_id = SB_Request::getInt('id');
		if( !$customer_id )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Identificador de cliente invalido'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=customers'));
		}
		sb_include_module_helper('customers');
		$customers = new SB_MBCustomer($customer_id);
		if( !$customers->customer_id )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('El cliente no existe'), 'customers');
			sb_redirect(SB_Route::_('index.php?mod=customers'));
		}
		sb_set_view('new');
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fineuploader');
	
		$stores = SB_Warehouse::GetUserStores($user);
		sb_set_view_var('stores', $stores);
		sb_set_view_var('customer_id', $customer_id);
		sb_set_view_var('customer', $customers);
		sb_set_view_var('page_title', SB_Text::_('Editar Cliente'));
		$this->_orders_history = $customers->GetOrderHistory();
		$this->_groups = $this->dbh->FetchResults("SELECT * FROM mb_customer_groups ORDER BY name ASC");
		$this->document->SetTitle(__('Edit Customer', 'customers'));
	}
	public function task_save_customer()
	{
		sb_include_module_helper('customers');
		$customer_id 	= SB_Request::getInt('customer_id');
		$first_name 	= SB_Request::getString('first_name');
		$last_name 		= SB_Request::getString('last_name');
		$company 		= SB_Request::getArrayVar('meta', 'company');
		$email 			= SB_Request::getString('email');
		$website 		= SB_Request::getArrayVar('meta', 'website');
		$telephone 		= SB_Request::getString('telephone');
		$mobil_telephone = SB_Request::getString('mobil_telephone');
		$city 			= SB_Request::getString('city');
		$country 		= SB_Request::getString('country_code');
		$address 		= SB_Request::getString('address');
		$group_id 		= SB_Request::getInt('group_id');
		$store_id		= SB_Request::getInt('store_id');
		$pass = true;
		$meta = SB_Request::getVar('meta');
		if( empty($first_name) )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Ingrese nombres del cliente'), 'error');
			LT_HelperCustomers::ViewSecuruty($customer_id, $this);
			return false;
		}
		if( empty($last_name) )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Ingrese los apellidos del cliente'), 'error');
			LT_HelperCustomers::ViewSecuruty($customer_id, $this);
			return false;
		}
		/*
		if( empty($telephone) )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Ingrese un telefono para el cliente'), 'error');
			LT_HelperCustomers::ViewSecuruty($customer_id, $this);
			return false;
		}
		if( empty($city) )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Ingrese la ciudad del cliente'), 'error');
			LT_HelperCustomers::ViewSecuruty($customer_id, $this);
			return false;
		}
		if( empty($country) || $country == -1 )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Ingrese el pa&iacute;s del cliente'), 'error');
			LT_HelperCustomers::ViewSecuruty($customer_id, $this);
			return false;
		}
		*/
		$countries = include INCLUDE_DIR . SB_DS . 'countries.php';
		$dbh = SB_Factory::getDbh();
		$date = date('Y-m-d H:i:s');
		$data = array(
				'group_id'		=> $group_id,
				'store_id'		=> $store_id,
				'first_name' 	=> $first_name,
				'last_name' 	=> $last_name,
				'company' 		=> $company,
				'address_1' 	=> $address,
				'city' 			=> $city,
				'country'		=> !empty($country) && isset($countries[$country]) ? $countries[$country] : '',
				'country_code' 	=> !empty($country) ? $country : '',
				'phone' 		=> $telephone,
				'mobile' 		=> $mobil_telephone,
				'email' 		=> $email,
				'website' 		=> $website,
				'zip_code'		=> SB_Request::getString('zip')
		);
		if( !$customer_id )
		{
			$data['creation_date'] = SB_Request::getString('creation_date')?
										sb_format_datetime(SB_Request::getString('creation_date'),'Y-m-d H:i:s'):$date;
			$customer_id = $dbh->Insert('mb_customers', $data);
			foreach($meta as $meta_key => $meta_value)
			{
				if( is_array($meta_value) || is_object($meta_value) )
					SB_Meta::addMeta('mb_customer_meta', $meta_key, $meta_value, 'customer_id', $customer_id);
				else
					SB_Meta::addMeta('mb_customer_meta', $meta_key, trim($meta_value), 'customer_id', $customer_id);
				
			}
			SB_Module::do_action('save_customer', $customer_id);
			SB_MessagesStack::AddMessage('El cliente ha sido creado correctamente', 'success');
			sb_redirect(SB_Route::_('index.php?mod=customers'));
		}
		else 
		{
			$data['last_modification_date']=$date;
			$where = array('customer_id' => $customer_id);
			$dbh->Update('mb_customers', $data, $where);
			foreach($meta as $meta_key => $meta_value)
			{
				if( is_array($meta_value) || is_object($meta_value) )
					SB_Meta::updateMeta('mb_customer_meta', $meta_key, $meta_value, 'customer_id', $customer_id);
				else
					SB_Meta::updateMeta('mb_customer_meta', $meta_key, trim($meta_value), 'customer_id', $customer_id);
			}
			SB_Module::do_action('save_customer', $customer_id);
			SB_MessagesStack::AddMessage('El cliente ha sido actualizado correctamente', 'success');
			sb_redirect(SB_Route::_('index.php?mod=customers&view=edit&id='.$customer_id));
		}
	}
	
	public function task_get_customer()
	{
		$by 			= SB_Request::getString('by');
		$customer_id 	= SB_Request::getInt('id');
		sb_include_module_helper('customers');
		$customerView	= null;
		if( $by )
		{
			$customerView = LT_HelperCustomers::FindCustomerBy($customer_id, $by);
		}
		else
		{
			$customerView = LT_HelperCustomers::GetCustomer($customer_id);
		}
		
		if( SB_Request::getInt('ajax') )
			sb_response_json(array('status' => 'ok', 'customer' => $customerView));
		sb_set_view_var('customerView', $customerView);
		$this->task_default();
	}
	public function task_customers_list()
	{
		$keyword = SB_Request::getString('keyword');
		$this->document->templateFile = 'module.php';
		$query = "SELECT c.*, 
						(select meta_value 
                            from mb_customer_meta cm 
                            WHERE cm.customer_id = c.customer_id and meta_key = '_nit_ruc_nif'
                            LIMIT 1
                        ) _nit_ruc_nif 
						FROM mb_customers c WHERE 1 = 1 ";
		if( $keyword )
		{
			$query .= "AND (c.first_name LIKE '%$keyword%'
							OR c.last_name LIKE '%$keyword%'
							OR c.company LIKE '%$keyword%') ";
		}
		$query .= "GROUP BY c.customer_id " .
					"ORDER BY c.last_name ASC ";
		$customers = $this->dbh->FetchResults($query);
		if( SB_Request::getInt('completion') )
		{
			for($i = 0; $i < count($customers);$i++)
			{
				$customers[$i]->id		= $customers[$i]->customer_id;
				$customers[$i]->label = "{$customers[$i]->first_name} {$customers[$i]->last_name} " . 
											(!empty($customers[$i]->company) ? "{$customers[$i]->company}" : '');
				$customers[$i]->name = "{$customers[$i]->first_name} {$customers[$i]->last_name}"; 
			}
			sb_response_json(array('status' => 'ok', 'results' => $customers));
		}
		sb_set_view_var('customers', $customers);
		sb_set_view_var('keyword', $keyword);
	}
	public function task_print_pp()
	{
		$id = SB_Request::getInt('id');
		/*
		sb_set_view_var('print_content', $print_content);
		//$this->SetView('print.preview');
		$customer = new SB_MBCustomer($id);
		ob_start();
		include MOD_CUSTOMERS . SB_DS . 'views' . SB_DS . 'print_pp.php';
		$print_content = ob_get_clean();
		*/
	}
	public function task_export()
	{
		$type 		= SB_Request::getString('type', 'csv');
		$query 		= "SELECT * FROM mb_customers ORDER BY last_name ASC";
		$customers 	= $this->dbh->FetchResults($query);
		if( $type == 'csv' )
		{
			$csv = '';
			foreach($customers as $c)
			{
				$first_name = str_replace('"', "'", $c->first_name);
				$last_name = str_replace('"', "'", $c->last_name);
				$company = str_replace('"', "'", $c->company);
				$csv .= "{$c->customer_id},{$c->store_id},\"{$first_name}\",\"{$last_name}\",\"{$company}\"".
						",{$c->phone},{$c->email}\n";
			}
			$filename = sprintf(__('customers-%s.txt', 'customers'), sb_format_date(time()));
			header('Content-Type: text/csv');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"" . $filename . "\"");
			die($csv);
		}
	}
}
