<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Module;

class LT_AdminControllerCustomersGroups extends SB_Controller
{
	public function task_default()
	{
		$sub_query = "SELECT COUNT(c.customer_id) FROM mb_customers c WHERE c.group_id = g.id";
		$query = "SELECT g.*, ($sub_query) AS total_customers FROM mb_customer_groups g ORDER BY g.creation_date DESC";
		$this->_groups = $this->dbh->FetchResults($query);
		$this->_title = __('Customer Groups', 'customer');
		$this->document->SetTitle($this->title);
	}
	public function task_new()
	{
        if( !sb_get_current_user()->can('customers_create_group') )
		{
			lt_die(__('You cant edit customer groups', 'customers'));
		}
		$title = __('Create New Customer Group', 'customers');
		$this->SetVar('title', $title);		
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		if( !sb_get_current_user()->can('customers_edit_group') )
		{
			lt_die(__('You cant edit customer groups', 'customers'));
		}
		$id = SB_Request::getInt('id');
		$query = "SELECT * FROM mb_customer_groups WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(__('The customer group does not exists', 'customers'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=customers&view=groups.default'));
		}
		$group = $this->dbh->FetchRow();
		
		$this->SetView('groups.new');
		$title = __('Create New Customer Group', 'customers');
		$this->_title = $title;
		$this->_group = $group;
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		$id 			= SB_Request::getInt('group_id');
		$name			= SB_Request::getString('gname');
		$description	= SB_Request::getString('description');
		$blocked		= SB_Request::getInt('blocked');
		$status			= 'active';
		$data = compact('name', 'description', 'blocked', 'status');
		$msg = $link = '';
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_customer_groups', $data);
			$msg = __('The group has been created', 'customers');
			$link = SB_Route::_('index.php?mod=customers&view=groups.default');
		}
		else
		{
			$this->dbh->Update('mb_customer_groups', $data, array('id' => $id));
			$msg = __('The group has been updated', 'customers');
			$link = SB_Route::_('index.php?mod=customers&view=groups.edit&id='.$id);
		}
		SB_Module::do_action('save_customer_group', $id, $data);
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_delete()
	{
		if( !sb_get_current_user()->can('customers_delete_group') )
		{
			lt_die(__('You cant edit customer groups', 'customers'));
		}
        $id = $this->request->getId('id');
        $this->dbh->Delete('mb_customer_groups', array('id' => $id));
        SB_MessagesStack::AddSuccess(__('Customer group deleted', 'customers'));
        sb_redirect($this->Route('index.php?mod=customers&view=groups.default'));
	}
}
