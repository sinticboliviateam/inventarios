<?php
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Factory;

function mb_get_customer_meta($id, $meta_key, $multiple = true)
{
	return SB_Meta::getMeta('mb_customer_meta', $meta_key, 'customer_id', $id, null, $multiple);
}
function mb_add_customer_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::addMeta('mb_customer_meta', $meta_key, $meta_value, 'customer_id', $id);
}
function mb_update_customer_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::updateMeta('mb_customer_meta', $meta_key, $meta_value, 'customer_id', $id);
}
function mb_get_customer_by($value, $by = 'email')
{
	$dbh = SB_Factory::GetDbh();
	$customer = null;
	if( $by == 'email' )
	{
		$query = "SELECT * FROM mb_customers WHERE email = '$value' LIMIT 1";
		if( $row = $dbh->FetchRow($query) )
		{
			$customer = new SB_MBCustomer();
			$customer->SetDbData($row);
		}
		
	}
	elseif( $by == 'id' )
	{
		$customer = new SB_MBCustomer((int)$value);
		if( !$customer->customer_id )
		{
			return null;
		}
	}
	elseif( $by == 'code' )
	{
		$query = "SELECT * FROM mb_customers WHERE code = '$value' LIMIT 1";
		if( !($row = $dbh->FetchRow($query)) )
		{
			return $customer;
		}
		$customer = new SB_MBCustomer();
		$customer->SetDbData($row);
	}
	
	return $customer;
}
/**
 * Insert or update a customer
 * 
 * @param <array> $data 
 * @param <array> $meta 
 * @return int customer_id
 */
function mb_customers_insert_new($data, $meta = null)
{
	$dbh = SB_Factory::GetDbh();
	$id = null;
	if( !isset($data['customer_id']) || !(int)$data['customer_id'] )
	{
		$data['last_modification_date']	= date('Y-m-d H:i:s');
		$data['creation_date']	= date('Y-m-d H:i:s');
		SB_Module::do_action_ref('customers_before_insert', $data, $meta);
		$id = $dbh->Insert('mb_customers', $data);
	}
	else
	{
	
	}
	if( $meta && is_array($meta) )
	{
		foreach($meta as $meta_key => $meta_value)
		{
			mb_update_customer_meta($id, $meta_key, $meta_value);
		}
		
	}
	SB_Module::do_action('customers_insert_new', $id, $data, $meta);
	return $id;
}
