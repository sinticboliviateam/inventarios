<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Modules\Customers\Classes\SB_MBCustomer;

class LT_HelperCustomers
{
	public  static function GetCustomer($customer_id)
	{
		$customer = new SB_MBCustomer($customer_id);
		//$dbh = SB_Factory::getDbh();
		//$query = "SELECT concat(c.first_name,' ',c.last_name) as 'name',c.* FROM mb_customers as c where c.customer_id=$customer_id";
		//$dbh->Query($query);
		return $customer;
	}
	
	public static function GetCustomerMeta($customer_id){
		$dbh=SB_Factory::getDbh();
		$query = "SELECT meta_key,meta_value FROM mb_customer_meta WHERE customer_id = $customer_id";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
	
	public static function ViewSecuruty($customer_id,$class){
		if(!$customer_id)
			$class->task_new();
		else
			$class->task_edit();
	}
	public static function FindCustomerBy($keyword, $by)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT c.* FROM mb_customers c, mb_customer_meta cm ".
					"WHERE 1 = 1 ".
					"AND c.customer_id = cm.customer_id ";
					
		if( $by == 'nit_ruc_nif' || $by == 'nit' )
		{
			$query .= 
						"AND cm.meta_key = '_nit_ruc_nif' ".
						"AND cm.meta_value = '$keyword' ";
		}
		$query .= "LIMIT 1";
		$row = $dbh->FetchRow($query);
		if( !$row )
			return null;
		$c = new SB_MBCustomer();
		$c->SetDbData($row);
		return $c;
	}
}