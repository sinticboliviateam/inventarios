<?php
namespace SinticBolivia\MonoBusiness\Modules\Customers;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Route;

define('MOD_CUSTOMERS_DIR', dirname(__FILE__));
define('MOD_CUSTOMERS_URL', MODULES_URL . '/' . basename(MOD_CUSTOMERS_DIR));
require_once MOD_CUSTOMERS_DIR . SB_DS . 'functions.php';
//require_once MOD_CUSTOMERS_DIR . SB_DS . 'classes' . SB_DS . 'class.customer.php';

class LT_ModuleCustomers
{
	protected $dbh;
	public function __construct()
	{
		$this->dbh = SB_Factory::GetDbh();
		$this->AddActions();
	}
	public function AddActions()
	{
		$is_api = SB_Request::getString('api');
		if( $is_api )
		{
			$method = SB_Request::getString('method');
			//require_once MOD_USERS_DIR . SB_DS . 'classes' . SB_DS . 'class.api.php';
			//$api = new SB_ModUsersAPI();
			
			if( $method == 'get_clientes' )
			{
				$query = "SELECT * FROM mb_customers";
				$rows = $this->dbh->FetchResults($query);
				//$res = call_user_func(array($api, $method));
				header('Access-Control-Allow-Origin: *');
				header('Content-type: application/json');
				die(json_encode($rows));
			}
			die(__FILE__);
		}
		
		SB_Module::add_action('init', array($this, 'action_init'));
		if( defined('LT_ADMIN') )
		{
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
			//##add the customer report
			SB_Module::add_action('mb_reports', array($this, 'action_mb_reports'));
		}
	}
	public function action_init()
	{
		SB_Language::loadLanguage(LANGUAGE, 'customers', MOD_CUSTOMERS_DIR . SB_DS . 'locale');
	}
	public function action_admin_menu()
	{
		SB_Menu::addMenuChild('menu-management', 
								'<span class="glyphicon glyphicon-user"></span> ' . __('Customers', 'customers'), 
								SB_Route::_('index.php?mod=customers'), 
								'mb-customers', 
								'manage_customers');
		SB_Menu::addMenuChild('mb-customers', 
								__('Groups', 'customers'), 
								SB_Route::_('index.php?mod=customers&view=groups.default'), 
								'mb-customers-group');
	}
	public function action_mb_reports($reports)
	{
		$reports['customers'] = array('class' => 'SB_MBReportCustomers', 'path' => MOD_CUSTOMERS_DIR . SB_DS . 'classes' . SB_DS . 'reports');
		return $reports;
	}
}
new LT_ModuleCustomers();

