��    U      �  q   l      0     1     ?     K     X     d     l     p  *   x  $   �     �     �     �     �     �                     %     -     5     O     _     s     |     �  	   �     �  !   �     �     �     �     �     	     	     	     %	  
   3	     >	     F	     Z	     ]	     c	     j	  	   r	     |	     �	     �	     �	     �	  	   �	     �	     �	  	   �	     �	     �	     
     
  	   #
     -
     K
     P
     W
  	   g
     q
     x
     �
     �
     �
  	   �
  
   �
  "   �
     �
  "   �
          /  %   J     p     v     �  +   �     �  5   �  /   
     :  �  >  
              ,     9     F  
   O     Z  ,   l  "   �     �     �  &   �  	   �     �                '     .     >     J     h     v     �     �     �     �  !   �  '   �                    +     2     A     P     W     f     n     v     �     �     �     �  	   �     �     �     �     �     �     �               $     0     @     Q     Z     t     |     �     �     �  	   �     �     �     �     �     �     �  	   
          3  (   N     w     �  #   �     �     �     �     �  &     4   ,  /   a     �     1   D   N   )   @              K                    6   	   .   L       :       +       2      #      -       F          H                     M       >   B   O                 (   7   Q          4   J           ?       A   G   !   U   E                        C   
      0   %   S      3                /   I   5   $   "   *      8                T                  ;   '   9       &   =   P      R   ,   <              -- country -- -- group -- -- status -- -- store -- Actions Add Address Are you sure to delete the customer group? Are you sure to delete the customer? Attachments Back Block customers into this group Build Business Information Cancel City Code Company Country Create New Customer Group Create customer Create new customer Customer Customer Groups Customer Type Customers Customers Management Customers Pending Payments Report Delete Delete customer Description Edit Edit Customer Edit customer Email Export to CSV First Name General General Information Go Group Groups History Last Name Manage customers Mobile Telephone NIT/RUC/NIF Name New Customer New Group Num. Office Telephone Order No. Payment Status Pending Payments Print Print Pending Payments Purchases Report by volume of purchases Save Search Search customer Search... Select Select customer State Status Store Telephone Telephones The customer group does not exists The customer has been deleted The customer identifier is invalid The group has been created The group has been updated There are no customers registered yet Total Total Amount Total: You are not logged in, please start session You cant edit customer groups You dont have enough permissions to delete a customer You dont have enough permissions view customers ZIP Project-Id-Version: Mono Customers
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-11 16:45-0400
PO-Revision-Date: 2017-03-11 17:22-0400
Last-Translator: J. Marcelo Aviles Paco <maviles@sinticbolivia.net>
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop;SB_Text::_;_e;__
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ../../..
 -- pais -- -- grupo -- -- estado -- -- tienda -- Acciones Adicionar  Direcci&oacute;n: Estas seguro de borrar el grupo de clientes? Estas seguro de borrar el cliente? Adjuntos Volver Bloquear clientes dentro de este grupo Construir Informacion de la Empresa Cancelar Ciudad Codigo Compa&ntilde;ia Pa&iacute;s Crear Nuevo Grupo de Clientes Crear cliente Crear nuevo cliente Cliente Grupos de Clientes Tipo de cliente Clientes Administraci&oacute;n de Clientes Reporte de Pagos Pendientes por Cliente Borrar Borrar cliente? Descripcion Editar Editar Cliente Editar cliente Correo Exportar a CSV Nombres General Informacion General Ir Grupo Grupos Historial de Pedidos Apellidos Administrar clientes Celular NIT/RUC/NIF Nombre Nuevo Cliente Nuevo Grupo Nro. Tel&eacute;fono de oficina: Pedido Nro. Estado del Pago Pagos Pendientes Imprimir Imprimir Pagos Pendientes Compras Reporte por volumen de compras Guardar Buscar Buscar cliente Buscar... Seleccionar Seleccione un cliente Estado Estado Tienda Tel&eacute;fono Telefonos El grupo de clientes no existe El cliente ha sido borrado El identificar de la cliente es invalido El grupo ha sido creado El grupo ha sido actualizado Aun no existen clientes registrados Total Monto Total Total: Necesita iniciar una sesion No puede editar los grupos de clientes No tiene suficientes permisos para borrar un cliente No tiene suficientes permisos para ver clientes Codigo Postal 