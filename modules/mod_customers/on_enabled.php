<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;

SB_Language::loadLanguage(LANGUAGE, 'customers', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('customers');
$permissions = array(
		array('group' => 'customers', 'permission' => 'manage_customers', 	'label'	=> __('Manage customers', 'customers')),
		array('group' => 'customers', 'permission' => 'create_customer',	'label'	=> __('Create customer', 'customers')),
		array('group' => 'customers', 'permission' => 'edit_customer', 		'label'	=> __('Edit customer', 'customers')),
		array('group' => 'customers', 'permission' => 'delete_customer', 	'label'	=> __('Delete customer', 'customers')),
        array('group' => 'customers', 'permission' => 'customers_create_group',	'label'	=> __('Create customer group', 'customers')),
		array('group' => 'customers', 'permission' => 'customers_edit_group', 		'label'	=> __('Edit customer group', 'customers')),
		array('group' => 'customers', 'permission' => 'customers_delete_group', 	'label'	=> __('Delete customer group', 'customers')),
);
sb_add_permissions($permissions);
