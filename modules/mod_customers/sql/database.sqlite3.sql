CREATE TABLE IF NOT EXISTS mb_customers ( 
    customer_id            INTEGER         NOT NULL PRIMARY KEY AUTOINCREMENT,
    extern_id              INTEGER         DEFAULT 0,
    code                   VARCHAR( 512 ),
    group_id				integer,
    store_id               INTEGER,
    first_name             VARCHAR( 128 ),
    last_name              VARCHAR( 128 ),
    company                VARCHAR( 128 ),
    date_of_birth          DATE,
    gender                 VARCHAR( 64 ),
    phone                  VARCHAR( 64 ),
    mobile                 VARCHAR( 64 ),
    fax                    VARCHAR( 64 ),
    email                  VARCHAR( 128 ),
    website                VARCHAR( 128 ),
    address_1              VARCHAR( 256 ),
    address_2              VARCHAR( 256 ),
    zip_code               VARCHAR( 32 ),
    city                   VARCHAR( 128 ),
    country                VARCHAR( 128 ),
    country_code           VARCHAR( 10 ),
    status					VARCHAR(64),
    last_modification_date DATETIME,
    creation_date          DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_customer_meta ( 
    meta_id       INTEGER         NOT NULL PRIMARY KEY AUTOINCREMENT,
    customer_id   INTEGER         NOT NULL,
    meta_key      VARCHAR( 256 ),
    meta_value    TEXT,
    creation_date DATETIME 
);
create table if not exists mb_customer_groups(
	id				integer not null primary key autoincrement,
	name			varchar(256),
	description		text,
	blocked			tinyint(1) default 0,
	status			varchar(128),
	creation_date	datetime
);

