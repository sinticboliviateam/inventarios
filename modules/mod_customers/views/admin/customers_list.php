<?php

?>
<form action="" method="get">
	<input type="hidden" name="mod" value="customers" />
	<input type="hidden" name="view" value="customers_list" />
	<div class="form-group">
		<input type="text" name="keyword" value="<?php print $keyword; ?>" placeholder="<?php _e('Search...', 'customers'); ?>" class="form-control" />
	</div>
</form>
<table class="table">
<thead>
<tr>
	<th><?php _e('No.', 'customers'); ?></th>
	<th><?php _e('Customer', 'customers'); ?></th>
	<th><?php _e('Email', 'customers'); ?></th>
	<th><?php _e('Empresa', 'customers'); ?></th>
	<th><?php _e('Select', 'customers'); ?></th>
</tr>
</thead>
<tbody>
<?php $i = 1; foreach($customers as $c): ?>
<tr>
	<td class="text-center"><?php print $i;?></td>
	<td><?php printf("%s %s", $c->first_name, $c->last_name); ?></td>
	<td><?php print $c->email; ?></td>
	<td><?php print $c->company; ?></td>
	<td class="text-center">
		<a href="javascript:;" 
			data-id="<?php print $c->customer_id; ?>"
			data-name="<?php printf("%s %s", $c->first_name, $c->last_name); ?>"
			<?php foreach($c as $prop => $val): ?>
			data-<?php print $prop ?>="<?php print $val; ?>"
			<?php endforeach; ?> 
			class="btn btn-default select"
			title="<?php _e('Select customer', 'customers'); ?>">
			<span class="glyphicon glyphicon-check"></span>
		</a>
	</td>
</tr>
<?php $i++; endforeach; ?>
</tbody>
</table>
<script>
jQuery(function()
{
	jQuery('.select').click(function()
	{
		parent.jQuery(parent.document).trigger('on_customer_selected', [this.dataset]);
		if( parent.select_callback )
		{
			parent.select_callback(this.dataset);
		}
		
		return false;
	});
});
</script>