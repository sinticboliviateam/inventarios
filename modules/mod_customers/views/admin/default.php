<?php
?>
<div class="wrap">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h2 id="page-title">
				<?php _e('Customers Management', 'customers'); ?>
			</h2>
		</div>
		<div class="col-xs-12 col-md-6">
			<a class="btn btn-info" href="<?php print $this->Route('index.php?mod=customers&view=groups.default'); ?>">
				<span class="glyphicon glyphicon-user"></span> <?php print $this->__('Groups', 'customers'); ?>
			</a>
			<a class="btn btn-warning" href="<?php print $this->Route('index.php?mod=customers&task=export&type=csv'); ?>">
				<span class="glyphicon glyphicon-print"></span> <?php _e('Export to CSV', 'customers'); ?>
			</a>
			<a class="btn btn-success" href="<?php print $this->Route('index.php?mod=customers&view=new'); ?>">
				<span class="glyphicon glyphicon-plus"></span> <?php print $this->__('New Customer', 'customers'); ?>
			</a>
		</div>
	</div>
	
	<form action="" method="get" class="">
		<input type="hidden" name="mod" value="customers" />
		<div class="row">
			<div class="col-xs-6 col-md-6">
				<div class="form-group">
					<input type="text" name="keyword" value="<?php print $this->request->getString('keyword'); ?>" placeholder="<?php _e('Search customer', 'customers'); ?>" class="form-control" />
				</div>
			</div>
			<button class="btn btn-gray"><span class="glyphicon glyphicon-search"></span></button>
		</div>
	</form>
	<table id="mb-customers-table" class="table table-condensed">
	<thead>
	<tr>
		<th><?php print $this->__('Nro'); ?></th>
		<th><?php print $this->__('Code', 'customers'); ?></th>
		<th><?php print $this->__('Name', 'customers'); ?></th>
		<th><?php print $this->__('Telephones', 'customers'); ?></th>
		<th><?php print $this->__('Email', 'customers'); ?></th>
		<th><?php print $this->__('Company', 'customers'); ?></th>
		<th><?php print $this->__('NIT/RUC/NIF', 'customers'); ?></th>
		<th><?php print $this->__('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i=1;if(count($customers)): foreach($customers as $c): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $c->code; ?></td>
		<td><?php printf("%s %s", $c->first_name, $c->last_name); ?></td>
		<td><?php printf("%s, %s", $c->phone, $c->mobile);?></td>
		<td><?php print $c->email; ?></td>
		<td><?php print $c->company; ?></td>
		<td><?php print $c->nit_ruc_nif; ?></td>
		<td>
			<?php /* ?>
			<a href="<?php print SB_Route::_('index.php?mod=customers&task=get_customer&id='.$c->customer_id); ?>">
				<?php print SB_Text::_('View', 'customers')?>
			</a>
			*/ ?>
			<a href="<?php print $this->Route('index.php?mod=customers&view=edit&id='.$c->customer_id); ?>" title="<?php print $this->__('Edit')?>"
				class="btn btn-default btn-sm">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a class="confirm btn btn-default btn-sm" data-message="<?php print $this->__('Are you sure to delete the customer?', 'customers'); ?>" 
				href="<?php print $this->Route('index.php?mod=customers&task=delete&id='.$c->customer_id); ?>"
				title="<?php print $this->__('Delete')?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++;endforeach; else: ?>
	<tr>
		<td colspan="4"><?php print $this->__('There are no customers registered yet', 'mb_c'); ?></td>
	</tr>
	<?php endif; ?>
	</tbody>
	</table>
	<?php lt_pagination($this->Route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
		<?php /* ?>
	<div class="">
		<div id="customer-data-panel" class="panel panel-primary">
			<div class="panel-heading"><?php print SB_Text::_('Customer', 'customers')?></div>
			<div id="customer-data" class="panel-body">
				<div class="row">
					<div class="col-md-2">
						<img src="<?php print BASEURL; ?>/images/empty-avatar.png" alt="" class="img-thumbnail" />
					</div>
					<div class="col-md-10">
						<div class="row-fluid">
							<div class="span2"><b><?php print SB_Text::_('Name', 'customers')?>&emsp;&emsp;&emsp;:</b>
							<label style="color: blue"><?php print isset($customerView) ? $customerView->name : '';?></label>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span2"><b><?php print SB_Text::_('Email', 'customers')?>&emsp;&emsp;&emsp; :</b>
							<label style="color: blue"><?php print isset($customerView) ? $customerView->email : '';?></label>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span2"><b><?php print SB_Text::_('Telephone', 'customers')?>&nbsp; :</b>
							<b style="color: blue"><?php print isset($customerView) ? $customerView->phone : '';?></b>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	jQuery(function()
	{
		jQuery('.btn-view-customer').click(function()
		{
			return false;
		});
	});
	</script>
	*/?>
</div>