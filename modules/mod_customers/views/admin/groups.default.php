<?php
?>
<div class="wrap">
	<h2 id="page-title">
		<?php _e('Customer Groups', 'customers'); ?>
		<a href="<?php print $this->Route('index.php?mod=customers&view=groups.new'); ?>" class="btn btn-success pull-right">
			<span class="glyphicon glyphicon-plus"></span> <?php _e('New Group', 'customers'); ?>
		</a>
	</h2>
	<table class="table">
	<thead>
	<tr>
		<th><?php _e('No.', 'customers'); ?></th>
		<th><?php _e('Group', 'customers'); ?></th>
		<th><?php _e('Customers', 'customers'); ?></th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<?php if( is_array($this->groups) ):$i = 1; foreach($this->groups as $g): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $g->name; ?></td>
		<td><?php print $g->total_customers; ?></td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=customers&view=groups.edit&id='.$g->id); ?>" title="<?php _e('Edit', 'customers')?>"
				class="btn btn-default">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a class="confirm btn btn-default" data-message="<?php _e('Are you sure to delete the customer group?', 'customers'); ?>" 
				href="<?php print $this->Route('index.php?mod=customers&task=groups.delete&id='.$g->id); ?>"
				title="<?php _e('Delete', 'customers')?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; endif; ?>
	</tbody>
	</table>
</div>
