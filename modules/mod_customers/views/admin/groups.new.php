<?php
?>
<div class="wrap">
	<h2>
		<?php print $this->title;  ?>
	</h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="customers" />
		<input type="hidden" name="task" value="groups.save" />
		<?php if( $this->group ): ?>
		<input type="hidden" name="group_id" value="<?php print $this->group->id; ?>" />
		<?php endif; ?>
		<div class="form-group">
			<label><?php _e('Name', 'customers'); ?></label>
			<input type="text" name="gname" value="<?php print $this->group ? $this->group->name : ''; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Description', 'customers'); ?></label>
			<textarea name="description" rows="" cols="" class="form-control"><?php print $this->group ? $this->group->description : ''; ?></textarea>
		</div>
		<div class="form-group">
			<label><?php _e('Block customers into this group', 'customers'); ?></label>
			<input type="checkbox" name="blocked" value="1" <?php print $this->group && @$this->group->blocked == 1 ? 'checked' : ''; ?> />
		</div>
		<div class="form-group">
			<a href="<?php print $this->Route('index.php?mod=customers&view=groups.default'); ?>" class="btn btn-danger">
				<?php _e('Cancel', 'customers'); ?>
			</a>
			<button type="submit" class="btn btn-success"><?php _e('Save', 'customers'); ?></button>
		</div>
	</form>
</div>
