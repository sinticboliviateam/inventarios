<?php
$meta 			= $this->request->getVar('meta');
$order_status 	= mb_get_order_statuses();
$payment_status = mb_get_order_statuses();
?>
<div class="wrap">
	<form action="" method="post">
		<input type="hidden" name="mod" value="customers" />
		<input type="hidden" name="task" value="save_customer" />
		<?php if( isset($customer) ): ?>
		<input type="hidden" name="customer_id" value="<?php print $customer->customer_id; ?>" />
		<?php endif;?>
		<h2 id="page-title">
			<?php print $page_title; ?>
			<div class="pull-right">
				<a href="<?php print $this->Route('index.php?mod=customers'); ?>" class="btn btn-danger"><?php _e('Cancel', 'customers'); ?></a>
				<button type="submit" class="btn btn-success"><?php isset($customer) ? _e('Save', 'customers') : _e('Add', 'customers'); ?></button>
			</div>
		</h2>
		<div>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab-general" data-toggle="tab"><?php _e('General', 'customers'); ?></a></li>
				<li><a href="#tab-history" data-toggle="tab"><?php _e('History', 'customers'); ?></a></li>
				<li><a href="#tab-attachments" data-toggle="tab"><?php _e('Attachments', 'customers'); ?></a></li>
				<?php b_do_action('customers_tabs', isset($customer) ? $customer : null); ?>
			</ul>
			<div class="tab-content">
				<div id="tab-general" class="tab-pane active">
					<h3 class="form-section-title"><?php _e('General Information', 'customers'); ?></h3>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('First Name', 'customers'); ?></label>
								<input type="text" id="first_name" name="first_name" value="<?php print isset($customer) ? $customer->first_name : $this->request->getString('first_name'); ?>"
									class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Last Name', 'customers'); ?></label>
								<input type="text" id="last_name" name="last_name" value="<?php print isset($customer) ? $customer->last_name : $this->request->getString('last_name'); ?>"
									class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Customer Type', 'customers'); ?></label>
								<select name="group_id" class="form-control">
									<option value="-1"><?php _e('-- group --', 'customers'); ?></option>
									<?php foreach($this->groups as $g): ?>
									<option value="<?php print $g->id; ?>" <?php print isset($customer) && $customer->group_id == $g->id ? 'selected' : ''; ?>>
										<?php print $g->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Telephone', 'customers'); ?></label>
								<input type="text" id="telephone" name="telephone" value="<?php print isset($customer) ? $customer->phone : $this->request->getString('telephone'); ?>"
									class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Mobile Telephone', 'customers'); ?></label>
								<input type="text" id="mobil_telephone" name="mobil_telephone" value="<?php print isset($customer) ? $customer->mobile : $this->request->getString('mobil_telephone'); ?>"
									class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Email', 'customers'); ?></label>
								<input type="email" id="email" name="email" value="<?php print isset($customer) ? $customer->email : $this->request->getString('email'); ?>"
									class="form-control" />
							</div>
						</div>
					</div>
					<H3 class="form-section-title"><?php _e('Business Information', 'customers'); ?></H3>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('Company', 'customers'); ?></label>
								<input type="text" id="meta_company" name="meta[company]" value="<?php print isset($customer) ? $customer->company : $this->request->getArrayVar('meta', 'company'); ?>"
									class="form-control" />
							</div>
							<div class="form-group">
								<label><?php print $this->__('NIT/RUC/NIF', 'customers'); ?></label>
								<input type="text" id="nit_ruc_nif" name="meta[_nit_ruc_nif]" value="<?php print isset($customer) ? $customer->_nit_ruc_nif : $this->request->getArrayVar('meta', '_nit_ruc_nif'); ?>"
									class="form-control" />
							</div>
							<div class="form-group">
								<label><?php print $this->__('Office Telephone', 'customers'); ?></label>
								<input type="text" name="meta[_office_telephone]" value="<?php print isset($customer) ? $customer->_office_telephone : ''; ?>"
									class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('City', 'customers'); ?></label>
								<input type="text" id="city" name="city" value="<?php print isset($customer) ? $customer->city : $this->request->getString('city'); ?>"
									class="form-control" />
							</div>
							<div class="form-group">
								<label><?php print $this->__('Address', 'customers'); ?></label>
								<input type="text" id="address" name="address" value="<?php print isset($customer) ? $customer->address_1 : $this->request->getString('address'); ?>"
									class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('No. Exterior - Interior', 'customers'); ?></label>
								<input type="text" name="meta[_num_exterior_interior]" class="form-control" 
									value="<?php print isset($customer) ? $customer->_num_exterior_interior : ''; ?>" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label><?php print $this->__('State', 'customers'); ?></label>
								<input type="text" name="meta[_state]" value="<?php print isset($customer) ? $customer->_state : ''; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php print $this->__('Country', 'customers'); ?></label>
								<select name="country_code" class="form-control">
									<option value="-1"><?php _e('-- country --', 'customers'); ?></option>
									<?php foreach(include INCLUDE_DIR . SB_DS . 'countries.php' as $code => $country): ?>
									<option value="<?php print $code; ?>" <?php print isset($customer) && $customer->country_code == $code ? 'selected' : ''; ?>><?php print $country; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label><?php _e('ZIP', 'customers'); ?></label>
								<input type="text" name="zip" value="<?php print isset($customer) ? $customer->zip_code : ''; ?>" class="form-control" />
							</div>
						</div>
					</div>
					<h3><?php _e('Store Information', 'customers'); ?></h3>
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label><?php _e('Store', 'customers'); ?></label>
								<select id="store_id" name="store_id" class="form-control">
									<?php foreach($stores as $s): ?>
									<option value="<?php print $s->store_id; ?>" 
										<?php print (isset($customer) && $s->store_id == $customer->store_id) ? 'selected' : ''; ?>>
										<?php print $s->store_name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>
				</div><!-- end id="tab-general" -->
				<div id="tab-history" class="tab-pane">
					<div class="input-group">
						<span class="input-group-addon"><?php _e('Search', 'customers'); ?></span>
						<input type="text" name="history_key" value="" placeholder="<?php _e('Search...', 'customers'); ?>" class="form-control" />
						<select name="history_status" class="form-control">
							<option value="-1"><?php _e('-- status --', 'customers'); ?></option>
							<?php foreach(mb_get_order_statuses() as $status => $label): ?>
							<option value="<?php print $status; ?>"><?php print $label; ?></option>
							<?php endforeach; ?>
						</select>
						<span class="input-group-addon">
							<button type="button" id="btn-filter-history" class="btn btn-default"><?php _e('Go', 'customers'); ?></button>
						</span>
					</div><br/>
					<div class="form-group">
						<a href="<?php print $this->Route('index.php?mod=customers&view=print_pp&id='.$customer->customer_id); ?>" 
							class="btn btn-warning" target="_blank">
							<span class="glyphicon glyphicon-print"></span> <?php _e('Print Pending Payments', 'customers'); ?>
						</a>
					</div>
					<table class="table">
					<thead>
					<tr>
						<th><?php _e('No.', 'customers'); ?></th>
						<th><?php _e('Order No.', 'customers'); ?></th>
						<th><?php _e('Status', 'customers'); ?></th>
						<th><?php _e('Payment Status', 'customers'); ?></th>
						<th><?php _e('Total', 'customers'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php $i = 1; $totals = 0; if( is_array($this->orders_history) ): foreach($this->orders_history as $o): ?>
					<tr>
						<td class="text-center"><?php print $i; ?></td>
						<td class="text-center"><?php print $o->order_id; ?></td>
						<td class="text-center">
							<?php
							$class = 'success'; 
							if( $o->status != 'complete' )
								$class = 'danger';
							?>
							<span class="label label-<?php print $class ?>"><?php print $order_status[$o->status]; ?></span>
						</td>
						<td class="text-center">
							<?php
							$class = 'success'; 
							if( $o->payment_status != 'complete' )
								$class = 'danger';
							?>
							<span class="label label-<?php print $class ?>"><?php print $payment_status[$o->payment_status]; ?></span>
						</td>
						<td class="text-right"><?php print $o->total; ?></td>
					</tr>
					<?php $i++; $totals += $o->total; endforeach;endif; ?>
					</tbody>
					<tfoot>
					<tr>
						<td colspan="4" class="text-right" style="font-weight:bold;font-size:20px;"><?php _e('Total', 'customers');?></td>
						<td class="text-right">
							<span class="label label-success" style="font-weight:bold;font-size:20px;">
								<?php print number_format($totals, 2, '.', ','); ?>
							</span></td>
					</tr>
					</tfoot>
					</table>
				</div><!-- end id="tab-history" -->
				<div id="tab-attachments" class="tab-pane">
				</div><!-- end id="tab-attachments" -->
				<?php b_do_action('customers_tab_contents', isset($customer) ? $customer : null); ?>
			</div><!-- end class="tab-content" -->
		</div>
		
		<?php /* 
		<div class="panel panel-primary">
			<div class="panel-heading">
				<?php print SB_Text::_('General Information')?>
			</div><!-- end class="panel-heading" -->
			<div class="panel-body">
				<?php SB_Module::do_action('before_customer_fields'); ?>
				
				
				<div class="form-group">
					<label><?php print SB_Text::_('Position', 'customers'); ?></label>
						<?php print SB_HtmlBuilder::writeInput('text', 'meta[position]', 
																isset($customer) ? $customer->position : SB_Request::getArrayVar('meta', 'position'), 
																'position', array('class' => 'form-control')); ?>
				</div>
				
				<div class="form-group">												
					<label><?php print SB_Text::_('Website', 'customers'); ?></label>
						<?php print SB_HtmlBuilder::writeInput('text', 'meta[website]', 
																isset($customer) ? $customer->website : SB_Request::getArrayVar('meta', 'website'), 
																'website', array('class' => 'form-control')); ?>
				</div>
				<div class="row-fluid">
					<span class="span2"><?php print SB_Text::_('Identity Document', 'customers'); ?></span>
					<span class="span3">
						<?php print SB_HtmlBuilder::writeInput('text', 'meta[identity_document]', 
																isset($customer) ? $customer->identity_document : SB_Request::getArrayVar('meta', 'identity_document'), 
																'identity_document', array('class' => 'input-medium')); ?></span>
					
				</div>
				
				<?php SB_Module::do_action('before_customer_notes_field', isset($customer) ? $customer : null); ?>
				<div class="row-fluid">
					<label><?php print SB_Text::_('Notes', 'mb_c'); ?></label>
					<textarea name="meta[notes]" class="form-control"><?php print isset($customer) ? $customer->notes : SB_Request::getString('notes'); ?></textarea>
				</div>
				<?php SB_Module::do_action('after_customer_fields', isset($customer) ? $customer : null); ?>
			</div>
			<?php SB_Module::do_action('customer_tabs', isset($customer) ? $customer : null); ?>
		</div><!-- end class="easyui-tabs" -->
		<div class="form-actions">
			<button class="btn btn-success" type="submit"><?php print SB_Text::_('Save'); ?></button>
			<a class="btn btn-danger" href="<?php print SB_Route::_('index.php?mod=customers'); ?>"><?php print SB_Text::_('Cancel'); ?></a>
			<?php SB_Module::do_action('customer_buttons'); ?>
		</div>
		*/?>
	</form>
	<script>
	jQuery(function()
	{
		jQuery('#identity_document').keyup(function()
		{
			jQuery('#nit_ruc_nif').val(this.value);
		});
	});
	</script>
</div>