<?php
?>
<style type="text/css">
#print-preview
{
	margin:10px auto;
	padding:10px;
	border:1px solid #000;
	box-shadow:5px 5px 0px 0px #000;
	background:#fff;
	position:relative;
	min-height:550px;
}
@media print
{
	body, #container, #content, .wrap{margin:0 !important;padding:0 !important;}
	#menu,#footer{display:none !important;}
	.no-print{display:none !important;}
	#print-preview{margin:0 !important;display:block !important;border:0;}
}
</style>
<div class="wrap">
	<div id="print-preview">
		<?php print $print_content; ?>
	</div>
	<div class="no-print" style="text-align:center;">
		<a href="<?php print $back_link ?>" class="btn btn-danger"><?php _e('Back', 'mb'); ?></a>
		<a href="javascript:;" onclick="mb_print();" class="btn btn-success"><?php print SBText::_('Print', 'mb'); ?></a>
	</div>
</div>
