<?php
class SB_CustomersWidget
{
	protected static $_instance = -1;
	protected $_columns = array();
	public $ShowCheckbox = false;
	public $ShowButtons = true;
	public $onButtonSelectClicked;
	public $onButtonCancelClicked;
	public $height = 200;
	
	public function __construct()
	{
		self::$_instance++;
	}
	public function setColumns($cols)
	{
		$this->_columns = $cols;
	}
	public function widget()
	{
		ob_start();
		$model = SB_Model::getModelInstance('Customers', 'SB_', 'mod_customers');
		$customers = $model->getCustomers();
		$customers_widget_id = 'widget-customers-'.self::$_instance;
		?>
		<table id="<?php print $customers_widget_id ?>" class="easyui-datagrid" title="<?php print SB_Text::_('Customers', 'mb_c'); ?>" 
				data-options="singleSelect:true" style="height:<?php print $this->height; ?>px;">
		<thead>
		<tr>
			<?php if($this->ShowCheckbox): ?>
			<th data-options="field:'ck',checkbox:true"></th>
			<?php endif; ?>
			<?php foreach($this->_columns as $col): ?>
			<?php 
			$ops = json_encode($col['ops']);
			$ops = str_replace('"', "'", $ops);
			$ops = substr($ops, 1);
			$ops = substr($ops, 0, -1);
			?>
			<th data-options="<?php print $ops; ?>"><?php print $col['text']; ?></th>
			<?php endforeach; ?>
		</tr>
		</thead>
		<tbody>
		<?php foreach($customers as $c): ?>
		<tr>
			<?php if($this->ShowCheckbox): ?>
			<td><input type="checkbox" value="" name="ck"></td>
			<?php endif; ?>
			<td><?php print $c->person_code; ?></td>
			<td><?php printf("%s %s", $c->first_name, $c->last_name); ?></td>
			<td><?php print $c->identity_document; ?></td>
			<td><?php print $c->nit_ruc_nif; ?></td>
		</tr>	
		<?php endforeach; ?>
		</tbody>
		</table>
		<?php if($this->ShowButtons): ?>
		<p>
			<a href="javascript:;" class="easyui-linkbutton" 
				onclick="<?php print $this->onButtonSelectClicked; ?>(jQuery('#<?php print $customers_widget_id?>'));">
				<?php print SB_Text::_('Select', 'mb_c'); ?>
			</a>
			<a href="javascript:;" class="easyui-linkbutton" onclick="<?php print $this->onButtonCancelClicked; ?>()">
				<?php print SB_Text::_('Cancel', 'mb_c'); ?>
			</a>
		</p>
		<?php endif; ?>
		<?php 
		return ob_get_clean();
	}
}