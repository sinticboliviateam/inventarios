<?php
class LT_AdminControllerEmono extends SB_Controller
{
	public function task_settings()
	{
		$processors = emono_get_payment_processors();
		$ops = sb_get_parameter('emono_ops');
		
		sb_set_view_var('ops', $ops);
		sb_set_view_var('processors', $processors);
	}
	public function task_save_settings()
	{
		$old_ops = (array)sb_get_parameter('emono_ops');
		$ops = SB_Request::getVar('ops');
		//$ops = array_merge($old_ops, $ops);
		SB_Module::do_action_ref('emono_save_settings', $ops);
		sb_update_parameter('emono_ops', $ops);
		SB_MessagesStack::AddMessage(__('The settings has been saved', 'emono'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=emono&view=settings'));
	}
}