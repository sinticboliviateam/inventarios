<?php
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Session;

class SB_MBCart extends SB_ORMObject
{
	protected $items = array();
	protected $sessionData = null;
	
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function Start()
	{
		$this->items =& SB_Session::getVar('cart_items');
		if( !$this->items || !is_array($this->items) )
		{
			SB_Session::setVar('cart_items', array());
			$this->items =& SB_Session::getVar('cart_items');
		}
	}
	public function AddProduct($id, $qty = 1, $increase = true)
	{
		$key = md5($id);
		if( !isset($this->items[$key]) )
		{
			$this->items[$key] = array(
					'product_id' 	=> $id,
					'quantity'		=> $qty
			);
		}
		else
		{
			if( $increase )
				$this->items[$key]['quantity'] += $qty;
			else
				$this->items[$key]['quantity'] = $qty;
		}
		return true;
		/*
		SB_MessagesStack::AddMessage(__('Product added to cart', 'emono'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=emono&view=product&id='.$id));
		*/
	}	
	/**
	 * Check if a product is already into cart
	 * 
	 * @param int $id 
	 * @return  bool
	 */
	public function ProductInCart($id)
	{
		$key = md5($id);
		return isset($this->items[$key]);
	}
	public function RemoveProduct($id)
	{
		$key = md5($id);
		if( isset($this->items[$key]) && $this->items[$key]['quantity'] == 1 )
		{
			unset($this->items[$key]);
		}
		if( isset($this->items[$key]) && $this->items[$key]['quantity'] > 1 )
		{
			$this->items[$key]['quantity']--;
		}
	}
	public function ClearItems()
	{
		$this->items = array();
		$this->sessionData = null;
		//##update session data
		SB_Session::unsetVar('cart');
		SB_Session::unsetVar('cart_items');
	}
	public function GetItems()
	{
		$ids = array();
		foreach($this->items as $item)
		{
			$ids[] = $item['product_id'];
		}
		if( !count($ids) )
			return array();
		$query = "SELECT * FROM mb_products WHERE product_id IN(".implode(',', $ids).")";
		$products = array();
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$prod = new SB_MBProduct();
			$prod->SetDbData($row);
			$key = md5($row->product_id);
		
			$products[] = (object)array(
					'id'			=> $row->product_id,
					'name'			=> $row->product_name,
					'short_name'	=> substr($row->product_name, 0, 10) . '...',
					'quantity' 		=> $this->items[$key]['quantity'],
					'price'			=> (float)$prod->product_price,
					'product'		=> $prod
					
			);
			
		}
		return $products;
	}
	public function GetTotalItems()
	{
		$total = 0;
		foreach($this->GetItems() as $item)
		{
			$total += (int)$item->quantity;
		}
		return $total;
	}
	public function GetSubtotalProducts()
	{
		$totals = 0;
		foreach($this->GetItems() as $item)
		{
			$totals += $item->price * $item->quantity;
		}
		return (float)$totals;//number_format($totals, 2);
	}
	public function GetSubtotal()
	{
		$totals = 0;
		foreach($this->GetItems() as $item)
		{
			$totals += $item->price * $item->quantity;
		}
		return (float)$totals;//number_format($totals, 2);
	}
	public function GetTotals()
	{
		$totals = 0;
		foreach($this->GetItems() as $item)
		{
			$totals += $item->price * $item->quantity;
		}
		return (float)$totals;//number_format($totals, 2);
	}
}