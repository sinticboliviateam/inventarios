<?php
abstract class SB_MBPaymentProcesor extends SB_ORMObject
{
	protected $id;
	protected $name;
	protected $description;
	protected $version;
	protected $apiVersion;
	protected $status = null;
	protected $settings;
	
	public function __construct()
	{
		
	}
	public function SetDbData($data){}
	public function GetDbData($id){}
	public function SetSettings($settings){$this->settings = (object)$settings;}
	abstract public function ShowSettings($ops);
	abstract public function SaveSettings(&$ops);
	abstract public function Show();
	abstract public function Redirect();
}