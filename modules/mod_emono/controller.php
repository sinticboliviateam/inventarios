<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Globals;

class LT_ControllerEmono extends SB_Controller
{
	public function __construct($doc = null)
	{
		parent::__construct($doc);
		$this->document->templateFile = 'tpl-emono.php';
		//##get settings
		$this->ops = (object)sb_get_parameter('emono_ops', array());
	}
	public function ShowView($print = true)
	{
		sb_set_view_var('ops', $this->ops);
		parent::ShowView($print);
	}
	public function task_default()
	{
		if( !SB_Module::moduleExists('mb') )
		{
			SB_MessagesStack::AddMessage(__('The module monobusiness is not installed, please install it.', 'emono'), 'error');
			return false;
		}
		$sortby 	= SB_Request::getString('sortby', 'desc');
		$limit		= SB_Request::getInt('limit');
		$page		= SB_Request::getInt('page', 1);
		$keyword	= SB_Request::getString('keyword');
		$price_from	= SB_Request::getFloat('pf');
		$price_to	= SB_Request::getFloat('pt');
		//$items_per_page = defined('PRODUCT_PER_PAGE') ? PRODUCT_PER_PAGE : 16;
		SB_Module::do_action_ref('emono_before_show_default_products', $this);
		if( isset($this->continue_task) && !$this->continue_task )
			return true;
		$args = array(
			'page'				=> $page,
			'keyword'			=> $keyword,
			'orderby'			=> $sortby,
			'items_per_page'	=> $limit ? $limit : 16,
			'price_from'		=> $price_from,
			'price_to'			=> $price_to
		);	
		
		list($products, $total_rows, $pages, $current_page) = emono_filter_products($args);	
		
		if( isset($this->ops->infinite_scroll) && (int)$this->ops->infinite_scroll )
		{
			sb_add_js_global_var('emono', 'infinite_scroll', true);
		}
		
		$layout = @$this->ops->layout == 'grid' ? 'layout-grid.php' : 'layout-list.php';
		$layout = lt_get_view('emono', $layout);
		$title = __('Products', 'emono');
		sb_set_view_var('products', $products);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $current_page);
		sb_set_view_var('layout', $layout);
		sb_set_view_var('title', $title);
		$this->document->templateFile = 'tpl-emono.php';
		$this->document->SetTitle($title);
	}
	public function task_category()
	{
		$id 	= SB_Request::getInt('id');
		$slug	= SB_Request::getString('slug');
		$sortby = SB_Request::getString('sortby');
		$limit	= SB_Request::getInt('limit');
		$price_from	= SB_Request::getFloat('pf');
		$price_to	= SB_Request::getFloat('pt');
		
		$cat 	= new SB_MBCategory($id ? $id : $slug);
		$title 	= sprintf(__("%s Category", 'emono'), $cat->name);
		$this->document->SetTitle($title);
		
		$args = array(
			'cats' 				=> array($cat->category_id),
			'page'				=> SB_Request::getInt('page'),
			'items_per_page'	=> $limit ? $limit : 16,
			'orderby'			=> $sortby,
			'price_from'		=> $price_from,
			'price_to'			=> $price_to
		);
		
		list($prods, $total_rows, $pages, $page) = emono_filter_products($args);
		
		$layout = @$this->ops->layout == 'grid' ? 'layout-grid.php' : 'layout-list.php';
		$layout = lt_get_view('emono', $layout);
		//##increase category views
		//TODO: Add meta table for categories or add table column for views
		
		if( isset($this->ops->infinite_scroll) && (int)$this->ops->infinite_scroll )
		{
			sb_add_js_global_var('emono', 'infinite_scroll', true);
		}
		sb_add_js_global_var('emono', 'category_id', $id);
		sb_set_view_var('category', $cat);
		sb_set_view_var('products', $prods);
		sb_set_view_var('layout', $layout);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $page);
		sb_set_view_var('title', $title);
		SB_Module::do_action('emono_berfore_show_category', $cat);
	}
	public function task_search()
	{
		if( !SB_Module::moduleExists('mb') )
		{
			SB_MessagesStack::AddMessage(__('The module monobusiness is not installed, please install it.', 'emono'), 'error');
			return false;
		}
		$keyword	= SB_Request::getString('keyword');
		$sortby 	= SB_Request::getString('sortby', 'creation_date');
		$limit		= SB_Request::getInt('limit');
		$price_from	= SB_Request::getFloat('pf');
		$price_to	= SB_Request::getFloat('pt');
		/*
		SB_Module::do_action_ref('emono_before_show_default_products', $this);
		if( isset($this->continue_task) && !$this->continue_task )
			return true;
		*/	
		//$tables		= array('mb_products');
		//$columns 	= array('*');
		//$order_by 	= 'creation_date';
		
		$args = array(
			'page'				=> SB_Request::getInt('page', 1),
			'keyword'			=> $keyword,
			'items_per_page'	=> $limit ? $limit : 16,
			'orderby'			=> $sortby,
			'price_from'		=> $price_from,
			'price_to'			=> $price_to
		);
		list($products, $total_rows, $pages, $current_page) = emono_filter_products($args);
		if( isset($this->ops->infinite_scroll) && (int)$this->ops->infinite_scroll )
		{
			sb_add_js_global_var('emono', 'infinite_scroll', true);
		}
		$title = !empty($keyword)  ? sprintf(__('Search results for "%s"', 'emono'), $keyword) :
										__('Search results', 'emono');
		$layout = @$this->ops->layout == 'grid' ? 'layout-grid.php' : 'layout-list.php';
		$layout = lt_get_view('emono', $layout);
		
		$this->document->templateFile = 'tpl-emono.php';
		$this->SetView('search-results');
		sb_set_view_var('title', $title);
		sb_set_view_var('products', $products);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $current_page);
		sb_set_view_var('layout', $layout);
		$this->document->SetTitle($title);
	}
	public function task_product()
	{
		//var_dump(__METHOD__);
		$id 	= SB_Request::getInt('id');
		$slug	= SB_Request::getString('slug');
		$product = new SB_MBProduct($id > 0 ? $id : $slug);
		//var_dump($id, $slug);print_r($product);
		$views = (int)$product->_views;
		
		//##increase product views
		mb_update_product_meta($product->product_id, '_views', $views + 1);
		sb_add_style('lightgallery', BASEURL . '/js/lightGallery/css/lightgallery.min.css');
		sb_add_script(BASEURL . '/js/lightGallery/js/lightgallery.min.js', 'lightgallery');
		sb_set_view_var('product', $product);
		SB_Globals::SetVar('product', $product);
		//##set opengraph tags
		SB_Module::add_action('lt_header', function()
		{
			$product = SB_Globals::GetVar('product');
			$description = preg_replace("/\s+/", " ", $product->excerpt);
			$description = preg_replace('/(\[.*\])/', '', $description);
			?>
			<meta property="og:image" content="<?php print $product->getFeaturedImage('350x350')->GetUrl() ?>" />
			<meta property="og:title" content="<?php print $product->product_name; ?>" />
			<meta property="og:type" content="product" />
			<meta property="og:description" content="<?php print $description; ?>" />
			<?php
		});
		$this->document->AddBodyClass('product');
		$this->document->SetTitle($product->product_name, 'emono');
		SB_Module::do_action_ref('emono_before_show_product', $this);
	}
	public function task_add_to_cart()
	{
		$id = SB_Request::getInt('id');
		$qty = SB_Request::getInt('qty', 1);
		$ajax = SB_Request::getInt('ajax');
		$increase = SB_Request::getInt('increase', 1);
		if( !$id )
		{
			$error = __('Invalid product identifier', 'emono');
			if( $ajax )
				sb_response_json(array('status' => 'error', 'error' => $error));
			SB_MessagesStack::AddMessage($error, 'error');
		}
		$prod = new SB_MBProduct($id);
		if( !$prod->product_id )
		{
			$error = __('The product does not exists', 'emono');
			if( $ajax )
				sb_response_json(array('status' => 'error', 'error' => $error));
			SB_MessagesStack::AddMessage($error, 'error');
		}
		
		$cart = LT_ModEMono::GetCart();
		$msg = $cart->ProductInCart($id) ? __('The product has been updated', 'emono') : 
											__('The product has been added to cart', 'emono');
		$cart->AddProduct($id, $qty, $increase);
		
		if( $ajax )
			sb_response_json(array('status' => 'ok', 'message' => $msg));
		SB_MessagesStack::AddMessage($msg, 'success');
		$link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $prod->link;
		sb_redirect($link);
 	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		$cart = LT_ModEMono::GetCart();
		$cart->RemoveProduct($id);
		$link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : SB_Route::_('index.php?mod=emono&view=cart');
		SB_MessagesStack::AddMessage(__('The products has been deleted from cart', 'emono'), 'success');
		sb_redirect($link);
	}
	public function task_clear_cart()
	{
		$cart = LT_ModEMono::GetCart();
		$cart->ClearItems();
		SB_MessagesStack::AddMessage(__('The cart has been cleared', 'emono'));
	}
	public function task_cart()
	{
		$cart = LT_ModEMono::GetCart();
		sb_set_view_var('cart', $cart);
		$this->document->SetTitle(__('Your Shopping Cart', 'emono'));
	}
	public function task_checkout()
	{
		$cart 		= LT_ModEMono::GetCart();
		
		$countries 	= include INCLUDE_DIR . SB_DS . 'countries.php';
		
		sb_set_view_var('processors', emono_get_payment_processors());
		sb_set_view_var('countries', $countries);
		sb_set_view_var('cart', $cart);
		$this->document->templateFile = 'tpl-emono.php';
		$this->document->SetTitle(__('Checkout', 'emono'));
	}
	public function ajax_get_more_products()
	{
		$cat_id			= SB_Request::getInt('cat_id');
		$current_page 	= SB_Request::getInt('cpage');
		//if( !$current_page )
			//$current_page = 1;
		$page = $current_page + 1;
		$args = array(
			'page'				=> $page,
			'items_per_page'	=> (int)@$this->ops->limit ? (int)$this->ops->limit : 16
		);
		if( $cat_id )
		{
			$args['cats'] = array($cat_id);
		}
		/*
		if( $sortby )
		{
			if( $sortby == 'oldest' )
			{
				$args['order'] = 'ASC';
			}
			elseif( $sortby == 'name_asc' )
			{
				$args['orderby'] = 'p.product_name';
				$args['order'] = 'ASC';
			}
			elseif( $sortby == 'name_desc' )
			{
				$args['orderby'] = 'p.product_name';
				$args['order'] = 'DESC';
			}
		}
		*/
		list($prods, $total_rows, $pages, $page) = emono_query_products($args);
		$html = '';
		ob_start();
		foreach($prods as $p)
			lt_include_partial('emono', 'product.php', array('p' => $p));
		$html = ob_get_clean();
		sb_response_json(array(
			'status' 			=> 'ok',
			'prods'				=> $prods,
			'html'				=> $html,
			'total_pages'		=> $pages,
			'current_page'		=> $page
		));
	}
	public function task_get_quote()
	{
		$countries 	= include INCLUDE_DIR . SB_DS . 'countries.php';
		$ajax = SB_Request::getInt('ajax');
		$data = SB_Request::getVars(array(
			'first_name',
			'last_name',
			'company',
			'email',
			'phone',
			'country_code',
			'address',
			'city',
			'postcode',
			'notes'
		));
		$cart 		= LT_ModEMono::GetCart();
		$customer_data = array(
			'first_name'	=> $data['first_name'],
			'last_name'		=> $data['last_name'],
			'company'		=> $data['company'],
			'phone'			=> $data['phone'],
			'address_1'		=> $data['address'],
			'zip_code'		=> $data['postcode'],
			'city'			=> $data['city'],
			'country'		=> $countries[$data['country_code']],
			'country_code'	=> $data['country_code'],
			'status'		=> 'enabled'
		);
		$customer_meta = array(
			'_nit_ruc_nif' => '',
			'company'		=> $data['company'],
			'_billing_name'	=> !empty($data['company']) ? $data['company'] : sprintf("%s %s", $data['first_name'], $data['last_name'])
		);
		$store_id		= 1;
		$customer_id 	= 0;
		//##try to find the customer
		$customer 	= mb_get_customer_by($data['email']);
		//print_r($customer);die($data['email']);
		if( $customer && $customer->customer_id )
		{
			$customer_data['customer_id'] = $customer->customer_id;
			//##update customer info
			$customer_id = mb_customers_insert_new($customer_data, $customer_meta);
		}
		else
		{
			$customer_data['email']	= $data['email'];
			//##create a new customer
			$customer_id = mb_customers_insert_new($customer_data, $customer_meta);
			
		}
		$total_items 	= $cart->GetTotalItems();
		$tax_id			= 0;
		$tax_percent	= 0;
		$subtotal		= $cart->GetSubtotal();
		$total_tax		= 0;
		$total			= $cart->GetTotals();
		$customer		= new SB_MBCustomer($customer_id);
		$cdate			= date('Y-m-d H:i:s');
		$quote = array(
				'name'					=> sprintf(__('Website Quote - %s', 'emono'), $customer->GetBillingName()),
				'customer_id' 			=> $customer_id,
				//'user_id'				=> sb_get_current_user()->user_id,
				'store_id'				=> $store_id,
				'items'					=> $total_items,
				'tax_id'				=> $tax_id,
				'tax_rate'				=> $tax_percent,
				'subtotal'				=> $subtotal,
				'total_tax'				=> $total_tax,
				'discount'				=> 0,
				'total'					=> $total,
				'notes'					=> $data['notes'],
				'status'				=> 'created',
				'quote_date'			=> $cdate,
				'last_modification_date'=> $cdate,
		);
		
		$items = array();
		foreach($cart->GetItems() as $item)
		{
			$items[] = array(
				'product_id'	=> $item->id,
				'item_code'		=> $item->product->product_code,
				'name'			=> $item->name,
				'quantity'		=> $item->quantity,
				'price'			=> $item->price,
				'subtotal'		=> $item->price * $item->quantity,
				'total'			=> $item->price * $item->quantity,
				'status'					=> 'created',
				'last_modification_date'	=> date('Y-m-d H:i:s'),
				'creation_date'				=> date('Y-m-d H:i:s')
			);
		}
		
		$quote_id = mb_quote_insert($quote, $items);
		//##insert quote meta
		SB_Meta::addMeta('mb_quote_meta', '_print_prices', 1, 'quote_id', $quote_id);
		//print_r($quote);print_r($items);die();
		//##send quote by email
		$quote = new SB_MBQuote($quote_id);
		$business 	= (object)sb_get_parameter('mb_settings');
		$pdf 		= mb_get_pdf_instance('', '', 'dompdf');
		ob_start();
		include SB_Module::do_action('quote_tpl', MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php');
		$quote_html = ob_get_clean();
		$pdf_file = dirname(__FILE__) . SB_DS . time() . '.pdf';
		$pdf->loadHtml($quote_html);
		$pdf->render();
		$output = $pdf->output();
		file_put_contents($pdf_file, $output);		
		$subject = sprintf(__('%s - Your products quote'), SITE_TITLE);
		$body = sprintf(__('Hello %s'), $customer->firstname) . '<br/>'.
				__("Please find your quote as attachment<br/><br/>", 'emono').
				__("Regards,", 'emono');
		$headers = array(
			'from'	=> 'From: ' . SITE_TITLE . ' <no-reply@monobusiness.com>',
			'type' => 'Content-type: text/html');
		lt_mail($customer->email, $subject, $body, $headers, $pdf_file);
		unlink($pdf_file);
		$cart->ClearItems();
		SB_Session::setVar('to_download_quote_id', $quote_id);
		sb_redirect(SB_Route::_('index.php?mod=emono&view=thankyou'));
	}
	public function task_thankyou()
	{
		$buttons = array();
		if( @$this->ops->enable_quotes && $quote_id = SB_Session::getVar('to_download_quote_id') )
		{
			$buttons[] = array(
				'id'		=> '',
				'class'		=> 'btn-download-quote',
				'text'		=> '<span class="glyphicon glyphicon-download"></span> ' . __('Download Quote', 'emono'),
				'link'		=> SB_Route::_('index.php?mod=quotes&task=download'),
				'target'	=> ''
			);
		}
		$buttons 	= SB_Module::do_action('emono_thankyou_buttons', $buttons);
		$title 		= __('Thank you for you order', 'emono');
		$text 		= __('Your order has been received and processed, please review your email for more details.'. 'emono');
		sb_set_view_var('title', $title);
		sb_set_view_var('text', $text);
		sb_set_view_var('buttons', $buttons);
	}
	public function ajax_get_pp_form()
	{
		$pp = SB_Request::getString('pp');
		$processors = emono_get_payment_processors();
		if( !isset($processors[$pp]) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('The payment processor does not exists', 'emono')));
		}
		$proc = $processors[$pp];
		ob_start();
		$proc->Show();
		$form = ob_get_clean();
		sb_response_json(array('status' => 'ok', 'form' => $form));
	}
	public function task_place_order()
	{
		$countries 	= include INCLUDE_DIR . SB_DS . 'countries.php';
		$ajax = SB_Request::getInt('ajax');
		$data = SB_Request::getVars(array(
			'first_name',
			'last_name',
			'company',
			'email',
			'phone',
			'country_code',
			'address',
			'city',
			'postcode',
			'notes',
			'processor'
		));
		
		$customer_data = array(
			'first_name'	=> $data['first_name'],
			'last_name'		=> $data['last_name'],
			'company'		=> $data['company'],
			'phone'			=> $data['phone'],
			'address_1'		=> $data['address'],
			'zip_code'		=> $data['postcode'],
			'city'			=> $data['city'],
			'email'			=> $data['email'],
			'country'		=> $countries[$data['country_code']],
			'country_code'	=> $data['country_code'],
			'status'		=> 'enabled'
		);
		$customer_meta = array(
			'_nit_ruc_nif' => '',
			'company'		=> $data['company'],
			'_billing_name'	=> !empty($data['company']) ? $data['company'] : sprintf("%s %s", $data['first_name'], $data['last_name'])
		);
		$order_id = emono_insert_order($customer_data, $customer_meta, $data);
		mb_add_order_meta($order_id, '_payment_processor', $data['processor']);
		sb_redirect(SB_Route::_('index.php?mod=emono&view=thankyou'));
	}
}
