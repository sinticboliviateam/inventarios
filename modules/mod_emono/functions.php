<?php
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Route;

/**
 * Write the breadcrumbs
 * 
 * @param SB_MBProduct|SB_MBCategory $product 
 * @param string $sep 
 * @return  null
 */
function emono_breadcrumb($object, $sep = '&gt')
{
	?>
	<div class="emono-breadcrumb">
		<div class="breadcrumb-inner">
			<a href="<?php print SB_Route::_('/'); ?>"><?php _e('Home', 'emono'); ?></a>
			<span class="pipe"><?php print $sep; ?></span>
			<span class="page">
				<a href="<?php print SB_Route::_('index.php?mod=emono'); ?>">
					<?php _e('Products', 'emono'); ?>
				</a>
			</span>
			<?php 
			if( is_a($object, 'SB_MBCategory') )
			{
				$the_cat = $object;
				$aux_cat = $the_cat;
				//var_dump($the_cat->parent);
				//var_dump($aux_cat->GetParentCategory());
				while( $cat = $aux_cat->GetParentCategory() )
				{
					?>
					<span class="pipe"><?php print $sep; ?></span>
					<span class="page">
						<a href="<?php print $cat->link; ?>">
							<?php print $cat->name; ?>
						</a>
					</span>
					<?php
					$aux_cat = $cat;
				}
				?>
				<span class="pipe"><?php print $sep; ?></span>
				<span class="page"><?php print $the_cat->name; ?></span>
				<?php
			}
			elseif( is_a($object, 'SB_MBProduct') )
			{
				foreach($object->GetCategories() as $cat)
				{
					?>
					<span class="pipe"><?php print $sep; ?></span>
					<span class="page">
						<a href="<?php print $cat->link; ?>">
							<?php print $cat->name; ?>
						</a>
					</span>
					<?php
				}
			}
			?>
		</div>
	</div>
	<?php 
}
function emono_get_featured_products($args = array())
{
	$args = array_merge(array(
			'limit' => 5,
	), $args);
	$dbh = SB_Factory::getDbh();
	$query = "SELECT p.* FROM mb_products p, mb_product_meta pm WHERE 1 = 1 " . 
				"AND status = 'publish' ".
				"AND p.product_id = pm.product_id ".
				"AND pm.meta_key = '_featured' ".
				"AND pm.meta_value = '1' ";
	if( $dbh->db_type == 'mysql' )
	{
		$query .= "ORDER BY RAND() "; 
	}
	elseif( $dbh->db_type == 'sqlite' || $dbh->db_type == 'sqlite3' )
	{
		$query .= "ORDER BY RANDOM() "; 
	}
	$query .= "LIMIT {$args['limit']}";
	$items = array();
	try
	{
		$rows = $dbh->FetchResults($query);
		foreach($rows as $row)
		{
			$p = new SB_MBProduct();
			$p->SetDbData($row);
			$items[] = $p;
		}
	}
	catch(Exception $e){die($e->getMessage());}
	return $items;
}
/**
 * Query products
 * 
 * @param array $args 
 * @return  
 */
function emono_query_products($args = array())
{
	$def = array(
		'cats'				=> null,
		'orderby'			=> 'p.creation_date',
		'order'				=> 'DESC',
		'base_type'			=> 'base',
		'items_per_page'	=> 25,
		'keyword'			=> null,
		'search_cols'		=> array('p.product_name'),
		'page'				=> 1,
		'in_ids'			=> null,
		'price_from'		=> null,
		'price_to'			=> null,
		'price_n'			=> 'product_price',
		'get_metas'			=> array(
			'_featured_image_id'
		)
	);
	$args 	= array_merge($def, $args);
	if( (int)$args['page'] <= 0 )
	{
		$args['page'] = 1;
	}
	SB_Module::do_action_ref('emono_query_products_args', $args);
	$args['price_from'] = (float)$args['price_from'];
	$args['price_to'] = (float)$args['price_to'];
	$dbh 			= SB_Factory::getDbh();
	$is_price_range	= (float)$args['price_from'] > 0 && (float)$args['price_to'] > 0;
	$cols 			= array('p.*');
	$where 	= array();	
	$tables = array('mb_products p');
	$joins	= array();
	$tpl_query = "SELECT {cols} FROM {tables} {joins} WHERE 1 = 1 {where} {order} {limit}";
	$where[] = array('op' => 'AND', 'cond' => "base_type = '{$args['base_type']}'");
	if( $is_price_range )
		$where[] = array('op' => 'AND', 'cond' => '', 'before' => '', 'after' => '(');
		
	if( (float)$args['price_from'] > 0 )
	{
		$where[] = array('op' => !$is_price_range ? 'AND' : '', 'cond' => "{$args['price_n']} >= {$args['price_from']}");
	}
	if( (float)$args['price_to'] > 0 )
	{
		$where[] = array('op' => 'AND', 'cond' => "{$args['price_n']} <= {$args['price_to']}");
	}
	if( $is_price_range )
		$where[] = array('op' => '', 'cond' => '', 'before' => '', 'after' => ')');
		
	if( $args['in_ids'] )
	{
		$ids = is_array($args['in_ids']) ? implode(',', $args['in_ids']) : trim($args['in_ids']);
		$where[] = array('op' => 'AND', 'cond' => "p.product_id IN($ids)");
	}
	if( $args['cats'] )
	{
		$cats = array();
		if( !is_array($args['cats']) )
		{
			$cats = explode(',', $args['cats']);
		}
		$cats = (array)$args['cats'];
		$tables[] = 'mb_product2category p2c';
		$where[] = array('op' => 'AND', 'cond' => 'p.product_id = p2c.product_id');
		if( count($cats) == 1 )
		{
			$cat_id = array_shift($cats);
			$where[] = array('op' => 'AND', 'cond' => 'p2c.category_id = '. (int)$cat_id);
		}
		else
		{
			$where[] = array('op' => 'AND', 'cond' => 'p2c.category_id IN('. implode(',', $cats) . ')');
		}
	}
	if( $args['keyword'] )
	{
		$where[] = array('op' => 'AND', 'cond' => '', 'before' => '', 'after' => '(');
		foreach($args['search_cols'] as $i => $col)
		{
			if( $i == 0 )
			{
				$where[] = array('op' => '', 'cond' => "$col LIKE '%{$args['keyword']}%'", 'before' => '');
			}
			else
			{
				$where[] = array('op' => 'OR', 'cond' => "$col LIKE '%{$args['keyword']}%'", 'before' => '', 'after' => '');
			}
		}
		$where[] = array('op' => '', 'cond' => '', 'before' => '', 'after' => ')');
	}
	//##query metas
	
	if( isset($args['get_metas']) && is_array($args['get_metas']) && count($args['get_metas']) )
	{
		$mi = 1;
		foreach($args['get_metas'] as $meta_key)
		{
			$cols[] = "(select m{$mi}.meta_value " .
						"from mb_product_meta m{$mi} " .
						"WHERE m{$mi}.product_id = p.product_id ".
						"AND m{$mi}.meta_key = '$meta_key') AS $meta_key";
			//$tables[] = "mb_product_meta m{$mi}";
			//$joins[] = "LEFT JOIN mb_product_meta m{$mi} ON (m{$mi}.product_id = p.product_id AND m{$mi}.meta_key = '{$meta_key}')";
			$mi++;
		}
	}
	$order 		= "ORDER BY {$args['orderby']} {$args['order']}";
	SB_Module::do_action_ref('before_query_products', $cols, $tables, $joins, $where, $order, $limit);
	$_cols 		= implode(',', $cols);
	$_tables 	= implode(',', $tables);
	$_joins		= implode(' ', $joins);
	$_where 	= '';
	$limit		= '';
	
	foreach($where as $w)
	{
		if( isset($w['before']) )
			$_where .= "{$w['before']}";
		$_where .= @"{$w['op']} {$w['cond']}";
		if( isset($w['after']) )
			$_where .= "{$w['after']}";
		$_where .= ' ';
	}
	$total_pages 	= 0;
	$total_rows		= 0;
	if( (int)$args['items_per_page'] > 0 )
	{
		
		$query = str_replace(array('{cols}', '{tables}', '{joins}', '{where}', '{order}', '{limit}'),
							array('COUNT(p.product_id)', $_tables, $_joins, $_where, '', ''),
							$tpl_query);
		$total_rows		= $dbh->GetVar($query);
		$total_pages	= ceil($total_rows/$args['items_per_page']);
		$offset			= $args['page'] == 1 ? 0 : ($args['page'] - 1) * $args['items_per_page'];
		$limit	= "LIMIT $offset, {$args['items_per_page']}";
		//print '<!--';var_dump($query);print '-->';
	}
	
	$query = str_replace(array('{cols}', '{tables}', '{joins}', '{where}', '{order}', '{limit}'),
							array($_cols, $_tables, $_joins, $_where, $order, $limit),
							$tpl_query);
	//print '<!--';var_dump($query);print '-->';
	$prods = array();
	foreach($dbh->FetchResults($query) as $row)
	{
		$p = new SB_MBProduct();
		$p->SetDbData($row);
		$prods[] = $p;
	}
	
	return array($prods, $total_rows, $total_pages, $args['page']);
}
/**
 * Loads and gets all available payments processors
 * @return array
 */
function emono_get_payment_processors()
{
	static $processors;
	if( $processors )
		return $processors;
	$processors = array();
	$list = array(
		array(
			'file' 	=> MOD_EMONO_DIR . SB_DS . 'processors' . SB_DS . 'processor.paypal.php',
			'class'	=> 'SB_MBPaypalProcessor'
		),
		array(
			'file' 	=> MOD_EMONO_DIR . SB_DS . 'processors' . SB_DS . 'processor.offline.php',
			'class'	=> 'SB_MBCCOfflineProcessor'
		),
	);
	$list = SB_Module::do_action('emono_payment_processors', $list);
	require_once MOD_EMONO_DIR . SB_DS . 'classes' . SB_DS . 'class.payment-processor.php';
	foreach($list as $pp)
	{
		require_once $pp['file'];
		if( !class_exists($pp['class']) )
			continue;
		$class = $pp['class'];
		$obj = new $class();
		if( lt_is_admin() )
		{
			SB_Module::add_action('emono_save_settings', array($obj, 'SaveSettings'));
		}
		$processors[$obj->id] = $obj;
	}
	
	return $processors;
}
function emono_insert_order($customer_data, $customer_meta, $data = array())
{
	$cart 			= LT_ModEMono::GetCart();
	$store_id		= 1;
	$customer_id 	= 0;
	//##try to find the customer
	$customer 	= mb_get_customer_by($customer_data['email']);
	//print_r($customer);die($data['email']);
	if( $customer && $customer->customer_id )
	{
		$customer_data['customer_id'] = $customer->customer_id;
		//##update customer info
		$customer_id = mb_customers_insert_new($customer_data, $customer_meta);
	}
	else
	{
		unset($customer_data['email']);
		//##create a new customer
		$customer_id = mb_customers_insert_new($customer_data, $customer_meta);
	}
	$total_items 	= $cart->GetTotalItems();
	$tax_id			= 0;
	$tax_percent	= 0;
	$subtotal		= $cart->GetSubtotal();
	$total_tax		= 0;
	$total			= $cart->GetTotals();
	$customer		= new SB_MBCustomer($customer_id);
	$cdate			= date('Y-m-d H:i:s');
	$order = array(
			'name'					=> sprintf(__('Website Order - %s', 'emono'), $customer->GetBillingName()),
			'store_id'				=> $store_id,
			'transaction_type_id'	=> -1,
			'customer_id' 			=> $customer_id,
			'items'					=> $total_items,
			'subtotal'				=> $subtotal,
			'total_tax'				=> $total_tax,
			'discount'				=> 0,
			'total'					=> $total,
			'details'				=> isset($data['notes']) ? $data['notes'] : null,
			'status'				=> 'created',
			'user_id'				=> $customer_id,
			'customer_id'			=> $customer_id,
			'order_date'			=> $cdate,
			'last_modification_date'=> $cdate,
	);
	
	$items = array();
	foreach($cart->GetItems() as $item)
	{
		$items[] = array(
			'product_id'				=> $item->id,
			'name'						=> $item->name,
			'quantity'					=> $item->quantity,
			'price'						=> $item->price,
			'subtotal'					=> $item->price * $item->quantity,
			'total'						=> $item->price * $item->quantity,
			'status'					=> 'created',
			'last_modification_date'	=> date('Y-m-d H:i:s'),
			'creation_date'				=> date('Y-m-d H:i:s')
		);
	}
	$order_id = mb_insert_sale_order($order, $items);
	//##clear cart
	$cart->ClearItems();
	SB_Module::do_action('emono_insert_order', $order_id);
	return $order_id;
}
/**
 * Search and filter products based on request parameters
 * 
 * @return  array
 */
function emono_filter_products($args)
{
	$def_args = array(
		'orderby'	=> 'p.creation_date'
	);
	$args 		= array_merge($def_args, $args);
	extract($args);
	
	if( $orderby == 'oldest' )
	{
		$args['orderby'] = 'p.creation_date';
		$args['order'] = 'ASC';
	}
	elseif( $orderby == 'name_asc' )
	{
		$args['orderby'] = 'p.product_name';
		$args['order'] = 'ASC';
	}
	elseif( $orderby == 'name_desc' )
	{
		$args['orderby'] = 'p.product_name';
		$args['order'] = 'DESC';
	}
	else
	{
		$args['orderby'] = 'p.creation_date';
	}
	
	//print_r($args);die();
	return emono_query_products($args);
}
