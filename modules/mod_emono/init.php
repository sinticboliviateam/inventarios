<?php
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Shortcode;
use SinticBolivia\SBFramework\Classes\SB_Route;

define('MOD_EMONO_DIR', dirname(__FILE__));
define('MOD_EMONO_URL', MODULES_URL . '/' . basename(MOD_EMONO_DIR));
require_once MOD_EMONO_DIR . SB_DS . 'classes' . SB_DS . 'class.cart.php';
require_once MOD_EMONO_DIR . SB_DS . 'functions.php';

class LT_ModEMono
{
	protected $ops;
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'emono', MOD_EMONO_DIR . SB_DS . 'locale');
		$this->AddActions();
		$this->AddShortcodes();
		$this->ops = (object)sb_get_parameter('emono_ops', array());
		
	}
	protected function AddActions()
	{
		SB_Module::add_action('init', array($this, 'action_init'));
		if( lt_is_admin() )
		{
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
			SB_Module::add_action('mb_product_general_info_after', array($this, 'action_mb_product_general_info_after'));
			SB_Module::add_action('product_tabs', array($this, 'action_product_tabs'));
			SB_Module::add_action('product_tabs_content', array($this, 'action_product_tabs_content'));
			SB_Module::add_action('menu_panel_items', array($this, 'action_menu_panel_items'));
			SB_Module::add_action('mb_save_product', array($this, 'action_mb_save_product'));
		}
		else
		{
			SB_Module::add_action('rewrite_routes', array($this, 'Routes'), 1);
			//##add default emono javascript
			sb_add_script(MOD_EMONO_URL . '/js/emono.js', 'emono', 0, true);
		}
	}
	public function AddShortcodes()
	{
		SB_Shortcode::AddShortcode('emono_products', array($this, 'shortcode_products'));
	}
	public function action_admin_menu()
	{
		SB_Menu::addMenuChild('menu-settings', __('Ecommerce Settings'), SB_Route::_('index.php?mod=emono&view=settings'), 
								'menu-emono-settings',
								'manage_backend'
		);
	}
	public function action_init()
	{
		$this->RegisterWidgets();
		$cart = self::GetCart();
	}
	protected function RegisterWidgets()
	{
		require_once MOD_EMONO_DIR . SB_DS . 'widgets' . SB_DS . 'widget.categories.php';
		require_once MOD_EMONO_DIR . SB_DS . 'widgets' . SB_DS . 'widget.cart.php';
		require_once MOD_EMONO_DIR . SB_DS . 'widgets' . SB_DS . 'widget.latest-products.php';
		require_once MOD_EMONO_DIR . SB_DS . 'widgets' . SB_DS . 'widget.most-viewed.php';
		require_once MOD_EMONO_DIR . SB_DS . 'widgets' . SB_DS . 'widget.filter.php';
	}
	/**
	 * Get the shoppping cart
	 * 
	 * @return SB_MBCart
	 */
	public static function GetCart()
	{
		static $cart;
		if( $cart == null )
		{
			$cart = new SB_MBCart();
			$cart->Start();
		}
		return $cart;
	}
	public function action_mb_product_general_info_after($product)
	{
		?>
		<div class="form-group">
			<label class="col-sm-2"><?php _e('Featured product', 'emono'); ?></label>
			<div class="col-sm-4">
				<input type="checkbox" name="meta[_featured]" value="1" <?php print $product && (int)$product->_featured == 1 ? 'checked' : ''; ?> />
			</div>
		</div>
		<?php
	}
	public function action_product_tabs()
	{
		lt_add_js('jquery-tags', BASEURL . '/js/tagging.min.js', 0, true);
		?>
		<li>
			<a href="#related-products" data-toggle="tab"><?php _e('Related Products', 'emono'); ?></a>
		</li>
		<li>
			<a href="#tags" data-toggle="tab"><?php _e('Tags', 'emono'); ?></a>
		</li>
		<?php
	}
	public function action_product_tabs_content($product)
	{
		$products 	= array();
		$related	= array();
		$tags		= array();
		if( $product )
		{
			//print_r($product->related);
			$products = $product->store_id ? SB_Warehouse::getStoreProducts($product->store_id, 1, -1, false) : array();
			$related = (array)json_decode($product->related);
			$_tags = lt_content_get_object_tags('product', $product->product_id);
			if( count($_tags) )
			{
				foreach($_tags as $tag)
				{
					$tags[] = $tag->str;
				}
			}
		}
		?>
		<div id="related-products" class="tab-pane">
			<div class="form-group row">
				<label class="col-sm-1 control-label"><?php _e('Products:', 'mb'); ?></label>
				<div class="col-sm-10">
					<div class="form-control" style="width:100%;height:200px;overflow:auto;">
						<?php if( isset($products) && is_array($products) ):foreach($products as $p): ?>
						<div><label>
							<input type="checkbox" name="meta[related][]" value="<?php print $p->product_id ?>" 
								class="" 
								<?php print ($product && in_array($p->product_id, $related)) ? 'checked' : ''; ?> />
							<?php print $p->product_name ?>
						</label></div>
						<?php endforeach; endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="tags" class="tab-pane">
			<div class="form-group">
				<div data-tags-input-name="tags" id="tagBox"></div>
			</div>
			<style>
			/* Tagging Basic Style */
			.tagging {border: 1px solid #CCCCCC;font-size: 1em;height: auto;padding: 10px 10px 15px;}
			.tagging.editable {cursor: text;}
			.tag {background: none repeat scroll 0 0 #EE7407;border-radius: 2px;color: white;cursor: default;display: inline-block;
				position: relative;
				white-space: nowrap;
				padding: 4px 20px 4px 0;
				margin: 5px 10px 0 0;
			}
			.tag span {background: none repeat scroll 0 0 #D66806;border-radius: 2px 0 0 2px;margin-right: 5px;padding: 5px 10px 5px;}
			.tag .tag-i {color: white;cursor: pointer;font-size: 1.3em;height: 0;line-height: 0.1em;position: absolute;right: 5px;top: 0.7em;text-align: center;width: 10px;}
			.tag .tag-i:hover {color: black;text-decoration: underline;}
			.type-zone {border: 0 none;height: auto;width: auto;min-width: 20px;display: inline-block;}
			.type-zone:focus {outline: none;}
			</style>
			<script>
			jQuery(function()
			{
				jQuery("#tagBox").tagging();
				jQuery("#tagBox").tagging('add', <?php print json_encode($tags); ?>);
				
			});
			</script>
		</div>
		<?php
	}
	public function action_menu_panel_items($menu)
	{
		require_once MOD_EMONO_DIR . SB_DS . 'partials' . SB_DS . 'menu_panel_items.php';
	}
	public function action_mb_save_product($id, $data)
	{
		$tags = SB_Request::getVar('tags');
		lt_content_set_object_tags($tags, 'product', $id);
	}
	public function shortcode_products($args)
	{
		$def_args = array(
			'page'				=> SB_Request::getInt('page', 1),
			'keyword'			=> SB_Request::getString('keyword'),
			'items_per_page'	=> 25,
			'sortby'			=> null,
			'limit'				=> 25
		);
		$args = array_merge($def_args, $args);
		
		$dbh 		= SB_Factory::getDbh();
		$tables		= array('mb_products');
		$columns 	= array('*');
		$order_by 	= 'creation_date';
		//$products = sb_get_navigation($order_by, 'SB_MBProduct', $columns, $tables, null, $items_per_page);
		
		if( $args['sortby'] )
		{
			if( $args['sortby'] == 'oldest' )
			{
				$args['order'] = 'ASC';
			}
			elseif( $args['sortby'] == 'name_asc' )
			{
				$args['orderby'] = 'p.product_name';
				$args['order'] = 'ASC';
			}
			elseif( $args['sortby'] == 'name_desc' )
			{
				$args['orderby'] = 'p.product_name';
				$args['order'] = 'DESC';
			}
		}
		list($products, $total_rows, $pages, $current_page) = emono_query_products($args);
		$layout = @$this->ops->layout == 'grid' ? 'layout-grid.php' : 'layout-list.php';
		$layout = lt_get_view('emono', $layout);
		if( isset($this->ops->infinite_scroll) && (int)$this->ops->infinite_scroll )
		{
			sb_add_js_global_var('emono', 'infinite_scroll', true);
		}
		/*
		sb_set_view_var('products', $products);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $current_page);
		*/
		//print_r($products);
		ob_start();
		lt_include_view('emono', 'default.php', array(
													'products' => $products, 
													'total_pages' => $pages, 
													'current_page' => $current_page,
													'show_title' => false,
													'layout'	=> $layout
												));
		return ob_get_clean();
	}
	public function Routes($routes)
	{
		$base 		= __('products', 'emono');
		$base_cat 	= __('category', 'emono');
		$checkout	= __('checkout', 'emono');
		$search		= __('search', 'emono');
		$cart		= __('cart', 'emono');
		//$routes['/^\/(.*)\/?/'] = 'mod=content&view=article&slug=$1';
		//$routes['/^\/([0-9a-zA-Z-_]+)\/?$/'] = 'mod=content&view=article&slug=$1';
		$routes['/^\/'.$base . '\/?$/'] = 'mod=emono';
		$routes['/^\/'.$base . '\/' . $search . '\/?$/'] = 'mod=emono&view=search';
		$routes['/^\/'.$base . '\/' . $cart . '\/?$/'] = 'mod=emono&view=cart';
		$routes['/^\/'.$base . '\/' . $checkout . '\/?$/'] = 'mod=emono&view=checkout';
		$routes['/^\/'.$base . '\/([0-9a-zA-Z-_]+)\/?$/'] = 'mod=emono&view=product&slug=$1';
		$routes['/^\/'.$base . '\/'.$base_cat . '\/([0-1a-zA-Z-_]+)\/?$/'] = 'mod=emono&view=category&slug=$1';
		
		//print_r($routes);
		return $routes;
	}
}
function lt_emono_rewrite($url, &$array)
{
	$base = BASEURL . '/' . __('products', 'emono');
	//print_r($url);
	//print_r($array);
	$url = $base . '/';
	$task	= isset($array['task']) ? $array['task'] : null;
	$view	= isset($array['view']) ? $array['view'] : null;
	if( $task )
	{
		if( $array['task'] == 'add_to_cart' )
		{
			$url .= '?task=add_to_cart&id=' . $array['id'];
			if( isset($array['qty']) )
				$url .= '&qty=' . $array['qty'];
		}
		
		return $url;
	}
	if( $view == 'category' )
	{
		$url .= __('category', 'emono') . '/' . $array['slug'] . '/';
	}
	elseif( $view == 'product' )
	{
		$url .= $array['slug'] . '/';
	}
	elseif( $view == 'checkout' )
	{
		$url .= __('checkout', 'emono') . '/';
	}
	elseif( $view == 'search' )
	{
		$url .= __('search', 'emono') . '/';
	}
	elseif( $view == 'cart' )
	{
		$url .= __('cart', 'emono') . '/';
	}
	unset($array['mod'], $array['task'], $array['view'], $array['id'], $array['slug']);

	return $url;
}
new LT_ModEMono();