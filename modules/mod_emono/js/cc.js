/* <![CDATA[ */ ;
(window.gaDevIds = window.gaDevIds || []).push('d6YPbH');

var card_number = document.getElementById('cardNumber');
var cleanCardNumber = function(e) 
{
	e.preventDefault();
	var pastedText = '';
	if (window.clipboardData && window.clipboardData.getData) {
		pastedText = window.clipboardData.getData('Text');
	} else if (e.clipboardData && e.clipboardData.getData) {
		pastedText = e.clipboardData.getData('text/plain');
	}
	this.value = pastedText.replace(/\D/g, '');
};
