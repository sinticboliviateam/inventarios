var emono = 
{
	current_page: 1,
	working: false,
	OnFilterOrderChange: function()
	{
		//jQuery(this).parents('form').submit();
		var url = jQuery(this).parents('form').attr('action');
		if( url.indexOf('?') == -1 )
			url += '?';
		else
			url += '&';
		url += 'sortby='+this.value;
		window.location = url;
	},
	SetInfiniteScroll: function()
	{
		window.onscroll = function(e)
		{
			try
			{
				//alert(document.body.offsetHeight);
				window.scrollMaxY = document.body.offsetHeight - window.innerHeight;
				if( window.scrollY >= window.scrollMaxY )
				{
					if( !emono.working )
					{
						//console.log('Getting more results');
						emono.working = true;
						jQuery('.infinite-scroll-loading').css('display', 'block');
						var params = 'mod=emono&task=ajax&action=get_more_products&cpage='+emono.current_page;
						if( lt.modules.emono && lt.modules.emono.category_id )
						{
							params += '&cat_id='+lt.modules.emono.category_id;
						}
						jQuery.get('index.php?'+params, function(res)
						{
							emono.working = false;
							jQuery('.infinite-scroll-loading').css('display', 'none');
							//console.log(res);
							if( res.status == 'ok' )
							{
								emono.current_page = res.current_page;
								if( res.prods )
								{
									//##set products into list container
									jQuery('#product_list').append(res.html);
								}
							}
						});
					}
				}
			}
			catch(e)
			{
				alert(e.message);
			}
		};
	},
	LoadMoreResults: function()
	{
		jQuery('#btn-load-more').css('display', 'none');
		jQuery('#infinite-scroll-loading').css('display', 'block');
		var params = 'mod=emono&task=ajax&action=get_more_products&cpage='+emono.current_page;
		if( typeof lt.modules.emono != 'undefined' && typeof lt.modules.emono.category_id != 'undefined' )
		{
			params += '&cat_id='+lt.modules.emono.category_id;
		}
		jQuery.get('index.php?'+params, function(res)
		{
			emono.working = false;
			jQuery('#btn-load-more').css('display', 'block');
			jQuery('#infinite-scroll-loading').css('display', 'none');
			//console.log(res);
			if( res.status == 'ok' )
			{
				emono.current_page = res.current_page;
				if( res.prods )
				{
					//##set products into list container
					jQuery('#product_list').append(res.html);
				}
			}
		});
	},
	SetupPaymentProcessorsEvents: function()
	{
		jQuery('#payment-processors .pp').click(function(e)
		{
			jQuery.get('index.php?mod=emono&task=ajax&action=get_pp_form&pp=' + this.value, function(res)
			{
				jQuery('#payment-processor-form').html(res.form);
			});
			return false;
		});
	},
	SetEvents: function()
	{
		emono.SetupPaymentProcessorsEvents();
	}
};
jQuery(function()
{
	try
	{
		emono.SetEvents();
		var filter_form = jQuery('.products_filter form:first');
		if( filter_form.length > 0 )
			jQuery(filter_form.get(0).sortby).change(emono.OnFilterOrderChange);
		if( lt.modules && lt.modules.emono && lt.modules.emono.infinite_scroll )
		{
			//emono.SetInfiniteScroll();
			jQuery('#btn-load-more').click(function(e)
			{
				//alert('');
				emono.LoadMoreResults();
				
				return false;
			});
		}
	}
	catch(e)
	{
		alert(e.message);
	}
});