��    h      \  �   �      �     �     �     �     �     �     	  	   	     	     /	     >	     S	     l	     }	  
   �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     

     
     +
     9
     G
  
   X
  	   c
     m
     r
     {
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
                          ,     5     ;     H     O     W     j     s  	   �     �     �     �     �     �     �     �     �     �               (     6  
   D     O  '   U     }     �     �  <   �  %   �       "   3     V  '   s     �     �  #   �     �     �       	        '     :     M  	   l     v  
   �  [   �     �                    #  �  *          0  	   =     G  	   \     f  
        �     �     �  +   �            
   6     A     M     ]     d     w     �     �     �     �     �     �     �               )     @     S     [     u     }     �     �     �  	   �     �     �     �     �                    &     8     G     M     V     f     t     {     �     �     �  	   �     �  
   �     �     �     �       (        ?     G      ^          �     �     �     �     �     �  :   �  	   .     8     N  D   e      �     �  %   �          #     B     `  (   |     �     �     �     �     �     �       
   1     <  	   R  P   \      �     �  	   �  	   �     �     R   &      <           9       B       Y   A   >   #   a   .       =   1       h   c   U      \   X            `      _      (   T      d      /          S       C                g      O   $   G   I       7   H      b   5      F       '   ;   %       0   D      N       4          ]                  2   K   :       M   !       	   V   f                 
   -              L       W   [   8   E       6       +       @   J                           P              3   Q   e   Z                  ^   )   "   ,       ?   *    %s - Your products quote %s Category Add Add to cart Address Allow Checkout Available Billing Details Business Email Button Checkout Text CVC (card security code) Card holder name Card number Categories Categories: Checkout City Code/SKU: Comments Company Name Continue Shopping Country Description Design Download Quote Ecommerce Settings Email Address Enable Orders Enable Quotes Featured product First Name Get Quote Grid Hello %s Home Infinite Scroll Invalid product identifier Last Name Latest Products Layout List Most Viewed Newest Oldest Order Notes Order Status Out of stock Payments Phone Place Order Postcode Price Price Filter Price: Product Product Categories Products Products Filter Products: Quantity Regards, Related Products Remove Same as billing details Save Search results Search results for "%s" Send email copy to Shipping Shipping Details Shopping Cart Show Comments Show Price Specs Store all card information in database. Tags Thank you for you order The cart has been cleared The module monobusiness is not installed, please install it. The payment processor does not exists The product does not exists The product has been added to cart The product has been updated The products has been deleted from cart The settings has been saved There are no products found There are no related products found View View more products View products We accept Website Order - %s Website Quote - %s You don't have items into cart Your Cart Your Shopping Cart Your order Your order has been received and processed, please review your email for more details.emono Your shopping cart is empty cart category products search Project-Id-Version: Ecommerce MonoBusiness
POT-Creation-Date: 2017-12-12 10:36-0400
PO-Revision-Date: 2017-12-12 10:39-0400
Last-Translator: 
Language-Team: Sintic Bolivia <info@sinticbolivia.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_
X-Poedit-SearchPath-0: .
 %s - Su cotizacion de productos %s Categoria Adicionar Adicionar al carrito Direccion Permitir Procesar Pedido Disponible Detalles de Facturaci&oacute;n Email de Negocio Text del Boton Procesar Pedido CVC (c&oacute;digo de seguridad de tarjeta) Nombre en la tarjeta N&uacute;mero de la tarjeta Categorias Categorias: Procesar Pedido Ciudad C&oacute;digo/SKU: Comentarios Nombre Compa&ntilde;ia Continuar Comprando Pais Descripcion Dise&ntilde;o Descargar Cotizaci&oacute;n Configuracion de Ecommerce Direccion de Email Habilitar Pedidos Habilitar Cotizaciones Producto destacada Nombres Obtener Cotizaci&oacute;n Rejilla Hola %s Inicio Scroll Infinito Identificador de producto Apellidos &Uacute;ltimos Productos Distribuci&oacute;n Lista M&aacute;s Vistos Nuevos Antiguos Notas del Pedido Estado del Pedido Fuera de stock Pagos Telefono Realizar Pedido Codigo postal Precio Filtro de Precio Precio: Producto Categorias de Producto Productos Filtro de Productos Productos: Cantidad Saludos cordiales Productos Relacionados Quitar Igual que detalles de facturaci&oacute;n Guardar Resultados de busqueda Resultados de busqueda para "%s" Enviar copia de email Envio Detalles de Envio Carrito de Compras Mostrar Comentarios Mostrar Precio Specificaciones Guardar la informaci&oacute;n de tarjeta en base de datos. Etiquetas Gracias por su pedido El carrito fue vaciado El modulo monobusiness no esta instalado, porfavor inst&aacute;lelo. El procesador de pago no existe. El producto no existe El producto fue adicionado al carrito El producto fue actualizado Productos borrados del carrito La configuracion fue guardada No se encontraron productos No se encontraron productos relacionados Ver Ver m&aacute;s productos Ver productos Nosotros aceptamos Pedido Sitio Web - %s Cotizacion Sitio Web - %s No tienes items en tu carrito Mi Carrito Tu Carrito de Compras Su pedido Su pedido fue recibido y procesado, porfavor revise su correo para mas detalles. Su carrito de compras esta vacio carrito categoria productos buscar 