<?php
//print_r($data);
$link = b_route('index.php?mod=emono&view=cart');
$cart = $data['cart'];
?>
<div class="widget widget-cart">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="cart-icon"><span class="glyphicon glyphicon-shopping-cart"></span></div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<a href="<?php print $link; ?>" class="item-count-link">
					<span class="items-count"><?php print $cart->GetTotalItems(); ?></span>
					<span class="items-text"><?php _e('Items', 'emono'); ?></span>
				</a>
			</div>
		</div>
		<?php /*
		<div class="row">
			<div class="col-md-12">
				<a href="<?php print $link; ?>" class="btn btn-view-cart">
					<?php _e('View cart', 'emono'); ?>
				</a>
			</div>
		</div>
		*/?>
	</div>
</div><!-- end class="widget cart-widget" -->
