<?php 
$products = isset($data['products']) ? $data['products'] : array()
?>
<div class="latest-products">
	<div class="container-fluid">
		<h2><?php _e('Latest Products', 'emono'); ?></h2>
		<?php if( is_array($products) && count($products) ): ?>
		<div class="row">
			<?php foreach($products as $p): ?>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="product">
					<a href="<?php print $p->link; ?>">
						<span class="image">
							<img src="<?php print $p->getFeaturedImage('330x330')->GetUrl(); ?>" alt="<?php print $p->product_name; ?>" />
						</span>
						<span class="info">
							<span class="name"><?php print $p->product_name; ?></span>
							<span class="code"><?php print $p->product_code; ?></span>
						</span>
					</a>
				</div><!-- end class="product" -->
			</div>
			<?php endforeach; ?>
		</div>
		<p class="text-center">
			<a href="<?php print b_route('index.php?mod=emono'); ?>" class="btn btn-view-more">
				<?php _e('View more products', 'emono'); ?>
			</a>
		</p>
		<?php else: ?>
		<h3><?php _e('There are no products found', 'emono'); ?></h3>
		<?php endif; ?>
	</div>
</div><!-- end class="latest-products" -->
