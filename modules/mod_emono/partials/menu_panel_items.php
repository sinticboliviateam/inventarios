<?php
$cats = SB_Warehouse::getCategories();
?>
<div class="panel panel-default">
	<div class="panel-heading"><?php _e('Product Categories', 'menu'); ?></div>
	<div class="panel-body" style="min-height:50px;max-height:200px;overflow:auto;">
		<ul class="list-group">
			<?php foreach($cats as $c): ?>
			<li class="row list-group-item" 
				data-id="<?php print $c->category_id; ?>"
				data-title="<?php print $c->name; ?>"
				data-slug="<?php print $c->slug ?>"
				data-link="<?php print b_route('index.php?mod=emono&view=category&id='.$c->category_id.'&slug='.$c->slug, 'frontend'); ?>"
				data-type="product_category">
				<div class="col-md-10"><?php print $c->name; ?></div>
				<div class="col-md-2">
					<a href="javascript:;" class="btn btn-default btn-add-page"><?php _e('Add', 'menu'); ?></a>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
