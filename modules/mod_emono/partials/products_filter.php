<?php
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Route;

$raw = 'index.php?mod=emono';
if( $view = SB_Request::getString('view') )
	$raw .= '&view='.$view;
if( $id = SB_Request::getInt('id') )
	$raw .= '&id='.$id;
if( $slug = SB_Request::getString('slug') )
	$raw .= '&slug='.$slug;
$sortby = SB_Request::getString('sortby');

$url = SB_Route::_($raw);
?>
<div class="products_filter">
	<form action="<?php print $url; ?>" method="get">
		<input type="hidden" name="mod" value="emono" />
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<select name="sortby" class="form-control">
					<option value="newest" <?php print $sortby == 'newest' ? 'selected' : ''; ?>><?php _e('Newest', 'emono'); ?></option>
					<option value="oldest" <?php print $sortby == 'oldest' ? 'selected' : ''; ?>><?php _e('Oldest', 'emono'); ?></option>
					<option value="name_asc" <?php print $sortby == 'name_asc' ? 'selected' : ''; ?>><?php _e('A - Z', 'emono'); ?></option>
					<option value="name_desc" <?php print $sortby == 'name_desc' ? 'selected' : ''; ?>><?php _e('Z - A', 'emono'); ?></option>
				</select>
			</div>
		</div>
	</form>
</div>
