<div id="related-products">
	<?php if( isset($data['product']) && $related_ids = json_decode($data['product']->related) ): 
	$args = array('in_ids' => $related_ids);
	if( isset($data['limit']) )
		$args['items_per_page'] = $data['limit'];
	list($products,,,) = emono_query_products($args);
	//print_r($products);
	?>
	<?php if( isset($data['title']) && $data['title'] ): ?>
	<h2><?php print $data['title']; ?></h2>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row">
			<?php foreach($products as $p): 
			$link = $p->link;
			?>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<div class="r-product">
					<a href="<?php print $link; ?>">
						<span class="image">
							<img src="<?php print $p->getFeaturedImage('330x330')->GetUrl(); ?>" alt="<?php print $p->product_name; ?>" />
						</span>
						<span class="info">
							<span class="name"><?php print $p->product_name; ?></span>
							<span class="code"><?php print $p->product_code; ?></span>
						</span>
					</a>
				</div><!-- end class="r-product" -->
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php else: ?>
	<b><?php _e('There are no related products found', 'emono'); ?></b>
	<?php endif; ?>
</div><!-- end id="related-products" -->