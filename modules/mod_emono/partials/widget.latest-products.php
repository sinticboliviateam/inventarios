<?php 
$products = isset($data['products']) ? $data['products'] : array();
?>
<div class="widget">
	<h2 class="title"><?php print $data['title']; ?></h2>
	<div class="body">
		<div class="latest-products">
			<?php if( $data['layout'] == 'default' ): ?>
			<div class="container-fluid">
				<?php if( is_array($products) && count($products) ): ?>
				<div class="row">
					<?php foreach($products as $p): ?>
					<div class="col-xs-12 col-sm-4 col-md-3">
						<div class="product">
							<a href="<?php print $p->link; ?>">
								<span class="image">
									<img src="<?php print $p->getFeaturedImage('330x330')->GetUrl(); ?>" alt="<?php print $p->product_name; ?>" />
								</span>
								<span class="info">
									<span class="name"><?php print $p->product_name; ?></span>
									<span class="code"><?php print $p->product_code; ?></span>
								</span>
							</a>
						</div><!-- end class="product" -->
					</div>
					<?php endforeach; ?>
				</div>
				<p class="text-center">
					<a href="<?php print b_route('index.php?mod=emono'); ?>" class="btn btn-view-more">
						<?php _e('View more products', 'emono'); ?>
					</a>
				</p>
				<?php else: ?>
				<h3><?php _e('There are no products found', 'emono'); ?></h3>
				<?php endif; ?>
			</div>
			<?php else: ?>
				<?php if( is_array($products) && count($products) ): ?>
				<div class="container-fluid">
					<?php foreach($products as $p): $link = $p->link; ?>
					<div class="row product">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<figure class="image">
								<a href="<?php print $link; ?>">
									<img src="<?php print $p->getFeaturedImage('330x330')->GetUrl(); ?>" alt="<?php print $p->product_name; ?>" />
								</a>
							</figure>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<h2 class="title"><a href="<?php print $link; ?>"><?php print $p->product_name ?></a></h2>
							<div class="price"><?php print $p->GetPrice(); ?></div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<?php else: ?>
				<h3><?php _e('There are no products found', 'emono'); ?></h3>
				<?php endif; ?>
			<?php endif; ?>
		</div><!-- end class="latest-products" -->
	</div>
</div>
