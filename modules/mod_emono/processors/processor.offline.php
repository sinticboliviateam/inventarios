<?php
class SB_MBCCOfflineProcessor extends SB_MBPaymentProcesor
{
	public function __construct()
	{
		$this->id 			= 'cc_offline';
		$this->name 		= __('Credit Card Offline payment processor', 'emono');
		$this->description 	= '';
		$this->version		= '1.0.0';
		$this->apiVersion	= '';
		
	}
	public function ShowSettings($_ops)
	{
		//print_r($ops);
		$op_prefix = "ops[payments][$this->id]";
		$ops = $_ops->payments->{$this->id};
		?>
		<div class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-3"><?php _e('Working mode'); ?></label>
				<div class="col-sm-9">
					<div >
						<label>
							<input type="radio" name="<?php print $op_prefix ?>[data]" value="send_store"
								<?php print @$ops->data == 'send_store' ? 'checked' : ''; ?> />
							<?php _e('Send card information by email and store in database.'); ?>
						</label>
					</div>
					<div >
						<label>
							<input type="radio" name="<?php print $op_prefix ?>[data]" value="store"
								<?php print @$ops->data == 'store' ? 'checked' : ''; ?> />
							<?php _e('Store all card information in database.'); ?>
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"><?php _e('Order Status'); ?></label>
				<div class="col-sm-9">
					<select name="<?php print $op_prefix ?>[status]" class="form-control">
						<?php foreach(mb_get_order_statuses() as $key => $label): ?>
						<option value="<?php print $key; ?>"
							<?php print @$ops->status == $key ? 'selected' : ''; ?>>
							<?php print $label; ?>
						</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"><?php _e('Send email copy to'); ?></label>
				<div class="col-sm-9">
					<input type="email" name="<?php print $op_prefix ?>[cc_email]" 
						value="<?php print @$ops->cc_email ?>" class="form-control" />
				</div>
			</div>
		</div>
		<?php
	}
	public function SaveSettings(&$ops)
	{
		$ops['payments'][$this->id] = array_map('trim', $ops['payments'][$this->id]); 
	}
	public function Show()
	{
		$cards = array(
			'visa.gif'			=> 'Visa',
			'mastercard.gif'	=> 'MasterCard',
			'amex.gif'			=> 'Amex',
			'maestro.gif'		=> 'Maestro',
			'diners.gif'		=> 'DinersClub',
			'carteblanche.gif'	=> 'CarteBlanche',
			'discover.gif'		=> 'Discover',
			'isracard.gif'		=> 'Isracard'
		);
		?>
		<p>
			<?php _e('We accept', 'emono'); ?>
			<?php foreach($cards as $card => $label): ?>
			<span>
				<img src="<?php print MOD_EMONO_URL . '/images/cards/' . $card ?>" 
					alt="<?php print $label ?>" title="<?php print $label ?>" />
			</span>
			<?php endforeach; ?>
		</p>
		<div class="form-group required">
			<label><?php _e('Card holder name', 'emono'); ?></label>
			<input type="text" name="cardholder" value="" class="form-control" required />
		</div>
		<div class="form-group required">
			<label><?php _e('Card number', 'emono'); ?></label>
			<input type="text" name="cardnumber" value="" class="form-control" required />
		</div>
		<div class="form-group required">
			<label><?php _e('CVC (card security code)', 'emono'); ?></label>
			<input type="text" name="cvc" value="" class="form-control" required />
		</div>
		
			<div class="form-group required">
				<label><?php _e('Card holder name', 'emono'); ?></label>
				<div class="form-horizontal">
					<div class="col-sm-2">
						<input type="text" name="exp_month" value="" class="form-control" required />
					</div>
					<div class="col-sm-2">
						<input type="text" name="exp_year" value="" class="form-control" required />
					</div>
				</div>
				
			</div>
		
		
		<?php
	}
	public function Redirect()
	{
	
	}
}