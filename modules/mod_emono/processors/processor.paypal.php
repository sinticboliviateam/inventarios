<?php
class SB_MBPaypalProcessor extends SB_MBPaymentProcesor
{
	public function __construct()
	{
		$this->id 	= 'paypal';
		$this->name = __('Paypal payment processor', 'emono');
		$this->description = '';
		$this->version		= '1.0.0';
		$this->apiVersion	= '';
	}
	public function ShowSettings($ops)
	{
		$op_prefix = "ops[payments][{$this->id}]";
		?>
		<div class="form-group">
			<label><?php _e('Business Email', 'emono'); ?></label>
			<input type="email" name="<?php print $op_prefix ?>[business_email]" value="<?php print @$ops->business_email ?>"
				class="form-control" />
		</div>
		<?php
	}
	public function SaveSettings(&$ops)
	{
		$ops['payments'][$this->id] = array_map('trim', $ops['payments'][$this->id]); 
	}
	public function Show()
	{
		?>
		
		<?php
	}
	public function Redirect()
	{
		$cart = SB_MBCart();
		$cart->Start();
		?>
		<form id="form-paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input name="cmd" value="_xclick" type="hidden">
			<input name="business" value="<?php print $this->settings->business_email; ?>" type="hidden" />
			<input name="currency_code" value="USD" type="hidden" />
			<?php foreach($cart->GetItems() as $i => $item): ?>
			<input name="item_number_<?php print $i; ?>" value="<?php print $item->product_code; ?>" type="hidden" />
			<input name="quantity_<?php print $i; ?>" value="<?php print $item->quantity; ?>" type="hidden" />
			<input name="item_name_<?php print $i; ?>" value="<?php print $item->product_name; ?>" type="text" />
			<input name="amount_<?php print $i; ?>" value="<?php print $item->price; ?>" type="amount" />
			<?php endforeach; ?>
		</form>
		<script>document.getElementById('form-paypal').submit();</script>
		<?php
	}
}