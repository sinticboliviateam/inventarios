alter table mb_products add column slug varchar(128) after product_description; 
alter table mb_categories add column slug varchar(128) after description;
