<div class="wrap">
	<h2>
		<?php _e('Ecommerce Settings', 'emono'); ?>
		<a href="javascript:;" onclick="jQuery('#form-settings').submit();" class="btn btn-success pull-right">
			<?php _e('Save', 'emono'); ?>
		</a>
	</h2>
	<ul class="nav nav-tabs">
		<li class="active"><a href="#general" class="" data-toggle="tab"><?php _e('General', 'emono'); ?></a></li>
		<li><a href="#design" class="" data-toggle="tab"><?php _e('Design', 'emono'); ?></a></li>
		<li ><a href="#payments" class="" data-toggle="tab"><?php _e('Payments', 'emono'); ?></a></li>
		<?php SB_Module::do_action('emono_settings_tabs', $ops); ?>
	</ul>
	<form id="form-settings" action="" method="post" class="tab-content">
		<input type="hidden" name="mod" value="emono" />
		<input type="hidden" name="task" value="save_settings" />
		<div id="general" class="tab-pane active">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label>
								<?php _e('Infinite Scroll', 'emono'); ?>
								<input type="checkbox" name="ops[infinite_scroll]" value="1" 
									<?php print (int)@$ops->infinite_scroll == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label>
								<?php _e('Show Price', 'emono'); ?>
								<input type="checkbox" name="ops[show_price]" value="1" 
									<?php print (int)@$ops->show_price == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label>
								<?php _e('Show Comments', 'emono'); ?>
								<input type="checkbox" name="ops[show_comments]" value="1" 
									<?php print (int)@$ops->show_comments == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label>
								<?php _e('Allow Checkout', 'emono'); ?>
								<input type="checkbox" name="ops[allow_checkout]" value="1" 
									<?php print (int)@$ops->allow_checkout == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label>
								<?php _e('Enable Orders', 'emono'); ?>
								<input type="checkbox" name="ops[enable_orders]" value="1" 
									<?php print (int)@$ops->enable_orders == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label>
								<?php _e('Enable Quotes', 'emono'); ?>
								<input type="checkbox" name="ops[enable_quotes]" value="1" 
									<?php print (int)@$ops->enable_quotes == 1 ? 'checked' : '' ?> />
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-4">
								<?php _e('Button Checkout Text', 'emono'); ?>
							</label>
							<div class="col-sm-8">
								<input type="text" name="ops[btn_checkout_text]" 
									value="<?php print @$ops->btn_checkout_text ?>" 
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
					</div>
				</div>
			</div>
		</div><!-- end id="general" -->
		<div id="design" class="tab-pane">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label><?php _e('Layout', 'emono'); ?></label>
							<select name="ops[layout]" class="form-control">
								<option value="list" <?php print @$ops->layout == 'list' ? 'selected' : ''; ?>>
									<?php _e('List', 'emono'); ?></option>
								<option value="grid" <?php print @$ops->layout == 'grid' ? 'selected' : ''; ?>>
									<?php _e('Grid', 'emono'); ?>
								</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end id="design" -->
		<div id="payments" class="tab-pane">
			<div id="payments-group" class="panel-group">
				<?php foreach($processors as $p): $id = 'panel_' . $p->id; ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a href="#<?php print $id; ?>" data-toggle="collapse" data-parent="#payments-group">
								<?php print $p->name; ?>
							</a>
						</h4>
					</div>
					<div id="<?php print $id; ?>" class="panel-collapse collapse ">
						<div class="panel-body">
							<p>
								<?php print $p->description; ?>
							</p>
							<?php $p->ShowSettings($ops); ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div><!-- end class="panel-group" -->
		</div><!-- end id="payments" -->
	</form>
</div>