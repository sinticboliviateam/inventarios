<?php
$items = $cart->GetItems();
?>
<div id="emono-cart-container">
	<h1><?php _e('Your Cart', 'emono'); ?></h1>
	<?php if( count($items) ): ?>
	<table class="table table-cart">
	<thead>
	<tr>
		<th class="col-image"></th>
		<th class="col-product"><?php _e('Product', 'emono'); ?></th>
		<th class="col-qty"><?php _e('Quantity', 'emono'); ?></th>
		<th class="col-price"><?php _e('Price', 'emono'); ?></th>
		<th class="col-total"><?php _e('Total', 'emono'); ?></th>
		<th class="col-actions"></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($items as $item): ?>
	<tr>
		<td class="col-image">
			<a href="<?php print $item->product->link; ?>">
				<img src="<?php print $item->product->getFeaturedImage('55x55')->GetUrl(); ?>" alt="<?php print $item->name; ?>" />
			</a>
		</td>
		<td class="col-product">
			<?php print $item->name; ?>
		</td>
		<td class="col-qty">
			<form action="" method="post">
				<input type="hidden" name="mod" value="emono" />
				<input type="hidden" name="task" value="add_to_cart" />
				<input type="hidden" name="id" value="<?php print $item->id; ?>" />
				<input type="number" name="qty" value="<?php print $item->quantity; ?>" class="form-control" />
				<input type="hidden" name="increase" value="0" />
			</form>
		</td>
		<td class="col-price"><?php print sb_number_format($item->price); ?></td>
		<td class="col-total"><?php print sb_number_format($item->price * $item->quantity); ?></td>
		<td class="col-actions">
			<a href="<?php print SB_Route::_('index.php?mod=emono&task=delete&id='.$item->id); ?>" 
				title="<?php _e('Remove', 'emono'); ?>" class="btn btn-default btn-xs">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="5">
			<div class="text-right"><?php _e('Total'); ?></div>
		</td>
		<td>
			<div class="cart-totals text-right"><?php print sb_number_format($cart->GetTotals()); ?></div>
		</td>
	</tr>
	</tfoot>
	</table>
	<div class="cart-buttons">
		<?php if( @$this->ops->allow_checkout ): ?>
		<a href="<?php print SB_Route::_('index.php?mod=emono&view=checkout'); ?>" class="btn btn-checkout">
			<?php print @$this->ops->btn_checkout_text ? $this->ops->btn_checkout_text : _e('Checkout', 'emono'); ?>
		</a>
		<?php endif; ?>
		<?php SB_Module::do_action('emono_cart_buttons', $this, $cart); ?>
	</div>
	<?php else: ?>
	<div class="alert alert-info"><b><?php _e('Your shopping cart is empty', 'emono'); ?></b></div>
	<p class="text-center">
		<a href="<?php print SB_Route::_('index.php?mod=emono'); ?>"><?php _e('View products', 'emono'); ?></a>
	</p>
	<?php endif; ?>
</div>