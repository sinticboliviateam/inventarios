<div id="product-category-container">
	<h1 id="page-title"><?php print $category->name; ?></h1>
	<?php lt_include_partial('emono', 'products_filter.php', array()); ?>
	<?php include $layout; ?>
	<?php /*
	<div id="product_list" class="row">
	<?php foreach($products as $p): ?>
		<?php 
		$slug = (empty($p->slug) ? sb_build_slug($p->product_name) : $p->slug);
		$link = SB_Route::_('index.php?mod=emono&view=product&id='.$p->product_id.'&slug='. $slug);
		?>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="product">
				<div class="image">
					<a href="<?php print $link; ?>">
						<img src="<?php print $p->getFeaturedImage('330x330')->GetUrl(); ?>" 
							alt="<?php print $p->product_name; ?>" title="<?php print $p->product_name; ?>" />
					</a>
				</div>
				<div class="name"><?php print $p->product_name; ?></div>
				<div class="code"><a href="<?php print $link ?>"><?php print $p->product_code; ?></a></div>
				<?php if( @$this->ops->show_price): ?>
				<div class="price"><?php print $p->price; ?></div>			
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	*/>
</div><!-- end id="product-category-container" -->
<?php lt_pagination(SB_Route::_('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>