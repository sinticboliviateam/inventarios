<?php
$user = sb_get_current_user();
$items = $cart->GetItems();
?>
<h1><?php _e('Checkout', 'emono'); ?></h1>
<?php if( count($items) > 0 ): ?>
<form action="" method="post">
	<input type="hidden" name="mod" value="emono" />
	<!-- <input type="hidden" name="task" value="place_order" /> -->
	<h2><?php _e('Your order', 'emono'); ?></h2>
	<table class="table">
	<thead>
	<tr>
		<th><?php _e('Product', 'emono'); ?></th>
		<th><?php _e('Total', 'emono'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($items as $item): ?>
	<tr>
		<td>
			<?php printf("%s x %d", $item->name, $item->quantity); ?>
		</td>
		<td>
			<?php print number_format($item->price * $item->quantity, 2); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<th><?php print _e('Subtotal', 'emono'); ?></th>
		<td><?php print $cart->GetSubtotal(); ?></td>
	</tr>
	<tr>
		<th><?php print _e('Shipping', 'emono'); ?></th>
		<td><?php print '0.00'; ?></td>
	</tr>
	<tr>
		<th><?php print _e('Total', 'emono'); ?></th>
		<td><?php print $cart->GetTotals(); ?></td>
	</tr>
	</tbody>
	</table>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div id="billing-details">
					<h2><?php _e('Billing Details', 'emono'); ?></h2>
					<div class="form-group">
						<label><?php _e('Email Address', 'emono'); ?></label>
						<input type="text" name="email" value="<?php $user ? $user->email : ''; ?>" class="form-control" required />
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('First Name', 'emono'); ?></label>
								<input type="text" name="first_name" value="<?php $user ? $user->first_name : ''; ?>" class="form-control" required />
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('Last Name', 'emono'); ?></label>
								<input type="text" name="last_name" value="<?php $user ? $user->last_name : ''; ?>" class="form-control" required />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label><?php _e('Company Name', 'emono'); ?></label>
						<input type="text" name="company" value="<?php $user ? $user->_company : ''; ?>" class="form-control" />
					</div>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<div class="form-group">
								<label><?php _e('Phone', 'emono'); ?></label>
								<input type="text" name="phone" value="<?php $user ? $user->_phone : ''; ?>" class="form-control" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label><?php _e('Country', 'emono'); ?></label>
						<select name="country_code" class="form-control" required>
							<?php foreach($countries as $code => $country): ?>
							<option value="<?php print $code; ?>" <?php $user && $user->_country == $code ? 'selected' : ''; ?>><?php print $country; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label><?php _e('Address', 'emono'); ?></label>
						<input type="text" name="address" value="<?php $user ? $user->_address : ''; ?>" class="form-control" />
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('City', 'emono'); ?></label>
								<input type="text" name="city" value="<?php $user ? $user->_city : ''; ?>" class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('Postcode', 'emono'); ?></label>
								<input type="text" name="postcode" value="<?php $user ? $user->_zip : ''; ?>" class="form-control" />
							</div>
						</div>
					</div>
				</div><!-- end id="billing-defailt" -->
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<?php if( @$this->ops->enable_orders ): ?>
				<h2><?php _e('Shipping Details', 'emono'); ?></h2>
				<div class="form-group">
					<label>
						<input type="checkbox" name="same_billing_data" value="1" checked />
						<?php _e('Same as billing details', 'emono'); ?>
					</label>
				</div>
				<div id="shipping-details">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('First Name', 'emono'); ?></label>
								<input type="text" name="shipping_first_name" value="" class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('Last Name', 'emono'); ?></label>
								<input type="text" name="shipping_last_name" value="" class="form-control" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label><?php _e('Company Name', 'emono'); ?></label>
						<input type="text" name="shipping_company" value="" class="form-control" />
					</div>
					<div class="form-group">
						<label><?php _e('Country', 'emono'); ?></label>
						<select name="country_code" class="form-control">
							<?php foreach($countries as $code => $country): ?>
							<option value="<?php print $code; ?>"><?php print $country; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label><?php _e('Address', 'emono'); ?></label>
						<input type="text" name="address" value="" class="form-control" />
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('City', 'emono'); ?></label>
								<input type="text" name="shipping_city" value="<?php $user ? $user->_city : ''; ?>" class="form-control" />
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label><?php _e('Postcode', 'emono'); ?></label>
								<input type="text" name="shipping_postcode" value="<?php $user ? $user->_zip : ''; ?>" class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="clearfix"></div>
			
			<div class="form-group">
				<label><?php _e('Order Notes', 'emono'); ?></label>
				<textarea name="notes" class="form-control"></textarea>
			</div>
		</div>
	</div>
	<div>&nbsp;</div>
	<?php if( @$this->ops->enable_orders ): ?>
	<div id="payment-processors">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<?php if( isset($processors) && is_array($processors)): foreach($processors as $p): ?>
				<div>
					<label>
						<input type="radio" name="processor" value="<?php print $p->id; ?>" required 
							class="pp" />
						<?php print $p->name; ?>
					</label>
				</div>
				<?php endforeach; endif; ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div id="payment-processor-form"></div>
			</div>
		</div>
	</div><!-- end id="payment-processors" -->
	<?php endif; ?>
	<div class="form-group">
		<?php if( @$this->ops->enable_orders ): ?>
		<button type="submit" class="btn btn-place-order" id="btn-place-order" name="task" value="place_order">
			<?php _e('Place Order', 'emono'); ?>
		</button>
		<?php endif; ?>
		<?php if( @$this->ops->enable_quotes ): ?>
		<button type="submit" class="btn btn-get-quote" id="btn-get-quote" name="task" value="get_quote">
			<?php _e('Get Quote', 'emono'); ?>
		</button>
		<?php endif; ?>
	</div>
</form>
<?php else: ?>
<div class="alert alert-warning"><b><?php _e('You don\'t have items into cart', 'emono'); ?></b></div>
<p>
	<a href="<?php print SB_Route::_('index.php?mod=emono'); ?>" class="btn btn-default">
		<?php _e('View products', 'emono'); ?>
	</a>
</p>
<?php endif; ?>