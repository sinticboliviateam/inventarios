<div id="product-list" class="layout-grid">
	<div class="container-fluid">
		<div class="row">
			<?php foreach($products as $p): $link = $p->link; ?>
			<div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
				<div class="product">
					<figure class="image">
						<a href="<?php print $link; ?>" title="<?php print $p->product_name; ?>" class="product-img-link">
							<img alt="<?php print $p->product_name; ?>" src="<?php print $p->getFeaturedImage()->GetUrl(); ?>">
						</a>
					</figure>
					<h3 class="title">
						<a href="<?php print $link; ?>" title="<?php print $p->product_name; ?>" class="product-link">
							<?php print $p->product_name; ?>
						</a>
					</h3>
					<p class="product-excerpt"><?php print $p->excerpt; ?></p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="product-flags">
									<?php if( $p->product_quantity > 0 ): ?>
									<span class="flag flag-availability">
										<span class="label label-success"><?php _e('Available', 'emono'); ?></span>
									</span> 
									<?php else: ?>
									<span class="flag flag-availability">
										<span class="label label-danger"><?php _e('Out of stock', 'emono'); ?></span>
									</span> 
									<?php endif; ?>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<span class="price">$<?php print $p->price; ?></span> 
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a href="<?php print SB_Route::_('index.php?mod=emono&task=add_to_cart&id='.$p->product_id); ?>" 
									class="btn btn-add2cart">
									<?php _e('Add to cart', 'emono'); ?>
								</a>
								<a href="<?php print $link; ?>" class="btn btn-view-product">
									<?php _e('View', 'emono'); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	
</div>