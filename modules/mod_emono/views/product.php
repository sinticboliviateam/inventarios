<?php
?>
<div id="product-container">
	<div id="product-images" class="col-md-6">
		<figure>
			<img src="<?php print $product->getFeaturedImage('full')->GetUrl(); ?>" 
				alt="<?php print $product->product_name; ?>" />
		</figure>
	</div><!-- end id="product-images" -->
	<div id="product-data" class="col-md-6">
		<h2 id="product-title"><?php print $product->product_name; ?></h2>
		<div id="product-price">
			<span><?php _e('Price:', 'emono'); ?></span><?php print $product->price; ?>
		</div>
		<div id="product-short-description">
			<?php print $product->product_description; ?>
		</div>
		<form id="product-form" action="" method="post">
			<input type="hidden" name="mod" value="emono" />
			<input type="hidden" name="task" value="add_to_cart" />
			<div class="form-inline">
				<div class="form-group">
					<input type="number" min="1" name="qty" value="1" class="form-control" />
					<button type="submit" class="btn btn-default btn-add-to-cart"><?php _e('Add to cart', 'emono'); ?></button>
				</div>
			</div>
		</form>
		<div id="product-code-sku">
			<?php _e('Code/SKU:', 'emono'); ?>
			<span id="the-code"><?php print $product->product_code; ?></span>
		</div>
		<div id="product-categories">
			<b><?php _e('Categories:', 'emono'); ?></b><br/>
			<?php foreach($product->GetCategories() as $cat): ?>
			<ul class="list">
				<li>
					<a href="<?php print SB_Route::_('index.php?mod=emono&view=category&id='.$cat->category_id.'&slug='.$cat->slug); ?>">
						<?php print $cat->name; ?>
					</a>
				</li>
			</ul>
			<?php endforeach; ?>
		</div>
	</div><!-- end id="product-date" -->
	<div class="clearfix">&nbsp;</div>
	<div id="tabs-container" class="clearfix">
		<ul id="product-tabs" class="nav nav-tabs">
			<li class="active"><a href="#description"><?php _e('Description', 'emono'); ?></a></li>
			<?php if( @$this->ops->show_comments ): ?>
			<li><a href="#comments"><?php _e('Comments', 'emono'); ?></a></li>
			<?php endif; ?>
			<li><a href="#specs"><?php _e('Specs', 'emono'); ?></a></li>
		</ul>
		<div class="tab-content">
			<div id="description" class="tab-pane active">
				<?php print $product->product_description; ?>
			</div><!-- end id="description" -->
			<?php if( @$this->ops->show_comments ): ?>
			<div id="comments" class="tab-pane">
			</div><!-- end id="description" -->
			<?php endif; ?>
			<div id="specs" class="tab-pane">
			</div><!-- end id="description" -->
		</div><!-- end class="tab-content" -->
	</div>
	<script>
	jQuery(function()
	{
		jQuery('#product-tabs a').click(function (e) 
		{
			  e.preventDefault();
			  jQuery(this).tab('show');
		});
	});
	</script>
</div><!-- end id="product-cotainer" -->