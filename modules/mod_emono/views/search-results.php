<?php
$show_title = true;
include 'default.php';
return;
?>
<h1><?php print $title; ?></h1>
<ul id="product_list" class="bordercolor list search-results">
	<?php foreach($products as $p): $link = $p->link; ?>
	<li class="ajax_block_product bordercolor">
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2">
				<a title="<?php print $p->product_name; ?>" class="product_img_link" href="#">
					<img alt="<?php print $p->product_name; ?>" src="<?php print $p->getFeaturedImage('150x150')->GetUrl(); ?>">
				</a>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-7">
				<div class="center_block">
					<div class="product_flags">
						<span class="availability bordercolor"><?php _e('Available', 'emono'); ?></span> 
					</div>
					<h3>
						<a title="<?php print $p->product_name; ?>" href="<?php print $link; ?>" 
							class="product_link"><?php print $p->product_name; ?>
						</a>
					</h3>
					<p class="product_desc">
						<a title="" href="<?php print $link; ?>" class="product_descr">
						<?php print $p->excerpt; ?>
						</a>
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-3">
				<div class="right_block bordercolor">
					<span class="price">$<?php print $p->price; ?></span> 
					<?php /* ?>
					<p class="compare checkbox">
						<input type="checkbox" value="comparator_item_31" id="comparator_item_list31" class="comparator"> 
						<label for="comparator_item_list31">Comparar</label>
					</p>
					*/?>
					<a title="<?php _e('Add to cart', 'emono'); ?>" href="<?php print SB_Route::_('index.php?mod=emono&task=add_to_cart&id='.$p->product_id); ?>" 
						rel="ajax_id_product_31" class="exclusive ajax_add_to_cart_button">
						<?php _e('Add to cart', 'emono'); ?>
					</a>
					<br>
					<a title="<?php _e('View', 'emono'); ?>" href="<?php print SB_Route::_('index.php?mod=emono&view=product&id='.$p->product_id); ?>" 
						class="button">
						<?php _e('View', 'emono'); ?>
					</a>
				</div>
			</div>
		</div>
	</li>
	<?php endforeach; ?>
</ul><!-- end id="products-container" -->