<h1><?php print $title; ?></h1>
<p>
	<?php $text; ?>
</p>
<p>
	<?php foreach($buttons as $btn): ?>
	<a href="<?php print $btn['link']; ?>" id="<?php print $btn['id']; ?>" class="btn <?php print $btn['class']; ?>">
		<?php print $btn['text']; ?>
	</a>
	<?php endforeach; ?>
</p>
<p class="text-center">
	<a href="<?php print SB_Route::_('index.php?mod=emono'); ?>" class="btn btn-continue-shopping">
		<span class="glyphicon glyphicon-cart"></span> <?php _e('Continue Shopping', 'emono'); ?>
	</a>
</p>