<?php
use SinticBolivia\SBFramework\Classes\SB_Widget;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SB_MBWidgetCart extends SB_Widget
{
	public function __construct()
	{
		parent::__construct(__('Shopping Cart', 'emono'));
	}
	public function Render($args = array())
	{
		$def_args = array(
			'layout'	=> 'default'
		);
		$args = array_merge($def_args, $args);
		$cart = LT_ModEMono::GetCart();
		$items = $cart->GetItems();
		$data = array(
			'args'	=> $args,
			'cart'	=> $cart,
			'items'	=> $items
		);
		lt_include_partial('emono', 'cart-'.$args['layout'].'.php', $data);
		
	}
}
sb_register_widget('SB_MBWidgetCart');