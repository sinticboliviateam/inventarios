<?php
use SinticBolivia\SBFramework\Classes\SB_Widget;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SB_MBWidgetCategories extends SB_Widget
{
	public $whole_items;
	
	public function __construct()
	{
		parent::__construct(__('Categories', 'emono'));
	}
	public function Render($args = array())
	{
		$def_args = array(
			'title'		=> $this->title,
			'in'		=> null,
			'exclude'	=> null,
			'orderby'	=> 'name',
			'order'		=> 'asc',
			'parent'	=> 0
		);
		$args = array_merge($def_args, $args);
		$query = "SELECT * FROM mb_categories 
					WHERE parent = {$args['parent']} ";
		if( $args['in'] )
		{
			$ids = is_array($args['in']) ? implode(',', $args['in']) : $args['in'];
			if( !empty($ids) )
				$query .= "AND category_id IN($ids) ";
		}
		$query .= "ORDER BY {$args['orderby']} {$args['order']}";
		$parents = $this->dbh->FetchResults($query);
		//##get whole child categories
		$iquery = "SELECT * FROM mb_categories WHERE parent <> 0 ";
		if( $args['in'] )
		{
			$ids = is_array($args['in']) ? implode(',', $args['in']) : $args['in'];
			if( !empty($ids) )
				$iquery .= "AND category_id IN($ids) ";
		}
		$iquery .= "ORDER BY {$args['orderby']} {$args['order']}";
		$this->whole_items = $this->dbh->FetchResults($iquery);
		?>
		<div class="widget">
			<h2 class="title"><?php print $args['title']; ?></h2>
			<div class="body">
				<ul class="product-categories menu">
					<?php $this->RenderItems($parents); ?>
				</ul>
			</div>
		</div>
		<?php
	}
	protected function GetChilds($id)
	{
		$childs = array();
		foreach($this->whole_items as $item)
		{
			if( $item->parent == $id )
			{
				$childs[] = $item;
			}
		}
		
		return $childs;
	}
	protected function RenderItems($items)
	{
		
		?>
		<?php foreach($items as $cat): ?>
		<?php 
		$link = SB_Route::_('index.php?mod=emono&view=category&id='.$cat->category_id.'&slug='.sb_build_slug($cat->name));
		?>
		<li class="category">
			<a href="<?php print $link; ?>"><?php print $cat->name; ?></a>
			<?php $childs = $this->GetChilds($cat->category_id); if( count($childs) ): ?>
			<ul class="childs submenu">
				<?php $this->RenderItems($childs); ?>
			</ul>
			<?php endif; ?>
		</li>
		<?php endforeach; ?>
		<?php
	}
}
sb_register_widget('SB_MBWidgetCategories');