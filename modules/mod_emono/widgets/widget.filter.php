<?php 
use SinticBolivia\SBFramework\Classes\SB_Widget;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Request;

class SB_MBWidgetFilters extends SB_Widget
{
	public function __construct()
	{
		parent::__construct(__('Products Filter', 'emono'));
	}
	public function Render($args = array())
	{
		$def_args = array(
			'title'	=> __('Price Filter', 'emono')
		);
		$args 		= array_merge($def_args, $args);
		$view 		= SB_Request::getString('view');
		$keyword	= SB_Request::getString('keyword');
		$sortby		= SB_Request::getString('sortby');
		$pf			= (float)SB_Request::getFloat('pf');
		$pt			= (float)SB_Request::getFloat('pt');
		$max		= 4000;
		$link 		= defined('LT_REWRITE') && LT_REWRITE ? SB_Route::_('index.php?mod=emono&view=search') : '';
		//##get max price
		$max 		= $this->dbh->GetVar("SELECT MAX(product_price) FROM mb_products LIMIT 1");
		?>
		<div class="widget widget-products-filter">
			<h2 class="title"><?php print $args['title']; ?></h2>
			<div class="body">
				<form action="<?php print $link; ?>" method="get">
					<?php if( !defined('LT_REWRITE') || !LT_REWRITE ): ?>
					<input type="hidden" name="mod" value="emono" />
					<input type="hidden" name="view" value="search" />
					<?php endif; ?>
					<input type="hidden" name="keyword" value="<?php print $keyword; ?>" />
					<input type="hidden" name="sortby" value="<?php print $sortby; ?>" />
					<div class="price-filter" data-pf="<?php print ($pf > 0) ? $pf : 0; ?>" 
						data-pt="<?php print ($pt > 0) ? $pt : $max; ?>"
						data-max="<?php print $max; ?>">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<input type="number" name="pf" value="<?php print ($pf > 0) ? $pf : 0; ?>" class="price-range form-control" />
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<input type="number" name="pt" value="<?php print ($pt > 0) ? $pt : $max; ?>" class="price-range form-control" />
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php
	}
}
sb_register_widget('SB_MBWidgetFilters');