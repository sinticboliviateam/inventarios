<?php
use SinticBolivia\SBFramework\Classes\SB_Widget;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SB_MBWidgetLatestProducts extends SB_Widget
{
	public function __construct()
	{
		parent::__construct(__('Latest Products', 'emono'));
	}
	public function Render($args = array())
	{
		$def_args = array(
			'layout'	=> 'default',
			'limit'		=> 12,
			'cat_id'	=> null,
			'title'		=> __('Latest Products', 'emono')
		);
		$args = array_merge($def_args, $args);
		list($products,,,) = emono_query_products(array('items_per_page' => $args['limit']));
		$args['products'] = $products;
		//lt_include_partial('emono', 'latest-products.php', $data);
		lt_include_partial('emono', 'widget.latest-products.php', $args);
		
	}
}
sb_register_widget('SB_MBWidgetLatestProducts');