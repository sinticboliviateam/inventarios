<?php
use SinticBolivia\SBFramework\Classes\SB_Widget;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SB_MBWidgetMostViewed extends SB_Widget
{
	public function __construct()
	{
		parent::__construct(__('Latest Products', 'emono'));
	}
	public function Render($args = array())
	{
		$def_args = array(
			'object'	=> 'products',
			'limit'		=> 5,
			'cat_id'	=> null,
			'title'		=> __('Most Viewed', 'emono')
		);
		$args = array_merge($def_args, $args);
		$query = "SELECT p.*, pm.meta_value as views 
					FROM mb_products p
					LEFT JOIN mb_product_meta pm ON pm.product_id = p.product_id
					WHERE 1 = 1
					AND pm.meta_key = '_views'
					ORDER BY pm.meta_value DESC
					LIMIT {$args['limit']}";
		
		$products = $this->dbh->FetchResults($query);
		?>
		<div class="widget widget-most-viewed widget-most-viewed-<?php print $args['object']; ?>">
			<h2 class="title"><?php print $args['title']; ?></h2>
			<div class="body">
				<div class="products">
					<?php foreach($products as $_p): $p = new SB_MBProduct();$p->SetDbData($_p);$link = $p->link; ?>
					<div class="product">
						<div class="container-fluid">
							<div class="col-xs-12 col-sm-6 col-md-5">
								<figure class="image">								
									<a href="<?php print $link; ?>">
										<img src="<?php print $p->getFeaturedImage('150x150x')->GetUrl(); ?>" 
												alt="<?php print $p->product_name; ?>" />
									</a>
								</figure>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-7">
								<div class="title">
									<a href="<?php print $link; ?>"><?php print $p->product_name; ?></a>
								</div>
								<div class="price"><?php print $p->GetPrice(); ?></div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php		
	}
}
sb_register_widget('SB_MBWidgetMostViewed');