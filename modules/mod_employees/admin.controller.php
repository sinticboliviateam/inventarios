<?php
class LT_AdminControllerEmployees  extends SB_Controller
{
    public function task_default()
    {
	    if(!sb_is_user_logged_in() )
	    {
	      die(SB_Text::_('You are not logged in, pleasee start session','employees'));
	    }
	    $user = sb_get_current_user();
	    /*
		if ( !$user->can ('manage-employees'))
	    {
	    	sb_redirect(SB_Route::_('index.php?mod=employees'),
	    			SB_Text::_('You dont have enough permissions view employees' , 'employees'), 'error');
	    }
		*/
		$page	= SB_Request::getInt('page');
		$limit	= 25;
		if( $page <= 0 )
			$page = 1;
			
		$this->dbh->Select('COUNT(*)')
			->From(array('mb_employees as e'));
		$total_rows = $this->dbh->GetVar(null);
		$pages = ceil($total_rows/$limit);
		$offset = $page == 1 ? 0 : ($page - 1) * $limit;
	    $this->dbh->Select('*')
			->From('mb_employees e')
			->OrderBy('creation_date', 'desc')
			->Limit($limit, $offset);
		$this->dbh->Query(null);
	    $employees = $this->dbh->FetchResults();
	    
	   	sb_set_view_var('title', __('Employees Management','employees'));
		sb_set_view_var('employees', $employees);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $page);
    }
	public function task_new()
  	{
	  	sb_include_module_helper('employees');
	  	$stores=LT_HelperEmployees::GetEmployeeStore();
	  	sb_set_view_var('stores', $stores);
	  	sb_set_view_var('title',SBText::_('Create New Employee', 'employees'));
  	}
  	public function task_delete()
  	{
	  	if (!sb_is_user_logged_in() )
	  	{
	  		die(SB_Text::_('You are not logged in, please start session', 'mb_c'));
	  	}
	  	$user = sb_get_current_user();
	  	if (!$user->can('delete_employee') )
	  	{
	  		SB_MessagesStack::AddMessage(SB_Text::_('You dont have enough permissions to delete a customer', 'mb_e'), 'error');
	  		sb_redirect(SB_Route::_('index.php?mod=employees'));
	  	}
	  	
	  	$employee_id = SB_Request::getInt('id');
	  	if( !$employee_id)
	  	{
	  		sb_redirect(SB_Route::_('index.php?mod=employees'), SB_Text::_('The employee identifier is invalid', 'mb_e'), 'error');
	  	}
	  	$dbh = SB_Factory::getDbh();
	  	$dbh->Delete('mb_employees', array('employee_id' => $employee_id));
	  	$dbh->Delete('mb_employee_meta', array('employee_id' =>$employee_id));
	  	$dbh->Delete('mb_contract', array('employee_id' => $employee_id));
	  	$dbh->Delete('mb_working', array('employee_id' =>$employee_id));
	  	SB_MessagesStack::AddMessage(SB_Text::_('The employee has been deleted' , 'mb_e'), 'error');
	  	sb_redirect(SB_Route::_('index.php?mod=employees'));
	  
  	}
  	public function task_edit()
  	{
	    $employee_id = SB_Request::getInt('id');
	    if(!$employee_id)
	    {  
	    	SB_MessagesStack::AddMessage(SB_Text::_('Identificador de empleado es invalido'),'error');
	    	sb_redirect(SB_Route::_('index.php?mod=employees'));
	    } 
	    sb_include_module_helper('employees');
	    $the_employee	= new SB_MBEmployee($employee_id);
	    //$employeeMeta	= LT_HelperEmployees::GetEmployeeMeta($employee_id);
	    $employee_con	= LT_HelperEmployees::GetEmployeeContract($employee_id);
	    $stores			= LT_HelperEmployees::GetEmployeeStore($employee_id);
	    sb_set_view('new');
	    sb_set_view_var('the_employee', $the_employee);
	    sb_set_view_var('stores', $stores);
	    //sb_set_view_var('the_employeeMeta', $employeeMeta);
	    sb_set_view_var('employee_id', $employee_id);
	    sb_set_view_var('employee_con', $employee_con);
	    sb_set_view_var('title', SB_Text::_('Edit Employee'));
  	}
  	public function task_save()
  	{
  		sb_include_module_helper('employees');
	  	$employee_id               =  SB_Request::getInt('employee_id');
	  	$first_name                =  SB_Request::getString('first_name');
	  	$last_name                 =  SB_Request::getString('last_name');
	  	$date_of_birth             =  SB_Request::getString('date_of_birth');
	  	$age                       =  SB_Request::getString('age');
	  	$DNI                       =  SB_Request::getString('DNI');
	  	$gender                    =  SB_Request::getString('gender');
	  	$city                      =  SB_Request::getString('city');
	  	$notes                     =  SB_Request::getString('notes');
	  	$phone                     =  SB_Request::getString('phone');
	  	$mobile                    =  SB_Request::getString('mobile');
	  	$status                    =  SB_Request::getString('status');
	  	$contract_id               =  SB_Request::getString('contract_id');
	  	$identity_document         =  SB_Request::getString('identity_document');
	  	$position                  =  SB_Request::getString('position');
	  	$salary                    =  SB_Request::getString('salary');
	  	$address_1 				   =  SB_Request::getString('address_1');
	  	$tota_hours_work		   =  SB_Request::getString('Total_hours_work');
	  	$contract_date             =  SB_Request::getString('contract_date');
	  	$contract_expiration_date  =  SB_Request::getString('contract_expiration_date');
	  	$store_id                  =  SB_Request::getString('store_id');
	  	$meta                      =  SB_Request::getVar('meta');
	  	
	  	if( empty($first_name) )
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese nombres del empleado', 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if (empty($last_name))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese apellidos del empleado','error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($age))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese al edad del empleado' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	
	  	if(empty($gender))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese el genero  del empleado' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	    if(empty($city))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese el dni del empleado' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($position))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese posicion del empleado','error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($salary))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese el salario del empleado' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($contract_date))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese la fecha de contrato' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($contract_expiration_date))
	  	{
	  		SB_MessagesStack::AddMessage('Ingrese la fecha de expiracion del contrato del empleado' , 'error');
			ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	  	if(empty($tota_hours_work))
	  	{
	  		SB_MessagesStack::AddMessage('Seleccione las horas de trabajo','error');
	  		ViewSecuruty($employee_id, $this);
	  		return false;
	  	}
	    $dbh =SB_Factory::getDbh();
	  	$date = date('Y-m-d H:i:s');
	  	$data_1 = array(
	  			'first_name'                =>  $first_name,
	  			'last_name'                 =>  $last_name,
	  			'store_id'					=> 	$store_id,
	  			'age'                       =>  $age,
			    'date_of_birth'             =>  sb_format_date($date_of_birth,'Y-m-d'),
	  			'identity_document'			=>  $DNI,
	  			'gender'                    =>  $gender,
	  			'total_hours_work'			=>	$tota_hours_work,
	  			'city'                      =>  $city,
	  			'phone'                     =>  $phone,
	  			'mobile'                    =>  $mobile,
	  			'status'                    =>  $status,
	  			'address_1'					=>  $address_1
	  	);
	  	$data_2 = array(
	  				'identity_document'           =>  $identity_document,
	  				'position'                    =>  $position,
	  				'salary'                      =>  $salary,
	  				'status'                      =>  $status  			
	  	);
	  	if ($employee_id)
	  	{
	  		$data_1['last_modification_date']=$date;
		  	$where = array(
		  			'employee_id' => $employee_id
		  	);
	  	    $dbh->Update('mb_employees', $data_1, $where);
	  		$data_2['last_modification_date'] = $date;
	  	    $dbh->Update('mb_contract', $data_2, $where);
	  	    //$dbh->Update('mb_working', $data, $where);
	  		foreach ($meta as $meta_key =>$meta_value )
	  		{
	  			if(is_array($meta_value) || is_object($meta_value) )
	              SB_Meta::updateMeta('mb_employee_meta', $meta_key, $meta_value, 'employee_id', $employee_id); 
	  			else 
	  				SB_Meta::updateMeta('mb_employee_meta', $meta_key, trim($meta_value), 'employee_id', $employee_id);
	  		}
	  		sb_set_view('new');
	  		$title = "El empleado ha sido actualizado";
	  		$link = SB_Route::_('index.php?mod=employees&view=edit&id='.$employee_id);
	  	}
	  	else 
	  	{
	  		$pin = SB_MBEmployee::GenerateNewPIN();
	  		$data_1['pin'] = $pin;
	  		$data_1['creation_date'] = $date; 
	  		$employee_id = $dbh->Insert('mb_employees', $data_1);
	  		foreach ($meta as $meta_key => $meta_value)
	  		{
	  			if (is_array($meta_value) || is_object($meta_value) )
	  			SB_Meta::addMeta('mb_employee_meta', $meta_key, $meta_value, 'employee_id', $employee_id);
	  			else 
	  				SB_Meta::addMeta('mb_employee_meta', $meta_key, trim($meta_value), 'employee_id', $employee_id);
	  		}
	  		$data=array(
	  				'store_id'                 =>  $store_id,
	  				'employee_id'              =>  $employee_id,
	  		);
	  		$data['creation_date'] = SB_Request::getString('creation_date') ?
	  		sb_format_datetime(SB_Request::getString('creation_date'), 'Y-m-d H:i:s') : $date;
	  		$dbh->Insert('mb_working', $data);
	  		
	  		$data_2['contract_expiration_date'] = $contract_expiration_date;
	  		$data_2['contract_date'] = $contract_date;
	  		$data_2['employee_id'] = $employee_id;
	  		$data_2['creation_date'] = SB_Request::getString('creation_date') ?
	  									sb_format_datetime(SB_Request::getString('creation_date'), 'Y-m-d H:i:s') : $date;
	  		$dbh->Insert('mb_contract', $data_2);
	  		$title = "El cliente ha sido creado";
	  		$link=SB_Route::_('index.php?mod=employees');
  		}
  		SB_Module::do_action('save_employee', $employee_id);
  		SB_MessagesStack::AddMessage($title , 'success');
  		sb_redirect($link);
  	}
  	
    public function task_view(){
    	$employee_id = SB_Request::getInt('id');
    	$dbh= SB_Factory::getDbh();
    	$query = "SELECT concat(c.first_name,' ',c.last_name) as 'name',c.* FROM mb_employees as c WHERE c.employee_id=$employee_id";
    	$dbh->Query($query);
    	$employee_new=$dbh->FetchRow();
    	$query = "SELECT * FROM mb_contract WHERE employee_id=$employee_id";
    	$dbh->Query($query);
    	$employee_old=$dbh->FetchRow();
    	sb_set_view_var('employee_new', $employee_new);
    	sb_set_view_var('employee_old', $employee_old);
    	$this->task_default();
    }
}