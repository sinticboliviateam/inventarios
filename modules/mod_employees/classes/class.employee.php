<?php
namespace SinticBolivia\MonoBusiness\Modules\Employees;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;

class SB_MBEmployee extends SB_ORMObject
{
	public function __construct($employee_id = null)
	{
		parent::__construct();
		if($employee_id)
			$this->GetDbData($employee_id);
	}
	public function GetDbData($id)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_employees WHERE employee_id = $id LIMIT 1";
		if( !$dbh->Query($query) )
		{
			return false;
		}
		$this->_dbData = $dbh->FetchRow();
		$this->GetDbMeta();
	}
	public function SetDbData($data)
	{
		$dbh = SB_Factory::getDbh();
		$this->_dbData = (object)$data;
		
		if( !isset($data->meta) )
		{
			//$this->GetDbMeta();
		}
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_employee_meta WHERE employee_id = $this->employee_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = $row->meta_value;
		}
	}
	public function SetDbMeta($meta)
	{
		$this->_meta = $meta;
	}
	public function CheckIn()
	{
		//##check if the employee already has the checkin
		$date = date('Y-m-d');
		$query = "SELECT id FROM mb_employee_time_card ".
					"WHERE employee_id = $this->employee_id " .
					"AND attendance_type = 'check_in' " .
					"AND DATE(attendance) = '$date'";
		
		if( $this->dbh->Query($query) )
			throw new Exception(__('The employee already has the checkin'));
		$this->dbh->Insert('mb_employee_time_card', array('employee_id' 		=> $this->employee_id,
															'attendance_type'	=> 'check_in', 
															'attendance' 		=> date('Y-m-d H:i:s'),
															'creation_date' 	=> date('Y-m-d H:i:s')
		));
		
		return true;
	}
	public function CheckOut()
	{
		//##check if the employee already has the checkin
		$date = date('Y-m-d');
		$query = "SELECT id FROM mb_employee_time_card ".
				"WHERE employee_id = $this->employee_id " .
				"AND attendance_type = 'check_out' " .
				"AND DATE(attendance) = '$date'";
		
		if( $this->dbh->Query($query) )
			throw new Exception(__('The employee already has the checkout'));
		
		$this->dbh->Insert('mb_employee_time_card', array('employee_id' 		=> $this->employee_id,
				'attendance_type'	=> 'check_out',
				'attendance' 		=> date('Y-m-d H:i:s'),
				'creation_date' 	=> date('Y-m-d H:i:s')
		));
		
		return true;
	}
	public function CheckLunch()
	{
		//##check if the employee already has the checkin
		$date = date('Y-m-d');
		$query = "SELECT id FROM mb_employee_time_card ".
				"WHERE employee_id = $this->employee_id " .
				"AND attendance_type = 'check_lunch' " .
				"AND DATE(attendance) = '$date'";
	
		if( $this->dbh->Query($query) )
			throw new Exception(__('The employee already has the lunch mark', 'employees'));
	
		$this->dbh->Insert('mb_employee_time_card', array('employee_id' 		=> $this->employee_id,
				'attendance_type'	=> 'check_lunch',
				'attendance' 		=> date('Y-m-d H:i:s'),
				'creation_date' 	=> date('Y-m-d H:i:s')
		));
	
		return true;
	}
	public function GetAttendance($from = null, $to = null)
	{
		/*
		$query = "SELECT ci.employee_id, ci.attendance as check_in, co.attendance as check_out, TIMEDIFF(co.attendance, ci.attendance) AS hours
					from mb_employee_time_card ci 
					left join mb_employee_time_card co on ci.employee_id = co.employee_id
					where 1 = 1
					and ci.employee_id = $this->employee_id
					and ci.attendance_type = 'check_in'
					and co.attendance_type = 'check_out'
					and DATE(ci.attendance) = DATE(co.attendance)";
		*/
		$check_out_query = "select co.attendance 
						from mb_employee_time_card co
						where co.employee_id = ci.employee_id
						and DATE(co.attendance) = DATE(ci.attendance)
						and co.attendance_type = 'check_out'";
		
		$query = "select ci.id, ci.employee_id, ci.attendance as check_in, ($check_out_query) as check_out, TIMEDIFF(($check_out_query), ci.attendance) AS hours
				FROM mb_employee_time_card ci 
				where 1 = 1
				and ci.attendance_type = 'check_in'
				AND ci.employee_id = $this->employee_id";
		return $this->dbh->FetchResults($query);
	}
	public static function GenerateNewPIN()
	{
		$dbh =  SB_Factory::getDbh();
		$aleatorio = mt_rand(1000, 9999);
		$query = "SELECT employee_id FROM mb_employees WHERE pin = $aleatorio";
		while( $dbh->Query($query) )
		{
			$aleatorio = mt_rand(1000,9999);
			$query = "SELECT employee_id FROM mb_employees WHERE pin = $aleatorio";
		}
		
		return $aleatorio;
	}
}