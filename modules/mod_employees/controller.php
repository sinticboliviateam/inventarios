<?php
class LT_ControllerEmployees extends SB_Controller
{
	public function task_attendance()
	{
		
	}
	public function task_check()
	{
		$pin 	= SB_Request::getInt('pin');
		$action = SB_Request::getString('action');
		if( empty($pin) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('Invalid employee PIN', 'employees')));
		}
		$actions = array(
				'check_in',
				'check_out',
				'check_lunch'
		);
		if( !in_array($action, $actions) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('The the action is not allowed', 'employees')));
		}
		
		$query = "SELECT * FROM mb_employees WHERE pin = $pin LIMIT 1";
		$row = $this->dbh->FetchRow($query);
		if( !$row )
		{
			sb_response_json(array('status' => 'error', 'error' => __('The PIN does not belongs to an employee', 'employees')));
		} 
		$emp = new SB_MBEmployee();
		$emp->SetDbData($row);
		try 
		{
			if( $action == 'check_in' )
			{
				$emp->CheckIn();
				sb_response_json(array('status' => 'ok', 'message' => sprintf(__('Welcome %s'), $emp->first_name)));
			}
			if( $action == 'check_out' )
			{
				$emp->CheckOut();
				sb_response_json(array('status' => 'ok', 'message' => sprintf(__('Good bye %s'), $emp->first_name)));
			}
			if( $action == 'check_lunch' )
			{
				$emp->CheckLunch();
				sb_response_json(array('status' => 'ok', 'message' => sprintf(__('Bon Appetite %s'), $emp->first_name)));
			}
		}
		catch(Exception $e)
		{
			sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
		}
		
		die();
	}
}