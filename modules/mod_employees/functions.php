<?php
/**
 * Function to page navigation
 *
 * @param string $order_by
 * @param Object $class  -> class module
 * @param array $columns
 * @param array $table
 * @param array $where is null
 * @return array[]
 */
function employees_page_navigation($order_by,$class,array $columns,array $table,array $where=NULL){
	$dbh = SB_Factory::getDbh();
	$page         =  SB_Request::getInt('page', 1 );
	$limit        =  SB_Request::getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
	$query        =  sprintf("SELECT {columns} FROM %s ",implode(',',$table));
	if($where){
		$query .= " WHERE ".implode(' AND ', $where);
	}

	$order_query  =  "ORDER BY $order_by ASC";

	$dbh->Query(str_replace('{columns}', 'COUNT(*) AS total_rows', $query));

	$total_rows   =  $dbh->FetchRow()->total_rows;
	$total_pages  =  ceil($total_rows / $limit);
	$offset       =  ($page <= 1) ? 0 : ($page - 1) * $limit;
	$limit_query  =  "LIMIT $offset, $limit";

	$query = str_replace('{columns}', implode(',', $columns), $query) . " $order_query $limit_query";
	$dbh->Query($query);
	$arreglo = array();
	foreach($dbh->FetchResults() as $row)
	{
		$a = new $class();
		$a->SetDbData($row);
		$arreglo[] = $a;
	}
	sb_set_view_var('total_pages', $total_pages);
	sb_set_view_var('current_page', $page);
	return $arreglo;
}
function ViewSecuruty($employee_id,$class){
	if(!$employee_id)
		$class->task_new();
	else
		$class->task_edit();
}