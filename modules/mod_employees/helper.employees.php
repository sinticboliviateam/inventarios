<?php
class LT_HelperEmployees
{
	public static function GetEmployee($employee_id)
	{
		return new SB_MBEmployee($employee_id);
	}
	public static function GetEmployeeMeta($employee_id){
		$dbh = SB_Factory::getDbh();
		$query = "SELECT meta_key,meta_value FROM mb_employee_meta WHERE employee_id=$employee_id";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
	
	public static function GetEmployeeContract($employee_id){
		$dbh = SB_Factory::getDbh();
		$query ="SELECT * FROM mb_contract WHERE employee_id=$employee_id";
		$dbh-> Query($query);
		return $dbh->FetchRow();
		
	}
	public static function GetEmployeeStore(){
		$dbh = SB_Factory::getDbh();
		$query ="SELECT * FROM mb_stores ORDER BY store_name ASC";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
	
	public static function ViewSecuruty($employee_id,$class){
		if(!$employee_id)
			$class->task_new();
		else
			$class->task_edit();
	}
	
}



