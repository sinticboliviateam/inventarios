<?php
namespace SinticBolivia\MonoBusiness\Modules\Employees;

use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Menu;

define('MOD_EMPLOYEES_DIR', MODULES_DIR .SB_DS . 'mod_employees');
define('MOD_EMPLOYEES_URL',MODULES_URL . '/mod_employees' );
define('MOD_EMPLOYEES_CLASSES_DIR', MOD_EMPLOYEES_DIR . SB_DS . 'classes');
require_once MOD_EMPLOYEES_DIR . SB_DS . 'functions.php';
require_once MOD_EMPLOYEES_CLASSES_DIR . SB_DS . 'class.employee.php';


//require_once MOD_CUSTOMERS_DIR . SB_DS . 'classes' . SB_DS . 'class.employee.php';

class SB_EmployeesHooks
{
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'employees',MOD_EMPLOYEES_DIR . SB_DS . 'locale');
		$this->AddActions();
	}
	public function AddActions()
	{
		SB_Module::add_action('init', array('SinticBolivia\MonoBusiness\Modules\Employees\SB_EmployeesHooks','action_init'));
		SB_Module::add_action('admin_menu',array('SinticBolivia\MonoBusiness\Modules\Employees\SB_EmployeesHooks','action_admin_menu'));
		//SB_Module::add_action('admin-dashboard', array('SB_EmployeesHooks','action-admin_dashboard'));
		if ( !defined('LT_ADMIN') )
		{
			SB_Module::add_action('rewrite_routes', array($this, 'action_rewrite_routes'));
			SB_Module::add_action('employee_menu', array('SB_EmployeesHooks','action_employee_menu'));
		}
	}
	public static function action_init()
	{
		SB_Language::loadLanguage(LANGUAGE, 'employees', MOD_EMPLOYEES_DIR . SB_DS . 'locale');
	}
    public static function action_admin_menu ()
	{
		SB_Menu::addMenuChild('menu-management',
			    '<span class="glyphicon glyphicon-user"></span> ' . __('Employees', 'employees'), 
				SB_Route::_('index.php?mod=employees'),
				'menu-employees', 
				'manage_employees');
	}
	public function action_rewrite_routes($routes)
	{
		$routes['/^\/employee\/attendance\/?$/'] = 'mod=employees&view=attendance';
		return $routes;
	}
}
new SB_EmployeesHooks();