<?php
SB_Module::RunSQL('employees');
$permissions = array(
		array('permission' => 'manage_employees','label'  => SB_Text::_('Manage employees', 'employees')),
		array('permission' => 'create_employee', 'label'  => SB_Text::_('Create employee','employees')),
		array('permission' => 'edit_employee',   'label'  => SB_Text::_('Edit employee','employees')),
		array('permission' => 'delete_employee', 'label'  => SB_Text::_('Delete employee','employees')),
);
$dbh = SB_Factory::getDbh();
$local_permissions = sb_get_permissions(false);
foreach ($permissions as $perm )
{
	if ( in_array($perm['permission'],$local_permissions) ) continue;
	$dbh->Insert('permissions', $perm);
}