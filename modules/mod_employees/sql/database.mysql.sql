CREATE TABLE IF NOT EXISTS mb_employees ( 
    employee_id            INTEGER         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    extern_id              INTEGER         DEFAULT 0,
    code                   VARCHAR( 512 ),
    store_id               INTEGER,
    pin						integer(4),
    total_hours_work 		tinyint(2),
    department_id			integer,
    first_name             VARCHAR( 128 ),
    last_name              VARCHAR( 128 ),
    company                VARCHAR( 128 ),
    date_of_birth          DATE,
    age						tinyint(2),
    identity_document		integer,
    gender                 VARCHAR( 64 ),
    phone                  VARCHAR( 64 ),
    mobile                 VARCHAR( 64 ),
    fax                    VARCHAR( 64 ),
    email                  VARCHAR( 128 ),
    address_1              VARCHAR( 256 ),
    address_2              VARCHAR( 256 ),
    zip_code               VARCHAR( 32 ),
    city                   VARCHAR( 128 ),
    country                VARCHAR( 128 ),
    country_code           VARCHAR( 10 ),
    status					varchar(64),
    notes					text,
    last_modification_date DATETIME,
    creation_date          DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_employee_time_card(
	id					integer not null auto_increment primary key,
	employee_id			integer not null,
	attendance_type		varchar(64),
	attendance			datetime,
	creation_date		datetime	
);
CREATE TABLE IF NOT EXISTS mb_employee_meta ( 
    meta_id       INTEGER         NOT NULL
                                  PRIMARY KEY AUTO_INCREMENT,
    employee_id   INTEGER         NOT NULL,
    meta_key      VARCHAR( 256 ),
    meta_value    TEXT,
    creation_date DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_working ( 
  employee_id               INTEGER NOT NULL,
  store_id                  INTEGER NOT NULL,
  last_modification_date    DATETIME,
  creation_date             DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_contract ( 
  contract_id                INTEGER     NOT NULL PRIMARY KEY AUTO_INCREMENT,
  employee_id                INTEGER     NOT NULL,
  identity_document          VARCHAR( 64 ),
  status                     VARCHAR( 128 ),
  position                   VARCHAR( 128 ),
  salary                     DECIMAL( 15, 2 ),
  contract_date              VARCHAR(100),
  contract_expiration_date   VARCHAR(100),
  last_modification_date     DATETIME,
  creation_date              DATETIME
);
