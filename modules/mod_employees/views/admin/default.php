<div class="wrap">
    <h1>
    	<?php print $title; ?>
    	<a class="btn btn-primary pull-right" href="index.php?mod=employees&view=new">
			<?php _e('New','employees');?>
		</a>
    </h1>
    <div class="row-fluid">
		<table class="table table-condensed">
			<thead>
				<tr>
	                <th><?php print SB_Text::_('Nro');?></th>
	                <th><?php print SB_Text::_('Code', 'employees');?></th>
	                <th><?php print SB_Text::_('Name', 'employees');?></th>
	                <th><?php print SB_Text::_('City', 'employees')?></th>
	                <th><?php print SB_Text::_('Telephone', 'employees');?></th>
	                <th><?php print SB_Text::_('Age', 'employees');?></th>
	                <th><?php print SB_Text::_('Gender','employees');?></th>
	                <th><?php print SB_Text::_('Position','employees');?></th>
	                <th><?php print SB_Text::_('Identity Document', 'employees');?></th>
	                <th><?php print SB_Text::_('Actions');?></th>
           		</tr>
           	</thead>
           	<tbody>
				<?php $i=1;if (count($employees)):foreach ($employees as $e): ?>
				<tr>
					<td> <?php print $e->employee_id; ?> </td>
					<td> <?php print $e->code; ?></td>
					<td> <?php printf ("%s %s" , $e->first_name, $e->last_name); ?> </td>
					<td> <?php print $e->city; ?> </td>
					<td> <?php printf ("%s %s" , $e->phone, $e->mobile);?> </td>
					<td> <?php print $e->age; ?> </td>
					<td> <?php print $e->gender == 'male' ? __('Male', 'employees') : __('Female', 'employees'); ?></td>
					<td> <?php print $e->position; ?></td>
					<td> <?php print $e->identity_document; ?></td>
					<td>
						<a class="btn btn-default btn-sm" href="<?php print SB_Route::_('index.php?mod=employees&task=view&id='.$e->employee_id);?>"
							title="<?php print SB_Text::_('View', 'employees')?>">
							<span class="glyphicon glyphicon-eye-open"></span>
						</a>
						<a class="btn btn-default btn-sm" href="<?php print SB_Route::_('index.php?mod=employees&task=edit&id=' .$e->employee_id);?>"
							title="<?php print SB_Text::_('Edit')?>">
							<span class="glyphicon glyphicon-edit"></span>
						</a>
						<a class="confirm btn btn-default btn-sm" data-message="<?php print SB_Text::_('Are you sure to delete the employee ?', 'employees');?>"
							href="<?php print SB_Route::_('index.php?mod=employees&task=delete&id=' . $e->employee_id);?>"
							title="<?php print SB_Text::_('Delete')?>">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td>
				</tr>
				<?php $i++;endforeach; else : ?>
				<tr>
					<td colspan="4"> <?php print SB_Text::_('There are no employees registered yet' , 'mb_e');?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
         <?php lt_pagination(SB_Route::_('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
	</div>
	<div>
		<div class="panel panel-primary">
			<div class="panel-heading"> <?php print SB_Text::_('Employee' , 'employees')?></div>
			<div id="employee-data" class="panel-body">
				<div class="row">
					<div class="col-md-2">
						<img src=" <?php print BASEURL; ?> /images/empty-avatar.png" alt="" class="img-thumbnail" /> 
					</div>
					<div class="col-md-10"> 
						<div class="row-fluid">
							<div class="span2"> <b> <?php print SB_Text::_('Name' , 'employees')?> : </b> <?php print isset($employee_new) ? $employee_new ->name : ''; ?> </div>
						</div>
						<div class="row-fluid">
							<div class="span2">  <b> <?php print SB_Text::_('Mobile' , 'employees')?> : </b> <?php print isset($employee_new) ? $employee_new ->mobile : ''; ?> </div>
						</div>
						<div  class="row-fluid">
							<div class="span2">  <b> <?php print SB_Text::_('Gender' , 'employees')?> : </b> <?php print isset($employee_new) ? $employee_new->gender : ''; ?> </div>
						</div> 
						<div  class="row-fluid">
							<div class="span2">  <b> <?php  print SB_Text::_('Identity Document','employees')?> : </b> <?php print isset($employee_old) ? $employee_old->identity_document : ''; ?> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>             
