<?php
$meta = SB_Request::getVar('meta');
if(isset($the_employee)){
	switch ($the_employee->total_hours_work){
		case 'Tiempo Completo' 	: $select_hours = SB_Request::getInt('Total_hours_work',1);
								 break;
		case 'Medio Tiempo' 	: $select_hours = SB_Request::getInt('Total_hours_work',2);
								 break;
		case 'opcional' 		: $select_hours = SB_Request::getInt('Total_hours_work',3);
								 break;
	}
	
}
else
{
	$select_hours = SB_Request::getInt('Total_hours_work', -1);
}
$select_store = SB_Request::getInt('store_id', isset($the_employee) ?  $the_employee->store_id : -1);
?>
<div class="wrap" >
	<h1><?php print $title; ?> </h1>
	<form action="" method="post" class="form-group-sm">
		<?php print SB_HtmlBuilder::writeInput('hidden', 'mod','employees');?>
		<?php print SB_HtmlBuilder::writeInput('hidden', 'task', 'save');?>
		<?php if(isset($employee_id)):?>
			<input type ="hidden" name="employee_id" value=<?php print $employee_id;?>>
		<?php endif;?>
		<div>
			<ul id="employee-tabs" class="nav nav-tabs">
				<li class="active"><a href="#general"><?php _e('General Info', 'employees'); ?></a></li>
				<li><a href="#tab-family" data-toggle="tab"><?php _e('Additional Information', 'employees'); ?></a></li>
				<li><a href="#tab-address" data-toggle="tab" onclick="initMap();"><?php _e('Address', 'employees'); ?></a></li>
				<li><a href="#attendance"><?php _e('Attendance Records', 'employees'); ?></a></li>
			</ul>
			<div class="tab-content">
				<div id="general" class="tab-pane active">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<?php print SB_Text::_('General Information', 'employees') ?>
							</div><!-- end class="panel-heading" -->
				            <div class="panel-body">
				            	<?php SB_module::do_action('before_employee_fields');?>
				            	<div class="form-group">  
				                	<label><?php print SB_Text::_('First Name', 'employees'); ?></label>
				                         <?php print SB_HtmlBuilder::writeInput('text', 'first_name',
				                  		                                 isset($the_employee) ? $the_employee->first_name : SB_Request::getString('first_name'),
				                  		                                 'first_name', array('class' => 'form-control'));?>
				              	</div> 
				              	<div class="form-group">
				              		<label> <?php print SB_Text::_('Last Name', 'employees'); ?></label>
				                      <?php print SB_HtmlBuilder::writeInput('text', 'last_name',
				                   		                                   isset($the_employee) ? $the_employee->last_name : SB_Request::getString('last_name'),
				                   		                                   'last_name', array('class'=> 'form-control'));?>
				             	</div>
				              	<div class="form-group">
				                 	<label><?php print SB_Text::_('Birth date', 'employees'); ?></label>
				                   	<input type="text" class="datepicker form-control" name="date_of_birth" style="width: 150px;border: 1px solid#aaaaaa;" value="<?php print isset($the_employee) ? $the_employee->date_of_birth : '' ?>" />                                                                                                                                  
				              	</div>
				              	<div class="form-group">
				                  	<label><?php print SB_Text::_('Age', 'employees');?></label>
				                         <?php print SB_HtmlBuilder::writeInput('text', 'age',
				                         		                            isset($the_employee) ? $the_employee->age : SB_Request::getString('age'),
				                         		                             'age', array('class' => 'form-control')); ?>
				                </div>
				              	<div class ="form-group">
				                  	<label><?php print SB_Text::_('CI', 'employees');?></label>
				                         <?php print SB_HtmlBuilder::writeInput('text', 'DNI',
				                         		                            isset($the_employee) ? $the_employee->DNI :SB_Request::getString('DNI'),
				                         		                             'DNI' , array('class' =>'form-control'));  ?>
				              	</div>
				              	<div class="form-group">
				              		<label><?php print SB_Text::_('Gender', 'employees');?> </label>
				                 	<select  id="gender"  name="gender" class="form-control">
					                	<option value="-1">  <?php print SB_Text::_('-- Gender --','employees'); ?></option>
					                    <option value="male" <?php print (isset($the_employee) && $the_employee->gender == 'male') ? 'selected' : '' ; ?>>
					                    	<?php _e('Male', 'employees'); ?>
					                    </option>                       
					                    <option value="female" <?php print (isset($the_employee) && $the_employee->gender == 'female') ? 'selected' : '' ; ?>>
					                    	<?php _e('Female', 'employees'); ?>
					                    </option>
				                 	</select>
				            	</div>
				              	<div class="form-group">
				                  	<label><?php print SB_Text::_('Phone', 'employees');?> </label>
				                         <?php print SB_HtmlBuilder::writeInput('text', 'phone',
				                         		                                isset($the_employee) ? $the_employee->phone : SB_Request::getString('phone'),
				                         		                                 'phone', array ('class'=>'form-control')); ?>
				              	</div>
				              	<div class="form-group">
				                    <label><?php print SB_Text::_('Mobile', 'employees');?></label>    
									<?php print SB_HtmlBuilder::writeInput('text', 'mobile',
			                                                                 isset($the_employee)  ? $the_employee->mobile : SB_Request::getString('mobile'),
			                                                                     'mobile', array('class'=>'form-control'));  ?>      
				              	</div>
				              	<div class="form-group">
				                  	<label><?php print SB_Text::_('City', 'employees'); ?></label>
				                         <?php print SB_HtmlBuilder::writeInput('text', 'city',
				                         		                                isset($the_employee)  ? $the_employee->city : SB_Request::getString('city'),
				                         		                                'city', array('class'=> 'form-control')); ?>
				             	</div>
				             	<div class="form-group">
					            	<label><?php print SB_Text::_('Store', 'employees'); ?></label>
					                <select id="store_id" name="store_id"  class="form-control">
						                <option value="-1" > <?php print SB_Text::_('--stores--','employees');?></option>
						                <?php foreach ($stores as $store): ?>
						                <option value="<?php print $store->store_id;?>" <?php print ($select_store==$store->store_id) ? 'selected' : '' ;?>>
						                <?php print $store->store_name; ?>
						                </option>                  
						                <?php endforeach; ?>
					                </select>       
				              	</div>
				              	<div class="form-group">
				              		<label><?php print SB_Text::_('Hours work','employees')?></label>
				              		<select id="Total_hours_work" name="Total_hours_work" class="form-control">
				              			<option value="-1"><?php print SB_Text::_('-- HOURS WORK --');?></option>
				              			<option value="Tiempo Completo" <?php print ($select_hours == 1) ? 'selected' : ''?>><?php print SB_Text::_('Tiempo Completo');?></option>
				              			<option value="Medio Tiempo" <?php print ($select_hours == 2) ? 'selected' : ''?>><?php print SB_Text::_('Medio Tiempo');?></option>
				              			<option value="opcional" <?php print ($select_hours == 3) ? 'selected' : ''?>><?php print SB_Text::_('opcional');?></option>
				              		</select>
				              		<?php SB_HtmlBuilder::writeInput('text', 'Total_hours_work',
				              				isset($the_employee) ? $the_employee->total_hours_work : '',
				              				'Total_hours_work',array('class'=>'form-control'));?>
				              	</div>
				                    <?php SB_Module::do_action('before_employee-notes_field', isset($the_employee) ? $the_employee : null); ?>
				             	<div  class="form-group">
				                	<label><?php print SB_Text::_('Notes','employees'); ?></label>
				                    <textarea rows="" cols="" name="meta[notes]" class="form-control"> <?php print isset($the_employee)  ? $the_employee->notes : ''; ?></textarea>             
				      		 	</div>
				      		 	<div class="form-group">
				      		 		<label><?php _e('PIN', 'employees'); ?></label>
				      		 		<input type="text" name="pinxx" value="<?php print isset($the_employee) ? $the_employee->pin : '';?>" 
				      		 				readonly="readonly" class="form-control" />
				      		 	</div>
				      		</div>
						</div><!-- end class="easyui-tabs" -->
					</div>
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<?php print SB_Text::_('Contract Information', 'employees') ?>
							</div><!-- end class="panel-heading" -->
				      		<div class="panel-body">
				      			<div class="form-group">
				      		    	<label><?php print SB_Text::_('Identity Document' , 'employees');?></label>
				      		                 <?php print SB_HtmlBuilder::writeInput('text', 'identity_document',
				      		                 		                                  isset($employee_con) ? $employee_con->identity_document : SB_Request::getString('identity_document'),
				      		                 		                                   'identity_document', array('class'=> 'form-control'));?>
				      		    </div>
				      		    <div class="form-group">
				      		         <label><?php print SB_Text::_('Position' , 'employees');?></label>
				      		                 <?php print SB_HtmlBuilder::writeInput('text', 'position',
				      		                 		                                  isset($employee_con) ? $employee_con->position : SB_Request::getString('position'),
				      		                 		                                   'position', array('class'=> 'form-control'));?>         
				      		    </div>
				      		    <div class ="form-group" >
				              		<label><?php print SB_Text::_('Status', 'employees'); ?></label>
				                  		<?php print SB_HtmlBuilder::writeInput('text', 'status',
				                         		                                isset($the_employee)  ? $the_employee->status : SB_Request::getString('status'),
				                         		                                'status', array('class'=> 'form-control')); ?>
				             	</div>
				      		    <div class="form-group">
				      		        <label><?php print SB_Text::_('Salary', 'employees');?></label>
				      		                <?php print SB_HtmlBuilder::writeInput('text', 'salary',
				      		                		                                 isset($employee_con)  ? $employee_con->salary  : SB_Request::getString('salary'),
				      		                		                                  'salary', array('class' =>'form-control'));?>
				      		    </div>
				      	        <div class="form-group">
				      		        <label><?php print SB_Text::_('Contract Date', 'employees');?></label>
									<input type="text" class="datepicker form-control" id="contract_date" name="contract_date" style="width: 150px;border: 1px solid#aaaaaa; "
							    							value="<?php print isset($employee_con) ? $employee_con->contract_date :''; ?>" />
				      		    </div>
				      		    <div class ="form-group">
				      		        <label><?php print SB_Text::_('Contract Expiration Date', 'employees');?>  </label>
									<input type="text" class="datepicker form-control" id="contract_expiration_date" name="contract_expiration_date" style="width: 150px;border: 1px solid#aaaaaa; "
							    							value="<?php print isset($employee_con) ? $employee_con->contract_expiration_date:''; ?>" />    
				      			</div>
				      		</div>
				      	</div><!-- end class="panel" -->  
			      	</div>
			      	<div class="clearfix"></div>
				</div><!-- end id="general" -->
				<div id="tab-family" class="tab-pane">
					<fieldset>
						<legend><?php _e('Father Information', 'employees'); ?></legend>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Firstname', 'employees'); ?></label>
									<input type="text" name="meta[_father_first_name]" value="<?php print isset($the_employee) ? $the_employee->_father_first_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Lastname', 'employees'); ?></label>
									<input type="text" name="meta[_father_last_name]" value="<?php print isset($the_employee) ? $the_employee->_father_last_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Telephone', 'employees'); ?></label>
									<input type="text" name="meta[_father_telephone]" value="<?php print isset($the_employee) ? $the_employee->_father_telephone : ''; ?>" class="form-control" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label><?php _e('Address', 'customers'); ?></label>
							<input type="text" name="meta[_father_address]" value="<?php print isset($the_employee) ? $the_employee->_father_address : ''; ?>" class="form-control" />
						</div>
					</fieldset>
					<fieldset>
						<legend><?php _e('Mother Information', 'employees'); ?></legend>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Firstname', 'employees'); ?></label>
									<input type="text" name="meta[_mother_first_name]" value="<?php print isset($the_employee) ? $the_employee->_mother_first_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Lastname', 'employees'); ?></label>
									<input type="text" name="meta[_mother_last_name]" value="<?php print isset($the_employee) ? $the_employee->_mother_last_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Telephone', 'employees'); ?></label>
									<input type="text" name="meta[_mother_telephone]" value="<?php print isset($the_employee) ? $the_employee->_mother_telephone : ''; ?>" class="form-control" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label><?php _e('Address', 'employees'); ?></label>
							<input type="text" name="meta[_mother_address]" value="<?php print isset($the_employee) ? $the_employee->_mother_address : ''; ?>" class="form-control" />
						</div>
					</fieldset>
					<fieldset>
						<legend><?php _e('Wife Information', 'employees'); ?></legend>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Firstname', 'employees'); ?></label>
									<input type="text" name="meta[_wife_first_name]" value="<?php print isset($the_employee) ? $the_employee->_wife_first_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Lastname', 'employees'); ?></label>
									<input type="text" name="meta[_wife_last_name]" value="<?php print isset($the_employee) ? $the_employee->_wife_last_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Telephone', 'employees'); ?></label>
									<input type="text" name="meta[_wife_telephone]" value="<?php print isset($the_employee) ? $the_employee->_wife_telephone : ''; ?>" class="form-control" />
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend><?php _e('Son Information', 'employees'); ?></legend>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Firstname', 'employees'); ?></label>
									<input type="text" name="meta[_son1_first_name]" value="<?php print isset($the_employee) ? $the_employee->_son1_first_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Lastname', 'employees'); ?></label>
									<input type="text" name="meta[_son1_last_name]" value="<?php print isset($the_employee) ? $the_employee->_son1_last_name : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php _e('Telephone', 'employees'); ?></label>
									<input type="text" name="meta[_son1_telephone]" value="<?php print isset($the_employee) ? $the_employee->_son1_telephone : ''; ?>" class="form-control" />
								</div>
							</div>
						</div>
					</fieldset>
				</div><!-- end id="tab-family" -->
				<div id="tab-address" class="tab-pane">
					<div class="form-group">
	              		<label><?php print SB_Text::_('Address', 'employees');?></label>
	              		<input type="text" id="address_1" name="address_1" value="<?php print isset($the_employee) ? $the_employee->address_1 : ''; ?>" class="form-control" />
	              	</div>
	              	<input type="hidden" id="lat" name="meta[_lat]" value="<?php print isset($the_employee) ? $the_employee->_lat : ''; ?>" />
	              	<input type="hidden" id="lng" name="meta[_lng]" value="<?php print isset($the_employee) ? $the_employee->_lng : ''; ?>" />
	              	<div id="map" style="width:100%;height:350px;">
	              		<?php /* ?>
	              		<iframe src="https://www.google.com/maps/embed/v1/directions?origin=Belisario Salinas, La Paz, Deparment,Bolivia&destination=Belisario Salinas, La Paz, Deparment,Bolivia&key=AIzaSyAL5RRwu9kXrgE6M5K7pLswjUIBGfqhxR0"
	              			style="width:100%;height:350px;" frameborder="0">
						</iframe>
						*/ ?>
	              	</div>
	              	<script src="https://maps.googleapis.com/maps/api/js?region=es_ES&key=AIzaSyAL5RRwu9kXrgE6M5K7pLswjUIBGfqhxR0"></script>
	              	<script>
					var map;
					var geocoder 	= null;
					var marker 		= null;
					var lat = lng 	= null;
					function initMap() 
					{
						geocoder = new google.maps.Geocoder();
				        map = new google.maps.Map(document.getElementById('map'), 
						{
				        	center: {lat: -16.4956817, lng: -68.1335464},
				          	zoom: 16
				        });
				        map.setZoom(16);
						lat = parseFloat(jQuery('#lat').val());
				        lng = parseFloat(jQuery('#lng').val());
				        if( !isNaN(lat) && !isNaN(lng) )
				        {
				        	map.setCenter({lat: lat, lng: lng});
				        	marker = new google.maps.Marker({
				            	map: map,
				            	draggable: true,
				                animation: google.maps.Animation.DROP,
				            	position: {lat: lat, lng: lng}
				        	});
				        	map.setCenter({lat: lat, lng: lng});
					    }
					}
					jQuery(function()
					{					
						jQuery('#address_1').keydown(function(e)
						{
							if( e.keyCode == 13 )
							{
								geocoder.geocode( { 'address': this.value}, function(results, status) 
								{
									//console.log(results);
							    	if (status == google.maps.GeocoderStatus.OK) 
								    {
							        	map.setCenter(results[0].geometry.location);
							        	//##remove the marker if exists
							        	if( marker != null )
								        	marker.setMap(null);
							        	marker = new google.maps.Marker({
							            	map: map,
							            	draggable:true,
							                animation: google.maps.Animation.DROP,
							            	position: results[0].geometry.location
							        	});
							        	//##set marker event
							        	google.maps.event.addListener(marker, 'dragend', function() 
					        			{
					        			    //geocodePosition(marker.getPosition());
					        			    jQuery('#lat').val(marker.getPosition().lat());
					        			    jQuery('#lng').val(marker.getPosition().lng());
					        			});
							      	} 
							      	else 
								    {
							        	alert("Geocode was not successful for the following reason: " + status);
							      	}
								});
								e.preventDefault();
								e.stopPropagation();
								return false;
							}
							return true;
						});
						jQuery('.map').on('shown',function(){ google.maps.event.trigger(map, 'resize'); });
					});
				    </script>
				    <style type="text/css">
				    @media print
				    {
						a[href]:after 
						{
						    content: none !important;
						}
					    .gm-style img 
					    {
    						max-width: none;
  						}
						  .gm-style label {
						    width: auto; display:inline;
						  }
				    }
				    </style>
				</div><!-- end id="tab-address" -->
				<div id="attendance" class="tab-pane">
					<div class="form-group">
						<a href="javascript:;" class="btn btn-success"><?php _e('Edit', 'employees'); ?></a>
						<a href="javascript:;" class="btn btn-danger"><?php _e('Delete', 'employees'); ?></a>
					</div>
					<table class="table">
					<thead>
					<tr>
						<th><input type="checkbox" id="select-attend-records" name="select" value="1" /></th>
						<th><?php _e('Check In', 'employees'); ?></th>
						<th><?php _e('Check Out', 'employees'); ?></th>
						<th><?php _e('Duration (Hours)', 'employees'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php if(isset($the_employee)): foreach($the_employee->GetAttendance() as $r): ?>
					<tr>
						<td class="text-center"><input type="checkbox" name="attendance[]" value="<?php print $r->id; ?>" /></td>
						<td class="text-center"><?php print $r->check_in; ?></td>
						<td class="text-center"><?php print $r->check_out; ?></td>
						<td class="text-right"><?php print $r->hours; ?></td>
					</tr>
					<?php endforeach;endif; ?>
					</tbody>
					</table>
				</div><!-- end id="general" -->
			</div>
		</div>
		<div class ="form-group">
      		<button class="btn btn-success" type ="submit" ><?php print SB_Text::_('Save'); ?></button>                      
        	<a class="btn btn-danger" href="<?php print SB_Route::_('index.php?mod=employees');?>"><?php print SB_Text::_('Cancel'); ?></a>  		                                  
			<?php SB_Module::do_action('employee_buttons');?>
        </div>	
	</form>
	<script>
	jQuery(function()
	{
		jQuery('#employee-tabs a').click(function (e) {
			  e.preventDefault()
			  jQuery(this).tab('show');
			})
	});
	</script>
</div>
 
        		