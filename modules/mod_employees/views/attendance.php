<?php
?>
	<div id="employee-check" class="container">
		<div class="row">
			<div id="employee-image">
				<div id="dclock"></div>
				<img src="<?php print MOD_EMPLOYEES_URL; ?>/images/nobody.png" alt="" class="img-thumbnail" />
				<div id="attendance-message"></div>
			</div>
			<div id="check-modes">
				<!-- 
				<p><a href="" class="btn btn-info">MANUAL CHECK</a></p>
				<p><a href="" class="btn btn-success">FINGERPRINT CHECK</a></p>
				<p><a href="" class="btn btn-warning">FACIAL CHECK</a></p>
				 -->
				<form id="check-form" action="" method="post" onsubmit="return false;">
					<input type="hidden" name="mod" value="employees" />
					<input type="hidden" name="task" value="check" />
					<div class="form-group">
						<label><?php _e('Employee PIN:', 'employees'); ?></label><br/>
						<input type="password" id="pin"  name="pin" value="" class="form-control" />
					</div>
					<div id="check-buttons">
						<h4><?php _e('Check', 'employees'); ?></h4>
						<a href="javascript:;" class="btn-check btn-check-in btn-success" data-action="check_in">IN</a>
						<a href="javascript:;" class="btn-check btn-check-out btn-danger" data-action="check_out">OUT</a>
						<a href="javascript:;" class="btn-check btn-check-lunch btn-info" data-action="check_lunch">LUNCH</a>
						<a href="javascript:;" class="btn-check btn-check-lunch btn-warning" data-action="check_metting">MEETING</a>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div id="check-clock">
				<canvas id="analog-clock" width="300" height="300"></canvas>
			</div>
		</div>
	</div>
<script>
function DClock(id, ops)
{
	var el = document.getElementById(id);
	el.style.background = ops && ops.background ? ops.background : '#000';
	el.style.color 		= ops && ops.color ? ops.color : '#fff';
	el.style.fontWeight = ops && ops.font_weight ? ops.font_weight : 'bold';
	el.style.fontSize 	= ops && ops.font_size ? ops.font_size : '20px';
	var timer = null;
	this.Start = function()
	{
		//Fetch the current time
		var ampm = "AM";
		var now = new Date();
		var hrs = now.getHours();
		var min = now.getMinutes();
		var sec = now.getSeconds();
		if( min < 10 )
		{
			min = '0' + min;
		}
		if( sec < 10 )
		{
			sec = '0' + sec;
		}
		el.innerHTML = hrs + ':' + min + ':' + sec;
		if( timer == null )
		{
			timer = setInterval(this.Start, 1000);
		}
	};
}
jQuery(function()
{
	//draw(200, 200);
	jQuery('head').append('<link rel="stylesheet" href="<?php print MOD_EMPLOYEES_URL; ?>/css/check.css" />');
	var dclock = new DClock('dclock', {font_size: '25px'});
	dclock.Start();
	jQuery('.btn-check').click(function()
	{
		var params = jQuery('#check-form').serialize();
		params += '&action=' + this.dataset.action;
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				jQuery('#attendance-message').html(res.message).css('display', 'block').fadeOut(6000);
			}
			else
			{
				alert(res.error);
			}
			jQuery('#pin').val('');
		});
		return false;
	});
	/*
	jQuery('head link').each(function(i, link)
	{
		var bootstrap_exists = false;
		if( link.href.indexOf('bootstrap') != -1 )
		{
			bootstrap_exists = true;
		}
		if( !bootstrap_exists )
		{
			jQuery('head').append('<link rel="stylesheet"   />');
		}
	});
	*/
});

</script>