��    1      �  C   ,      8     9     >     E     W     \     i  Z   r  	   �     �     �     �                    %  	   2     <     H     \     d     m     r     x     |      �     �     �     �     �     �     �     �     �     �     �     �               <  9   Z  8   �  "   �      �       !   ,  6   N     �  C   �  �  �     �	     �	     �	     �	     �	     �	  f   
     l
     ~
     �
     �
     �
     �
     �
     �
       
             -     5     >     E     M     S  *   d     �     �     �     �     �     �     �     �     �     �  	   	  !     *   5     `  8   }  F   �  "   �  &         G  )   d  <   �     �  P   �     	           *   %   '       #      /   +             .                            ,   "       0       
       !         )                                                                       -   &   1                    $   (           Back Cancel Customer: %s<br/> Date Description: Designer Dont forget to add these placholders at the begining of your form or before &lt;/form> tag Edit form Entries Export to Excel Find the details below Form Entries Form Fields Forms General Info HTML Code Hello Admin Hello Administrator Message Message: Name Name: New New Form New entry for "%s" form received Phone Phone: %s<br/> Preview Qty Regards Regards, Save Subject Submit Target Email: Template The form has been updated The form identifier is invalid The new form has been created There is errors on your form submit, please double check. You have a new %s request, find details below.<br/><br/> You have a new form entry for "%s" You need to type a customer name You need to type a message You need to type an email address Your form action needs to be equal to => {form_action} Your form has been sent Your message has been sent, we will contact you as soon as possible Project-Id-Version: BeetleCMS Forms
POT-Creation-Date: 2017-10-04 20:13-0400
PO-Revision-Date: 2017-10-04 20:13-0400
Last-Translator: 
Language-Team: Sintic Bolivia <info@sinticbolivia.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_;_e
X-Poedit-SearchPath-0: .
 Volver Cancelar Cliente: %s<br/> Fecha Descripcion: Dise&ntilde;ador No olvide adicionar estos placeholders al principio de su formulario o antes de la etiqueta &lt;/form> Editar Formulario Entradas Exportar a Excel Encuentra los detalles abajo Entradas del Formulario Campos del Formulario Formularios Informacion General Codigo HTML Hola Admin Hola Administrador Mensaje Mensaje: Nombre Nombre: Nuevo Nuevo Formulario Nueva entrada del formulario "%s" recibida Telefono Telefono: %s<br/> Previsualizacion Cant Saludos Cordiales Soludos Cordiales Guardar Asunto Enviar Email Destino: Plantilla El formulario ha sido actualizado El identificador de formulario es invalido El formulario ha sido creado Hay errores en el envio del formulario, porfavor revisa. Tienes una nueva solicitud %s, encuentra los detalles abajo.<br/><br/> Tienes una nueva entrada para "%s" Necesita ingresar un nombre de cliente Necesita ingresar un mensaje Necesita ingresar una direccion de correo La accion de su formulario debe ser igual a => {form_action} Su formulario ha sido enviado Su mensaje fue enviado, nos pondremos en contacto con usted lo mas antes posible 