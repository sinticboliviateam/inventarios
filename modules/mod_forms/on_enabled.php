<?php 
defined('BASEPATH') or die('Dont fuck with me buddy.');
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;

SB_Module::RunSQL('forms');
$dbh = SB_Factory::getDbh();
$permissions = array(
		array('group' => 'forms', 'permission' => 'manage_forms', 'label'	=> __('Gestionar Formularios', 'forms')),
		array('group' => 'forms', 'permission' => 'create_form', 'label'	=> __('Crear formulario', 'forms')),
		array('group' => 'forms', 'permission' => 'edit_form', 'label'	=> __('Editar formulario','forms')),
		array('group' => 'forms', 'permission' => 'delete_form', 'label'	=> __('Borrar formulario', 'forms')),
);
sb_add_permissions($permissions);