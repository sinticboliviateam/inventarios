<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Invoices\Classes\LT_MBInvoice;
use SinticBolivia\SBFramework\Classes\SB_Session;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Route;

class LT_AdminControllerInvoices extends SB_Controller
{
	public function task_default()
	{
		$user = sb_get_current_user();
		if( !$user->can('manage_invoices') )
		{
			lt_die(__('You dont have enough permissions', 'invoices'));
		}
		
		$keyword 	= SB_Request::getString('keyword');
		$search_by 	= SB_Request::getString('search_by');
		$from_date	= SB_Request::getDate('from_date');
		$to_date	= SB_Request::getDate('to_date');
		$page		= SB_Request::getInt('page', 1);
		$limit		= SB_Request::getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
		$dbh = SB_Factory::getDbh();
		$query = "SELECT {columns} FROM {tables} ";
		$columns = array('i.*', 'd.name');
		$tables = array("mb_invoices i", 'mb_invoice_dosages d');
		$query .= "WHERE 1 = 1 ";
		$query .= "AND i.dosage_id = d.id ";
		if( !empty($keyword) )
		{
			if( $search_by == 'invoice_number' )
			{
				$query .= "AND invoice_number = $keyword ";
			}
			elseif( $search_by == 'customer' )
			{
				$query .= "AND customer LIKE '%$keyword%' ";
			}
			elseif( $search_by == 'id' )
			{
				$query .= "AND invoice_id = $keyword ";
			}
			
		}
		elseif( $from_date && $to_date )
		{
			$query .= "AND (DATE(i.invoice_date_time) >= '$from_date' AND DATE(i.invoice_date_time) <= '$to_date') ";
		}
		$query .= "ORDER BY i.creation_date DESC ";
		$dbh->Query(str_replace(array('{columns}', '{tables}'), array('COUNT(invoice_id)', implode(',', $tables)), $query));
		$total_rows 	= $this->dbh->GetVar();
		$total_pages 	= ceil($total_rows / $limit); 
		$offset			= $page <= 1 ? 0 : ($page - 1) * $limit;
		$query 			.= "LIMIT $offset, $limit";
		$invoices 		= $dbh->FetchResults(str_replace(array('{columns}', '{tables}'), array(implode(',', $columns), implode(',', $tables)), $query));
		sb_set_view_var('user', $user);
		sb_set_view_var('invoices', $invoices);
		sb_set_view_var('total_pages', $total_pages);
		sb_set_view_var('current_page', $page);
	}
	public function task_new()
	{
		$query = "SELECT * FROM mb_invoice_dosages ".
					"WHERE 1 = 1 " . 
					"AND emission_limit_date >= curdate() " .
					"ORDER BY name ASC";
		$this->_dosages = $this->dbh->FetchResults($query);
		if( !count($this->dosages) )
		{
			SB_MessagesStack::AddMessage(__('You dont have invoice dosages, please add atleast one dosage', 'invoices'), 'info');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		/*
		if( SB_Module::moduleExists('mb') )
		{
			
		}
		else*/
		//{
			$taxes = array();
			$taxes[] = (object)array(
							'name' => __('IVA (13%)'),
							'rate'	=> 13
			);
			$taxes[] = (object)array(
					'name' => __('IT (3%)'),
					'rate'	=> 3
			);
			$this->_taxes = $taxes; 
		//}
		$this->_currencies 	= mb_invoices_get_currencies();
		$this->_templates	= mb_invoices_get_templates();
		$title = __('New Invoice', 'invoices');
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_INVOICES_URL . '/js/new.js', 'invoices-new');
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
		SB_Module::do_action('invoice_new');
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		if( !$user->can('edit_invoice') )
		{
			lt_die(__('You dont have enough permissions to edit invoices', 'invoices'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The invoice identifier is invalid', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$invoice = new LT_MBInvoice($id);
		if( !$invoice->invoice_id )
		{
			SB_MessagesStack::AddMessage(__('The invoice does not exists', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$title = __('Edit Invoice', 'invoices');
		sb_set_view('new');
		$taxes = array();
		$taxes[] = (object)array(
				'name' => __('IVA (13%)'),
				'rate'	=> 13
		);
		$taxes[] = (object)array(
				'name' => __('IT (3%)'),
				'rate'	=> 3
		);
		$this->_taxes 		= $taxes;
		$this->_dosages		= mb_invoices_get_dosages($invoice->store_id);
		$this->_currencies 	= mb_invoices_get_currencies();
		$this->_templates	= mb_invoices_get_templates();
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_INVOICES_URL . '/js/new.js', 'invoices-new');
		sb_set_view_var('title', $title);
		sb_set_view_var('invoice', $invoice);
		
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		if( !sb_is_user_logged_in() )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You need to start a session', 'invoices')));
		}
		if( !sb_get_current_user()->can('create_invoice') )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You dont have enough permissions for the operation.', 'invoices')));
		}
		$dbh 			= SB_Factory::getDbh();
		$invoice_ops 	= sb_get_parameter('invoices_ops');
		$invoice_id		= SB_Request::getInt('invoice_id');
		$customer_id 	= SB_Request::getInt('customer_id');
		$customer_name	= SB_Request::getString('customer');
		$nit			= SB_Request::getString('nit_ruc_nif');
		$store_id		= SB_Session::getVar('invoice_store_id', 1);
		$tax_percent	= SB_Request::getInt('tax');
		$dosage_id		= SB_Request::getInt('dosage_id');
		$dosage			= null;
		$update 		= $invoice_id ? true : false;
		if( @$invoice_ops->business_country == 'BO' )
		{
			if( !$dosage_id )
				sb_response_json(array('status' => 'error', 'error' => __('You need to select an invoice dosage', 'invoices')));
			//##get invoice dosage
			$query = "SELECT * FROM mb_invoice_dosages WHERE id = $dosage_id LIMIT 1";
			$dosage = $this->dbh->FetchRow($query);
			if( !$dosage )
				sb_response_json(array('status' => 'error', 'error' => __('The invoice dosage does not exists', 'invoices')));
			if( strtotime($dosage->emission_limit_date) < time() )
				sb_response_json(array('status' => 'error', 'error' => __('The invoice dosage has expired, you need to enter a new one.', 'invoices')));
		}
		
		if( !is_numeric($nit) )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You must enter a NIT/RUC/NIF', 'invoices')));
		}
		if( empty($customer_name) )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You must enter a customer name', 'invoices')));
		}
		
		if( $customer_id > 0 )
		{
			//##update the nit
			SB_Meta::updateMeta('mb_customer_meta', '_nit_ruc_nif', $nit, 'customer_id', $customer_id);
		}
		else
		{
			//##try to find the customer into database
			$query = "SELECT c.* FROM mb_customers c, mb_customer_meta cm ".
						"WHERE c.customer_id = cm.customer_id ".
						"AND cm.meta_key = '_nit_ruc_nif' " . 
						"AND cm.meta_value = '$nit' ".
						"LIMIT 1";
			//##if customer does not exists, create a new one
			if( !$dbh->Query($query) )
			{
				//##create the customer
				$customer_data = array(
						'store_id'					=> $store_id,
						'first_name'				=> $customer_name,
						'last_modification_date'	=> date('Y-m-d H:i:s'),
						'creation_date'				=> date('Y-m-d H:i:s')
				);
				$customer_id = $dbh->Insert('mb_customers', $customer_data);
				SB_Meta::addMeta('mb_customer_meta', '_nit_ruc_nif', $nit, 'customer_id', $customer_id);
			}
			else
			{
				$customer_id = $dbh->FetchRow()->customer_id;
			}
		}
		$the_invoice	= $invoice_id ? new LT_MBInvoice($invoice_id) : null;
		$invoice_number = LT_MBInvoice::GetNextInvoiceNumber($dosage_id, $store_id);
		/*
		if( $invoice_ops->business_country == 'BO' )
		{
			//##get the next invoice number
			$invoice_number = LT_MBInvoice::GetNextInvoiceNumber($dosage_id, $store_id);
			
		}
		*/
		$items = SB_Request::getVar('items');
		//##create the invoice
		$total = 0;
		$tax = 0;
		$subtotal = 0;
		
		for($i = 0; $i < count($items) ; $i++)
		{
			$items[$i]['subtotal'] 	= $items[$i]['qty'] * $items[$i]['price'];
			$items[$i]['total'] 	= $items[$i]['subtotal'];// + $items[$i]['tax'];
			$total 					+= $items[$i]['subtotal'];
		}
		$tax 		= $tax_percent > 0 ? $total * ($tax_percent / 100) : 0;
		$subtotal 	= $total - $tax;
		$cdate 		= date('Y-m-d H:i:s');
		
		//##build the invoice security
		$invoice		= null;
		$invoice_class 	= null;
		$invoice_class_file = sprintf("class.%s.php", $invoice_ops->business_country);
		if( file_exists(MOD_INVOICES_DIR . SB_DS . 'countries' . SB_DS . $invoice_class_file) )
		{
			require_once MOD_INVOICES_DIR . SB_DS . 'countries' . SB_DS . $invoice_class_file;
			$invoice_class = 'SB_Invoice' . $invoice_ops->business_country;
			$invoice = new $invoice_class();
		}
		$control_code = null;
		if( $invoice )
		{
			if( $invoice_ops->business_country == 'BO' )
			{
				$data = array(
						'dosage'				=> $dosage->dosage,
						'authorization_number' 	=> $dosage->authorization,
						'invoice_number'		=> $the_invoice ? $the_invoice->invoice_number : $invoice_number,
						'nit_ci'				=> $nit,
						'transaction_date'		=> $the_invoice ? sb_format_date($the_invoice->invoice_date_time, 'Ymd') : date('Ymd'),
						'transaction_amount'	=> $the_invoice ? $the_invoice->total : $total
				);
				$control_code = $invoice->buildControlCode($data);
			}
				
		}
		$data = array(
				'dosage_id'				=> $dosage_id,
				'customer_id' 			=> $customer_id,
				'customer'				=> $customer_name,
				'user_id'				=> sb_get_current_user()->user_id,
				'store_id'				=> $store_id,
				'nit_ruc_nif'			=> $nit,
				'tax_rate'				=> ($tax_percent / 100),
				'subtotal'				=> $the_invoice ? $the_invoice->subtotal : $subtotal,
				'total_tax'				=> $the_invoice ? (float)$the_invoice->total_tax : $tax,
				'total'					=> $the_invoice ? $the_invoice->total : $total,
				'invoice_number'		=> $the_invoice ? $the_invoice->invoice_number : $invoice_number,
				'dosage'				=> $dosage->dosage,
				'control_code'			=> $control_code,
				'authorization'			=> $dosage->authorization,
				'invoice_date_time'		=> $cdate,
				'invoice_limite_date'	=> $dosage->emission_limit_date,
				'currency_code'			=> SB_Request::getString('currency'),
				'exchange_rate'			=> SB_Request::getFloat('exchange_rate', 1),
				'status'				=> 'issued',
				'creation_date'			=> $cdate
		);
		if( $the_invoice )
		{
			$data['invoice_id'] = $the_invoice->invoice_id;
		}
		//print_r($data);die();
		//##insert invoice into database
		$db_items = array();
		foreach($items as $item)
		{
			$db_items[] = array(
					//'invoice_id'	=> $invoice_id,
					'store_id'		=> $store_id,
					'product_id'	=> $item['id'],
					'product_code'	=> $item['code'],
					'product_name'	=> $item['name'],
					'price'			=> $item['price'],
					'quantity'		=> $item['qty'],
					'total'			=> $item['total'],
					'creation_date'	=> $cdate
			);
		}
		//##insert invoice items
		$the_invoice 	= mb_invoices_insert_new($data, $db_items);
		$invoice_id 	= $the_invoice->invoice_id;
		//##add invoice meta
		foreach((array)SB_Request::getVar('meta', array()) as $meta_key => $meta_value)
		{
			mb_invoice_update_meta($invoice_id, $meta_key, trim($meta_value));
		}
		mb_invoice_update_meta($invoice_id, '_billing_name', $customer_name);
		SB_Module::do_action('mb_invoice_save', $invoice_id, $data, $items, $invoice, $update);
		$res = array(
				'status' => 'ok',
				'message'	=> $the_invoice ? __('The invoice has been updated', 'invoices') : __('The invoice has been created', 'invoices'),
				'id'		=> $invoice_id,
				'print_url'	=> SB_Route::_('index.php?mod=invoices&view=view&print=1&id='.$invoice_id)
		);
		sb_response_json($res);
	}
	public function task_void()
	{
		if( !sb_is_user_logged_in() )
		{
			SB_MessagesStack::AddMessage(SBText::_('You need to start a session'), 'error');
			sb_redirect(SB_Route::_('login.php'));
		}
		if( !sb_get_current_user()->can('void_invoice') )
		{
			SB_MessagesStack::AddMessage(SBText::_('You dont have enough permission to complete this operation', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The invoice identifier is invalid', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_invoices WHERE invoice_id = $id LIMIT 1";
		if( !$dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(SBText::_('The invoice does not exists', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$invoice = $dbh->FetchRow();
		$dbh->Update('mb_invoices', array('status' => 'void'), array('invoice_id' => $invoice->invoice_id));
		SB_MessagesStack::AddMessage(SBText::_('The invoice is now void', 'invoices'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=invoices'));
	}
	public function task_delete()
	{
		$user = sb_get_current_user();
		if( !$user->can('delete_invoice') )
		{
			lt_die(__('You dont have enough permissions to delete invoices', 'invoices'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The invoice id is invalid.', 'invoices'), 'error');
			sb_redirect();
		}
		$invoice = new LT_MBInvoice($id);
		if( !$invoice->invoice_id )
		{
			SB_MessagesStack::AddMessage(__('The invoice does not exists', 'invoices'), 'error');
			sb_redirect();
		}
		$this->dbh->Delete('mb_invoice_items', array('invoice_id' => $invoice->invoice_id));
		$this->dbh->Delete('mb_invoice_meta', array('invoice_id' => $invoice->invoice_id));
		$this->dbh->Delete('mb_invoices', array('invoice_id' => $invoice->invoice_id));
		SB_MessagesStack::AddMessage(__('The invoice has been deleted', 'invoices'), 'error');
		sb_redirect(SB_Route::_('index.php?mod=invoices'));
	}
	public function task_view()
	{
		$user = sb_get_current_user();
		if( !$user->can('view_invoice') || !$user->can('print_invoice') )
		{
			lt_die(SBText::_('You dont have enough permissions to print invoices.', 'invoices'));
		}
		
		$id 	= SB_Request::getInt('id');
		$print 	= SB_Request::getInt('print');
		$dbh 	= SB_Factory::getDbh();
		$invoice = new LT_MBInvoice($id);
		if( !$invoice->invoice_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The invoice does not exists.', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		if( !$user->can('view_all_invoices') && $invoice->user_id != $user->user_id )
		{
			lt_die(__('You dont have enough permissions to print invoices.', 'invoices'));
		}
		//##get billing data
		$ops 		= (object)sb_get_parameter('invoices_ops', array());
		$dosage		= $this->dbh->FetchRow("SELECT * FROM mb_invoice_dosages WHERE id = $invoice->dosage_id LIMIT 1");
		
		$invoice_date = date('d/m/Y', strtotime($invoice->invoice_date_time));
		//##build qr code
		sb_include_lib('phpqrcode/qrlib.php');
		//################ -> 152030024|41734|384401600030589|25/07/2016|38.00|38.00|5C-EF-79-A2|4277340|0|0|0|0
		//##qr data format -> NIT|INVOICE_NUMBER|AUTHORIZATION|INVOICE_DATE|INVOICE_AMOUNT|INVOICE_AMOUNT|CONTROL_CODE|CUSTOMER_NIT|
		$qr_string = "$ops->business_nit_ruc_nif".
						"|$invoice->invoice_number".
						"|$dosage->authorization".
						"|$invoice_date".
						"|{$invoice->total}".
						"|{$invoice->total}".
						"|$invoice->control_code".
						"|$invoice->nit_ruc_nif".
						"|0|0|0|0";
		ob_start();
		QRcode::png($qr_string, $outfile = null, $level = QR_ECLEVEL_H, $size = 2, $margin = 0, $saveandprint=false);
		$res 				= ob_get_clean();
		$qr_code 			= 'data:image/png;base64,' . base64_encode($res);
		$invoice_limit_date = $dosage->emission_limit_date;
		$invoice_footer_msg = $dosage->footer_text;
		$countries 			= include_once INCLUDE_DIR . SB_DS . 'countries.php';
		$invoice_tpl_file 	= !empty($dosage->template) ? $dosage->template : 'printer.php';
		$invoice_tpl 		= MOD_INVOICES_DIR . SB_DS . 'tpl' . SB_DS . $invoice_tpl_file;
		$this->_dosage 		= $dosage;
		$this->_currencies 	= mb_invoices_get_currencies();
		$tpl_info 			= sb_get_template_info($invoice_tpl);
		$tpl_fields 		= isset($tpl_info['Fields']) ? (array)json_decode($tpl_info['Fields']) : null;
		$copy_number		= 1;
		$logo				= isset($ops->business_logo) && $ops->business_logo ? UPLOADS_DIR . SB_DS . $ops->business_logo : null;
		$logo_url			= null;
		if( is_file($logo) )
		{
			$logo_url = UPLOADS_URL . '/' . $ops->business_logo;
		}
		$current_copies = $_prints = (int)trim($invoice->_prints);
		$store = new SB_MBStore($this->dosage->store_id);
		extract((array)$ops);
		if( SB_Request::getInt('print') )
		{
			$pdf = mb_invoices_get_pdf_instance(__('Invoice', 'invoices'), __('Invoice', 'invoices'));
			/*
			//##check if we already have an invoice buffer in session
			if( $invoice_html = SB_Session::getVar('invoice_pdf_'.$invoice->invoice_id) )
			{
				$pdf->loadHtml(base64_decode($invoice_html));
				$pdf->render();
				$pdf->stream(sprintf(__('invoice-%d-%d.pdf'), $store->store_id, $invoice->invoice_number),
						array('Attachment' => 0, 'Accept-Ranges' => 1));
				die();
			}
			*/
			ob_start();
			include $invoice_tpl;
			$invoice_html = ob_get_clean();
			SB_Session::setVar('invoice_pdf_'.$invoice->invoice_id, base64_encode($invoice_html));
			$pdf = mb_invoices_get_pdf_instance(__('Invoice', 'invoices'), __('Invoice', 'invoices'));
			//$pdf->set_paper('letter');
			$pdf->loadHtml($invoice_html);
			// Render the HTML as PDF
			$pdf->render();
			// Output the generated PDF to Browser
			$pdf->stream(sprintf(__('invoice-%d-%d.pdf'), $store->store_id, $invoice->invoice_number), 
							array('Attachment' => 0, 'Accept-Ranges' => 1));
			die();
		}
		sb_set_view_var('current_copies', $current_copies);
		sb_set_view_var('store', $store);
		sb_set_view_var('copy_number', $copy_number);
		sb_set_view_var('ops', $ops);
		sb_set_view_var('countries', $countries);
		sb_set_view_var('invoice', $invoice);
		sb_set_view_var('dosage', $dosage);
		sb_set_view_var('qr_code', $qr_code);
		sb_set_view_var('invoice_limit_date', $invoice_limit_date);
		sb_set_view_var('invoice_footer_msg', $invoice_footer_msg);
		sb_set_view_var('invoice_tpl', $invoice_tpl);
		sb_set_view_var('logo', $logo);
		sb_set_view_var('logo_url', $logo_url);
	}
	public function task_form_test()
	{
		$code = SB_Request::getString('country');
		$class_file = MOD_INVOICES_DIR . SB_DS . 'countries' . SB_DS . 'class.' . $code . '.php';
		if( !file_exists($class_file) )
		{
			SB_Text::_('The invoice class file does not exists', 'invoices');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$class_name = 'SB_Invoice' . $code;
		require_once $class_file;
		$obj = new $class_name;
		
		sb_set_view_var('title', sprintf("%s %s", SBText::_('Invoice test for', 'invoices'), $obj->GetCountryName()));
		sb_set_view_var('obj', $obj);
		sb_set_view_var('test_url', SB_Route::_('index.php?mod=invoices&task=test_invoice&country=' . $code));
	}
	public function task_test_invoice()
	{
		$code = SB_Request::getString('country');
		if( !$code )
		{
			sb_response_json(array('status' => 'error', 'error' => SB_Text::_('Invalid country code', 'invoices')));
		}
		$obj = sb_mb_invoices_get_country_obj($code);
		try 
		{
			$_POST['dosage'] = base64_decode(trim($_POST['dosage']));
			$res = $obj->buildControlCode(array_map('trim', array_map('trim', $_POST)));
		}
		catch(Exception $e)
		{
			sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
		}
		
		sb_response_json(array('status' => 'ok', 'result' => $res, 'post' => $_POST));
	}
	public function task_search_product()
	{
		$keyword = SB_Request::getString('keyword');
		if( !SB_Module::moduleExists('mb') )
		{
			die();
		}
		$query = "SELECT * FROM mb_products WHERE product_name LIKE '%$keyword%' ORDER BY product_name ASC";
		$items = array();
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$items[] = array('id' => $row->product_id,
					'code'	=> $row->product_code, 
					'name' => $row->product_name, 
					'label' => $row->product_name,
					'price'	=> number_format($row->product_price, 2, '.', '')
			);
		}
		sb_response_json(array('status' => 'ok', 'results' => $items));
	}
	public function task_save_dosage()
	{
		$id = SB_Request::getInt('id');
		$name = SB_Request::getString('dname');
		$data = array(
				'store_id'				=> SB_Request::getInt('dstore_id'),
				'nit'					=> 0,
				'name'					=> htmlentities($name, ENT_QUOTES, 'utf-8'),
				'dosage'				=> base64_decode(SB_Request::getString('ddosage')),
				'authorization'			=> SB_Request::getString('dauth'),
				'emission_limit_date'	=> SB_Request::getDate('dlimitdate'),
				'header_text'			=> htmlentities(SB_Request::getString('dheader_text'), ENT_QUOTES, 'utf-8'),
				'footer_text'			=> htmlentities(SB_Request::getString('dfooter_text'), ENT_QUOTES, 'utf-8'),
				'is_default'			=> SB_Request::getInt('d_is_default'),
				'template'				=> SB_Request::getString('dtemplate', 'default.php')
		);
		if( $data['is_default'] == 1 )
		{
			$this->dbh->Update('mb_invoice_dosages', 
					array('is_default' => 0), 
					array(
							'is_default' => 1, 
							'store_id' => $data['store_id']
					)
			);	
		}
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_invoice_dosages', $data);
		}
		else
		{
			$this->dbh->Update('mb_invoice_dosages', $data, array('id' => $id));
		}
		
		sb_response_json(array('status' => 'ok', 'message' => __('New dosage added', 'invoices')));
	}
	public function task_delete_dosage()
	{
		$id = SB_Request::getInt('id');
		$this->dbh->Delete('mb_invoice_dosages', array('id' => $id));
	}
	public function task_get_dosages()
	{
		$query = "SELECT id.*, s.store_name
					FROM mb_invoice_dosages id
					LEFT JOIN mb_stores s ON s.store_id = id.store_id
					ORDER BY creation_date DESC";
		$res = array(
				'status' 	=> 'ok',
				'dosages'	=> $this->dbh->FetchResults($query)
		);
		sb_response_json($res);
	}
	public function task_report()
	{
		$date_from 	= SB_Request::getDate('date_from');
		$date_to	= SB_Request::getDate('date_to');
		$issued		= SB_Request::getInt('issued');
		$void		= SB_Request::getInt('void');
		$status = '';
		if( $issued && $void )
		{
			$status = null;
		}
		elseif( $issued )
		{
			$status = 'issued';
		}
		elseif( $void )
		{
			$status = 'void';
		}
		$subquery 	= "SELECT name FROM mb_invoice_dosages d WHERE d.id = i.dosage_id";
		$query 		= "SELECT i.*, ($subquery) AS dosage_name FROM mb_invoices i WHERE 1 = 1 ";
		if( $status )
		{
			$query .= "AND i.status = '$status' ";
		}
		$query		.= "AND DATE(invoice_date_time) >= '$date_from' ";
		$query		.= "AND DATE(invoice_date_time) <= '$date_to' ";
		$query		.= "ORDER BY invoice_date_time DESC";
		//var_dump($query);
		if( SB_Request::getInt('print') ) 
		{
			$this->SetView('print.preview');
		}
		else
		{
			$this->SetView('reports');
		}
		
		$this->_items = $this->dbh->FetchResults($query);
		$this->_report_title = sprintf("Reporte de Facturas<br/>Desde: %s - Hasta: %s", sb_format_date($date_from), sb_format_date($date_to));
		$this->_back_link	= SB_Route::_('index.php?mod=invoices');
		if( SB_Request::getInt('print') )
		{
			ob_start();
			include 'views/admin/reports.php';
			$this->_print_content = ob_get_clean();
		}
		
	}
	public function task_settings()
	{
		$ops = (object)sb_get_parameter('invoices_ops', array());
		//print_r($ops);
		if( !defined('COUNTRY_CODE') )
		{
			printf("<h4>%s</h4>", SBText::_('Your country settings are incorrect.', 'invoices'));
			return false;
		}
		$this->templates = mb_invoices_get_templates();
		$show_common_fields = false;
		$query = "SELECT * FROM mb_invoice_dosages ORDER BY creation_date DESC";
		sb_set_view_var('ops', $ops);
		sb_set_view_var('show_common_fields', true);
		$this->dosages = SB_Factory::getDbh()->FetchResults($query);
	}
	public function task_save_settings()
	{
		$ops = SB_Request::getVar('invoices_ops');
		if( !$ops || !is_array($ops))
			return false;
		if( isset($_FILES['logo']) && $_FILES['logo']['size'] > 0 )
		{
			$destination = UPLOADS_DIR . SB_DS . $_FILES['logo']['name'];
			move_uploaded_file($_FILES['logo']['tmp_name'], $destination);
			$ops['business_logo'] = basename($destination);
		}
		sb_update_parameter('invoices_ops', $ops);
		SB_MessagesStack::AddMessage(__('Invoice settings saved', 'invoices'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=invoices&view=settings'));
	}
	public function task_order2invoice()
	{
		if( !sb_get_current_user()->can('create_invoice') )
		{
			lt_die(__('You dont have enough permission to create invoices', 'invoices'));
		}
		$id = SB_Request::getInt('id');
		$order = new SB_MBOrder($id);
		if( !$order->order_id )
		{
			SB_MessagesStack::AddMessage(__('The order identifier does not exists', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$invoice_id = null;
		try 
		{
			$invoice_id = mb_invoices_order2invoice($order, sb_get_current_user()->user_id);
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddMessage($e->getMessage(), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
		}
		
		SB_MessagesStack::AddMessage(__('The invoice has been created and issued.', 'invoices'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
	}
}