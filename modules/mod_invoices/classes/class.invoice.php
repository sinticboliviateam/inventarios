<?php
namespace SinticBolivia\SBFramework\Modules\Invoices\Classes;
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Factory;

/**
 * @property int 	invoice_id
 * @property int 	dosage_id
 * @property int 	customer_id
 * @property string customer
 * @property int 	user_id
 * @property int 	store_id
 * @property int 	nit_ruc_nif
 * @property float 	subtotal
 * @property float 	total
 * @property int 	invoice_number
 * @property string control_code
 * @property string authorization
 * @property string invoice_date_time
 * @property string invoice_limite date
 * @property string curreny_code
 * @property float	exchange_rate
 * @property string status issued|void
 * @property datetime creation_date
 */
class LT_MBInvoice extends SB_ORMObject
{
	protected	$items = array();
	
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		if( !$id )
			return false;
		$query = "SELECT * FROM mb_invoices WHERE invoice_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
		$this->GetDbItems();
		$this->GetDbMeta();
	}
	public function SetDbData($data)
	{
		$this->_dbData = (object)$data;
	}
	public function GetDbItems()
	{
		if( !$this->invoice_id )
			return false;
		$query = "SELECT invoice_id,store_id,product_id,product_code,product_name, price, quantity, total 
					FROM mb_invoice_items 
					WHERE invoice_id = $this->invoice_id";
		$this->items = $this->dbh->FetchResults($query);
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_invoice_meta WHERE invoice_id = $this->invoice_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = $row->meta_value;
		}
	}
	public function GetItems()
	{
		if( !count($this->items) )
			$this->GetDbItems();
		return $this->items;	
	}
	public function SetItems($items)
	{
		$this->items = $items;
	}
	public function AddItem($item)
	{
		$this->items[] = $item;
	}
	public function GetItemsTotalAmount()
	{
		$total = 0;
		foreach($this->GetItems() as $item)
		{
			if( $item->product_id <= 0 ) continue;
			$total += $item->total;
		}
		return $total;
	}
	/**
	 * Save or update an invoice
	 * 
	 * @return int invoice_id
	 */
	public function Save()
	{
		if( !$this->invoice_id )
		{
			//##create a new invoice
			$this->invoice_id = $this->dbh->Insert('mb_invoices', (array)$this->_dbData);
			for($i = 0; $i < count($this->items); $i++)
			{
				$this->items[$i]['invoice_id'] = $this->invoice_id;
			}
			$this->dbh->InsertBulk('mb_invoice_items', $this->items);
		}
		else
		{
			//##update the invoice
			$data	= (array)$this->_dbData;
			unset($data['user_id'], $data['invoice_date_time'], $data['creation_date']);
			$this->dbh->Update('mb_invoices', $data, array('invoice_id' => $this->invoice_id));
			//TODO:update invoice Items
		}
		return $this->invoice_id;
	}
	public static function GetNextInvoiceNumber($dosage_id, $store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT IF(MAX(invoice_number), MAX(invoice_number) + 1, 1) AS NUM ".
					"FROM mb_invoices ".
					"WHERE dosage_id = $dosage_id ";
		if( $store_id )
		{
			$query .= "AND store_id = $store_id ";
		}
		$query .= "LIMIT 1";
		$dbh->Query($query);
		return $dbh->FetchRow()->NUM;
	}
	public static function GetInvoiceByNumber($number, $year = null, $store_id = null)
	{
		$dbh = SB_Factory::GetDbh();
		
		$query = "SELECT * FROM mb_invoices WHERE 1 = 1 AND invoice_number = $number ";
		if( $year && (int)$year )
		{
			$query .= "AND YEAR(invoice_date_time) = '$year' ";
		}
		if( (int)$store_id )
		{
			$query .= "AND store_id = $store_id ";
		}
		//var_dump($query);
		$invoices = array();
		foreach($dbh->FetchResults($query) as $invoice)
		{
			$i = new LT_MBInvoice();
			$i->SetDbData($invoice);
			$i->GetDbMeta();
			$invoices[] = $i;
		}
		return $invoices;
	}
}