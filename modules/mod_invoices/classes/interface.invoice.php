<?php
interface SB_IMBInvoice
{
	public function GetCountryName();
	public function GetCountryCode();
	public function InvoiceFields($ops);
	public function FormTest($url);
	public function buildControlCode($data);
}