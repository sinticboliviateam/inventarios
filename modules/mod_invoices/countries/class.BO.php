<?php
require_once dirname(dirname(__FILE__)) . '/libs/Verhoeff.class.php';
ini_set('display_errors', 1);error_reporting(E_ALL);

class SB_InvoiceBO implements SB_IMBInvoice
{
	protected $_version = 7.0;
	protected $_dosage;
	protected $countryName = 'Bolivia';
	protected $countryCode = 'BO';
	/**
	 * 
	 * @param string $dosage Dosificacion emitida por impuesto nacionales de bolivia
	 */
	public function __construct($dosage = null)
	{
		if( $dosage )	
			$this->_dosage = $dosage;
	}
	public function setDosage($dosage)
	{
		$this->_dosage = $dosage;
	}
	/**
	 * 
	 * $data['authorization_number] -> numero de autorizacion
	 * $data['invoice_number']
	 * $data['nit_ci']
	 * $data['transaction_date'] -> format [Ymd]
	 * $data['transaction_amount']
	 * $data['dosage']
	 * 
	 * @param array $data
	 */
	public function buildControlCode($data)
	{
		if( isset($data['dosage']) )
		{
			$this->_dosage = $data['dosage']; 
		}
		$data['transaction_date'] = str_replace('/', '-', trim($data['transaction_date']));
		$data['transaction_date'] = date('Ymd', strtotime($data['transaction_date']));
		$data['transaction_amount'] = str_replace(',', '.', $data['transaction_amount']);
		$data['transaction_amount'] = round($data['transaction_amount']);
		//print_r($data);
		extract(array_map('trim', $data));
		$invoice_digits = $nit_ci_digits = $transaction_date_digits = $transaction_amount_digits = '';
		$auth_number = $data['authorization_number'].SB_Verhoeff::generate($data['authorization_number'].SB_Verhoeff::generate($data['authorization_number']));
		$invoice_digits = SB_Verhoeff::generate($data['invoice_number']);
		$invoice_digits .= SB_Verhoeff::generate($data['invoice_number'].$invoice_digits);
		$nit_ci_digits = SB_Verhoeff::generate($data['nit_ci']);
		$nit_ci_digits .= SB_Verhoeff::generate($data['nit_ci'].$nit_ci_digits);
		$transaction_date_digits = SB_Verhoeff::generate($data['transaction_date']);
		$transaction_date_digits .= SB_Verhoeff::generate($data['transaction_date'].$transaction_date_digits);
		$transaction_amount_digits = SB_Verhoeff::generate($data['transaction_amount']);
		$transaction_amount_digits .= SB_Verhoeff::generate($data['transaction_amount'].$transaction_amount_digits);
		
		$sum = ($invoice_number . $invoice_digits) + ($nit_ci . $nit_ci_digits) + ($transaction_date . $transaction_date_digits) + 
				($transaction_amount . $transaction_amount_digits);
		$verhoeff_digits = '';
		for($i = 0; $i < 5; $i++)
		{
			$verhoeff_digits .= SB_Verhoeff::generate($sum . $verhoeff_digits);
		} 
		//partir dosificacion segun longitud de digitos verhoeff + 1
		$length = ($verhoeff_digits{0} + 1);
		$strings = array(
			'authorization_str' => substr($this->_dosage, 0, $length)
		);
		$strings['invoice_str'] = substr($this->_dosage, $length, ($verhoeff_digits{1} + 1));
		$strings['nit_ci_str'] = substr($this->_dosage, ($verhoeff_digits{0} + $verhoeff_digits{1} + 2), ($verhoeff_digits{2} + 1));
		$strings['transaction_date_str'] = substr($this->_dosage, ($verhoeff_digits{0} + $verhoeff_digits{1} + $verhoeff_digits{2} + 3), ($verhoeff_digits{3} + 1));
		$strings['amount_str'] = substr($this->_dosage, ($verhoeff_digits{0} + $verhoeff_digits{1} + $verhoeff_digits{2} + $verhoeff_digits{3} + 4), ($verhoeff_digits{4} + 1));
		/*
		print "Numero de factura: {$data['invoice_number']}<b>$invoice_digits</b><br/>";
		print "NIT/CI: {$data['nit_ci']}<b>$nit_ci_digits</b><br/>";
		print "Fecha transaccion: {$data['transaction_date']}<b>$transaction_date_digits</b><br/>";
		print "Monto transaccion: {$data['transaction_amount']}<b>$transaction_amount_digits</b><br/>";
		print "5 digitos verhoeff: $sum<b>$verhoeff_digits</b><br/><br/><br/>";
		
		print "Autorizacion: {$data['authorization_number']}<b>{$strings['authorization_str']}</b><br/>";
		print "Numero de factura: {$data['invoice_number']}$invoice_digits<b>{$strings['invoice_str']}</b><br/>";
		print "NIT/CI: {$data['nit_ci']}$nit_ci_digits<b>{$strings['nit_ci_str']}</b><br/>";
		print "Fecha transaccion: {$data['transaction_date']}$transaction_date_digits<b>{$strings['transaction_date_str']}</b><br/>";
		print "Monto transaccion: {$data['transaction_amount']}$transaction_amount_digits<b>{$strings['amount_str']}</b><br/>";
		*/
		$concatenated_str = ($data['authorization_number'] . $strings['authorization_str']) . 
							($data['invoice_number'] . $invoice_digits . $strings['invoice_str']) . 
							($data['nit_ci'] . $nit_ci_digits . $strings['nit_ci_str']) . 
							($data['transaction_date'] . $transaction_date_digits . $strings['transaction_date_str']) . 
							($data['transaction_amount'] . $transaction_amount_digits . $strings['amount_str']);
		$encryption_key = $this->_dosage . $verhoeff_digits;
		//print $concatenated_str.'<br/>' . $encryption_key . '<br/>';
		$rc4 = str_replace('-', '', strtoupper(allegedRC4($concatenated_str, $encryption_key)));
		//print "rc4: $rc4<br/>";
		//summations
		$ST = 0;
		for($i = 0; $i < strlen($rc4); $i++)
		{
			$ST += ord($rc4{$i});
		}
		//print "ST: $ST<br/>";
		$SP1 = $SP2 = $SP3 = $SP4 = $SP5 = 0;
		//print "rc4 length: " .strlen($rc4) . '<br/>';
		for($i = 0; $i < 5; $i++)
		{
			if( $i == 0 )
			{
				$aux = $i;
				while($aux <= strlen($rc4) - 1)
				{
					$SP1 += ord($rc4{$aux});
					$aux += 5;
				}
			}
			if( $i == 1 )
			{
				$aux = $i;
				while($aux <= strlen($rc4) - 1)
				{
					$SP2 += ord($rc4{$aux});
					$aux += 5;
				}
			}
			if( $i == 2 )
			{
				$aux = $i;
				while($aux <= strlen($rc4) - 1)
				{
					$SP3 += ord($rc4{$aux});
					$aux += 5;
				}
			}
			if( $i == 3 )
			{
				$aux = $i;
				while($aux <= strlen($rc4) - 1)
				{
					$SP4 += ord($rc4{$aux});
					$aux += 5;
				}
			}
			if( $i == 4 )
			{
				$aux = $i;
				while($aux <= strlen($rc4) - 1)
				{
					$SP5 += ord($rc4{$aux});
					$aux += 5;
				}
			}
		}
		//print "SP1: $SP1<br/>SP2: $SP2<br/>SP3: $SP3<br/>SP4: $SP4<br/>SP5: $SP5<br/>";
		$sum = 0;
		$sum += (int)(($ST * $SP1) / ($verhoeff_digits{0} + 1));
		$sum += (int)(($ST * $SP2) / ($verhoeff_digits{1} + 1));
		$sum += (int)(($ST * $SP3) / ($verhoeff_digits{2} + 1));
		$sum += (int)(($ST * $SP4) / ($verhoeff_digits{3} + 1));
		$sum += (int)(($ST * $SP5) / ($verhoeff_digits{4} + 1));
		//print "$sum<br/>";
		//print base64($sum);
		$control_code = allegedRC4(base64($sum), $this->_dosage . $verhoeff_digits);
		return strtoupper($control_code);
		
	}
	public function runTests()
	{
		$csv_file = dirname(__FILE__) . '/5000CasosPruebaCCVer7.csv';
		$fh = fopen($csv_file, 'r');
		$i = 0;
		
		while (($row = fgetcsv($fh, null, "|")) !== FALSE) 
		{
			if($i == 0)
			{
				$i++;
				continue;
			}
			$data = array('authorization_number' => $row[0], 'invoice_number' => $row[1], 
							'nit_ci' => $row[2],
							'transaction_date' => date('Ymd', strtotime($row[3])),
							'transaction_amount' => $row[4]  
			);
			
			$inv = new SB_InvoiceBolivia($row[5]);
			$control_code = $inv->buildControlCode($data);
			if( $control_code == $row[10] )
			{
				print "Pass: $control_code<br/>";
			}
			else 
			{
				print "Error: $control_code<br/>";
				print_r($data);die();
			}
		}
	}
	public function GetCountryName()
	{
		return $this->countryName;
	}
	public function GetCountryCode()
	{
		return $this->countryCode;
	}
	public function InvoiceFields($ops)
	{
		?>
		<fieldset>
			<legend><?php print SB_Text::_('Facturaci&oacute;n Bolivia', 'invoices')?></legend>
			<div class="row-fluid">
				<label class="span3"><?php print SB_Text::_('NIT:'); ?></label>
				<?php print SB_HtmlBuilder::writeInput('text', 'invoice_bo[nit]', @$ops->nit, 'invoice_bo_nit'); ?>
			</div>
			<div class="row-fluid">
				<label class="span3"><?php print SB_Text::_('IVA(%):'); ?></label>
				<?php print SB_HtmlBuilder::writeInput('text', 'invoice_bo[iva]', @$ops->iva, 'invoice_bo_iva'); ?>
			</div>
			<div class="row-fluid">
				<label class="span3"><?php print SB_Text::_('Dosificaci&oacute;n'); ?></label>
				<?php print SB_HtmlBuilder::writeInput('text', 'invoice_bo[dosage]', @$ops->dosage, 'invoice_bo_dosage', 
														array('class' => 'input-xlarge')); ?>
			</div>
			<div class="row-fluid">
				<label class="span3"><?php print SB_Text::_('No. Autorizaci&oacute;n'); ?></label>
				<?php print SB_HtmlBuilder::writeInput('text', 'invoice_bo[authorization_number]', @$ops->authorization_number, 
														'invoice_bo_authorization_number'); ?>
			</div>
		</fieldset>
		<?php 
	}
	public function FormTest($url)
	{
		?>
		<form id="form-invoice-bo" action="" method="post">
			<div class="form-group">
				<label>Autorizacion:</label>
				<input type="text" id="authorization_number" name="authorization_number" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label>No. Factura:</label>
				<input type="number" id="invoice_number" name="invoice_number" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label>NIT:</label>
				<input type="text" id="nit_ci" name="nit_ci" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label>Fecha Emision:</label>
				<input type="text" id="transaction_date" name="transaction_date" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label>Monto Facturado:</label>
				<input type="text" id="transaction_amount" name="transaction_amount" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label>Dosificacion:</label>
				<input type="text" id="dosage" name="_dosage" value="" class="form-control" />
			</div>
			<h4 style="border:1px solid #888; margin:0 0 10px 0;padding:10px;font-size:20px;text-align:center;display:none;" id="control-code"></h4>
			<div class="form-group">
				<a href="javascript:;" onclick="window.history.back();" class="btn btn-danger">Cancelar</a>
				<button type="submit" class="btn btn-primary">Generar codigo</button>
			</div>
		</form>
		<script>
		jQuery(function()
		{
			jQuery('#authorization_number').bind('paste', function(e)
			{
				e.preventDefault();
				var text = e.originalEvent.clipboardData.getData('text');
				var data = text.split(/\s+/);
				jQuery('#authorization_number').val(data[0]);
				jQuery('#invoice_number').val(data[1]);
				jQuery('#nit_ci').val(data[2]);
				jQuery('#transaction_date').val(data[3]);
				jQuery('#transaction_amount').val(data[4]);
				jQuery('#dosage').val(data[5]);
			});
			jQuery('#form-invoice-bo').submit(function()
			{
				var params = jQuery('#form-invoice-bo').serialize();
				params += '&dosage=' + btoa(jQuery('#dosage').val().trim());
				jQuery.post('<?php print $url; ?>', params, function(res)
				{
					if( res.status == 'ok' )
					{
						jQuery('#control-code').html(res.result).css('display', 'block');
					}
					else
					{
						alert(res.error);
					}
					jQuery('#authorization_number, #invoice_number, #nit_ci, #transaction_date, #transaction_amount, #dosage').val('');
				});
				return false;
			});
		});
		</script>
		<?php
	}
}
/*
$inv = new SB_InvoiceBolivia();
$inv->runTests();
*/
/*
$i = new SB_InvoiceBolivia('9rCB7Sv4X29d)5k7N%3ab89p-3(5[A');
print $i->buildControlCode(array('authorization_number' => 29040011007, 
									'invoice_number' => 1503, 
									'nit_ci' => 4189179011, 
									'transaction_date' => 20070702,
									'transaction_amount' => 2500
							)
		);
*/
