/**
 * 
 */
function SBInvoice()
{
	var item_tpl = '<div class="trow">'+
						'<div class="cell inv-item-remove"><a href="javascript:;" class="btn btn-default btn-sm remove-item"><span class="glyphicon glyphicon-trash"></span></a></div>' +
						'<div class="cell inv-item-number">{number}<input type="hidden" name="items[{index}][id]" value="{id}" /></div>' +
						'<div class="cell inv-item-code"><input type="text" name="items[{index}][code]" value="{code}" class="form-control" /></div>' +
						'<div class="cell inv-item-name">{name}<input type="hidden" name="items[{index}][name]" value="{name}" /></div>' +
						'<div class="cell inv-item-qty"><input type="number" min="1" name="items[{index}][qty]" value="1" class="form-control item-qty" /></div>' +
						'<div class="cell inv-item-price"><input type="text" name="items[{index}][price]" value="{price}" class="form-control item-price" autocomplete="off" /></div>' +
						'<!-- <div class="cell inv-item-tax"><input type="text" name="items[{index}][tax]" value="{tax}" class="form-control item-tax" /></div> -->' +
						'<div class="cell inv-item-total"><span class="item-total">{total}</span></div>' +
					'</div>';
	var $this 	= this;
	var tax		= jQuery('#tax');
	var table	= jQuery('#invoice-table');
	var search 	= jQuery('#search_product');
	var form	= jQuery('#form-invoice');
	var doing_ajax = false;

	this.AddItem = function(prod)
	{
		if( !prod && search.val().trim().length <= 0 )
		{
			return false;
		}
		/*
		if( !prod && search.val().trim().length > 0 )
		{
			
		}
		*/
		//console.log('search_val');
		//console.log(search.val());
		var sb_product = prod || window.sb_product;
		console.log('sb_product');
		console.log(sb_product);
		//##get total rows in table
		var rows = table.find('.body').find('.trow').length;
		//##replace data into table row template
		var row = item_tpl.replace(/{index}/g, rows)
							.replace('{number}', rows + 1)
							//.replace('{code}', rows + 1)
							.replace(/{name}/g, sb_product ? sb_product.product_name : search.val());
		
		//##check if product is selected
		if( sb_product )
		{
			console.log(sb_product);
			row = row.replace('{id}', sb_product.product_id ? sb_product.product_id : 0)
						.replace('{price}', sb_product.price)
						.replace('{code}', sb_product.code)
						.replace('{tax}', 0.00)
						.replace('{total}', sb_product.price);
		}
		else
		{
			row = row.replace('{id}', 0)
						.replace('{price}', '0.00')
						.replace('{code}', '')
						.replace('{tax}', '0.00')
						.replace('{total}', '0.00');
		}
		table.find('.body').append(row);
		search.val('').focus();
		$this.CalculateTotals();
	};
	this.RemoveItem = function(e)
	{
		//this.parentNode.parentNode.remove();
		//return false;
		var tr 				= jQuery(this.parentNode.parentNode);
		var current_index 	= tr.index();
		var new_index 		= current_index;
		tr.nextAll().each(function(i, ntr)
		{
			var inputs = jQuery(ntr).find('input');
			inputs.each(function(ii, input)
			{
				if(input.name.indexOf('item') != -1)
				{
					input.name = input.name.replace(/\[\d+\]/, '['+new_index+']')
				}
			});
			new_index++;
		});
		tr.remove();
		$this.CalculateTotals();
		return false;
	};
	this.OnQtyChanged = function(e)
	{
		if( e.type == 'change' || e.type == 'keyup' )
		{
			if( e.type == 'keyup' && e.keyCode != 13 )
			{
				return false;
			}
			
			var row = jQuery(this).parents('.trow:first');
			$this.CalculateRowTotal(row);
			$this.CalculateTotals();
		}
	};
	this.OnPriceChanged = function(e)
	{
		if( e.keyCode != 13 )
		{
			return false;
		}
		var row = jQuery(this).parents('.trow:first');
		$this.CalculateRowTotal(row);
		$this.CalculateTotals();
	};
	this.CalculateRowTotal = function(row)
	{
		var qty = parseInt(row.find('.item-qty:first').val());
		var price = parseFloat(row.find('.item-price:first').val());
		var tax = 0;//parseFloat(row.find('.item-tax:first').val());
		var total = (qty * price) + tax;
		row.find('.item-total:first').html(total.toFixed(2));
		
		return total;
	};
	this.CalculateTotals = function()
	{
		var tax_rate 	= parseInt(tax.val()) / 100;
		var rows 		= jQuery('#invoice-table .body .trow');
		var subtotal 	= 0;
		var total_tax 	= 0;
		var total 		= 0;
		jQuery.each(rows, function(i, row)
		{
			total += $this.CalculateRowTotal(jQuery(row));
		});
		subtotal = total;
		if( !isNaN(tax_rate) && tax_rate > 0 )
		{
			total_tax 	= total * tax_rate;
			subtotal 	= total - total_tax;
		}
		jQuery('#invoice-subtotal').html(subtotal.toFixed(2));
		jQuery('#invoice-tax').html(total_tax.toFixed(2));
		jQuery('#invoice-total').html(total.toFixed(2));
	}
	this.Save = function()
	{
		var customer = jQuery('#customer').val().trim();
		var nit		= parseInt(jQuery('#nit_ruc_nif').val().trim());
		if( customer.length <= 0 )
		{
			alert(lt.modules.invoices.locale.CUSTOMER_NAME_NEEDED);
			return false;
		}
		if( nit.toString().length <= 0 )
		{
			alert(lt.modules.invoices.locale.CUSTOMER_NIT_NEEDED);
			return false;
		}
		if( isNaN(nit) )
		{
			alert(lt.modules.invoices.locale.CUSTOMER_INVALID_NIT);
			return false;
		}
		var params = form.serialize();
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				jQuery('#btn-print-invoice').attr('href', res.print_url);
				jQuery('#success-dialog .modal-body').html(res.message)
				jQuery('#success-dialog').modal('show');
			}
			else
			{
				alert(res.error);
			}
		});
	};
	this.GetCustomer = function(nit)
	{
		if( doing_ajax )
			return false;
		if( parseInt(nit) <= 0 )
			return false;
		doing_ajax = true;
		jQuery.get('index.php?mod=customers&task=get_customer&by=nit&ajax=1&id=' + nit, function(res)
		{
			if( res.status == 'ok' )
			{
				if( res.customer )
				{
					var customer_name = '';
					if(res.customer._last_sale_name)
					{
						customer_name = res.customer._last_sale_name;
					}
					else if(res.customer.first_name)
					{
						customer_name = res.customer.first_name + (res.customer.last_name ? res.customer.last_name : ''); 
					}
					jQuery('#customer_id').val(res.customer.customer_id);
					jQuery('#customer').val(customer_name);
				}
			}
			else
			{
				alert(res.error);
			}
			doing_ajax = false;
		});
	}
	function setEvents()
	{
		if( lt.modules.invoices.mod_customers_exists )
		{
			jQuery('#nit_ruc_nif').on('keyup blur', function(e)
			{
				if( e.type == 'blur' || e.keyCode == 13 )
				{
					$this.GetCustomer(this.value);	
					jQuery('#customer').focus();
				}
			});
		}
		jQuery('#search_product').keyup(function(e)
		{
			if( e.keyCode != 13)
				return false;
			$this.AddItem();
		});
		jQuery('#tax').change(function()
		{
			if( this.value > 0 )
				$this.CalculateTotals();
		});
		jQuery('#btn-add-item').click(function()
		{
			$this.AddItem(null);
		});
		jQuery(document).on('click', '.remove-item', $this.RemoveItem);
		jQuery(document).on('keyup change', '.item-qty', $this.OnQtyChanged);
		jQuery(document).on('keyup', '.item-price', $this.OnPriceChanged);
		jQuery('#btn-save').click($this.Save);
		jQuery('.btn-select-dosage').click(function(e)
		{
			console.log(this.dataset);
			jQuery('#dosage_id').val(this.dataset.id);
			jQuery('#dosage').val(this.dataset.name);
			jQuery('.dosage-tpl-options').css('display', 'none');
			jQuery('#dosage-tpl-'+this.dataset.tpl).css('display', 'block');
			jQuery('#modal-dosages').modal('hide');
			jQuery(document).trigger('mb_invoices_select_dosage');
			return false;
		});
		jQuery('#customer').keyup(function(e)
		{
			if( e.keyCode == 13 )
			{
				jQuery('#search_product').focus();
			}
		});
	};
	setEvents();
}
jQuery(function()
{
	window.sb_invoice = new SBInvoice();
	var completion = new SBCompletion({
		input: document.getElementById('search_product'),
		url: lt.modules.invoices.completion_url,
		callback: function(sugesstion)
		{
			window.sb_product = jQuery(sugesstion).data('obj');
			//console.log(sugesstion);
			//console.log(jQuery(sugesstion).data('obj'));
		}
	});
});