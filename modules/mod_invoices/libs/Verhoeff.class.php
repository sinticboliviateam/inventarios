<?php
class SB_Verhoeff
{
	static public $d = array(
			array(0,1,2,3,4,5,6,7,8,9),
			array(1,2,3,4,0,6,7,8,9,5),
			array(2,3,4,0,1,7,8,9,5,6),
			array(3,4,0,1,2,8,9,5,6,7),
			array(4,0,1,2,3,9,5,6,7,8),
			array(5,9,8,7,6,0,4,3,2,1),
			array(6,5,9,8,7,1,0,4,3,2),
			array(7,6,5,9,8,2,1,0,4,3),
			array(8,7,6,5,9,3,2,1,0,4),
			array(9,8,7,6,5,4,3,2,1,0)
	);
	
	static public $p = array(
			array(0,1,2,3,4,5,6,7,8,9),
			array(1,5,7,6,2,8,3,0,9,4),
			array(5,8,0,3,7,9,6,1,4,2),
			array(8,9,1,6,0,4,3,5,2,7),
			array(9,4,5,3,1,2,6,8,7,0),
			array(4,2,8,6,5,7,3,9,0,1),
			array(2,7,9,3,8,0,6,4,1,5),
			array(7,0,4,6,9,1,3,2,5,8)
	);
	
	static public $inv = array(0,4,3,2,1,5,6,7,8,9);
	
	static function calc($num) 
	{
		if(!preg_match('/^[0-9]+$/', $num)) {
			throw new \InvalidArgumentException(sprintf("Error! Value is restricted to the number, %s is not a number.",
					$num));
		}
	
		$r = 0;
		foreach(array_reverse(str_split($num)) as $n => $N) {
			$r = self::$d[$r][self::$p[($n+1)%8][$N]];
		}
		return self::$inv[$r];
	}
	
	static function check($num) 
	{
		if(!preg_match('/^[0-9]+$/', $num)) {
			throw new \InvalidArgumentException(sprintf("Error! Value is restricted to the number, %s is not a number.",
					$num));
		}
	
		$r = 0;
		foreach(array_reverse(str_split($num)) as $n => $N) {
			$r = self::$d[$r][self::$p[$n%8][$N]];
		}
		return $r;
	}
	/**
	 * Get digit
	 * @param int $num
	 * @return string
	 */
	static function generate($num) 
	{
		//return sprintf("%s%s", $num, self::calc($num));
		return self::calc($num);
	}
	
	static function validate($num) {
		return self::check($num) === 0;
	}
}
//die("digits:".SB_Verhoeff::generate(15031));
/*
 * RC4 symmetric cipher encryption/decryption
*
* @license Public Domain
* @param string key - secret key for encryption/decryption
* @param string str - string to be encrypted/decrypted
* @return string
*/
function rc4($key, $str) 
{
	$s = array();
	for ($i = 0; $i < 256; $i++) {
		$s[$i] = $i;
	}
	$j = 0;
	for ($i = 0; $i < 256; $i++) {
		$j = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
		$x = $s[$i];
		$s[$i] = $s[$j];
		$s[$j] = $x;
	}
	$i = 0;
	$j = 0;
	$res = '';
	for ($y = 0; $y < strlen($str); $y++) {
		$i = ($i + 1) % 256;
		$j = ($j + $s[$i]) % 256;
		$x = $s[$i];
		$s[$i] = $s[$j];
		$s[$j] = $x;
		$res .= $str[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
	}
	return $res;
}
function allegedRC4($message, $key)
{
	$state = array();
	$x = $y = $index1 = $index2 = $NMen = 0;
	$i = null;
	$MensajeCifrado = '';
	for($i = 0;$i < 256; $i++){$state[] = $i;}
	for($i = 0;$i < 256; $i++)
	{
		$index2 = (ord($key{$index1}) + $state[$i] + $index2) % 256;
		list($state[$index2], $state[$i]) = array( $state[$i], $state[$index2] );
		$index1 = ($index1 + 1) % strlen($key);
	}
	for($i = 0; $i < strlen($message); $i++)
	{
		$x = ($x + 1) %	256;
		$y = ($state[$x] + $y) % 256;
		list($state[$y], $state[$x]) = array( $state[$x] , $state[$y] );
		$NMen = ord($message[$i]) ^ $state[($state[$x] + $state[$y]) % 256];
		//$MensajeCifrado = $MensajeCifrado . "-" . RellenaCero(ConvierteAHexadecimal(NMen));
		$MensajeCifrado .= "-" . ((strlen(dechex($NMen)) == 1) ? '0'.dechex($NMen) : dechex($NMen));
	}
	
	//return substr($MensajeCifrado, 1, strlen($MensajeCifrado) - 1);
	return substr($MensajeCifrado, 1);
}
function base64($number)
{
	$diccionario = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
						'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
						'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
						'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
						'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
						'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
						'y', 'z', '+', '/' );
	$cociente = 1;
	$Resto = 0;
	$Palabra = '';
	while($cociente > 0)
	{
		$cociente = $number / 64;
		$Resto = $number % 64;
		$Palabra = $diccionario[$Resto] . $Palabra;
		$number = $cociente;
	}
	return ltrim($Palabra, '0');
}