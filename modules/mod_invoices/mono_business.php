<?php
namespace SinticBolivia\MonoBusiness\Modules\Invoices;
use SinticBolivia\SBFramework\Classes\SB_Object;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Modules\Customers\Classes\SB_MBCustomer;
use SinticBolivia\SBFramework\Modules\Invoices\Classes\LT_MBInvoice;
use \SB_InvoiceBO;

class LT_ModuleInvoicesMonoBusiness extends SB_Object
{
	public function __construct()
	{
		SB_Module::add_action('mb_order_info_fields', array($this, 'action_mb_order_info_fields'));
		SB_Module::add_action('mb_pos_register_sale', array($this, 'action_mb_pos_register_sale'));
		SB_Module::add_action('mb_pos_sale_response', array($this, 'action_mb_pos_sale_response'));
		SB_Module::add_action('mb_settings_tab', array($this, 'action_mb_settings_tab'));
		SB_Module::add_action('mb_settings_pane', array($this, 'action_mb_settings_pane'));
		SB_Module::add_action('mb_save_settings', array($this, 'action_mb_save_settings'));
		SB_Module::add_action('mb_pos_javascript', array($this, 'action_mb_pos_javascript'));
		SB_Module::add_action('mb_order_cancelled', array($this, 'action_mb_order_cancelled'));
	}
	public function action_mb_settings_tab($ops)
	{
		?>
		<li><a href="#billing" data-toggle="tab"><?php _e('Billing', 'invoices'); ?></a></li>
		<?php 
	}
	public function action_mb_settings_pane()
	{
		$ops = (object)sb_get_parameter('invoices_ops', array());
		//print_r($ops);
		if( !defined('COUNTRY_CODE') )
		{
			printf("<h4>%s</h4>", SBText::_('Your country settings are incorrect.', 'invoices'));
			return false;
		}
		$this->templates = mb_invoices_get_templates();
		$show_common_fields = false;
		$query = "SELECT id.*,s.store_name 
					FROM mb_invoice_dosages id
					LEFT JOIN mb_stores s ON s.store_id = id.store_id 
					ORDER BY creation_date DESC";
		$this->dosages = SB_Factory::getDbh()->FetchResults($query);
		require_once 'views/admin/settings.php';
	}
	public function action_mb_save_settings($mb_ops)
	{
		$ops = SB_Request::getVar('invoices_ops');
		if( !$ops || !is_array($ops))
			return false;
		$ops = array_merge($ops, $mb_ops);
		sb_update_parameter('invoices_ops', $ops);
	}
	/**
	 * Hook into POS to register the tax rate to use
	 * 
	 */
	public function action_mb_pos_javascript()
	{
		$tax = mb_get_default_tax();
		if( !$tax )
			return false;
		?>
		window.pos_tax	= <?php print json_encode($tax); ?>;
		window.pos_rate = <?php print $tax->rate; ?>;
		jQuery(function()
		{
			jQuery('#pos-tax-rate').html('<?php print $tax->rate; ?>%');
		});
		<?php 
	}
	/**
	 * Hook into POS register_sale to create an invoice using default dosage
	 * 
	 * This hook is valid only for Bolivia country
	 * 
	 * @param SB_MBOrder $order
	 * @return LT_MBInvoice
	 */
	public function action_mb_pos_register_sale($order)
	{
		//##get billing options
		$ops = (object)sb_get_parameter('invoices_ops', array());
		if( $ops->business_country != 'BO' )
			return false;
		$customer = new SB_MBCustomer($order->customer_id);
		require_once MOD_INVOICES_DIR . SB_DS . 'countries' . SB_DS . 'class.BO.php';
		//##get dosage
		$dosage			= mb_invoices_get_default_dosage($order->store_id);
		if( !$dosage )
			return false;
		
		$invoice_number = LT_MBInvoice::GetNextInvoiceNumber($dosage->id, $order->store_id);
		$country 		= new SB_InvoiceBO();
		$control_code 	= $country->buildControlCode(array(
				'authorization_number'	=> $dosage->authorization,
				'invoice_number'		=> $invoice_number,
				'nit_ci'				=> $customer->_nit_ruc_nif ? $customer->_nit_ruc_nif : 0,
				'transaction_date'		=> $order->order_date,
				'transaction_amount'	=> $order->total,
				'dosage'				=> $dosage->dosage
		));
		//##set invoice data
		$data = array(
				'dosage_id'				=> $dosage->id,
				'customer_id'			=> $order->customer_id,
				'customer'				=> $customer->_billing_name ? $customer->_billing_name : sprintf("%s %s", $customer->first_name, $customer->last_name),
				'user_id'				=> (int)sb_get_current_user()->user_id,
				'store_id'				=> $order->store_id,
				'nit_ruc_nif'			=> $customer->_nit_ruc_nif ? $customer->_nit_ruc_nif : 0,
				'tax_rate'				=> 0,
				'subtotal'				=> $order->subtotal,
				'total' 				=> $order->total,
				'cash'					=> 0,
				'invoice_number'		=> $invoice_number,
				'control_code'			=> $control_code,
				'authorization'			=> $dosage->authorization,
				'invoice_date_time'		=> date('Y-m-d H:i:s'),
				'invoice_limite_date'	=> $dosage->emission_limit_date,
				'currency_code'			=> SB_Request::getString('currency'),
				'exchange_rate'			=> SB_Request::getFloat('exchange_rate', 1),
				'status'				=> 'issued',
				'creation_date'  		=> date('Y-m-d H:i:s')
		);
		$invoice_items = array();
		foreach($order->GetItems() as $i)
		{
			$invoice_items[] = array(
					//'invoice_id'	=> 0,
					'store_id'		=> $order->store_id,
					'product_id'	=> $i->product_id,
					'product_name'	=> $i->name,
					'price'			=> $i->price,
					'quantity'		=> $i->quantity,
					'total'			=> $i->total,
					'creation_date'	=> date('Y-m-d H:i:s')
			);
		}
		//##insert the invoice
		$invoice = mb_invoices_insert_new($data, $invoice_items);
		mb_add_order_meta($order->order_id, '_invoice_id', $invoice->invoice_id);
		//mb_invoice_add_meta($invoice->invoice_id, '_billing_name', $customer->_billing_name);
		foreach(SB_Request::getVar('meta', array()) as $meta_key => $meta_value)
		{
			SB_Meta::addMeta('mb_invoice_meta', $meta_key, $meta_value, 'invoice_id', $invoice->invoice_id);
		}
		return $invoice;
	}
	public function action_mb_pos_sale_response($res)
	{
		$btn_text = __('Print Invoice', 'invoices');
		$invoice_id = (int)SB_Meta::getMeta('mb_order_meta', '_invoice_id', 'order_id', $res['order_id']);
		if( !$invoice_id )
		{
			$res['invoice_error'] = __('No invoice was not generated, review dosages', 'invoices');
			SB_MessagesStack::AddMessage($res['invoice_error'], 'info');
			return $res;
		}
		//$invoice_url = SB_Route::_('index.php?tpl_file=module&mod=invoices&view=view&id='.$invoice_id);
		$invoice_url = SB_Route::_('index.php?mod=invoices&view=view&print=1&id='.$invoice_id);
		$js_code =<<<JS
var a = document.createElement('a');
a.href = '$invoice_url';
a.target = '_blank';
a.className = 'btn btn-primary';
a.innerHTML = '$btn_text';
jQuery('#success-buttons').append(a);
JS;
		if( isset($res['eval']) )
		{
			$res['eval'] .= $js_code;
		}
		else 
		{
			$res['eval'] = $js_code;
		}
		$res['invoice_url'] = $invoice_url;
		return $res;
	}
	public function action_mb_order_info_fields($order)
	{
		$user = sb_get_current_user();
		if( !$order )
		{
			return false;
		}
		?>
		<fieldset>
			<legend><?php _e('Billing', 'invoices'); ?></legend>
			<?php if( !$order->_invoice_id ): ?>
			<p>
				<?php _e('The order has no invoice issued.', 'invoices'); ?>
				<a href="<?php print SB_Route::_('index.php?mod=invoices&task=order2invoice&id='.$order->order_id); ?>" class="btn btn-default" 
					title="<?php _e('Issue Invoice', 'invoices'); ?>">
					<span class="glyphicon glyphicon-cog"></span>
				</a>
			</p>
			<?php else: ?>
			<?php
			$invoice = new LT_MBInvoice($order->_invoice_id);
			?>
			<div class="form-group clearfix">
				<label class="col-sm-2 control-label"><?php _e('Invoice Number:', 'invoices'); ?></label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="" value="<?php print $invoice->invoice_number; ?>" readonly="readonly" />
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<a href="<?php print SB_Route::_('index.php?mod=invoices&view=view&id='.$invoice->invoice_id)?>"
							target="_blank"
							class="btn btn-default btn-sm"
							title="<?php _e('View Invoice', 'invoices'); ?>">
							<span class="glyphicon glyphicon-eye-open"></span></a>
					</div>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2 control-label"><?php _e('Billing Name:', 'invoices'); ?></label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="" value="<?php print $invoice->_billing_name ? $invoice->_billing_name : $invoice->customer; ?>" readonly="readonly" />
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-2 control-label"><?php _e('Control Code:', 'invoices'); ?></label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="" value="<?php print $invoice->control_code; ?>" readonly="readonly" />
				</div>
			</div>
			<?php endif; ?>
		</fieldset>
		<?php 
	}
	public function action_mb_order_cancelled($order, $revert_kardex)
	{
		$invoice_id = $order->_invoice_id;
		if( !$invoice_id )
		{
			return false;
		}
		//##change invoice status to void
		SB_Factory::getDbh()->Update('mb_invoices', array('status' => 'void'), array('invoice_id' => $invoice_id));
	}
}
