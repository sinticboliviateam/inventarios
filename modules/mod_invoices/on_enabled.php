<?php
SB_Language::loadLanguage(LANGUAGE, 'invoices', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('invoices');
$dbh = SB_Factory::getDbh();
$permissions = array(
		array('group' => 'invoices', 'permission' => 'manage_invoices', 'label'	=> SB_Text::_('Manage Invoices', 'invoices')),
		array('group' => 'invoices', 'permission' => 'create_invoice', 'label'	=> SB_Text::_('Create Invoice', 'invoices')),
		array('group' => 'invoices', 'permission' => 'edit_invoice', 'label'	=> SB_Text::_('Edit Invoice','invoices')),
		array('group' => 'invoices', 'permission' => 'view_invoice', 'label'	=> SB_Text::_('View Invoice','invoices')),
		array('group' => 'invoices', 'permission' => 'delete_invoice', 'label'	=> SB_Text::_('Delete Invoice', 'invoices')),
		array('group' => 'invoices', 'permission' => 'print_invoice', 'label'	=> SB_Text::_('Print Invoice', 'invoices')),
		array('group' => 'invoices', 'permission' => 'view_all_invoices', 'label'	=> SB_Text::_('View All Invoices', 'invoices')),
		array('group' => 'invoices', 'permission' => 'void_invoice', 'label'	=> SB_Text::_('Void Invoice', 'invoices')),
		array('group' => 'invoices', 'permission' => 'build_invoice_reports', 'label'	=> SB_Text::_('Build Invoice Reports', 'invoices')),
		array('group' => 'invoices', 'permission' => 'manage_invoices_settings', 'label' => __('Manage Invoices Settings', 'invoices')),
		array('group' => 'invoices', 'permission' => 'report_sales_book', 'label' => __('Build Sales Book Report', 'invoices'))
);
sb_add_permissions($permissions);