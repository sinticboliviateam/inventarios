CREATE TABLE IF NOT EXISTS mb_invoice_dosages(
	id						bigint not null auto_increment primary key,
	store_id				integer not null,
	nit						varchar(128) not null,
	name					varchar(512),
	dosage					varchar(512),
	authorization			varchar(128),
	emission_limit_date		date,
	header_text				text,
	footer_text				text,
	template				varchar(128),
	is_default				tinyint(1) default 0,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_invoices(
	invoice_id				bigint not null auto_increment primary key,
	dosage_id				bigint not null,
	customer_id				bigint not null,
	customer				varchar(256),
	user_id					integer not null,
	store_id				integer not null,
	nit_ruc_nif				bigint unsigned not null,
	tax_id					integer,
	tax_rate				decimal(10,2),
	subtotal				decimal(10,2),
	total_tax				decimal(10,2),
	total					decimal(10,2),
	cash					decimal(10,2),
	money_back				decimal(10,2),
	invoice_number			bigint not null,
	dosage					varchar(512),
	control_code			varchar(128),
	authorization			varchar(128),
	invoice_date_time		datetime,
	invoice_limite_date		date,
	currency_code			varchar(16),
	exchange_rate			decimal(10,2),
	status					varchar(64),
	creation_date			datetime			
);
CREATE TABLE IF NOT EXISTS mb_invoice_meta(
	meta_id					bigint not null auto_increment primary key,
	invoice_id				bigint not null,
	meta_key				varchar(128),
	meta_value				text,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_invoice_items(
	item_id					integer not null auto_increment primary key,
	invoice_id				integer not null,
	store_id				integer not null,
	product_id				integer not null,
	product_code			varchar(64),
	product_name			varchar(256),
	price					decimal(10,2),
	quantity				integer not null default 1,
	total					decimal(10,2),
	creation_date			datetime
);
