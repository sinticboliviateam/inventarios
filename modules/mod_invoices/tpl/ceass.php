<?php
/**
 * Template name: Ceass
 */
$invoice_title = '';
?>
<style type="text/css">
#print-preview{width:100%;}
#invoice{margin:0;padding:0;width:100%;height:auto;page-break-after: always !important;}
#invoice *{font-family:Helvetica;font-size:11px;}
#invoice #header{overflow:hidden;}
#business-data
{
	float:left;width:50%;
}
#the-data{width:60%;text-align:center;}
#title{width:100%;font-weight:bold;text-transform: uppercase;text-align:center;clear:both;font-size:20px;}
#the-invoice-data{border:1px solid #000;padding:10px;width:60%;float:right;}
.invoice-items{width:100%;border:1px solid #000;border-collapse: collapse;}
.items-cell{border:1px solid #000;padding:5px;}
.item-qty, .item-price{width:14%;text-align:center;}
.item-product{width:47%;}
.item-total{width:20%;text-align:right;}
.total-text,.invoice-total{font-weight: bold;text-align:right;}
#limit-data{float:left:50%;}
#qr-code{text-align:right;}
#invoice-footer{clear:both;text-align:center;}
#invoice-info td{padding:4px;}
.text-center{text-align:center;}
#order-items thead th, #order-items tbody td{border:1px solid #000;padding:2px;}
</style>
<script type="text/php"> 
if ( isset($pdf) ) 
{ 
	$font = Font_Metrics::get_font("verdana");
  	$size = 6; 
  	$color = array(0,0,0); 
  	$text_height = Font_Metrics::get_font_height($font, $size); 

  	$foot = $pdf->open_object(); 

  	$w = $pdf->get_width(); 
  	$h = $pdf->get_height(); 

  	// Draw a line along the bottom 
  	$y = $h - $text_height - 24; 
  	$pdf->line(16, $y, $w - 16, $y, $color, 0.5); 

  	$pdf->close_object(); 
  	$pdf->add_object($foot, "all"); 

  	$text = "Page {PAGE_NUM} of {PAGE_COUNT}"; 

  	// Center the text 
  	$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size); 
  	$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color); 
} 
</script> 
<?php 
ob_start();
?>
<div id="invoice" style="">
	<div id="header">
		<table style="width:100%;">
		<tr>
			<td style="width:33.33%;">
				<?php if($logo_url): ?>
				<div>
					<img src="<?php print $logo_url; ?>" alt="" style="width:220px;" />
				</div>
				<?php endif; ?>
			</td>
			<td style="width:33.33%;">
				<div style="text-align:center;">
					<?php print $ops->business_owner; ?><br/>
					<?php print SBText::_('Main House', 'invoices'); ?><br/>
					<?php print $business_address; ?><br/>
					<?php printf("%s%s - Fax: %s", SBText::_('Phone:', 'invoices'), $business_phone, isset($business_fax) ? $business_fax : $business_phone); ?><br/>
					<?php printf("%s - %s", $business_city, $countries[$business_country]); ?><br/>
					Sucursal No: <?php print $store->store_id; ?><br/>
					<?php print $store->store_address; ?><br/>
					Telf: <?php print $store->phone; ?>
				</div>
			</td>
			<td style="width:33.33%;">
				<div style="border:1px solid #000;padding:10px;">
					<table style="width:100%;">
					<tr>
						<td><b>NIT:</b></td>
						<td><?php print $business_nit_ruc_nif; ?></td>
					</tr>
					<tr>
						<td><b><?php print SBText::_('Invoice No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->invoice_number; ?></td>
					</tr>
					<tr>
						<td><b><?php print SBText::_('Authorization No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->authorization; ?></td>
					</tr>
					</table>
				</div>
				<p style="clear:both;text-align:center;">
					<b style="font-size:18px;">{titulo}</b>
				</p>
				<div style="clear:both;">
					<?php print $this->dosage->header_text; ?>
				</div>
			</td>
		</tr>
		</table>
	</div><!-- end id="header" -->
	<h3 id="title" style="text-align:center;text-transform: uppercase;font-size:20px;margin:0 0 5px 0;"><?php print SBText::_('Invoice', 'invoices'); ?></h3>
	<div id="customer-info">
		<table style="width:100%;">
		<tr>
			<td style="width:60%;">
				<table style="width:100%;">
				<tr>
					<td style="width:25%;"><b><?php print SBText::_('Date:', 'invoices'); ?></b></td>
					<td style="width:75%;text-align:left;"><?php print sb_format_date($invoice->invoice_date_time); ?></td>
				</tr>
				<tr>
					<td style="width:25%;"><b>Nombre a Facturar:</b></td>
					<td style="width:75%;text-align:left;"><?php print $invoice->_billing_name; ?></td>
				</tr>
				</table>
			</td>
			<td style="width:40%;">
				<table style="width:100%;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="width:60px;"><b><?php print SBText::_('NIT:', 'invoices'); ?></b></td>
					<td><?php print $invoice->nit_ruc_nif; ?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<div>&nbsp;</div>
	<table id="order-items" style="width:100%;border-collapse: collapse;">
	<thead>
	<tr>
		<th style="text-align:center;">Nro.</th>
		<th class="item-product items-cell" style="text-align:center;"><?php _e('Concept', 'invoices'); ?></th>
		<th class="item-qty items-cell">Cantidad</th>
		<th class="item-price items-cell">P. Unitario</th>
		<th class="item-total text-center items-cell"><?php _e('Total', 'invoices'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $count = 1; foreach($invoice->GetItems() as $item): ?>
	<tr>
		<td style="text-align:center;"><?php print $count; ?></td>
		<td class="item-product items-cell"><?php print $item->product_name; ?></td>
		<td class="item-qty items-cell"><?php print $item->quantity; ?></td>
		<td class="item-price items-cell"><?php print number_format($item->price, 2, '.', ','); ?></td>
		<td class="item-total items-cell"><?php print number_format($item->total, 2, '.', ','); ?></td>
	</tr>
	<?php $count++; endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="3" style="border-left:1px solid #fff;border-right:1px solid #000;">&nbsp;</td>
		<td class="total-text items-cell"><?php _e('Total:', 'invoices'); ?></td>
		<td class="invoice-total items-cell"><?php print number_format($invoice->total, 2, '.', ','); ?></td>
	</tr>
	</tfoot>
	</table>
	<br/>
	<table style="width:100%">
	<tr>
		<td style="width:70%;">
			<table style="width:100%;">
			<tr>
				<td style="width:150px;"><b><?php print SBText::_('Are:', 'invoices') ?></b></td>
				<td><?php print sb_num2letras($invoice->total); ?></td>
			</tr>
			<tr>
				<td><b><?php print SBText::_('Control code:', 'invoices'); ?></b></td>
				<td><?php print $invoice->control_code; ?></td>
			</tr>
			<tr>
				<td><b><?php print SBText::_('Deadline for issuance:', 'invoices'); ?></b></td>
				<td><?php print SB_format_date($invoice_limit_date, 'd-m-Y'); ?></td>
			</tr>
			</table><br/>
			<div><?php print str_replace("\n", "<br/>", $invoice_footer_msg); ?></div>
		</td>
		<td style="width:20%;">
			<div id="qr-code">
				<img src="<?php print $qr_code; ?>" />
			</div>
		</td>
	</tr>
	</table>
</div>
<?php
$prints = (int)$invoice->_prints;
$cp1 = ob_get_clean();
if( $prints <= 0 )
{
	print str_replace('{titulo}', 'ORIGINAL', $cp1);
	print str_replace('{titulo}', 'COPIA 1', $cp1);
	print str_replace('{titulo}', 'COPIA 2', $cp1);
	if( SB_Request::getInt('print') )
		mb_invoice_update_meta($invoice->invoice_id, '_prints', 2);
}
else 
{
	$prints++;
	print str_replace('{titulo}', "COPIA $prints", $cp1);
	if( SB_Request::getInt('print') )
		mb_invoice_update_meta($invoice->invoice_id, '_prints', $prints);
}
?>
<?php if( !SB_Request::getInt('print') ): ?>
<script type="text/javascript">
//<![[CDATA
function mb_print_invoice()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
//]]>
</script>
<?php endif; ?>
