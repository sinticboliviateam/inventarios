<?php
/**
 * Template name: Default
 */
use SinticBolivia\SBFramework\Classes\SB_Request;
$mime = sb_get_file_mime($logo);
$data = base64_encode(file_get_contents($logo));
?>
<style type="text/css">
#print-preview{width:100%;}
#invoice{margin:0;padding:0;width:100%;height:100%;}
#invoice *{font-family:Helvetica;font-size:11px;}
#invoice #header{}
#title{width:100%;font-weight:bold;text-transform: uppercase;text-align:center;clear:both;font-size:20px;}
#the-invoice-data{border:1px solid #000;padding:10px;width:60%;float:right;}
.invoice-items{width:100%;border:1px solid #000;border-collapse: collapse;}
.items-cell{border:1px solid #000;padding:5px;}
.item-qty, .item-price{width:14%;text-align:center;}
.item-product{width:50%;}
.item-total{width:20%;text-align:right;}
.total-text,.invoice-total{font-weight: bold;text-align:right;}
#limit-data{float:left:50%;}
#qr-code{float:right;width:200px;text-align:right;}
#invoice-footer{clear:both;text-align:center;}
#invoice-info td{padding:4px;}
.text-center{text-align:center;}
#order-items thead th{background-color:#9e9fa3;}
</style>
<div id="invoice">
	<div id="header">
		<table style="width:100%;">
		<tr>
			<td style="width:50%;">
				<div style="text-align:center;">
					<?php if($logo_url): ?>
					<div>
						<img src="<?php print "data:$mime;base64,$data"; ?>" alt="" style="width:250px;" />
					</div>
					<?php endif; ?>
					<?php print $ops->business_owner; ?><br/>
					<?php _e('Main House', 'invoices'); ?><br/>
					<?php print $business_address; ?><br/>
					<?php printf("%s%s - Fax: %s", __('Phone:', 'invoices'), $business_phone, isset($business_fax) ? $business_fax : $business_phone); ?><br/>
					<?php printf("%s - %s", $business_city, $countries[$business_country]); ?>
				</div>
			</td>
			<td style="width:50%;">
				<div style="width:80%;float:right;border:1px solid #000;padding:5px;">
					<table style="width:100%;">
					<tr>
						<td><b>NIT:</b></td>
						<td><?php print $business_nit_ruc_nif; ?></td>
					</tr>
					<tr>
						<td><b><?php _e('Invoice No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->invoice_number; ?></td>
					</tr>
					<tr>
						<td><b><?php _e('Authorization No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->authorization; ?></td>
					</tr>
					</table>
				</div>
				<div style="clear:both;margin-top:5px;">
					<?php print $this->dosage->header_text; ?>
				</div>
			</td>
		</tr>
		</table>
	</div><!-- end id="header" -->
	<br/>
	<h3 id="title" style="text-align:center;text-transform: uppercase;font-size:22px;"><?php _e('Invoice', 'invoices'); ?></h3>
	<div id="customer-info">
		<table style="width:100%;">
		<tr>
			<td style="width:50%;">
				<table style="width:100%;">
				<tr>
					<td style="width:60px;"><b><?php _e('Date:', 'invoices'); ?></b></td>
					<td style="text-align:left;"><?php print sb_format_date($invoice->invoice_date_time); ?></td>
				</tr>
				<tr>
					<td><b><?php _e('Customer:', 'invoices'); ?></b></td>
					<td style="text-align:left;"><?php print $invoice->customer; ?></td>
				</tr>
				</table>
			</td>
			<td style="width:50%;">
				<table style="width:100%;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="width:60px;"><b><?php _e('NIT:', 'invoices'); ?></b></td>
					<td><?php print $invoice->nit_ruc_nif; ?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<div>&nbsp;</div>
	<table id="order-items" style="width:100%;border-collapse:collapse;">
	<thead>
	<tr>
		<th class="item-qty items-cell"><?php _e('Quantity', 'invoices'); ?></th>
		<th class="item-product items-cell"><?php _e('Concept', 'invoices'); ?></th>
		<th class="item-price items-cell"><?php _e('Price', 'invoices'); ?></th>
		<th class="item-total text-center items-cell"><?php _e('Total', 'invoices'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($invoice->GetItems() as $item): ?>
	<tr>
		<td class="item-qty items-cell"><?php print $item->quantity; ?></td>
		<td class="item-product items-cell"><?php print $item->product_name; ?></td>
		<td class="item-price items-cell"><?php print number_format($item->price, 2, '.', ','); ?></td>
		<td class="item-total items-cell"><?php print number_format($item->total, 2, '.', ','); ?></td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="2" style="border-left:1px solid #fff;border-right:1px solid #000;"></td>
		<td class="total-text items-cell"><?php _e('Total:', 'invoices'); ?></td>
		<td class="invoice-total items-cell"><?php print number_format($invoice->total, 2, '.', ','); ?></td>
	</tr>
	</tbody>
	</table><br/>
	<div id="invoice-info" style="">
		<table style="width:100%;">
		<tr>
			<td style="width:120px;"><b><?php print _e('Are:', 'invoices') ?></b></td>
			<td><?php print sb_num2letras($invoice->total); ?></td>
		</tr>
		<tr>
			<td><b><?php _e('Control code:', 'invoices'); ?></b></td>
			<td><?php print $invoice->control_code; ?></td>
		</tr>
		<tr>
			<td><b><?php _e('Deadline for issuance:', 'invoices'); ?></b></td>
			<td><?php print SB_format_date($invoice_limit_date, 'd-m-Y'); ?></td>
		</tr>
		</table>
	</div>
	<div id="qr-code" style="text-align:right;">
		<img src="<?php print $qr_code; ?>" />
	</div>
	<br/><br/>
	<div id="invoice-footer" style="text-align:center;">
		<?php print $invoice_footer_msg; ?>
	</div>
</div>
<?php if( !SB_Request::getInt('print') ): ?>
<script type="text/javascript">
//<![[CDATA
function mb_print_invoice()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
//]]>
</script>
<?php endif; ?>
