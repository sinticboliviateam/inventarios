<?php
/**
 * Template name: Factura de Exportacion (Majota)
 * Fields: {"_transporte":{"type":"text","label":"Transporte"},"_cantidad_desc_bultos":{"type":"textarea","label":"Cantidad y descripcion de Bultos:"},"_instrucciones_notas":{"type":"textarea","label":"Instrucciones y Notas"},"_incoterm":{"type":"text","label":"Incoterm"}}
 */
?>
<?php
$total_bob = number_format($invoice->total * $invoice->exchange_rate, 2, '.', ','); 
?>
<style>
th{text-align:center;}
.col-1{width:200px;}
.col-2{width:350px;}
.col-3{width:150px;}
.col-nandina,.col-product,.col-qty,.col-um,.col-price,.col-total{border:1px solid #000;padding:2px;}
.col-qty{text-align:center;}
.col-price,.col-total{width:140px;text-align:right;}
</style>
<table style="width:100%;">
<tr>
	<td style="width:50%;text-align:center;">
		<div><img src="<?php print UPLOADS_URL; ?>/<?php print $ops->business_logo; ?>" alt="" /></div>
		<div><b>De:</b> <?php print $ops->business_owner; ?></div>
		<div><b>Casa Matriz</b></div>
		<div><?php print $ops->business_address; ?></div>
		<div><b>Telefono:</b><?php print $ops->business_phone; ?></div>
		<?php print $ops->business_city; ?> - <?php print $countries[$ops->business_country];?>
	</td>
	<td style="width:45%;">
		<div style="width:100%;border:1px solid #000;">
			<table style="width:100%;">
			<tr>
				<td>NIT:</td>
				<td><?php print $ops->business_nit_ruc_nif; ?></td>
			</tr>
			<tr>
				<td>FACTURA No.: </td>
				<td><?php print $invoice->invoice_number; ?></td>
			</tr>
			<tr>
				<td>AUTORIZACION No.:</td>
				<td><?php print $invoice->authorization; ?></td>
			</tr>
			</table>
		</div>
		<div>
			<h3 style="text-align:center;">ORIGINAL</h3>
			<p>
				Elaboracion de otros productos alimenticios (tostado, elab. de t&eacute;, mates, miel artificial, chocolates, etc.)
			</p>
		</div>
	</td>
</tr>
</table>
<h2 style="text-align:center;">FACTURA COMERCIAL DE EXPORTACION SIN DERECHO A CREDITO FISCAL</h2>
<div style="">
	<table style="width:100%;">
	<tr>
		<td class="col-1"><b>Nombre del Comprador:</b></td>
		<td class="col-2"><?php print $invoice->customer; ?></td>
		<td class="col-3"></td>
		<td class="col-4"></td>
	</tr>
	<tr>
		<td class="col-1"><b>Direcci&oacute;n del Comprador:</b></td>
		<td class="col-2"></td>
		<td class="col-3"></td>
		<td class="col-4"></td>
	</tr>
	<tr>
		<td class="col-1"><b>NIT:</b></td>
		<td class="col-2"><?php print $invoice->nit_ruc_nif; ?></td>
		<td class="col-3"></td>
		<td class="col-4"></td>
	</tr>
	<tr>
		<td class="col-1"><b>INCOTERM:</b></td>
		<td class="col-2">CIF</td>
		<td class="col-3"></td>
		<td class="col-4"></td>
	</tr>
	<tr>
		<td class="col-1"><b>Puerto Destino:</b></td>
		<td class="col-2"></td>
		<td class="col-3"></td>
		<td class="col-4"></td>
	</tr>
	<tr>
		<td class="col-1"><b>Moneda de la Transacci&oacute;n Comercial:</b></td>
		<td class="col-2"><?php print $this->currencies[$invoice->currency_code]; ?></td>
		<td class="col-3"><b>Tipo de Cambio:</b></td>
		<td class="col-4"><?php print $invoice->exchange_rate; ?></td>
	</tr>
	</table>
	<table style="width:100%;border-collapse:collapse;">
	<thead>
	<tr>
		<th class="col-nandina">NANDINA</th>
		<th class="col-product">DESCRIPCION</th>
		<th class="col-qty">CANTIDAD</th>
		<th class="col-um">UNIDAD DE MEDIDA</th>
		<th class="col-price">PRECIO UNITARIO</th>
		<th class="col-total">SUBTOTAL</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($invoice->GetItems() as $item): ?>
	<tr>
		<td class="col-nandina"><?php print $item->product_code; ?></td>
		<td class="col-product"><?php print $item->product_name; ?></td>
		<td class="col-qty"><?php print $item->quantity; ?></td>
		<td class="col-um"><?php  ?></td>
		<td class="col-price"><?php print $item->price; ?></td>
		<td class="col-total"><?php print $item->total; ?></td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="5" style="text-align:right;border:1px solid #000;"><b>TOTAL DOLAR ESTADOUNIDENSE</b></td>
		<TD style="text-align:right;border:1px solid #000;"><?php print $invoice->total; ?></TD>
	</tr>
	<tr>
		<td colspan="5" style="text-align:right;border:1px solid #000;"><b>VALOR CIF TOTAL BOB</b></td>
		<TD style="text-align:right;border:1px solid #000;"><?php print $total_bob; ?></TD>
	</tr>
	<tr>
		<td colspan="6" style="border:1px solid #000;"><b>Son:</b> <?php print sb_num2letras(str_replace(',', '', $invoice->total), $this->currencies['usd']); ?></td>
	</tr>
	<tr>
		<td colspan="6" style="border:1px solid #000;"><b>Son:</b> <?php print sb_num2letras($total_bob, $this->currencies['bob']); ?></td>
	</tr>
	<tr>
		<td colspan="5" style="border:1px solid #000;"><b>TRANSPORTE</b></td>
		<TD style="text-align:right;border:1px solid #000;"><?php print number_format($invoice->_transporte, 2, '.', ','); ?></TD>
	</tr>
	</tbody>
	</table>
	<h4>Cantidad y descripci&oacute;n de Bultos:</h4>
	<div style="border:1px solid #000;padding:5px;"><?php print $invoice->_cantidad_desc_bultos; ?></div>
	<h4>Instrucciones y Notas:</h4>
	<div style="border:1px solid #000;padding:5px;"><?php print $invoice->_instrucciones_notas; ?></div>
	<table style="width:100%;">
	<tr>
		<td style="width:200px;"><b>C&oacute;digo de Control:</b></td>
		<td><?php print $invoice->control_code; ?></td>
	</tr>
	<tr>
		<td style="width:200px;"><b>Fecha Limite de Emision:</b></td>
		<td><?php print sb_format_date($this->dosage->emission_limit_date); ?></td>
	</tr>
	</table>
	<br/><br/>
	<p style="text-align:center;"><?php print $this->dosage->footer_text; ?></p>
</div>
<script>
function mb_print_invoice()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
</script>