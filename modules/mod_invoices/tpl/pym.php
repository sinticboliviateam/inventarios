<?php
/**
 * Template name: Petalos y Matices (Factura Medio Oficio)
 */
$copies = $current_copies > 1 ? 1 : 1 + 1;
?>
<style type="text/css">
@page {/*size:21cm 14.85cm;*/ margin: 0.5cm 1cm 1cm 1cm }
#print-preview{width:100%;}
.factura{clear:both;overflow:hidden;margin:0 0 10px 0;padding:0;width:100%;height:500px;/*border-bottom:1px solid #000;*/}
.factura *{font-family:Helvetica;font-size:10px;}
#title{width:100%;font-weight:bold;text-transform:uppercase;text-align:center;clear:both;font-size:20px;}
#the-invoice-data{border:1px solid #000;padding:10px;width:60%;float:right;}
.invoice-items{width:100%;border:1px solid #000;border-collapse: collapse;}
.items-cell{border:1px solid #000;padding:2px;}
.item-qty, .item-price{width:14%;text-align:center;}
.item-product{width:50%;}
.item-total{width:20%;text-align:right;}
.total-text,.invoice-total{font-weight: bold;text-align:right;}
#invoice-footer{clear:both;text-align:center;}
#invoice-info td{padding:4px;}
.text-center{text-align:center;}
#order-items thead th{background-color:#9e9fa3;}
</style>
<script type="text/php">
if( isset($pdf) )
{
	print_r($pdf);
	$font = Font_Metrics::get_font("Arial", "bold");
	$pdf->page_text(765, 550, "Pagina {PAGE_NUM} de {PAGE_COUNT}", $font, 9, array(0, 0, 0));
}
</script>
<?php for($c = 1; $c <= $copies; $c++): ?>
<?php
$invoice_title = $current_copies > 1 ? sprintf("Copia $c") : $c == 1 ? 'ORIGINAL' : sprintf("COPIA %d", $c - 1);
?>
<div class="factura">
	<div id="header">
		<table style="width:100%;">
		<tr>
			<td style="width:33.33%;">&nbsp;</td>
			<td style="width:33.33%;">
				<div style="text-align:center;">
					<?php if($logo_url): ?>
					<div>
						<img src="<?php print $logo_url; ?>" alt="" style="width:250px;" />
					</div>
					<?php endif; ?>
					<?php print $ops->business_owner; ?><br/>
					<?php print SBText::_('Main House', 'invoices'); ?><br/>
					<?php print $business_address; ?><br/>
					<?php printf("%s%s - Fax: %s", SBText::_('Phone:', 'invoices'), $business_phone, isset($business_fax) ? $business_fax : $business_phone); ?><br/>
					<?php printf("%s - %s", $business_city, $countries[$business_country]); ?>
				</div>
			</td>
			<td style="width:33.33%;">
				<div style="width:80%;float:right;border:1px solid #000;padding:5px;">
					<table style="width:100%;">
					<tr>
						<td><b>NIT:</b></td>
						<td><?php print $business_nit_ruc_nif; ?></td>
					</tr>
					<tr>
						<td><b><?php print SBText::_('Invoice No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->invoice_number; ?></td>
					</tr>
					<tr>
						<td><b><?php print SBText::_('Authorization No.:', 'invoices'); ?></b></td>
						<td><?php print $invoice->authorization; ?></td>
					</tr>
					</table>
				</div>
				<div style="clear:both;width:100%;"></div>
				<h3 style="text-align:center;"><?php print $invoice_title; ?></h3>
			</td>
		</tr>
		</table>
		<div style="clear:both;font-size:8px;margin:2px 0;">
			<?php print $this->dosage->header_text; ?>
		</div>
	</div><!-- end id="header" -->
	<br/>
	<h3 id="title" style="text-align:center;text-transform: uppercase;font-size:22px;margin:0 0 5px 0;">
		<?php print SBText::_('Invoice', 'invoices'); ?>
	</h3>
	<div id="customer-info">
		<table style="width:100%;">
		<tr>
			<td style="width:50%;">
				<table style="width:100%;">
				<tr>
					<td style="width:60px;"><b><?php print SBText::_('Date:', 'invoices'); ?></b></td>
					<td style="text-align:left;"><?php print sb_format_date($invoice->invoice_date_time); ?></td>
				</tr>
				<tr>
					<td><b><?php print SBText::_('Customer:', 'invoices'); ?></b></td>
					<td style="text-align:left;"><?php print $invoice->customer; ?></td>
				</tr>
				</table>
			</td>
			<td style="width:50%;">
				<table style="width:100%;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="width:60px;"><b><?php print SBText::_('NIT:', 'invoices'); ?></b></td>
					<td><?php print $invoice->nit_ruc_nif; ?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<table id="order-items" style="width:100%;border-collapse:collapse;">
	<thead>
	<tr>
		<th class="item-qty items-cell"><?php _e('Cantidad', 'invoices'); ?></th>
		<th class="item-product items-cell"><?php _e('Concept', 'invoices'); ?></th>
		<th class="item-price items-cell"><?php _e('Price', 'invoices'); ?></th>
		<th class="item-total text-center items-cell"><?php _e('Total', 'invoices'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if( $invoice->_invoice_detail_name ): ?>
	<tr>
		<td class="item-qty items-cell">1</td>
		<td class="item-product items-cell"><?php print $invoice->_invoice_detail_name; ?></td>
		<td class="item-price items-cell"><?php print number_format($invoice->GetItemsTotalAmount(), 2, '.', ','); ?></td>
		<td class="item-total items-cell"><?php print number_format($invoice->GetItemsTotalAmount(), 2, '.', ','); ?></td>
	</tr>
	<?php $i = 2; foreach($invoice->GetItems() as $item): if( (int)$item->product_id != -1 ) continue; ?>
	<tr>
		<td class="item-qty items-cell"><?php print $i; ?></td>
		<td class="item-product items-cell"><?php print $item->product_name; ?></td>
		<td class="item-price items-cell"><?php print number_format($item->total, 2, '.', ','); ?></td>
		<td class="item-total items-cell"><?php print number_format($item->total, 2, '.', ','); ?></td>
	</tr>
	<?php endforeach; ?>
	<?php else: $i = 1; foreach($invoice->GetItems() as $item): ?>
	<tr>
		<td class="item-qty items-cell"><?php print $item->quantity; ?></td>
		<td class="item-product items-cell"><?php print $item->product_name; ?></td>
		<td class="item-price items-cell"><?php print number_format($item->price, 2, '.', ','); ?></td>
		<td class="item-total items-cell"><?php print number_format($item->total, 2, '.', ','); ?></td>
	</tr>
	<?php $i++; endforeach; endif; ?>
	<tr>
		<td colspan="2" style="border-left:1px solid #fff;border-right:1px solid #000;"></td>
		<td class="total-text items-cell"><?php _e('Total:', 'invoices'); ?></td>
		<td class="invoice-total items-cell"><?php print number_format($invoice->total, 2, '.', ','); ?></td>
	</tr>
	</tbody>
	</table><br/>
	<div style="clear:both;overflow:hidden;height:100px;">
		<div id="invoice-info" style="width:80%;float:left;">
			<table style="width:100%;">
			<tr>
				<td style="width:120px;"><b><?php print SBText::_('Are:', 'invoices') ?></b></td>
				<td><?php print sb_num2letras($invoice->total); ?></td>
			</tr>
			<tr>
				<td><b><?php print SBText::_('Control code:', 'invoices'); ?></b></td>
				<td><?php print $invoice->control_code; ?></td>
			</tr>
			<tr>
				<td><b><?php print SBText::_('Deadline for issuance:', 'invoices'); ?></b></td>
				<td><?php print SB_format_date($invoice_limit_date, 'd-m-Y'); ?></td>
			</tr>
			</table>
		</div>
		<div id="qr-code" style="float:right;width:20%;text-align:right;height:100px;">
			<img src="<?php print $qr_code; ?>" style="height:80px;" />
		</div>
	</div>
	<div id="invoice-footer" style="width:100%;text-align:center;">
		<?php print $invoice_footer_msg; ?>
	</div>
</div>
<?php if( !SB_Request::getInt('print') ): ?>
<script type="text/javascript">
//<![[CDATA
function mb_print_invoice()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
//]]>
</script>
<?php endif; ?>
<?php endfor; ?>