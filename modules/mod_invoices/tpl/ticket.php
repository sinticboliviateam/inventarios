﻿<?php
/**
 * Template name: Ticket
 */
?>
<style type="text/css">
#print-preview{width:270px;}
@media print
{
	
	#invoice-container{width:100% !important;}
}
@page {
	margin: 0.5;
	size:
	font-size:11px;
	font-family:Arial,Verdana;
}
#invoice-container{width:250px;margin:0;padding:0;}
.text-center{text-align:center;}
#invoice-table-items{}
#invoice-table-items tr th{text-align:center;}
#invoice-table-items tr th,
#invoice-table-items tr td{padding:2px;}
</style>
<div id="invoice-container">
	<div class="text-center"><?php print $business_name ?></div>
	<div class="text-center"><?php print @$business_branch_office ?></div>
	<div class="text-center"><?php print @$business_address ?></div>
	<div class="text-center">Telefono: <?php print @$business_phone; ?></div>
	<div class="text-center"><?php print @$business_city ?> - <?php print @$business_country ?></div>
	<div class="text-center">SUCURSAL: 0</div>
	<div class="text-center">FACTURA ORIGINAL</div>
	<div class="text-center">NIT:<?php print @$business_nit_ruc_nif; ?></div>
	<div class="text-center">Factura No. <?php print $invoice->invoice_number ?></div>
	<div class="text-center">Autorizacion No.: <?php print $invoice->authorization; ?></div><br/>
	<p>
		<?php print $this->dosage->header_text; ?>
	</p>
	<table style="width:100%;">
	<tr>
		<td style="width:50%;">
			Fecha: <?php print sb_format_date($invoice->invoice_date_time); ?><br/>
			Se&ntilde;or(es): <?php print $invoice->customer; ?><br/>
			NIT/CI: <?php print $invoice->nit_ruc_nif; ?>
		</td>
		<td style="width:50%;">
			Hora: <?php print sb_format_time($invoice->invoice_date_time); ?>
		</td>
	</tr>
	</table>
	<table id="invoice-table-items" style="width:100%;">
	<thead>
	<tr>
		<th>Cant.</th>
		<th>Cod.</th>
		<th>Detalle</th>
		<th>Precio</th>
		<th>Total</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($invoice->GetItems() as $item): ?>
	<tr>
		<td id="item-qty" style="text-align:center;"><?php print $item->quantity; ?></td>
		<td id="item-code"><?php print ''; ?></td>
		<td id="item-product"><?php print $item->product_name; ?></td>
		<td id="item-price" style="text-align:right;"><?php print number_format($item->price, 2); ?></td>
		<td id="item-total" style="text-align:right;"><?php print number_format($item->total, 2); ?></td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="4">Total:</td>
		<td style="text-align:right;"><?php print $invoice->total; ?></td>
	</tr>
	<tr>
		<td colspan="4">Cancelado:</td>
		<td style="text-align:right;">0.00</td>
	</tr>
	<tr>
		<td colspan="4">Cambio:</td>
		<td style="text-align:right;">0.00</td>
	</tr>
	</tfoot>
	</table>
	<p>
		Son: <?php print sb_num2letras($invoice->total); ?>
	</p>
	<p>
		Codigo de Control: <?php print $invoice->control_code; ?><br/>
		Fecha Limite de Emision: <?php print $invoice_limit_date; ?>
	</p>
	<p style="text-align:center;">
		<img src="<?php print $qr_code; ?>" alt="" />
	</p>
	<p style="text-align:center;">
		<?php print str_replace(array("\r\n", "\n"), array("<br/>"), $this->dosage->footer_text); ?>
	</p>
</div>

</div>
<script>
function mb_print_invoice()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
</script>
