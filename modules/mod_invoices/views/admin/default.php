<?php
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

$can_delete_invoice = $user->can('delete_invoice');
$can_edit_invoice	= $user->can('edit_invoice');
$search_by = $this->request->getString('search_by');
?>
<div class="wrap">
	<h2><?php _e('Invoice Management', 'invoices'); ?></h2>
	<div class="row">
		<div class="col-md-12">
			<a href="<?php print $this->Route('index.php?mod=invoices&view=new'); ?>" class="btn btn-secondary">
				<?php _e('New', 'invoices'); ?>
			</a>
			<a href="<?php print $this->Route('index.php?mod=invoices&view=reports'); ?>" class="btn btn-secondary">
				<?php _e('Reportes', 'invoices'); ?>
			</a>
			<a href="<?php print $this->Route('index.php?mod=invoices&view=form_test&country=BO'); ?>" class="btn btn-info">
				<?php _e('Test Bolivia', 'invoices'); ?>
			</a>
		</div>
	</div>
	<form action="index.php" method="get">
		<input type="hidden" name="mod" value="invoices" />
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<input type="text" name="keyword" value="" class="form-control" />
				</div>
				<div class="col-md-2">
					<select name="search_by" class="form-control">
						<option value="-1"><?php _e('-- search by --', 'invoices'); ?></option>
						<option value="invoice_number" <?php print $search_by == 'invoice_number' ? 'selected' : ''; ?>><?php _e('Numero Factura'); ?></option>
						<option value="customer" <?php print $search_by == 'invoice_number' ? 'selected' : ''; ?>><?php _e('Cliente'); ?></option>
						<option value="id" <?php print $search_by == 'invoice_number' ? 'selected' : ''; ?>><?php _e('ID'); ?></option>
					</select>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="from_date" value="<?php print $this->request->getString('from_date', ''); ?>" 
							class="form-control datepicker" placeholder="<?php _e('Date From', 'invoices'); ?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="to_date" value="<?php print $this->request->getString('to_date', ''); ?>" 
							class="form-control datepicker" placeholder="<?php _e('Date To', 'invoices'); ?>" />
					</div>
				</div>
				<div class="col-md-2"><button type="submit" class="btn btn-default"><?php _e('Buscar'); ?></button></div>
			</div>
		</div>
	</form>
	<table class="table table-condensed table-hover">
	<thead>
	<tr>
		<th>#</th>
		<th><?php _e('ID', 'invoices'); ?></th>
		<th><?php _e('User', 'invoices'); ?></th>
		<th><?php _e('Dosage', 'invoices'); ?></th>
		<th><?php _e('Numero de Factura', 'invoices'); ?></th>
		<th><?php _e('Customer', 'invoices'); ?></th>
		<th><?php _e('Amount', 'invoices'); ?></th>
		<th><?php _e('Date', 'invoices'); ?></th>
		<th><?php _e('Status', 'invoices'); ?></th>
		<th><?php _e('Actions', 'invoices'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if( is_array($invoices) ): $i = 1;foreach($invoices as $inv): $user = new SB_User($inv->user_id); ?>
	<tr>
		<td class="text-center"><?php print $i; ?></td>
		<td class="text-center"><?php print $inv->invoice_id; ?></td>
		<td><?php print $user->username; ?></td>
		<td class="text-center"><?php print $inv->name; ?></td>
		<td class="text-center"><?php print $inv->invoice_number; ?></td>
		<td><?php print $inv->customer; ?></td>
		<td class="text-right"><?php print $inv->total; ?></td>
		<td class="text-right"><?php print sb_format_datetime($inv->creation_date); ?></td>
		<td class="text-center">
			<?php
			$class = '';
			if( $inv->status == 'void' )
				$class = 'danger';
			if( $inv->status == 'issued' )
				$class = 'success';
			?>
			<label class="label label-<?php print $class; ?>"><?php print $inv->status; ?></label>
		</td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=invoices&task=void&id='.$inv->invoice_id); ?>" class="btn btn-default confirm btn-sm" 
				data-message="<?php _e('Are you sure to void this invoice?', 'invoices'); ?>"
				title="<?php _e('Void', 'invoices'); ?>">
				<span class="glyphicon glyphicon-remove"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=invoices&view=view&id='.$inv->invoice_id); ?>" class="btn btn-default btn-sm"
				title="<?php _e('View', 'invoices'); ?>">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<?php if( $can_edit_invoice ): ?>
			<a href="<?php print $this->Route('index.php?mod=invoices&view=edit&id='.$inv->invoice_id); ?>" class="btn btn-default btn-sm"
				title="<?php print _e('Edit', 'invoices'); ?>">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<?php endif; ?>
			<a href="<?php print $this->Route('index.php?mod=invoices&task=view&print=1&id='.$inv->invoice_id); ?>" 
				class="btn btn-default print-invoice btn-sm"
				title="<?php _e('Print', 'invoices'); ?>">
				<span class="glyphicon glyphicon-print"></span>
			</a>
			<?php if( $can_delete_invoice ): ?>
			<a href="<?php print $this->Route('index.php?mod=invoices&task=delete&&id='.$inv->invoice_id); ?>" 
				class="btn btn-default btn-sm confirm"
				data-message="<?php _e('Are you sure to delete the invoice?', 'invoices'); ?>"
				title="<?php _e('Delete Invoice', 'invoices'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
			<?php endif; ?>
		</td>
	</tr>
	<?php $i++; endforeach; endif; ?>
	</tbody>
	</table>
	<p><?php lt_pagination($this->Route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?></p>
	<script>
	jQuery(function()
	{
		jQuery('.print-invoice').click(function()
		{
			if( jQuery('#invoice-iframe').length > 0 )
			{
				jQuery('#invoice-iframe').remove();
			}
			var iframe = jQuery('<iframe id="invoice-iframe" src="'+this.href+'" style="display:none;"></iframe>');
			//window.iframe = iframe;
			jQuery('body').append(iframe);
			try
			{
				iframe.load(function()
				{
					iframe.get(0).contentWindow.mb_print_invoice();
				});
				
			}
			catch(e)
			{
				alert(e);
			}
			
			return false;
		});
	});
	</script>
</div>