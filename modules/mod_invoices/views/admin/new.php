<?php
?>
<div class="wrap">
	<h2><?php print $title; ?></h2>
	<?php if( isset($invoice) ): ?>
	<p>
		<a href="<?php print $this->Route('index.php?mod=invoices&view=view&print=1&id='.$invoice->invoice_id); ?>" class="btn btn-warning btn-sm" target="_blank">
			<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'invoices'); ?>
		</a>
	</p>
	<?php endif; ?>
	<form action="" method="post" id="form-invoice" class="form-group-sm" >
		<input type="hidden" name="mod" value="invoices" />
		<input type="hidden" name="task" value="save" />
		<?php if( isset($invoice) ): ?>
		<input type="hidden" name="invoice_id" value="<?php print $invoice->invoice_id; ?>" />
		<?php endif; ?>
		<input type="hidden" id="dosage_id" name="dosage_id" value="<?php print count($this->dosages) == 1 ? $this->dosages[0]->id : ''; ?>" />
		<input type="hidden" id="customer_id" name="customer_id" value="<?php print isset($invoice) ? $invoice->customer_id : ''; ?>" />
		<div class="form-horizontal">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('Dosage:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<input type="text" id="dosage" name="dosage" value="<?php print count($this->dosages) == 1 ? $this->dosages[0]->name : ''; ?>" 
								class="form-control" readonly="readonly" />
						</div>
						<?php if( count($this->dosages) > 1 ): ?>
						<a href="javascript:;" class="btn btn-default" title="<?php _e('Select Dosage', 'invoices'); ?>"
							data-toggle="modal" data-target="#modal-dosages">
							<span class="glyphicon glyphicon-search"></span>
						</a>
						<?php endif; ?>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('NIT/RUC/NIF:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<input type="text" id="nit_ruc_nif" name="nit_ruc_nif" value="<?php print isset($invoice) ? $invoice->nit_ruc_nif : ''; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('Customer:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<input type="text" id="customer" name="customer" 
								value="<?php print isset($invoice) ? $invoice->_billing_name ? $invoice->_billing_name : $invoice->customer : ''; ?>" class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('Tax:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<select id="tax" name="tax" class="form-control">
								<?php foreach($this->taxes as $tax): ?>
								<option value="<?php print $tax->rate; ?>"><?php print $tax->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('Currencies:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<select id="currency" name="currency" class="form-control">
								<?php foreach($this->currencies as $curr => $label): ?>
								<option value="<?php print $curr; ?>"><?php print $label; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php _e('Exchange Rate:', 'invoices'); ?></label>
						<div class="col-sm-9">
							<input type="text" name="exchange_rate" value="1.00" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<input type="text" id="search_product" name="search_product" value="" placeholder="<?php _e('Search product', 'invoice'); ?>" class="form-control" />
				<span class="input-group-btn">
		        	<button id="btn-add-item" class="btn btn-default btn-sm" type="button"><?php _e('Add item', 'invoices'); ?></button>
		      	</span>
			</div>
		</div>
		<style>
		.inv-item-remove{width:5%;text-align:center;}
		.inv-item-remove img{width:25px;}
		.inv-item-number{width:5%;}
		.inv-item-code{width:10%;}
		.inv-item-name{width:41.4%;}
		.inv-item-qty{width:9%;}
		.inv-item-price{width:12.5%;}
		.inv-item-tax{width:12.5%;}
		.inv-item-total{width:15%;}
		.cool-table{background:#fff;}
		.cool-table .body{max-height:250px;}
		.cool-table .inv-item-number, .cool-table .inv-item-qty{text-align:center;}
		.cool-table .inv-item-qty input{text-align:center;}
		.cool-table .inv-item-price input, 
		.cool-table .inv-item-tax input
		{text-align:right;}
		.cool-table .inv-item-total
		{text-align:right;}
		</style>
		<div id="invoice-table" class="cool-table">
			<div class="heading">
				<div class="trow">
					<div class="cell inv-item-remove">&nbsp;</div>
					<div class="cell inv-item-number">#</div>
					<div class="cell inv-item-code"><?php _e('Codigo', 'invoices'); ?></div>
					<div class="cell inv-item-name"><?php _e('Item', 'invoices'); ?></div>
					<div class="cell inv-item-qty"><?php _e('Qty', 'invoices'); ?></div>
					<div class="cell inv-item-price"><?php _e('Price', 'invoices'); ?></div>
					<!-- <div class="cell inv-item-tax"><?php _e('Tax', 'invoices'); ?></div>  -->
					<div class="cell inv-item-total"><?php _e('Total', 'invoices'); ?></div>
				</div>
			</div>
			<div class="body">
				<?php if( isset($invoice) ): $i = 1; foreach($invoice->GetItems() as $item): ?>
				<div class="trow">
					<div class="cell inv-item-remove">
						<a href="javascript:;" class="btn btn-default btn-sm remove-item">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</div>
					<div class="cell inv-item-number">
						<?php print $i; ?>
						<input type="hidden" name="items[<?php print $i - 1; ?>][id]" value="<?php print $item->product_id; ?>" />
					</div>
					<div class="cell inv-item-code">
						<?php print $item->product_code; ?>
						<input type="hidden" name="items[<?php print $i - 1; ?>][code]" value="<?php print $item->product_code; ?>" />
					</div>
					<div class="cell inv-item-name">
						<?php print $item->product_name; ?>
						<input type="hidden" name="items[<?php print $i - 1; ?>][name]" value="<?php print $item->product_name; ?>" />
					</div>
					<div class="cell inv-item-qty">
						<input type="number" min="1" name="items[<?php print $i - 1; ?>][qty]" value="<?php print $item->quantity; ?>" class="form-control item-qty" />
					</div>
					<div class="cell inv-item-price">
						<input type="text" name="items[<?php print $i - 1; ?>][price]" value="<?php print $item->price; ?>" 
								class="form-control item-price" autocomplete="off" />
					</div>
					<div class="cell inv-item-total">
						<span class="item-total"><?php print $item->total; ?></span>
					</div>
				</div>
				<?php $i++; endforeach;endif;?>
			</div>
		</div><!-- end id="invoice-table" -->
		<?php foreach($this->templates as $tpl): if( !isset($tpl['Fields']) || empty($tpl['Fields']) ) continue; $fields = json_decode($tpl['Fields']); ?>
		<div id="dosage-tpl-<?php print preg_replace('/[^a-zA-Z0-9]/', '-', basename($tpl['template_file'])); ?>" class="dosage-tpl-options" style="display:none;">
			<?php foreach($fields as $key => $f): ?>
			<div class="form-group">
				<label><?php print $f->label; ?></label>
				<?php if($f->type == 'textarea'): ?>
				<textarea name="meta[<?php print $key; ?>]" class="form-control"></textarea>
				<?php elseif( $f->type == 'text' ): ?>
				<input type="text" name="meta[<?php print $key?>]" value="" class="form-control" />
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
		</div>
		<?php endforeach; ?>
		<br/>
		<div class="row">
			<div class="col-md-6">
				<div class="btn-grou">
					<a href="<?php print $this->Route('index.php?mod=invoices'); ?>" class="btn btn-danger btn-lg"><?php _e('Cancel', 'invoices'); ?></a>
					<a href="javascript:;" id="btn-save" class="btn btn-success btn-lg"><?php _e('Save', 'invoices'); ?></a>
					<?php if( isset($invoice) ): ?>
					<a href="javascript:;" id="btn-void" class="btn btn-danger btn-lg"><?php _e('Void', 'invoices'); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="pull-right" style="background:#fff;width:150px;padding:10px;">
					<table id="table-totals" class="float-right" style="width:100%;">
					<tr>
						<th class="text-right"><?php _e('Subtotal:', 'invoices'); ?></th>
						<td class="text-right"><span id="invoice-subtotal"><?php print isset($invoice) ? $invoice->subtotal : '0.00'; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Tax:', 'invoices'); ?></th>
						<td class="text-right"><span id="invoice-tax"><?php print isset($invoice) ? $invoice->tax : '0.00'?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Total:', 'invoices'); ?></th>
						<td class="text-right"><span id="invoice-total"><?php print isset($invoice) ? $invoice->total : '0.00'; ?></span></td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<style>
		.sb-suggestions{max-height:200px;width:100%;}
		.sb-suggestions .the_suggestion{padding:5px;display:block;}
		.sb-suggestions .the_suggestion:focus,
		.sb-suggestions .the_suggestion:hover{background:#ececec;text-decoration:none;}, 
		</style>
	</form>
	<div id="modal-dosages" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><?php _e('Select Invoice Dosage', 'invoices'); ?></h4>
				</div>
				<div class="modal-body">
					<?php foreach($this->dosages as $d): ?>
					<a href="javascript:;" class="btn btn-default btn-select-dosage"
						data-id="<?php print $d->id; ?>"
						data-name="<?php print $d->name; ?>"
						data-tpl="<?php print preg_replace('/[^a-zA-Z0-9]/', '-', $d->template); ?>">
						<span class="glyphicon glyphicon-transfer"></span> <?php print $d->name; ?></a>
					<?php endforeach; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'invoices'); ?></button>
				</div>
			</div>
		</div>
	</div>
	<div id="success-dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><?php _e('Factura Registrada', 'invoices'); ?></h4>
				</div>
				<div class="modal-body">
					La Factura fue registrada correctamente.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'invoices'); ?></button>
					<a href="javascript:;" target="_blank" id="btn-print-invoice" class="btn btn-primary" onclick="jQuery('#success-dialog').modal('hide');window.location.reload();">
						<?php _e('Print', 'invoices'); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
	<script>
	var invoice_templates = <?php print json_encode($this->templates); ?>
	</script>
</div><!-- end class="wrap" -->