<?php
?>
<style type="text/css">
#print-preview
{
	margin:10px auto;
	padding:10px;
	border:1px solid #000;
	box-shadow:5px 5px 0px 0px #000;
	background:#fff;
	position:relative;
	min-height:550px;
	
}
#print-preview .no-print{display:none !important;}
@media print
{
	body, #container, #content, .wrap{margin:0 !important;padding:0 !important;}
	#menu,#footer,#mobile-menu{display:none !important;}
	.no-print{display:none !important;}
	#print-preview{margin:0 !important;display:block !important;border:0;}
}
</style>
<div class="wrap">
	<div id="print-preview">
		<?php print $this->print_content; ?>
	</div>
	<div class="no-print" style="text-align:center;">
		<a href="<?php print $this->back_link ?>" class="btn btn-danger"><?php _e('Back', 'invoices'); ?></a>
		<a href="javascript:;" onclick="mb_print();" class="btn btn-success"><?php print SBText::_('Print', 'invoices'); ?></a>
	</div>
</div>
