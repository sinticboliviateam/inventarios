<?php
?>
<div class="wrap">
	<h2 class="no-print">
		<?php print SBText::_('Reportes de Facturas', 'invoices'); ?>
		<span class="pull-right">
			<a href="<?php print SB_Route::_('index.php?mod=invoices'); ?>" class="btn btn-danger">
				<?php _e('Volver', 'invoices'); ?></a></span>
	</h2>
	<form action="index.php" method="get" class="no-print">
		<input type="hidden" name="mod" value="invoices" />
		<input type="hidden" name="task" value="report" />
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label><?php _e('Fecha Inicio'); ?></label>
						<input type="text" name="date_from" value="<?php print sb_format_date(SB_Request::getDate('date_from', date('Y-m-d'))); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><?php _e('Fecha Termino'); ?></label>
						<input type="text" name="date_to" value="<?php print sb_format_date(SB_Request::getDate('date_to', date('Y-m-d'))); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button type="submit" class="btn btn-default"><?php _e('Generar'); ?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<label><input type="checkbox" name="issued" value="1" checked="checked" />Validas</label>
					<label><input type="checkbox" name="void" value="1" checked="checked" />Anuladas</label>
				</div>
			</div>
		</div>
	</form>
	<?php if( $this->items ): ?>
	<div class="form-group no-print">
		<form action="index.php" method="get" target="_blank">
			<input type="hidden" name="mod" value="invoices" />
			<input type="hidden" name="task" value="report" />
			<input type="hidden" name="print" value="1" />
			<input type="hidden" name="date_from" value="<?php print SB_Request::getString('date_from'); ?>" />
			<input type="hidden" name="date_to" value="<?php print SB_Request::getString('date_to'); ?>" />
			<button type="submit" class="btn btn-warning">
				<span class="glyphicon glyphicon-print"></span> <?php print SBText::_('Imprimir', 'invoices'); ?>
			</button>
		</form>
	</div>
	<h3 style="text-align:center;"><?php print $this->report_title; ?></h3>
	<table class="table">
	<thead>
		<th><?php _e('No'); ?></th>
		<th><?php _e('Fecha'); ?></th>
		<th><?php _e('Cliente'); ?></th>
		<th><?php _e('Dosificacion'); ?></th>
		<th><?php _e('Numero Factura'); ?></th>
		<th><?php _e('Estado'); ?></th>
		<th><?php _e('Monto'); ?></th>
	</thead>
	<tbody>
	<?php $total = 0;$i = 1;foreach($this->items as $item): ?>
	<?php
	$total += $item->total; 
	?>
	<tr>
		<td class="text-center"><?php print $i; ?></td>
		<td><?php print sb_format_date($item->invoice_date_time); ?></td>
		<td ><?php print $item->customer; ?></td>
		<td><?php print $item->dosage_name; ?></td>
		<td class="text-center"><?php print $item->invoice_number; ?></td>
		<td><?php print $item->status == 'issued' ? '<span class="label label-success">Valida</span>' : '<span class="label label-danger">Anulada</span>'; ?></td>
		<td class="text-right"><?php print $item->total; ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tr>
		<td colspan="6" style="text-align:right;"><b><?php _e('Total'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
	</tr>
	</table>
	<?php endif; ?>
</div>
<script>
function mb_print()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
</script>