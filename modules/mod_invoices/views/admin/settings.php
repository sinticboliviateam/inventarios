<?php
use SinticBolivia\SBFramework\Classes\SB_Module;
$mb_exists = ( !SB_Module::moduleExists('mb') || !SB_Module::IsEnabled('mb') ) ? false : true;
$stores = class_exists('SB_Warehouse') ? SB_Warehouse::getStores() : null;
?>
<?php if( !$mb_exists ): ?>
<div class="wrap">
	<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="mod" value="invoices" />
		<input type="hidden" name="task" value="save_settings" />
		<input type="hidden" name="invoices_ops[business_country]" value="BO" />
	<h2>
		<?php _e('Configuracion de Facturacion', 'invoices'); ?>
		<button type="submit" class="pull-right btn btn-success"><?php _e('Save', 'invoices'); ?></button>
	</h2>
<?php endif; ?>
<div id="billing" class="tab-pane">
	<div class="form-group">
		<label><?php _e('Invoice Size'); ?></label>
		<select name="invoices_ops[size]" class="form-control">
			<option value="A4" <?php print @$ops->size == 'A4' ? 'selected' : ''; ?>>A4</option>
			<option value="legal" <?php print @$ops->size == 'legal' ? 'selected' : ''; ?>><?php _e('Legal', 'invoices'); ?></option>
			<option value="ticket" <?php print @$ops->size == 'ticket' ? 'selected' : ''; ?>><?php _e('Ticket', 'invoices'); ?></option>
		</select>
	</div>
	<?php if( isset($show_common_fields) && $show_common_fields ): ?>
	<div class="form-group">
		<label><?php _e('Logo:', 'invoices'); ?></label>
		<input type="file" name="logo" value="" class="form-control" />
	</div>
		<?php if( @$ops->business_logo): ?>
		<p class="text-center" style="width:50%;">
			<img src="<?php print UPLOADS_URL; ?>/<?php print $ops->business_logo; ?>" style="max-width:100%;" />
		</p>
		<?php endif;?>
	<?php endif; ?>
	<div class="form-group">
		<label><?php _e('NIT/RUC/NIF:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_nit_ruc_nif]" value="<?php print @$ops->business_nit_ruc_nif; ?>" class="form-control" />
	</div>
	<div class="form-group">
		<label><?php _e('Owner:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_owner]" value="<?php print @$ops->business_owner; ?>" class="form-control" />
	</div>
	<?php if( isset($show_common_fields) && $show_common_fields ): ?>
	<div class="form-group">
		<label><?php _e('Business Name:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_name]" value="<?php print @$ops->business_name; ?>" class="form-control" />
	</div>
	<div class="form-group">
		<label><?php _e('Business Address:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_address]" value="<?php print @$ops->business_address; ?>" class="form-control" />
	</div>
	<div class="form-group">
		<label><?php _e('Business Telephone:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_phone]" value="<?php print @$ops->business_phone; ?>" class="form-control" />
	</div>
	<div class="form-group">
		<label><?php _e('City:', 'invoices'); ?></label>
		<input type="text" name="invoices_ops[business_city]" value="<?php print @$ops->business_city; ?>" class="form-control" />
	</div>
	<div class="form-group">
		<label><?php _e('Country:', 'invoices'); ?></label>
		<select name="invoices_ops[business_country]" class="form-control">
			<option value="-1"><?php _e('-- country --', 'invoices'); ?></option>
			<?php foreach( include INCLUDE_DIR . SB_DS . 'countries.php' as $code => $country): ?>
			<option value="<?php print $code; ?>" <?php print @$ops->business_country == $code ? 'selected' : ''; ?>><?php print $country; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<?php endif; ?>
	<div class="form-group">
		<a href="javascript:;" id="btn-new-dosage" class="btn btn-primary">
			<span class="glyphicon glyphicon-plus"> <?php _e('New Dosage', 'invoices'); ?></span>
		</a>
	</div>
	<table id="table-dosages" class="table table-condensed table-bordered">
	<thead>
	<tr>
		<th><?php _e('No.', 'invoices'); ?></th>
		<th><?php _e('Store', 'invoices'); ?></th>
		<th><?php _e('Name', 'invoices'); ?></th>
		<th style="width:15%;"><?php _e('Dosage', 'invoices'); ?></th>
		<th><?php _e('Authorization', 'invoices'); ?></th>
		<th><?php _e('Emission Limit Date', 'invoices'); ?></th>
		<th><?php _e('Default', 'invoices'); ?></th>
		<th><?php _e('Actions', 'invoices'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($this->dosages as $d): ?>
	<tr data-id="<?php print $d->id; ?>"
		data-store_id="<?php print $d->store_id; ?>" 
		data-name="<?php print $d->name; ?>" 
		data-dosage="<?php print $d->dosage; ?>" 
		data-authorization="<?php print $d->authorization; ?>"
		data-emission_limit_date="<?php print sb_format_date($d->emission_limit_date); ?>"
		data-header_text="<?php print $d->header_text; ?>"
		data-footer_text="<?php print $d->footer_text; ?>"
		data-template="<?php print $d->template; ?>"
		data-is_default="<?php print $d->is_default; ?>">
		<td><?php print $i; ?></td>
		<td><?php print $d->store_name; ?></td>
		<td><?php print $d->name; ?></td>
		<td><?php print $d->dosage; ?></td>
		<td><?php print $d->authorization; ?></td>
		<td><?php print sb_format_date($d->emission_limit_date); ?></td>
		<td>
			<?php if( $d->is_default ): ?>
			<span class="glyphicon glyphicon-ok"></span>
			<?php else: ?>
			-
			<?php endif; ?>
		</td>
		<td>
			<a href="javascript:;" class="btn btn-default btn-edit btn-sm" title="<?php _e('Edit', 'invoices'); ?>">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="javascript:;" class="btn btn-default btn-delete btn-sm" title="<?php _e('Delete', 'invoices'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table>
</div>
<?php if(!$mb_exists): ?></form></div><?php endif; ?>
<div id="modal-dosage" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h2 id="dasage-title"></h2>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php _e('Name', 'invoices'); ?></label>
					<input type="text" id="dname" name="dname" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Dosage', 'invoices'); ?></label>
					<input type="text" id="ddosage" name="ddosage" value="" class="form-control" />
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label><?php _e('Authorization', 'invoices'); ?></label>
							<input type="text" id="dauth" name="dauth" value="" class="form-control" />
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label><?php _e('Emission Limit Date', 'invoices'); ?></label>
							<input type="text" id="dlimitdate" name="dlimitdate" value="" class="form-control datepicker" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?php _e('Header Text', 'invoices'); ?></label>
					<textarea id="dheader_text" name="dheader_text" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label><?php _e('Footer Text', 'invoices'); ?></label>
					<textarea id="dfooter_text" name="dfooter_text" class="form-control"></textarea>
				</div>
				<?php if( is_array($stores) ): ?>
				<div class="form-group">
					<label><?php _e('Store', 'invoices'); ?></label>
					<select id="dstore_id" name="dstore_id" class="form-control">
						<option value="-1"><?php _e('-- store --', 'invoices'); ?></option>
						<?php foreach($stores as $store): ?>
						<option value="<?php print $store->store_id; ?>"><?php print $store->store_name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<?php else: ?>
				<input type="hidden" id="dstore_id" name="dstore_id" value="0" />
				<?php endif; ?>
				<div class="form-group">
					<label>
						<?php _e('Default', 'invoices'); ?>
						<input type="checkbox" id="d_is_default" name="d_is_default" value="1" />
					</label>
				</div>
				<div class="form-group">
					<label><?php _e('Template', 'invoices'); ?></label>
					<select id="dtemplate" name="dtemplate" class="form-control">
						<?php foreach($this->templates as $tpl): if( empty($tpl['Template name']) ) continue; ?>
						<option value="<?php print basename($tpl['template_file']); ?>"><?php print $tpl['Template name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'invoices'); ?></button>
        		<button type="button" id="btn-save-dosage" class="btn btn-primary"><?php _e('Save', 'invoices'); ?></button>
			</div>
		</div>
	</div>
</div>
<script>
var row_tpl = '<tr data-id="{id}"\
				data-store_id={store_id}\
				data-name="{name}"\
				data-dosage="{dosage}"\
				data-authorization="{auth}"\
				data-emission_limit_date="{date}"\
				data-header_text="{htext}"\
				data-footer_text="{ftext}"\
				data-template="{tpl}"\
				data-is_default="{default}">\
				<td>{i}</td>\
				<td>{store}</td>\
				<td>{name}</td>\
				<td>{dosage}</td>\
				<td>{auth}</td>\
				<td>{date}</td>\
				<td>{is_default}</td>\
				<td>\
					<a href="javascript:;" class="btn btn-default btn-edit btn-sm" title="<?php _e('Edit', 'invoices'); ?>">\
						<span class="glyphicon glyphicon-edit"></span>\
					</a>\
					<a href="javascript:;" class="btn btn-default btn-delete btn-sm" title="<?php _e('Delete', 'invoices'); ?>">\
						<span class="glyphicon glyphicon-trash"></span>\
					</a>\
				</td>\
			</tr>';
var dosage_id = null;

function ResetDosageForm()
{
	dosage_id = null;
	jQuery('#dname, #ddosage, #dauth, #dlimitdate, #dheader_text, #dfooter_text').val('');
	jQuery('#dstore_id').val('-1');
	jQuery('#dtemplate').val('default.php');
}
function FillDosages()
{
	jQuery('#table-dosages tbody').html('');
	jQuery.get('index.php?mod=invoices&task=get_dosages', function(res)
	{
		if( res.status == 'ok' )
		{
			jQuery.each(res.dosages, function(i, d)
			{
				jQuery('#table-dosages tbody').append(row_tpl.replace(/{i}/g, i + 1)
						.replace(/{id}/g, d.id)
						.replace(/{store_id}/g, d.store_id)
						.replace(/{store}/g, d.store_name)
						.replace(/{name}/g, d.name)
						.replace(/{dosage}/g, d.dosage)
						.replace(/{auth}/g, d.authorization)
						.replace(/{date}/g, d.emission_limit_date)
						.replace(/{htext}/g, d.header_text)
						.replace(/{ftext}/g, d.footer_text)
						.replace(/{tpl}/g, d.template)
						.replace(/{default}/g, d.is_default)
						.replace(/{is_default}/g, parseInt(d.is_default) == 1 ? '<span class="glyphicon glyphicon-ok"></span>' : '-')
				);
			})
		}
	});
}
jQuery(function()
{
	jQuery('#btn-new-dosage').click(function()
	{
		jQuery('#dasage-title').html('<?php _e('Add New Dosage', 'invoices'); ?>');
		ResetDosageForm();
		jQuery('#modal-dosage').modal('show');
	});
	jQuery(document).on('click', '.btn-edit', function()
	{
		var row = jQuery(this).parents('tr:first').get(0);
		dosage_id = row.dataset.id;
		jQuery('#dasage-title').html('<?php _e('Edit Dosage', 'invoices'); ?>');
		//##fill form data
		jQuery('#dstore_id').val(row.dataset.store_id);
		jQuery('#dname').val(row.dataset.name);
		jQuery('#ddosage').val(row.dataset.dosage);
		jQuery('#dauth').val(row.dataset.authorization);
		jQuery('#dlimitdate').val(row.dataset.emission_limit_date);
		jQuery('#dheader_text').val(row.dataset.header_text);
		jQuery('#dfooter_text').val(row.dataset.footer_text);
		jQuery('#d_is_default').get(0).checked = parseInt(row.dataset.is_default) == 1;
		jQuery('#dtemplate').val(row.dataset.template || 'default.php');
		jQuery('#modal-dosage').modal('show');
	});
	jQuery(document).on('click', '.btn-delete', function()
	{
		var row = jQuery(this).parents('tr:first').get(0);
		jQuery.get('index.php?mod=invoices&task=delete_dosage&id='+row.dataset.id, function(res)
		{
			FillDosages();
		});
	});
	jQuery('#btn-save-dosage').click(function()
	{
		var params = 'mod=invoices&task=save_dosage';
		if( dosage_id )
		{
			params += '&id=' + dosage_id;
		}
		jQuery('#dname, #ddosage, #dauth, #dlimitdate, #dheader_text, #dfooter_text, #dtemplate, #d_is_default, #dstore_id').each(function(i, input)
		{
			if( (input.type == 'checkbox' || input.type == 'radio') )
			{
				if( input.checked )
					params += '&' + input.name + '=' + input.value;
			}
			else
			{
				if( input.name == 'ddosage' )
				{
					params += '&' + input.name + '=' + btoa(input.value.trim());
				}
				else
					params += '&' + input.name + '=' + input.value;
			}
		});
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				FillDosages();
			}
			else
			{
			}
		});
		ResetDosageForm();
		dosage_id = null;
		jQuery('#modal-dosage').modal('hide');
		return false;
	});
});
</script>