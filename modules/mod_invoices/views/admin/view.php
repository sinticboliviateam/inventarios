<?php
extract((array)$ops);
?>
<style type="text/css">
#print-preview
{
	margin:10px auto;
	padding:10px;
	border:1px solid #000;
	box-shadow:5px 5px 0px 0px #000;
	background:#fff;
	position:relative;
}
@media print
{
	body, #container, #content, .wrap{margin:0 !important;padding:0 !important;}
	#menu,#footer,#mobile-menu-right{display:none !important;}
	.no-print{display:none !important;}
	#print-preview{width:<?php print $width;?>;margin:0 !important;display:block !important;border:0;}
}
</style>
<div class="wrap">
	<h3 class="text-center"><?php _e('Preview', 'invoices'); ?></h3>
	<div id="print-preview">
		<?php include $invoice_tpl; ?>
	</div>
	<div class="no-print" style="text-align:center;">
		<a href="<?php print SB_Route::_('index.php?mod=invoices'); ?>" class="btn btn-danger"><?php _e('Back', 'invoices'); ?></a>
		<!-- 
		<a href="javascript:;" onclick="mb_print_invoice();" class="btn btn-success">
			<span class="glyphicon glyphicon-print"></span> <?php print SBText::_('Print', 'invoices'); ?>
		</a>
		-->
		<a href="<?php print SB_Route::_('index.php?mod=invoices&view=view&print=1&id='.$invoice->invoice_id); ?>" class="btn btn-info">
			<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'invoices'); ?></a>
	</div>
</div>
