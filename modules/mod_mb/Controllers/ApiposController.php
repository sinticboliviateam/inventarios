<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Controllers;
use SinticBolivia\SBFramework\Modules\Mb\Models\PosModel;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Order;
use SinticBolivia\SBFramework\Modules\Customers\Models\CustomersModel;
use SinticBolivia\SBFramework\Classes\SB_ApiRest;
use SinticBolivia\SBFramework\Modules\Users\Classes\ApiBase;
use SinticBolivia\SBFramework\Modules\Customers\Entities\Customer;
use SinticBolivia\SBFramework\Classes\SB_ApiRestException;
use Exception;

class ApiposController extends ApiBase
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var PosModel
     */
    protected $posModel;
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Customers\Models
     * @var CustomersModel
     */
    protected $customersModel;
    /**
     * @method PROTECTED
     * @method POST
     */
    public function TaskCreate()
    {
        $data       = $this->request->ToJSON();
        //print_r($data);die();
        $sale       = new MB_Order();
        
        try
        {
            $sale->Bind($data);
            if( $sale->customer )
            {
                $customer = new Customer();
                $customer->Bind($sale->customer);
                $sale->customer = $customer;
            }
            $newSale = $this->posModel->RegisterSale($sale, $customerCreated);
            $this->Response($newSale);
        }
        catch(Exception $e)
        {
            throw new SB_ApiRestException($e->getMessage(), $this, 500);
        }
    }
    /**
     * @method GET
     */
    public function TaskGet()
    {
        $id = $this->request->getInt('id');
        if( !$id )
            throw new SB_ApiRestException($this->__('Invalid sale identifier'), $this, 500);
        $sale = MB_Order::Get($id);
        $this->Response($sale);
    }
}
