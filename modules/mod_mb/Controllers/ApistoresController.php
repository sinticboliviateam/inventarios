<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Modules\Mb\Controllers;
use SinticBolivia\SBFramework\Classes\SB_ApiRest;
use SinticBolivia\SBFramework\Modules\Mb\Models\StoresModel;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store;
use SinticBolivia\SBFramework\Classes\SB_ApiRestException;
use Exception;
use SinticBolivia\SBFramework\Modules\Users\Classes\ApiBase;

class ApistoresController extends ApiBase
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var StoresModel 
     */
    protected $storesModel;
    
    /**
     * @access PROTECTED
     * @method GET
     */
    public function TaskShow()
    {
        try
        {
            $stores = MB_Store::GetRows(-1);
            $this->Response($stores);
        }
        catch(Exception $e)
        {
            $this->ResponseError($e->getMessage());
        }
    }
    /**
     * @access PROTECTED
     * @method GET
     */
    public function TaskGet()
    {
        $id = $this->request->getInt('id');
        $store = MB_Store::Get($id);
        if( !$store )
            throw new SB_ApiRestException($this->__('The store does not exists'), $this);
        $this->Response($store);
    }
    /**
     * @access PROTECTED
     * @method POST
     * @throws SB_ApiRestException
     */
    public function TaskCreate()
    {
        $data = $this->request->ToJSON();
        
        if( !$data )
            throw new SB_ApiRestException($this->__('Invalid store data'), $this);
        
        $store = new MB_Store();
        $store->Bind($data);
        
        $newStore = $store->Save();
        $this->Response($newStore);
    }
    /**
     * @access PROTECTED
     * @method PUT
     */
    public function TaskUpdate()
    {
        $data = $this->request->ToJSON();
        if( !$data )
            throw new SB_ApiRestException($this->__('Invalid store data'), $this);
        $store = new MB_Store();
        $store->Bind($data);
        if( !$store->store_id )
            throw new SB_ApiRestException($this->__('Invalid store identifier'), $this);
        $updatedStore = $store->Save();
        
        $this->Response($updatedStore);
    }
    /**
     * @access PROTECTED
     * @method DELETE
     */
    public function TaskDelete()
    {
        $data = $this->request->ToJSON();
        if( !$data )
            throw new SB_ApiRestException($this->__('Invalid store data'), $this);
        $store = new MB_Store();
        $store->Bind($data);
        if( !$store->store_id )
            throw new SB_ApiRestException($this->__('Invalid store identifier'), $this);
        
        $this->storesModel->Delete($store);
        
    }
}
