<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Controllers;

use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Brand;
use Exception;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Modules\Mb\Entities\SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Classes\SB_TableList;

class BrandsAdminController extends SB_Controller
{
	public function TaskDefault()
	{
		$limit	= 25;
		$offset = 0;
		//$brands = MB_Brand::GetRows($limit, $offset);
		$title	= __('Brands Management', 'mb');
		$table	= new SB_TableList('mb_brands', 'id', 'mb');
		$table->SetColumns([
				'id'						=> ['label' => 'ID'],
				'name'						=> ['label' => __('Name', 'mb')],
				'last_modification_date'	=> ['show' => false],
				'creation_date'				=> ['show' => false]
		]);
		$table->SetRowActions([
				'brands.edit'			=> ['label' => __('Edit', 'mb'), 'icon' => 'glyphicon glyphicon-edit'],
				'task:brands.delete'	=> [
						'label' => __('Delete', 'mb'), 
						'icon'	=> 'glyphicon glyphicon-trash', 'class' => 'confirm',
						'data'	=> ['message' => __('Are you sure to delete the brand?', 'mb')] 
				]
		]);
		$table->Fill();
		$this->SetView('brands/default');
		$this->SetVars(get_defined_vars());
		$this->document->SetTitle($title);
	}	
	public function TaskNew()
	{
		$title = __('Create New Brand', 'mb');
		$this->SetView('brands/new');
		$this->SetVars(get_defined_vars());
		$this->document->SetTitle($title);
	}
	public function TaskEdit()
	{
		$id = $this->request->getInt('id');
		try
		{
			if( !$id )
				throw new Exception(__('Invalid brand identifier', 'mb'));
			$brand = MB_Brand::Get($id);
			if( !$brand )
				throw new Exception(__('The brand does not exists', 'mb'));
			
			$title = __('Edit Brand', 'mb');
			$this->SetVars(get_defined_vars());
			$this->SetView('brands/new');
			$this->document->SetTitle($title);
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($this->Route('index.php?mod=mb&view=brands.default'));
		}
		
	}
	public function TaskDelete()
	{
		try
		{
			$id = $this->request->getInt('id');
			if( !$id )
				throw new Exception(__('The brand identifier is invalid', 'mb'));
			$brand = MB_Brand::Get($id);
			if( !$brand )
				throw new Exception(__('The brand does not exists', 'mb'));
			$brand->Delete();
			SB_MessagesStack::AddSuccess(__('The brand has been deleted', 'mb'));
			sb_redirect($this->Route('index.php?mod=mb&view=brands.default'));
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($this->Route('index.php?mod=mb&view=brands.default'));
		}
	}
	public function TaskSave()
	{
		try
		{
			$id				= $this->request->getInt('id');
			//print_r($_FILES);die();
			$brand 			= $id ? MB_Brand::Get($id) : new MB_Brand();
			//$brand->id		= $id;
			$brand->name 	= $this->request->getString('name');
			$brand 			= $brand->Save();
			if( isset($_FILES['image']) && $_FILES['image']['size'] > 0 )
			{
				$filename = sb_get_unique_filename($_FILES['image']['name'], UPLOADS_DIR);
				
				if( !move_uploaded_file($_FILES['image']['tmp_name'], $filename) )
					throw new Exception(__('Error uploading brand image', 'mb'));
				$img_id 			= lt_insert_attachment($filename, 'brand', $brand->id, 0, 'image');
				//var_dump($img_id);die();
				$brand->image_id 	= $img_id;
				$brand->Save(); 
			}
			SB_MessagesStack::AddSuccess(__('The brand has been saved', 'mb'));
			sb_redirect($this->Route('index.php?mod=mb&view=brands.default'));
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($this->Route('index.php?mod=mb&view=brands.default'));
		}
	}
}