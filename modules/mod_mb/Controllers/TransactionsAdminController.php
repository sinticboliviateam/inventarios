<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Controllers;
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SB_Warehouse;

class TransactionsAdminController extends SB_Controller
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var TransactionTypesModel
     */
    protected $transactionTypesModel;
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var TransactionRecordsModel
     */
    protected $transactionRecordsModel;
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var StoresModel
     */
    protected $storesModel;
    
    public function TaskDefault()
    {
        $user = sb_get_current_user();
        sb_add_style('mb-default', MOD_MB_URL . '/css/default.css');
        $title = __('Transactions Record', 'mb');
        $this->document->SetTitle($title);
        $this->SetView('transactions/default');
        $transaction_types  = $this->transactionTypesModel->GetAll();
        $stores             = $user->can('mb_see_all_stores') ? $this->storesModel->GetAll() : SB_Warehouse::GetUserStores($user);
        $this->SetVars(get_defined_vars());
    }
    public function ajax_getrecords()
    {
        $user               = sb_get_current_user();
        $page               = $this->request->getInt('page', 1);
        $store_id           = $this->request->getInt('store_id');
        $transactionTypeId  = $this->request->getInt('transaction_type_id');
        $fromDate           = $this->request->getDate('from_date');
        $toDate             = $this->request->getDate('to_date');
        
        $limit  = 50;
        if( $page <= 0 )
            $page = 1;
        if( !$user->can('mb_see_all_orders') )
        {
            $user_id    = $user->user_id;
            $store_id   = (int)$user->_store_id;
        }
        else
        {
            $user_id = null;
        }
        $res = $this->transactionRecordsModel->GetAll($store_id, $transactionTypeId, $user_id, $fromDate, $toDate, $page, $limit);
        
        ob_start();
        lt_pagination($this->Route('index.php?'.$_SERVER['QUERY_STRING']), $res['total_pages'], $page);
        $pagination = ob_get_clean();
        sb_response_json(array('status' => 'ok', 'items' => $res['rows'], 'total_rows' => $res['total_rows'], 'total_amount' => $res['total_amount'], 'pagination' => $pagination));
    }
}
