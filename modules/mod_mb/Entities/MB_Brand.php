<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;

use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
use SinticBolivia\SBFramework\Classes\SB_AttachmentImage;
use SinticBolivia\SBFramework\Classes\SinticBolivia\SBFramework\Classes;
/**
 * 
 * @author marcelo
 * @table mb_brands
 */
class MB_Brand extends SB_DbEntity
{
	/**
	 * @primaryKey true
	 * @var INTEGER
	 */
	public 	$id;
	/**
	 * 
	 * @var STRING
	 */
	public	$name;
	/**
	 * 
	 * @var BIGINT
	 */
	public	$image_id;
	/**
	 * 
	 * @var \SinticBolivia\SBFramework\Classes\SB_AttachmentImage
	 */
	protected	$image;
	
	protected function AfterBind($data)
	{
		if( $this->image_id )
			$this->image = new SB_AttachmentImage($this->image_id); 
	}
}