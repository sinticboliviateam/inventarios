<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;

use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
/**
 * 
 * @author marcelo
 * @table mb_categories
 */
class MB_Category extends SB_DbEntity
{
	public	$category_id;
	public	$name;
	public	$description;
	public	$parent;
	public	$extern_id;
	public	$store_id;
	public	$slug;
	public	$extern_type;
	public	$image_id;
	
}