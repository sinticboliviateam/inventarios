<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;

/**
 * @table mb_orders
 */
class MB_Order extends SB_DbEntity
{
    const STATUS_PENDING     = 'PENDING';
    const STATUS_COMPLETED   = 'COMPLETE';
    const STATUS_VOID        = 'VOID';
    const STATUS_REVERSED    = 'REVERSED';
    const STATUS_DRAFT       = 'DRAFT';
    /**
     * @primaryKey true
     * @var BIGINTEGER
     */
    public  $order_id;
    /**
     * @var VARCHAR
     */
    public  $code;
    /**
     * @var VARCHAR
     */
    public  $name;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $store_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $supplier_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $transaction_type_id;
    /**
     * @var BIGINTEGER
     */
    public  $sequence;
    /**
     * @var INTEGER
     */
    //public  $items;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $subtotal;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $total_tax;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $discount;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $total;
    /**
     * @var TEXT
     */
    public  $details;
    /**
     * @var VARCHAR
     */
    public  $status;
    /**
     * @var VARCHAR
     */
    public  $payment_status;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $user_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $customer_id;
    /**
     * @var DATETIME
     */
    public  $order_date;
    /**
     * @var DATETIME
     */
    public  $delivery_date;
    /**
     * Sale type
     * pos|web
     * @var VARCHAR
     */
    public  $type;
      
    /**
     * @var array
     */
    public  $meta;
    /**
     * @reference	[customer_id]
     * @hasOne 		SinticBolivia\SBFramework\Modules\Customers\Entities\Customer
     * @var 		SinticBolivia\SBFramework\Modules\Customers\Entities\Customer
     */
    protected  $customer;
    /**
     * @var array
     */
    public  $items;
    /**
     * Get order statuses
     * @return array
     */
    public function GetStatuses()
    {
        return array(
            self::STATUS_COMPLETED  => __('Completed', 'mb'),
            self::STATUS_DRAFT      => __('Draft', 'mb'),
            self::STATUS_PENDING    => __('Pending', 'mb'),
            self::STATUS_REVERSED   => __('Reversed', 'mb'),
            self::STATUS_VOID       => __('Void', 'mb'),
        );
    }
    public function AfterBind($data)
    {
        parent::AfterBind($data);
        
        if( !$this->order_id )
            return false;
        if( $this->items != null )
            return false;
            
        $query = "SELECT i.*, p.product_code, p.product_name ".
						"FROM mb_order_items i ".
						"LEFT JOIN mb_products p ON i.product_id = p.product_id " .
						"WHERE 1 = 1 " .
						"AND i.order_id = {$this->order_id} ".
						"ORDER BY creation_date ASC";
        $this->dbh->Query($query);
		$resultSet      = $this->dbh->GetResultSet();
       
        $this->items    = $resultSet->GetRows();
       
		return true;
    }
    public function GetItems()
    {
        return $this->items;
    }
}
