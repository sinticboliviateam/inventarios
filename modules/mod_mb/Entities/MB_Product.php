<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_AttachmentImage;

/**
 * @table mb_products
 */
class MB_Product extends SB_DbEntity
{
    /**
     * @primaryKey true
     * @var INTEGER 
     */
    public $product_id;
    /**
     * 
     * @var INTEGER 
     */
    public $extern_id;
    /**
     * @var VARCHAR
     */
    public $extern_type;
    /**
     * @var VARCHAR 
     */
    public $product_code;
    /**
     * @var VARCHAR 
     */
    public $product_number;
    /**
     * @var VARCHAR 
     */
    public $stocking_code;
    /**
     * @var VARCHAR 
     */
    public $product_name;
    /**
     * @var VARCHAR 
     */
    public $product_description;
    /**
     * @var INTEGER 
     */
    public $product_line_id;
    /**
     * @var INTEGER
     */
    public $type_id;
    /**
     * @var VARCHAR 
     */
    public $product_model;
    /**
     * @var VARCHAR 
     */
    public $product_barcode;
    /**
     * @var VARCHAR 
     */
    public $product_cost;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $product_price;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $product_price_2;
     /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $product_price_3;
     /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $product_price_4;
    /**
     * @var INTEGER 
     */
    public $product_quantity;
    /**
     * @var INTEGER 
     */
    public $product_unit_measure;
    /**
     * @var INTEGER 
     */
    public $store_id;
    /**
     * @var INTEGER 
     */
    public $user_id;
    /**
     * @var INTEGER 
     */
    public $department_id;
    /**
     * @var VARCHAR 
     */
    public $status;
    /**
     * @var INTEGER 
     */
    public $min_stock;
    /**
     * @var VARCHAR 
     */
    public $product_internal_code;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $width;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL 
     */
    public $height;
    /**
     * @var INTEGER 
     */
    public $unit_pack;
    /**
     * @var VARCHAR 
     */
    public $base_type;
    /**
     * @var VARCHAR
     */
    public $slug;
    /**
     * @var INTEGER 
     */
    public $for_sale;
    /**
     * @var DATETIME 
     */
    public $last_modification_date;
    /**
     * @var DATETIME 
     */
    public $creation_date;
    /**
     * 
     * @var Array
     */
    public $meta = null;
    /**
     * @reference	[store_id]
     * @belongsTo 	SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store
     * @var SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store
     */
    protected $store;
    /**
     * @var SB_AttachmentImage
     */
    protected	$images;
    /**
     * 
     * @var \SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Category
     */
    protected	$categories = null;
    protected	$categories_ids = [];
    
    protected function AfterBind($data)
    {
        /*
        
        */
    }
    public function getImages()
	{
		if( $this->images )
			return $this->images;
			
		if( !$this->product_id )
			return array();
			
	    if( empty($this->images) )
	    {
	    	$query = "SELECT * FROM attachments 
						WHERE object_type = 'product' 
						AND object_id = $this->product_id 
						AND type = 'image'
						AND parent = 0";
	    	$this->images = array();
			foreach($this->dbh->FetchResults($query) as $row)
			{
				$img	= new SB_AttachmentImage();
				$img->SetDbData($row);
				$this->images[] = $img;
			}
	    }
		
	    return $this->images;
	}
    /**
	 * Example: $size => '55x55'
	 * @brief Get product featured image
	 * @param string $size The image size 
	 * @return  string image url
	 */
	public function getFeaturedImage($size = null)
	{
		$this->getImages();
		
		if( !(int)$this->GetMeta('_featured_image_id', null) )
		{
			if( !count($this->images) )
			{
				$img = new SB_AttachmentImage();
				$img->SetDbData((object)array('file' => '../images/no-image.png'));
				return $img;
			}
			else 
			{
				return $size ? current($this->images)->GetThumbnail($size) : current($this->images);
			}
		}
		
		$id = (int)$this->GetMeta('_featured_image_id');
		$img = new SB_AttachmentImage($id);
		$thumb = null;
		if( $size )
		{
			$thumb = $img->GetThumbnail($size);
		}
		
		return $thumb ? $thumb : $img;
	}
	public function GetAsmComponents()
	{
		$query = "SELECT p.product_id, p.product_code, p.product_name, a2p.qty_required 
					FROM mb_assemblie2product a2p, mb_products p 
					WHERE 1 = 1
					AND a2p.product_id = p.product_id
					AND a2p.assembly_id = $this->product_id";
		$coms = $this->dbh->FetchResults($query);
		for($i = 0; $i < count($coms); $i++)
		{
			$coms[$i]->product = new SB_MBProduct($coms[$i]->product_id);
		}
		return $coms;
	}
	public function __get($var)
	{
		if( $var == 'link' )
		{
			$slug = (empty($this->slug) ? sb_build_slug($this->product_name) : $this->slug);
			$link = SB_Route::_('index.php?mod=emono&view=product&id='.$this->product_id.'&slug='. $slug);
			return $link;
		}
		if( $var == 'price' )
		{
			return sprintf("%s", sb_number_format($this->product_price));
		}
		/*
		if( $var == 'product_name' )
		{
			return html_entity_decode($this->_dbData->product_name);
		}
		*/
		if( $var == 'excerpt' )
		{
			$desc = trim(strip_tags($this->product_description));
			return empty($desc) ? __('There is no description', 'mb') : substr($desc, 0, 128) . '...';
		}
		return parent::__get($var);
	}
	public function GetCategoriesName()
	{
		if( $this->categories == null )
			$this->GetDbCategories();
		$cats = array();
		foreach($this->categories as $c)
		{
			$cats[] = $c->name;
		}
		return implode(',', $cats);
	}
	public function GetCategories()
	{
		if( $this->categories == null )
			$this->GetDbCategories();
		return $this->_categories;
	}
	public function GetDbCategories()
	{
		$query = "SELECT c.* ".
				"FROM mb_product2category p2c, mb_categories c " .
				"WHERE c.category_id = p2c.category_id " .
				"AND p2c.product_id = $this->product_id ".
				"ORDER BY c.parent ASC";
		$res = $this->dbh->Query($query);
		if($res)
		{
			foreach($this->dbh->FetchResults() as $r)
			{
				$cat = new \SB_MBCategory();
				$cat->SetDbData($r);
				$this->categories[] = $cat;
				$this->categories_ids[] = $r->category_id;
			}
		}
	}
	/**
	 * Get the top category of product
	 * 
	 * @return SB_MBCategory
	 */
	public function GetTopCategory()
	{
		$cat = null;
		foreach($this->GetCategories() as $_cat)
		{
			if( (int)$_cat->parent === 0 )
			{
				$cat = $_cat;
				break;
			}
		}
		return $cat;
	}
	/**
	 * Return formatted price including currency code
	 * @param string $curreny_code 
	 * @return string formatted price
	 */
	public function GetPrice($currency_code = null)
	{
		$_currency_code = '$';
		if( $currency_code )
			$_currency_code = $currency_code;
		elseif( defined('MB_CURRENCY_CODE') )
		{
			$_currency_code = MB_CURRENCY_CODE;
		}
		$price = sb_number_format((float)b_do_action('mb_product_price', $this->product_price, $this));
		return sprintf("%s %s", empty($_currency_code) ? '$' : $_currency_code, $price);
	}
	public function GetWarehouseQty($warehouse_id)
	{
		if( !(int)$warehouse_id )
			return 0;
		$query = "select quantity from mb_product_quantity where warehouse_id = $warehouse_id and product_id = $this->product_id";
		return (int)$this->dbh->GetVar($query);
	}
	public function GetWarehouseMinStock($warehouse_id)
	{
		if( !(int)$warehouse_id )
			return 0;
		$query = "select min_stock from mb_product_quantity where warehouse_id = $warehouse_id and product_id = $this->product_id";
		return (int)$this->dbh->GetVar($query);
	}
	public function jsonSerialize()
	{
		$data 				= parent::jsonSerialize();
		$data->link 		= $this->link;
		$data->priceHtml 	= $this->GetPrice();
		$data->thumbnailUrl	= $this->getFeaturedImage('150x150')->GetUrl();
		return $data;
	}
}
