<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use Exception;

/**
 * @table mb_purchase_orders
 */
class MB_PurchaseOrder extends SB_DbEntity
{
    const STATUS_CREATED        = 'CREATED';
    const STATUS_WAITING_STOCK  = 'WAITING_STOCK';
    const STATUS_PENDING        = 'PENDING';
    const STATUS_COMPLETED      = 'COMPLETE';
    const STATUS_VOID           = 'VOID';
    const STATUS_REVERSED       = 'REVERSED';
    const STATUS_DRAFT          = 'DRAFT';
    const STATUS_DELETED        = 'DELETED';
    
    /**
     * @primaryKey true
     * @var BIGINTEGER
     */
    public  $order_id;
    /**
     * @var VARCHAR
     */
    public  $code;
    /**
     * @var VARCHAR
     */
    public  $name;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $store_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $warehouse_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $supplier_id;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $transaction_type_id;
    /**
     * @var BIGINTEGER
     */
    public  $sequence;
     /**
     * @var BIGINTEGER
     */
    public  $tax_id;
    /**
     * @digits 10
     * @precision 2    
     * @var DECIMAL
     */
    public  $tax_rate;
    /**
     * @var INTEGER
     */
    //public  $items;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $subtotal;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $total_tax;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $discount;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $total;
    /**
     * @var TEXT
     */
    public  $details;
    /**
     * @var VARCHAR
     */
    public  $status;
    /**
     * @foreignKey true
     * @var BIGINTEGER
     */
    public  $user_id;
    /**
     * @var DATETIME
     */
    public  $order_date;
    /**
     * @var DATETIME
     */
    public  $delivery_date;
    /**
     * @var array
     */
    public  $meta;
    /**
     * @reference 	[supplier_id]
     * @hasOne		SinticBolivia\SBFramework\Modules\Provider\Entities\Supplier 
     * @var SinticBolivia\SBFramework\Modules\Provider\Entities\Supplier 
     */
    protected	$supplier;
    /**
     * @reference 	[store_id]
     * @belongsTo	SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store 
     * @var SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store 
     */
    protected	$store;
    /**
     * @reference 	[transaction_type_id]
     * @hasOne		SinticBolivia\SBFramework\Modules\Mb\Entities\MB_TransactionType 
     * @var SinticBolivia\SBFramework\Modules\Mb\Entities\MB_TransactionType
     */
    protected	$transactionType;
    /**
     * @var array
     */
    protected	$items;
    /**
     * Get order statuses
     * @return array
     */
    public function GetStatuses()
    {
        return array(
            self::STATUS_COMPLETED  => __('Completed', 'mb'),
            self::STATUS_DRAFT      => __('Draft', 'mb'),
            self::STATUS_PENDING    => __('Pending', 'mb'),
            self::STATUS_REVERSED   => __('Reversed', 'mb'),
            self::STATUS_VOID       => __('Void', 'mb'),
        );
    }
    public function AfterBind($data)
    {
        parent::AfterBind($data);
        
        if( !$this->order_id )
            return false;
        if( $this->items != null )
            return false;
            
        $query = "SELECT i.*, p.product_code, p.product_name ".
						"FROM mb_order_items i ".
						"LEFT JOIN mb_products p ON i.product_id = p.product_id " .
						"WHERE 1 = 1 " .
						"AND i.order_id = {$this->order_id} ".
						"ORDER BY creation_date ASC";
        $this->dbh->Query($query);
		$resultSet      = $this->dbh->GetResultSet();
       
        $this->items    = $resultSet->GetRows();
       
		return true;
    }
    public function GetItems()
    {
        return $this->items;
    }
    protected function BeforeInsert()
    {
		$this->sequence = self::GetNewSequence($this->store_id, date('Y'));
		if( !$this->sequence )
		    throw new Exception(__('Unable to insert purchase order, the secuence is invalid'));
		$this->code     = sprintf("%s-%d-%s%s", 
									$this->__get('transactionType')->transaction_key, 
									$this->sequence, 
									date('Ymd'), 
									$this->__get('store')->code);
	}
	public static function GetNewSequence($store_id, $year)
	{
		$dbh = SB_Factory::getDbh();
		$sequence = 0;
		if( $dbh->db_type == 'mysql' )
		{
			$query = "SELECT MAX(sequence) " .
			         "FROM mb_purchase_orders " .
			         "WHERE store_id = $store_id " .
			         "AND YEAR(creation_date) = '$year' " .
			         "ORDER BY creation_date " .
			         "DESC LIMIT 1";
			$sequence = (int)$dbh->GetVar($query);
		}
		elseif( $dbh->db_type == 'sqlite3' )
		{
			$query = "SELECT MAX(sequence) " .
			 			"FROM mb_purchase_orders " .
			 			"WHERE store_id = $store_id " .
			 			"AND strftime('%Y', creation_date) = '$year' " .
			 			"ORDER BY creation_date DESC " .
			 			"LIMIT 1";
			$sequence = (int)$dbh->GetVar($query);
		}
		$sequence++;
		
		return $sequence;
	}
}
