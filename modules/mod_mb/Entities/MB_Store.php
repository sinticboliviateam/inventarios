<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;

/**
 * @table mb_stores
 */
class MB_Store extends SB_DbEntity
{
    /**
     * @primaryKey true
     * @var INTEGER
     */
    public $store_id;
    /**
     * @var VARCHAR 
     */
    public $code;
    /**
     * @var VARCHAR 
     */
    public $store_name;
    /**
     * @var VARCHAR 
     */
    public $store_address;
    /**
     * @var VARCHAR 
     */
    public $phone;
    /**
     * @var VARCHAR 
     */
    public $fax;
    /**
     * @var VARCHAR 
     */
    public $store_key;
    /**
     * @var VARCHAR 
     */
    public $store_description;
    /**
     * @var VARCHAR 
     */
    public $store_type;
    /**
     * @var INTEGER 
     */
    public $sales_transaction_type_id;
    /**
     * @var INTEGER 
     */
    public $purchase_transaction_type_id;
    /**
     * @var DATETIME 
     */
    public $last_modification_date;
    /**
     * @var DATETIME 
     */
    public $creation_date;
    /**
     * @var Array 
     */
    public $meta = array();
    
    public function BuildKey()
    {
        $this->store_key = sb_build_slug($this->store_name);
    }
    protected function AfterBind($data)
    {
        if( !$this->store_id || count($this->meta) )
            return false;
        $this->meta = $this->dbh->FetchResults("SELECT * FROM mb_store_meta WHERE store_id = {$this->store_id}");
    }
    public function Save($getNewRecord = true)
    {
        $this->BuildKey();
        return parent::Save($getNewRecord);
    }
}
