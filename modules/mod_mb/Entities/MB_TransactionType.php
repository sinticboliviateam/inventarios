<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
/**
 * @table mb_transaction_types
 */
class MB_TransactionType extends SB_DbEntity
{
	/**
	 * @primaryKey true
	 * @var BIGINT
	 */
	public	$transaction_type_id;
	/**
	 * @var VARCHAR
	 */
    public	$transaction_key;
    /**
     * @var VARCHAR
     */
    public	$transaction_name;
    /**
     * @var TEXT
     */
    public	$transaction_description;
    /**
     * @var VARCHAR
     */
    public	$in_out;
    /**
     * @var BIGINT
     */
    public	$store_id;
    
}
