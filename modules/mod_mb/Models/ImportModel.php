<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;

use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Product;
use SinticBolivia\SBFramework\Modules\Mb\Models\StoresModel;
use SB_Warehouse;
use Exception;

class ImportModel extends SB_Model
{
    protected $_cols = array('A' => 0,'B' => 1, 'C' => 2, 'D' => 3, 'E' => 4, 'F' => 5, 'G' => 6, 'H' => 7, 'I' => 8,
							'J' => 9, 'K' => 10, 'L' => 11, 'M' => 12, 'N' => 13, 'O' => 14, 'P' => 15, 'Q' => 16,
							'R' => 17, 'S' => 18, 'T' => 19, 'U' => 20, 'V' => 21, 'W' => 22, 'X' => 23, 'Y' => 24,
							'Z' => 25);
    /**
     * @var \SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel
     */
    protected $productModel;
    
    public function ImportExcel($columnsIndex, $excelFile, $storeId, $warehouseId = null, $category_id = 0, 
                                $initial_stock = false, $update_only = false)
    {
       
        if( $columnsIndex->code_column < 0 )
            throw new Exception($this->__('The product code column is invalid'));
        if( !$storeId )
            throw new Exception($this->__('Invalid store identifier'));
        
        $warehouse = null;
        if( !$warehouseId ) 
        {
            //##get the first and older warehouse
            $query = "SELECT w.* FROM mb_warehouse w WHERE 1 = 1 ";
			$query .= "AND w.store_id = $storeId ";
			$query .= "ORDER BY w.creation_date ASC LIMIT 1";
			$warehouse = $this->dbh->FetchRow($query);
        }
        if( !$warehouse )
            throw new Exception($this->__('There are no warehouses for the store'));
            
        sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
        sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
        
        $xls 		= \PHPExcel_IOFactory::load($excelFile);
        $sheet 		= $xls->setActiveSheetIndex($columnsIndex->sheet_num);
       
        //$sheet = $xls->getActiveSheet();
        //$sheet 		= (!$sheet_num) ? $xls->getActiveSheet() : $xls->getSheet($sheet_num);
        $total_rows = $sheet->getHighestRow();
        $total_cols = $sheet->getHighestColumn();
        
        $images		= array();
        if( $columnsIndex->image_column > 0 )
        {
            //##extract all images
            foreach($sheet->getDrawingCollection() as $drawing)
            {
                $string 	= $drawing->getCoordinates();
                $coordinate = \PHPExcel_Cell::coordinateFromString($string);
                if( is_a($drawing, 'PHPExcel_Worksheet_Drawing') )
                {
                    $filename = $drawing->getPath();
                    //$drawing->getDescription();
                    //copy($filename, MOD_MB_PROD_IMAGE_DIR . SB_DS . basename($filename));
                    $index = '_'.$_cols[$coordinate[0]] . $coordinate[1];
                    $images[$index] = array(
                        'filename'		=> $filename,
                        'description'	=> $drawing->getDescription(),
                        'coordinate'	=> $coordinate,
                        'string'		=> $string
                    );
                }
            }
        }
        $inserts = 0;
        $updated = 0;
        $this->dbh->BeginTransaction();
        for($row = $columnsIndex->row_start; $row <= $total_rows; $row++)
        {
            
            $product_code 	    = trim($sheet->getCellByColumnAndRow($columnsIndex->code_column, $row)->getCalculatedValue());
            $product_barcode    = $columnsIndex->barcode_column >= 0 ? 
                                    trim($sheet->getCellByColumnAndRow($columnsIndex->barcode_column, $row)->getCalculatedValue()) :
                                    '';
            $product_name 	    = $columnsIndex->name_column > -1 ? 
                                    trim($sheet->getCellByColumnAndRow($columnsIndex->name_column, $row)->getCalculatedValue()) : 
                                    '';
            $product_desc 	    = $columnsIndex->desc_column >= 0 ? 
                                    trim($sheet->getCellByColumnAndRow($columnsIndex->desc_column, $row)->getCalculatedValue()) : 
                                    '';
            $product_qty 	    = $columnsIndex->qty_column >= 0 ? trim($sheet->getCellByColumnAndRow($columnsIndex->qty_column, $row)->getCalculatedValue()) : 0;
            $product_cost	    = $columnsIndex->cost_column != -1 ? $sheet->getCellByColumnAndRow($columnsIndex->cost_column, $row)->getCalculatedValue() : 0;
            $product_price	    = $columnsIndex->price_column != -1 ? $sheet->getCellByColumnAndRow($columnsIndex->price_column, $row)->getCalculatedValue() : 0;
            $product_price_2	= $columnsIndex->price_column_2 != -1 ? $sheet->getCellByColumnAndRow($columnsIndex->price_column_2, $row)->getCalculatedValue() : 0;
            $product_price_3	= $columnsIndex->price_column_3 != -1 ? $sheet->getCellByColumnAndRow($columnsIndex->price_column_3, $row)->getCalculatedValue() : 0;
            $product_price_4	= $columnsIndex->price_column_4 != -1 ? $sheet->getCellByColumnAndRow($columnsIndex->price_column_4, $row)->getCalculatedValue() : 0;
            ##check if product code is empty
            if( empty($product_code) )
                continue;
            $product_qty 		= (int)trim($product_qty);
            $product_cost		= (float)str_replace(',', '.', trim($product_cost));
            $product_price		= (float)str_replace(',', '.', trim($product_price));
            $product_price_2	= (float)str_replace(',', '.', trim($product_price_2));
            $product_price_3	= (float)str_replace(',', '.', trim($product_price_3));
            $product_price_4	= (float)str_replace(',', '.', trim($product_price_4));
            $product = null;
            //var_dump($product_code, $storeId);die();
            //##check if product already exists
            if( $p = SB_Warehouse::GetProductBy('code', $product_code, $storeId) )
            {
                $product = new MB_Product();
                $product->Bind($p);
                //print_r($product);
                //die;
                //##try to update values
                if( !empty($product_name) )
                    $product->product_name      = $product_name;
                if( $product_cost > 0 )
                    $product->product_cost      = $product_cost;
                if( $product_price > 0 )
                    $product->product_price     = $product_price;
                if( $product_price_2 > 0 )
                    $product->product_price_2   = $product_price_2;
                if( $product_price_3 > 0 )
                    $product->product_price_3   = $product_price_3;
                if( $product_price_4 > 0 )
                    $product->product_price_4   = $product_price_4;
                if( !empty($product_barcode) )
                    $product->product_barcode   = $product_barcode;
                //print_r($product);die;
                if( $product_qty > 0 )
                {
                    $product->product_quantity = $this->productModel->SetQuantity($product->product_id, $storeId, $warehouse->id, $product_qty);
                }
                //print_r($columnsIndex);print_r($product);die("propduct_qty: $product_qty");
                //##update the product
                $product->Save();
                if( $initial_stock )
                {
                    //##create initial product kardex
                    $kardex = array(
                        'product_id'			=> $product->product_id,
                        'in_out'				=> 'input',
                        'quantity'				=> $product_qty,
                        'quantity_balance'		=> $product->product_quantity,
                        'unit_price'            => $product->product_cost,
                        'total_amount'          => $product_qty * $product->product_cost,
                        'monetary_balance'      => $product_qty * $product->product_cost,
                        'transaction_type_id'	=> -1,
                        'author_id'				=> sb_get_current_user()->user_id,
                        'transaction_id'		=> -1,
                        'creation_date'			=> date('Y-m-d H:i:s')
                    );
                    $this->dbh->Insert('mb_product_kardex', $kardex);
                }
                $updated++;
            }     
            else//if( !$update_only )
            {
                $product 						= new MB_Product();
                $product->product_code  		= $product_code;
                $product->product_name  		= empty($product_name) ? $product_code : $product_name;
                $product->product_description   = $product_desc;
                $product->product_cost          = $product_cost;
                $product->product_price         = $product_price;
                $product->product_price_2		= $product_price_2;
                $product->product_price_3       = $product_price_3;
                $product->product_price_4       = $product_price_4;
                $product->store_id  			= $storeId;
                $product->type_id               = 0;
                $product->min_stock             = 1;
                $product->product_unit_measure	= 0;
                $product->status				= 'publish';
                $product->base_type             = 'base';
                $product->for_sale  			= 1;
                $product->product_barcode		= $product_barcode;
                $product->product_number		= '';
                $product->last_modification_date= date('Y-m-d H:i:s');
                $product->creation_date         = date('Y-m-d H:i:s');
                
                SB_Module::do_action_ref('mb_before_import_item', $product);
                //##insert a new product
                $product->product_id = $product->Save(false);
                $this->productModel->SetQuantity($product->product_id, $storeId, $warehouse->id, $product_qty);
                $inserts++;
                if( $initial_stock )
                {
                    //##create initial product kardex
                    $kardex = array(
                        'product_id'			=> $product->product_id,
                        'in_out'				=> 'input',
                        'quantity'				=> $product_qty,
                        'quantity_balance'		=> $product_qty,
                        'unit_price'            => $product->product_cost,
                        'total_amount'          => $product_qty * $product->product_cost,
                        'monetary_balance'      => $product_qty * $product->product_cost,
                        'transaction_type_id'	=> -1,
                        'author_id'				=> sb_get_current_user()->user_id,
                        'transaction_id'		=> -1,
                        'creation_date'			=> date('Y-m-d H:i:s')
                    );
                    $this->dbh->Insert('mb_product_kardex', $kardex);
                }
            }
            
            if( $product && $product->product_id )
            {
                //##set featured image
                $img_file = MOD_MB_PROD_IMAGE_DIR . SB_DS . strtoupper($product->product_code) . '.jpg';
                if( file_exists($img_file) )
                {
                    if( $aid = lt_insert_attachment($img_file, 'product', $product->product_id, 0, 'image') )
                    {
                        mb_update_product_meta($product->product_id, '_featured_image', $aid);
                    }
                }
                //##check for image
                $image_index = '_' . $columnsIndex->image_column . $row;
                if( $columnsIndex->image_column > 0 && count($images) && isset($images[$image_index]) )
                {
                    $dest_image = sb_get_unique_filename(basename($images[$image_index]['filename']), 
                                                            MOD_MB_PROD_IMAGE_DIR);
                    copy($images[$image_index]['filename'], $dest_image);
                    lt_insert_attachment($dest_image, 'product', $product->product_id, 0, 'image');
                }
                //##check for product category
                if( $category_id > 0 )
                {
                    $this->dbh->Query("DELETE FROM mb_product2category WHERE product_id = $product->product_id LIMIT 1");
                    $this->dbh->Insert('mb_product2category', array('product_id' => $product->product_id, 
                                                                    'category_id' => $category_id));
                }
            }
            
        }
        $this->dbh->EndTransaction();
        return array('inserts' => $inserts, 'updated' => $updated);
    }
}
