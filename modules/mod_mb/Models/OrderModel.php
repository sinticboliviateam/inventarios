<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;

use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Order;
use \SB_MBStore;
use \SB_MBOrder;
use \SB_MBProduct;
use \Exception;

class OrderModel extends SB_Model
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var StoresModel
	 */
	protected $storesModel;
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var ProductModel
	 */
	protected $productModel;
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var TransactionTypesModel
	 */
	protected $transactionTypesModel;
    
	/**
	 * 
	 * @param mixed $data 
	 * @return MB_Order
	 */
	public function Create(MB_Order $order, $meta = array())
	{
		//$items	= $data->items;
		//unset($data->items);
		$cdate      = date('Y-m-d H:i:s');
		if( !$order->status )
			$order->status = 'draft';
		if( !$order->payment_status )
			$order->payment_status = 'pending';
        if( !$order->order_date )
            $order->order_date = $cdate;
        
        $store  = new SB_MBStore($order->store_id);    
        $tt     = $this->transactionTypesModel->Get($order->transaction_type_id);
		$order->last_modification_date 	= $cdate;
		$order->creation_date			= $cdate;
		$order->sequence                = $this->GetNextSequence($order->store_id);
		//##Build order code
		
		$order->code	= SB_Module::do_action('mb_build_order_code', sprintf("%s-%d-%s%s", 
                                                                        $tt->transaction_key,
                                                                        $order->sequence,
                                                                        date('Ymd'), 
																		$store->code), 
																		$order, 
																		$store,
                                                                        $tt
		);
        //$this->dbh->BeginTransaction();
        
		$newOrderId = $order->Save(false);
        //var_dump($newOrderId);
        //print ($order);
        //print ($newOrder);
        //die();
		for($i = 0; $i < count($order->items); $i++ )
		{
			$order->items[$i]->order_id                 = $newOrderId;
            $order->items[$i]->last_modification_date   = date('Y-m-d H:i:s');
            $order->items[$i]->creation_date            = date('Y-m-d H:i:s');
		}
		
		//##insert order items
		$this->dbh->InsertBulk('mb_order_items', $order->items);
        $newOrder = MB_Order::Get($newOrderId);
		SB_Module::do_action('mb_pos_after_register_sale', $newOrder);
		//##insert order meta
		if( isset($order->meta) && is_array($order->meta) )
		{
			foreach($order->meta as $_meta)
			{
                $meta = (object)$_meta;
				SB_Meta::updateMeta('mb_order_meta', $meta->meta_key, trim($meta->meta_value), 
                                    'order_id', $newOrder->order_id);
			}
		}
        //$this->dbh->EndTransaction();
		SB_Module::do_action('mb_insert_sale_order', $newOrder);
        
		return $newOrder;
	}
	public function Update($order_id, $data)
	{
		$order_id = $data['order_id'];
		unset($data['creation_date'], $data['order_id']);
		$dbh->Update('mb_orders', $data, array('order_id' => $order_id));
		$dbh->Delete('mb_order_items', array('order_id' => $order_id));
		for($i = 0; $i < count($items); $i++ )
		{
			$items[$i]['order_id'] = $order_id;
		}
		$dbh->InsertBulk('mb_order_items', $items);
		SB_Module::do_action('mb_pos_after_update_sale', $order_id, $data, $items);
	}
	public function UpdateStatus($order_id, $new_status)
	{
		$this->dbh->Update('mb_orders', array('order_id' => $order_id), array('status' => $new_status));
	}
	public function GetNextSequence($store_id)
	{
		$query = "select MAX(sequence) + 1 
				from mb_orders
				where store_id = $store_id";
		$seq = (int)$this->dbh->GetVar($query);
		if( $seq <= 0 )
			$seq = 1;
		return $seq;
	}
    public function Complete(MB_Order $order)
    {
        $this->UpdateStock($order);
        
        $this->ChangeStatus($order, MB_Order::STATUS_COMPLETED);
        SB_Module::do_action('order_completed', $order);
    }
    public function ChangeStatus(MB_Order $order, $status)
	{
		$this->dbh->Update('mb_orders', array('status' => $status), array('order_id' => $order->order_id));
		SB_Module::do_action('mb_order_status_changed', $this, $status);
		SB_Module::do_action('mb_order_status_changed_' . $status, $this);
	} 
    /**
	 * Update stock only
	 */
	public function UpdateStock(MB_Order $order)
	{
		$kardex = array();
		$store = $this->storesModel->Get($order->store_id);
        //##get store warehouses
        $warehouses = $this->storesModel->GetWarehouses($store->store_id);
		//print_r($order->items);die();
		foreach($order->items as $_item)
		{
			$item = (object)$_item;
            /*
			$sql_operation = "OP[product_quantity - {$item->quantity}]";
			$this->dbh->Update('mb_products', 
								array('product_quantity' => $sql_operation), 
								array('product_id' => $item->product_id)
			);
            */
            $total_product_qty = 0;
            foreach($warehouses as $warehouse)
            {
				
                $total_product_qty += $this->productModel->GetQuantity($item->product_id, $warehouse->id);
                
            }
            if( $total_product_qty < $item->quantity )
                throw new Exception($this->__("The product has no enought stock", 'mb'));
            
            $sale_qty = $item->quantity;
            while($sale_qty > 0 )
            {
                foreach($warehouses as $warehouse)
                {
                    $warehouse_qty = $this->productModel->GetQuantity($item->product_id, $warehouse->id);
                    if( $warehouse_qty >= $item->quantity )
                    {
                        $this->productModel->SetQuantity(
                            $item->product_id, 
                            $store->store_id, 
                            $warehouse->id, 
                            $warehouse_qty - $item->quantity,
                            null, 
                            false
                        );
                        $sale_qty = 0;
                        break;
                    }
                    else if( $warehouse_qty > 0 && $warehouse_qty < $item->quantity )
                    {
                        $this->productModel->SetQuantity(
                            $item->product_id, 
                            $store->store_id, 
                            $warehouse->id, 
                            $warehouse_qty,
                            null, 
                            false
                        );
                        $sale_qty -= $warehouse_qty;
                    }
                }
            }
            $product 	= new SB_MBProduct($item->product_id);
            
			
			//##build update product kardex
			$ki 		            = $this->BuildKardexItem($item, $product, 'output', $store->GetMeta('_sale_tt_id'));
            $ki['author_id']        = $order->user_id;
            $ki['transaction_id']   = $order->order_id;
			$kardex[] 	= $ki;
			//##check if product is an assembly
			if( $product->base_type == 'asm' )
			{
				foreach($product->GetAsmComponents() as $com)
				{
					$subitem = (object)array(
						'product_id'	=> $com->product->product_id,
						'quantity'		=> $com->qty_required,
						'price'			=> $com->product->product_price
					);
					//##build update product kardex
					$ki = $this->BuildKardexItem($subitem, $product, 'output', $store->GetMeta('_sale_tt_id'));
					$kardex[] = $ki;
				}
			}
            
		}
        
		$kardex = SB_Module::do_action('mb_before_insert_kardex', $kardex, $order, 'output');
		$this->dbh->InsertBulk('mb_product_kardex', $kardex);
	}
    public function BuildKardexItem($item, $product, $inout, $transaction_type_id)
	{
		$ki = array(
				'product_id'			=> $item->product_id,
				'in_out'				=> $inout,
				'quantity'				=> $item->quantity,
				'quantity_balance'		=> (int)$product->product_quantity,
				'unit_price'			=> $item->price,
				'total_amount'			=> $item->quantity * $item->price,
				'monetary_balance'		=> $product->product_quantity * $product->product_cost,
				'transaction_type_id'	=> (int)$transaction_type_id,
				'author_id'				=> 0,
				'transaction_id'		=> 0,
				'creation_date'			=> date('Y-m-d H:i:s')
		);
		$ki = SB_Module::do_action('mb_build_kardex_item', $ki, $item, $product, $this, $inout);
		
		return $ki;
	}
	public function Revert($order)
	{
		if( $order->status == MB_Order::STATUS_REVERSED )
			throw new Exception(__('The order is already reversed', 'mb'));
		$user	= sb_get_current_user();
		$items 	= $order->GetItems();
		$order->GetStore();
		
		foreach($items as $item)
		{
			$product = $this->productModel->Get($item->product_id);
			if( !$product || !$product->product_id ) continue;
			$newQty = $product->product_quantity + $item->quantity;
			$warehouse_id = $item->warehouse_id ? $item->warehouse_id : null;
			if( !$warehouse_id )
			{
				$warehouses = $this->productModel->GetWarehouses($product);
				$warehouse_id = $warehouses && isset($warehouses[0]) ? $warehouses[0]->id : null;
			}
			if( !$warehouse_id )
				throw new Exception(sprintf(__('The product with identifier %d has not warehouse', 'mb'), $product->product_id));
			//##revert product quantities
			$quantityBalance = $this->productModel->SetQuantity($item->product_id, 
									$order->store_id, 
									$warehouse_id, 
									$newQty, 
									null, 
									false
			);
			$this->dbh->Update('mb_order_items', ['status' => MB_Order::STATUS_REVERSED], ['item_id' => $item->item_id]);
			//##revert product kardex
			//##build update product kardex
			$tt_id = $order->store && $order->store->_revert_tt_id ? $order->store->_revert_tt_id : -2;
			$kardex_i = array(
					'product_id'			=> $item->product_id,
					'in_out'				=> 'input',
					'quantity'				=> $item->quantity,
					'quantity_balance'		=> $quantityBalance,
					'unit_price'			=> $product->product_cost,
					'total_amount'			=> $item->total,
					'monetary_balance'		=> $quantityBalance * $product->product_cost,
					'transaction_type_id'	=> $tt_id,
					'author_id'				=> $user->user_id,
					'transaction_id'		=> $order->order_id,
					'cost'					=> $product->product_cost,
					//'status'				=> MB_Order::STATUS_REVERSED,
					'creation_date'			=> date('Y-m-d H:i:s')
			);
			
			$kardex_i = SB_Module::do_action('mb_purchase_build_revert_kardex_item', $kardex_i);
			$this->dbh->Insert('mb_product_kardex', $kardex_i);
		}
		$this->dbh->Update('mb_orders', 
			['transaction_type_id' => $tt_id, 'status' => MB_Order::STATUS_REVERSED], 
			['order_id' => $order->order_id]);
		return true;
	}
}
