<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;

use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Customers\Models\CustomersModel;
use SinticBolivia\SBFramework\Modules\Customers\Entities\Customer;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Order;

use Exception;

class PosModel extends SB_Model
{
	/**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var OrderModel
	 */
	protected $orderModel;
	/**
     * @namespace SinticBolivia\SBFramework\Modules\Customers\Models
	 * @var CustomersModel
	 */
	protected $customersModel;
   
	/**
	 * Register a new sale
	 * 
	 * Sale data
	 * ==========
	 * store_id		=> The store id for the sale
	 * nit_ruc_nif	=> The customer document or national tax identifier
	 * customer		=> The customer name
	 * customer_id	=> The customer identifier (optional, if the customer id is present it will be created)
	 * products		=> An array of products (see products class for more detail SB_MBProduct)
	 * tax_percent	=> The tax amount for the sale
	 * notes		=> Sale notes (optional)
	 * payment_status	=> The status for the new sale payments (optional, the default status is "complete")
	 * 						[complete|pending]
	 * status			=> The status for the new sale (optional, the default status is "complete")
	 * 						[complete|draft]
	 * @param mixed $data An object or array with all sale data 
	 * @return  
	 */
	public function RegisterSale(MB_Order $sale, &$customer_created = false)
	{
		//$app = \SinticBolivia\SBFramework\Classes\SB_Factory::getApplication();
		//$app->Log($sale);
		if( !is_object($sale)  )
			throw new Exception(__('Invalid sale data, array or object expected', 'mb'));
        if( !$sale->store_id )
            throw new Exception(__('Invalid store identifier', 'mb'));
		$c_query			= '';
		if( !$sale->customer )
            throw new Exception(__('The order has no customer', 'mb'));
        if( !($nit_ruc_nif = $sale->customer->GetMeta('_nit_ruc_nif')) )
            throw new Exception(__('The customer has no NIT/RUC/NIF', 'mb'));
        if( !is_array($sale->items) )
            throw new Exception(__('Invalid order items', 'mb'));
        if( !count($sale->items) )
            throw new Exception(__('The order has no items', 'mb'));
            
        
		if( $sale->customer && $sale->customer->customer_id > 0 )
		{
            $this->customersModel->Update($sale->customer);
		}
		else
		{
            //##try to find the customer by nit_ruc_nif
            $nit_ruc_nif = $this->dbh->EscapeString($nit_ruc_nif);    
            //##try to find the customer into database
            $c_query = "SELECT c.* ". 
                        "FROM mb_customers c, mb_customer_meta cm ".
                        "WHERE c.customer_id = cm.customer_id ".
                        "AND cm.meta_key = '_nit_ruc_nif' " . 
                        "AND cm.meta_value = '$nit_ruc_nif' ".
                        "LIMIT 1";
            $c_row = $this->dbh->FetchRow($c_query);
            //##if customer does not exists, create a new one
            if( !$c_row )
            {
                $sale->customer->store_id   = $sale->store_id;
                $sale->customer->meta[] = (object)array('meta_key' => '_last_sale_name', 'meta_value' => $sale->customer->first_name);
                $sale->customer->meta[] = (object)array('meta_key' => '_billing_name', 
                                                        'meta_value' => $sale->customer->first_name);
                                                        
                $customer = $this->customersModel->Create($sale->customer);
                $sale->customer = $customer;
                $customer_created = true;
            }
            else
            {
                $sale->customer_id              = $c_row->customer_id;
                $sale->customer->customer_id    = $c_row->customer_id;
                $sale->customer->meta[] = (object)array('meta_key' => '_last_sale_name', 
                                                        'meta_value' => $sale->customer->first_name);
                $sale->customer->meta[] = (object)array('meta_key' => '_billing_name', 
                                                        'meta_value' => $sale->customer->first_name);
                $sale->customer = $this->customersModel->Update($sale->customer);
            }
            
		}
        
		$sale->subtotal         = 0;
        $sale->discount	        = 0;
		$sale->total 	        = 0;
		$sale->total_tax        = 0;
		for($i = 0; $i < count($sale->items); $i++)
		{
            $p =& $sale->items[$i];
			$p = SB_Module::do_action('mb_build_order_item', $p);
			$sale->subtotal     += $p->subtotal;
		}
        $tax_percent = 0;
		//print_r($order_items);die();
		$sale->total_tax            = ($tax_percent > 0) ? $sale->subtotal * $tax_percent : 0;
		$sale->total 	            = $sale->total_tax + $sale->subtotal;
		$sale->status               = $sale->status ? $sale->status : MB_Order::STATUS_COMPLETED;
        $sale->transaction_type_id  = (int)mb_get_store_meta($sale->store_id, '_sale_tt_id');
        $sale->user_id              = sb_get_current_user()->user_id;
        $sale->customer_id          = $sale->customer->customer_id;
		$sale->type                 = 'pos';
        $sale->payment_status       = 'paid';
        
		SB_Module::do_action_ref('pos_before_register_sale', $sale);
		//##create order/sale
		$newSale = $this->orderModel->Create($sale);
        if( $sale->status == MB_Order::STATUS_COMPLETED )
        {
            //##mark order as completed, this will create kardex and update product stock
            $this->orderModel->Complete($newSale);
        }
		
        
		SB_Module::do_action('mb_pos_register_sale', $newSale);
		
		return $newSale;
	}
	public function GetReceipt($order, &$printing_type)
    {
        if( !$order )
            throw new Exception($this->__('Invalid sale object'));
        if( !$order->order_id )
            throw new Exception($this->__('Invalid sale identifier'));
        $settings = sb_get_parameter('mb_settings');
        SB_Module::do_action('mb_pos_before_show_receipt', $order);
        
        $logo = isset($settings->business_logo) ? UPLOADS_DIR . SB_DS . $settings->business_logo : null;
        $logo_url = null;
        if( file_exists($logo) )
            $logo_url = UPLOADS_URL . '/' . $settings->business_logo;
        //##check for POS receipt type
        $tpl            = 'tpl-receipt.php'; //default
        $printing_type  = 'browser'; //default
        if( isset($settings->pos) )
        {
            if( $settings->pos->receipt_type == 'legal' )
                $tpl = 'tpl-legal.php';
            elseif( $settings->pos->receipt_type == 'ticket' )
                $tpl = 'tpl-voucher.php';
            
            $printing_type = $settings->pos->printing_type ? $settings->pos->printing_type : $printing_type;
        }
        
        $tpl_receipt = dirname(dirname(__FILE__)) . SB_DS . 'templates' . SB_DS . $tpl;
        $tpl_receipt = SB_Module::do_action('mb_pos_receipt_tpl', $tpl_receipt, $order);
        
        if( !is_file($tpl_receipt) )
            throw new Exception($this->__('The receipt template does not exists'));
        $customerName = sprintf("%s %s", $order->customer->first_name, $order->customer->last_name);    
        extract((array)$settings);
        ob_start();
        include $tpl_receipt;
        $html = ob_get_clean();
        if( $printing_type == 'browser' )
        {
            if( (int)@$settings->pos->silent_print )
            {
                $html .= '<script>this.print({bUI: false, bSilent: true, bShrinkToFit: true});window.onafterprint = function(){window.close();}</script>';
            }
            return $html;
        }
            
        if( $printing_type == 'pdf' )
        {
            $pdf = mb_get_pdf_instance(__('Sale Order Receipt', 'mb'), '', 'dompdf');
            $pdf->loadHtml($html);
            $pdf->render();
            /*
            $pdf->stream(sprintf(__('sale-receipt-%d-%s.pdf', 'mb'), $order->sequence, $customerName), 
                array('Attachment' => 0)
            );
            */
            return $pdf;
        }
    }
}
