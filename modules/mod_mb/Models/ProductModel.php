<?php
//namespace SinticBolivia\MonoBusiness\Models;

namespace SinticBolivia\SBFramework\Modules\Mb\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Product;
use \SB_MBProduct;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Category;

class ProductModel extends SB_Model
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var StoresModel
     */
    protected $storesModel;
	/**
	 * Get a product from id
	 * 
	 * @param int $id 
	 * @return  SB_MBProduct
	 */
	public function Get($id)
	{
		if( !$id )
			throw new Exception(__('Invalid product identifier', 'mb'));
		$product = new SB_MBProduct($id);
		if( !(int)$product->product_id )
			throw new Exception(__('The product does not exists', 'mb'));
			
		return $product;
	}
	/**
	 * Get a record by column
	 */
	public function GetBy(array $conds)
	{
		return MB_Product::Where($conds);
	}
	/**
	 * Create or Update a product
	 * 
	 * @param object 	$productData
	 * @param SB_User 	$user
	 * @return integer	The new product id
	 */
	public function Create($productData, $categories, $meta, $user, $images = null)
	{
		$quantities = null;
		if( isset($productData->quantities) && is_array($productData->quantities) )
		{
			$quantities = $productData->quantities;
			unset($productData->quantities);
		}
		$productData->product_id	= null;
		$productData->user_id		= $user->user_id;
		$productData->slug			= sb_build_slug($productData->product_name);
		$productData->creation_date = date('Y-m-d H:i:s');
		$this->dbh->BeginTransaction();
		//##insert the producto into database
		$id = $this->dbh->Insert('mb_products', $productData);
		$this->SetCategories($id, $categories);
		$this->SetMeta($id, $meta);
		$this->SetImages($id, $images);
		if( $quantities && is_array($quantities) )
		{
			foreach($quantities as $qty)
			{
				$this->SetQuantity($id, (int)$productData->store_id, (int)$qty['warehouse_id'], 
									$qty['quantity'], $qty['min_stock']);
			}
			
		}
		SB_Module::do_action('mb_create_product', $id);
		SB_Module::do_action('mb_save_product', $id, $productData);
		$this->dbh->EndTransaction();
		return $id;
	}
	public function Update($productData, $categories, $meta, $user, $images = null)
	{
		$quantities = null;
		if( isset($productData->quantities) && is_array($productData->quantities) )
		{
			$quantities = $productData->quantities;
			unset($productData->quantities);
		}
		$this->dbh->Update('mb_products', $productData, array('product_id' => $productData->product_id));
		$this->SetCategories($productData->product_id, $categories);
		$this->SetMeta($productData->product_id, $meta);
		$this->SetImages($productData->product_id, $images);
		if( $quantities && is_array($quantities) )
		{
			foreach($quantities as $qty)
			{
				$this->SetQuantity($productData->product_id, 
									$productData->store_id, 
									$qty['warehouse_id'], 
									$qty['quantity'], 
									$qty['min_stock'],
									false
				);
			}
									
		}
		SB_Module::do_action('mb_update_product', $productData->product_id);
		SB_Module::do_action('mb_save_product', $productData->product_id, $productData);
	}
	/**
	 * Delete a product 
	 * 
	 * @param int $productId 
	 * @param bool $login If true the product is just deleted logically
	 * @return bool
	 */
	public function Delete($productId, $logic = false)
	{
		$prod = new SB_MBProduct($productId);
		if( !$prod->product_id )
		{
			throw new Exception(__('The product does not exists', 'mb'));
		}
		if( $logic )
		{
			$this->dbh->Update('mb_products', array('product_id' => $prod->product_id, 'status' => 'deleted'));
			return true;
		}
		$this->dbh->Delete('mb_product_meta', array('product_id' => $productId));
		$this->dbh->Delete('mb_product2category', array('product_id' => $productId));
		$this->dbh->Delete('mb_product2suppliers', array('product_id' => $productId));
		$this->dbh->Delete('mb_product2tag', array('product_id' => $productId));
		$this->dbh->Delete('mb_product_kardex', array('product_id' => $productId));
		$this->dbh->Delete('mb_products', array('product_id' => $productId));
		if( $prod->base_type == 'asm' )
		{
			$this->dbh->Delete('mb_assemblie2product', array('assembly_id' => $prod->product_id));
		}
		//##delete image
		$query = "SELECT * FROM attachments WHERE object_id = $prod->product_id AND object_type = 'product' and parent = 0";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$attach = new SB_AttachmentImage();
			$attach->SetDbData($row);
			$attach->Delete();
		}
		
		return true;
	}
	public function SetCategories($productId, $categories)
	{
		if( !is_array($categories) || !count($categories) )
			return false;
		//##delete product categories
		$this->dbh->Query("DELETE FROM mb_product2category WHERE product_id = $productId");
		foreach($categories as $cat_id)
		{
			$this->dbh->Insert('mb_product2category', array('product_id' => $productId, 'category_id' => $cat_id));
		}
		
		return true;
	}
	public function SetMeta($productId, $meta)
	{
		if( !is_array($meta) || !count($meta) )
			return false;
		//##add product meta
		foreach($meta as $meta_key => $_meta_value)
		{
			$meta_value = $_meta_value;
			/*
			 if( is_array($meta_value) )
			 {
			 $meta_value = array_map(array($dbh, 'EscapeString'), $meta_value);
			 }
			 */
			mb_update_product_meta($productId, $meta_key, $meta_value);
		}
		return true;
	}
	public function SetImages($productId, $images)
	{
		//##set product images
		if( !is_array($images) || !count($images) )
		{
			return false;
		}
		$query = "UPDATE attachments SET object_id = $productId WHERE attachment_id IN(".implode(',', $images).")";
		$this->dbh->Query($query);
		$query = "UPDATE attachments SET object_id = $productId WHERE parent IN(".implode(',', $images).")";
		$this->dbh->Query($query);
		
		return true;
	}
	public function BuildCatalog($storeId, $ids = null, $catId = null, $type = 'pdf')
	{
		
        $app 		= \SinticBolivia\SBFramework\Classes\SB_Factory::getApplication();
        $user 		= sb_get_current_user();
		$products 	= array();
		$title 		= __('Products Catalog', 'mb');
		$group		= true;
		$builder 	= $this->dbh->NewQuery();
		$query		= null;
		if( $ids && is_array($ids) && count($ids) )
		{
			$products = MB_Product::GetIn($ids);
			$group = false;
		}
		else
		{
			$query		= $builder->select('mb_products');
			if( $storeId )
			{
				$query->where()->equals('store_id', $storeId);
			}
			if( $catId )
			{
				$join = $query->join('mb_product2category', 'product_id', 'product_id');
				$join->join('mb_categories', 'category_id', 'category_id');
				$join->where()->equals('category_id', $catId);
				$group = false;	
			}
		}
		
        $headers = array(
            'num'       => array('label' => __('Num.', 'mb'), 'width' => 9),
            //'image'     => array('label' => __('Image', 'mb'), 'width' => 23),
            'code'      => array('label' => __('Code', 'mb'), 'width' => 25),
            'product'   => array('label' => __('Product', 'mb'), 'width' => 80),
            'barcode'   => array('label' => __('Barcode', 'mb'), 'width' => 80),
            'qty'       => array('label' => __('Qty', 'mb'), 'width' => 15),
            'cost'      => array('label' => __('Cost', 'mb'), 'width' => 15),
            'price_1'   => array('label' => __('Price 1', 'mb'), 'width' => 15),
            'price_2'   => array('label' => __('Price 2', 'mb'), 'width' => 15),
        );
        $viewCost = $user->can('mb_can_see_cost');
        if( $type == 'pdf' )
        {
			unset($headers['barcode']);
        	$tableWidth		= 0;
			$cellHeight     = 9;
			$cellPadding    = 2;
			$pdf = mb_get_pdf_instance($title);
			
			$pdf->setFontSubsetting(false);
			$pdf->setPrintHeader(true);
			$pdf->setPrintFooter(true);
			$pdf->setHeaderData('', 0, '', '', array(0,0,0), array(255,255,255));
			$pdf->setFooterData(array(0,64,0), array(0,64,128));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			
			$pdf->SetFillColor(255, 0, 0);
			$pdf->SetTextColor(255);
			//$pdf->SetDrawColor(128, 0, 0);
			$pdf->SetLineWidth(0.1);
			$pdf->SetCellPadding($cellPadding);
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			$pdf->AddPage();
			$pdf->Cell(0, 0, $title, 0, 1, 'C', 0, '', 0);
			
			foreach($headers as $col => $head)
			{
				$x = $pdf->getX();
				$y = $pdf->getY();
				if( !$viewCost && $col == 'cost' ) continue;
				$pdf->Cell($head['width'], $cellHeight, $head['label'], 1, 0, 'C', 1);
				$tableWidth += $head['width'];
			}
			$pdf->Ln();
			//die();
			// set font
			$font       = 'freesans';
			$fontSize   = 7;
			$pdf->SetFont($font, '', $fontSize);
			//$pdf->SetFillColor(255, 255, 255);
			$pdf->SetTextColor(0,0,0);
			$i = 1;
			
			$maxWidth = 52;
			if( $group )
			{
				//##get categories
				$categories = $this->dbh->FetchResults("SELECT * FROM mb_categories WHERE store_id = $storeId");//MB_Category::GetBy('store_id', $storeId, false);
				$ci = 1;
				foreach($categories as $category)
				{
					$query	= $builder->select('mb_products');
					$join 	= $query->join('mb_product2category', 'product_id', 'product_id');
					$join->join('mb_categories', 'category_id', 'category_id');
					$join->where()->equals('category_id', $category->category_id);
					$sql		= $builder->writeWithValues($query);
					$products 	= $this->dbh->FetchResults($sql, MB_Product::class);
					//##write category
					$pdf->Cell($tableWidth, $cellHeight, "$ci. {$category->name}", 1, 0, 'C', 0, '', 0, false);
					$pdf->Ln();
					foreach($products as $product)
					{
						$this->WritePDFProduct($headers, $pdf, $product, $i, $cellHeight, $cellPadding, $viewCost);
						$i++;
					}
					$ci++;
				}
			}
			elseif( $ids )
			{
				foreach($products as $product)
				{
					$this->WritePDFProduct($headers, $pdf, $product, $i, $cellHeight, $cellPadding, $viewCost);
					$i++;
				}
			}
			else
			{
				$category	= $catId ? $this->dbh->FetchRow("SELECT * FROM mb_categories WHERE category_id = $catId LIMIT 1") : null;
				$sql		= $builder->writeWithValues($query);
				$products 	= $this->dbh->FetchResults($sql, MB_Product::class);
				if( $category )
				{
					//##write category
					$pdf->Cell($tableWidth, $cellHeight, "{$category->name}", 1, 0, 'C', 0, '', 0, false);
					$pdf->Ln();
				}
				foreach($products as $product)
				{
					$this->WritePDFProduct($headers, $pdf, $product, $i, $cellHeight, $cellPadding, $viewCost);
					$i++;
				}
			}
			
			//$pdf->Cell(100, 0, '', 'T');
			return $pdf;
		}
        elseif( $type == 'excel' )
        {
        	$sql		= $builder->writeWithValues($query);
        	$products 	= $this->dbh->FetchResults($sql, MB_Product::class);
			return $this->BuildExcelCatalog($headers, $products);
		}
		
	}
	protected function WritePDFProduct($headers, $pdf, $product, $i, $cellHeight, $cellPadding, $viewCost = false)
	{
		$product_name   = preg_replace('/\s+/', ' ', trim($product->product_name));
		$width          = $pdf->GetStringWidth($product_name, $font, '', $fontSize);//strlen($product_name);//imagettfbbox(7, 0, '', $product->product_name);
		$lineWidth      = 0;
		$product_lines  = array($product_name);
		if( $width > ($headers['product']['width'] - ($cellPadding * 2)) )
		{
			$index = 0;
			$product_lines = array();
			for($c = 0; $c < strlen($product_name); $c++)
			{
				$char = $product_name[$c];
				if( $pdf->GetStringWidth($product_lines[$index]) >= ($headers['product']['width'] - ($cellPadding * 2)) )
				{
						$index++;
						$char = trim($char);
				}
				$product_lines[$index] .= $char;
			}
		}
		$pdf->Cell($headers['num']['width'], $cellHeight, $i, 1, 0, 'C', 0, '', 0, false);
				//$pdf->Cell($headers['image']['width'], $cellHeight, '', 1, 0, 'L', 0, '', 0, false);
		$pdf->Cell($headers['code']['width'], $cellHeight, $product->product_code, 1, 0, 'L', 0, '', 0, false);
		$pdf->Cell($headers['product']['width'], $cellHeight, $product_lines[0], 1, 0, 'L', 0, '', 0, false, false,
		count($product_lines) > 1 ? 'T' : '', '');
		//$pdf->Cell($headers['barcode']['width'], $cellHeight, $product->product_barcode, 1, 0, 'L', 0, '', 0, false);
		if( count($product_lines) > 1 )
		{
			$x = $pdf->getX();
			$y = $pdf->getY();
			//$pdf->setX($x - $headers['product']['width']);
			for($li = 1; $li < count($product_lines); $li++)
			{
			$pdf->setXY($x - $headers['product']['width'], $y + (2 * $li));
			$text = $product_lines[$li];
			$pdf->Cell($headers['product']['width'], $cellHeight, $text, 0, 0, 'L', 0, '', 0, false);
			//$pdf->Write($cellHeight, $text);
			//
			//$x = $pdf->getX();
			//$y = $pdf->getY();
			}
			$pdf->setXY($x, $y);
		}
		$pdf->Cell($headers['qty']['width'], $cellHeight, $product->product_quantity, 1, 0, 'C', 0, '', 0, false);
		if( $viewCost )
		{
			$pdf->Cell($headers['cost']['width'], $cellHeight, $product->product_cost, 1, 0, 'R', 0, '', 0, false);
		}
		$pdf->Cell($headers['price_1']['width'], $cellHeight, $product->product_price, 1, 0, 'R', 0, '', 0, false);
		$pdf->Cell($headers['price_2']['width'], $cellHeight, $product->product_price_2, 1, 0, 'R', 0, '', 0, false);
		
		/*
				$reseth = true;
				$stretch = 0;
				$isHtml = false;
						$autopadding = false;
		$maxh = 0;
		
		$pdf->MultiCell($headers['num']['width'], $cellHeight, $i, 1, 'C', 0, 0, '', '', $reseth);
		//$pdf->MultiCell($headers['image']['width'], $cellHeight, '', 1, 'C', 0, 0, '', '', !true);
			$pdf->MultiCell($headers['code']['width'], $cellHeight, $product->product_code, 1, 'L', 0, 0, '', '', $reseth);
			$pdf->MultiCell($headers['product']['width'], $cellHeight, $product->product_name, 1, 'L', 0, 0, '', '', $reseth);
			$pdf->MultiCell($headers['qty']['width'], $cellHeight, $product->product_quantity, 1, 'C', 0, 0, '', '', $reseth);
			if( $user->can('mb_can_see_cost') )
			{
			$pdf->MultiCell($headers['cost']['width'], $cellHeight, $product->product_cost, 1, 'R', 0, 0, '', '', $reseth);
			}
			$pdf->MultiCell($headers['price_1']['width'], $cellHeight, $product->product_price, 1, 'R', 0, 0, '', '', $reseth);
			$pdf->MultiCell($headers['price_2']['width'], $cellHeight, $product->product_price_2, 1, 'R', 0, 0, '', '', $reseth);
		
			*/
		$pdf->Ln();
	}
	public function BuildExcelCatalog($headers, $products)
	{
		$user 		= sb_get_current_user();
		$printCost 	= $user->can('mb_can_see_cost');
		
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
		$xls = new \PHPExcel();
		$sheet 	= $xls->setActiveSheetIndex(0);
		$row 	= 1;
		$cols 	= ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'O'];
		$col	= 0;
		foreach($headers as $key => $head)
		{
			$position = $cols[$col] . $row;
			$sheet->setCellValue($position, $head['label']);
			$col++;
		}
		$row++;
		$i = 1;
		foreach($products as $item)
		{
			$sheet->setCellValue("A$row", $i);
			$sheet->setCellValue("B$row", $item->product_code);
			$sheet->setCellValue("C$row", $item->product_name);
			$sheet->setCellValue("D$row", " " . $item->product_barcode . " ");
			$sheet->setCellValue("E$row", $item->product_quantity);
			$sheet->setCellValue("F$row", $printCost ? sb_number_format($item->product_cost) : 0);
			$sheet->setCellValue("G$row", sb_number_format($item->product_price));
			$sheet->setCellValue("H$row", sb_number_format($item->product_price_2));
			$row++;
			$i++;
		}
		//##set download method
		$xls->Output = function($xls)
		{
			$excelFilename = __('products-catalog.xlsx', 'mb');
			// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$excelFilename.'"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			$objWriter = \PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
			$objWriter->save('php://output');
			exit;
		};
		return $xls;
	}
	public function RemoveImage($imageId)
	{
		$query = "select p.*, c.attachment_id as child_id,c.file as child_file
					from attachments p
					left join attachments c on p.attachment_id = c.parent
					where p.attachment_id = $imageId";
		
		$images = $this->dbh->FetchResults($query);
		
		$ids = array();
		foreach($images as $img)
		{
			$ids[] = $img->attachment_id;
			if( $img->child_id )
				$ids[] = $img->child_id;
			if( file_exists(UPLOADS_DIR . SB_DS . $img->file) && is_file(UPLOADS_DIR . SB_DS . $img->file) )
			{
				@unlink(UPLOADS_DIR . SB_DS . $img->file);
		
			}
			if( file_exists(UPLOADS_DIR . SB_DS . $img->child_file) && is_file(UPLOADS_DIR . SB_DS . $img->child_file) )
			{
				@unlink(UPLOADS_DIR . SB_DS . $img->child_file);
		
			}
		}
		
		if( count($ids) )
			$this->dbh->Query("DELETE FROM attachments WHERE attachment_id IN(".implode(',', $ids).")");
		
		return true;
	}
	public function Export($storeId, $category_id, $fields, $file)
	{
		$export_store_name	= in_array('store_name', $fields);
		$export_categories 	= in_array('categories', $fields);
		$export_cost		= in_array('product_cost', $fields);
		$export_price		= in_array('product_price', $fields);
		$export_quantity	= in_array('product_quantity', $fields);
		
		$cols 	= "p.*";
		$where 	= "AND p.store_id = $storeId ";
		$tables = array("mb_products p");
		
		if( $category_id && (int)$category_id )
		{
			$tables[] 	= 'mb_product2category p2c';
			$where		.= "AND p.product_id = p2c.product_id AND p2c.category_id = $category_id ";
		}
		if( $export_store_name )
		{
			$cols .= ",s.store_name";
			$where .= "AND p.store_id = s.store_id ";
			$tables[] = 'mb_stores s';
		}
		if( in_array('categories', $fields) )
		{
			$cols .= ",(
							SELECT GROUP_CONCAT(c.name)
							FROM mb_categories c, mb_product2category p2c
							WHERE c.category_id = p2c.category_id
							AND p2c.product_id = p.product_id
						) as categories,
						(
							SELECT GROUP_CONCAT(c.category_id)
							FROM mb_categories c, mb_product2category p2c
							WHERE c.category_id = p2c.category_id
							AND p2c.product_id = p.product_id
						) as category_ids";
		}
		$query = "SELECT $cols ".
				"FROM " . implode(',', $tables) . " ".
				"WHERE 1 = 1 ".
				$where .
				"ORDER BY p.product_name ASC";
		
		$csv = fopen($file, 'w');
		if( in_array('product_id', $fields) )
		{
			$data[] = __('Product ID', 'mb');
		}
		if( in_array('product_code', $fields) )
		{
			$data[] = __('Product Code', 'mb');
		}
		if( in_array('product_name', $fields) )
		{
			$data[] = __('Product Name', 'mb');
		}
		if( $export_cost )
		{
			$data[] = __('Product Cost', 'mb');
		}
		if( $export_price )
		{
			$data[] = __('Product Price', 'mb');
		}
		if( $export_quantity )
		{
			$data[] = __('Product Quantity', 'mb');
		}
		if( in_array('store_id', $fields) )
		{
			$data[] = __('Store ID', 'mb');
		}
		if( in_array('store_name', $fields) )
		{
			$data[] = __('Product Name', 'mb');
		}
		if( in_array('categories', $fields) )
		{
			$data[] = __('Categories', 'mb');
		}
		if( in_array('dimensions', $fields) )
		{
			$data[] = __('Height', 'mb');
			$data[] = __('Seat Height', 'mb');
			$data[] = __('Width', 'mb');
			$data[] = __('Depth', 'mb');
			$data[] = __('Weight', 'mb');
		}
		fputcsv($csv, $data);
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$data = array();
			if( in_array('product_id', $fields) )
			{
				$data[] = $row->product_id;
			}
			if( in_array('product_code', $fields) )
			{
				$data[] = $row->product_code;
			}
			if( in_array('product_name', $fields) )
			{
				$data[] = mb_convert_encoding($row->product_name, "ISO-8859-1", mb_detect_encoding($row->product_name));
			}
			if( $export_cost )
			{
				$data[] = sb_number_format($row->product_cost);
			}
			if( $export_price )
			{
				$data[] = sb_number_format($row->product_price);
			}
			if( $export_quantity )
			{
				$data[] = (int)$row->product_quantity;
			}
			if( in_array('store_id', $fields) )
			{
				$data[] = $row->store_id;
			}
			if( in_array('store_name', $fields) )
			{
				$data[] = $row->store_name;
			}
			if( in_array('categories', $fields) )
			{
				$data[] = $row->categories;
			}
			if( in_array('dimensions', $fields) )
			{
				$attr = mb_get_product_meta($row->product_id, '_attributes');
				if( $attr && isset($attr->height) )
				{
					foreach($attr as $prop => $val)
					{
						$data[] = $val;
					}
				}
				elseif( $attr )
				{
					$new_attr = (object)array(
							'height'		=> '',
							'seat_height'	=> '',
							'width'			=> '',
							'depth'			=> '',
							'weight'		=> ''
					);
						
					foreach($attr as $a)
					{
						if( $a->taxonomy == 'pa_height' )
							$new_attr->height = $a->value;
						elseif( $a->taxonomy == 'pa_seat-height' )
						$new_attr->seat_height = $a->value;
						elseif( $a->taxonomy == 'pa_width' )
						$new_attr->width = $a->value;
						elseif( $a->taxonomy == 'pa_depth' )
						$new_attr->depth = $a->value;
						elseif( $a->taxonomy == 'pa_weight' )
						$new_attr->weight = $a->value;
					}
					$data[] = $new_attr->height;
					$data[] = $new_attr->seat_height;
					$data[] = $new_attr->width;
					$data[] = $new_attr->depth;
					$data[] = $new_attr->weight;
				}
					
			}
			fputcsv($csv, $data);
			
		}
		fclose($csv);
		return true;
	}
	/**
	 * Get Product warehouses including quantities
	 * 
	 * @param int $id  The product identifier
	 * @return array An array of warehouses incluing its quantity
	 */
	public function GetWarehouses($id)
	{
		//try
		//{
			$product 	= is_object($id) && $id->product_id ? $id : $this->Get((int)$id);
			$query 		= "select w.*, q.product_id, q.quantity, q.min_stock ".
							"FROM mb_warehouse w, mb_product_quantity q " .
							"WHERE 1 = 1 AND w.id = q.warehouse_id AND q.product_id = $product->product_id";
			
			$items		= $this->dbh->FetchResults($query);
		//
		
		return $items;
		
	}
    /**
     * 
     * @param type $id
     * @param type $warehouse_id
     * @return type
     */
	public function GetQuantity($id, $warehouse_id)
	{
		$query = "SELECT quantity FROM mb_product_quantity WHERE product_id = $id AND warehouse_id = $warehouse_id LIMIT 1";
		return (int)$this->dbh->GetVar($query);
	}
	/**
	 * 
	 * @param int $id  Product identifier
	 * @param int $store_id 
	 * @param int $warehouse_id 
	 * @param int $qty 
	 * @param int $min_stock 
	 * @return bool 
	 */
	public function SetQuantity($id, $store_id, $warehouse_id, $qty, $min_stock = 0, $increase = true)
	{
        $newQty = 0;
		$query = "SELECT * FROM mb_product_quantity WHERE product_id = $id AND warehouse_id = $warehouse_id LIMIT 1";
		$old = $this->dbh->FetchRow($query);
		if( !$old )
		{
            $newQty = $qty;
			$this->dbh->Insert('mb_product_quantity', 
								array('product_id' 	=> $id, 
									'store_id' 		=> $store_id, 
									'warehouse_id' 	=> $warehouse_id, 
									'quantity'		=> $qty,
									'min_stock'		=> $min_stock,
									'batch_id'		=> 0
								)
			);
			//##check if quantity is grater than 0, so we can create product initial kardex
			if( $qty > 0 )
			{
				/*
				//##create initial kardex
				$this->dbh->Insert('mb_product_kardex',
						array(
								'product_id' 			=> $id,
								'in_out'				=> 'input',
								'quantity'				=> $qty,
								'quantity_balance'		=> $qty,
								'unit_price'			=> $product->product_cost,
								'total_amount'			=> $product->product_quantity * $product->product_cost,
								'monetary_balance'		=> $product->product_quantity * $product->product_cost,
								'transaction_type_id'	=> -1,
								'author_id'				=> $user->user_id,
								'transaction_id'		=> -1,
								'creation_date'			=> date('Y-m-d H:i:s')
						)
				);
				*/
			}
		}
		else
		{
			$data = array('quantity' => (int)$qty);
			if( $increase )
				$data['quantity'] += (int)$old->quantity;
			
			if( $min_stock != null && (int)$old->min_stock != (int)$min_stock )
				$data['min_stock'] = (int)$min_stock;
			$this->dbh->Update('mb_product_quantity', $data, array('id' => $old->id));
            $newQty = $data['quantity'];
		}
		//##calculate global quantity
		$query = "UPDATE mb_products SET product_quantity = (select sum(quantity) from mb_product_quantity where product_id = $id) where product_id = $id";
		$this->dbh->Query($query);
		return $newQty;
	}
	public function BuildLabels($products, $cellsNum = 5, $printBarcode = true, $printPrice = false)
	{
		
        $title = __('Products Labels', 'mb');
		$pdf = mb_get_pdf_instance($title);
        $pdf->SetAutoPageBreak(!TRUE, 10);
		$pdf->AddPage();
        $pdf->Cell(0, 0, $title, 0, 1, 'C', 0, '', 0);
		$headers = array(
            60,60,60,60,60
        );
        //$cellsNum   = 5;
        $cellWidth  = 200/$cellsNum;
        $cellHeight = $printBarcode ? 33 : 17;
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => !true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $i = 0;
        $x = $pdf->getX();
        $y = $pdf->getY();
        $pageHeight = $pdf->getPageHeight() - PDF_MARGIN_TOP - PDF_MARGIN_BOTTOM - PDF_MARGIN_FOOTER - PDF_MARGIN_HEADER;
        foreach($products as $product)
        {
            $barcode = trim($product->product_barcode);
            //var_dump($pdf->getPageHeight());die();
            //$pdf->Cell($cellWidth, $cellHeight, $product->product_barcode, 1, 0, 'C', 0);
            //$pdf->Cell(0, 0, 'CODE 39 + CHECKSUM', 0, 1);
            for($q = 0; $q < $product->product_quantity; $q++)
            {
                try
                {
					$x0 = $y0 = null;
					if( $printPrice )
					{
						//##print product price
						$price = sb_number_format($product->product_price);
						$pdf->Cell($cellWidth, $cellHeight, $price, 1, 0, 'C', false, '', 0, false, '', 'B');
						$x0 = $pdf->getX();
						$y0 = $pdf->getY();
						$pdf->setXY($x0 - $cellWidth, $y0 - 4);
					}
					//##print product code
                    $pdf->Cell($cellWidth, $cellHeight, $product->product_code, !$x0 ? 1 : 0, 0, 'C', false, '', 0, false, '', 'B');
                    if( $x0 == null && $y0 == null )
                    {
						$x0 = $pdf->getX();
						$y0 = $pdf->getY();
					}
					$pdf->setXY($x0 - $cellWidth, $y0 - 7);
                    //##print product name
                    $product_name = substr($product->product_name, 0, 15);
                    $pdf->Cell($cellWidth, $cellHeight, $product_name, 0, 0, 'C', false, '', 0, false, '', 'B');
                    $pdf->setXY($x0, $y0 - $cellHeight);
                    if( $printBarcode && $barcode )
                    {
						//##print barcode
						$pdf->write1DBarcode($barcode, 'EAN13', $x0 - $cellWidth, $y0, $cellWidth, $cellHeight - 10, 0.4, $style, 'T');
						$x = $pdf->getX();
						$y = $pdf->getY();
						//$pdf->setXY($x0, $y0 - $cellHeight);
						
					}
                    $pdf->setXY($x0, $y0);
                    //var_dump($pdf->getY() );
                    $i++;
                    
					if( $i == $cellsNum )
					{
						if (($pdf->getY() /*+ $cellHeight*/) >= $pageHeight ) 
						{
							$pdf->AddPage();
						}
						else
						{
							$pdf->Ln();
							$pdf->setXY(4, $y0 + $cellHeight);
						}
						$i = 0;
					}
                    
                }
                catch(\Exception $e)
                {
                    SB_Factory::getApplication()->log($e->getMessage());
                    SB_Factory::getApplication()->log("Barcode: $barcode");   
                }
            }
            
        }
        $pdf->Ln();
		//$pdf->writeHTML($html, true, false, true, false, '');
		//$pdf->Output(sprintf(__('products-labels-%s.pdf'), sb_format_date(date('Y-m-d'))), 'I');
		return $pdf;
	}
}
