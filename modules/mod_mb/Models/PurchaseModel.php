<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_TableList;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use \SB_MBPurchase;
use \SB_MBProduct;
use SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_PurchaseOrder;
use Exception;


class PurchaseModel extends SB_Model
{
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var ProductModel
	 */
	protected $productModel;
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var StoresModel
	 */
    protected $storesModel;
    /**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var TransactionTypesModel
	 */
    protected $transactionTypesModel;
    
	public function GetListingTable()
	{
		/*
		 $page	= SB_Request::getInt('page', 1);
		 $limit 	= SB_Request::getInt('limit', 25);
		 if( $page <= 0 )
		 	$page = 1;
		 	$total_orders = $this->dbh->GetVar("SELECT COUNT(*) FROM mb_purchase_orders");
		 	*/
		 $table = new SB_TableList('mb_purchase_orders o', 'order_id', 'mb');
		 $table->SetColumns(array(
	 		'order_id'		=> array('show' => false),
	 		'code'	=> array(
	 				'label'	=> __('Code', 'mb'),
	 		),
            'order_date'	=> array(
	 				'label'	=> __('Order Date', 'mb'),
	 		),
	 		'store_name'	=> array(
	 				'label' 	=> __('Store Name', 'mb'),
	 				'subquery' 	=> 'SELECT store_name FROM mb_stores s WHERE s.store_id = o.store_id'
	 		),
	 		'supplier'	=> array(
	 				'label'		=> __('Supplier Name', 'mb'),
	 				'subquery'	=> 'SELECT supplier_name FROM mb_suppliers as s WHERE s.supplier_id = o.supplier_id'
	 		),
	 		'total'		=> array(
								'label' => __('Amount', 'mb'), 
								'callback' => function($order){return sb_number_format($order->total, 2);}
			),
	 		'status'		=> array(
	 				'label' => __('Status', 'mb'),
	 				'callback' => function($p)
	 				{
	 					$class = 'danger';
	 					$label = __('Unknow', 'mb');
                        
                        switch(strtoupper($p->status))
                        {
                            case MB_PurchaseOrder::STATUS_VOID:
                                $class = 'danger';
                                $label = __('Void', 'quotes');
                            break;
                            case MB_PurchaseOrder::STATUS_CREATED:
                                $class = 'success';
                                $label = __('Created', 'Created');
                            break;
                            case MB_PurchaseOrder::STATUS_COMPLETED:
                                $class = 'success';
                                $label = __('Completed', 'quotes');
                            break;
                            case MB_PurchaseOrder::STATUS_WAITING_STOCK:
                                $class = 'warning';
                                $label = __('Waiting Stock', 'mb');
                            break;
                            default:
                            break;
                        }
	 					?>
						<label class="label label-<?php print $class; ?>"><?php print $label; ?></label>
						<?php
					}
				),
				'creation_date'	=> array(
					'label' => __('Creation Date', 'mb'), 
					'callback' => function($item)
					{
						print sb_format_datetime($item->creation_date);
					}
				)
		));
			
		$table->Fill();
		$table->SetRowActions(array(
			'purchases.receive'		=> array(
                'label'     => __('Receive', 'mb'), 
                'icon'      => 'glyphicon glyphicon-cog',
                'callback'  => function($action, $data, $item, $obj)
                {
                    if( strtoupper($item->status) == MB_PurchaseOrder::STATUS_COMPLETED )
                        return false;
                    return true;
                }
            ),
			'purchases.edit'		=> array('label' => __('Edit', 'mb'), 'icon' => 'glyphicon glyphicon-edit'),
			'task:purchases.delete'	=> array('label' => __('Delete', 'mb'), 'icon' => 'glyphicon glyphicon-trash')
		));
		
		return $table;
	}
	public function Create($data, $items, $meta)
	{
		$data['order_id'] = null;
		return $this->Save($data, $items, $meta);
	}
	public function Update($order_id, $data, $items, $meta)
	{
		$data['order_id'] = $order_id;
		return $this->Save($data, $items);
	}
	/**
	 * Inserts or update an order
	 * 
	 * @param mixed $data 
	 * @param mixed $items 
	 * @param mixed $meta 
	 * @return  SB_MBOrder
	 */
	public function Save(MB_PurchaseOrder $order, $items, $meta)
	{
		if( !is_object($order) )
			throw new Exception(__('Invalid purchase order object', 'mb'));
        if( !$order->store_id )
            throw new Exception(__('The purchase order has no store', 'mb'));
        if( !$order->transaction_type_id )
            throw new Exception(__('The purchase order has no transaction type for the purchase order', 'mb'));
           
        if( !$order->order_id )
			$order->order_id = null;
			
        //$store  = $this->storesModel->Get($order->store_id);
        //$tt     = $this->transactionTypesModel->Get($order->transaction_type_id);
        
		//##calculate totals
		$overall_total		= 0;
		$overall_subtotal	= 0;
		$overall_discount 	= 0;
		$total_tax 			= 0;
		$total_items 		= 0;
		$order_items 		= array();
		foreach($items as $item)
		{
			$discount			= isset($item['discount']) ? (float)$item['discount'] : 0;
			$subtotal			= (int)$item['quantity'] * (float)$item['supply_price'];
			$total		 		= $subtotal - $discount;
			$overall_subtotal	+= $subtotal;
			$overall_discount 	+= $discount;
			$overall_total		+= $total;
			$total_items 		+= (int)$item['quantity'];
			$order_item 			= array(
					'product_id'				=> (int)$item['product_id'] ? $item['product_id'] : 0,
					'name'						=> $item['product_name'],
					'quantity'					=> $item['quantity'],
					'supply_price'				=> (float)$item['supply_price'],
					'subtotal'					=> $subtotal,
					'discount'					=> $discount,
					'total'						=> $total,
					'status'					=> MB_PurchaseOrder::STATUS_WAITING_STOCK,
					'last_modification_date'	=> date('Y-m-d H:i:s'),
					'creation_date'				=> date('Y-m-d H:i:s')
			);
			SB_Module::do_action_ref('mb_purchase_build_order_item', $order_item, $item);
			$order_items[] 	= $order_item;
		}
		//##check if order date has included the time (only for new orders)
		if( (!isset($order->order_id) || !$order->order_id) && !strstr($order->order_date, ':') )
			$order->order_date = $order->order_date . ' ' . date('H:i:s');
			
		$order->subtotal	= $overall_subtotal;
        $order->total_tax   = 0;
        $order->total	    = $overall_total;
        if( $order->tax_rate > 0 )
        {
            $order->total_tax    = $order->subtotal * $order->tax_rate;
            $order->total        = $order->subtotal + $order->total_tax;
        }
		$order->discount	= $overall_discount;
		$order->items		= $total_items;
		$updated 			= false;
		$order->status		= MB_PurchaseOrder::STATUS_WAITING_STOCK;
		if( !isset($order->order_id) || !$order->order_id )
		{
			//$order->sequence 		= $this->GetNewSequence($order->store_id, date('Y'));
			//$order->code            = sprintf("%s-%d-%s%s", $tt->transaction_key, $data->sequence, date('Ymd'), $store->code);
			
			$order->user_id	= sb_get_current_user()->user_id;
            $order			= $order->Save();
           
		}
		else
		{
			//unset($data->creation_date);
			//$data->last_modification_date = date('Y-m-d');
			//$this->dbh->Update('mb_purchase_orders', $data, array('order_id' => $data->order_id));
			//##get oldOrder
			$oldOrder = MB_PurchaseOrder::Get($order->order_id);
			$oldOrder->Bind($order, ['code', 'creation_date']);
			$order 	= $oldOrder->Update();
			$this->dbh->Delete('mb_purchase_order_items', array('order_id' => $order->order_id));
		}
		
		$order_items 	= SB_Module::do_action('mb_before_insert_purchase_order_items', $order_items);
		
		//##insert the order items
		for($i = 0; $i < count($order_items); $i++)
		{
			$order_items[$i]['order_id'] = $order->order_id;
		}
		$this->dbh->InsertBulk('mb_purchase_order_items', $order_items);
		
		SB_Module::do_action('mb_insert_purchase_order', $order, $order_items);
		
		foreach($meta as $meta_key => $meta_value)
		{
			SB_Meta::updateMeta('mb_purchase_order_meta', $meta_key, trim($meta_value), 'order_id', $order->order_id);
		}
		
		
		return $order;
	}
	/**
	 * Get a new purchase order sequence
	 * 
	 * @param integer $store_id
	 * @param integer  $year
	 * @return integer 
	 */
	public function GetNewSequence($store_id, $year)
	{
		return MB_PurchaseOrder::GetNewSequence($store_id, $year);
	}
	public function Receive($purchase, $req_items)
	{
        if( strtoupper($purchase->status) != MB_PurchaseOrder::STATUS_WAITING_STOCK )
            throw new Exception(__('The purchase order can\t be received, check the status'));
            
		$user = sb_get_current_user();
		$mb_purchase_order_items = SB_DbTable::GetTable('mb_purchase_order_items', 1);
		
		//$items = $purchase->GetItems();
		$all_completed = true;
		foreach($req_items as $_r_item)
		{
			$r_item = (object)$_r_item;
			if( !isset($r_item->item_id) || !(int)$r_item->item_id )
				continue;
			$db_item = $mb_purchase_order_items->GetRow((int)$r_item->item_id);
			
			if( !$db_item )
				continue;
			if( $db_item->status == 'complete' )
				continue;
			//##check if we are received atleast one unit
			if( (int)$r_item->quantity_received <= 0 )
			{
				$all_completed = false;
				continue;
			}
			//##check if item is already completed
			if( (int)$db_item->quantity > 0 && (int)$db_item->quantity == (int)$db_item->quantity_received )
			{
				continue;
			}
			$item_subtotal = (float)$r_item->supply_price * (int)$r_item->quantity_received;
			//##register the delivery
			$delivery = array(
				'order_id'				=> $purchase->order_id,
				'item_id'				=> $db_item->item_id,
				'user_id'				=> $user->user_id,
				'quantity_ordered'		=> $db_item->quantity,
				'quantity_delivered'	=> (int)$r_item->quantity_received,
				'supply_price'			=> (float)$r_item->supply_price,
				'sub_total'				=> $item_subtotal,
				'total_tax'				=> 0,
				'discount'				=> (float)$r_item->discount,
				'total'					=> $item_subtotal - (float)$r_item->discount,
				'creation_date'			=> date('Y-m-d H:i:s')
			);
			$this->dbh->Insert('mb_purchase_order_deliveries', $delivery);
			//##update product quantity
			$newQty = $this->productModel->SetQuantity($db_item->product_id, 
												$purchase->store_id, 
												$purchase->warehouse_id, 
												$r_item->quantity_received);
			//##update product cost
			$this->dbh->Update('mb_products', 
								array('product_cost' 	=> (float)$r_item->supply_price), 
								array('product_id' 		=> $r_item->product_id)
			);
			
			SB_Module::do_action('mb_purchase_update_stock_item', $db_item, $purchase);
			$quantity_balance 	= $newQty;
			//##build update product kardex
			$kardex_i = array(
					'product_id'			=> $db_item->product_id,
					'in_out'				=> 'input',
					'quantity'				=> $r_item->quantity_received,
					'quantity_balance'		=> $quantity_balance,
					'unit_price'			=> $r_item->supply_price,
					'total_amount'			=> $item_subtotal,
					'monetary_balance'		=> $quantity_balance * $r_item->supply_price,
					'transaction_type_id'	=> $purchase->transaction_type_id,
					'author_id'				=> $user->user_id,
					'transaction_id'		=> $purchase->order_id,
					'cost'					=> $r_item->supply_price,
					'creation_date'			=> date('Y-m-d H:i:s')
			);
			
			$kardex_i = SB_Module::do_action('mb_purchase_build_kardex_item', $kardex_i);
			$this->dbh->Insert('mb_product_kardex', $kardex_i);	
			//##update purchase order item
			$db_item->quantity_received += (int)$r_item->quantity_received;
			$db_item->supply_price		= $r_item->supply_price;
			$db_item->discount			+= $r_item->discount;
			$db_item->subtotal			= $r_item->supply_price * $db_item->quantity;
			$db_item->total				= $db_item->subtotal - (float)$r_item->discount;
			//##check if total quantity is received
			if( $db_item->quantity_received >= $db_item->quantity )
			{
				//##mark items as complete
				//$db_item->quantity_received = $db_item->quantity;
				$db_item->status 			= 'complete';
			}
			else
			{
				$all_completed = false;
			}
			//print_r($db_item);die();
			$db_item->Save();
		}
		//##check if all items were received completed
		if( $all_completed )
		{
			$this->dbh->Update('mb_purchase_orders', 
								array('status' => 'complete'), 
								array('order_id' => $purchase->order_id));
		}
		
		return new SB_MBPurchase($purchase->order_id);
	}
	public function GetOrderItem()
	{
		
	}
	public function GetOrderProducts($order)
	{
		
		$subquery = "SELECT oi.product_id ".
					"FROM mb_purchase_order_items oi ".
					"WHERE oi.order_id = {$order->order_id} ".
					"ORDER BY item_id";
		$query = "SELECT p.* FROM mb_products p WHERE p.product_id IN($subquery)";
		
		return $this->dbh->FetchResults($query);
	}
	public function Revert($purchase)
	{
		$user	= sb_get_current_user();
		$items 	= $purchase->GetItems();
		/*
		foreach($items as $item)
		{
			$product = $this->productModel->Get($item->product_id);
			if( !$product || !$product->product_id ) continue;
			$newQty = $product->product_quantity + $item->quantity_received;
			//##revert product quantities
			$quantityBalance = $this->productModel->SetQuantity($item->product_id, $purchase->store_id, $purchase->warehouse_id, $newQty, null, false);
			//##revert product kardex
			//##build update product kardex
			$kardex_i = array(
					'product_id'			=> $item->product_id,
					'in_out'				=> 'input',
					'quantity'				=> $item->quantity_received,
					'quantity_balance'		=> $quantityBalance,
					'unit_price'			=> $item->supply_price,
					'total_amount'			=> $item->total,
					'monetary_balance'		=> $quantityBalance * $item->supply_price,
					'transaction_type_id'	=> $purchase->store && $purchase->store->_revert_tt_id ? $purchase->store->_revert_tt_id : -2,
					'author_id'				=> $user->user_id,
					'transaction_id'		=> $purchase->order_id,
					'cost'					=> $item->supply_price,
					'creation_date'			=> date('Y-m-d H:i:s')
			);
			$kardex_i = SB_Module::do_action('mb_purchase_build_revert_kardex_item', $kardex_i);
			$this->dbh->Insert('mb_product_kardex', $kardex_i);
		}
		*/
		return true;
	}
}
