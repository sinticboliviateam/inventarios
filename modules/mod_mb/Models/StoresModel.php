<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models
{
	use SinticBolivia\SBFramework\Classes\SB_Model;
    use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Product;
    use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store;
    
	class StoresModel extends SB_Model
	{
        public function GetAll()
        {
            $query = "SELECT * FROM mb_stores ORDER BY store_name ASC";
            
            return $this->dbh->FetchResults($query);
        }
        public function Get($id)
        {
            return MB_Store::Get($id);
        }
		/**
		 * 
		 * @param array $data
		 * @return int The store id
		 */
		public function Save($data, $meta = array())
		{
			$id = null;
			if( !isset($data['store_id']) || !(int)$data['store_id'] )
			{
				$data['creation_date'] 	= date('Y-m-d');
				$id 					= $this->dbh->Insert('mb_stores', $data);
				
			}
			else
			{
				$id = $data['store_id'];
				$this->dbh->Update('mb_stores', $data, array('store_id' => $data['store_id']));
			}
			if( is_array($meta) )
			{
				foreach($meta as $meta_key => $meta_value)
				{
					mb_update_store_meta($id, $meta_key, trim($meta_value));
				}
			}
			return $id;
		}
		public function GetWarehouses($store_id)
		{
			$query = "SELECT w.* FROM mb_warehouse w WHERE 1 = 1 ";
			$query .= "AND w.store_id = $store_id ";
			$query .= "ORDER BY w.name ASC";
			$items = $this->dbh->FetchResults($query);
			return $items;
		}
        /**
         * Get store products
         * 
         * @param int $id
         */
        public function GetProducts($id)
        {
            
        }
        /**
         * Deletes a store
         *  
         * @param MB_Store $store
         * @param bool $fisically
         * @return boolean
         * @throws Exception
         */
        public function Delete(MB_Store $store, $fisically = true)
        {
            if( !$store->store_id )
                throw new Exception(__('Invalid store identifier', 'mb'));
            $store->Delete($fisically);
            //##delete meta
            $this->dbh->Delete($store->GetTableName(), array('store_id' => $store->store_id));
            
            return true;
        }
        public function GetBestSellers(MB_Store $store)
        {
        	if( !$store )
        		throw new Exception(__('Invalid store object', 'mb'));
			//$tt_id = mb_get_store_meta($store->store_id, '_sale_tt_id');
			//$tt_id = $store->GetMeta('_sale_tt_id');
			//if( !$tt_id )
			//	return [];
			$builder 	= $this->dbh->NewQuery();
			$query		= $builder->select('mb_products');
			$join1 		= $query->join('mb_order_items', 'product_id', 'product_id', ['quantity']);
			$query->setFunctionAsColumn('SUM', ['mb_order_items.quantity'], 'total_saled');
			$join1->join('mb_orders', 'order_id', 'order_id')
							->where()
								->equals('store_id', $store->store_id);
			$query->groupBy(['product_id']);
			$query->orderBy('total_saled', 'ASC', '');
			$query->limit(0, $limit);
			
			$sql = $builder->writeWithValues($query);
			//die($sql);
			return $this->dbh->FetchResults($sql, MB_Product::class);
			/*
			$query = "select p.product_id,p.product_code,p.product_name, sum(oi.quantity) as total_saled
						from mb_products p, mb_orders o, mb_order_items oi
						where 1 = 1
						and o.order_id = oi.order_id
						and oi.product_id = p.product_id
						and o.store_id = {$store->store_id}
						group by p.product_id
						order by total_saled desc
						limit $limit";
			*/
		}
		/**
		 * Get the product max price from store
		 * 
		 * @param MB_Store	$store
		 * @param integer	$priceNum
		 * @return float
		 */
		public function GetMaxPrice($store, $priceNum = 1)
		{
			$column		= 'product_price';
			if( $priceNum > 1 )
				$column .= '_' . $priceNum;
			
			$builder 	= $this->dbh->NewQuery();
			$query		= $builder->select('mb_products', []);
			$query->setFunctionAsColumn('MAX', [$column], 'max_price');
			$sql = $builder->writeWithValues($query);
			
			return (float)$this->dbh->GetVar($sql);
		}
		/**
		 * Get the product min price from store
		 *
		 * @param MB_Store	$store
		 * @param integer	$priceNum
		 * @return float
		 */
		public function GetMinPrice($store, $priceNum = 1)
		{
			$column		= 'product_price';
			if( $priceNum > 1 )
				$column .= '_' . $priceNum;
				
			$builder 	= $this->dbh->NewQuery();
			$query		= $builder->select('mb_products', []);
			$query->setFunctionAsColumn('MIN', [$column], 'max_price');
			$sql = $builder->writeWithValues($query);
				
			return (float)$this->dbh->GetVar($sql);
		}
	}
}
