<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;

class TaxModel extends SB_Model
{
    public function Get($id)
    {
        if( !(int)$id )
            throw new Exception($this->__('Invalid tax identifier'));
            
        $query = "SELECT * FROM mb_tax_rates WHERE tax_id = $id LIMIT 1";
		$tax = $this->dbh->FetchRow($query);
        
        return $tax;
    }
    public function GetAll()
    {
        $query = "SELECT * FROM mb_tax_rates ORDER BY name ASC";
		$taxes = $this->dbh->FetchResults($query);
        
        return $taxes;
    }
}
