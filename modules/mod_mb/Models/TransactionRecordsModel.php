<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;

use SinticBolivia\SBFramework\Classes\SB_Model;

class TransactionRecordsModel extends SB_Model
{
    public function GetAll($store_id = null, $tt_id = null, $user_id = null, $fromDate = null, $toDate = null, $page = 1, $limit = 100)
    {
        $where = '';
        $where_orders = '';
        //print_r($user);var_dump($user->can('mb_see_all_orders'));
        if( $store_id && $store_id > 0 )
        {
            $where .= "AND po.store_id = {$store_id} ";
            $where_orders .= "AND o.store_id = {$store_id} ";
        }
        if( $user_id && $user_id > 0 )
        {
            $where .= "AND po.user_id = {$user_id} ";
            $where_orders .= "AND o.user_id = {$user_id} ";
        }
        if( $tt_id && $tt_id > 0 )
        {
            $where .= "AND po.transaction_type_id = $tt_id ";
            $where_orders .= "AND o.transaction_type_id = $tt_id ";
        }
        
        if( $fromDate )
        {
            $where          .= "AND DATE(po.order_date) >= '$fromDate' ";
            $where_orders   .= "AND DATE(o.order_date) >= '$fromDate' ";
        }
        if( $toDate )
        {
            $where          .= "AND DATE(po.order_date) <= '$toDate' ";
            $where_orders   .= "AND DATE(o.order_date) <= '$toDate' ";
        }
        $query = "select {columns} FROM (
                        (
                            SELECT 'purchase_order' as 'entity_type', po.order_id,po.code,po.store_id,po.transaction_type_id,po.sequence,po.details,
                                        po.subtotal,po.total_tax,po.discount,po.total,po.`status`,po.user_id,po.supplier_id as customer_id,po.order_date,po.creation_date,
                                        s.supplier_name as first_name, '' as last_name,
                                        tt.transaction_name,
                                        u.username,
                                        store.store_name
                            FROM (mb_purchase_orders po, mb_transaction_types tt, users u, mb_stores store)
                            LEFT JOIN mb_suppliers s ON s.supplier_id = po.supplier_id 
                            WHERE 1 = 1
                            AND po.transaction_type_id = tt.transaction_type_id
                            AND po.user_id = u.user_id
                            AND po.store_id = store.store_id
                            $where
                        )
                        UNION
                        (
                            SELECT 'order' as 'entity_type', o.order_id, o.code, o.store_id, o.transaction_type_id, o.sequence,o.details,
                                        o.subtotal, o.total_tax,o.discount,o.total,o.`status`,o.user_id,o.customer_id,o.order_date,o.creation_date,
                                        co.first_name, co.last_name,
                                        tt.transaction_name,
                                        u.username,
                                        store.store_name
                            FROM (mb_orders o, mb_transaction_types tt, users u, mb_stores store)
                            LEFT JOIN mb_customers co ON co.customer_id = o.customer_id
                            WHERE 1 = 1
                            AND o.transaction_type_id = tt.transaction_type_id
                            AND o.user_id = u.user_id
                            AND o.store_id = store.store_id
                            $where_orders
                        )
                    ) transactions
                    order by order_date desc
                    {limit}";
        $totals = $this->dbh->FetchRow(str_replace(array('{columns}', '{limit}'), 
                                                    array('count(*) rows,sum(total) total', ''), 
                                                    $query)
        );
        $totalRows		= (int)$totals->rows;
        $totalAmount 	= (float)$totals->total;
        $offset			= 0;
        $totalPages		= 1;
		if( (int)$limit > 0 )
		{
			$totalPages = (int)ceil($totalRows / $limit);
			$offset		= ($page <= 1) ? 0 : ($page - 1) * $limit;
			$limit		= "$offset,$limit";
		}
		
        $q = str_replace(array('{columns}', '{limit}'), array('*', "LIMIT $limit"), $query);
        //die($q);        
        $rows = $this->dbh->FetchResults($q);
        return array('rows' => $rows, 'total_rows' => $totals->rows, 'total_pages' => $totalPages, 'total_amount' => sb_number_format($totals->total));
    }
}
