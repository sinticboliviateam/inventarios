<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;

class TransactionTypesModel extends SB_Model
{
    public function GetAll()
    {
        $query = "SELECT * FROM mb_transaction_types ORDER BY transaction_name ASC";
        
        return $this->dbh->FetchResults($query);
    }
    public function Get($id)
    {
        $query = "SELECT * FROM mb_transaction_types WHERE transaction_type_id = $id LIMIT 1";
        
        return $this->dbh->FetchRow($query);
    }
}
