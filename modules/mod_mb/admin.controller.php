<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Session;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel;
use SinticBolivia\SBFramework\Modules\Mb\Models\StoresModel;
use SinticBolivia\SBFramework\Classes\SB_BarcodeEAN13;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Product;

class LT_AdminControllerMb extends SB_Controller
{
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var ProductModel
	 */
	protected	$productModel;
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var StoresModel
	 */
	protected	$storesModel;
    /**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var SinticBolivia\SBFramework\Modules\Mb\Models\ImportModel
	 */
	protected	$importModel;
    
	public function task_default()
	{
		$user = sb_get_current_user();
		/*
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_products ORDER BY creation_date DESC";
		$dbh->Query($query);
		$products = $dbh->FetchResults();
		$products = SB_Warehouse::getStoreProducts($store_id = null);
		sb_set_view_var('products', $products);
		*/
		if( $user->role_id !== 0 && !$user->_store_id && $user->role_key != 'superadmin' )
		{
			lt_die(__('You dont have any store assigned', 'mb'));
		}
		$store_id	= $this->request->getInt('store_id');
		$cat_id		= $this->request->getInt('category_id');
		$type_id	= $this->request->getInt('type_id');
		$filter 	= $this->request->getString('filter');
		$base_type	= $this->request->getString('btype', null);
		$search_by	= $this->request->getString('search_by', 'product_code');
		$def_search_text = $search_by == 'product_name' ? __('Name', 'mb') : __('Code', 'mb');
		$where 		= array();
		if( !$user->can('mb_see_all_stores') )
		{
			$store_id = (int)$user->_store_id;
		}
		if( $filter == null )
		{
			$columns = array('*');
			$table = array('mb_products p');
			if( $store_id > 0 )
			{
				$where[] = "p.store_id = $store_id";
			}
			if( $cat_id > 0 )
			{
				$table[] = 'mb_product2category p2c';
				$where[] = 'p.product_id = p2c.product_id';
				$where[] = 'p2c.category_id = ' . $cat_id;
			}
			$order_by = SB_Request::getString('order_by', "p.product_name");
			
			if( $type_id > 0 )
			{
				$where[] = "p.type_id = $type_id";
			}
			if( $base_type )
				$where[] = "p.base_type = '$base_type'";
			$products = sb_get_navigation($order_by, 'SB_MBProduct', $columns, $table, $where, null, 'asc');
			$title = __('Products', 'mb');
			sb_set_view_var('stores', SB_Warehouse::GetUserStores($user));
			sb_set_view_var('title', $title);
			sb_set_view_var('products', $products);
			sb_set_view_var('def_search_text', $def_search_text);
			if( $store_id > 0 )
			{
				//sb_set_view_var('categories', SB_Warehouse::getCategories($store_id));
				sb_set_view_var('categories', mb_dropdown_categories(array('store_id' => $store_id, 
																			'name' => 'categories[]',
																			'checkbox' => true
																	)));
			}
			$this->document->SetTitle($title);
		}
		else 
		{
			if( $filter == 'null' )
			{
				sb_set_view_var('title', '<div class="alert alert-warning">' . __('Products without minimal quantity assigned', 'mb') . '</div>');
				sb_set_view_var('keyword', '');
				sb_set_view_var('total_pages', 1);
				sb_set_view_var('current_page', 1);
				sb_set_view_var('products', SB_Warehouse::GetProductsWithNullMinQty());
                sb_set_view_var('stores', SB_Warehouse::GetUserStores($user));
				sb_set_view_var('def_search_text', SBText::_('Name', 'mb'));
			}
			elseif( $filter == 'warning' )
			{
				sb_set_view_var('title', '<div class="alert alert-warning">' . __('Products with minimal quantity warning', 'mb') . '</div>');
				sb_set_view_var('keyword', '');
				sb_set_view_var('total_pages', 1);
				sb_set_view_var('current_page', 1);
				sb_set_view_var('products', SB_Warehouse::GetProductsWithMinQtyWarning());
                sb_set_view_var('stores', SB_Warehouse::GetUserStores($user));
				sb_set_view_var('def_search_text', SBText::_('Name', 'mb'));
			}
			elseif( $filter == 'zero' )
			{
				sb_set_view_var('title', '<div class="alert alert-danger">' . __('Products with quantity danger', 'mb') . '</div>');
				sb_set_view_var('keyword', '');
				sb_set_view_var('total_pages', 1);
				sb_set_view_var('current_page', 1);
				sb_set_view_var('products', SB_Warehouse::GetProductsWithZeroQty());
                sb_set_view_var('stores', SB_Warehouse::GetUserStores($user));
				sb_set_view_var('def_search_text', SBText::_('Name', 'mb'));
			}
		}
		sb_set_view_var('user', $user);
		sb_set_view_var('store_id', $store_id);
		sb_set_view_var('types', SB_Warehouse::getTypes());
		sb_set_view_var('base_type', $base_type);
		sb_add_js_global_var('mb', 'locale', array(
			'ALERT_SELECT_PRODUCTS' 			=> __('You need to select atleast one product', 'mb'),
			'ALERT_SELECT_CATEGORIES'			=> __('You need to select atleast one category', 'mb'),
			'ERROR_SELECT_A_STORE'				=> __('You need to select a store', 'mb'),
			'MSG_SELECTION_SAVED'				=> __('The selection has been saved', 'mb'),
			'MSG_SELECTION_WAS_DELETED'			=> __('The selection has been deleted', 'mb')
		));
		sb_add_style('default', MOD_MB_URL . '/css/default.css');
		sb_add_script(BASEURL . '/js/camera.js', 'camera');
		sb_add_script(BASEURL . '/js/get_barcode_from_image.js', 'barcode');
		sb_add_script(MOD_MB_URL . '/js/products.js', 'products-js', 0, true);
	}
	public function task_new_product()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_create_product') )
		{
			lt_die(__('You dont have enough permissions to create products', 'mb'));
		}
		$title = __('Create New Product', 'mb');
		//sb_set_view_var('categories', SB_Warehouse::getCategories());
		
		sb_add_style('new-product', MOD_MB_URL . '/css/new-product.css');
		sb_add_style('fine-uploaders', BASEURL . '/js/fineuploader/uploader.css');
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fine-uploader');
		sb_add_script(MOD_MB_URL . '/js/products.new.js', 'product-js');
		
		$this->SetView('products/new');
		$this->SetVars(array(
			'types'				=> SB_Warehouse::GetProductTypes(),
			'title'				=> $title,
			'stores'			=> SB_Warehouse::getStores(),
			'warehouse_list'	=> array(),
			'measurement_units'	=> SB_Warehouse::GetMeasurementUnits(),
			'upload_endpoint'	=> SB_Route::_('index.php?mod=mb&task=upload_image'),
			'extensions'		=> array('jpg', 'jpeg', 'gif', 'bmp', 'png'),
			'user'				=> $user
		));
				
		if( $base_type = $this->request->getString('btype') )
		{
			$this->SetVar('base_type', $base_type);
		}
		
		if( (isset($_SERVER['HTTP_REFERER']) && strstr($_SERVER['HTTP_REFERER'], 'keyword')) || SB_Session::getVar('search_link') )
		{
			if( $search_link = SB_Session::getVar('search_link') )
			{
				$this->SetVar('search', $search_link);
			}
			else
			{
				SB_Session::setVar('search_link', $_SERVER['HTTP_REFERER']);
				$this->SetVar('search', $_SERVER['HTTP_REFERER']);
			}
			
		}
		
		//##restart session images for a new product
		SB_Session::unsetVar('product_images');
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_view_product') )
		{
			lt_die(__('You cant view or edit the product', 'mb'));
		}
		$id = $this->request->getInt('id');
		$p  = new SB_MBProduct($id);
		if( !$p->product_id )
		{
			SB_MessagesStack::AddMessage(__('The product does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb'));
		}
		
		$title 	= __('Edit Product', 'mb');
		$p 		= SB_Module::do_action('mb_edit_product', $p);
		
		sb_add_style('new-product', MOD_MB_URL . '/css/new-product.css');
		sb_add_style('fine-uploaders', BASEURL . '/js/fineuploader/uploader.css');
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fine-uploader');
		sb_add_script(MOD_MB_URL . '/js/products.new.js', 'product-js');
		$this->SetView('products/new');
		$this->SetVars(array(
			'user'					=> $user,
			'title'					=> $title,
			'the_product'			=> $p,
			'stores'				=> SB_Warehouse::getStores(),
			'warehouse_list'		=> SB_DbTable::GetTable('mb_warehouse', 1)->GetRows(-1, 0, array('store_id' => $p->store_id)),
			'types'					=> SB_Warehouse::GetProductTypes(),
			'measurement_units'		=> SB_Warehouse::GetMeasurementUnits(),
			'base_type'				=> $p->base_type,
			'upload_endpoint'		=> b_route('index.php?mod=mb&task=upload_image&id='.$p->product_id),
			'extensions'			=> array('jpg', 'jpeg', 'gif', 'bmp', 'png'),
			//'categories'			=> SB_Warehouse::getCategories($p->store_id)
		));
		/*
		if( (isset($_SERVER['HTTP_REFERER']) && strstr($_SERVER['HTTP_REFERER'], 'keyword')) || SB_Session::getVar('search_link') )
		{
			/*
			if( $search_link = SB_Session::getVar('search_link') )
			{
				$this->SetVar('search_link', $search_link);
			}
			else
			{
				SB_Session::setVar('search_link', $_SERVER['HTTP_REFERER']);
				$this->SetVar('search_link', $_SERVER['HTTP_REFERER']);
			}
			
		}*/
		$search_link = null;
		if( isset($_SERVER['HTTP_REFERER']) && !strstr($_SERVER['HTTP_REFERER'], 'view=') )
		{
			$search_link = $_SERVER['HTTP_REFERER'];
			SB_Session::setVar('search_link', $search_link);
		}
		else
		{
			$search_link = SB_Session::getVar('search_link');
		}
		//ump($search_link);
		$this->SetVar('search_link', $search_link);
		$this->document->SetTitle($title);
		//print_r($p);die(__FILE__);
	}
	/**
	 * Delete a product
	 * 
	 * @return  
	 */
	public function task_delete()
	{
		if( !sb_is_user_logged_in() )
		{
			lt_die(__('You need to start a session', 'mb'));
		}
		if( !sb_get_current_user()->can('mb_delete_product') )
		{
			SB_MessagesStack::AddMessage(__('You are not authorized to delete a product.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb'));
		}
		$id = $this->request->getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The product id is invalid', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb'));
		}
		try 
		{
			$this->productModel->Delete($id);
			SB_MessagesStack::AddMessage(__('The product has been deleted', 'mb'), 'success');
			sb_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : SB_Route::_('index.php?mod=mb'));
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddMessage($e->getMessage(), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb'));
		}
	}
	public function task_save()
	{
		$user = sb_get_current_user();
		
		$id = SB_Request::getInt('id');
		if( !$id && !$user->can('mb_create_product') )
		{
			lt_die(__('You cant edit the product', 'mb'));
		}
		elseif( $id && !$user->can('mb_edit_product') )
		{
			lt_die(__('You cant edit the product', 'mb'));
		}
		$productData	= new stdClass();
		$meta 			= array();
		$categories 	= array();
		
		$productData->product_id			= (int)$id;
		$productData->product_code 			= SB_Request::getString('product_code');
		$productData->product_name 			= SB_Request::getString('product_name');
		$productData->product_description 	= SB_Request::getString('description');
		$productData->product_cost			= SB_Request::getFloat('product_cost');
		$productData->product_price			= SB_Request::getFloat('product_sale_price');
		$productData->product_price_2		= SB_Request::getFloat('product_sale_price_2');
		$productData->product_price_3		= SB_Request::getFloat('product_sale_price_3');
		$productData->product_price_4		= SB_Request::getFloat('product_sale_price_4');
		$productData->store_id				= SB_Request::getInt('store_id');
		$productData->warehouse_id			= SB_Request::getInt('warehouse_id');
		$productData->type_id				= SB_Request::getInt('type_id');
		$productData->product_quantity		= SB_Request::getInt('product_quantity');
		$productData->min_stock				= SB_Request::getInt('min_stock');
		$productData->product_unit_measure	= SB_Request::getInt('unit_measure');
		$productData->for_sale				= SB_Request::getInt('for_sale');
		$productData->status				= SB_Request::getString('status', 'publish');
		$productData->base_type				= SB_Request::getString('base_type', 'base');
		$productData->product_barcode		= SB_Request::getString('product_barcode');
		$productData->product_number		= SB_Request::getString('product_serial_number');

		$categories							= (array)SB_Request::getVar('cats', array());
		$images 							= (array)SB_Session::getVar('product_images', array());
		$quantities							= $this->request->getArray('quantities');
		
		if( empty($productData->product_name) )
		{
			SB_MessagesStack::AddMessage(SBText::_('You need to enter a product name', 'mb'), 'error');
			return false;
		}
		
		if( $cat_id = SB_Request::getInt('category_id') )
		{
			$categories[] = $cat_id;
		}
		$meta = array(
				'_discount' 	=> SB_Request::getFloat('product_discount'),
				'_stock_alert' 	=> SB_Request::getInt('stock_alert')
		);
		$meta = array_merge($meta, (array)SB_Request::getVar('meta', array()));
		
		$productData->quantities = $quantities;
		
		if( !$productData->product_id )
		{	
			$id		= $this->productModel->Create($productData, $categories, $meta, $user, $images);
			$msg 	= __('The product has been created', 'mb');
			$link 	= SB_Route::_('index.php?mod=mb');
		}
		else
		{
			$this->productModel->Update($productData, $categories, $meta, $user, $images);
			$msg	= __('The product has been updated', 'mb');
			$link 	= SB_Route::_('index.php?mod=mb&view=edit&id='.$id);
			
		}
		
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_ajax()
	{
		$action = SB_Request::getString('action');
		if( $action == 'get_store_types' )
		{
			$store_id = SB_Request::getInt('store_id');
			$types = SB_Warehouse::GetProductTypes($store_id);
			sb_response_json($types);
		}
		if( $action == 'test' )
		{
			$store_id = SB_Request::getInt('store_id');
			$cats = SB_Warehouse::getCategories($store_id);
			$res = sb_json_encode(array('status' => 'ok', 'categories' => $cats));
			die($res);
		}
		if( $action == 'get_product' )
		{
			$id = SB_Request::getInt('id');
			$prod = new SB_MBProduct($id);
			if( !$prod->product_id )
			{
				sb_response_json(array('status' => 'error', 'error' => __('The product does not exists', 'mb')));
			}
			sb_response_json(array('status' => 'ok', 'product' => $prod));
		}
		
		if( $action == 'get_batches' )
		{
			$res = array(
					'status'	=> 'ok',
					'items'		=> SB_Warehouse::GetBatches() 
			);
			sb_response_json($res);
		}
		if( $action == 'register_spending' )
		{
			$detail 	= SB_Request::getString('detail');
			$amount 	= SB_Request::getFloat('amount');
			$source 	= SB_Request::getString('source');
			$store_id	= SB_Request::getInt('store_id');
			if( empty($detail) )
			{
				sb_response_json(array('status' => 'error', 'error' => __('The spending detail is empty', 'mb')));
			}
			if( $amount <= 0 )
			{
				sb_response_json(array('status' => 'error', 'error' => __('The spending amount is empty', 'mb')));
			}
			$cdate = date('Y-m-d H:i:s');
			$data = array(
					'store_id'					=> $store_id,
					'user_id'					=> sb_get_current_user()->user_id,
					'detail'					=> $detail,
					'amount'					=> $amount,
					'source'					=> $source,
					'ptype'						=> 'output',
					'status'					=> 'complete',
					'payment_date'				=> $cdate,
					'last_modification_date'	=> $cdate,
					'creation_date'				=> $cdate
			);
			$id = $this->dbh->Insert('mb_payments', $data);
			$res = array(
					'status'	=> 'ok',
					'message'	=> __('The spending has been registered', 'mb'),
					'payment_id'	=> $id
			);
			sb_response_json($res);
		}
		parent::task_ajax();
	}
	public function task_import()
	{
        $stores = SB_Warehouse::getStores();
       
		$title = __('Import Products', 'mb');
        //##try to recover the saved request data
        $data = $this->request->GetSaved('import');
        if( $data )
        {
            foreach($data as $key => $val)
            {
                $this->request->setVar($key, $val, 'POST');
            }
            
        }
		$this->SetVars(get_defined_vars());
		$this->document->SetTitle($title);
	}
	public function task_do_import()
	{
        set_time_limit(0);
		$store_id 		= $this->request->getInt('store_id');
		$category_id 	= $this->request->getInt('category_id');
		$sep 			= $this->request->getString('separator', ',');
		$file_type		= $this->request->getString('file_type');
		$initial_stock	= $this->request->getInt('initial_stock');
		$from_path		= $this->request->getInt('from_path');
        $update_only    = (bool)$this->request->getInt('update_prices_only');
       
        $this->request->Save('import');
        $mimes = array(
				'text/csv',
				'text/xls'
		);
        //~ 
		//~ if( SB_Request::getInt('ta') )
		//~ {
			//~ $this->dbh->Query("TRUNCATE mb_categories");
			//~ $this->dbh->Query("TRUNCATE mb_products");
			//~ $this->dbh->Query("TRUNCATE mb_product2category");
			//~ die(__FILE__);
		//~ }
        /*
		//##check for update prices only
		if( !$from_path && $file_type == 'xls_file' && SB_Request::getInt('update_prices_only') )
		{
			$this->task_update_prices();
			return;
		}
        */
        try
        {
            if( $store_id <= 0 )
            {
                throw new Exception(__('You need to select a store', 'mb'));
            }
            if( !$from_path && !isset($_FILES['the_file']) )
            {
                throw new Exception(__('You need to select a file', 'mb'));
            }
            if( !$from_path && $_FILES['the_file']['size'] <= 0 )
            {
                throw new Exception(__('Error uploading file', 'mb'));
            }
            if( !$file_type )
            {
                throw new Exception(__('You need to select a file type', 'mb'));
            }
            if( $file_type == 'monobusiness_csv' && !in_array($_FILES['the_file']['type'], $mimes) )
            {
                throw new Exception(__('The file is not a CSV.'));
            }
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddMessage($e->getMessage(), 'error');
            sb_redirect($this->Route('index.php?mod=mb&view=import'));
        }
        
		$updated = $i = 0;
		if( $file_type == 'monobusiness_csv' )
		{
			$products = array();
			$fh = fopen($_FILES['the_file']['tmp_name'], 'r');
			$headers = explode($sep, fgets($fh));
			if( count($headers) < 4 )
			{
				SB_MessagesStack::AddMessage(SBText::_('Invalid CSV separator'), 'error');
				return false;
			}
			$i = 0;
			while( $line = fgets($fh) )
			{
				$data = array_map(create_function('$el', "return trim(\$el, '\"');"), explode($sep, $line));
				$products[] = array(
						'extern_id'			=> $data[0],
						'product_code'		=> $data[1],
						'product_name'		=> $data[2],
						'product_cost'		=> $data[4],
						'product_price'		=> $data[5],
						'product_price_2'	=> $data[6],
						'product_price_3'	=> $data[7],
						'product_quantity'	=> $data[3],
						'store_id'			=> $store_id
				);
				$i++;
			}
			fclose($fh);
			$dbh = SB_Factory::getDbh();
			$dbh->InsertBulk('mb_products', $products);
		}
		elseif( $file_type == 'monobusiness_file' )
		{
			//##include database driver for sqlite
			sb_include('database' . SB_DS . 'database.sqlite3.php', 'file');
			$destination = TEMP_DIR . SB_DS . basename($_FILES['the_file']['tmp_name']);
			move_uploaded_file($_FILES['the_file']['tmp_name'], $destination);
			$sqlite = new SB_Sqlite3($destination);
			//##get sqlite categories
			$query = "SELECT * FROM categories";
			foreach($sqlite->FetchResults($query) as $cat)
			{
				//##check if category already exists
				if( !$this->dbh->Query("SELECT category_id FROM mb_categories WHERE extern_id = $cat->category_id AND store_id = $store_id LIMIT 1") )
				{
					$data = array(
							'name' 			=> $cat->name,
							'description' 	=> $cat->description,
							'extern_id'		=> $cat->category_id,
							'store_id'		=> $store_id,
							'creation_date'	=> date('Y-m-d H:i:S')
					);
					$this->dbh->Insert('mb_categories', $data);
				}
			}
			//##get local categories
			$categories = array();
			foreach($this->dbh->FetchResults("SELECT * FROM mb_categories WHERE store_id = $store_id") as $row)
			{
				$categories['_'.$row->extern_id] = $row; 
			}
			//##get products from sqlite
			$query = "SELECT p.*, p2c.category_id, ".
							"(SELECT c.name FROM categories c WHERE c.category_id = p2c.category_id) as category ".
						"FROM products p " .
						"LEFT JOIN product2category p2c ON p.product_id = p2c.product_id " .
						"WHERE 1 = 1 ";
			$items = $sqlite->FetchResults($query);
			foreach($items as $item)
			{
				$row = $this->dbh->FetchRow("SELECT product_id FROM mb_products WHERE extern_id = $item->product_id AND store_id = $store_id LIMIT 1");
				if( !$row )
				{
					$data = array(
							'extern_id'				=> $item->product_id,
							'product_code'			=> $item->product_code,
							'product_name'			=> $item->product_name,
							'product_description'	=> $item->product_description,
							'product_cost'			=> $item->product_cost,
							'product_price'			=> $item->product_price,
							'product_price_2'		=> $item->product_price_2,
							'product_price_3'		=> $item->product_price_3,
							//'product_price_4'		=> $item->product_price_4,
							'product_quantity'		=> $item->product_quantity,
							'store_id'				=> $store_id,
							'status'				=> $item->status,
							'min_stock'				=> $item->min_stock,
							'creation_date'			=> date('Y-m-d H:i:s')
					);
					$new_id = $this->dbh->Insert('mb_products', $data);
					$row = (object)array('product_id' => $new_id);
				}
				//##update the product data
				else 
				{
					$data = array(
							'product_cost'			=> $item->product_cost,
							'product_price'			=> $item->product_price,
							'product_price_2'		=> $item->product_price_2,
							'product_price_3'		=> $item->product_price_3,
							//'product_price_4'		=> $item->product_price_4,
							'product_quantity'		=> $item->product_quantity,
							'store_id'				=> $store_id,
							'min_stock'				=> $item->min_stock,
					);
					$this->dbh->Update('mb_products', $data, array('product_id' => $row->product_id));
				}
				//##set product categories
				$this->dbh->Query("DELETE FROM mb_product2category WHERE product_id = $row->product_id");
				$this->dbh->Insert('mb_product2category', 
									array(
											'category_id' => $categories['_'.$item->category_id]->category_id,
											'product_id'	=> $row->product_id				
									)
				);
				$i++;
			}
			$sqlite->Close();
		}
		elseif( $file_type == 'xls_file' )
		{
            $from_path      = $this->request->getInt('from_path');
            $columnsIndex   = (object)$this->request->getVars(array(
                'int:sheet_num',
                'int:row_start',
                'int:code_column',
                'int:barcode_column',
                'int:name_column',
                'int:desc_column',
                'int:qty_column',
                'int:cost_column',
                'int:price_column',
                'int:price_column_2',
                'int:price_column_3',
                'int:price_column_4',
                'int:image_column',
            ));
			//$sheet_num--;
			if( $columnsIndex->sheet_num < 0 )
				$columnsIndex->sheet_num = 0;
			if( $columnsIndex->sheet_num > 0 )
				$columnsIndex->sheet_num--;
			
            try
            {
                
                $excel_file = !$from_path ? $_FILES['the_file']['tmp_name'] : 
                                            BASEPATH . SB_DS . $this->request->getString('the_file');
                if( !is_file($excel_file) )
                    throw new Exception(sprintf(__('The file \'%s\' does not exists', 'mb'), basename($excel_file)));
                
                $result = $this->importModel->ImportExcel($columnsIndex, $excel_file, $store_id, null, 
                                                            $category_id, 
                                                            $initial_stock,
                                                            $update_only);
                $msg = sprintf(__('Import completed, there were %d imported and %d updated products.', 'mb'), 
                                $result['inserts'], $result['updated']);
                SB_MessagesStack::AddMessage($msg, 'success');
                sb_redirect(SB_Route::_('index.php?mod=mb&view=import'));
            }
            catch(Exception $e)
            {
                SB_MessagesStack::AddError($e->getMessage());die($e->getMessage());
				sb_redirect(SB_Route::_('index.php?mod=mb&view=import'));
            }
		}
		
	}
	public function task_update_prices()
	{
		$sheet_num		= SB_Request::getInt('sheet_num');
		$row_start		= SB_Request::getInt('row_start', 1);
		$code_column 	= SB_Request::getInt('code_column', -1);
		$cost_column	= SB_Request::getInt('cost_column', -1);
		$price_column	= SB_Request::getInt('price_column', -1);
		$price_column_2	= SB_Request::getInt('price_column_2', -1);
		$price_column_3	= SB_Request::getInt('price_column_3', -1);
		$price_column_4	= SB_Request::getInt('price_column_4', -1);
			
		$sheet_num--;
		if( $sheet_num < 0 )
			$sheet_num = 0;
		if( $sheet_num > 0 )
			$sheet_num--;
			
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
		$xls 		= PHPExcel_IOFactory::load($_FILES['the_file']['tmp_name']);
		$sheet 		= $xls->setActiveSheetIndex($sheet_num);
		$total_rows = $sheet->getHighestRow();
		/*
		var_dump($sheet_num);
		var_dump($row_start);
		var_dump($total_rows);
		die();
		*/
		$updated = 0;
		for($row = $row_start; $row <= $total_rows; $row++)
		{
			$product_code 		= trim($sheet->getCellByColumnAndRow($code_column, $row)->getCalculatedValue());
			$product_cost		= $cost_column != -1 ? $sheet->getCellByColumnAndRow($cost_column, $row)->getCalculatedValue() : 0;
			$product_price		= $price_column != -1 ? $sheet->getCellByColumnAndRow($price_column, $row)->getCalculatedValue() : 0;
			$product_price_2	= $price_column_2 != -1 ? $sheet->getCellByColumnAndRow($price_column_2, $row)->getCalculatedValue() : 0;
			$product_price_3	= $price_column_3 != -1 ? $sheet->getCellByColumnAndRow($price_column_3, $row)->getCalculatedValue() : 0;
			$product_price_4	= $price_column_4 != -1 ? $sheet->getCellByColumnAndRow($price_column_4, $row)->getCalculatedValue() : 0;
		
			
			if( $p = SB_Warehouse::GetProductBy('code', $product_code) )
			{
				$data = array();
				if( $product_cost > 0 )
					$data['product_cost'] = $product_cost;
				if( $product_price > 0 )
					$data['product_price'] = $product_price;
				if( $product_price_2 > 0 )
					$data['product_price_2'] = $product_price_2;
				if( $product_price_3 > 0 )
					$data['product_price_3'] = $product_price_3;
				if( $product_price_4 > 0 )
					$data['product_price_4'] = $product_price_4;
				if( count($data) )
				{
					//##update the product
					$this->dbh->Update('mb_products', $data, array('product_code' => $p->product_code));
					$updated++;
				}
			}
		}
		SB_MessagesStack::AddMessage(sprintf(__('Product prices update complete. Total updated: %d', 'mb'), $updated), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=import'));
	}
	public function task_settings()
	{
		$ops = sb_get_parameter('mb_settings');
		sb_set_view_var('ops', $ops);
		$this->document->SetTitle(__('Mono Business Settings', 'mb'));
	}
	public function task_save_settings()
	{
		$prev_mb_settings = (array)sb_get_parameter('mb_settings', array());
		$ops = array();
        foreach($this->request->getArray('ops', array()) as $key => $option)
        {
            if( is_array($option) || is_object($option) )
                $ops[$key] = $option;
            else
                $ops[$key] = trim($option);
        }
		$uploaded = false;
		if( isset($_FILES['business_logo']) && $_FILES['business_logo']['size'] > 0 )
		{
			$file_logo = sb_get_unique_filename($_FILES['business_logo']['name'], UPLOADS_DIR);
			$uploaded = move_uploaded_file($_FILES['business_logo']['tmp_name'], $file_logo);
			$ops['business_logo'] = basename($file_logo);
		}
		$ops = array_merge($prev_mb_settings, $ops);
		sb_update_parameter('mb_settings', $ops);
		//##get and override global settings
		$settings = (object)sb_get_parameter('settings', array());
		$settings->SITE_TITLE = $ops['business_name'];
		if( $uploaded )
		{
			$settings->SITE_LOGO = $ops['business_logo'];
		}
		sb_update_parameter('settings', $settings);
		SB_Module::do_action('mb_save_settings', $ops);
		SB_MessagesStack::AddMessage(__('Settings saved', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=settings'));
	}
	public function task_providers_list()
	{
		$query = "SELECT * FROM mb_suppliers";
		$rows = $this->dbh->FetchResults($query);
		sb_set_view_var('rows', $rows);
	}
	public function task_upload_image()
	{
		$id = SB_Request::getInt('id');
		sb_include('qqFileUploader.php', 'file');
		$res = array();
		try
		{
			$uh = new qqFileUploader();
			//$uh->allowedExtensions = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'psd', 'tiff', 'ai');
			$uh->inputName = 'qqfile';
			// If you want to use resume feature for uploader, specify the folder to save parts.
			$uh->chunksFolder 		= 'chunks';
			$res 					= $uh->handleUpload(MOD_MB_PROD_IMAGE_DIR);
			if( isset($res['error']) )
			{
				throw new Exception($res['error']);
			}
			$filename 				= $uh->getUploadName();
			$aid 					= lt_insert_attachment(MOD_MB_PROD_IMAGE_DIR . SB_DS . $filename, 'product', $id, 0, 'image');
			$res['attachment_id'] 	= $aid;
			if( !$id )
			{
				$images = (array)SB_Session::getVar('product_images', array());
				$images[] = $aid;
				SB_Session::setVar('product_images', $images);
			}
			else
			{
				
			}
			$res['url'] = UPLOADS_URL . '/products/' . $filename; 
		}
		catch(Exception $e)
		{
			$res['error'] = $e->getMessage();
		}
		
		sb_response_json($res);
	}
	public function task_set_featured_image()
	{
		$id = SB_Request::getInt('id');
		$img_id = SB_Request::getInt('img_id');
		if( !$id )
			die(__('Invalid product id', 'mb'));
		mb_update_product_meta($id, '_featured_image_id', $img_id);
		die();
	}
	public function task_print_catalog()
	{
		set_time_limit(0);
		ini_set('memory_limit', '512M');
		$ids		= $this->request->getArray('ids');
		$store_id 	= $this->request->getInt('store_id');
		$cat_id 	= $this->request->getInt('cat_id');
		$type 		= $this->request->getString('type', 'pdf');
		$obj 		= $this->productModel->BuildCatalog($store_id, $ids, $cat_id, $type);
		if( $type == 'pdf' )
			$obj->Output(sprintf(__('products-catalog-%s'), sb_format_date(date('Y-m-d'))), 'I');
		elseif( $type == 'excel' )
		{
			$cb = $obj->Output;
			$cb($obj);
		}
			
	}
	public function task_print_labels()
	{
		set_time_limit(0);
		ini_set('memory_limit', '512M');
		$store_id   	= SB_Request::getInt('store_id');
		$cat_id			= SB_Request::getInt('category_id');
		$cellsNum		= $this->request->getInt('columns', 5);
		$printBarcode	= $this->request->getInt('print_barcode');
		$printPrice		= $this->request->getInt('print_price');
		$products 		= $this->request->getArray('products');
		$title 			= '';
		
		$builder = SB_Factory::getQueryBuilder();
		$query = $builder->select();
		$query->setTable('mb_products')
				->join('mb_stores', 'store_id', 'store_id');
		$query->where()->equals('store_id', $store_id);
		if( count($products) )
		{
			$query->where()
				->in('product_id', $products);
		}
		if( $cat_id )
		{
			$query->join('mb_product2category', 'product_id', 'product_id')
				->where()
				->equals('category_id', $cat_id);
		}
		$sql 		= $builder->writeWithValues($query);
		$products 	= $this->dbh->FetchResults($sql);
        $pdf 		= $this->productModel->BuildLabels($products, $cellsNum, $printBarcode, $printPrice);
        $pdf->Output(sprintf(__('products-labels-%s.pdf'), sb_format_date(date('Y-m-d'))), 'I');
		die();
	}
	public function task_batch()
	{
		$action = SB_Request::getString('action');
		$ids	= SB_Request::getVar('ids');
       
        try
        {
            if( !$action || $action == '-1' )
                throw new Exception($this->__('No batch action selected'));
            if( $action == 'delete' )
            {
                $ids_str = implode(',', $ids);
                $this->dbh->Query("DELETE FROM mb_product_meta WHERE product_id IN($ids_str)");
                $this->dbh->Query("DELETE FROM mb_product2category WHERE product_id IN($ids_str)");
                $this->dbh->Query("DELETE FROM mb_product2suppliers WHERE product_id IN($ids_str)");
                $this->dbh->Query("DELETE FROM mb_product2tag WHERE product_id IN($ids_str)");
                $this->dbh->Query("DELETE FROM mb_products WHERE product_id IN($ids_str)");
                SB_MessagesStack::AddMessage(__('The products has been deleted', 'mb'), 'success');
                sb_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : SB_Route::_('index.php?mod=mb'));
            }
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddError($e->getMessage());
            sb_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : SB_Route::_('index.php?mod=mb'));
        }
	}
	public function task_remove_image()
	{
		$id = SB_Request::getInt('id');
		$this->productModel->RemoveImage($id);
		die();
	}
	public function task_get_images()
	{
		$id = SB_Request::getInt('id');
		$the_product = new SB_MBProduct($id);
		ob_start();
		if( $the_product->product_id ): foreach($the_product->getImages() as $img): ?>
		<div class="product-image">
			<img src="<?php print $img->GetUrl('330x330'); ?>" alt="" class="img-responsive thumbnail" />
			<a href="javascript:;" class="btn btn-default btn-xs btn-delete-img" 
				title="<?php _e('Remove image', 'mb'); ?>"
				data-id="<?php print $img->id ?>"
				data-product_id="<?php print $the_product->product_id ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</div>
		<?php endforeach; endif;
		$res = array('status' => 'ok', 'html' => ob_get_clean());
		sb_response_json($res);
	}
	public function task_closemanagement()
	{
		set_time_limit(0);
		$user 	= sb_get_current_user();
		$stores = SB_Warehouse::getStores();
		foreach($stores as $store)
		{
			$store->CloseManagement();
		}
	}
	public function task_startmanagement()
	{
		set_time_limit(0);
		$user 	= sb_get_current_user();
		$stores = SB_Warehouse::getStores();
		foreach($stores as $store)
		{
			$store->StartManagement();
		}
	}
	public function task_set_bulk_categories()
	{
		if( !sb_is_user_logged_in() )
		{
			lt_die(__('You need to start a session', 'mb'));
		}
		$ajax		= SB_Request::getInt('ajax');
		$products 	= SB_Request::getVar('products');
		$categories = SB_Request::getVar('categories');
		foreach($products as $_id)
		{
			$id = (int)$_id;
			if( !$id ) continue;
			//##delete product categories
			$this->dbh->Query("DELETE FROM mb_product2category WHERE product_id = $id");
			foreach($categories as $cat_id)
			{
				$this->dbh->Insert('mb_product2category', array('product_id' => $id, 'category_id' => $cat_id));
			}
		}
		$msg = __('The categories has been applied', 'mb');
		if( $ajax )
		{
			sb_response_json(array(
				'status' => 'ok',
				'message'	=> $msg
			));
		}
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb'));
	}
	public function task_export()
	{
		$fields = array(
			'product_id' 	=> __('Product ID', 'mb'),
			'product_code' 	=> __('Product Code', 'mb'),
			'product_name' 	=> __('Product Name', 'mb'),
			'product_quantity'	=> __('Quantity', 'mb'),
			'product_cost'	=> __('Cost', 'mb'),
			'product_price'	=> __('Price', 'mb'),
			'store_id'		=> __('Store ID', 'mb'),
			'store_name' 	=> __('Store Name', 'mb'),
			'categories'	=> __('Categories', 'mb'),
			'dimensions'	=> __('Dimensions', 'mb')
		);
		$user = sb_get_current_user();
		sb_set_view_var('fields', $fields);
		sb_set_view_var('user', $user);
	}
	public function task_do_export()
	{
		set_time_limit(0);
		$store_id 		= $this->request->getInt('store_id');
		$category_id	= $this->request->getInt('category_id');
		$fields 	= (array)SB_Request::getVar('fields', array());
		Header('Content-Description: File Transfer');
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachement; filename="products.csv"');
		$this->productModel->Export($store_id, $category_id, $fields, 'php://output');
		die();
	}
	public function ajax_get_store_cats()
	{
		
		$store_id 	= $this->request->getInt('store_id');
		$type		= $this->request->getString('type', 'array');
		$cb			= $this->request->getString('checkbox', false);
		$cats 		= mb_dropdown_categories(array('store_id' => $store_id, 'type' => $type, 'checkbox' => $cb));
		//$cats = SB_Warehouse::getCategories($store_id);
		sb_response_json(array('status' => 'ok', 'categories' => $cats));

	}
	public function ajax_search_product()
	{
		$user			= sb_get_current_user();
		$keyword 		= $this->request->getString('keyword');
		$store_id 		= $this->request->getInt('store_id');
		$warehouse_id	= $this->request->getInt('warehouse_id');
		if( !$user->IsRoot() && !$user->can('mb_see_all_stores') && !$store_id )
		{
			sb_response_json(array('status' => 'error', 'error' => __('You need to specify the store to search', 'mb')));
		}
		$cols		= array(
				'product_id AS id',
				'product_id',
				'product_code',
				'product_name AS name',
				'product_name',
				'product_quantity',
				'product_cost',
				'product_price',
				'product_price_2',
				'product_price_3',
				'product_price_4'
		);
		SB_Module::do_action_ref('mb_search_product_columns', $cols);
		if( $this->dbh->db_type == 'mysql' )
			$cols[] = 'CONCAT(product_code, \' - \',product_name) AS label';
		elseif( $this->dbh->db_type == 'sqlite3' )
			$cols[] = '(product_code || \' - \' || product_name) AS label';
			
			
		$query = sprintf("SELECT %s ", implode(',', $cols)) .
						"FROM mb_products ".
						"WHERE (product_name LIKE '%$keyword%' OR product_code LIKE '%$keyword%' OR product_barcode LIKE '%$keyword%') ";
		if( $store_id > 0 )
		{
			$query .= "AND store_id = $store_id ";
		}
		/*
		if( $warehouse_id > 0 )
		{
			$query .= "AND warehouse_id = $warehouse_id ";
		}
		*/
		$query .= "ORDER BY product_name";
		$rows = $this->dbh->FetchResults($query);
		SB_Module::do_action_ref('mb_ajax_search_results', $rows);
		for($i = 0; $i < count($rows); $i++ )
		{
			$rows[$i]->label = $rows[$i]->product_quantity <= 0  ?
								sprintf("<span class=\"text-danger\">%d - %s (%.2f)</span>", $rows[$i]->product_id, $rows[$i]->label, $rows[$i]->product_price) :
								sprintf("<span class=\"text-success\">%d - %s (%.2f)</span>", $rows[$i]->product_id, $rows[$i]->label, $rows[$i]->product_price);
		}
		sb_response_json(array('status' => 'ok', 'results' => $rows, 'query' => $query));
	}
	public function ajax_search_one_product()
	{
		$store_id 		= SB_Request::getInt('store_id');
		$warehouse_id 	= SB_Request::getInt('warehouse_id');
		$keyword 		= SB_Request::getString('keyword');
		$cols		= array(
				'product_id AS id',
				'product_id',
				'product_code',
				'product_name AS name',
				'product_name',
				'product_quantity',
				'product_cost',
				'product_price',
				'product_price_2',
				'product_price_3',
				'product_price_4'
		);
		SB_Module::do_action_ref('mb_search_one_columns', $cols);
		if( $this->dbh->db_type == 'mysql' )
			$cols[] = 'CONCAT(product_code, \' - \',product_name) AS label';
		elseif( $this->dbh->db_type == 'sqlite3' )
			$cols[] = '(product_code || \' - \' || product_name) AS label';
			
		$where = array();
		if( (int)$keyword )
		{
			$id = (int)$keyword;
			$where[] = "product_id = $id";
		}
		$where[] = "product_code = '$keyword'";
		$where[] = "product_barcode = '$keyword'";
		
		$query = sprintf("SELECT %s FROM mb_products WHERE 1 = 1 AND(%s) ", 
						implode(',', $cols),
						implode(' OR ', $where));
		if( $store_id > 0 )
		{
			$query .= "AND store_id = $store_id ";
		}
		if( $warehouse_id > 0 )
		{
			$query .= "AND warehouse_id = $warehouse_id ";
		}
		$query .= "LIMIT 1";
		$row = $this->dbh->FetchRow($query);
		if( !$row )
		{
			sb_response_json(array('status' => 'not_found', 'message' => __('Product not found', 'mb'), 'sql' => $query));
		}
		sb_response_json(array('status' => 'ok', 'message' => __('Product found', 'mb'), 'product' => $row));
	}
	public function ajax_get_warehouses()
	{
		$store_id	= $this->request->getInt('store_id');
		$items 		= $this->storesModel->GetWarehouses($store_id);
		
		$res = array(
				'status'	=> 'ok',
				'items'		=> $items
		);
		sb_response_json($res);		
	}
	public function ajax_get_warehouses_html()
	{
		$store_id		= $this->request->getInt('store_id');
		$product_id		= $this->request->getInt('product_id');
		$items 			= $this->storesModel->GetWarehouses($store_id);
		$tpl			= lt_get_view('mb', 'products/warehouses-list.php');
		
		if( $product_id )
			$the_product	= $this->productModel->Get($product_id);
		ob_start();
		foreach($items as $i => $w)
		{
			include $tpl;
		}
		$html = ob_get_clean();
		sb_response_json(array('status' => 'ok', 'html' => $html));
	}
	public function ajax_get_product_quantity()
	{
		$product_id 	= $this->request->getInt('product_id');
		$warehouse_id 	= $this->request->getInt('warehouse_id');
		
	}
	public function ajax_get_product_quantities()
	{
		$product_id 	= $this->request->getInt('product_id');
		if( !$product_id )
			throw new Exception(__('Invalid product identifier', 'mb'));
		$items			= $this->productModel->GetWarehouses($product_id);
		sb_response_json(array('status' => 'ok', 'items' => $items));
	}
    public function ajax_buildbarcodes()
    {
        $ids = $this->request->getArray('ids');
        try
        {
            
            if( !is_array($ids) || !count($ids) )
                throw new Exception($this->__('Invalid product identifiers'));
            $mb_settings = (object)sb_get_parameter('mb_settings', new stdClass());
            
            for($i = 0; $i < count($ids); $i++)
            {
                $id = $ids[$i];
                $product = $this->productModel->Get((int)$id);
                if( !$product || !$product->product_id ) continue;
                $barcode = SB_BarcodeEAN13::Build($mb_settings->barcode_ean13_country, $mb_settings->barcode_ean13_company, $id);
                
                $this->dbh->Update('mb_products', 
                    array('product_barcode' => $barcode ),
                    array('product_id' => $product->product_id )
                );
            }
        }
        catch(Exception $e)
        {
            sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
        }
        die();
    }
    public function ajax_generate_barcode()
    {
        $res = array('status' => 'ok');
        $id = SB_Request::getInt('id');
        if( !$id )
        {
            $res['status'] = 'error';
            $res['error'] = __('Invalid product identifier', 'mb');
            sb_response_json($res);
        }
        
        
        $res['barcode'] = SB_BarcodeEAN13::Build($mb_settings->barcode_ean13_country, $mb_settings->barcode_ean13_company, $id);
        sb_response_json($res);
    }
}
