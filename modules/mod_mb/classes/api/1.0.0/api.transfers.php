<?php
class MB_API_Transfers extends SB_ApiRest
{
	public function __construct($dbh = null)
	{
		$this->dbTable = SB_DbTable::GetTable('mb_transfers', 1, $dbh);
		parent::__construct($dbh);
	}
	public function Save()
	{
		$data = file_get_contents('php://input');
		if( empty($data) )
			$this->ResponseError(__('The data is empty', 'mb'));
		$transfer_data = json_decode($data);
		if( !$transfer_data )
			$this->ResponseError(__('The data format is invalid', 'mb'));
		if( !$transfer_data->items || !count($transfer_data->items) )
			$this->ResponseError(__('The transfer has no items', 'mb'));
		
		try
		{
			$transfer 		= new SB_MBTransfer();
			foreach($transfer_data->items as $id => $item)
			{
				$product = new SB_MBProduct($item->product_id);
				if( !$product->product_id )
				{
					$error = sprintf(__('The product with id "%d" does not exists', 'mb'), $item->product_id);
					throw new Exception($error);
				}
				if( $product->warehouse_id == $transfer_data->to_warehouse )
				{
					throw new Exception(__('Invalid target warehouse', 'mb'));
				}
				if( $product->product_quantity <= 0 || (int)$product->product_quantity < (int)$item->quantity )
				{
					$error = sprintf(__('The product with code "%s" has no enough stock to transfer', 'mb'), $product->product_code);
					throw new Exception($error);
				}
				if( SB_Module::do_action('mb_transfer_before_add_item', true, $product) )
				{
					$transfer->AddProduct($product, $item->quantity, null, isset($item->batch) ? $item->batch : '');
				}
			}
			$id = $transfer->DoTransfer($transfer_data->from_store, $transfer_data->from_warehouse, 
									$transfer_data->to_store, $transfer_data->to_warehouse, 
									'', '', $transfer_data->user_id, $transfer_data->details
			);
			/*
			$transfer_data['status'] = isset($transfer_data['status']) ? $transfer_data['status'] : 'on_the_way'; 
			$transfer_data['creation_date'] = date('Y-m-d H:i:s');
			print_r($transfer_data);
			*/	
			$this->Response(array('id' => $id), 'ok', 200, __('Transfer Registered', 'mb'));
		}
		catch(Exception $e)
		{
			$this->ResponseError($e->getMessage());
		}
	}
}