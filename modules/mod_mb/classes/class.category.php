<?php
//namespace SinticBolivia\MonoBusiness\Classes;

use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_AttachmentImage;
use SinticBolivia\SBFramework\Classes\SB_Route;
/**
 * 
 * @author marcelo
 * @property int category_id
 * @property string name
 * @property int store_id
 */
class SB_MBCategory extends SB_ORMObject
{
	protected $_products 		= array();
	protected $parentCategory 	= null;
	public function __construct($category_id = null)
	{
		parent::__construct();
		if( $category_id )
			$this->GetDbData($category_id);
	}
	public function GetDbData($id)
	{
		$column = is_int($id) ? "category_id = $id" : "slug = '$id'";
		$query = "SELECT * FROM mb_categories WHERE $column LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
		
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbParentCategory()
	{
		if( (int)$this->parent > 0 )
		{
			$this->parentCategory = new SB_MBCategory((int)$this->parent);
		}
		else
		{
			$this->parentCategory = null;
		}
	}
	public function GetDbProducts()
	{
		$query = "SELECT p.* FROM mb_products p, mb_product2category p2c 
					WHERE 1 = 1 
					AND p.product_id = p2c.product_id
					AND p2c.category_id = $this->category_id ";
		$this->_products = $this->dbh->FetchResults($query);
	}
	/**
	 * Get the category parent
	 * 
	 * @return  SB_MBCategory
	 */
	public function GetParentCategory()
	{
		if( (int)$this->parent === 0 )
			return null;
		if( !$this->parentCategory )
			$this->GetDbParentCategory();
		
		return $this->parentCategory;
	}
	/**
	 * Get category latest products
	 * 
	 * @param int $limit 
	 * @return  array
	 */
	public function GetLatestProducts($limit = 5)
	{
		$query = "SELECT p.* 
					FROM mb_products p, mb_product2category p2c 
					WHERE 1 = 1 
					AND p.product_id = p2c.product_id
					AND p2c.category_id = $this->category_id 
					ORDER BY p.creation_date DESC
					LIMIT 0, $limit";
		
		$prods = array();
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$p = new SB_MBProduct();
			$p->SetDbData($row);
			$prods[] = $p;
		}
		return $prods;
	}
	public function getProducts()
	{
		if( count($this->_products) <= 0)
		{
			$prods = array();
			$i = 0;
			foreach($prods as $p)
			{
				$this->_products[$i] = new SB_MBProduct();
				$this->_products[$i]->SetDbData($p);
				$i++; 
			}
		}
		return $this->_products;
	}
	/**
	 * Get Category Attachment Image
	 * @return  SB_AttachmentImage
	 */
	public function GetImage()
	{
		if( !$this->image_id )
			return '';
		return new SB_AttachmentImage($this->image_id);
	}
	public function GetThumbnail($size = '330x330')
	{
		$img = $this->GetImage();
		if( !$img )
			return null;
		return $img->GetThumbnail($size);
	}
	public function __get($var)
	{
		if( $var == 'link' )
		{
			$slug = $this->slug ? $this->slug : sb_build_slug($this->name);
			return SB_Route::_('index.php?mod=emono&view=category&id='.$this->category_id . '&slug='.$slug);
		}
			
		return parent::__get($var);
	}
	public function jsonSerialize()
	{
		return $this->_dbData;
	}
	public function CountProducts()
	{
		$query = "SELECT COUNT(p.product_id) 
					FROM mb_products p, mb_product2category p2c 
					WHERE 1 = 1 
					AND p.product_id = p2c.product_id
					AND p2c.category_id = $this->category_id";
		
		return (int)$this->dbh->GetVar($query);
	}
}
