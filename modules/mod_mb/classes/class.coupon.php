<?php
namespace SinticBolivia\MonoBusiness\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
class SB_MBCoupon extends SB_ORMObject
{
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_coupons WHERE coupon_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public static function GetCouponByCode($code)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_coupons WHERE code = '$code' LIMIT 1";
		if( !$dbh->Query($query) )
			return null;
		$coupon = new SB_MBCoupon();
		$coupon->SetDbData($dbh->FetchRow());
		
		return $coupon;
	}
}