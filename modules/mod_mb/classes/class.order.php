<?php
//namespace SinticBolivia\MonoBusiness\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Modules\Customers\Classes\SB_MBCustomer;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;
/**
 * 
 * @author marcelo
 * @property int 	order_id;
 * @property string code
 * @property int 	store_id
 * @property int 	supplier_id
 * @property int 	items Total items in order
 * @property float 	subtotal
 * @property float 	total_tax
 * @property float 	discount
 * @property float 	total
 * @property string details
 * @property string status
 * @property string payment_status
 * @property int	user_id
 * @property int	customer_id
 * @property string	order_date
 * @property string delivery_date
 * @property string type
 * @property string	last_modification_date
 * @property string creation_date
 * @property SB_MBCustomer customer
 */
class SB_MBOrder extends SB_ORMObject
{
	/**
	 * 
	 * @var SB_MBCustomer
	 */
	public $customer        = null;
	protected $orderItems   = array();
	protected $store        = null;
    protected $user         = null;
    
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT o.*, s.store_name ".
					"FROM mb_orders o LEFT JOIN mb_stores s ON o.store_id = s.store_id ".
					"WHERE order_id = $id ".
					"" .
					"LIMIT 1";
		if( !$this->dbh->Query($query) )
			return null;
		$this->_dbData = $this->dbh->FetchRow();
		$query = "SELECT * FROM mb_order_meta WHERE order_id = $id";
		if( $this->dbh->Query($query) )
		{
			foreach($this->dbh->FetchResults() as $row)
			{
				$this->meta[$row->meta_key] = trim($row->meta_value);
			}
		}
		if( $this->customer_id )
		{
			$this->customer = new SB_MBCustomer($this->customer_id);
		}
		//##get attachments
		$query = "SELECT * FROM attachments WHERE object_type = 'order' AND object_id = $this->order_id ";
		$this->attachments = $this->dbh->FetchResults($query);
	}
	public function SetDbData($data)
	{
		$this->_dbData = (object)$data;
		if( (!isset($data->customer) || empty($data->customer)) && $this->customer_id )
		{
			$this->customer = new SB_MBCustomer($this->customer_id);
		}
	}
	public function GetItems()
	{
		if( !count($this->orderItems) )
		{
			$query = "SELECT i.*, p.product_code, p.product_name ".
						"FROM mb_order_items i ".
						"LEFT JOIN mb_products p ON i.product_id = p.product_id " .
						"WHERE 1 = 1 " .
						"AND i.order_id = $this->order_id ".
						"ORDER BY item_id ASC";
			if( $this->dbh->Query($query) )
			{
				$this->orderItems = $this->dbh->FetchResults();
				
			}
		}
		return $this->orderItems;
	}
	public function SetItems($items)
	{
		$this->orderItems = $items;
	}
	public function GetStore()
	{
		if( !$this->store )
		{
			$this->store = new SB_MBStore($this->store_id);
		}
		return $this->store;
	}
    public function GetUser()
    {
        if( !$this->user )
            $this->user = new SB_User($this->user_id);
        
        return $this->user;
    }
	public function ChangeStatus($status)
	{
		$this->dbh->Update('mb_orders', array('status' => $status), array('order_id' => $this->order_id));
		SB_Module::do_action('mb_order_status_changed', $this, $status);
		SB_Module::do_action('mb_order_status_changed_' . $status, $this);
	} 
	/**
	 * Update stock and mark order with a status
	 * 
	 */
	public function Complete($status = 'complete')
	{
		$this->UpdateStock();	
		$this->ChangeStatus($status);
		SB_Module::do_action('order_completed', $this);
	}
	/**
	 * Update stock only
	 */
	public function UpdateStock()
	{
		$kardex = array();
		$store = $this->GetStore();
		foreach($this->GetItems() as $item)
		{
			$sql_operation = "OP[product_quantity - {$item->quantity}]";
			$this->dbh->Update('mb_products', 
								array('product_quantity' => $sql_operation), 
								array('product_id' => $item->product_id)
			);
			$product 	= new SB_MBProduct($item->product_id);
			//##build update product kardex
			$ki 		= $this->BuildKardexItem($item, $product, 'output', $store->_sale_tt_id);
			$kardex[] 	= $ki;
			//##check if product is an assembly
			if( $product->base_type == 'asm' )
			{
				foreach($product->GetAsmComponents() as $com)
				{
					$subitem = (object)array(
						'product_id'	=> $com->product->product_id,
						'quantity'		=> $com->qty_required,
						'price'			=> $com->product->product_price
					);
					//##build update product kardex
					$ki = $this->BuildKardexItem($subitem, $product, 'output', $store->_sale_tt_id);
					$kardex[] = $ki;
				}
			}
		}
		$kardex = SB_Module::do_action('mb_before_insert_kardex', $kardex, $this, 'output');
		$this->dbh->InsertBulk('mb_product_kardex', $kardex);
	}
	public function BuildKardexItem($item, $product, $inout, $transaction_type_id)
	{
		$ki = array(
				'product_id'			=> $item->product_id,
				'in_out'				=> $inout,
				'quantity'				=> $item->quantity,
				'quantity_balance'		=> (int)$product->product_quantity,
				'unit_price'			=> $item->price,
				'total_amount'			=> $item->quantity * $item->price,
				'monetary_balance'		=> $product->product_quantity * $product->product_cost,
				'transaction_type_id'	=> (int)$transaction_type_id,
				'author_id'				=> $this->user_id,
				'transaction_id'		=> $this->order_id,
				'creation_date'			=> date('Y-m-d H:i:s')
		);
		$ki = SB_Module::do_action('mb_build_kardex_item', $ki, $item, $product, $this, $inout);
		
		return $ki;
	}
	/**
	 * Cancel the order and revert back the stock quantity
	 * @return boolean
	 */
	public function Cancel($revert_kardex = true)
	{
		if( $this->status == 'cancelled' )
			return false;
		//##revert to stock
		$ids = array();
		foreach($this->GetItems() as $item)
		{
			$this->RevertItem($item, $revert_kardex);
			
		}
		if( $revert_kardex )
		{
			//##delete items from kardex
			$kqd = "DELETE FROM mb_product_kardex WHERE transaction_id = $this->order_id AND in_out = 'output'";
			$this->dbh->Query($kqd);
		}
		
		$this->ChangeStatus('cancelled');
		SB_Module::do_action('mb_order_cancelled', $this, $revert_kardex);
		return true;
	}
	public function RevertItem($item, $revert_kardex = true)
	{
		//##revert global quantity
		$query = "UPDATE mb_products ".
					"SET product_quantity = (product_quantity + $item->quantity) ".
					"WHERE product_id = $item->product_id ".
					"AND store_id = $this->store_id";
		$this->dbh->Query($query);
		SB_Module::do_action('mb_order_revert_item', $item, $revert_kardex, $this);
	}
	public function RevertItemByProductId($id)
	{
		$subquery = "SELECT oi.quantity FROM mb_order_items oi WHERE oi.order_id = $this->order_id and oi.product_id = $id LIMIT 1";
		$query = "UPDATE mb_products SET product_quantity = product_quantity + ($subquery) WHERE product_id = $id AND store_id = $this->store_id";
		$this->dbh->Query($query);
	}
}
