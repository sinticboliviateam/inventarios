<?php
//namespace SinticBolivia\SBFramework\Modules\Mb\Classes;

use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_AttachmentImage;
use SinticBolivia\SBFramework\Classes\SB_Route;
/**
 * 
 *@author marcelo
 *@property int product_id
 *@property string		product_code
 *@property string product_name
 *@property int store_id
 *@property int product_quantity
 *@property float product_price
 *@property float product_price_2
 *@property float product_price_3
 *@property float product_price_4
 */
class SB_MBProduct extends SB_ORMObject
{
    protected 	$_store 		= null;
    protected 	$_images 		= array();
    protected 	$_categories 	= array();
    public 		$categories_ids = array();
    public 		$kardex 		= array();
    protected	$warehouse;
	
	public function __construct($product_id = null)
	{
		parent::__construct();
		if( $product_id )
		    $this->GetDbData($product_id);
	}
	public function GetDbData($product_id)
	{
		$column = (int)$product_id > 0 ? "product_id = $product_id" : "slug = '$product_id'";
		$query = "SELECT * FROM mb_products WHERE $column LIMIT 1";
		$dbh = SB_Factory::getDbh();
		if( !$dbh->Query($query) )
			return false;
		$this->_dbData = $dbh->FetchRow();
		$this->GetDbMeta();
		//get categories
		$this->GetDbCategories();
		//##get store
		$this->_store = new SB_MBStore($this->store_id);
		$this->warehouse = new SB_MBWarehouse($this->warehouse_id);
		$this->getImages();
		
		/*
		//get product kardex
		//$query = "SELECT k.*, tt.transaction_key, tt.transaction_name, tt.transaction_description, tt.in_out 
		$query = "SELECT k.*, tt.transaction_key, tt.transaction_name, tt.transaction_description
					FROM product_kardex k 
					LEFT JOIN transaction_types tt ON k.transaction_type_id = tt.transaction_type_id 
					WHERE product_code = '$this->product_code' 
					ORDER BY creation_date DESC";
		
		$res = $dbh->Query($query);
		$this->kardex = $dbh->FetchResults();
		//print_r($this->kardex);
		*/
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
		if( $this->store_id )
		{
		    $this->_store = new SB_MBStore($this->store_id);
		}
		if( $this->warehouse_id )
		{
		    $this->warehouse = new SB_MBWarehouse($this->warehouse_id);
		}
		$this->getImages();
		$this->GetDbCategories();
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_product_meta WHERE product_id = $this->product_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = trim($row->meta_value);
		}
	}
	public function GetDbCategories()
	{
		$query = "SELECT c.* ".
					"FROM mb_product2category p2c, mb_categories c " .
					"WHERE c.category_id = p2c.category_id " .
					"AND p2c.product_id = $this->product_id ".
					"ORDER BY c.parent ASC";
		$res = $this->dbh->Query($query);
		if($res)
		{
			foreach($this->dbh->FetchResults() as $r)
			{
				$cat = new SB_MBCategory();
				$cat->SetDbData($r);
				$this->_categories[] = $cat;
				$this->categories_ids[] = $r->category_id;
			}
		}
	}
	
	public function getImages()
	{
		if( !$this->product_id )
			return array();
			
	    if( empty($this->_images) )
	    {
	    	$query = "SELECT * FROM attachments 
						WHERE object_type = 'product' 
						AND object_id = $this->product_id 
						AND type = 'image'
						AND parent = 0";
	    	$this->_images = array();
			foreach($this->dbh->FetchResults($query) as $row)
			{
				//$img 		= new SB_MBProductImage($row, MOD_MB_PROD_IMAGE_URL);
				$img	= new SB_AttachmentImage();
				$img->SetDbData($row);
				$this->_images[] = $img;
			}
	    }
		
	    return $this->_images;
	}
	public function getStore()
	{
		return $this->_store;
	}
	/**
	 * Example: $size => '55x55'
	 * @brief Get product featured image
	 * @param string $size The image size 
	 * @return  string image url
	 */
	public function getFeaturedImage($size = null)
	{
		$this->getImages();
		
		if( !(int)$this->_featured_image_id )
		{
			if( !count($this->_images) )
			{
				$img = new SB_AttachmentImage();
				$img->SetDbData((object)array('file' => '../images/no-image.png'));
				return $img;
			}
			else 
			{
				return $size ? current($this->_images)->GetThumbnail($size) : current($this->_images);
				/*
				return file_exists($this->_images[0]->file) ? 
							$this->_images[0]->GetUrl($size) :
							BASEURL . '/images/no-image.png';				
				*/
			}
		}
		
		$id = (int)$this->_featured_image_id;
		$img = new SB_AttachmentImage($id);
		$thumb = null;
		if( $size )
		{
			$thumb = $img->GetThumbnail($size);
		}
		
		return $thumb ? $thumb : $img;
	}
	public function GetAsmComponents()
	{
		$query = "SELECT p.product_id, p.product_code, p.product_name, a2p.qty_required 
					FROM mb_assemblie2product a2p, mb_products p 
					WHERE 1 = 1
					AND a2p.product_id = p.product_id
					AND a2p.assembly_id = $this->product_id";
		$coms = $this->dbh->FetchResults($query);
		for($i = 0; $i < count($coms); $i++)
		{
			$coms[$i]->product = new SB_MBProduct($coms[$i]->product_id);
		}
		return $coms;
	}
	public function __get($var)
	{
		if( $var == 'link' )
		{
			$slug = (empty($this->slug) ? sb_build_slug($this->product_name) : $this->slug);
			$link = SB_Route::_('index.php?mod=emono&view=product&id='.$this->product_id.'&slug='. $slug);
			return $link;
		}
		if( $var == 'price' )
		{
			return sprintf("%s", sb_number_format($this->product_price));
		}
		/*
		if( $var == 'product_name' )
		{
			return html_entity_decode($this->_dbData->product_name);
		}
		*/
		if( $var == 'excerpt' )
		{
			$desc = trim(strip_tags($this->product_description));
			return empty($desc) ? __('There is no description', 'mb') : substr($desc, 0, 128) . '...';
		}
		return parent::__get($var);
	}
	public function GetCategoriesName()
	{
		$cats = array();
		foreach($this->_categories as $c)
		{
			$cats[] = $c->name;
		}
		return implode(',', $cats);
	}
	public function GetCategories()
	{
		if( !$this->_categories )
		{
			$this->GetDbCategories();
		}
		return $this->_categories;
	}
	/**
	 * Get the top category of product
	 * 
	 * @return SB_MBCategory
	 */
	public function GetTopCategory()
	{
		$cat = null;
		foreach($this->GetCategories() as $_cat)
		{
			if( (int)$_cat->parent === 0 )
			{
				$cat = $_cat;
				break;
			}
		}
		return $cat;
	}
	/**
	 * Return formatted price including currency code
	 * @param string $curreny_code 
	 * @return string formatted price
	 */
	public function GetPrice($currency_code = null)
	{
		$_currency_code = '$';
		if( $currency_code )
			$_currency_code = $currency_code;
		elseif( defined('MB_CURRENCY_CODE') )
		{
			$_currency_code = MB_CURRENCY_CODE;
		}
		
		return sprintf("%s %s", empty($_currency_code) ? '$' : $_currency_code, sb_number_format((float)$this->product_price));
	}
	public function GetWarehouseQty($warehouse_id)
	{
		if( !(int)$warehouse_id )
			return 0;
		$query = "select quantity from mb_product_quantity where warehouse_id = $warehouse_id and product_id = $this->product_id";
		return (int)$this->dbh->GetVar($query);
	}
	public function GetWarehouseMinStock($warehouse_id)
	{
		if( !(int)$warehouse_id )
			return 0;
		$query = "select min_stock from mb_product_quantity where warehouse_id = $warehouse_id and product_id = $this->product_id";
		return (int)$this->dbh->GetVar($query);
	}
}
class SB_MBProductImage 
{
	public $id;
	public $file;
	public $ext;
	public $name;
	public $mime;
	public $base_dir;
	public $base_url;
	public $data;
	
	public function __construct($data, $baseurl)
	{
		$this->id		= $data->attachment_id;
		$this->file 	= UPLOADS_DIR . SB_DS . $data->file;
		$this->base_dir = dirname($this->file);
		$this->base_url	= $baseurl;
		$this->ext 		= sb_get_file_extension($this->file);
		$this->name 	= str_replace('.' . $this->ext, '', basename($this->file));	
		$this->mime		= sb_get_file_mime($this->file);
		$this->data		= $data;
	}
	public function GetUrl($size = null)
	{
		$url = '';
		if( $size )
		{
			$filename = "{$this->name}-{$size}.{$this->ext}";
			$file = $this->base_dir . SB_DS . $filename;
			//var_dump($file);
			if( file_exists($file) )
			{
				$url = $this->base_url . '/' . $filename;
			}
			else
			{
				$url = $this->base_url . '/' . basename($this->file);
			}
		}
		else
		{
			$url = $this->base_url . '/' . basename($this->file);
		}
		return $url;
	}
}
