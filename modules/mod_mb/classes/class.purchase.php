<?php
//namespace SinticBolivia\SBFramework\Modules\Mb\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Modules\Provider\Classes\SB_Prov;
use SinticBolivia\SBFramework\Modules\Provider\Classes\SB_Prov as SB_MBSupplier;
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

/**
 * 
 * @author marcelo
 * 
 * @property int		order_id
 * @property string		code
 * @property string		name
 * @property int		store_id
 * @property int		supplier_id
 * @property int		transaction_type_id
 * @property int		sequence
 * @property int		items
 * @property float		subtotal
 * @property float		total_tax
 * @property float		discount
 * @property float		total
 * @property string		details
 * @property string 	status
 * @property int		user_id
 * @property date		order_date
 * @property date		delivery date
 * @property datetime	last_modification_date
 * @property detetime				creation_date
 * @property SB_MBTransactionType	transactionType;
 */
class SB_MBPurchase extends SB_ORMObject
{
	protected $supplier;
	protected $store;
	protected $user;
	protected $orderItems = array();
	protected $transactionType;
	
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_purchase_orders WHERE order_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
		$this->GetDbMeta();
		if( $this->supplier_id )
		{
			$this->supplier = new SB_MBSupplier($this->supplier_id);
		}
		if( $this->store_id )
		{
			$this->store = new SB_MBStore($this->store_id);
		}
		if( $this->warehouse_id )
		{
			$this->warehouse = new SB_MBWarehouse($this->warehouse_id);
		}
		if( $this->user_id )
		{
			$this->user	= new SB_User($this->user_id);
		}
		if( $this->transaction_type_id )
		{
			$this->transactionType = new SB_MBTransactionType($this->transaction_type_id);
		}
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_purchase_order_meta WHERE order_id = $this->order_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = $row->meta_value;
		}
	}
	public function GetItems()
	{
		if( !count($this->orderItems) )
		{
			$query = "SELECT i.*, p.product_code, p.product_name ".
					"FROM mb_purchase_order_items i ".
					"LEFT JOIN mb_products p ON i.product_id = p.product_id " .
					"WHERE 1 = 1 " .
					"AND i.order_id = $this->order_id ".
					"ORDER BY creation_date ASC";
			if( $this->dbh->Query($query) )
			{
				$this->orderItems = $this->dbh->FetchResults();
			}
		}
		return $this->orderItems;
	}
	public function SetItems($items)
	{
		$this->orderItems = $items;
	}
	/**
	 * Update products stock and generate product kardex
	 * 
	 * @param int $user_id 
	 * @return  
	 */
	public function UpdateStock($user_id)
	{
		$kardex = array();
		//##update stock
		foreach($this->GetItems() as $item)
		{
			if( (int)$item->product_id > 0 )
			{
				//##update product quantity and cost
				$this->dbh->Update('mb_products',
						array(
								'product_quantity' 	=> "OP[product_quantity + {$item->quantity}]",
								'product_cost'		=> $item->supply_price
						),
						array('product_id' => $item->product_id)
				);
				SB_Factory::getApplication()->Log($this->dbh->lastQuery);
				$the_product 		= new SB_MBProduct($item->product_id);
				$quantity_balance 	= (int)$the_product->product_quantity;// + (int)$item->quantity;
				SB_Module::do_action('mb_purchase_update_stock_item', $item, $this);
				//##build update product kardex
				$kardex_i = array(
						'product_id'			=> $item->product_id,
						'in_out'				=> 'input',
						'quantity'				=> $item->quantity,
						'quantity_balance'		=> $quantity_balance,
						'unit_price'			=> $item->supply_price,
						'total_amount'			=> $item->quantity * $item->supply_price,
						'monetary_balance'		=> $quantity_balance * $item->supply_price,
						'transaction_type_id'	=> $this->transaction_type_id,
						'author_id'				=> $user_id,
						'transaction_id'		=> $this->order_id,
						'cost'					=> $item->supply_price,
						'creation_date'			=> date('Y-m-d H:i:s')
				);
				if( isset($item->batch_code) )
				{
					$kardex_i['batch_code']	= $item->batch_code;
				}
				$kardex[] = SB_Module::do_action('mb_purchase_build_kardex_item', $kardex_i);
			}
			$this->dbh->Update('mb_purchase_order_items', array('status' => 'complete'), array('item_id' => $item->item_id));
			
		}
		//##insert product kardex
		$this->dbh->InsertBulk('mb_product_kardex', $kardex);
		$this->dbh->Update('mb_purchase_orders', array('status' => 'complete'), array('order_id' => $this->order_id));
		
	}
}