<?php
//namespace SinticBolivia\SBFramework\Modules\Mb\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Database\Classes\SB_DbRow;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;

class SB_MBWarehouse extends SB_DbRow
{
	public function __construct($id = null)
	{
		parent::__construct(SB_DbTable::GetTable('mb_warehouse', 1));
		if( $id )
			$this->GetDbData((int)$id);
	}
	public function GetDbData($id)
	{
		$data = $this->table->GetRow((int)$id, null);
		if( $data )
			$this->Bind($data);
		//print_r($this);
	}
}