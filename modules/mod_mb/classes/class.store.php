<?php
//namespace SinticBolivia\MonoBusiness\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Factory;

class SB_MBStore extends SB_ORMObject
{
	protected $_products = array();
	
	public function __construct($store_id = null)
	{
		parent::__construct();
		if($store_id)
			$this->getDbData($store_id);
	}
	public function GetDbData($store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_stores WHERE store_id = $store_id";
		if( !$dbh->Query($query) )
			return false;
		$this->_dbData = $dbh->FetchRow();
		$this->GetDbMeta();
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_store_meta WHERE store_id = $this->store_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = $row->meta_value;
		}
	}
	public function getProducts()
	{
		if( empty($this->_products) )
		{
			$this->_products = SB_Warehouse::getStoreProducts($this->store_id);
		}
		return $this->_products;
	}
	/**
	 * Start a business year, this method will create a kardex records based on 
	 * closed record from past business year
	 * 
	 * @return  bool
	 */
	public function StartManagement()
	{
		//##get current year
		$year = date('Y');
		//##check if store already has started its business year
		$init = mb_get_store_meta($this->store_id, '_init_'. $year);
		if( $init )
		{
			return false;
		}
		$prods = SB_Warehouse::getStoreProducts($this->store_id, 1, -1);
		foreach($prods as $p)
		{
			//##get product last transaction
			$query = "SELECT * FROM mb_product_kardex 
						WHERE product_id = {$p->product_id} 
						ORDER BY creation_date DESC
						LIMIT 1";
			$last_transaction = $this->dbh->FetchRow($query);
			$data = array(
				'product_id'			=> $p->product_id,
				'in_out'				=> 'init',
				'quantity'				=> 0,
				'quantity_balance'		=> 0,
				'unit_price'			=> 0,
				'total_amount'			=> 0,
				'monetary_balance'		=> 0,
				'transaction_type_id'	=> -1,
				'author_id'				=> $user->user_id,
				'transaction_id'		=> -1,
				'creation_date'			=> date('Y-m-d H:i:s')
			);
			if( !$last_transaction )
			{
				$data['quantity_balance'] 	= $p->product_quantity;
				$data['unit_price']			= $p->product_cost;
				$data['monetary_balance']	= $p->product_quantity * $p->product_cost;
			}
			else
			{
				$data['quantity_balance'] 	= $last_transaction->quantity_balance;
				$data['unit_price']			= $last_transaction->unit_price;
				$data['monetary_balance']	= $last_transaction->quantity_balance * $last_transaction->unit_price;
			}
			
			SB_Module::do_action_ref('mb_before_insert_init_kardex', $data);
			$this->dbh->Insert('mb_product_kardex', $data);
		}
		mb_add_store_meta($this->store_id, '_init_'.$year, time());
	}
	public function CloseManagement()
	{
		$year = date('Y');
		//##check if store already has closed its business year
		$closed = mb_get_store_meta($this->store_id, '_closed_'. $year);
		if( $closed )
		{
			return false;
		}
		$prods = SB_Warehouse::getStoreProducts($this->store_id, 1, -1);
		foreach($prods as $p)
		{
			//##get product last transaction
			$query = "SELECT * FROM mb_product_kardex 
						WHERE product_id = {$p->product_id} 
						ORDER BY creation_date DESC
						LIMIT 1";
			$last_transaction = $this->dbh->FetchRow($query);
			$data = array(
				'product_id'			=> $p->product_id,
				'in_out'				=> 'close',
				'quantity'				=> 0,
				'quantity_balance'		=> 0,
				'unit_price'			=> 0,
				'total_amount'			=> 0,
				'monetary_balance'		=> 0,
				'transaction_type_id'	=> -1,
				'author_id'				=> $user->user_id,
				'transaction_id'		=> -1,
				'creation_date'			=> date('Y-m-d H:i:s')
			);
			if( !$last_transaction )
			{
				$data['quantity_balance'] 	= $p->product_quantity;
				$data['unit_price']			= $p->product_cost;
				$data['monetary_balance']	= $p->product_quantity * $p->product_cost;
			}
			else
			{
				$data['quantity_balance'] 	= $last_transaction->quantity_balance;
				$data['unit_price']			= $last_transaction->unit_price;
				$data['monetary_balance']	= $last_transaction->quantity_balance * $last_transaction->unit_price;
			}
			
			SB_Module::do_action_ref('mb_before_insert_close_kardex', $data);
			$this->dbh->Insert('mb_product_kardex', $data);
		}
		mb_add_store_meta($this->store_id, '_closed_'.$year, time());
		return true;
	}
}