<?php
namespace SinticBolivia\SBFramework\Modules\Mb\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;

/**
 * 
 * @author Sintic Bolivia
 * 
 * @property int tax_id
 * @property string code
 * @property string name
 * @property string creation_date
 */
class SB_MBTax extends SB_ORMObject
{
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_tax_rates WHERE tax_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
}
