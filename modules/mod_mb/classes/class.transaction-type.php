<?php
//namespace SinticBolivia\SBFramework\Modules\Mb\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;

class SB_MBTransactionType extends SB_ORMObject
{
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_transaction_types WHERE transaction_type_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
}