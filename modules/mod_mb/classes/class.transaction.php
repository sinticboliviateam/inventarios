<?php
class SB_WarehouseTransaction extends SB_ORMObject
{
    protected $_items = array();
    public $store;
    
	public function __construct($transaction_code_id = null)
	{
		if( $transaction_code_id )
		{
			$this->getDbData($transaction_code_id);
		}
	}
	public function getDbData($transaction_code_id)
	{
		$query = '';
		if( is_numeric($transaction_code_id) )
			$query = "SELECT * FROM transactions WHERE transaction_id = $transaction_code_id";
		elseif( is_string($transaction_code_id) )
			$query = "SELECT * FROM transactions WHERE transaction_code = '$transaction_code_id'";
		$dbh = SB_Factory::getDbh();
		$dbh->Query($query);
		$this->_dbData = $dbh->FetchRow();
		$this->getDbItems();
		$this->store = new SB_WarehouseStore($this->store_id);
	}
	public function setDbData($data)
	{
		$this->_dbData = $data;
	}
	public function getDbItems($transaction_code_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$transaction_code_id = ($transaction_code_id) ? $transaction_code_id : $this->transaction_code;
		$where = ' WHERE ';
		//get transaction items
		if( is_numeric($transaction_code_id) )
			$where .= "transaction_id = $transaction_code_id";
		else
			$where .= "transaction_code = '$transaction_code_id'";
		$query = "SELECT * FROM transaction_items $where ORDER BY creation_date DESC";
		$dbh->Query($query);
		$this->_items = $dbh->FetchResults();
		
		return $this->_items;
	}
	public function getItems()
	{
		return $this->_items;
	}
	/**
	 * build a new transaction code
	 * 
	 * @return string
	 */
	public static function buildNewCode($prefix = null)
	{
	    $dbh = SB_Factory::getDbh();
	    $exists = true;
	    $code = '';
	    $prefix = ($prefix) ? strtoupper($prefix) . '-' : 'T-';
	    while($exists)
	    {
	        $code = $prefix . parent::buildNewCode();
	        $query = "SELECT transaction_id FROM transactions WHERE transaction_code = '$code'";
	        $res = $dbh->Query($query);
	        if( $res <= 0 )
	            $exists = false;
	    }
		
	    return $code;
	}
	public static function getNextSequence($transaction_type_id)
	{
		$query = "SELECT MAX(sequence) + 1 AS next_sequence FROM transactions";
		$dbh = SB_Factory::getDbh();
		$res = $dbh->Query($query);
		if( !$res )
			return 1;
		
		$row = $dbh->FetchRow();
		return empty($row->next_sequence) ? 1 : $row->next_sequence;
	}
	public static function convertSequenceToString($seq)
	{
		$sequence = '';
		for($i = 0;  $i < 5 - strlen($seq); $i++)
		{
			$sequence .= '0';
		}
		$sequence .= $seq;
		
		return $sequence;
	}
}