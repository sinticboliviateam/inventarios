<?php
//namespace SinticBolivia\SBFramework\Modules\Mb\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;
/**
 * 
 * @author  marcelo
 * @table   mb_transfers
 */
class SB_MBTransfer extends SB_DbEntity
{
    const STATUS_ON_THE_WAY             = 'on_the_way';
    const STATUS_WAITING_STOCK          = 'waiting_stock';
    const STATUS_COMPLETED              = 'complete';
    const STATUS_PARTIAL_RECEIVED       = 'partial_received';
    const STATUS_REVERTED               = 'reverted';
    /**
     * @primaryKey true
     * @var INTEGER
     */
    public   $id;
    /**
     *
     * @var INTEGER 
     */
    public   $user_id;
    /**
     *
     * @var INTEGER
     */
    public   $from_store;
    /**
     *
     * @var INTEGER
     */
    public   $from_warehouse;
    /**
     *
     * @var INTEGER
     */
    public   $to_store;
    /**
     *
     * @var INTEGER
     */
    public   $to_warehouse;
    /**
     *
     * @var INTEGER
     */
    public   $to_sequence;
    /**
     *
     * @var INTEGER
     */
    public   $from_sequence;
    /**
     * @var VARCHAR 
     */
    public   $from_lot;
    /**
     *
     * @var VARCHAR 
     */
    public   $to_lot;
    /**
     *
     * @var TEXT 
     */
    public   $details;
    /**
     * 
     * @var VARCHAR
     */
    public   $status;
    /**
     * 
     * @var INTEGER 
     */
    public   $receiver_id;
    /**
     *
     * @var DATETIME 
     */
    public  $reception_date;
    /**
     *
     * @var DATETIME 
     */
    public   $creation_date;
    /**
     * @class   TransferItem
     * @var     Array 
     */
    public   $items = array();
	
	public function GetDbData($id)
	{
      
		$query = "SELECT * FROM mb_transfers WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
		$this->GetDbItems();
		$this->source_store     = new SB_MBStore($this->from_store);
		$this->target_store     = new SB_MBStore($this->to_store);
		$this->source_warehouse	= new SB_MBWarehouse((int)$this->from_warehouse);
		$this->target_warehouse	= new SB_MBWarehouse((int)$this->to_warehouse);
	}
	public function SetDbData($data)
	{
		$this->_dbData = (object)$data;
	}
	public function GetDbItems()
	{
		$query = "SELECT ti.*, ti.status as item_status, p.* 
					FROM mb_transfer_items ti, mb_products p
					WHERE 1 = 1
					AND ti.product_id = p.product_id
					AND ti.transfer_id = $this->id";
		$this->items = $this->dbh->FetchResults($query);
	}
	public function GetItems()
	{
		if( !count($this->items) )
			$this->GetDbItems();
		
		return $this->items;
	}	
	
	
	
}