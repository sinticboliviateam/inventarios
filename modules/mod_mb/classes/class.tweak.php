<?php
namespace SinticBolivia\MonoBusiness\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
class MB_Tweak extends SB_ORMObject
{
	protected $items = array();
	protected $transaction;
	protected $store;
	
	public function __construct($id)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_tweaks WHERE id = $id LIMIT 1";
		$row = $this->dbh->FetchRow($query);
		if( !$row )
			return false;
		$this->_dbData = $row;
		$this->transaction 	= new SB_MBTransactionType($this->transaction_type_id);
		$this->store		= new SB_MBStore($this->store_id);
		$this->user			= new SB_User($this->user_id);
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbItems()
	{
		$query = "SELECT p.product_name,di.* ".
					"FROM mb_tweaks_items di, mb_products p 
					WHERE 1 = 1
					AND di.product_id = p.product_id
					AND tweak_id = $this->id";
		$this->items = $this->dbh->FetchResults($query);
	}
	public function GetItems()
	{
		if( !$this->items || !count($this->items) || !is_array($this->items) )
			$this->GetDbItems();
		return $this->items;
	}
}