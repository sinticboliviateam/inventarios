<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;

class SB_Warehouse
{
	
	/**
	 * Get all available stores
	 * 
	 */
	public static function getStores()
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_stores ORDER BY store_name ASC";
		$dbh->Query($query);
		$stores = array();
		foreach($dbh->FetchResults() as $s)
		{
			$store = new SB_MBStore();
			$store->SetDbData($s);
			$stores[] = $store;
		} 
		
		return $stores;
	}
	public static function getProviders()
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM suppliers ORDER BY supplier_name ASC";
		$dbh->Query($query);
		
		return $dbh->FetchResults();
	}
	public static function getProvider($provider_id)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM suppliers WHERE supplier_id = $provider_id";
		$dbh->Query($query);
		
		return $dbh->FetchRow();
	}
	/**
	 * Get all available product categories
	 */
	public static function getCategories($store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_categories ";
		if( $store_id )
		{
			$query .= "WHERE store_id = $store_id ";
		}
		$query .= "ORDER BY name ASC";
		$res = $dbh->Query($query);
		$cats = array();
		$i = 0;
		foreach($dbh->FetchResults() as $c)
		{
			$cats[$i] = new SB_MBCategory();
			$cats[$i]->SetDbData($c);
			$i++;
		}
		return $cats;
	}
	/**
	 * Get all available product lines
	 */
	public static function getLines()
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_product_lines ORDER BY line_name ASC";
		$res = $dbh->Query($query);
		
		return $dbh->FetchResults();
	}
	public static function getTypes($store_id = null)
	{
		$query = "SELECT * FROM mb_product_types ";
		if( $store_id )
		{
			$query .= "WHERE store_id = $store_id ";
		}
		$query .= "ORDER BY type ASC";
		
		return SB_Factory::getDbh()->FetchResults($query);
	}
	public static function getStoreProducts($store_id = null, $page = 1, $limit = 25, $with_data = true)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_products ";
		$where = 'WHERE 1 = 1 ';
		$qlimit = '';
		if( $store_id )
		{
			$where .= "AND store_id = $store_id ";
		}
		if( $limit > 0 )
		{
			if( $page == 0 )
				$page = 1;
			
			$offset = ($page == 1) ? 0 : ($page - 1) * $limit;
			
			$qlimit = "LIMIT $offset, $limit ";
		}
		
		$query = "$query $where ";
		$query .= 'ORDER BY product_name ASC ';
		$query .= $qlimit;
		$records = $dbh->FetchResults($query);
		$products = array();
		$i = 0;
		foreach($records as $rec)
		{
			$products[$i] = new SB_MBProduct();
			if( $with_data)
				$products[$i]->SetDbData($rec);
			else
				$products[$i]->_dbData = $rec;
			$i++;
		}
		
		return $products;
	}
	public static function getAllPurchases()
	{
		$wc = SB_Factory::getApplication()->getParameter('warehouse_config');
		$query = "SELECT * FROM transactions WHERE transaction_type_id = $wc->purchase_tt_id ORDER BY creation_date DESC";
		$dbh = SB_Factory::getDbh();
		$res = $dbh->Query($query);
		
		return $dbh->FetchResults();
	}
	/**
	 * Update product kardex
	 * 
	 * @param SB_WarehouseProduct $product
	 * @param array $data
	 */
	public static function updateProductKardex($product, $in_out, $qty, $unit_price, $transaction_type_id)
	{
		$user = SB_SessionFunc::getCurrentUser();
		$dbh = SB_Factory::getDbh();
		$in_out = strtoupper(trim($in_out));
		$kdata = array(
			'product_id' => $product->product_id,
			'in_out' => $in_out,
			'quantity' => $qty,
			'quantity_balance' => ($in_out == 'IN') ? ($product->product_quantity + $qty) : ($product->product_quantity - $qty),
			'unit_price' => number_format($unit_price, 2),
			'total_amount' => $qty * $unit_price,
			'transaction_type_id' => $transaction_type_id,
			'author_id' => $user->user_id,
			'creation_date' => date('Y-m-d H:i:s') 
		);
		
		$dbh->Insert('mb_product_kardex', $kdata);
		
		return true;
	}
	/**
	 * 
	 * @param SB_WarehousePurchase $purchase
	 * @return boolean
	 */
	public static function receivePurchaseOrder($purchase)
	{
		if( $purchase->status != 'waiting_stock' )
			return true;
		$dbh = SB_Factory::getDbh();
		$warehouse_config = SB_Factory::getApplication()->getParameter('warehouse_config');
		$tt_id = $warehouse_config->purchase_tt_id;
		
		foreach($purchase->getItems() as $item)
		{
			$product = new SB_WarehouseProduct($item->object_code);
			if( !$product->product_code )
				continue;
			//update product stock
			$dbh->Update('products', array('product_quantity' => $product->product_quantity + $item->object_quantity, 'product_cost' => $item->object_price), 
							array('product_code' => $product->product_code));
			//update product kardex
			self::updateProductKardex($product, 'IN', $item->object_quantity, $item->object_price, $tt_id);
			$dbh->Update('transaction_items', array('status' => 'received'), array('transaction_item_id' => $item->transaction_item_id));
		}
		//update purchase order status
		$dbh->Update('transactions', array('status' => 'received'), array('transaction_code' => $purchase->transaction_code));
		return true;
	}
	public static function getProductsWithoutCategory()
	{
		$query = "SELECT p.* FROM products p LEFT JOIN product2category p2c ON p.product_id = p2c.product_id WHERE p2c.category_id ISNULL";
		$dbh = SB_Factory::getDbh();
		$res = $dbh->Query($query);
		$prods = array();
		$i = 0;
		foreach($dbh->FetchResults() as $r)
		{
			$prods[$i] = new SB_WarehouseProduct();
			$prods[$i]->setDbData($r);
			$i++;
		}
		
		return $prods;
	}
	public static function getAllWarehouseOutputs($store_id = null)
	{
		$model = SB_Model::getModelInstance('TransactionTypes', 'SB_Warehouse', 'mob_warehouse');
		$ttypes = $model->getAllTransactionTypes('OUT');
		
		$query = "SELECT * FROM ";
	}
	public static function GetProductTypes($store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_product_types WHERE 1 = 1 ";
		if( $store_id )
			$query .= "AND store_id = $store_id ";
		$query .= "ORDER BY type ASC";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
	public static function GetProductsWithNullMinQty($count = false)
	{
		$dbh = SB_Factory::getDbh();
		//##get products without minimal stock assigned
		$query = "SELECT ". ($count ? 'COUNT(product_id)' : '*') ." FROM mb_products WHERE min_stock IS NULL";
		if($count) 
			return $dbh->GetVar($query);
		
		$prods = array();
		foreach($dbh->FetchResults($query) as $p)
		{
			$prod = new SB_MBProduct();
			$prod->SetDbData($p);
			$prods[] = $prod;
		}
		
		return $prods;
	}
	public static function GetProductsWithMinQtyWarning($count = false)
	{
		$dbh = SB_Factory::getDbh();
		//##get products with quantity lower or equal to minimal stock
		$query = "SELECT ". ($count ? 'COUNT(product_id)' : '*') ." from mb_products
					WHERE 1 = 1
					AND product_quantity <= min_stock
					AND product_quantity > 0
					AND min_stock IS NOT NULL";
		if($count) 
			return $dbh->GetVar($query);
		$prods = array();
		foreach($dbh->FetchResults($query) as $p)
		{
			$prod = new SB_MBProduct();
			$prod->SetDbData($p);
			$prods[] = $prod;
		}
		return $prods;
	}
	public static function GetProductsWithZeroQty($count = false)
	{
		$dbh = SB_Factory::getDbh();
		//##get products with quantity lower or equal to cero
		$query = "SELECT ". ($count ? 'COUNT(product_id)' : '*') ." from mb_products
					WHERE 1 = 1
					AND (product_quantity <= 0)
					and min_stock IS NOT NULL";
		if($count) 
			return $dbh->GetVar($query);
		$prods = array();
		foreach($dbh->FetchResults($query) as $p)
		{
			$prod = new SB_MBProduct();
			$prod->SetDbData($p);
			$prods[] = $prod;
		}
		return $prods;
	}
	public static function GetMeasurementUnits()
	{
		$query = "SELECT * FROM mb_unit_measures ORDER BY name";
		
		return SB_Factory::getDbh()->FetchResults($query);
	}
	public static function GetBranches()
	{
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_departments ORDER BY name ASC";
		return $dbh->FetchResults($query);
	}
	public static function GetProductBy($by, $id, $store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		if( $by == 'code' )
		{
			$query = "SELECT * FROM mb_products ";
			$query .= "WHERE product_code = '$id' ";
			if( $store_id )
			{
				$query .= "AND store_id = $store_id ";
			}
			$query .= "LIMIT 1";
			return $dbh->FetchRow($query);
		}
		if( $by == 'barcode' )
		{
			$query = "SELECT * FROM mb_products ";
			$query .= "WHERE product_barcode = '$id'";
			if( $store_id )
			{
				$query .= "AND store_id = $store_id ";
			}
			$query .= "LIMIT 1";
			return $dbh->FetchRow($query);
		}
	}
	public static function GetTransactionTypes($type = null)
	{
		$dbh = SB_Factory::getDbh();
		$dbh->Select('*')->From('mb_transaction_types');
		if( $type )
		{
			$dbh->Where(array('in_out' => $type));
		}
		$dbh->OrderBy('transaction_name', 'asc');
		$dbh->Query(null);
		
		return $dbh->FetchResults();
	} 
	public static function GetBatches()
	{
		$query = "SELECT * FROM mb_batch order by name asc";
		return SB_Factory::getDbh()->FetchResults($query);
	}
	public static function GetWarehouses($store_id = null)
	{
		$dbh = SB_Factory::getDbh();
		$dbh->Select('*')->From('mb_warehouse');
		if( $store_id )
		{
			$dbh->Where(array('store_id' => $store_id));
		}
		$dbh->OrderBy('name', 'asc');
		$dbh->Query(null);
		return $dbh->FetchResults();
	}
	/**
	 * 
	 * @param SB_User $user
	 * @return multitype:SB_MBStore
	 */
	public static function GetUserStores($user)
	{
		$stores = array();
		if( $user->can('mb_see_all_stores') )
		{
			$stores = SB_Warehouse::getStores();
		}
		elseif( $user->_store_id )
		{
			$store = new SB_MBStore($user->_store_id);
			$stores[] = $store;
		}
		return $stores;
	}
}
