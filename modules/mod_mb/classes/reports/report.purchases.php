<?php
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;

class SB_MBReportPurchases extends SB_ORMObject
{
	public 	$tabLabel;
	public	$tabLink;
	
	public function __construct()
	{
		parent::__construct();
		$this->tabLabel = __('Purchases', 'mb');
		$this->tabLink = SB_Route::_('index.php?mod=mb&view=reports.default&report=purchases');
	}
	public function GetDbData($id){}
	public function SetDbData($data){}
	public function GetTabs()
	{
		$tab = SB_Request::getString('tab', 'daily');
		?>
		<div class="navbar navbar-default">
			<ul class="nav nav-tabs">
				<li class="<?php print !$tab || $tab == 'daily' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=purchases&tab=daily'); ?>">
						<?php _e('Daily Purchases', 'mb'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'by_date' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=purchases&tab=by_date')?>">
						<?php _e('Purchases by date', 'mb'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'by_product' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=purchases&tab=by_product')?>">
						<?php _e('Purchases by product', 'mb'); ?>
					</a>
				</li>
				<?php SB_Module::do_action('mb_report_purchases_tabs'); ?>
			</ul>
		</div>
		<?php 	
	}
	public function Form()
	{
		$tab = SB_Request::getString('tab', 'daily');
		$this->GetTabs();	
		$form_method 	= 'Form'.ucfirst($tab);
		
		if( method_exists($this, $form_method) )
		{
			call_user_func(array($this, $form_method));
		}
		SB_Module::do_action('mb_report_sales_form_'.$tab);
		
	}
	public function Build()
	{
		$tab = SB_Request::getString('tab', 'daily');
		$build_method 	= 'Build'.ucfirst($tab);
		if( method_exists($this, $build_method) )
			call_user_func(array($this, 'Build'.ucfirst($tab)));
		SB_Module::do_action('mb_report_sales_build_'.$tab);
	}
	protected function FormDaily()
	{
		$date		= SB_Request::getDate('date', date('Y-m-d'));
		$store_id 	= SB_Request::getInt('store_id');
		
		?>
		<h2 class="no-print"><?php _e('Daily Purchases Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="purchases" />
			<input type="hidden" name="tab" value="daily" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::getStores() as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('Date', 'mb'); ?></label>
						<input type="text" name="date" value="<?php print sb_format_date($date); ?>" placeholder="<?php _e('Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
				
			</div>
		</form>
		<?php 
	}
	protected function BuildDaily()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$date		= SB_Request::getDate('date', date('Y-m-d'));
		$store_id 	= SB_Request::getInt('store_id');
		$store		= new SB_MBStore($store_id);
		
		$query = "SELECT p.*,st.store_name,s.supplier_name
					FROM (mb_purchase_orders p, mb_stores st)
					LEFT JOIN mb_suppliers s ON s.supplier_id = p.supplier_id
					WHERE 1 = 1
					AND p.store_id = st.store_id ";
		
		if( $store_id )
		{
			$query .= "AND p.store_id = $store_id ";
		}
		$query .= "AND DATE(order_date) = '$date' ";
		$query .= "ORDER BY order_date DESC";
		$items = $this->dbh->FetchResults($query);
		$report_title = sprintf(__('%s - %s - %s', 'mb'), $store->store_name, __('Daily Purchases Report', 'mb'), sb_format_date($date));
		if( SB_Request::getInt('print') ) ob_get_clean();
		ob_start();
		?>
		<?php if( SB_Request::getInt('print') ): ?>
		<style type="text/css">
		*{font-family: Helvetica,Verdana;font-size:10px;}
		#tabler{border-collapse: collapse;}
		#tabler th, #tabler td{padding:2px;}
		#tabler td{border:1px solid #000;}
		form#form-build, .no-print{display:none;}
		h2{text-align:center;}
		th{background-color:#337ab7;color:#fff;text-align:center;padding:4px;}
		.text-center{text-align:center;}
		.text-right{text-align:right;}
		</style>
		<?php endif;?>
		<?php if( count($items) ): ?>
		<h2 class="text-center"><?php print $report_title; ?></h2>
		<?php if( !SB_Request::getInt('print') ): ?>
		<div class="hidden-print">
			<form id="" action="" method="get" target="_blank" style="display:inline;">
				<input type="hidden" name="mod" value="mb" />
				<input type="hidden" name="view" value="reports.default" />
				<input type="hidden" name="report" value="purchases" />
				<input type="hidden" name="tab" value="daily" />
				<input type="hidden" name="print" value="1" />
				<input type="hidden" name="build" value="1" />
				<input type="hidden" name="store_id" value="<?php print $store_id; ?>" />
				<input type="hidden" name="date" value="<?php print isset($sales_date) ? sb_format_date($sales_date) : sb_format_date(date('Y-m-d')); ?>" placeholder="<?php _e('Date', 'mb'); ?>" class="form-control datepicker" />
				<button class="btn btn-danger"><span class="glyphicon glyphicon-print"></span> <?php _e('Export PDF', 'mb');?></button>
			</form>
		</div><br/>
		<?php endif; ?>
		<table id="tabler" class="table table-condensed table-hover">
		<thead>
		<tr>
			<th class="col-no"><?php _e('No.'); ?></th>
			<th class="col-customer"><?php _e('Store', 'mb'); ?></th>
			<th class="col-customer"><?php _e('Supplier', 'mb'); ?></th>
			<th class="col-customer"><?php _e('Invoice Num.', 'mb'); ?></th>
			<th class="col-customer"><?php _e('Date', 'mb'); ?></th>
			<th class="col-total"><?php _e('Total Bs.', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; $total_purchases = 0; foreach($items as $item): $total_purchases += $item->total; ?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<td class=""><?php print $item->store_name; ?></td>
			<td class=""><?php print $item->supplier_name; ?></td>
			<td class="text-center"><?php print ''; ?></td>
			<td class="text-center"><?php print sb_format_datetime($item->creation_date); ?></td>
			<td class="text-right"><?php print number_format($item->total, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		<tr>
			<td colspan="5" style="text-align:right;">
				<span style="font-weight:bold;font-size:20px;"><?php _e('Total Purchases:', 'mb'); ?></span>
			</td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;" class="label label-success">
					<?php print number_format($total_purchases, 2, '.', ','); ?>
				</span>
			</td>
		</tr>
		</tbody>
		</table>
		<?php endif; ?>
		<?php
		$report = ob_get_clean();
		if( SB_Request::getInt('print') )
		{
			$pdf = mb_get_pdf_instance('', '', 'dompdf');
			$pdf->loadHtml($report);
			$pdf->render();
			$pdf->stream(sprintf(__('daily-purchases-%d'), $store_id), 
							array('Attachment' => 0, 'Accept-Ranges' => 1));
			die();
		} 
		print $report;
	}
	public function FormBy_date()
	{
		$store_id	= SB_Request::getInt('store_id');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		?>
		<h2 class="hidden-print"><?php _e('Purchases Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="purchases" />
			<input type="hidden" name="tab" value="by_date" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::getStores() as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_date()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$store_id 	= SB_Request::getInt('store_id');
		$date_from 	= SB_Request::getDate('from_date');
		$date_to 	= SB_Request::getDate('to_date');
		$print		= SB_Request::getInt('print');
		
		$store		= new SB_MBStore($store_id);
		$query = "SELECT p.*,st.store_name,s.supplier_name
					FROM (mb_purchase_orders p, mb_stores st)
					LEFT JOIN mb_suppliers s ON s.supplier_id = p.supplier_id
					WHERE 1 = 1
					AND p.store_id = st.store_id ";
		
		if( $store_id )
		{
			$query .= "AND p.store_id = $store_id ";
		}
		$query .= "AND (DATE(order_date) >= '$date_from' AND DATE(order_date) <= '$date_to') ";
		$query .= "ORDER BY order_date DESC";
		
		$items = $this->dbh->FetchResults($query);
		$report_title = sprintf("%s - %s<br/>(%s - %s)", $store->store_name, __('Purchases Report', 'mb'), sb_format_date($date_from), sb_format_date($date_to));
		?>
		<?php if( count($items) ): ?>
		<p class="hidden-print">
			<a href="javascript:print();" class="btn btn-warning">
				<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'mb'); ?>
			</a>
		</p>
		<h2 class="text-center"><?php print $report_title; ?></h2>
		<table class="table">
		<thead>
		<tr>
			<th class="col-no"><?php _e('No.'); ?></th>
			<th class="col-customer"><?php _e('Supplier', 'mb'); ?></th>
			<th class="col-items"><?php _e('Invoice Num.', 'mb'); ?></th>
			<th class="col-total"><?php _e('Total', 'mb'); ?></th>
			<th class="col-customer"><?php _e('Date', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; $total_purchases = 0; foreach($items as $item): $total_purchases += $item->total; ?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<td class=""><?php print $item->supplier_name; ?></td>
			<td class="text-center"><?php print ''; ?></td>
			<td class="text-right"><?php print number_format($item->total, 2, '.', ','); ?></td>
			<td class="text-center"><?php print sb_format_datetime($item->order_date); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		<tr><td colspan="4">&nbsp;</td></tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;"><?php _e('Total Purchases:', 'mb'); ?></span></td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;" class="label label-success">
					<?php print number_format($total_purchases, 2, '.', ','); ?>
				</span>
			</td>
		</tr>
		</tbody>
		</table>
		<?php endif; ?>
		<?php 
	}
	public function FormBy_product()
	{
		$user 		= sb_get_current_user();
		$store_id	= SB_Request::getInt('store_id');
		$by			= SB_Request::getString('by', 'qty_items');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		$stores		= SB_Warehouse::GetUserStores($user);
		?>
		<h2 class="hidden-print"><?php _e('Sales By Product Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="by_product" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="qty_items" <?php print $by == 'qty_items' ? 'checked' : ''; ?> /><?php _e('Quantity Items', 'mb'); ?></label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="amount" <?php print $by == 'amount' ? 'checked' : ''; ?> /><?php _e('Monetary Amounts', 'mb'); ?></label>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_product()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$user		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$from_date	= SB_Request::getDate('from_date');
		$to_date	= SB_Request::getDate('to_date');
		$by			= SB_Request::getString('by', 'qty_items');
		$store = new SB_MBStore($store_id);
		$query = '';
		if( $by == 'qty_items' )
		{
			$query = "SELECT p.product_id,p.product_code,p.product_name, p.product_cost, sum(oi.quantity) as cant, sum(oi.total) as monto
						FROM mb_products p
						LEFT JOIN mb_order_items oi ON oi.product_id = p.product_id
						LEFT JOIN mb_orders o ON o.order_id = oi.order_id
						WHERE 1 = 1 
						AND p.store_id = $store_id 
						AND o.status = 'complete'
						AND (o.order_date >= '$from_date' AND o.order_date <= '$to_date' )
						GROUP BY p.product_id
						ORDER BY cant DESC";
		}
		elseif( $by == 'amount' )
		{
			$query = "SELECT p.product_id,p.product_code,p.product_name, p.product_cost, sum(oi.quantity) as cant, sum(oi.total) as monto
						FROM mb_products p
						LEFT JOIN mb_order_items oi ON oi.product_id = p.product_id
						LEFT JOIN mb_orders o ON o.order_id = oi.order_id
						WHERE 1 = 1
						AND p.store_id = $store_id
						AND o.status = 'complete'
						AND (o.order_date >= '$from_date' AND o.order_date <= '$to_date' )
						GROUP BY p.product_id
						ORDER BY monto DESC";
			/**
			 * select p.product_id, p.store_id, p.product_name, p.product_quantity
				from mb_products p
				where p.product_id NOT IN(select oi.product_id from mb_order_items oi)
				and p.store_id = 2
			 */
		}
		$is_root = $user->IsRoot();
		//var_dump($query);
		$rows = $this->dbh->FetchResults($query);
		?>
		<h2 style="text-align:center;font-size:20px;">
			<span><?php printf(__('Sales by Product - %s', 'mb'), $store->store_name); ?><br/></span>
			<span style="font-size:15px;"><?php printf("%s - %s", sb_format_date($from_date), sb_format_date($to_date)); ?></span>
		</h2>
		<table class="table table-condensed table-hover">
		<thead>
		<tr>
			<th><?php _e('Num.', 'mb'); ?></th>
			<th><?php _e('Product', 'mb'); ?></th>
			<th><?php _e('Unit Cost', 'mb'); ?></th>
			<th><?php _e('Quantity Sold', 'mb'); ?></th>
			<th><?php _e('Total Cost', 'mb'); ?></th>
			<th><?php _e('Total Sold', 'mb'); ?></th>
			<th><?php _e('Utilidad', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($rows as $row): ?>
		<?php
		$total_cost = $row->product_cost * $row->cant; 
		?>
		<tr>
			<td><?php print $i; ?></td>
			<td>
				<?php if($is_root): ?>
				<a href="<?php print SB_Route::_('index.php?mod=mb&view=edit&id='.$row->product_id); ?>" target="_blank">
				<?php endif; ?>
					<?php print $row->product_name; ?>
				<?php if($is_root): ?>
				</a>
				<?php endif; ?>
			</td>
			<td class="text-right"><?php print number_format($row->product_cost, 2, '.', ','); ?></td>
			<td class="text-right"><?php print number_format($row->cant, 2, '.', ','); ?></td>
			<td class="text-right"><?php print number_format($total_cost, 2, '.', ','); ?></td>
			<td class="text-right"><?php print number_format($row->monto, 2, '.', ','); ?></td>
			<td class="text-right"><?php print number_format($row->monto - $total_cost, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		</table>
		<?php 
	}
	public function FormBy_type()
	{
		$user 		= sb_get_current_user();
		$store_id	= SB_Request::getInt('store_id');
		$by			= SB_Request::getString('by', 'qty_items');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		$stores		= SB_Warehouse::GetUserStores($user);
		
		?>
		<h2 class="hidden-print"><?php _e('Report by Sale Type', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="by_type" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="cash" <?php print $by == 'cash' ? 'checked' : ''; ?> /><?php _e('Cash', 'mb'); ?></label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="credit" <?php print $by == 'credit' ? 'checked' : ''; ?> /><?php _e('Credit', 'mb'); ?></label>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_type()
	{
		if( !SB_Request::getInt('build') )
			return false;
		
		$user		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$from_date	= SB_Request::getDate('from_date');
		$to_date	= SB_Request::getDate('to_date');
		$by			= SB_Request::getString('by', 'cash');
		$store 		= new SB_MBStore($store_id);
		$query = '';
		
			$query = "SELECT voi.store_id, 
							fac.meta_value as NomFactura , 
							voi.invoice_number as NumFactura,
							voi.nit_ruc_nif as Nit,
					       	voi.invoice_date_time as FechaFactura,
					       	voi.authorization as NumAutorizacion, 
					       	voi.control_code as CodigoControl,
					       	voi.total as Total
					FROM mb_invoices voi, mb_invoice_meta as voimet, mb_invoice_meta fac
					WHERE 1 = 1
					AND voi.invoice_id = voimet.invoice_id
					and voi.invoice_id = fac.invoice_id
					AND voi.store_id = $store_id
					AND voimet.meta_key = '_sale_type'
					AND voimet.meta_value = '$by'
					and fac.meta_key = '_billing_name'
					AND (date(voi.invoice_date_time) >= '$from_date' AND date(voi.invoice_date_time) <= '$to_date')
					GROUP BY voi.invoice_number
					ORDER BY voi.invoice_number";
		$rows = $this->dbh->FetchResults($query);
		?>
		<table class="table">
		<thead>
		<tr>
			<th><?php _e('Num', 'mb'); ?></th>
			<th><?php _e('Invoice Name', 'mb'); ?></th>
			<th><?php _e('Invoice Num', 'mb'); ?></th>
			<th><?php _e('NIT', 'mb'); ?></th>
			<th><?php _e('Invoice Date', 'mb'); ?></th>
			<th><?php _e('Invoice Authorization', 'mb'); ?></th>
			<th><?php _e('Control Code', 'mb'); ?></th>
			<th><?php _e('Total', 'mb'); ?></th>
		</tr>
		</thead>
		<?php 
		$i = 1;
		$total = 0; 
		foreach($rows as $row): $total += $row->Total; ?>
		<tr>
			<td><?php print $i; ?></td>
			<td><?php print $row->NomFactura; ?></td>
			<td><?php print $row->NumFactura; ?></td>
			<td><?php print $row->Nit; ?></td>
			<td><?php print sb_format_date($row->FechaFactura); ?></td>
			<td><?php print $row->NumAutorizacion; ?></td>
			<td><?php print $row->CodigoControl; ?></td>
			<td style="text-align:right;"><?php print number_format($row->Total, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		<tfoot>
		<tr>
			<td colspan="7" style="text-align:right;"><b><?php _e('Total', 'mb'); ?></b></td>
			<td style="text-align:right;"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
		</tr>
		</tfoot>
		</table>
		<?php 
	}
}
