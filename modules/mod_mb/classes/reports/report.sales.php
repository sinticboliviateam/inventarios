<?php
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SB_MBReportSales extends SB_ORMObject
{
	public 	$tabLabel;
	public	$tabLink;
	
	public function __construct()
	{
		parent::__construct();
		$this->tabLabel = __('Sales', 'mb');
		$this->tabLink = SB_Route::_('index.php?mod=mb&view=reports.default&report=sales');
	}
	public function GetDbData($id){}
	public function SetDbData($data){}
	public function GetTabs()
	{
		$tab = SB_Request::getString('tab', 'daily');
		?>
		<div class="navbar navbar-default">
			<ul class="nav nav-tabs">
				<li class="<?php print !$tab || $tab == 'daily' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=sales&tab=daily'); ?>">
						<?php _e('Daily Sales', 'mb'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'by_date' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=sales&tab=by_date')?>">
						<?php _e('Sales by date', 'mb'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'by_product' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=sales&tab=by_product')?>">
						<?php _e('Sales by product', 'mb'); ?>
					</a>
				</li>
				<li class="<?php print $tab == 'by_type' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=sales&tab=by_type')?>">
						<?php _e('Sales by Type', 'mb'); ?>
					</a>
				</li>
				<?php SB_Module::do_action('mb_report_sales_tabs'); ?>
			</ul>
		</div>
		<?php 	
	}
	public function Form()
	{
		$tab = SB_Request::getString('tab', 'daily');
		$this->GetTabs();	
		$form_method 	= 'Form'.ucfirst($tab);
		
		if( method_exists($this, $form_method) )
		{
			call_user_func(array($this, $form_method));
		}
		SB_Module::do_action('mb_report_sales_form_'.$tab);
		
	}
	public function Build()
	{
		$tab = SB_Request::getString('tab', 'daily');
		$build_method 	= 'Build'.ucfirst($tab);
		if( method_exists($this, $build_method) )
			call_user_func(array($this, 'Build'.ucfirst($tab)));
		SB_Module::do_action('mb_report_sales_build_'.$tab);
	}
	protected function FormDaily()
	{
		$date		= SB_Request::getDate('sales_date', date('Y-m-d'));
		$store_id 	= SB_Request::getInt('store_id');
		
		?>
		<h2 class="no-print"><?php _e('Daily Sales Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="daily" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::getStores() as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('Date', 'mb'); ?></label>
						<input type="text" autocomplete="off" name="sales_date" value="<?php print sb_format_date($date); ?>" placeholder="<?php _e('Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
				
			</div>
		</form>
		<?php 
	}
	protected function BuildDaily()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$date		= SB_Request::getDate('sales_date', date('Y-m-d'));
		$store_id 	= SB_Request::getInt('store_id');
		$store		= new SB_MBStore($store_id);
		/*
		$query = "SELECT o.*, (SELECT CONCAT(c.first_name, ' ', c.last_name) ".
								"FROM mb_customers c ".
								"WHERE c.customer_id = o.customer_id) AS customer, ".
					"i.invoice_number, i.control_code, i.invoice_date_time, im.meta_value as nombre_fac ".
					"FROM mb_orders o, mb_order_meta om, mb_invoices i, mb_invoice_meta im " .
					"WHERE 1 = 1 " .
					"AND o.order_id = om.order_id " . 
					"AND om.meta_key = '_invoice_id' ".
					"AND om.meta_value = i.invoice_id ".
					"AND i.invoice_id = im.invoice_id ".
					"AND im.meta_key = '_billing_name' ";
		$query .= "AND o.status = 'complete' ";
		*/
		$invoicesEnabled = SB_Module::IsEnabled('invoices');
		$invoice_left_query = '';
		$invoice_columns = '';
		if( $invoicesEnabled )
		{
			$invoice_columns = ",om.meta_value as invoice_id
							,i.invoice_number, i.control_code, i.invoice_date_time,
							im.meta_value as nombre_fac";
			$invoice_left_query = "LEFT JOIN mb_order_meta om ON om.order_id = o.order_id AND om.meta_key = '_invoice_id'
					LEFT JOIN mb_invoices i on om.meta_value = i.invoice_id
					LEFT JOIN mb_invoice_meta im ON im.invoice_id = om.meta_value AND im.meta_key = '_billing_name'";
		}
		$query = "SELECT o.*, 
							(SELECT CONCAT(c.first_name, ' ', c.last_name) 
								FROM mb_customers c WHERE c.customer_id = o.customer_id) AS customer
							$invoice_columns
					FROM mb_orders o
					$invoice_left_query
					WHERE 1 = 1 
					AND o.status = 'complete'";
		if( $store_id )
		{
			$query .= "AND o.store_id = $store_id ";
		}
		$query .= "AND DATE(order_date) = '$date' ";
		$query .= "ORDER BY order_id DESC";
		//print '<!-- ' . $query . ' -->';
		$items = $this->dbh->FetchResults($query);
		$report_title = sprintf(__('%s - %s - %s', 'mb'), $store->store_name, __('Daily Sales Report', 'mb'), sb_format_date($date));
		if( SB_Request::getInt('print') ) ob_get_clean();
		ob_start();
		?>
		<?php if( SB_Request::getInt('print') ): ?>
		<style type="text/css">
		*{font-family: Helvetica,Verdana;font-size:10px;}
		#tabler{border-collapse: collapse;}
		#tabler th, #tabler td{padding:2px;}
		#tabler td{border:1px solid #000;}
		form#form-build, .no-print{display:none;}
		h2{text-align:center;}
		th{background-color:#337ab7;color:#fff;text-align:center;padding:4px;}
		.text-center{text-align:center;}
		.text-right{text-align:right;}
		</style>
		<?php endif;?>
		<?php if( count($items) ): ?>
		<h2 class="text-center"><?php print $report_title; ?></h2>
		<?php if( !SB_Request::getInt('print') ): ?>
		<div class="hidden-print">
			<form id="" action="" method="get" target="_blank" style="display:inline;">
				<input type="hidden" name="mod" value="mb" />
				<input type="hidden" name="view" value="reports.default" />
				<input type="hidden" name="report" value="sales" />
				<input type="hidden" name="tab" value="daily" />
				<input type="hidden" name="print" value="1" />
				<input type="hidden" name="build" value="1" />
				<input type="hidden" name="store_id" value="<?php print $store_id; ?>" />
				<input type="hidden" name="sales_date" value="<?php print isset($sales_date) ? sb_format_date($sales_date) : sb_format_date(date('Y-m-d')); ?>" placeholder="<?php _e('Date', 'mb'); ?>" class="form-control datepicker" />
				<button class="btn btn-danger"><span class="glyphicon glyphicon-print"></span> <?php _e('Export PDF', 'mb');?></button>
			</form>
		</div><br/>
		<?php endif; ?>
		<table id="tabler" class="table table-condensed table-hover">
		<thead>
		<tr>
			<th class="col-no text-center"><?php _e('No.'); ?></th>
			<th class="col-customer text-center"><?php _e('Customer', 'mb'); ?></th>
			<?php if( $invoicesEnabled ): ?>
			<th class="col-customer text-center"><?php _e('Invoice Name', 'mb'); ?></th>
			<th class="col-customer text-center"><?php _e('Invoice Num.', 'mb'); ?></th>
			<th class="col-customer text-center"><?php _e('Control Code', 'mb'); ?></th>
			<?php endif; ?>
			<th class="col-customer text-center"><?php _e('Date', 'mb'); ?></th>
			<th class="col-items text-center"><?php _e('Items', 'mb'); ?></th>
			<th class="col-total text-center"><?php _e('Total', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; $total_sales = 0; foreach($items as $item): $total_sales += $item->total; ?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<td class=""><?php print $item->customer; ?></td>
			<?php if( $invoicesEnabled ): ?>
			<td class=""><?php print $item->nombre_fac; ?></td>
			<td class="text-center"><?php print $item->invoice_number; ?></td>
			<td class="text-center"><?php print $item->control_code; ?></td>
			<?php endif; ?>
			<td class="text-center"><?php print sb_format_date($invoicesEnabled ? $item->invoice_date_time : $item->order_date); ?></td>
			<td class="text-center"><?php print (int)$item->items; ?></td>
			<td class="text-right"><?php print number_format($item->total, 2); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		<tr>
			<td colspan="<?php print $invoicesEnabled ? 7 : 4; ?>" style="text-align:right;">
				<span style="font-weight:bold;font-size:20px;"><?php _e('Total Sales:', 'mb'); ?></span>
			</td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;" class="label label-success"><?php print number_format($total_sales, 2); ?></span>
			</td>
		</tr>
		</tbody>
		</table>
		<?php endif; ?>
		<?php
		$report = ob_get_clean();
		if( SB_Request::getInt('print') )
		{
			$pdf = mb_get_pdf_instance('', '', 'dompdf');
			$pdf->loadHtml($report);
			$pdf->render();
			$pdf->stream(sprintf(__('daily-sales-%d.pdf'), $store_id), 
							array('Attachment' => 0, 'Accept-Ranges' => 1));
			die();
		} 
		print $report;
	}
	public function FormBy_date()
	{
		$store_id	= SB_Request::getInt('store_id');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		?>
		<h2 class="hidden-print"><?php _e('Sales Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="by_date" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::getStores() as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" autocomplete="off" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" autocomplete="off" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Show Detail', 'mb'); ?></label>
						<input type="checkbox" name="show_detail" value="1" />
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_date()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$store_id 		= SB_Request::getInt('store_id');
		$date_from 		= SB_Request::getDate('from_date');
		$date_to 		= SB_Request::getDate('to_date');
		$show_detail	= SB_Request::getInt('show_detail');
		$order			= SB_Request::getString('order', 'ASC');
		$invoiceEnabled = SB_Module::IsEnabled('invoices');
		$export			= SB_Request::getString('export');
		
		$store			= new SB_MBStore($store_id);
		$subquery1 	= "SELECT CONCAT(c.first_name, ' ', c.last_name) FROM mb_customers c WHERE c.customer_id = o.customer_id";
		$query = "SELECT o.*, ($subquery1) AS customer ".
					"FROM mb_orders o WHERE 1 = 1 ";
		$query .= "AND status = 'complete' ";
		if( $store_id )
		{
			$query .= "AND store_id = $store_id ";
		}
		$query .= "AND (DATE(order_date) >= '$date_from' AND DATE(order_date) <= '$date_to') ";
		$query .= "ORDER BY o.order_id $order";
		$items = $this->dbh->FetchResults($query);
		$report_title = sprintf("%s - %s<br/>(%s - %s)", $store->store_name, __('Sales Report', 'mb'), sb_format_date($date_from), sb_format_date($date_to));
		if( $export == 'excel')
		{
			$this->ByDate2Excel($report_title, $items, $show_detail);
		}
		?>
		<?php if( count($items) ): ?>
		<p class="hidden-print">
			<a href="javascript:print();" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'mb'); ?></a>
			<a href="<?php print $_SERVER['REQUEST_URI']; ?>&export=excel" class="btn btn-success"><?php _e('Export to Excel', 'mb'); ?></a>
		</p>
		<h2 class="text-center"><?php print $report_title; ?></h2>
		<table class="table table-condensed">
		<thead>
		<tr>
			<th class="col-no"><?php _e('No.'); ?></th>
			<th class="col-customer"><?php _e('Date', 'mb'); ?></th>
			<th class="col-customer"><?php _e('Customer', 'mb'); ?></th>
			<th class="col-items"><?php _e('Items', 'mb'); ?></th>
			<th class="col-items"><?php _e('Invoice Num.', 'mb'); ?></th>
			<th class="col-total"><?php _e('Total', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; $total_sales = 0; foreach($items as $item): $total_sales += $item->total; $invoice_id = mb_get_order_meta($item->order_id, '_invoice_id'); ?>
		<tr class="main-row <?php print $show_detail ? 'bg-primary': ''; ?>">
			<td class="text-center"><?php print $i; ?></td>
			<td class="text-center"><?php print sb_format_datetime($item->order_date); ?></td>
			<td class=""><?php print $item->customer; ?></td>
			<td class="text-center"><?php print $item->items; ?></td>
			<td class="text-center">
				<?php print $invoiceEnabled && $invoice_id ? 
							$this->dbh->GetVar("SELECT invoice_number FROM mb_invoices WHERE invoice_id = $invoice_id LIMIT 1") 
							: 
							$item->details; ?>
			</td>
			<td class="text-right"><?php print sb_number_format($item->total); ?></td>
		</tr>
			<?php if( $show_detail ): ?>
			<?php 
			$query = "SELECT oi.*,p.product_code FROM mb_order_items oi JOIN mb_products p ON p.product_id = oi.product_id WHERE oi.order_id = $item->order_id ORDER BY oi.item_id ASC";
			$orderItems = $this->dbh->FetchResults($query);
			$ic = 1;
			foreach($orderItems as $oi): ?>
			<tr>
				<td><?php print $ic; ?></td>
				<td><?php print $oi->product_code; ?></td>
				<td><?php print $oi->name; ?></td>
				<td><?php print $oi->quantity; ?></td>
				<td><?php print ''; ?></td>
				<td class="text-right"><?php print sb_number_format($oi->total); ?></td>
			</tr>
			<?php $ic++; endforeach; ?>
			<?php endif; ?>
		<?php $i++; endforeach; ?>
		<tr><td colspan="4">&nbsp;</td></tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;"><?php _e('Total Sales:', 'mb'); ?></span></td>
			<td class="text-right">
				<span style="font-weight:bold;font-size:20px;" class="label label-success"><?php print number_format($total_sales, 2); ?></span>
			</td>
		</tr>
		</tbody>
		</table>
		<?php endif; ?>
		<?php 
	}
	protected function ByDate2Excel($title, $items, $showDetail)
	{
		ob_clean();
		ob_get_clean();
		ob_flush();
		$excelFilename = __('sales-report.xlsx', 'mb');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
		$fontStyleWhite = [
				'bold' 	=> true,
				'color'	=> ['rgb' => 'FFFFFF'],
				'size'	=> 9
		];
		$fontStyleBlack = [
				'bold' 	=> false,
				'color'	=> ['rgb' => '000000'],
				'size'	=> 9
		];
		$mainStyle = array(
				'font' => $fontStyleWhite,
				'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '1E90FF')
				)
		);
		$xls = new PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		$rowHeight = 30;
		$nameWidth = 50;
		$row = 1;
		$sheet->setCellValue("A$row", __('Num', 'mb'));
		$sheet->setCellValue("B$row", __('Date', 'mb'));
		$sheet->setCellValue("C$row", __('Customer', 'mb'));
		$sheet->setCellValue("D$row", __('Items', 'mb'));
		$sheet->setCellValue("E$row", __('Invoice Num.', 'mb'));
		$sheet->setCellValue("F$row", __('Total', 'mb'));
		$sheet->getStyle("A$row:F$row")->getAlignment()->setWrapText(true);
		$sheet->getStyle("A$row:F$row")->applyFromArray($mainStyle);
		$sheet->getRowDimension($row)->setRowHeight($rowHeight);
		$sheet->getColumnDimension("C")->setWidth($nameWidth);
		$row++;
		$i = 1;
		$total_sales = 0;
		foreach($items as $item)
		{
			$sheet->setCellValue("A$row", $i);
			$sheet->setCellValue("B$row", sb_format_datetime($item->order_date));
			$sheet->setCellValue("C$row", $item->customer);
			$sheet->setCellValue("D$row", (int)$item->items);
			$sheet->setCellValue("E$row", $item->details);
			$sheet->setCellValue("F$row", sb_number_format($item->total));
			$sheet->getStyle("A$row:F$row")->getAlignment()->setWrapText(true);
			$sheet->getStyle("A$row:F$row")->applyFromArray(['font' => $fontStyleBlack]);
			$sheet->getRowDimension($row)->setRowHeight($rowHeight);
			
			$total_sales += $item->total;
			$row++;
			$i++;
			if( $showDetail )
			{
				$prevRow = $row - 1;
				$sheet->getStyle("A$prevRow:F$prevRow")->applyFromArray($mainStyle);
				$query = "SELECT oi.*,p.product_code FROM mb_order_items oi JOIN mb_products p ON p.product_id = oi.product_id WHERE oi.order_id = $item->order_id ORDER BY oi.item_id ASC";
				$orderItems = $this->dbh->FetchResults($query);
				$ic = 1;
				foreach($orderItems as $oi)
				{
					$sheet->setCellValue("A$row", $ic);
					$sheet->setCellValue("B$row", $oi->product_code);
					$sheet->setCellValue("C$row", $oi->name);
					$sheet->setCellValue("D$row", $oi->quantity);
					$sheet->setCellValue("E$row", '');
					$sheet->setCellValue("F$row", sb_number_format($oi->total));
					$sheet->getStyle("A$row:F$row")->getAlignment()->setWrapText(true);
					$sheet->getStyle("A$row:F$row")->applyFromArray(['font' => $fontStyleBlack]);
					$sheet->getRowDimension($row)->setRowHeight($rowHeight);
					$row++;
					$ic++;
				}
			}
		}
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$excelFilename.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}
	public function FormBy_product()
	{
		$user 		= sb_get_current_user();
		$store_id	= SB_Request::getInt('store_id');
		$by			= SB_Request::getString('by', 'qty_items');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		$stores		= SB_Warehouse::GetUserStores($user);
		?>
		<h2 class="hidden-print"><?php _e('Sales By Product Report', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="by_product" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="qty_items" <?php print $by == 'qty_items' ? 'checked' : ''; ?> /><?php _e('Quantity Items', 'mb'); ?></label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="amount" <?php print $by == 'amount' ? 'checked' : ''; ?> /><?php _e('Monetary Amounts', 'mb'); ?></label>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_product()
	{
		if( !SB_Request::getInt('build') )
			return false;
		$user		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$from_date	= SB_Request::getDate('from_date');
		$to_date	= SB_Request::getDate('to_date');
		$by			= SB_Request::getString('by', 'qty_items');
		$store = new SB_MBStore($store_id);
		$query = '';
		if( $by == 'qty_items' )
		{
			$query = "SELECT p.product_id,p.product_code,p.product_name, p.product_cost, sum(oi.quantity) as cant, sum(oi.total) as monto
						FROM mb_products p
						LEFT JOIN mb_order_items oi ON oi.product_id = p.product_id
						LEFT JOIN mb_orders o ON o.order_id = oi.order_id
						WHERE 1 = 1 
						AND p.store_id = $store_id 
						AND o.status = 'complete'
						AND (o.order_date >= '$from_date' AND o.order_date <= '$to_date' )
						GROUP BY p.product_id
						ORDER BY cant DESC";
		}
		elseif( $by == 'amount' )
		{
			$query = "SELECT p.product_id,p.product_code,p.product_name, p.product_cost, sum(oi.quantity) as cant, sum(oi.total) as monto
						FROM mb_products p
						LEFT JOIN mb_order_items oi ON oi.product_id = p.product_id
						LEFT JOIN mb_orders o ON o.order_id = oi.order_id
						WHERE 1 = 1
						AND p.store_id = $store_id
						AND o.status = 'complete'
						AND (o.order_date >= '$from_date' AND o.order_date <= '$to_date' )
						GROUP BY p.product_id
						ORDER BY monto DESC";
			/**
			 * select p.product_id, p.store_id, p.product_name, p.product_quantity
				from mb_products p
				where p.product_id NOT IN(select oi.product_id from mb_order_items oi)
				and p.store_id = 2
			 */
		}
		$is_root = $user->IsRoot();
		//var_dump($query);
		$rows = $this->dbh->FetchResults($query);
		$overallCost 	= 0;
		$overallQty		= 0;
		$overallSold	= 0;
		$overallRevenue	= 0;
		$viewCost		= $user->can('mb_can_see_cost');
		?>
		<h2 style="text-align:center;font-size:20px;">
			<span><?php printf(__('Sales by Product - %s', 'mb'), $store->store_name); ?><br/></span>
			<span style="font-size:15px;"><?php printf("%s - %s", sb_format_date($from_date), sb_format_date($to_date)); ?></span>
		</h2>
		<table class="table table-condensed table-hover">
		<thead>
		<tr>
			<th><?php _e('Num.', 'mb'); ?></th>
			<th><?php _e('Product', 'mb'); ?></th>
			<th><?php _e('Unit Cost', 'mb'); ?></th>
			<th><?php _e('Quantity Sold', 'mb'); ?></th>
			<th><?php _e('Total Cost', 'mb'); ?></th>
			<th><?php _e('Total Sold', 'mb'); ?></th>
			<th><?php _e('Utilidad', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($rows as $row): ?>
		<?php
		$total_cost = $row->product_cost * $row->cant; 
		$revenue	= $row->monto - $total_cost;
		$overallCost	+= $total_cost;
		$overallRevenue += $revenue; 
		$overallSold	+= $row->monto;
		?>
		<tr>
			<td><?php print $i; ?></td>
			<td>
				<?php if($is_root): ?>
				<a href="<?php print SB_Route::_('index.php?mod=mb&view=edit&id='.$row->product_id); ?>" target="_blank">
				<?php endif; ?>
					<?php print $row->product_name; ?>
				<?php if($is_root): ?>
				</a>
				<?php endif; ?>
			</td>
			<td class="text-right"><?php print $viewCost ? number_format($row->product_cost, 2, '.', ',') : 0; ?></td>
			<td class="text-right"><?php print number_format($row->cant, 2, '.', ','); ?></td>
			<td class="text-right"><?php print $viewCost ? number_format($total_cost, 2, '.', ',') : 0; ?></td>
			<td class="text-right"><?php print number_format($row->monto, 2, '.', ','); ?></td>
			<td class="text-right"><?php print number_format($revenue, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		<tfoot>
		<tr>
			<th colspan="2"><div class="text-right"><?php _e('Totals', 'mb'); ?></div></th>
			<th></th>
			<th></th>
			<th><div class="text-right"><?php print sb_number_format($overallCost); ?></div></th>
			<th><div class="text-right"><?php print sb_number_format($overallSold); ?></div></th>
			<th><div class="text-right"><?php print sb_number_format($overallRevenue); ?></div></th>
		</tr>
		</tfoot>
		</table>
		<?php 
	}
	public function FormBy_type()
	{
		$user 		= sb_get_current_user();
		$store_id	= SB_Request::getInt('store_id');
		$by			= SB_Request::getString('by', 'qty_items');
		$from_date 	= SB_Request::getString('from_date', date('Y-m-d', mktime(null, null, null, date('m'), date('d') - 5, date('Y'))));
		$to_date 	= SB_Request::getString('to_date', date('Y-m-d', time()));
		$stores		= SB_Warehouse::GetUserStores($user);
		
		?>
		<h2 class="hidden-print"><?php _e('Report by Sale Type', 'mb'); ?></h2>
		<form id="form-build" action="" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="sales" />
			<input type="hidden" name="tab" value="by_type" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print sb_format_date($from_date); ?>" placeholder="<?php _e('From Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print sb_format_date($to_date); ?>" placeholder="<?php _e('To Date', 'mb'); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button class="btn btn-primary"><?php _e('Build', 'mb');?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="cash" <?php print $by == 'cash' ? 'checked' : ''; ?> /><?php _e('Cash', 'mb'); ?></label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><input type="radio" name="by" value="credit" <?php print $by == 'credit' ? 'checked' : ''; ?> /><?php _e('Credit', 'mb'); ?></label>
					</div>
				</div>
			</div>
		</form>
		<?php 
	}
	public function BuildBy_type()
	{
		if( !SB_Request::getInt('build') )
			return false;
		
		$user		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$from_date	= SB_Request::getDate('from_date');
		$to_date	= SB_Request::getDate('to_date');
		$by			= SB_Request::getString('by', 'cash');
		$store 		= new SB_MBStore($store_id);
		$query = '';
		
			$query = "SELECT voi.store_id, 
							fac.meta_value as NomFactura , 
							voi.invoice_number as NumFactura,
							voi.nit_ruc_nif as Nit,
					       	voi.invoice_date_time as FechaFactura,
					       	voi.authorization as NumAutorizacion, 
					       	voi.control_code as CodigoControl,
					       	voi.total as Total
					FROM mb_invoices voi, mb_invoice_meta as voimet, mb_invoice_meta fac
					WHERE 1 = 1
					AND voi.invoice_id = voimet.invoice_id
					and voi.invoice_id = fac.invoice_id
					AND voi.store_id = $store_id
					AND voimet.meta_key = '_sale_type'
					AND voimet.meta_value = '$by'
					and fac.meta_key = '_billing_name'
					AND (date(voi.invoice_date_time) >= '$from_date' AND date(voi.invoice_date_time) <= '$to_date')
					GROUP BY voi.invoice_number
					ORDER BY voi.invoice_number";
		$rows = $this->dbh->FetchResults($query);
		?>
		<table class="table">
		<thead>
		<tr>
			<th><?php _e('Num', 'mb'); ?></th>
			<th><?php _e('Invoice Name', 'mb'); ?></th>
			<th><?php _e('Invoice Num', 'mb'); ?></th>
			<th><?php _e('NIT', 'mb'); ?></th>
			<th><?php _e('Invoice Date', 'mb'); ?></th>
			<th><?php _e('Invoice Authorization', 'mb'); ?></th>
			<th><?php _e('Control Code', 'mb'); ?></th>
			<th><?php _e('Total', 'mb'); ?></th>
		</tr>
		</thead>
		<?php 
		$i = 1;
		$total = 0; 
		foreach($rows as $row): $total += $row->Total; ?>
		<tr>
			<td><?php print $i; ?></td>
			<td><?php print $row->NomFactura; ?></td>
			<td><?php print $row->NumFactura; ?></td>
			<td><?php print $row->Nit; ?></td>
			<td><?php print sb_format_date($row->FechaFactura); ?></td>
			<td><?php print $row->NumAutorizacion; ?></td>
			<td><?php print $row->CodigoControl; ?></td>
			<td style="text-align:right;"><?php print number_format($row->Total, 2, '.', ','); ?></td>
		</tr>
		<?php $i++; endforeach; ?>
		<tfoot>
		<tr>
			<td colspan="7" style="text-align:right;"><b><?php _e('Total', 'mb'); ?></b></td>
			<td style="text-align:right;"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
		</tr>
		</tfoot>
		</table>
		<?php 
	}
}
