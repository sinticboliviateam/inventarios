<?php
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Request;

class SB_MBReportStock extends SB_ORMObject
{
	public		$key = 'stock';
	public		$tabLabel;
	public 		$tabLink;
	protected	$viewCost = true;
	protected	$user;
	
	public function __construct()
	{
		parent::__construct();
		$this->tabLabel	= __('Inventory', 'mb');
		$this->tabLink	= SB_Route::_('index.php?mod=mb&view=reports.default&report=stock');
		$this->user 	= sb_get_current_user();
		$this->viewCost = $this->user->can('mb_can_see_cost');
	}
	public function GetDbData($id){}
	public function SetDbData($data){}
	public function GetTabs()
	{
		?>
		<div class="navbar navbar-default">
			<ul class="nav nav-tabs">
				<li class="<?php print !SB_Request::getString('tab') ? 'active' : ''; ?>">
					<a href="<?php print $this->tabLink; ?>"><?php print $this->tabLabel; ?></a>
				</li>
				<li class="<?php print SB_Request::getString('tab') == 'kardex' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=stock&tab=kardex'); ?>">
						<?php _e('Kardex'); ; ?>
					</a>
				</li>
				<!--
				<li class="<?php print SB_Request::getString('tab') == 'kardexglobal' ? 'active' : ''; ?>">
					<a href="<?php print SB_Route::_('index.php?mod=mb&view=reports.default&report=stock&tab=kardexglobal'); ?>">
						<?php _e('Kardex Consolidated Annual', 'mb'); ; ?>
					</a>
				</li>
				-->
				<?php SB_Module::do_action('mb_report_stock_tabs'); ?>
			</ul>
		</div>
		<?php 
	}
	public function Form()
	{
		$user		= sb_get_current_user();
		$tab 		= SB_Request::getString('tab', 'default');
		$tab		= strtolower(preg_replace('/[^a-zA-Z0-9_]/', '', $tab));
		$this->GetTabs();
		$form_method 	= 'Form'. ucfirst($tab);
		if( method_exists($this, $form_method) )
		{
			call_user_func(array($this, $form_method));
		}
		SB_Module::do_action('mb_report_stock_form_'.$tab);
	}
	public function FormDefault()
	{
		$user		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$valued		= SB_Request::getInt('valued');
		?>
		<form action="index.php" method="get" class="hidden-print">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="stock" />
			<input type="hidden" name="build" value="1" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::GetUserStores($user) as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button type="submit" class="btn btn-primary"><?php _e('Build', 'mb'); ?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<?php if( $this->viewCost ): ?>
				<div class="col-md-2">
					<div class="form-group">
						<label>
							<input type="checkbox" name="valued" value="1" <?php print $valued ? 'checked' : ''; ?> />
							<?php _e('Valued Report', 'mb'); ?>
						</label>
					</div>
				</div>
				<?php endif; ?>
				<?php SB_Module::do_action('mb_report_stock_options'); ?>
			</div>
		</form>
		<?php 
	}
	public function Build()
	{
		$tab = SB_Request::getString('tab', 'default');
		$build_method 	= 'Build'.ucfirst($tab);
		$callback = SB_Module::do_action('mb_report_build_callback', 
											array($this, 'Build'.ucfirst($tab)),
											$this->key, 
											$tab
		);
		//print_r($callback);die();
		if( is_callable($callback) )
			call_user_func($callback);
		SB_Module::do_action('mb_report_'.$this->key.'_build_'.$tab);
		
	}
	public function BuildDefault()
	{
		$build = SB_Request::getInt('build');
		if( !$build )
			return;
		$store_id	= SB_Request::getInt('store_id');
		$valued		= SB_Request::getInt('valued');
		$store		= null;
		$products 	= array();
		if( !$store_id )
		{
			$title 			= $valued ? __('Global Valued Stock Report', 'mb') : __('Global Stock Report', 'mb');
			$query = "SELECT product_code, 
							product_name, 
							pm.meta_value presentacion,
							pc.meta_value concentracion,
							SUM(product_quantity) product_quantity, 
							product_cost, 
							SUM(product_quantity) * product_cost,
							(select name from mb_warehouses w where w.id = p.warehouse_id) as warehouse
							
						FROM mb_products p, mb_product_meta pm,mb_product_meta pc
						WHERE 1 = 1
						AND pm.product_id = p.product_id
						and pc.product_id = p.product_id
						AND pm.meta_key = '_presentacion'
						and pc.meta_key = '_concentracion'
						GROUP BY product_code
						ORDER BY product_name ASC";
			
			$products		= $this->dbh->FetchResults($query);
		}
		else 
		{
			$store 			= new SB_MBStore($store_id);
			$title 			= $valued ? sprintf(__('Valued Stock Report - %s', 'mb'), $store->store_name) : sprintf(__('Stock Report - %s', 'mb'), $store->store_name);
			$products		= SB_Warehouse::getStoreProducts($store_id, -1, -1);
		}
		
		$total_qty 		= 0;
		$total_items 	= 0;
		$total_cost		= 0;
		?>
		<p class="hidden-print">
			<a href="javascript:;" onclick="print();" class="btn btn-warning">
				<span class="glyphicon glyphicon-print"></span> <?php print _e('Print', 'mb'); ?>
			</a>
		</p>
		<h2 class="text-center"><?php print $title; ?></h2>
		<table class="table table-condensed table-bordered">
		<thead>
		<tr>
			<th class="text-center"><?php _e('No.', 'mb'); ?></th>
			<th class="text-center"><?php _e('Code', 'mb'); ?></th>
			<th class="text-center"><?php _e('Product', 'mb'); ?></th>
			<th class="text-center"><?php _e('Warehouse', 'mb'); ?></th>
			<?php if( isset($products[0]->presentacion) ): ?>
			<th class="text-center"><?php _e('Presentacion', 'mb'); ?></th>
			<?php endif; ?>
			<?php if( isset($products[0]->concentracion) ): ?>
			<th class="text-center"><?php _e('Concentracion', 'mb'); ?></th>
			<?php endif; ?>
			<th class="text-center"><?php _e('Qty', 'mb'); ?></th>
			<?php if( $valued ): ?>
			<th class="text-center"><?php _e('Cost', 'mb'); ?></th>
			<th class="text-center"><?php _e('Total', 'mb'); ?></th>
			<?php endif; ?>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($products as $p): ?>
		<?php
		$total_cost += $p->product_cost * $p->product_quantity; 
		?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<td><?php print $p->product_code; ?></td>
			<td>
				<?php 
				print str_replace(array(@$p->presentacion, @$p->concentracion, '()', ''), '', $p->product_name); 
				?>
			</td>
			<td>
				<?php print $p->warehouse->name; ?>
			</td>
			<?php if( isset($p->presentacion) ): ?>
			<td class="text-center"><?php print $p->presentacion ?></td>
			<?php endif; ?>
			<?php if( isset($p->concentracion) ): ?>
			<td class="text-center"><?php print $p->concentracion; ?></td>
			<?php endif; ?>
			<td class="text-right"><?php print $p->product_quantity; ?></td>
			<?php if( $valued ): ?>
			<td class="text-right">
				<?php print $this->viewCost ? number_format($p->product_cost, 2, '.', ',') : 0; ?>
			</td>
			<td class="text-right">
				<?php print $this->viewCost ? number_format($p->product_cost * $p->product_quantity, 2, '.', ',') : 0; ?>
			</td>
			<?php endif; ?>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		<?php if( $valued ): ?>
		<tfoot>
		<tr>
			<td colspan="<?php print $valued ? 5 : 3; ?>" style="text-align:right;font-weight:bold;font-size:20px;"><?php _e('Total', 'mb'); ?></td>
			<td style="text-align:right;">
				<span style="font-weight:bold;font-size:20px;" class="label label-success"><?php print number_format($total_cost, 2, '.', ','); ?></span>
			</td>
		</tr>
		</tfoot>
		<?php endif; ?>
		</table>
		<?php 
	}
	public function FormKardex()
	{
		$store_id 	= SB_Request::getInt('store_id');
		$user		= sb_get_current_user();
		$tt_id		= SB_Request::getInt('tt_id');
		$code		= SB_Request::getString('code');
		?>
		<form action="index.php" method="get" class="hidden-print form-group-sm">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="view" value="reports.default" />
			<input type="hidden" name="report" value="stock" />
			<input type="hidden" name="tab" value="kardex" />
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="store_id" class="form-control">
							<option><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::GetUserStores($user) as $store): ?>
							<option value="<?php print $store->store_id; ?>" <?php print $store_id == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label><?php _e('Transaction Type', 'mb'); ?></label>
						<select name="tt_id" class="form-control">
							<option><?php _e('-- transaction --', 'mb'); ?></option>
							<?php foreach(SB_Warehouse::GetTransactionTypes() as $type): ?>
							<option value="<?php print $type->transaction_type_id; ?>" <?php print $tt_id == $type->transaction_type_id ? 'selected' : ''; ?>>
								<?php print $type->transaction_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<label><?php _e('Product Code', 'mb'); ?></label>
					<input type="text" name="code" value="<?php print $code; ?>" class="form-control" />
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label>&nbsp;</label><br/>
						<button type="submit" class="btn btn-primary"><?php _e('Build', 'mb'); ?></button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('From Date', 'mb'); ?></label>
						<input type="text" name="from_date" value="<?php print SB_Request::getString('from_date', sb_format_date(mktime(null, null, null, date('m') - 1))); ?>" class="form-control datepicker" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label><?php _e('To Date', 'mb'); ?></label>
						<input type="text" name="to_date" value="<?php print SB_Request::getString('to_date', sb_format_date(time())); ?>" class="form-control datepicker" />
					</div>
				</div>
			</div>
		</form><br/>
		<div class="form-group">
			<span style="padding:5px;margin:0 8px 0 0;line-height:15px;"><span class="bg-primary" style="width:15px;height:15px;display:inline-block;">&nbsp;</span>Recibo</span>
			<span style="padding:5px;margin:0 8px 0 0;line-height:15px;"><span class="bg-warning" style="width:15px;height:15px;display:inline-block;">&nbsp;</span>Cantidades</span>
			<span style="padding:5px;margin:0 8px 0 0;line-height:15px;"><span class="bg-success" style="width:15px;height:15px;display:inline-block;">&nbsp;</span>Valorado</span>
		</div>
		<br/>
		<?php 
	}
	public function BuildKardex()
	{
		$store_id 		= SB_Request::getInt('store_id');
		$product_code 	= SB_Request::getString('code');
		$tt_id			= SB_Request::getInt('tt_id');
		$from_date		= SB_Request::getDate('from_date');
		$to_date		= SB_Request::getDate('to_date');
		$pdf			= SB_Request::getInt('pdf');
		$excel			= SB_Request::getInt('excel');
		if( !$store_id )
			return false;
		
		$results = array();
		$query = '';
		if( $product_code )
		{
			$query = "SELECT p.product_id,p.product_code,p.store_id,p.product_name,tt.transaction_name,
							k.in_out,k.quantity as quantity_in, k.quantity as quantity_out,
							k.quantity_balance,k.unit_price,k.total_amount,k.monetary_balance,
							k.transaction_type_id,k.transaction_id,k.creation_date,
							(k.unit_price * k.quantity) as valued_input,
							(k.unit_price * k.quantity) as valued_output,
							(k.unit_price * k.quantity_balance) as valued_balance,
							o.details as o_details,po.details as po_details
							FROM (mb_product_kardex k, mb_products p)
							LEFT JOIN mb_purchase_orders po ON po.order_id = k.transaction_id
							LEFT JOIN mb_orders o ON o.order_id = k.transaction_id
							LEFT JOIN mb_transaction_types tt ON (k.transaction_type_id = tt.transaction_type_id)
							WHERE 1 = 1
							AND k.product_id = p.product_id
							AND p.store_id = $store_id
							AND p.product_code = '$product_code' ";
			if( $tt_id > 0 )
			{
				//$query .= "AND k.transaction_type_id = tt.transaction_type_id ";
				$query .= "AND k.transaction_type_id = $tt_id ";
			}
			if( $from_date )
			{
				$query .= "AND DATE(k.creation_date) >= '$from_date' ";
			}
			if( $to_date )
			{
				$query .= "AND DATE(k.creation_date) <= '$to_date' ";
			}
			$query .= "ORDER BY k.creation_date ASC";
			$items = $this->dbh->FetchResults($query);
			$results[] = array('label' => sprintf(__("Product: %s", 'mb'), count($items) ? $items[0]->product_name : ''), 'items' => $items);
		}
		else
		{
			//##get all products into store
			$query = "SELECT product_id,product_code,product_name 
						FROM mb_products 
						WHERE store_id = $store_id 
						ORDER BY product_name ASC";
			$prods = $this->dbh->FetchResults($query);
			//##iterate all products into store
			foreach($prods as $p)
			{
				/*
				//##get product batches
				$query = "SELECT batch_code FROM mb_ceass.mb_product_kardex 
							where product_id = {$p->product_id }
							and creation_date >='$from_date' and creation_date <= '$to_date'
							group by batch_code";
				$batches = $this->dbh->FetchResults($query);
				
				foreach($batches as $batch)
				{*/
					//##get product kardex
					$query = "SELECT p.product_id,p.product_code,p.store_id,p.product_name,tt.transaction_name,
									k.in_out,k.quantity as quantity_in, k.quantity as quantity_out,
									k.quantity_balance,k.unit_price,k.total_amount,k.monetary_balance,
									k.transaction_type_id,k.transaction_id,k.creation_date,
									(k.unit_price * k.quantity) as valued_input,
									(k.unit_price * k.quantity) as valued_output,
									(k.unit_price * k.quantity_balance) as valued_balance,
									o.details as o_details,po.details as po_details
								FROM (mb_product_kardex k, mb_products p)
								LEFT JOIN mb_purchase_orders po ON po.order_id = k.transaction_id
								LEFT JOIN mb_orders o ON o.order_id = k.transaction_id
								LEFT JOIN mb_transaction_types tt ON (k.transaction_type_id = tt.transaction_type_id)
								WHERE 1 = 1
								AND k.product_id = p.product_id
								AND k.transaction_type_id = tt.transaction_type_id
								AND p.store_id = $store_id
								AND p.product_id = $p->product_id ";
								//AND k.batch_code = '{$batch->batch_code}' ";
					if( $tt_id > 0 )
					{
						$query .= "AND k.transaction_type_id = $tt_id ";
					}
					if( $from_date )
					{
						$query .= "AND DATE(k.creation_date) >= '$from_date' ";
					}
					if( $to_date )
					{
						$query .= "AND DATE(k.creation_date) <= '$to_date' ";
					}
					$query .= "ORDER BY k.creation_date ASC";
					//var_dump($query);
					$results[] = array('label' => sprintf(__("Product: %s", 'mb'), $p->product_name), 
										'items' => $this->dbh->FetchResults($query));
				//}
			}
		}
		
		$columns = array(
				'creation_date'		=> array('label' => __('Date', 'mb'), 'class' => 'text-left'),
				'product_code'		=> array('label' => __('Code', 'mb')),
				//'product_name'		=> array('label' => __('Product', 'mb')),
				'transaction_name'	=> array('label' => __('Transaction', 'mb')),
				'details'			=> array('label' => __('Recibo', 'mb'), 'class' => 'bg-primary', 'callback' => [$this, '__show_details']),
				//'prev_balance'		=> array('label' => __('Prev. Balance', 'mb'), 'class' => 'text-left'),
				//'batch_code'		=> array('label' => __('Batch', 'mb')),
				'unit_price'		=> array('label' => __('Price', 'mb'), 'class' => 'text-right', 'callback' => [$this, '__show_value']),
				'quantity_in'		=> array('label' => __('Input', 'mb'), 'class' => 'text-right bg-warning', 'callback' => [$this, '__show_qty']),
				'quantity_out'		=> array('label' => __('Output', 'mb'), 'class' => 'text-right bg-warning','callback' => [$this, '__show_qty']),
				'quantity_balance'	=> array('label' => __('Balance', 'mb'), 'class' => 'text-right bg-warning'),
				'valued_input'		=> array(
					'label' 	=> __('Input', 'mb'), 
					'class' 	=> 'text-right bg-success', 
					'callback' 	=> [$this, '__show_value']
				),
				'total_amount'		=> array('label' => __('Output', 'mb'), 'class' => 'text-right bg-success', 'callback' => [$this, '__show_value']),
				'valued_balance'	=> array('label' => __('Balance', 'mb'), 'class' => 'text-right bg-success', 'callback' => [$this, '__show_value']),
				//'total_amount'		=> array('label' => __('Total', 'mb'), 'class' => 'text-right', 'callback' => 'sb_number_format'),
				//'store_id'			=> array('label' => __('Store', 'mb')),
		);
		$columns = SB_Module::do_action('mb_kardex_columns', $columns);
			
		if( $excel )
		{
			$this->BuildKardexExcel($results, $columns);
		}
		elseif( $pdf )
		{
			$this->BuildKardexPDF($results, $columns);
		}
		else
		{
			print $this->BuildKardexHTML($results, $columns);
		}
	}
	public function __show_details($item, $prop)
	{
		if( $item->in_out == 'input' )
		{
			return $item->po_details;
		}
		elseif( $item->in_out == 'output' )
		{
			return $item->o_details;
		}
		return '';
	}
	public function __show_value($item, $prop)
	{
		//return sb_number_format($item->total_amount); 
		if( $item->in_out == 'input' )
		{
			if( in_array($prop, ['unit_price', 'valued_input', 'valued_balance']) )
				return $this->viewCost ? sb_number_format($item->$prop) : 0; 
			return 0;
		}
		elseif( $item->in_out == 'output' )
		{
			if( in_array($prop, ['unit_price', 'quantity_out', 'total_amount', 'valued_balance']) )
				return sb_number_format($item->$prop); 
			else
				return 0;
		}
		return 0;
	}
	public function __show_qty($item, $prop)
	{
		if( $item->in_out == 'input' && $prop == 'quantity_in' )
		{
			return $item->quantity_in; 
		}
		if( $item->in_out == 'output' && $prop == 'quantity_out' )
		{
			return $item->quantity_out; 
		}
		return '';
	}
	protected function BuildKardexHTML($results, $columns)
	{
		
		ob_start(); ?>
		<style>
		td.bg-warning, td.bg-success{font-weight:bold;}
		
		</style>
		<thead>
		<tr>
			<th><?php _e('Num', 'mb'); ?></th>
			<?php foreach($columns as $prop => $col): ?>
			<th><?php print $col['label']; ?></th>
			<?php endforeach; ?>
		</tr>
		</thead> 
		<?php $thead = ob_get_clean(); ?>
		
		<?php ob_start(); ?>
		<?php foreach($results as $res): $i = 1;?>
		<div><b><?php printf(__('Product Kardex - %s', 'mb'), $res['label']); ?></b></div>
		<table class="table table-condensed table-bordered">
		<?php print $thead; ?>
		<tbody>
		<?php foreach($res['items'] as $item): ?>
		<tr class="<?php print $item->transaction_id == -1 ? 'bg-primary' : ''; ?>">
			<td><?php print $i; ?><!-- <?php  ?> --></td>
			<?php foreach($columns as $prop => $col): ?>
			<td class="<?php print isset($col['class']) ? $col['class'] : ''; ?>">
				<?php 
				if( isset($col['callback']) )
				{
					print call_user_func($col['callback'], $item, $prop);
				}
				elseif( $prop == 'transaction_name' && $item->transaction_id == -1 )
				{
					print __('Initial stock', 'mb');
				}
				else
				{
					print $item->$prop;
				}
				?>
			</td>
			<?php endforeach; ?>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		</table>
		<?php endforeach;
		return ob_get_clean();
	}
	/*
	protected function BuildKardexExcel($results, $columns)
	{
	}
	protected function BuildKardexPDF($results, $columns)
	{
		$pdf = mb_get_pdf_instance('', '', 'tcpdf');
		$pdf->AddPage();
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);
		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->MultiCell(55, 10, "KARDEX DE EXISTENCIA", 1, 'C', $fill = 0, 
						$ln = 0, $x = '', $y = '', true);
		//Close and output PDF document
		$pdf->Output('kardex.pdf', 'D');
		die();
	}
	*/
}
