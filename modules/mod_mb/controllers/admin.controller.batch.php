<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_Request;

class LT_AdminControllerMbBatch extends SB_Controller
{
	public function task_default()
	{
		$this->dbh->Select('*')
				->From('mb_batch')
				->OrderBy('creation_date', 'desc');
		$this->dbh->Query(null);
		$items = $this->dbh->FetchResults();
		sb_set_view_var('items', $items);
	}
	public function task_new()
	{
		$title = __('Edit Batch', 'mb');
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		$query = "SELECT * FROM mb_batch WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=batch.default'));
		}
		$w = $this->dbh->FetchRow();
		sb_set_view('batch.new');
		sb_set_view_var('title', SBText::_('Edit Batch', 'mb'));
		sb_set_view_var('b', $w);
		$this->document->SetTitle($title);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier is invalid.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=batch.default'));
		}
		$query = "SELECT * FROM mb_batch WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier does not exists.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=batch.default'));
		}
		$query = "DELETE FROM mb_batch WHERE id = $id LIMIT 1";
		$this->dbh->Query($query);
		SB_MessagesStack::AddMessage(SBText::_('The batch has been deleted.'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=batch.default'));
	}
	public function task_save()
	{
		$id 			= SB_Request::getInt('id');
		$name 			= SB_Request::getString('name');
		
		$data 			= compact('name');
		$msg = $link = null;
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_batch', $data);
			$msg = __('The batch has been created', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=batch.default');
		}
		else
		{
			$this->dbh->Update('mb_batch', $data, array('id' => $id));
			$msg = __('The batch has been updated', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=batch.edit&id='.$id);
		}
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
}