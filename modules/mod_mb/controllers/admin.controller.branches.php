<?php
class LT_AdminControllerMbBranches extends SB_Controller
{
	public function task_default()
	{
		$branches = SB_Warehouse::GetBranches();
		$title = __('Branch Office', 'mb');
		sb_set_view_var('title', $title);
		sb_set_view_var('branches', $branches);
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		$id 		= SB_Request::getInt('id');
		$name 		= SB_Request::getString('bo_name');
		$telephone 	= SB_Request::getString('bo_telephone');
		$address	= SB_Request::getString('bo_address');
		
		$data = compact('name', 'address', 'telephone');
		
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d');
			$id = $this->dbh->Insert('mb_departments', $data);
			SB_MessagesStack::AddMessage(__('The branch office has been created', 'mb'), 'success');
		}
		else
		{
			$this->dbh->Update('mb_departments', $data, array('department_id' => $id));
			SB_MessagesStack::AddMessage(__('The branch office has been updated', 'mb'), 'success');
		}
		SB_Module::do_action('mb_save_branch', $id, $data);
		sb_redirect(SB_Route::_('index.php?mod=mb&view=branches.default'));
	}
	public function task_delete()
	{
		$id 		= SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid branch office identifier', 'mb'), 'success');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=branches.default'));
		}
		$this->dbh->Delete('mb_departments', array('department_id' => $id));
		SB_MessagesStack::AddMessage(__('The branch office has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=branches.default'));
	}
}