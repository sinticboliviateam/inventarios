<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;

class LT_AdminControllerMbCategories extends SB_Controller
{
	public function task_default()
	{
		$keyword = SB_Request::getString('keyword');
		$storeId	= $this->request->getInt('store_id');
		$query = "SELECT c.*, cc.category_id as child_id, cc.name as child_name,cc.parent as child_parent, cc.description as child_description, s.store_name ".
                    "FROM mb_categories c ".
                    "LEFT JOIN mb_categories cc ON cc.parent = c.category_id ".
                    "LEFT JOIN mb_stores s ON s.store_id = c.store_id ".
                    "WHERE 1 = 1 " .
                    "AND (c.parent IS NULL OR c.parent = 0) ";
        if( $storeId )
        {
			$query .= "AND c.store_id = $storeId ";
		}
                    /*
		$query = "SELECT c.*, s.store_id,s.store_name 
					FROM mb_categories c
					LEFT JOIN mb_stores s ON s.store_id = c.store_id ";
		$query .= "WHERE 1 = 1 ";
        */
		if( !empty($keyword) )
		{
			$query .= "AND c.name LIKE '%$keyword%' ";
		} 
		$query .= "ORDER BY c.creation_date DESC";
		$this->dbh->Query($query);
		$categories = $this->dbh->FetchResults();
		$stores = SB_Warehouse::getStores();
		//sb_set_view_var('categories', $categories);
		$this->SetVars(get_defined_vars());
	}
	public function task_new()
	{
		$categories = SB_Warehouse::getCategories();
		$stores = SB_Warehouse::getStores();
		sb_set_view_var('page_title', SBText::_('Create New Category', 'mb'));
		sb_set_view_var('stores', $stores);
		sb_set_view_var('categories', array());
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The category identifier is invalid', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=categories.default'));
		}
		$category = new SB_MBCategory($id);
		if( !$category->category_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The category identifier does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=categories.default'));
		}
		$categories	= SB_Warehouse::getCategories($category->store_id);
		$stores 	= SB_Warehouse::getStores();
		sb_set_view('categories.new');
		sb_set_view_var('page_title', SBText::_('Create New Category', 'mb'));
		sb_set_view_var('stores', $stores);
		sb_set_view_var('categories', $categories);
		sb_set_view_var('the_cat', $category);
	}
	public function task_save()
	{
		$id 			= SB_Request::getInt('cat_id');
		$name 			= SB_Request::getString('category_name');
		$description 	= SB_Request::getString('description');
		$store_id		= SB_Request::getInt('store_id');
		$parent			= SB_Request::getInt('parent_id');
		$slug			= SB_Request::getString('slug', sb_build_slug($name));
		
		if( $parent < 0 )
			$parent = 0;
		
		$data = compact('name', 'description', 'store_id', 'slug', 'parent');
		$dbh = SB_Factory::getDbh();
		$msg = $link = null;
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d');
			$id = $dbh->Insert('mb_categories', $data);
			$msg = __('The category has been created', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=categories.default');
		}
		else
		{
			$this->dbh->Update('mb_categories', $data, array('category_id' => $id));
			$msg 	= __('The category has been updated', 'mb');
			$link 	= SB_Route::_('index.php?mod=mb&view=categories.edit&id='.$id);
		}
		//##check for image upload
		if( isset($_FILES['image']) && $_FILES['image']['size'] > 0 )
		{
			$cat = new SB_MBCategory($id);
			$filename = sb_get_unique_filename($_FILES['image']['name'], UPLOADS_DIR);
			if( move_uploaded_file($_FILES['image']['tmp_name'], $filename) )
			{
				if( $cat->image_id )
				{
					$attach = new SB_Attachment($cat->image_id);
					$attach->Delete();
				}
				$img_id = lt_insert_attachment($filename, 'SB_MBCategory', $id, 0, 'image');
				$this->dbh->Update('mb_categories', array('image_id' => $img_id), array('category_id' => $id));
			}
			
		}
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The category identifier is invalid', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=categories.default'));
		}
		$category = new SB_MBCategory($id);
		if( !$category->category_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The category identifier does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=categories.default'));
		}
		$dbh = SB_Factory::getDbh();
		$query = "DELETE FROM mb_categories WHERE category_id = $category->category_id";
		$dbh->Query($query);
		$query = "DELETE FROM mb_product2category WHERE category_id = $category->category_id";
		$dbh->Query($query);
		SB_MessagesStack::AddMessage(SBText::_('The category has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=categories.default'));
	}
	public function task_get_store_categories()
	{
		$store_id = SB_Request::getInt('store_id');
		$ops = mb_dropdown_categories(array('store_id' => $store_id, 'type' => 'select', 'only_ops' => true));
		//$cats = SB_Warehouse::getCategories($store_id);
		$res = array('status' => 'ok', 
						'ops' => '<option value="-1">'.__('-- parent category --', 'mb').'</option>'.$ops);
		/*
		$res['ops'] .= '<option value="-1">'.SBText::_('-- parent category --').'</option>';
		foreach($cats as $c)
		{
			$res['ops'] .= '<option value="'.$c->category_id.'">'.$c->name.'</option>';
		}
		*/
		header('Content-type: application/json');
		die(json_encode($res));
	}
}
