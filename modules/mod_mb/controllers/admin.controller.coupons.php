<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;

class LT_AdminControllerMbCoupons extends SB_Controller
{
	public function task_default()
	{
		$query = "SELECT * FROM mb_coupons ORDER BY creation_date DESC";
		$coupons = $this->dbh->FetchResults($query);
		sb_set_view_var('coupons', $coupons);
	}
	public function task_new()
	{
		$title = __('New Coupon', 'mb');
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid coupon identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
		}
		$coupon = new SB_MBCoupon($id);
		if( !$coupon->coupon_id )
		{
			SB_MessagesStack::AddMessage(__('The coupon does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
		}
		sb_set_view('coupons.new');
		$title = __('Edit Coupon', 'mb');
		sb_set_view_var('title', $title);
		sb_set_view_var('coupon', $coupon);
		$this->document->SetTitle($title);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid coupon identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
		}
		$coupon = new SB_MBCoupon($id);
		if( !$coupon->coupon_id )
		{
			SB_MessagesStack::AddMessage(__('The coupon does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
		}
		$this->dbh->Delete('mb_coupons', array('coupon_id' => $id));
		SB_MessagesStack::AddMessage(__('The coupon has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
	}
	public function task_save()
	{
		$id 					= SB_Request::getInt('id');
		$code 					= SB_Request::getString('code');
		$description 			= SB_Request::getString('description');
		$discount				= SB_Request::getFloat('discount');
		$usage_limit			= SB_Request::getInt('usage_limit');
		$min_amount_required	= SB_Request::getFloat('min_amount_required');
		$sdate					= SB_Request::getDate('start_date');
		$sdate_time				= SB_Request::getTimeStamp('start_date');
		$shour					= SB_Request::getInt('start_date_hour');
		$smin					= SB_Request::getInt('start_date_min');
		$edate_time				= SB_Request::getTimeStamp('end_date');
		$ehour					= SB_Request::getInt('end_date_hour');
		$emin					= SB_Request::getInt('end_date_min');
		$status					= SB_Request::getString('status', 'active');
		if( empty($code) )
		{
			SB_MessagesStack::AddMessage(__('Invalid coupon code'), 'error');
			$id ? $this->task_edit() : $this->task_new();
			return false;
		}
		if( $discount < 0 )
		{
			SB_MessagesStack::AddMessage(__('Invalid coupon discount'), 'error');
			$id ? $this->task_edit() : $this->task_new();
			return false;
		}
		if( !$id && SB_MBCoupon::GetCouponByCode($code) )
		{
			SB_MessagesStack::AddMessage(__('The coupon code already exists, please choose a new one.'), 'error');
			$this->task_new();
			return false;
		}
		$start_date_time = mktime($shour, $smin, null, date('m', $sdate_time), date('d', $sdate_time), date('Y', $sdate_time));
		$start_date = date('Y-m-d H:i:s', $start_date_time);
		$end_date_time = mktime($ehour, $emin, null, date('m', $edate_time), date('d', $edate_time), date('Y', $edate_time));
		$end_date = date('Y-m-d H:i:s', $end_date_time);
		
		$data = compact('code', 'description', 'discount', 'usage_limit', 'min_amount_required', 'start_date', 'end_date', 'status');
		if( $id )
		{
			unset($data['code']);
			$this->dbh->Update('mb_coupons', $data, array('coupon_id' => $id));
			SB_MessagesStack::AddMessage(__('The coupon has been updated', 'mb'), 'success');
		}
		else 
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_coupons', $data);
			SB_MessagesStack::AddMessage(__('The coupon has been created', 'mb'), 'success');
		}
		sb_redirect(SB_Route::_('index.php?mod=mb&view=coupons.default'));
		die();
	}
}