<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Route;

class LT_AdminControllerMbMu extends SB_Controller
{
	public function task_default()
	{
		$query = "SELECT * FROM mb_unit_measures ORDER BY creation_date DESC";
		$units = $this->dbh->FetchResults($query);
		sb_set_view_var('units', $units);
	}
	public function task_get()
	{
	}
	public function task_save()
	{
		$id 	= SB_Request::getInt('id');
		$code 	= SB_Request::getString('code');
		$name	= SB_Request::getString('unit');
		if( empty($code) )
		{
			SB_MessagesStack::addMessage(__('The measurement unit code is invalid or empty.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&mu.default'));
			return false;
		}
		if( empty($name) )
		{
			SB_MessagesStack::addMessage(__('The measurement unit is invalid or empty.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&mu.default'));
			return false;
		}
		$data = compact('code', 'name');
		$data['last_modification_date'] = date('Y-m-d H:i:s');
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_unit_measures', $data);
			SB_MessagesStack::addMessage(__('Measurement Unit added', 'mb'), 'success');
		}
		else
		{
			$this->dbh->Update('mb_unit_measures', $data, array('measure_id' => $id));
			SB_MessagesStack::addMessage(__('Measurement Unit updated', 'mb'), 'success');
		}
		sb_redirect(SB_Route::_('index.php?mod=mb&view=mu.default'));
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		$this->dbh->Delete('mb_unit_measures', array('measure_id' => $id));
		SB_MessagesStack::addMessage(__('The measurement unit has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=mu.default'));
	}
}
