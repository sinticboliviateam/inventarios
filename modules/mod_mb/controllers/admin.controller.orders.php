<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

class LT_AdminControllerMbOrders extends SB_Controller
{
	/**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var OrderModel
	 */
	protected $orderModel;
	
	public function task_default()
	{
		$user		= sb_get_current_user();
        
		if( !$user->can('mb_manage_orders') )
		{
			lt_die(__('You are not allowed to see the orders', 'mb'));
		}
		$page		= $this->request->getInt('page', 1);
		$limit		= $this->request->getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
		$order_by 	= $this->request->getString('order_by', 'creation_date');
		$order		= $this->request->getString('order', 'desc');
		$keyword	= $this->request->getString('keyword');
		$store_id	= $this->request->getInt('store_id');
		$date_from	= $this->request->getDate('date_from');
		$date_to	= $this->request->getDate('date_to');
		$status		= $this->request->getString('status');
		
		
		$columns = "o.*,s.store_id,s.store_name";
		$tables = array(
				'mb_orders o',
				'mb_stores s'
		);
		$where = array(
				'o.store_id = s.store_id'
		);
		if( !$user->isRoot() )
		{
			$store_id = (int)$user->_store_id;
		}
		if( $store_id > 0 )
		{
			$where[] = 's.store_id = ' . $store_id;
		}
		if( $user->role_id == 0 || $user->can('mb_see_all_orders') )
		{
			
		}
		elseif( $user->can('mb_see_its_orders')  )
		{
			$where[] = "o.user_id = {$user->user_id}";
		}
		else
		{
			$where[] = "o.user_id = {$user->user_id}";
		}
		if( $keyword )
		{
			if( (int)$keyword > 0 )
			{
				$where[] = "order_id = $keyword ";
			}
			else 
			{
				$tables[] = 'mb_customers c';
				$where[] = "o.customer_id = c.customer_id";
				$where[] = "CONCAT(c.first_name, ' ', c.last_name) LIKE '%$keyword%'";
			}
		}
		if( $date_from )
		{
			$where[] = "DATE(o.order_date) >= '$date_from'";
		}
		if( $date_to )
		{
			$where[] = "DATE(o.order_date) <= '$date_to'";
		}
		if( $status && (int)$status != -1 )
		{
			if( $status == 'deleted' && !$user->can('mb_can_see_deleted_orders') )
			{
				$where[] = "o.status <> 'deleted'";
			}
			else 
			{
				$where[] = "o.status = '$status'";
			}
			
		}
		else 
		{
			$where[] = "o.status <> 'deleted'";
		}
		$query = "SELECT {columns} " .
					sprintf("FROM  %s ", implode(',', $tables)).
					(count($where) ? 'WHERE 1 = 1 AND ' . implode(' AND ', $where) . ' ' : '');
		//var_dump(str_replace('{columns}', 'COUNT(order_id)', $query));
		$total_rows = $this->dbh->GetVar(str_replace('{columns}', 'COUNT(order_id)', $query));
		$pages		= ceil($total_rows / $limit);
		$page 		= ($page <= 0) ? 1 : $page;
		$offset		= $page == 1 ? 0 : ($page - 1) * $limit;
		$sql_limit 	= "LIMIT $offset, $limit";
		$query 		= str_replace('{columns}', $columns, $query);  
		$query 		.= "ORDER BY o.$order_by $order $sql_limit";
		$rows 		= $this->dbh->FetchResults($query);
	
		$orders = array();
		foreach($rows as $row)
		{
			$o = new SB_MBOrder();
			$o->SetDbData($row);
			$orders[] = $o; 
		}
		$table_cols = array(
				array('label' => __('Order', 'mb'), 'data' => '')
		);
		$order_actions = SB_Module::do_action('mb_order_actions', array(
				'send_receipt' 	=> SBText::_('Send receipt', 'mb'),
				'update_stock' 	=> SBText::_('Update Stock', 'mb'),
		));
		sb_set_view_var('user', $user);
		sb_set_view_var('orders', $orders);
		sb_set_view_var('keyword', $keyword);
		sb_set_view_var('stores', SB_Warehouse::GetUserStores($user));
		sb_set_view_var('date_from', $date_from ? $date_from : date('Y-m-d', mktime(null, null, null, date('m'), null, date('Y'))));
		sb_set_view_var('date_to', $date_to ? $date_to : date('Y-m-d H:i:s'));
		sb_set_view_var('current_page', $page);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('order_actions', $order_actions);
		$this->document->SetTitle(__('Orders', 'mb'));
	}
	public function task_new()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_create_order') )
		{
			lt_die(__('You dont have enough permissions to create orders', 'mb'));
		}
		$order_status = mb_get_order_statuses();
		$payment_statuses = mb_get_order_payment_statuses();
		$order_actions = SB_Module::do_action('mb_order_actions', array(
				'send_receipt' 	=> SBText::_('Send receipt', 'mb'),
				'update_stock' 	=> SBText::_('Update Stock', 'mb'),
		));
        
		$title = __('New Order', 'mb');
		sb_set_view_var('title', $title);
		sb_set_view_var('order_status', $order_status);
		sb_set_view_var('payment_status', $payment_statuses);
		sb_set_view_var('stores', SB_Warehouse::getStores());
        sb_set_view_var('order_actions', $order_actions);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_edit_order') )
		{
			lt_die(__('You dont have enough permissions to edit orders', 'mb'));
		}
		$order_status = mb_get_order_statuses();
		$payment_statuses = mb_get_order_payment_statuses();
		
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('Invalid order identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$order = new SB_MBOrder($id);
		if( !$order->order_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The order does not exists.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$order_actions = SB_Module::do_action('mb_order_actions', array(
				'send_receipt' 	=> SBText::_('Send receipt', 'mb'),
				'update_stock' 	=> SBText::_('Update Stock', 'mb'),
		));
		sb_set_view('orders.new');
		$title = sprintf(SBText::_('Edit Order #%d', 'mb'), $order->order_id);
		sb_set_view_var('title', $title);
		sb_set_view_var('order', $order);
		sb_set_view_var('order_status', $order_status);
		sb_set_view_var('payment_status', $payment_statuses);
		sb_set_view_var('stores', SB_Warehouse::getStores());
		sb_set_view_var('user', new SB_User($order->user_id));
		sb_set_view_var('order_actions', $order_actions);
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fine-uploader');
		$this->document->SetTitle($title);
	}
	public function task_view()
	{
		$user = sb_get_current_user();
		/*
		if( !$user->can('mb_edit_order') )
		{
			lt_die(__('You dont have enough permissions to edit orders', 'mb'));
		}
		*/
		$order_status = mb_get_order_statuses();
		$payment_statuses = mb_get_order_payment_statuses();
	
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('Invalid order identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$order = new SB_MBOrder($id);
		if( !$order->order_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The order does not exists.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$order_actions = SB_Module::do_action('mb_order_actions', array(
				'send_receipt' 	=> SBText::_('Send receipt', 'mb'),
				'update_stock' 	=> SBText::_('Update Stock', 'mb'),
		));
		$title = sprintf(SBText::_('Edit Order #%d', 'mb'), $order->order_id);
		sb_set_view_var('title', $title);
		sb_set_view_var('order', $order);
		sb_set_view_var('order_status', $order_status);
		sb_set_view_var('payment_status', $payment_statuses);
		sb_set_view_var('stores', SB_Warehouse::getStores());
		sb_set_view_var('user', new SB_User($order->user_id));
		sb_set_view_var('order_actions', $order_actions);
		sb_set_view_var('current_user', $user);
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fine-uploader');
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		$user 			= sb_get_current_user();
		$id 			= SB_Request::getInt('id');
		if( !$id && !$user->can('mb_create_order') )
		{
			lt_die(__('You dont have enough permissions to create orders', 'mb'));
		}
		elseif( $id && !$user->can('mb_edit_order') )
		{
			lt_die(__('You dont have enough permissions to edit orders', 'mb'));
		}
		
		$status 		= SB_Request::getString('status');
		$payment_status	= SB_Request::getString('payment_status');
		$details		= SB_Request::getString('notes');
		$action			= SB_Request::getString('action');
		$store_id		= SB_Request::getInt('store_id');
		$customer_id	= SB_Request::getInt('customer_id');
		$order_date		= SB_Request::getDateTime('order_date');
		$delivery_date	= SB_Request::getDate('delivery_date');
		//$type 		= 'backend';
		$the_items		= (array)SB_Request::getVar('item', array());
		
		$items 			= 0;
		$subtotal 		= 0;
		$total_tax		= 0;
		$discount 		= 0;
		$total			= 0;
		$order_items 	= array();
		
		$order		= null;
		if( $id )
		{
			$order = new SB_MBOrder($id);
			//##check if the order exists
			if( $order->order_id )
			{
				if( $user->IsRoot() && $order->status == 'complete' && 'cancelled' != $status )
				{
					$order->ChangeStatus($status);
					SB_MessagesStack::AddMessage(__('The order has been updated.', 'mb'), 'success');
					sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
				}
				//##cehck if current order status is complete, then do nothing
				elseif( !$user->IsRoot() && $order->status == 'complete' )
				{
					SB_MessagesStack::AddMessage(__('The order is complete, you cant do changes', 'mb'), 'info');
					sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
				}
				//##check if the new status for the order is cancelled
				elseif( $order->status != 'cancelled' && $status == 'cancelled' )
				{
					$order->Cancel();
					SB_MessagesStack::AddMessage(__('The order has been cancelled.', 'mb'), 'success');
					sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
				}
			}
		}
		
		foreach($the_items as $item)
		{
			$item_total = (int)$item['qty'] * (float)$item['price'];
			$order_items[] = array(
					'product_id'	=> $item['product_id'],
					'name'			=> htmlentities($item['name']),
					'quantity'		=> (int)$item['qty'],
					'price'			=> (float)$item['price'],
					'subtotal'		=> (int)$item['qty'] * (float)$item['price'],
					'total'			=> $item_total,
					'status'		=> $status, 
					'last_modification_date'	=> date('Y-m-d H:i:s'),
					'creation_date'	=> date('Y-m-d H:i:s')
			);
			$items 		+= (int)$item['qty'];
			$subtotal 	+= $item_total;
		}
		$total = $total_tax + $subtotal;
		$data = compact('store_id', 'items', 'subtotal', 'discount', 'total', 'details', 'customer_id', 'order_date', 'delivery_date', 'status', 'payment_status');
		$msg = null;
		$updated_items = array();
		$deleted_items = array();
		$old_items_ids = array();
		$new_items_ids = array();
		if( !$id )
		{
            
			$data['type']			= 'backend';
			$data['user_id']		= sb_get_current_user()->user_id;
			$data['last_modification_date'] = $data['creation_date'] 	= date('Y-m-d H:i:s');
			$id = mb_insert_sale_order($data, $order_items);
			$msg = __('The order has been registered', 'mb');
		}
		else 
		{
			$data['order_id'] = $id;
			$data['last_modification_date'] = date('Y-m-d H:i:s');
			//##revalidate and compare order items
			//##get old order items
			$old_items = $order->GetItems();
			//##create new items
			foreach($old_items as $index => $old_item)
			{
				$old_items_ids[$index] = $old_item->product_id;
				foreach($the_items as $new_item)
				{
					$new_items_ids[] = $new_item['product_id'];
					//var_dump("if( (int)$old_item->product_id == (int){$new_item['product_id']} && (int)$old_item->quantity != (int){$new_item['qty']} )");
					if( (int)$old_item->product_id == (int)$new_item['product_id'] && (int)$old_item->quantity != (int)$new_item['qty'] )
					{
						$update_item 		= $new_item;
						//var_dump($update_item);
						if( $new_item['qty'] > $old_item->quantity )
						{
							$update_item['qty']	= ($new_item['qty'] - $old_item->quantity) * (-1);
						}
						else
						{
							$update_item['qty']	= ($old_item->quantity - $new_item['qty']);
						}
						
						$updated_items[] 	= $update_item; 
					}
				}
			}
			/*
			print_r($old_items_ids);
			print_r($new_items_ids);
			print_r(array_diff($old_items_ids, $new_items_ids));
			die();
			*/
			$id = mb_insert_sale_order($data, $order_items);
			$msg = __('The order has been updated', 'mb');
		}
		//##update ordermeta
		foreach((array)SB_Request::getVar('meta') as $meta_key => $meta_value)
		{
			mb_update_order_meta($id, $meta_key, trim($meta_value));
		}
		$order = new SB_MBOrder($id);
		
		$link = SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id);
		//##check for action
		if( $action )
		{
			if( $action == 'update_stock' )
			{
				$deleted_items = array_diff($old_items_ids, $new_items_ids);
				//##check for deleted items and revert quantities
				foreach($deleted_items as $prod_id)
				{
					foreach($old_items as $oi)
					{
						if( $oi->product_id == $prod_id )
						{
							$order->RevertItem($oi);
						}
					}		
				}
				//##check if we need to update the product stock based on items changes
				if( count($updated_items) )
				{
					//##update stock for updated items
					foreach($updated_items as $new_item)
					{
						if( in_array($new_item['product_id'], $deleted_items) ) continue;
						$data = array();
						if( $new_item['qty'] < 0)
						{
							//##the item quantity has incremented
							$this->dbh->Update('mb_products',
									array('product_quantity' => "OP[product_quantity {$new_item['qty']}]"),
									array('product_id' => $new_item['product_id']));
						}
						else
						{
							//##the item quantity has reduced
							$this->dbh->Update('mb_products',
									array('product_quantity' => "OP[product_quantity + {$new_item['qty']}]"),
									array('product_id' => $new_item['product_id']));
						}
							
					}
				}
				/*
				else 
				{
					$order->UpdateStock();
				}
				*/
			}
			
			SB_Module::do_action('mb_order_action_' . $action, $order);
		}
		SB_Module::do_action('mb_order_save', $id, $data);
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_upload()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			sb_response_json(array('status' => 'error', 'error' => __('Invalid order identifier', 'mb')));
		}
		sb_include('qqFileUploader.php', 'file');
		$uh = new qqFileUploader();
		$uh->allowedExtensions = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'psd', 'tiff', 'ai', 'pdf', 'xls', 
										'xlsx', 'xlsm', 'doc', 'docx', 'ppt', 'pptx',
										'zip', 'rar', 'tar.gz'
		);
		$uh->inputName = 'qqfile';
		$uh->chunksFolder 		= 'chunks';
		$res 					= $uh->handleUpload(UPLOADS_DIR);
		if( !isset($res['error']) )
		{
			$filename 				= $uh->getUploadName();
			$aid 					= lt_insert_attachment(UPLOADS_DIR . SB_DS . $filename, 'order', $id, 0, 'document');
			$res['attachment_id'] 	= $aid;
			$res['url'] = UPLOADS_URL . '/' . $filename;
		}
		sb_response_json($res);
	}
	/**
	 * Delete an order attachment
	 * 
	 */
	public function task_da()
	{
		if( !sb_is_user_logged_in() )
		{
			lt_die(__('You need to start a session', 'mb'));
		}
		if( !sb_get_current_user()->can('delete_attachment') )
		{
			lt_die(__('You cant complete this operation, please contact with administrator.', 'mb'));
		}
		$id = SB_Request::getInt('id');
		$aid = SB_Request::getInt('aid');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid order identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		if( !$aid )
		{
			SB_MessagesStack::AddMessage(__('Invalid attachment identifier', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$query = "SELECT * FROM attachments WHERE object_type = 'order' AND object_id = $id AND attachment_id = $aid LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(__('The attachment does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
		}
		$row = $this->dbh->FetchRow();
		if( file_exists(UPLOADS_DIR . SB_DS . $row->file) )
			unlink(UPLOADS_DIR . SB_DS . $row->file);
		$this->dbh->Delete('attachments', array('attachment_id' => $aid));
		SB_MessagesStack::AddMessage(__('The attachment has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.edit&id='.$id));
	}
	public function task_print_receipt()
	{
		$settings = sb_get_parameter('mb_settings');
		
		$id = SB_Request::getInt('id');
		$order = new SB_MBOrder($id);
		if( !$order->order_id )
		{
			lt_die(__('The order does not exists', 'mb'));
		}
		SB_Module::do_action('mb_order_before_show_receipt', $order);
		$logo = isset($settings->business_logo) ? UPLOADS_DIR . SB_DS . $settings->business_logo : null;
		$logo_url = null;
		if( file_exists($logo) )
			$logo_url = UPLOADS_URL . '/' . $settings->business_logo;
        
		//$tpl_receipt = dirname(dirname(__FILE__)) . SB_DS . 'templates' . SB_DS . 'email' . SB_DS . 'tpl-receipt.php';
        $tpl_receipt = dirname(dirname(__FILE__)) . SB_DS . 'templates' . SB_DS . 'tpl-voucher.php';
		$tpl_receipt = SB_Module::do_action('mb_order_receipt_tpl', $tpl_receipt, $order);
		extract((array)$settings);
		ob_start();
		include $tpl_receipt;
		$html = ob_get_clean();
		$pdf = mb_get_pdf_instance(__('Sale Order Receipt', 'mb'), '', 'dompdf');
		$pdf->loadHtml($html);
		$pdf->render();
		$pdf->stream(sprintf(__('order-receipt-%d.pdf', 'mb'), $order->sequence), array('Attachment' => 0));
		die();
	}
	public function task_delete()
	{
		if( !sb_get_current_user()->can('mb_delete_order') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permissions to delete orders', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
		}
		$id = SB_Request::getInt('id');
		$this->dbh->Update('mb_orders', array('status' => 'deleted'), array('order_id' => $id));
		SB_MessagesStack::AddMessage(__('The order has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=orders.default'));
	}
	public function task_revert()
	{
		$user  = sb_get_current_user();
		$id = $this->request->getInt('id');
		$backlink = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->Route('index.php?mod=mb&view=orders.default');
		try
		{
			if( !$user->can('mb_revert_sale') )
				throw new Exception(__('You are not authorized to revert sales', 'mb'));
			if( !$id )
				throw new Exception(__('The purchase order identifier is invalid', 'mb'));
			$order = new SB_MBOrder($id);
			if( !$order || !$order->order_id )
				throw new Exception(__('The purchase order does not exists', 'mb'));
			$this->dbh->BeginTransaction();
			$this->orderModel->Revert($order);
			$this->dbh->EndTransaction();
			SB_MessagesStack::AddSuccess(__('The purchase order has been reverted', 'mb'));
			sb_redirect($backlink);
		}
		catch(Exception $e)
		{
			$this->dbh->Rollback();
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($backlink);
		}
	}
}
