<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Order;
use SinticBolivia\SBFramework\Modules\Customers\Entities\Customer;
use SinticBolivia\SBFramework\Modules\Mb\Models\PosModel;
//use Exception;

class LT_AdminControllerMbPos extends SB_Controller
{
	/**
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\PosModel
	 **/
	protected $posModel;
	
	public function task_default()
	{
		$user = sb_get_current_user();
		if( !sb_is_user_logged_in() )
		{
			die(SBText::_('You need to start session', 'mb'));
		}
		if( !$user->can('mb_sell') )
		{
			SB_MessagesStack::AddMessage(SBText::_('You dont have enough permission to do this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb'));
		}
        
		$store_id = null;
        if( !$user->can('mb_see_all_stores') )
        {
            $store_id = (int)$user->_store_id;
        }
        else
        {
            $store_id = SB_Request::getInt('store_id');
        }
        
		if( !$store_id && $user->can('mb_see_all_stores') )
		{
			SB_MessagesStack::AddMessage(SBText::_('You need to select a store', 'mb'), 'info');
			$stores = SB_Warehouse::getStores();
			sb_set_view('select_store');
			sb_set_view_var('stores', $stores);
			return true;
		}
        elseif( !$store_id )
        {
            lt_die($this->__('You don\'t have any store assigned, place contact with administrator'));
        }
		$store = new SB_MBStore($store_id);
		if( !$store->store_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The store does not exists', 'mb'), 'error');
			return false;
		}
		$layout 	= SB_Request::getString('layout', 'list');
		$products 	= SB_Warehouse::getStoreProducts($store_id);
		$categories = SB_Warehouse::getCategories($store_id);
		//##check if cashbox is opened
		$cdate = date('Y-m-d');
		$query = "SELECT * FROM mb_cashbox WHERE cashier_id = $user->user_id AND date(today) = '$cdate' LIMIT 1";
		if( $row = $this->dbh->FetchRow($query) )
		{
			SB_Session::setVar('cashbox_id', $row->id);
		}
		//##get payment methods
		$payment_methods = array(
				(object)array(
						'key' => 'cash',
						'name'	=> SBText::_('Cash', 'mb'),
						'image_url' => MOD_MB_URL . '/images/money-icon.jpg'
				),
				(object)array(
						'key' => 'credit_card',
						'name'	=> SBText::_('Credit Card', 'mb'),
						'image_url' => MOD_MB_URL . '/images/credit-cards.jpg'
				),
				(object)array(
						'key' => 'paypal',
						'name'	=> SBText::_('Paypal', 'mb'),
						'image_url' => MOD_MB_URL . '/images/PayPal.jpg'
				)
		);
        $title = sprintf($this->__('%s - Point of Sale - %s'), SITE_TITLE, $store->store_name);
		sb_set_view_var('layout', $layout);
		sb_set_view_var('store', $store);
		sb_set_view_var('categories', $categories);
		sb_set_view_var('products', $products);
		sb_set_view_var('payment_methods', $payment_methods);
        sb_set_view_var('user', $user);
		SB_Module::add_action('lt_js_globals', array($this, 'pos_js_locale'));
		sb_add_style('pos-css', MOD_MB_URL . '/css/pos.css');
		sb_add_script(MOD_MB_URL . '/js/pos.js?t='.time(), 'pos-js');
		//##set the template
		$this->document->templateFile = 'module.php';
        $this->document->SetTitle($title);
		SB_Module::do_action('mb_pos_before_show');
	}
	public function task_ajax()
	{
        parent::task_ajax();
		$res = array();
		$user	= sb_get_current_user();
		$action = SB_Request::getString('action');
		if( $action == 'get_product' )
		{
			$id = SB_Request::getInt('id');
			$by = SB_Request::getString('by');
			$product = null;
			if( $by )
			{
				$product = SB_Warehouse::GetProductBy($by, $id);
			}
			else 
			{
				$product = new SB_MBProduct($id);
			}
			if( $product->product_id )
			{
				$res['status'] = 'ok';
				$res['product'] = $product;
			}
			else
			{
				$res['status'] = 'error';
				$res['error'] = SB_Text::_('The product does not exists', 'mb');
			}
		}
		else if( $action == 'find_customer' )
		{
			sb_include_module_helper('customers');
			$keyword = SB_Request::getString('keyword');
			if( is_numeric($keyword) )
			{
				$customer = LT_HelperCustomers::FindCustomerBy($keyword, 'nit_ruc_nif');
				if( $customer )
				{
					$customer->pending_payments = $customer->GetPayments('pending');
					$res['status'] = 'ok';
					$res['customer'] = $customer;
				}
				else 
				{
					$res['status'] = 'error';
					$res['error'] = SB_Text::_('The customer does not exists', 'mb');
				}
			}
		}
		elseif( $action == 'cash_count' )
		{
			//##get data for cash count
			$store_id	= SB_Request::getInt('store_id'); 
			$cashier 	= sb_get_current_user();
			$dbh		= SB_Factory::getDbh();
			$query 		= "SELECT SUM(total) FROM mb_orders " .
							"WHERE user_id = $cashier->user_id " .
							"AND store_id = $store_id " .
							"AND type = 'pos' " .
							"AND DATE(order_date) = CURDATE()";
			$res['status'] 		= 'ok';
			$res['total_sales']	= (float)$dbh->GetVar($query);
			if( !$res['total_sales'] )
				$res['total_sales'] = 0;
			$cdate = date('Y-m-d');
			$query = "SELECT * FROM mb_cashbox WHERE cashier_id = $user->user_id AND date(today) = '$cdate' LIMIT 1";
			$row = $this->dbh->FetchRow($query);
			if( !$row )
			{
				$res['status'] = 'error';
				$res['error'] = __('The cashbox has not been opened', 'mb');
			}
			else
			{
				$res['balance'] = $row->initial_amount;
				//##get spending
				$query = "SELECT * FROM mb_payments ".
							"WHERE store_id = $store_id ".
							"AND user_id = $user->user_id ".
							"AND ptype = 'output' ".
							"AND DATE(payment_date) = '$cdate' ".
							"ORDER BY payment_date ASC";
				$rows = $this->dbh->FetchResults($query);
				$res['total_spends'] = 0;
				foreach($rows as $_row)
				{
					$res['total_spends'] += $_row->amount; 
				}
				$res['expensed'] 	= $rows;
				$res['total_cash'] 	= ($row->initial_amount + $res['total_sales']) - $res['total_spends'];
			}
		}
		elseif( $action == 'save_cashcount' )
		{
			$cashier	= sb_get_current_user();
			$cashier_id = $cashier->user_id;
			$data		= json_encode($_POST);
			$store_id 	= SB_Request::getInt('store_id');
			$sales		= (float)SB_Request::getVar('sales');
			$spends		= (float)SB_Request::getVar('total_spends');
			$calculated	= (float)SB_Request::getVar('total_cash');
			$cash		= (float)SB_Request::getFloat('manual_balance');
			$cashbox_id = (int)SB_Session::getVar('cashbox_id');
			$diff		= $calculated - $cash;
			$data = compact('cashier_id', 'store_id', 'cashbox_id', 'sales', 'spends', 'calculated', 'cash', 'diff', 'data');
			$data['creation_date'] = date('Y-m-d H:i:s');
			if( $cashbox_id )
			{
				$id = SB_Factory::getDbh()->Insert('mb_cashcount', $data);
				$res = array('status' => 'ok', 
						'message' => __('The cashcount has been registered', 'mb'), 
						'print_link' => SB_Route::_('index.php?mod=mb&view=pos.printcashcount&id='.$id));
			}
			else
			{
				$res = array('status' => 'error', 
							'error' => __('An error ocurred while trying to register the cashcount. The cashbox has not been opened', 'mb')
				);
			}
		}
		elseif( $action == 'open_cashbox' )
		{
			$amount = SB_Request::getFloat('amount', 0);
			$notes	= SB_Request::getString('notes');
			$cdate	= date('Y-m-d');
			//##check for open cashbox
			$query = "SELECT * FROM mb_cashbox WHERE cashier_id = $user->user_id AND date(today) = '$cdate' LIMIT 1";
			if( $row = $this->dbh->FetchRow($query) )
			{
				$res = array('status' => 'error', 'error' => __('The cashbox is already open', 'mb'));
			}
			else
			{
				$data = array(
						'cashier_id'		=> $user->user_id,
						'today'				=> date('Y-m-d H:i:s'),
						'initial_amount'	=> $amount,
						'final_amount'		=> 0,
						'notes'				=> $notes,
						'status'			=> 'open'
				);
				$cashbox_id = $this->dbh->Insert('mb_cashbox', $data);
				SB_Session::setVar('cashbox_id', $cashbox_id);
				$res = array('status' => 'ok', 'message' => __('Cashbox opening was successful.', 'mb'));
			}
		}
		sb_response_json($res);
	}
	public function task_get_layout()
	{
        $user       = sb_get_current_user();
		$store_id   = SB_Request::getInt('store_id');
		$layout     = SB_Request::getString('layout');
		$layout_file = MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'pos_' . $layout . '_layout.php';
		if( !$store_id )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('The store is invalid', 'mb')));
		}
		if( !file_exists($layout_file) )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('The pos layout does not exists', 'mb'), 'file' => $layout_file));
		}
		$products = SB_Warehouse::getStoreProducts($store_id);
		ob_start();
		require_once $layout_file;
		$res = array('status' => 'ok', 'html' => ob_get_clean());
		sb_response_json($res);
	}
	public function task_register_sale()
	{
		if( !sb_is_user_logged_in() )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You need to start a session', 'mb')));
		}
		if( !sb_get_current_user()->can('mb_sell') )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You dont have enough permission to complete this operation', 'mb')));
		}
		/*
		$store_id 		= SB_Request::getInt('store_id');
		$nit			= SB_Request::getString('nit_ruc_nif');
		$customer 		= SB_Request::getString('customer');
		$customer_id 	= SB_Request::getInt('customer_id');
		$products 		= (array)SB_Request::getVar('products');
		$tax_percent	= SB_Request::getFloat('tax_percent');
		$notes			= SB_Request::getString('notes');
		$payment_status = SB_Request::getString('payment_status', 'complete');
		*/
		
		$data = $this->request->getVars(array(
			'int:store_id',
			'nit_ruc_nif',
			'customer',
			'int:customer_id',
			'array:products',
			'float:tax_percent',
			'notes',
			'payment_status'
		));
				
		if( !is_array($data['products']) || empty($data['products']) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('You need to select items for the order.', 'mb')));
		}
		if( empty($data['customer']) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('You need to enter a customer name', 'mb')));
		}
        $sale = new MB_Order();
        $sale->store_id     = $data['store_id'];
        $sale->customer_id  = $data['customer_id'];
        $sale->tax_percent  = $data['tax_percent'];
        $sale->details      = trim($data['notes']);
        $sale->items        = array();
        foreach($data['products'] as $item)
        {
            $sale->items[] = (object)array(
                'product_id'    => $item['id'],
                'name'          => $item['name'],
                'quantity'      => $item['qty'],
                'price'         => $item['price'],
                'subtotal'      => $item['qty'] * $item['price'],
                'total'         => $item['qty'] * $item['price']
            );
        }
        if( $sale->customer_id )
        {
            $sale->customer = Customer::Get($sale->customer_id);
        }
        else
        {
            $cnames = explode(' ', $data['customer']); 
            $lnames = '';
            if( isset($cnames[1]) )
            {
                for($_i = 1; $_i < count($cnames); $_i++)
                {
                    $lnames .= "{$cnames[$_i]} ";
                }
            }
            $sale->customer = new Customer();
            $sale->customer->first_name = $cnames[0];
            $sale->customer->last_name  = $lnames;
        }
        $sale->customer->meta[] = array('meta_key' => '_nit_ruc_nif', 'meta_value' => $data['nit_ruc_nif']);
		
        try
        {
            $settings = (object)sb_get_parameter('mb_settings', array());
            $this->dbh->BeginTransaction();
            $sale = $this->posModel->RegisterSale($sale, $customer_created);
            $this->dbh->EndTransaction();
            $res = array('status' 				=> 'ok', 
                            'message' 			=> __('The sale has been registered', 'mb'), 
                            'order_id' 			=> $sale->order_id,
                            'customer_created' 	=> $customer_created,
                            'customer_id'		=> $sale->customer_id,
                            //'customer_query'	=> $c_query,
                            //'print_url'			=> $this->Route('index.php?mod=mb&task=orders.print_receipt&id='.$sale->order_id)
                            'print_url'         => $this->Route('index.php?mod=mb&view=pos.print&id='.$sale->order_id),
                            'printing_type'     => $settings->pos->printing_type,
                            'silent_print'      => $settings->pos->silent_print,
                            'receipt_type'      => $settings->pos->receipt_type
            );
            
            $res = SB_Module::do_action('mb_pos_sale_response', $res);
            sb_response_json($res);
        }
		catch(Exception $e)
        {
            $this->dbh->Query('ROLLBACK;');
            sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
        }
	}
	public function task_get_products()
	{
		$store_id	= SB_Request::getInt('store_id');
		$page 		= SB_Request::getInt('page');
		$products 	= SB_Warehouse::getStoreProducts($store_id, $page);
		$html = '';
		ob_start();
		$i = 1;
		foreach($products as $p)
		{
			include MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'product_row.php';
			$i++;
		}
		$html = ob_get_clean();
		sb_response_json(array('status' => 'ok', 'items' => $html));
	}
	public function task_search_product()
	{
		$store_id		= SB_Request::getInt('store_id');
		$category_id 	= SB_Request::getInt('category_id');
		$keyword 		= SB_Request::getString('keyword');
		$limit_items 	= 50;//defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25;
		$prefix		= null;
		$limit = '';
		if( !empty($keyword) )
		{
			$prefix = 'name'; 
		}
		else
		{
			$limit = "LIMIT $limit_items";
		}
		//##check if keyword has prefix
		if( strstr($keyword, '::') )
		{
			$parts = explode('::', $keyword);
			$prefix = $parts[0];
			$keyword = isset($parts[1]) ? trim($parts[1]) : '';
		}
		
		$query = "SELECT p.* 
					FROM mb_products p ";
		if( $category_id > 0 )
		{
			$query .= ", mb_product2category p2c ";
		}
		$query .= "WHERE 1 = 1 ";
		if( $category_id > 0 )
		{
			$query .= "AND p.product_id = p2c.product_id AND p2c.category_id = $category_id "; 
		}
		$query .= "AND p.store_id = $store_id ";
		
		if( $prefix == __('id', 'mb') )
		{
			$keyword = (int)$keyword;
			$query .= "AND product_id = $keyword ";
		}
		elseif( $prefix == __('code', 'mb') )
		{
			$query .= "AND product_code LIKE '%$keyword%' ";
		}
		else
		{
			$query .= "AND (product_code LIKE '%$keyword%' OR product_name LIKE '%$keyword%') ";
		}
					
		$query .= "ORDER BY creation_date DESC $limit";
		
		$products = $this->dbh->FetchResults($query);
		$tpl = $this->request->getString('layout', 'list') == 'list' ? MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'product_row.php' : 
														MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'grid_item.php';
		ob_start();
		$i = 1;
		foreach($products as $row)
		{
			$p = new SB_MBProduct();
			$p->SetDbData($row);
			include $tpl;
			$i++;
		}
		$html = ob_get_clean() . '<div class="clearfix"></div>';
		sb_response_json(array('status' => 'ok', 'items' => $html, 'total' => count($products)));
		
	}
	public function pos_js_locale($globals)
	{
		$globals['modules']['mb']['locale'] = array(
				'CUSTOMER_BLOCKED'				=> __('The customer is blocked', 'mb'),
				'CUSTOMER_HAS_PENDING_PAYMENTS' => __('The customer has pending payments', 'mb'),
				'CUSTOMER_IS_BLOCKED'			=> __('The customer is blocked', 'mb'),
				'NEED_TO_SELECT_STORE'			=> __('You need to select a store', 'mb'),
				'THE_PRODUCT_HAS_NO_PRICE'		=> __('The product has no price or is cero', 'mb'),
				'NIT_RUC_NIF_EMPTY'				=> __('You need to enter a NIT for customer', 'mb'),
				'ERROR_CUSTOMER_EMPTY'			=> __('You need to enter a customer name', 'mb'),
				'ERROR_INVOICE_NAME_EMPTY'		=> __('You need to enter the invoice name', 'mb'),
				'ERROR_SELECT_STORE'			=> __('You need to select a store', 'mb'),
				'ERROR_SELECT_SALE_TYPE'		=> __('You need to select a sale type', 'mb'),
				'ERROR_PRODUCT_WO_STOCK'		=> __('The product has no enough stock for sale', 'mb'),
                'SALE_SAVED'                    => __('The sale has been saved', 'mb')
		);
		return $globals;
	}
	public function task_retail()
	{
		SB_Module::add_action('lt_js_globals', array($this, 'pos_js_locale'));
		$user = sb_get_current_user();
		if( !$user->can('mb_sell') )
		{
			lt_die(__('You dont have enough permission to sell products', 'mb'));
		}
		
		$this->_stores = SB_Warehouse::GetUserStores($user);
		$title = __('Sales', 'mb');
		sb_set_view_var('user', $user);
		$this->document->SetTitle($title);
	}
	public function task_printcashcount()
	{
		$id = SB_Request::getInt('id');
		$query = "SELECT * FROM mb_cashcount WHERE id = $id LIMIT 1";
		$cashcount = $this->dbh->FetchRow($query);
		$cashcount->data = json_decode($cashcount->data);
		//##get sales
		$query = "SELECT * FROM mb_orders 
					WHERE DATE(order_date) = '".sb_format_date($cashcount->creation_date, 'Y-m-d')."' 
					AND status = 'complete'
					ORDER BY order_date ASC";
		$sales = $this->dbh->FetchResults($query);
		//##get expensed
		$query = "SELECT * FROM mb_payments WHERE user_id = $cashcount->cashier_id 
					AND DATE(payment_date) = '".sb_format_date($cashcount->creation_date, 'Y-m-d')."'
					ORDER BY payment_date ASC";
		$expensed = $this->dbh->FetchResults($query);
		ob_start();
		include MOD_MB_DIR . SB_DS . 'templates/cashbox.php';
		$html = ob_get_clean();
		$pdf = mb_get_pdf_instance('', '', 'dompdf');
		$pdf->loadHtml($html);
		$pdf->render();
		$pdf->stream(sprintf(__('cashcount-%d.pdf', 'mb'), $cashcount->id),
				array('Attachment' => 0, 'Accept-Ranges' => 1));
		die();
	}
    public function task_print()
    {
        $id = $this->request->getInt('id');
        
        try
        {
            if( !$id )
                throw new Exception($this->__('Invalid sale identifier'));
            
            $order = new SB_MBOrder($id);
            if( !$order->order_id )
            {
                throw new Exception(__('The order does not exists', 'mb'));
            }
            $receipt = $this->posModel->GetReceipt($order, $type);
            if( $type == 'browser' )
            {
                print $receipt;
                die();
            }
            elseif( $type == 'pdf' )
            {
                $pdfName = sprintf(__('pos-sale-receipt-%d-%s', 'mb'), $order->sequence, $customerName);
                $pdfName = 'pos-sale-receipt';
                $pdf->stream($pdfName, array('Attachment' => 0));
                die();
            }
            die($receipt);
        }
        catch(Exception $e)
        {
            print_r($e->getTraceAsString());
            lt_die($e->getMessage());
            SB_MessagesStack::AddError($e->getMessage());
            sb_redirect($this->Route('index.php?mod=mb&view=orders.default'));
        }
    }
    public function ajax_savedraft()
    {
        
		$data = $this->request->getVars(array(
			'int:store_id',
			'nit_ruc_nif',
			'customer',
			'int:customer_id',
			'array:products',
			'float:tax_percent',
			'notes',
			'payment_status'
		));
        try
        {
            if( !sb_get_current_user()->can('mb_sell') )
            {
                sb_response_json(array('status' => 'error', 'error' => SBText::_('You dont have enough permission to complete this operation', 'mb')));
            }
            if( !is_array($data['products']) || empty($data['products']) )
            {
                throw new Exception(__('You need to select items for the order.', 'mb'));
            }
            
            sb_response_json(array('status' => 'ok', 'message' => $this->__('Not implemented yet')));
            
            $sale = new MB_Order();
            $sale->store_id     = $data['store_id'];
            $sale->customer_id  = $data['customer_id'];
            $sale->tax_percent  = $data['tax_percent'];
            $sale->items        = array();
            foreach($data['products'] as $item)
            {
                $sale->items[] = (object)array(
                    'product_id'    => $item['id'],
                    'name'          => $item['name'],
                    'quantity'      => $item['qty'],
                    'price'         => $item['price'],
                    'subtotal'      => $item['qty'] * $item['price'],
                    'total'         => $item['qty'] * $item['price']
                );
            }
            //##save draft without customers
            $sale->customer_id = null;
            $settings = (object)sb_get_parameter('mb_settings', array());
            $sale->status = MB_Order::STATUS_DRAFT;
            $sale = $this->posModel->RegisterSale($sale, $customer_created);
            $res = array('status' 				=> 'ok', 
                            'message' 			=> __('The sale has been saved as draft', 'mb'), 
                            'draft_id' 			=> $sale->order_id,
                            'customer_created' 	=> $customer_created,
                            'customer_id'		=> $sale->customer_id
            );
            $res = SB_Module::do_action('mb_pos_sale_response', $res);
            sb_response_json($res);
        }
		catch(Exception $e)
        {
            sb_response_json(array('status' => 'error', 'error' => 'POS_DRAFT_ERROR: ' . $e->getMessage()));
        }
    }
}
