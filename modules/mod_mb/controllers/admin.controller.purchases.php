<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Session;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_PurchaseOrder;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Store;

class LT_AdminControllerMbPurchases extends SB_Controller
{
	/**
	 * @namespace \SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\PurchaseModel
	 */
	protected $purchaseModel;
    /**
	 * @namespace \SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\TaxModel
	 */
    protected $taxModel;
    /**
	 * @namespace \SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel
	 */
	protected $productModel;
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\StoresModel
	 */
	protected $storesModel;
	
	public function task_default()
	{
		$table = $this->purchaseModel->GetListingTable();
		$title = __('Purchase Orders', 'mb');
		$this->SetView('purchases/default');
		$this->SetVars(get_defined_vars());
		
		$this->document->SetTitle($title);
	}
	public function task_new()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_can_create_purchase') )
		{
			lt_die(__('You dont have enough permission to create purchases', 'mb'));
		}
		if( !$user->IsRoot() && !$user->can('mb_see_all_stores') && !(int)$user->_store_id )
		{
			lt_die(__('You dont have any store assigned', 'mb'));
		}
		$user_store = $user->IsRoot() ? null : new SB_MBStore($user->_store_id);
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_MB_URL . '/js/purchase.new.js', 'sb-purchase', 0, true);
		$title = __('New Purchase Order', 'mb');
		if( $post = SB_Session::getVar('request_post') )
		{
			//set post data
			
		}
		$model = array(
			'key_field'	=> 'product_hash',
			'columns' => array(
				array('label' => __('Num', 'mb'), 'class' => '', 'type' => 'count'),
				array('label' => __('ID', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_id', 'show' => false),
				array('label' => __('Code', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_code'),
				array('label' => __('Product', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_name'),
				array('label' => __('Current Quantity', 'mb'), 'class' => 'bg-success text-center', 'type' => 'static', 'value_key' => 'stock'),
				array('label' => __('Quantity', 'mb'), 'class' => '', 'type' => 'number', 'value_key' => 'quantity'),
				array('label' => __('Price', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'supply_price'),
				array('label' => __('Discount', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'discount'),
				array('label' => __('Total', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'total'),
				array('label' => __('Actions', 'mb'), 'class' => '', 'type' => 'buttons', 
						'buttons' => array(
							array(
								'title' 	=> __('Remove', 'mb'),
                                'label'     => '<span class="glyphicon glyphicon-trash"></span>',
								'callback'	=> 'mb_purchase.DeleteItem',
								'data'		=> array('product_id')
							)
						)
				)
			),
			'OnAddItem' => 'mb_purchase.OnAddItem',
			'OnChange'	=> 'mb_purchase.OnChange'
		);
		$excelColumns = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		SB_Module::do_action_ref('mb_purchase_table_model', $model);
		$stores = SB_Warehouse::GetUserStores($user);
		$ttypes = SB_Warehouse::GetTransactionTypes('in');
        $taxes  = $this->taxModel->GetAll();
		$this->SetView('purchases/new');
		$this->SetVars(get_defined_vars());
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_can_edit_purchase') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permission to complete this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$user_store = $user->IsRoot() ? null : new SB_MBStore($user->_store_id);
		$id = SB_Request::getInt('id');
		$purchase = new SB_MBPurchase($id);
		$items = $purchase->GetItems();
		$title = __('Edit Purchase Order', 'mb');
		$model = array(
			'key_field'	=> 'product_hash',
			'columns' => array(
				array('label' => __('Num', 'mb'), 'class' => '', 'type' => 'count'),
				array('label' => __('ID', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_id', 'show' => !false),
				array('label' => __('Code', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_code'),
				array('label' => __('Product', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_name'),
				array('label' => __('Quantity', 'mb'), 'class' => '', 'type' => 'number', 'value_key' => 'quantity'),
				array('label' => __('Price', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'supply_price'),
				array('label' => __('Discount', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'discount'),
				array('label' => __('Total', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'total'),
				array('label' => __('Actions', 'mb'), 'class' => '', 'type' => 'buttons', 
						'buttons' => array(
							array(
								'title' 	=> __('Remove', 'mb'),
                                'label'     => '<span class="glyphicon glyphicon-trash"></span>',
								'callback'	=> 'mb_purchase.DeleteItem',
								'data'		=> array('product_id')
							)
						)
				)
			),
			'OnAddItem' => 'mb_purchase.OnAddItem',
			'OnChange'	=> 'mb_purchase.OnChange'
		);
		SB_Module::do_action_ref('mb_purchase_table_model', $model);
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_MB_URL . '/js/purchase.new.js', 'sb-purchase', 0, true);
		$warehouses = [];
		if( $purchase->store_id )
			$warehouses = $this->storesModel->GetWarehouses($purchase->store_id);
			
		$stores 	= SB_Warehouse::GetUserStores($user);
		$ttypes 	= SB_Warehouse::GetTransactionTypes('in');
		
        $taxes      = $this->taxModel->GetAll();
        $excelColumns = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$this->SetView('purchases/new');
		$this->SetVars(get_defined_vars());
		$this->document->SetTitle($title);
		SB_Module::do_action_ref('mb_purchase_edit_before_show', $this);
	}
	public function task_save()
	{
		if( !sb_get_current_user()->can('mb_can_create_purchase') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permission to complete this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$ajax					= $this->request->getInt('ajax');
		$order_id 				= $this->request->getInt('purchase_id');
		$supplier_id			= $this->request->getInt('supplier_id');
		$supplier_name			= $this->request->getString('supplier_name');
		$store_id				= $this->request->getInt('store_id');
		$warehouse_id			= $this->request->getInt('warehouse_id');
		$transaction_type_id	= $this->request->getInt('transaction_type_id');
		$order_date				= $this->request->getDate('order_date');
		$delivery_date			= $this->request->getDate('delivery_date');
		$details				= $this->request->getString('notes');
        $tax_id                 = $this->request->getInt('tax_id');
        $tax_rate               = $this->request->getFloat('tax_rate');
        
		$store = MB_Store::Get($store_id);
		$transaction_type_id = $transaction_type_id > 0 ? $transaction_type_id : (int)$store->GetMeta('_purchase_tt_id');
		//print_r($store);die();
		if( !(int)$transaction_type_id )
		{
			$error = __('The store has no purchases transaction assigned', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $error));
			}
			SB_Request::Save('purchase_data');
			SB_MessagesStack::AddMessage($error);
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.new'));
			return false;
		}
		$purchase 						= new MB_PurchaseOrder();
		$purchase->order_id 			= $order_id;
		$purchase->store_id				= $store_id;
		$purchase->store				= $store;
		$purchase->warehouse_id			= $warehouse_id;
		$purchase->supplier_id			= $supplier_id;
		$purchase->transaction_type_id 	= $transaction_type_id;
		$purchase->tax_id				= $tax_id;
		$purchase->tax_rate				= $tax_rate;
		$purchase->items				= 0;
		$purchase->details				= $details;
		$purchase->order_date			= $order_date;
		$purchase->delivery_date		= $delivery_date;
		
		$updated = false;
		if( !$order_id )
		{
			$link = SB_Route::_('index.php?mod=mb&view=purchases.edit&id=');
			$msg = __('The purchase order has been registered', 'mb');
		}
		else
		{
			$link = SB_Route::_('index.php?mod=mb&view=purchases.edit&id=');
			$msg = __('The purchase order has been updated', 'mb');
			$updated = true;
		}
        try
        {
            $this->dbh->BeginTransaction();
            $order = $this->purchaseModel->Save($purchase, $this->request->getArray('items'), $this->request->getArray('meta'));
            $this->dbh->EndTransaction();
            if( $ajax )
            {
                sb_response_json(array('status' => 'ok', 'message' => $msg, 'order' => $order, 'updated' => $order_id ? true : false));
            }
            SB_MessagesStack::AddMessage($msg, 'success');
            sb_redirect($link);
        }
        catch(Exception $e)
        {
            $this->dbh->Rollback();
            if( $ajax )
            {
                sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
            }
            SB_MessagesStack::AddMessage($e->getMessage(), 'error');
        }
		
	}
	public function task_receive()
	{
		$user = sb_get_current_user();
        $ajax = $this->request->getInt('ajax');
        $id 		= SB_Request::getInt('id');
        try
        {
            if( !$user->can('mb_can_receive_stock') )
                throw new Exception(__('You dont have enough permission to complete this operation', 'mb'));
            if( !$id )
                throw new Exception(__('Invalid purchase order identifier', 'mb'));
            $user_store = $user->IsRoot() ? null : new SB_MBStore($user->_store_id);
            $purchase = new SB_MBPurchase($id);
            if( !$purchase->order_id )
                throw new Exception(__('The purchase order does not exists', 'mb'));
            //##get purchase items
            $items = $purchase->GetItems();
            $this->SetView('purchases/receive');
            //$this->SetView('purchases.new');
            $title = __('Receive Purchase Order', 'mb');
            $model = array(
                'key_field'	=> 'product_hash',
                'columns' => array(
                    array('label' => __('Num', 'mb'), 'class' => '', 'type' => 'count'),
                    array('label' => __('ID', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_id', 'show' => !false),
                    array('label' => __('Code', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_code'),
                    array('label' => __('Product', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'product_name'),
                    array('label' => __('Quantity', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'quantity'),
                    array('label' => __('Received', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'received'),
                    array('label' => __('Receive Qty', 'mb'), 'class' => '', 'type' => 'number', 'value_key' => 'quantity_received'),
                    array('label' => __('Price', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'supply_price'),
                    array('label' => __('Discount', 'mb'), 'class' => '', 'type' => 'text', 'value_key' => 'discount'),
                    array('label' => __('Total', 'mb'), 'class' => '', 'type' => 'static', 'value_key' => 'total'),
                    /*
                    array('label' => __('Actions', 'mb'), 'class' => '', 'type' => 'buttons', 
                            'buttons' => array(
                                array(
                                    'label' 	=> __('Remove', 'mb'),
                                    'callback'	=> 'mb_purchase.DeleteItem',
                                    'data'		=> array('product_id')
                                )
                            )
                    )
                    */
                ),
                //'OnAddItem' => 'mb_purchase.OnAddItem',
                'OnChange'	=> 'mb_purchase.OnChange'
            );
            SB_Module::do_action_ref('mb_receive_purchase_table_model', $model);
            sb_add_script(BASEURL . '/js/table-model.js', 'sb-table-model', 0, true);
            sb_add_script(MOD_MB_URL . '/js/purchase-receive.js', 'sb-purchase', 0, true);
            $this->SetVars(get_defined_vars());
            $this->document->SetTitle($title);
            SB_Module::do_action_ref('mb_purchase_receive_before_show', $this);
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddMessage($e->getMessage(), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
        }
	}
	public function task_do_receive()
	{
		$user		= sb_get_current_user();
		$id 		= $this->request->getInt('purchase_id');
		$ajax 		= $this->request->getInt('ajax');
		$req_items 	= SB_Request::getArray('items');
		
		try
		{
            if( !$user->can('mb_can_receive_stock') )
                throw new Exception(__('You dont have enough permission to complete this operation', 'mb'));
			if( !$id )
				throw new Exception(__('Invalid purchase identifier', 'mb'));
			$purchase = new SB_MBPurchase($id);
			if( !$purchase->order_id )
				throw new Exception(__('The purchase order does not exists', 'mb'));
			
			if( !count($req_items) )
				throw new Exception(__('There are no items to receive, please mark the items you want to receive', 'mb'));
            $this->dbh->BeginTransaction();
			$updated_purchase = $this->purchaseModel->Receive($purchase, $req_items);
            $this->dbh->EndTransaction();
			$msg = __('The purchase order has been received', 'mb');
			SB_MessagesStack::AddMessage($msg, 'success');
			if( $ajax )
			{
				//$purchase->orderItems = null;
				sb_response_json(array('status' => 'ok', 'message' => $msg, 'items' => $updated_purchase->GetItems()));
			}
			
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.edit&id=' . $updated_purchase->order_id));
		}
		catch(Exception $e)
		{
            $this->dbh->Rollback();
			if( $ajax )
				sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
			SB_MessagesStack::AddMessage($error, 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
	}
	public function ___task_receive()
	{
		if( !sb_get_current_user()->can('mb_can_receive_stock') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permission to complete this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		set_time_limit(0);
		$id = SB_Request::getInt('id');
		$purchase = new SB_MBPurchase($id);
		if( !$purchase->order_id )
		{
			SB_MessagesStack::AddMessage(__('The purchase order does not exists.', 'mb'), 'error');
			sb_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		if( strtoupper($purchase->status) == MB_PurchaseOrder::STATUS_COMPLETED )
		{
			SB_MessagesStack::AddMessage(__('The purchase order is already completed.', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$purchase->UpdateStock(sb_get_current_user()->user_id);
		
		//##call hooks
		SB_Module::do_action('mb_purchase_order_receive', $purchase);
		SB_MessagesStack::AddMessage(__('The purchase order has been completed', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.edit&id='.$purchase->order_id));
	}
	public function task_print()
	{
		if( !sb_get_current_user()->can('mb_can_print_purchase') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permission to complete this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$id 		= SB_Request::getInt('id');
		$purchase 	= new SB_MBPurchase($id);
		if( !$purchase->order_id )
		{
			SB_MessagesStack::AddMessage(__('The purchase order does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$tpl = SB_Module::do_action('mb_purchase_order_note_tpl', MOD_MB_DIR . SB_DS . 'templates' . SB_DS . 'purchase.order.php');
		$ops = (object)sb_get_parameter('mb_settings', array());
		//print_r($ops);
		ob_start();
		include $tpl;
		$html = ob_get_clean();
		if( SB_Request::getInt('pdf') )
		{
			$pdf = mb_get_pdf_instance('', '', 'dompdf');
			$pdf->loadHtml($html);
			$pdf->render();
			$pdf->stream(sprintf(__('purchase-order-%d-%d.pdf', 'mb'), $purchase->order_id, $purchase->sequence),
					array('Attachment' => 0, 'Accept-Ranges' => 1));
			die();
		}
		$title = __('Print Purchase Order', 'mb');
		$this->document->SetTitle($title);
		sb_set_view('print.preview');
		sb_set_view_var('purchase', $purchase);
		sb_set_view_var('back_link', SB_Route::_('index.php?mod=mb&view=purchases.edit&id='.$purchase->order_id));
		sb_set_view_var('iframe_url', SB_Route::_('index.php?mod=mb&view=purchases.print&id='.$purchase->order_id.'&pdf=1'));
	}
	public function task_delete()
	{
		if( !sb_get_current_user()->can('mb_can_delete_purchase') )
		{
			SB_MessagesStack::AddMessage(__('You dont have enough permission to complete this operation', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		$id = SB_Request::getInt('id');
		$purchase = new SB_MBPurchase($id);
		if( !$purchase->order_id )
		{
			SB_MessagesStack::AddMessage(__('The purchase order does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		if( strtoupper($purchase->status) == MB_PurchaseOrder::STATUS_COMPLETED )
		{
			SB_MessagesStack::AddMessage(__('You cant delete this order', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
		}
		/*
		$this->dbh->Delete('mb_purchase_order_items', array('order_id' => $purchase->order_id));
		$this->dbh->Delete('mb_purchase_orders', array('order_id' => $purchase->order_id));
		*/
		$this->dbh->Update('mb_purchase_orders', 
							array('status' => MB_PurchaseOrder::STATUS_DELETED), 
							array('order_id' => $purchase->order_id));
		SB_Module::do_action('mb_purchase_order_deleted', $purchase);
		SB_MessagesStack::AddMessage(__('The purchase order has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=purchases.default'));
	}
	public function task_importexcel()
	{
		$mimes = array(
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.ms-excel',
			'application/vnd.ms-office',
			'application/wps-office.xls',
			'application/wps-office.xlsx'
		);
		$data = $this->request->ToJSON();
		$row_start = isset($data->row_start) ? (int)$data->row_start : 1;
		if( $row_start <= 0 )
			$row_start = 1;
		//print_r($data);
		try
		{
			if( !$data->store_id )
				throw new Exception(__('You need to select a store', 'mb'));
			if( strlen(trim($data->code)) <= 0 || (int)$data->code < 0 )
				throw new Exception(__('Invalid column code', 'mb'));
			if( strlen(trim($data->quantity)) <= 0 || (int)$data->quantity < 0 )
				throw new Exception(__('Invalid column quantity', 'mb'));
			if( strlen(trim($data->cost)) <= 0 || (int)$data->cost < 0 )
				throw new Exception(__('Invalid column cost', 'mb'));
			$filename = isset($data->filename) ? $data->filename : 'purchase-import.xlsx';
			//##write temp file
			$excelFile = sb_get_unique_filename($filename, TEMP_DIR);
			file_put_contents($excelFile, base64_decode($data->buffer));
			
			//$mime = sb_get_file_mime($excelFile);
			
			if( !in_array($data->mime, $mimes) )
			{
				unlink($excelFile);
				throw new Exception(__('Invalid excel file, ' . $mime, 'mb'));
			}
				
			sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
			sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
			$xls 			= \PHPExcel_IOFactory::load($excelFile);
			$sheet 			= $xls->setActiveSheetIndex(0 /*$data->sheet_num*/);
			$total_rows 	= $sheet->getHighestRow();
			$total_cols 	= $sheet->getHighestColumn();
			$totalExists 	= 0;
			$totalNotExists = 0;
			$items 			= array();
			$noProducts 	= [];
			$i = 0;
			for($row = $row_start; $row <= $total_rows; $row++)
			{
				$code		= trim($sheet->getCellByColumnAndRow((int)$data->code, $row)->getCalculatedValue());
				$barcode	= (int)$data->barcode > -1 ? 
								trim($sheet->getCellByColumnAndRow((int)$data->barcode, $row)->getCalculatedValue()) : null;
				$quantity 	= (int)trim($sheet->getCellByColumnAndRow((int)$data->quantity, $row)->getCalculatedValue());
				$cost		= (float)trim($sheet->getCellByColumnAndRow((int)$data->cost, $row)->getCalculatedValue());
				$name		= isset($data->name) && (int)$data->name >= 0 ? 
								trim($sheet->getCellByColumnAndRow((int)$data->name, $row)->getCalculatedValue()) : ''; 
				if( empty($code) )
				{
					continue;
				}
				//##check if product already exists
				$exists 	= false;
				$product	= $this->productModel->GetBy(['product_code' => $code, 'store_id' => $data->store_id]);
				if( $product )
				{
					$exists = true;
					$totalExists++;
					$items[] 	= array(
						'product_id' 		=> $product->product_id ? $product->product_id : ($i * -1),
						'product_code' 		=> $code, 
						'product_name'		=> $product ? $product->product_name : $name,
						'product_barcode' 	=> $barcode, 
						'quantity' 			=> $quantity, 
						'product_cost' 		=> $cost, 
						'exists' 			=> $exists,
						'product_quantity'	=> $product->product_quantity
					);
				}
				else
				{
					$noProducts[] = [
						'code' 		=> $code, 
						'barcode' 	=> $barcode, 
						'quantity' 	=> $quantity, 
						'cost' 		=> $cost,
						'name'		=> $name
					];
					$totalNotExists++;
				}
				$i++;
			}
			$noProductsLink = null;
			if( count($noProducts) )
			{
				$filename_hash = md5(sprintf("purchase-no-products-%d", time()));
				$filename = sprintf("%s.json", $filename_hash);
				$filename = sb_get_unique_filename($filename, TEMP_DIR);
				file_put_contents($filename, json_encode($noProducts));
				$noProductsLink = $this->Route('index.php?mod=mb&task=purchases.download_no_products&f='.basename($filename));
			}
			sb_response_json(array(
				'status' 			=> 'ok', 
				'items' 			=> $items, 
				'noProducts'		=> $noProducts,
				'noProductsLink'	=> $noProductsLink,
				'confirm_message'	=> __('There are products that were not imported, do you want to download them?', 'mb')
			));
		}
		catch(Exception $e)
		{
			sb_response_json(array('status' => 'error', 'error' => $e->getMessage()) );
		}
		die();
	}
	public function task_download_no_products()
	{
		$f = $this->request->getString('f');
		if( !$f )
			throw new Exception(__('Invalid filename', 'mb'));
		$filename = TEMP_DIR . SB_DS . $f;
		if( !is_file($filename) )
			throw new Exception(__('The file does not exists', 'mb'));
		$items = json_decode(file_get_contents($filename));
		$excelFilename = __('purchase-products.xlsx', 'mb');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel/IOFactory.php');
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
		$xls = new PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		$row = 1;
		$sheet->setCellValue("A$row", __('Num', 'mb'));
		$sheet->setCellValue("B$row", __('Code', 'mb'));
		$sheet->setCellValue("C$row", __('Barcode', 'mb'));
		$sheet->setCellValue("D$row", __('Product', 'mb'));
		$sheet->setCellValue("E$row", __('Quantity', 'mb'));
		$sheet->setCellValue("F$row", __('Cost', 'mb'));
		$row++;
		$i = 1;
		foreach($items as $item)
		{
			$sheet->setCellValue("A$row", $i);
			$sheet->setCellValue("B$row", $item->code);
			$sheet->setCellValue("C$row", $item->barcode);
			$sheet->setCellValue("D$row", $item->name);
			$sheet->setCellValue("E$row", $item->quantity);
			$sheet->setCellValue("F$row", $item->cost);
			$row++;
			$i++;
		}
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$excelFilename.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
	public function task_printlabels()
	{
		$id 			= $this->request->getInt('id');
		$cellsNum		= $this->request->getInt('columns', 5);
		$printBarcode	= $this->request->getInt('print_barcode');
		$printPrice		= $this->request->getInt('print_price');
		try
		{
			if( !$id )
				throw new Exception(__('Invalid purchase order identifier', 'mb'));
			$purchase = new SB_MBPurchase($id);
			if( !$purchase->order_id )
				throw new Exception(__('The purchase order does not exists', 'mb'));
			//##get purchase products
			$products 	= $this->purchaseModel->GetOrderProducts($purchase);
			for($i = 0; $i < count($products); $i++)
			{
				$product = &$products[$i];
				foreach($purchase->GetItems() as $item)
				{
					if( $item->product_id == $product->product_id )
					{
						$product->product_quantity = $item->quantity_received;
						break;
					}
				}
			}
			
			$pdf 		= $this->productModel->BuildLabels($products, $cellsNum, $printBarcode, $printPrice);
			$pdf->Output(sprintf(__('products-labels-%s.pdf'), sb_format_date(date('Y-m-d'))), 'I');
			die();
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($this->Route('index.php?mod=mb&view=purchases.default'));
		}
	}
	public function task_revert()
	{
		$id = $this->request->getInt('id');
		$backlink = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->Route('index.php?mod=mb&view=purchases.default');
		try
		{
			if( !$id )
				throw new Exception(__('The purchase order identifier is invalid', 'mb'));
			$purchase = new SB_MBPurchase($id);
			if( !$purchase || !$purchase->order_id )
				throw new Exception(__('The purchase order does not exists', 'mb'));
			$this->dbh->BeginTransaction();
			$this->purchaseModel->Revert($purchase);
			$this->dbh->EndTransaction();
			SB_MessagesStack::AddSuccess(__('The purchase order has been reverted', 'mb'));
			sb_redirect($backlink);
		}
		catch(Exception $e)
		{
			$this->dbh->Rollback();
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($backlink);
		}
	}
}
