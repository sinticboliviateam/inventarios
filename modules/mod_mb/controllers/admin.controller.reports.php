<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Request;

class LT_AdminControllerMbReports extends SB_Controller
{
	protected	$reports = array();
	protected	$instances = array();
	
	public function __construct($doc = null)
	{
		parent::__construct($doc);
		$this->reports = SB_Module::do_action('mb_reports', array(
				'stock' => array('class' => 'SB_MBReportStock', 'path' => null),
				'sales' => array('class' => 'SB_MBReportSales', 'path' => null),
				'purchases' => array('class' => 'SB_MBReportPurchases', 'path' => null)
		));
		foreach($this->reports as $key => $report)
		{
			$path = isset($report['path']) && $report['path'] ? $report['path'] : MOD_MB_DIR . SB_DS . 'classes' . SB_DS . 'reports';  
			$class_file = $path . SB_DS . 'report.' . $key . '.php';
			if( file_exists($class_file) )
				include $class_file;
			if( class_exists($report['class']) )
			{
				$class = $report['class'];
				$this->instances[$key] = new $class();
			}
		}
	}
	public function task_default()
	{
		$report	= SB_Request::getString('report', 'stock');
		$from 	= SB_Request::getString('from');
		$to		= SB_Request::getString('to');
		$tab 	= SB_Request::getString('tab');
		sb_set_view_var('report', $report);
		sb_set_view_var('tab', $tab);
		//print_r($this->instances);		
		$obj = null;
		if( $tab && isset($this->instances[$tab]) )
		{
			$obj = $this->instances[$tab];
		}
		elseif( $report && isset($this->instances[$report]) )
		{
			$obj = $this->instances[$report];
		}
		
		sb_set_view_var('obj', $obj);
	}
}