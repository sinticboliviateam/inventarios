<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;

class LT_AdminControllerMbStores extends SB_Controller
{
	/*protected $models = array(
			'StoresModel'
	);
	*/
	/**
	 * @var \SinticBolivia\SBFramework\Modules\Mb\Models\StoresModel
	 */
	protected	$storesModel;
	
	public function task_default()
	{
		
		$query = "SELECT * FROM mb_stores ORDER BY creation_date DESC";
		$this->dbh->Query($query);
		$stores = array();
		foreach($this->dbh->FetchResults() as $row)
		{
			$store = new SB_MBStore();
			$store->SetDbData($row);
			$stores[] = $store;
		}
		
		sb_set_view_var('stores', $stores);
	}
	public function task_new()
	{
		$ttypes = SB_Warehouse::GetTransactionTypes();
		sb_set_view_var('ttypes', $ttypes);
		sb_set_view_var('page_title', SBText::_('Create New Store', 'mb'));
	}
	public function task_edit()
	{
		$id = $this->request->getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The store identifier is invalid'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=stores.default'));
		}
		$ttypes = SB_Warehouse::GetTransactionTypes();
		$store = new SB_MBStore($id);
		sb_set_view('stores.new');
		sb_set_view_var('ttypes', $ttypes);
		sb_set_view_var('page_title', $this->__('Edit Store', 'mb'));
		sb_set_view_var('the_store', $store);
	}
	public function task_save()
	{
		$id 			= $this->request->getInt('store_id');
        $code           = $this->request->getString('code');
		$store_name 	= $this->request->getString('store_name');
		$store_address 	= $this->request->getString('store_address');
		$phone 			= $this->request->getString('store_phone');
		$data 			= compact('code', 'store_name', 'store_address', 'phone');
		$meta			= (array)$this->request->getVar('meta', array());
		if( $id )
			$data['store_id'] = $id;
		
		$store_id = $this->storesModel->Save($data, $meta);
		if( !$id )
		{
			$link 	= $this->Route('index.php?mod=mb&view=stores.default');
			$msg 	= $this->__('The store has been created', 'mb');
		}
		else
		{
			$link 	= $this->Route('index.php?mod=mb&view=stores.edit&id='.$id);
			$msg	= $this->__('The store has been updated', 'mb');
		}
		
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The store identifier is invalid'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=stores.default'));
		}
		$store = new SB_MBStore($id);
		if( !$store->store_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The store identifier does not exists'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=stores.default'));
		}
		$dbh = SB_Factory::getDbh();
		$query = "DELETE FROM mb_store_meta WHERE store_id = $store->store_id";
		$dbh->Query($query);
		$query = "DELETE FROM mb_stores WHERE store_id = $store->store_id";
		$dbh->Query($query);
		SB_MessagesStack::AddMessage(SBText::_('The store has been deleted'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=stores.default'));
	}
}
