<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Modules\Mb\Classes\SB_MBTax;

class LT_AdminControllerMbTaxes extends SB_Controller
{
	public function task_default()
	{
		$query = "SELECT * FROM mb_tax_rates ORDER BY creation_date";
		$taxes = $this->dbh->FetchResults($query);
		sb_set_view_var('taxes', $taxes);
		$this->document->SetTitle(__('Taxes', 'mb'));
	}
	public function task_new()
	{
		$title = __('Create New Tax', 'mb');
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid tax indentifier', 'mb'), 'error');
			sb_redirect($this->Route('index.php?mod=mb&view=taxes.default'));
		}
		sb_set_view('taxes.new');
		$tax = new SB_MBTax($id);
		if( !$tax->tax_id )
		{
			SB_MessagesStack::AddMessage(__('The tax does not exists', 'mb'), 'error');
			sb_redirect($this->Route('index.php?mod=mb&view=taxes.default'));
		}
		sb_set_view_var('tax', $tax);
		$title = __('Edit Tax', 'mb');
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		$id 		= SB_Request::getInt('tax_id');
		$code 		= SB_Request::getString('code');
		$name 		= SB_Request::getString('tax_name');
		$rate 		= SB_Request::getFloat('percent');
		$is_default = SB_Request::getInt('is_default');
		
		if( empty($code) )
		{
			SB_MessagesStack::AddMessage(__('You need to enter a tax code', 'mb'), 'error');
			$id ? $this->task_edit() : $this->task_new();
			return false;
		}
		if( empty($name) )
		{
			SB_MessagesStack::AddMessage(__('You need to enter a tax name', 'mb'), 'error');
			$id ? $this->task_edit() : $this->task_new();
			return false;
		}
		$data = compact('code', 'name', 'rate', 'is_default');
		if( $id )
		{
			$this->dbh->Update('mb_tax_rates', $data, array('tax_id' => $id));
			SB_MessagesStack::AddMessage(__('The tax rate has been updated', 'mb'), 'success');
		}
		else
		{
			$id = $this->dbh->Insert('mb_tax_rates', $data);
			SB_MessagesStack::AddMessage(__('The tax rate has been created', 'mb'), 'success');
		}
		sb_redirect($this->Route('index.php?mod=mb&view=taxes.default'));
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		$this->dbh->Delete('mb_tax_rates', array('tax_id' => $id));
		SB_MessagesStack::AddMessage(__('The tax has been deleted.', 'mb'), 'success');
		sb_redirect($this->Route('index.php?mod=mb&view=taxes.default'));
	}
}
