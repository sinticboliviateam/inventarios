<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Modules\Mb\Models\TransferModel;

class LT_AdminControllerMbTransfers extends SB_Controller
{
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
	 * @var TransferModel
	 */
	protected $transferModel;
	
	public function task_default()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_manage_transfers') )
		{
			lt_die(__('You are not authorized to view the information', 'mb'));
		}
		$user_store_id = (int)$user->_store_id;
		if( !$user->IsRoot() && !$user_store_id )
		{
			lt_die(__('You dont have any store assigned to your profile, contact with your administrator', 'mb'));
		}
		
		$query = "SELECT t.*,".
					"(SELECT store_name FROM mb_stores fs WHERE fs.store_id = t.from_store) AS from_store_name,".
					"(SELECT store_name FROM mb_stores ts WHERE ts.store_id = t.to_store) AS to_store_name, ".
					"(SELECT name FROM mb_warehouse fw WHERE fw.id = t.from_warehouse) AS from_warehouse,".
					"(SELECT name FROM mb_warehouse tw WHERE tw.id = t.to_warehouse) AS to_warehouse ".
					"FROM mb_transfers t " . 
					"WHERE 1 = 1 ";
		
		if( !$user->can('mb_view_all_transfers') )
		{
			$query .= "AND (t.from_store = $user_store_id OR t.to_store = $user_store_id) ";
		}
		$query .= "ORDER BY creation_date DESC";
		$this->_transfers = $this->dbh->FetchResults($query);
		$this->_statuses	= mb_get_transfer_statuses();
		sb_set_view_var('user', $user);
	}
	public function task_new()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_create_transfer') )
		{
			lt_die(__('You are not authorized to create transfers', 'mb'));
		}
		$this->_stores = SB_Warehouse::getStores();
		$this->_title	= __('New Transfer', 'mb');
		sb_add_script(BASEURL . '/js/sb-completion.js', 'completion-js');
		//sb_add_script(MOD_MB_URL . '/js/transfer.new.js', 'transfer-js');
		sb_add_js_global_var('mb', 'locale', array(
				'SOURCE_WAREHOUSE_NEEDED'	=> __('You need to select a warehouse source', 'mb'),
				'DESTINATION_WAREHOUSE_NEEDED'	=> __('You need to select the destination warehouse', 'mb'),
				'TRANSFER_SUCCESS'				=> __('The transfer has been registered', 'mb')
		));
		sb_set_view_var('user', $user);
		$this->document->SetTitle($this->title);
	}
	public function task_importnew()
	{
		$store_id		= SB_Request::getInt('store_id');
		$col_code 		= SB_Request::getInt('col_code', 0);
		$col_name		= SB_Request::getInt('col_name', 1);
		$col_batch		= SB_Request::getInt('col_batch', 2);
		$col_expdate	= SB_Request::getInt('col_expdate', 3);
		$col_qty		= SB_Request::getInt('col_qty', 4);
		$sheet_num		= SB_Request::getInt('sheet_num', 1);
		$row_start		= SB_Request::getInt('row_start', 2);
		
		if( $sheet_num < 0 )
			$sheet_num = 0;
		else
			$sheet_num--;
		if( !$store_id )
		{
			SB_MessagesStack::AddMessage(__('You need to select a source store', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
		sb_include_lib('php-office/PHPExcel-1.8/PHPExcel.php');
		$xls 		= PHPExcel_IOFactory::load($_FILES['the_excel']['tmp_name']);
		$sheet 		= $xls->setActiveSheetIndex($sheet_num);
		$total_rows = $sheet->getHighestRow();
		$total_cols = $sheet->getHighestColumn();
		SB_Session::setVar('transfer_items', array());
		$items =& SB_Session::getVar('transfer_items');
		//var_dump($total_rows);die();
		
		for($row = $row_start; $row <= $total_rows; $row++)
		{
			$product_code 	= trim($sheet->getCellByColumnAndRow($col_code, $row)->getCalculatedValue());
			$product_name 	= trim($sheet->getCellByColumnAndRow($col_name, $row)->getCalculatedValue());
			$product_batch 	= trim($sheet->getCellByColumnAndRow($col_batch, $row)->getCalculatedValue());
			$product_qty 	= (int)trim($sheet->getCellByColumnAndRow($col_qty, $row)->getCalculatedValue());
			//##check if product exists into store
			$query = "SELECT p.*, p2b.batch, p2b.quantity as batch_qty 
						FROM mb_products p, mb_product2batch p2b 
						WHERE 1 = 1
						AND p.product_id = p2b.product_id
						AND p2b.batch = '$product_batch'
						AND p2b.quantity >= $product_qty
						AND p.product_code = '$product_code' 
						AND p.store_id = $store_id 
						LIMIT 1";
			$_row = $this->dbh->FetchRow($query);
			if( !$_row ) 
			{
				continue;
			}
			
			$items[] = (object)array(
					'product_id'	=> $_row->product_id,
					'product_code' 	=> $_row->product_code,
					'product_name'	=> $_row->product_name,
					'quantity'		=> $product_qty,
					'batch'			=> $product_batch
			);
		}
		sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		die();
	}
	public function task_save()
	{
        $currentUser = sb_get_current_user();
		if( !$currentUser->can('mb_create_transfer') )
		{
			lt_die(__('You dont have enough permissions to do transfers', 'mb'));
		}
        
		$data = new SB_MBTransfer();
        $data->from_store       = $this->request->getInt('from_store_id');
        $data->from_warehouse   = $this->request->getInt('from_warehouse_id');
        $data->to_store         = $this->request->getInt('to_store_id');
        $data->to_warehouse     = $this->request->getInt('to_warehouse_id');
        $data->from_lot         = $this->request->getInt('from_lot');
        $data->to_lot           = $this->request->getInt('to_lot');
        $data->details          = $this->request->getString('notes');
        $data->user_id          = $currentUser->user_id;
		$ajax                   = $this->request->getInt('ajax');
		$data->items            = $this->request->getArray('items');
        $data->receiver_id      = 0;
        
		if( !$data->from_store )
		{
			$msg = __('The source store is invalid', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $msg));
			}
			SB_MessagesStack::AddMessage($msg, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
		if( !$data->from_warehouse )
		{
			$msg = __('The source warehouse is invalid', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $msg));
			}
			SB_MessagesStack::AddMessage($msg, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
		if( !$data->to_store )
		{
			$msg = __('The target store is invalid', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $msg));
			}
			SB_MessagesStack::AddMessage($msg, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
		if( !$data->to_warehouse || $data->from_warehouse == $data->to_warehouse )
		{
			$msg = __('The target warehouse is invalid', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $msg));
			}
			SB_MessagesStack::AddMessage($msg, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
		if( !$data->items || !count($data->items) )
		{
			$msg = __('You need to add items to transfer', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $msg));
			}
			SB_MessagesStack::AddMessage($msg, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
	
		try
		{
			$transfer = $this->transferModel->Create($data);
			$msg = __('The transfer has been registered', 'mb');
			if( $ajax )
			{
				sb_response_json(array('status' => 'ok', 
						'message' 		=> $msg, 
						'transfer_id' 	=> $transfer->id, 
						'redirect' 		=> $this->Route('index.php?mod=mb&view=transfers.default')
				));
			}
			SB_MessagesStack::AddMessage($msg, 'success');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
		}
		catch(Exception $e)
		{
			if( $ajax )
			{
				sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
			}
			SB_MessagesStack::AddMessage($error, 'error');
			SB_Request::Save('new_transfer_data');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.new'));
		}
	}
	public function task_view()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_view_its_transfers') )
		{
			lt_die(__('You dont have enough permission to receive or view transfers', 'mb'));
		}
		$user_store_id = (int)$user->_store_id;
		if( !$user->IsRoot() && !$user_store_id )
		{
			lt_die(__('You dont have any store assigned to your profile, contact with your administrator', 'mb'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The transfer identifier is invalid', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
		}
		$transfer = $this->transferModel->Get($id);
        
		if( !$transfer->id )
		{
			SB_MessagesStack::AddMessage(__('The transfer does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
		}
		if( !$user->can('mb_view_all_transfers') )
		{
			if( $transfer->from_store != $user_store_id && $transfer->to_store != $user_store_id )
			{
				lt_die(__('The transfer does not belongs to your assigned store', 'mb'));
			}
		}
		sb_set_view_var('user', $user);
		sb_set_view_var('transfer', $transfer);
	}
	public function task_accept()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_receive_transfer') )
		{
			lt_die(__('You dont have enough permissions to accept transfers', 'mb'));
		}
		$id = $this->request->getInt('id');
        try
        {
            if( !$id )
            {
                throw new Exception(__('The transfer identifier is invalid', 'mb'));
            }
            $transfer = $this->transferModel->Get($id);
            if( !$transfer || !$transfer->id )
            {
                throw new Exception(__('The transfer does not exists', 'mb'));
            }
            $user_store_id = (int)$user->_store_id;
            if( !$user->IsRoot() && !$user_store_id )
            {
                throw new Exception(__('You dont have any store assigned to your profile, contact with your administrator', 'mb'));
            }
            if( !$user->can('mb_view_all_transfers') )
            {
                if( $transfer->from_store != $user_store_id && $transfer->to_store != $user_store_id )
                {
                    throw new Exception(__('The transfer does not belongs to your assigned store', 'mb'));
                }
            }
            if( $transfer->status == 'complete' )
            {
                SB_MessagesStack::AddMessage(__('The transfer is already completed', 'mb'), 'info');
                sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
            }
            $this->transferModel->Accept($transfer);
            //$transfer->AcceptTransfer($user->user_id);
            SB_MessagesStack::AddMessage(__('The transfer has been accepted', 'mb'), 'success');
            sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
        catch(Exception $e)
        {
           SB_MessagesStack::AddError(__('The transfer identifier is invalid', 'mb'));
           sb_redirect($this->Route('index.php?mod=mb&view=transfers.default'));
        }
	}
	public function task_delete()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_delete_transfer') )
		{
			lt_die(__('You dont have enough permission to complete this operation', 'mb'));
		}
		
		$id = SB_Request::getInt('id');
        try
        {
            if( !$id )
            {
                throw new Exception(__('The transfer identifier is invalid', 'mb'));
            }
            $this->transferModel->Delete($id);
            
            SB_MessagesStack::AddMessage(__('The transfer has been deleted', 'mb'), 'success');
            sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddError($e->getMessage());
            sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
	}
	public function task_revert()
	{
		$user = sb_get_current_user();
		if( !$user->can('mb_delete_transfer') )
		{
			lt_die(__('You dont have enough permission to complete this operation', 'mb'));
		}
		
		$id = SB_Request::getInt('id');
        try
        {
            if( !$id )
            {
                throw new Exception(__('The transfer identifier is invalid', 'mb'));
            }
            $transfer = $this->transferModel->Get($id);
            if( !$transfer || !$transfer->id )
            {
                throw new Exception(__('The transfer does not exists', 'mb'));
                
            }
            $this->transferModel->Revert($transfer);
            SB_MessagesStack::AddMessage(__('The transfer has been reverted', 'mb'), 'success');
            sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddError(__('The transfer identifier is invalid', 'mb'));
            sb_redirect($this->Route('index.php?mod=mb&view=transfers.default'));
        }
		
	}
	public function task_printoutput()
	{
		$user = sb_get_current_user();
		$id = SB_Request::getInt('id');
        try
        {
            if( !$id )
            {
                throw new Exception(__('The transfer identifier is invalid', 'mb'));
            }
            $transfer = $this->transferModel->Get($id);
            if( !$transfer || !$transfer->id )
            {
                throw new Exception(__('The transfer does not exists.', 'mb'));
            }
            $mb_settings = sb_get_parameter('mb_settings', array());
            $logo_url = null;
            if( isset($mb_settings->business_logo) && file_exists(UPLOADS_DIR . SB_DS . $mb_settings->business_logo) )
            {
                $logo_url = UPLOADS_URL . '/' . $mb_settings->business_logo;
            }
            $output_tpl = SB_Module::do_action('mb_transfer_output_tpl', MOD_MB_DIR . SB_DS . 'templates' . SB_DS . 'transfer.output.php');
            ob_start();
            include $output_tpl;
            $html = ob_get_clean();
            $pdf = mb_get_pdf_instance('', '', 'dompdf');
            $pdf->loadHtml($html);
            $pdf->render();
            $pdf->stream(sprintf(__('transfer-%d-%d.pdf', 'mb'), $transfer->from_store, 'K', $transfer->to_store),
                    array('Attachment' => 0, 'Accept-Ranges' => 1));
            die();
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddError($e->getMessage());
            sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
	}
	public function task_printinput()
	{
		$user = sb_get_current_user();
		$id = SB_Request::getInt('id');
		
        try
        {
            if( !$id )
            {
                throw new Exception(__('The transfer identifier is invalid', 'mb'));
            }
            $transfer = $this->transferModel->get($id);
            if( !$transfer || !$transfer->id )
            {
                throw new Exception(__('The transfer does not exists.', 'mb'));
            }
            $mb_settings = sb_get_parameter('mb_settings', array());
            $logo_url = null;
            if( isset($mb_settings->business_logo) && file_exists(UPLOADS_DIR . SB_DS . $mb_settings->business_logo) )
            {
                $logo_url = UPLOADS_URL . '/' . $mb_settings->business_logo;
            }
            $input_tpl = SB_Module::do_action('mb_transfer_input_tpl', MOD_MB_DIR . SB_DS . 'templates' . SB_DS . 'transfer.input.php');
            ob_start();
            include $input_tpl;
            $html = ob_get_clean();
            $pdf = mb_get_pdf_instance('', '', 'dompdf');
            $pdf->loadHtml($html);
            $pdf->render();
            $pdf->stream(sprintf(__('transfer-%d-%d.pdf', 'mb'), $transfer->from_store, 'K', $transfer->to_store),
                    array('Attachment' => 0, 'Accept-Ranges' => 1));
            die();
        }
        catch(Exception $e)
        {
            SB_MessagesStack::AddError($e->getMessage());
			sb_redirect(SB_Route::_('index.php?mod=mb&view=transfers.default'));
        }
		
		
	}
}