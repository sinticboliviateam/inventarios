<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Route;

class LT_AdminControllerMbTtypes extends SB_Controller
{
	public function task_default()
	{
		$this->dbh->Select('*')
				->From('mb_transaction_types')
				->OrderBy('transaction_name', 'asc');
		$this->dbh->Query(null);
		$this->_items = $this->dbh->FetchResults();
		
	}
	public function task_new()
	{
		$this->_title = __('New Transaction Type', 'mb');
		$this->document->SetTitle($this->title);
		
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		$this->SetView('ttypes.new');
		$query = "SELECT * from mb_transaction_types WHERE transaction_type_id = $id LIMIT 1";
		$this->_tt = $this->dbh->FetchRow($query);
		if( !$this->tt )
		{
			SB_MessagesStack::AddMessage(__('The transaction type does not exists', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=ttypes.default'));
		}
		
		$this->_title = __('Edit Transaction Type', 'mb');
		$this->document->SetTitle($this->title);
	}
	public function task_save()
	{
		$id 						= SB_Request::getInt('id');
		$transaction_key 			= SB_Request::getString('transaction_key');
		$transaction_name 			= SB_Request::getString('name');
		$in_out 					= SB_Request::getString('in_out', 'in');
		$transaction_description 	= SB_Request::getString('description');
		$last_modification_date		= date('Y-m-d H:i:s');
		
		$data = compact('transaction_name', 'transaction_key', 'in_out', 'transaction_description', 'last_modification_date');
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$this->dbh->Insert('mb_transaction_types', $data);
		}
		else 
		{
			$this->dbh->Update('mb_transaction_types', $data, array('transaction_type_id' => $id));
		}
		SB_MessagesStack::AddMessage(__('The transaction type has been created', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=ttypes.default'));
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The transaction type id is invalid', 'mb'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=ttypes.default'));
		}
		$this->dbh->Delete('mb_transaction_types', array('transaction_type_id' => $id));
		SB_MessagesStack::AddMessage(__('The transaction type has been deleted', 'mb'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=ttypes.default'));
	}
}