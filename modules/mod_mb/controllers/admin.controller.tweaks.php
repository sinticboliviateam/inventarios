<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;

class LT_AdminControllerMbTweaks extends SB_Controller
{
	public function task_default()
	{
		$user 		= sb_get_current_user();
		$store_id 	= SB_Request::getInt('store_id');
		$limit 		= SB_Request::getInt('limit', 25);
		$page 		= SB_Request::getInt('page', 1);
		$tipo		= SB_Request::getString('tipo_ajuste', '-1');
		$desde		= SB_Request::getDate('desde', null);
		$hasta		= SB_Request::getDate('hasta', null);
		if( $page <= 0 )
			$page = 1;
		$this->dbh->Select('COUNT(*) AS total_rows')->From('mb_tweaks t, mb_stores s')->Where(null);
		$this->dbh->Join(array('t.store_id' => 's.store_id'));
		if( $store_id )
		{
			$this->dbh->SqlAND(array("s.store_id" => $store_id));
		}
		if( $tipo != '-1' )
		{
			$this->dbh->SqlAND(array('tipo' => $tipo));
		}
		if( $desde )
		{
			$this->dbh->SqlAND(array('transaction_date' => $desde), '>=');
		}
		if( $hasta )
		{
			$this->dbh->SqlAND(array('transaction_date' => $hasta), '<=');
		}
		//var_dump($this->dbh->builtQuery);
		$this->dbh->Query(null);
		$total_rows = (int)$this->dbh->FetchRow()->total_rows;
		$pages		= ceil($total_rows / $limit);
		$offset 	= $page == 1 ? 0 : ($page - 1) * $limit;
		$this->dbh->Select('t.*, s.store_name')->From('mb_tweaks t, mb_stores s')->Where(null);
		
		$this->dbh->Join(array('t.store_id' => 's.store_id'));
		if( $store_id )
		{
			$this->dbh->SqlAND(array("s.store_id" => $store_id));
		}
		if( $tipo != '-1' )
		{
			$this->dbh->SqlAND(array('tipo' => $tipo));
		}
		if( $desde )
		{
			$this->dbh->SqlAND(array('transaction_date' => $desde), '>=');
		}
		if( $hasta )
		{
			$this->dbh->SqlAND(array('transaction_date' => $hasta), '<=');
		}
		$this->dbh->OrderBy('transaction_date');
		$this->dbh->Limit($limit, $offset);
		//var_dump($this->dbh->builtQuery);
		$this->dbh->Query(null);
		$items = $this->dbh->FetchResults();
		$stores = SB_Warehouse::GetUserStores($user);
		sb_set_view_var('stores', $stores);
		sb_set_view_var('tipos', mb_get_tweaks_types());
		sb_set_view_var('items', $items);
		sb_set_view_var('total_pages', $pages);
		sb_set_view_var('current_page', $page);
	}
	public function task_new()
	{
		$user 	= sb_get_current_user();
		$stores = SB_Warehouse::GetUserStores($user);
		$title 	= __('Registrar Nuevo Ajuste de Inventario', 'ceass');
		sb_set_view_var('causas', mb_get_tweak_causes());
		sb_set_view_var('stores', $stores);
		sb_set_view_var('tipos', mb_get_tweaks_types());
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$user = sb_get_current_user();
		$title = __('Editar Ajuste de Inventario', 'ceass');
		$stores = SB_Warehouse::GetUserStores($user);
		sb_set_view('ajustes.nuevo');
		sb_set_view_var('causas', mb_get_tweak_causes());
		sb_set_view_var('stores', $stores);
		sb_set_view_var('tipos', mb_get_tweaks_types());
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_ver()
	{
		$id = SB_Request::getInt('id');
		$user = sb_get_current_user();
		$title = __('Ajuste de Inventario', 'ceass');
		$stores = SB_Warehouse::GetUserStores($user);
		$ajuste	= new MB_Tweak($id);
		sb_set_view_var('ajuste', $ajuste);
		sb_set_view_var('stores', $stores);
		sb_set_view_var('tipos', mb_get_tweaks_types());
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_delete()
	{
		$user = sb_get_current_user();
	}
	public function task_register()
	{
		$user			= sb_get_current_user();
		$store_id 		= SB_Request::getInt('store_id');
		$products		= SB_Request::getVar('product');
		$details		= SB_Request::getString('details');
		$tipo_ajuste	= SB_Request::getString('tipo_ajuste');
		$causa			= SB_Request::getString('causa');
		$status			= 'complete';
		
		$store = new SB_MBStore($store_id);
		$items = array();
		$subtotal = 0;
		foreach($products as $p)
		{
			$total 		= (int)$p['qty'] * (float)$p['costo'];
			$subtotal 	+= $total;
			$items[] = array(
					'product_id'		=> $p['id'],
					'product_code'		=> $p['codigo'],
					'quantity'			=> $p['qty'],
					'cost'				=> $p['costo'],
					'total'				=> $total,
					'creation_date'		=> date('Y-m-d H:i:s')
			);
		}
		$sequence 	= mb_tweak_get_next_sequence($store_id, 
									$tipo_ajuste == 'positivo' ? (int)$store->_ajuste_positivo_id : (int)$store->_ajuste_negativo_id, 
									$tipo_ajuste);
		$total 		= $subtotal;
		$cdate 		= date('Y-m-d H:i:s');
		$data 		= array(
				'store_id'				=> $store_id,
				'transaction_type_id'	=> $tipo_ajuste == 'positivo' ? $store->_ajuste_positivo_id : $store->_ajuste_negativo_id,
				'user_id'				=> $user->user_id,
				'sequence'				=> $sequence,
				'tipo'					=> $tipo_ajuste,
				'causa'					=> $causa,
				'details'				=> $details,
				'subtotal'				=> $subtotal,
				'total'					=> $total,
				'status'				=> $status,
				'transaction_date'		=> $cdate,
				'creation_date'			=> $cdate
		);
		$id = mb_tweak_insert($data, $items);
		SB_MessagesStack::AddMessage(__('El ajuste fue registrado correctamente', 'ceass'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=tweaks.default'));
		die();
	}
	public function task_print()
	{
		$id = SB_Request::getInt('id');
		$ajuste = new MB_Tweak($id);
		if( !$ajuste->id )
		{
			SB_MessagesStack::AddMessage('El ajuste no existe', 'error');
			sb_redirect('index.php?mod=mb&view=tweaks.default');
		}
		$settings 	= sb_get_parameter('mb_settings');
		$logo 		= isset($settings->business_logo) ? UPLOADS_DIR . SB_DS . $settings->business_logo : null;
		$logo_url = null;
		if( file_exists($logo) )
			$logo_url = UPLOADS_URL . '/' . $settings->business_logo;
		$causas = mb_get_tweak_causes();
		$pdf = mb_get_pdf_instance('', '', 'dompdf');
		ob_start();
		include MOD_MB_DIR . SB_DS . 'templates' . SB_DS . 'tweak.php';
		$tpl = ob_get_clean();
		$pdf->loadHtml($tpl);
		$pdf->render();
		$pdf->stream(sprintf(__('tweak-%d-%d.pdf', 'mb'), $ajuste->id, $ajuste->sequence),
				array('Attachment' => 0, 'Accept-Ranges' => 1));
		die();
	}
}