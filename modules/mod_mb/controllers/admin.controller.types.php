<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;

class LT_AdminControllerMbTypes extends SB_Controller
{
	public function task_default()
	{
		$store_id = SB_Request::getInt('store_id');
		
		$query = "SELECT {columns} FROM mb_product_types pt";
		$columns = array('*');
		$columns[] = "(SELECT store_name FROM mb_stores s WHERE s.store_id = pt.store_id) AS store_name";
		$where = '';
		if( $store_id )
		{
			
			$where = "store_id = $store_id";
		}
		if( !empty($where) )
		{
			$query = "$query $where";
		}
		$query = str_replace('{columns}', implode(',', $columns), $query);
		$this->dbh->Query($query);
		$types = $this->dbh->FetchResults();
		sb_set_view_var('types', $types);
		sb_set_view_var('stores', SB_Warehouse::getStores());
	}
	public function task_new()
	{
		sb_set_view_var('title', SBText::_('Create new product type', 'mb'));
		sb_set_view_var('stores', SB_Warehouse::getStores());
		//
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		
		$query = "SELECT pt.*, (SELECT store_name FROM mb_stores s WHERE s.store_id = pt.store_id) AS store_name ".
					"FROM mb_product_types pt WHERE type_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		$type = $this->dbh->FetchRow();
		sb_set_view('types.new');
		sb_set_view_var('title', SBText::_('Edit product type', 'mb'));
		sb_set_view_var('stores', SB_Warehouse::getStores());
		sb_set_view_var('type', $type);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier is invalid.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		
		$query = "SELECT pt.*, (SELECT store_name FROM mb_stores s WHERE s.store_id = pt.store_id) AS store_name ".
				"FROM mb_product_types pt WHERE type_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier does not exists.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		$query = "DELETE FROM mb_product_types WHERE type_id = $id";
		$this->dbh->Query($query);
		SB_MessagesStack::AddMessage(SBText::_('The product type has been deleted.'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
	}
	public function task_save()
	{
		$id 			= SB_Request::getInt('id');
		$type 			= SB_Request::getString('type');
		$code 			= SB_Request::getString('code');
		$description 	= SB_Request::getString('description');
		$store_id		= SB_Request::getInt('store_id');
		$data = compact('type', 'code', 'description', 'store_id');
		$dbh = SB_Factory::getDbh();
		$msg = $link = null;
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $dbh->Insert('mb_product_types', $data);
			$msg = SBText::_('The product type has been created', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=types.default');
		}
		else
		{
			$dbh->Update('mb_product_types', $data, array('type_id' => $id));
			$msg = SBText::_('The product type has been updates', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=types.edit&id='.$id);
		}
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
}