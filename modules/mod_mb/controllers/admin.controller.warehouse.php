<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;

class LT_AdminControllerMbWarehouse extends SB_Controller
{
	public function task_default()
	{
		$this->dbh->Select('w.*, s.store_name')
				->From('mb_warehouse w')
				->LeftJoin('mb_stores s', 's.store_id = w.store_id')
				->OrderBy('creation_date', 'desc');
		$this->dbh->Query(null);
		$items = $this->dbh->FetchResults();
		sb_set_view_var('items', $items);
	}
	public function task_new()
	{
		sb_set_view_var('stores', SB_Warehouse::getStores());
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=types.default'));
		}
		$query = "SELECT * FROM mb_warehouse WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			sb_redirect(SB_Route::_('index.php?mod=mb&view=warehouse.default'));
		}
		$w = $this->dbh->FetchRow();
		sb_set_view('warehouse.new');
		sb_set_view_var('title', SBText::_('Edit product type', 'mb'));
		sb_set_view_var('stores', SB_Warehouse::getStores());
		sb_set_view_var('w', $w);
	}
	public function task_delete()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier is invalid.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=warehouse.default'));
		}
		$query = "SELECT * FROM mb_warehouse WHERE id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(SBText::_('The indentifier does not exists.'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=mb&view=warehouse.default'));
		}
		$query = "DELETE FROM mb_warehouse WHERE id = $id LIMIT 1";
		$this->dbh->Query($query);
		SB_MessagesStack::AddMessage(SBText::_('The warehouse has been deleted.'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=mb&view=warehouse.default'));
	}
	public function task_save()
	{
		$id 			= SB_Request::getInt('id');
		$name 			= SB_Request::getString('name');
		$address		= SB_Request::getString('address');
		$store_id		= SB_Request::getInt('store_id');
		
		$data 			= compact('name', 'address', 'store_id');
		SB_Module::do_action_ref('mb_warehouse_before_save', $data);
		$msg = $link = null;
		if( !$id )
		{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$id = $this->dbh->Insert('mb_warehouse', $data);
			$msg = __('The warehouse has been created', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=warehouse.default');
		}
		else
		{
			$this->dbh->Update('mb_warehouse', $data, array('id' => $id));
			$msg = __('The warehouse has been updated', 'mb');
			$link = SB_Route::_('index.php?mod=mb&view=warehouse.edit&id='.$id);
		}
		SB_Module::do_action('mb_warehouse_save', $id, $data);
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
}