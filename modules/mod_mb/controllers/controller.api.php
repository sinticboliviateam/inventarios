<?php
sb_include('class.api-rest.php');
use SinticBolivia\SBFramework\Classes\SB_Controller;

class LT_ControllerMbApi extends SB_Controller
{
	protected $version = '1.0.0';
	
	public function task_default()
	{
		http_response_code(200);
		sb_response_json(array('status' => 'ok', 
				'code' => 200, 
				'message' => __('MonoBussiness API REST', 'mb'),
				'version'	=> $this->version
		));
	}
	public function task_products()
	{
		require_once MOD_MB_CLASSES_DIR . SB_DS . 'api' . SB_DS . $this->version . SB_DS . 'api.products.php';
		$api = new MB_APIProducts();
		$api->Start();
		$api->HandleRequest();
	}
	public function task_transfers()
	{
		require_once MOD_MB_CLASSES_DIR . SB_DS . 'api' . SB_DS . $this->version . SB_DS . 'api.transfers.php';
		$api = new MB_API_Transfers();
		$api->Start();
		$api->HandleRequest();
	}
}