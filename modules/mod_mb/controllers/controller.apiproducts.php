<?php
use SinticBolivia\SBFramework\Classes\SB_ApiRest;
use SinticBolivia\SBFramework\Modules\Mb\Models\ProductModel;
use SinticBolivia\SBFramework\Classes\SB_ApiRestException;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Product;

//class Modules_Mb_Controllers_ApiProducts extends SB_ApiRest
class LT_ControllerMbApiProducts extends SB_ApiRest
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Mb\Models
     * @var ProductModel 
     */
    protected $productModel;
    /**
     * Count all products
     * @method GET
     */
    public function TaskCount()
    {
        $total = $this->dbh->GetVar("SELECT COUNT(product_id) FROM mb_products");
        $this->Response(array('total' => $total));
    }
    /**
     * Get all products
     * 
     * @method GET
     */
	public function TaskShow()
    {
        $storeId = $this->request->getInt('storeId');
        $conds = array();
        if( $storeId )
        {
            $conds = array('store_id' => $storeId);
            //throw new SB_ApiRestException($this->__('Invalid store identifier'), $this);
        }
        
        $products = MB_Product::GetRows(-1, 0, $conds);
        $this->Response($products);
    }
    /**
     *
     * @method GET
     */
    public function TaskGet()
    {
        $id = $this->request->getInt('id');
        if( !$id )
            throw new SB_ApiRestException($this->__('Invalid product identifier'), $this);
        $product = MB_Product::Get($id);
        
        $this->Response($product);
    }
    /**
     * @method DELETE
     */
    public function TaskDelete()
    {
        
    }
}
