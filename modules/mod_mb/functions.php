<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Database\Classes\SB_DbRow;

function mb_get_product_meta($id, $meta_key)
{
	return SB_Meta::getMeta('mb_product_meta', $meta_key, 'product_id', $id);
}
function mb_add_product_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::addMeta('mb_product_meta', $meta_key, $meta_value, 'product_id', $id);
}
function mb_update_product_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::updateMeta('mb_product_meta', $meta_key, $meta_value, 'product_id', $id);
}
function mb_delete_product_meta($id, $meta_key)
{
	return SB_Meta::deleteMeta('mb_product_meta', $meta_key, 'product_id', $id);
}
function mb_get_store_meta($id, $meta_key)
{
	return SB_Meta::getMeta('mb_store_meta', $meta_key, 'store_id', $id);
}
function mb_add_store_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::addMeta('mb_store_meta', $meta_key, $meta_value, 'store_id', $id);
}
function mb_update_store_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::updateMeta('mb_store_meta', $meta_key, $meta_value, 'store_id', $id);
}
function mb_delete_store_meta($id, $meta_key)
{
	return SB_Meta::deleteMeta('mb_store_meta', $meta_key, 'store_id', $id);
}
function mb_get_order_meta($id, $meta_key)
{
	return SB_Meta::getMeta('mb_order_meta', $meta_key, 'order_id', $id);
}
function mb_add_order_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::addMeta('mb_order_meta', $meta_key, $meta_value, 'order_id', $id);
}
function mb_update_order_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::updateMeta('mb_order_meta', $meta_key, $meta_value, 'order_id', $id);
}
function mb_delete_order_meta($id, $meta_key)
{
	return SB_Meta::deleteMeta('mb_order_meta', $meta_key, 'order_id', $id);
}
function mb_add_order_item_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::addMeta('mb_order_item_meta', $meta_key, $meta_value, 'item_id', $id);
}
function mb_update_order_item_meta($id, $meta_key, $meta_value)
{
	return SB_Meta::updateMeta('mb_order_item_meta', $meta_key, $meta_value, 'item_id', $id);
}
function mb_delete_order_item_meta($id, $meta_key)
{
	return SB_Meta::deleteMeta('mb_order_item_meta', $meta_key, 'item_id', $id);
}
function mb_get_order_item_meta($id, $meta_key)
{
	return SB_Meta::getMeta('mb_order_item_meta', $meta_key, 'item_id', $id);
}
/**
 * 
 * @param unknown $title
 * @param string $subject
 * @return TCPDF
 */
function mb_get_pdf_instance($title, $subject = '', $engine = 'tcpdf')
{
	$pdf = null;
	if( $engine == 'tcpdf' )
	{
		sb_include_lib('tcpdf/tcpdf.php');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator('MonoBusiness - Sintic Bolivia');
		$pdf->SetAuthor('J. Marcelo Aviles Paco');
		$pdf->SetTitle($title);
		$pdf->SetSubject($subject);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->SetFont('freesans', '', 9, '', true);
		// set margins
		$pdf->SetMargins(4, 5, 3);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->setPrintFooter(false);
		$pdf->setPrintHeader(false);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
	}
	elseif( $engine == 'dompdf' )
	{
		sb_include_lib('dompdf/dompdf.php');
		
		$pdf = new Dompdf\Dompdf();
		$pdf->set_option('defaultFont', 'Helvetica');
		$pdf->set_option('isRemoteEnabled', true);
		$pdf->set_option('isPhpEnabled', true);
		//$pdf->set_option('defaultPaperSize', 'Legal');
		// (Optional) Setup the paper size and orientation
		$pdf->setPaper('A4'/*, 'landscape'*/);
		
	}
	
	return $pdf;
}
/**
 * Function to page navigation
 * 
 * @param string $order_by
 * @param Object $class -> class module
 * @param array $columns
 * @param array $table
 * @param array $where is null
 * @return array[]
 */
function sb_get_navigation($order_by, $class, array $columns, array $table, array $where = NULL, $_limit = null, $order = 'DESC')
{
	$dbh		= SB_Factory::getDbh();
	$page       = SB_Request::getInt('page', 1 );
	$limit      = SB_Request::getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
	$query      = sprintf("SELECT {columns} FROM %s ",implode(',',$table));
	$search_by	= SB_Request::getString('search_by');
	$keyword	= SB_Request::getString('keyword');
	
	if( $_limit )
		$limit = $_limit;
	
	$_where		= "WHERE 1 = 1 ";
	if( is_array($where) )
	{
		foreach($where as $w)
		{
			$_where .= "AND $w ";
		}
		$_where = rtrim($_where, ' ');
	}
	if( $search_by && $keyword )
	{
		$_where .= " AND $search_by LIKE '%$keyword%'";
	}
	$query .= " $_where ";
	$order_query  =  "ORDER BY $order_by $order";
	//var_dump(str_replace('{columns}', 'COUNT(*) AS total_rows', $query));
	$dbh->Query(str_replace('{columns}', 'COUNT(*) AS total_rows', $query));

	$total_rows   =  $dbh->FetchRow()->total_rows;
	$total_pages  =  ceil($total_rows / $limit);
	$offset       =  ($page <= 1) ? 0 : ($page - 1) * $limit;
	$limit_query  =  "LIMIT $offset, $limit";

	$query = str_replace('{columns}', implode(',', $columns), $query) . " $order_query $limit_query";
	$dbh->Query($query);
	$arreglo = array();
	foreach($dbh->FetchResults() as $row)
	{
		$a = new $class();
		$a->SetDbData($row);
		$arreglo[] = $a;
	}
	sb_set_view_var('search_by', $search_by);
	sb_set_view_var('keyword', $keyword);
	sb_set_view_var('total_pages', $total_pages);
	sb_set_view_var('current_page', $page);
	return $arreglo;
}
/**
 * Get Available order statuses
 * 
 * @return Array
 */
function mb_get_order_statuses()
{
	$statuses = array(
			'pending' 		=> __('Pending', 'mb'),
			'processing' 	=> __('Processing', 'mb'),
			'on_the_way' 	=> __('On the way', 'mb'),
			'delivered' 	=> __('Delivered', 'mb'),
			'complete' 		=> __('Complete', 'mb'),
			'cancelled'		=> __('Cancelled', 'mb'),
			'waiting_stock'	=> __('Wating Stock', 'mb')
	);
	
	return b_do_action('mb_order_statuses', $statuses);
}
function mb_get_order_payment_statuses($keys_only = false)
{
	$statuses = array(
			'pending' 		=> __('Pending', 'mb'),
			'complete' 		=> __('Complete', 'mb'),
	);
	return SB_Module::do_action('mb_order_payment_statuses', $statuses);
}
function mb_get_transfer_statuses()
{
	$statuses = array(
			'pending' 		=> __('Pending', 'mb'),
			'on_the_way' 	=> __('On the way', 'mb'),
			'delivered' 	=> __('Delivered', 'mb'),
			'complete' 		=> __('Complete', 'mb'),
			'cancelled'		=> __('Cancelled', 'mb'),
			'reverted'		=> __('Reverted', 'mb')
	);
	
	return SB_Module::do_action('mb_transfer_statuses', $statuses);
}
/**
 * Get all available tax rates
 * 
 * @return array
 */
function mb_get_tax_rates()
{
	$dbh = SB_Factory::getDbh();
	$query = "SELECT * FROM mb_tax_rates WHERE 1 = 1 ORDER BY rate ASC";
	return $dbh->FetchResults($query);
}
/**
 * Get default tax rate
 * 
 * @return object
 */
function mb_get_default_tax()
{
	$dbh = SB_Factory::getDbh();
	$query = "SELECT * FROM mb_tax_rates WHERE is_default = 1 LIMIT 1";
	return $dbh->FetchRow($query);
}
/**
 * Register a new purchase order into database
 * 
 * @param array $data
 * @param array $items
 * @return int the purchase order id
 */
function mb_insert_purchase_order($data, $items)
{
	
}
function mb_get_order_next_sequence($store_id)
{
	$query = "select MAX(sequence) + 1 
				from mb_orders
				where store_id = $store_id";
	$seq = (int)SB_Factory::getDbh()->GetVar($query);
	if( $seq <= 0 )
		$seq = 1;
	return $seq;
}
function mb_get_tweaks_types()
{
	return array(
			(object)array('key' => 'positivo', 'nombre' => 'Positivo'), 
			(object)array('key' => 'negativo', 'nombre' => 'Negativo')
	);
}
function mb_get_tweak_causes()
{
	return SB_Module::do_action('mb_tweak_causes', array(
			(object)array('key' => 'prestamo_saldo', 'nombre' => 'Prestamo Saldo', 'tipo' => '-'),
			(object)array('key' => 'inventario_fisico', 'nombre' => 'Inventario Fisico', 'tipo' => '-'),
			(object)array('key' => 'error_registro', 'nombre' => 'Error de Registro', 'tipo' => '-'),
			(object)array('key' => 'expiracion', 'nombre' => 'Expiracion', 'tipo' => '-'),
			(object)array('key' => 'danado_fallido', 'nombre' => 'Dañados / Fallidos', 'tipo' => '-'),
			(object)array('key' => 'robo', 'nombre' => 'Robo', 'tipo' => '-'),
			(object)array('key' => 'perdida', 'nombre' => 'Perdida', 'tipo' => '-'),
			(object)array('key' => 'prestamo_ingresado', 'nombre' => 'Prestamo Ingresado', 'tipo' => '+'),
			(object)array('key' => 'inventario_fisico', 'nombre' => 'Inventario Fisico', 'tipo' => '+'),
			(object)array('key' => 'error_registro', 'nombre' => 'Error de Registro', 'tipo' => '+'),
			(object)array('key' => 'otra_fuente', 'nombre' => 'Recibido de Otra Fuente', 'tipo' => '+'),
			(object)array('key' => 'devolucion_prestamo', 'nombre' => 'Devolucion de Prestamo', 'tipo' => '+'),
			(object)array('key' => 'factura_anticipada', 'nombre' => 'Factura Anticipada', 'tipo' => '-'),
	));
}
function mb_get_cause($key)
{
	$res = null;
	foreach(mb_get_tweak_causes() as $causa)
	{
		if( $key == $causa->key )
		{
			$res = $causa;
			break;
		}
	}
	return $res;
}
function mb_tweak_get_next_sequence($store_id, $tt_id, $tipo)
{
	$year = date('Y');
	$query = "SELECT MAX(sequence) 
				FROM mb_tweaks 
				WHERE store_id = $store_id 
				AND transaction_type_id = $tt_id 
				AND tipo = '$tipo'
				AND YEAR(transaction_date) = '$year'
				LIMIT 1";
	$sequence = SB_Factory::getDbh()->GetVar($query);
	if( !$sequence )
		$sequence = 1;
	else
		$sequence++;
	
	return $sequence;
}
/**
 * Registra o actualiza una donacion generando un kardex de la transaccion y actualizando el stock de lotes
 * @param array $data
 * @param array $items
 * @return id|NULL	Retorna un id de la donacion o null en caso de que ocurra un error
 */
function mb_tweak_insert($data, $items)
{
	$dbh 	= SB_Factory::getDbh();
	$id		= null;
	$tt		= new SB_MBTransactionType((int)$data['transaction_type_id']);
	//die($tt->in_out);
	if( !isset($data['id']) || !$data['id'] )
	{
		$id = $dbh->Insert('mb_tweaks', $data);
		for($i = 0; $i < count($items); $i++)
		{
			$items[$i]['tweak_id'] = $id;
		}
		$dbh->InsertBulk('mb_tweak_items', $items);
		foreach ($items as $item)
		{
			//##verificar si es una salida
			if( $tt->in_out == 'out' )
			{
				//##reducir stock global
				$dbh->Update('mb_products',
						array('product_quantity' => "OP[product_quantity - {$item['quantity']}]"),
						array('product_id' => $item['product_id']));
				//##generar kardex del producto
				//##obtener cantidad y costo actual del producto
				$sq1 = "SELECT product_cost,product_quantity FROM mb_products WHERE product_id = {$item['product_id']} LIMIT 1";
				$row = $dbh->FetchRow($sq1);
				$kdata = array(
						'product_id'				=> $item['product_id'],
						'in_out'					=> 'output',
						'quantity'					=> $item['quantity'],
						'quantity_balance'			=> $row->product_quantity,
						'unit_price'				=> $item['cost'],
						'total_amount'				=> $item['quantity'] * $item['cost'],
						'monetary_balance'			=> ((int)$row->product_quantity * (float)$item['cost']),
						'transaction_type_id'		=> $data['transaction_type_id'],
						'author_id'					=> $data['user_id'],
						'transaction_id'			=> $id,
						'creation_date'				=> date('Y-m-d H:i:s')
				);
				$dbh->Insert('mb_product_kardex', $kdata);
			}
			//##verificar si es una entrada
			elseif( $tt->in_out == 'in' )
			{
				//##reducir stock global
				$dbh->Update('mb_products',
						array('product_quantity' => "OP[product_quantity + {$item['quantity']}]"),
						array('product_id' => $item['product_id']));
				
				//##generar kardex del producto
				//##obtener cantidad y costo actual del producto
				$sq1 = "SELECT product_cost,product_quantity FROM mb_products WHERE product_id = {$item['product_id']} LIMIT 1";
				$row = $dbh->FetchRow($sq1);
				$kdata = array(
						'product_id'				=> $item['product_id'],
						'in_out'					=> 'input',
						'quantity'					=> $item['quantity'],
						'quantity_balance'			=> $row->product_quantity,
						'unit_price'				=> $item['cost'],
						'total_amount'				=> $item['quantity'] * $item['cost'],
						'monetary_balance'			=> ((int)$row->product_quantity * (float)$item['cost']),
						'transaction_type_id'		=> $data['transaction_type_id'],
						'author_id'					=> $data['user_id'],
						'transaction_id'			=> $id,
						'creation_date'				=> date('Y-m-d H:i:s')
				);
				$dbh->Insert('mb_product_kardex', $kdata);
			}
		}
	}
	else
	{

	}

	return $id;
}
/**
 * Get category childs
 * 
 * @param int $parent 
 * @param SB_DbTable $table 
 * @return array
 */
function __mb_dropdown_categories_get_childs($parent, $table = null)
{
	if( !$table )
	{
		$table = SB_DbTable::GetTable('mb_categories', 1);
	}
	$childs = $table->GetRows(-1, 0, array('parent' => $parent), 'SB_MBCategory');
	
	return $childs;
}
function __mb_dropdown_categories_write_childs($childs, $args, $table, $prefix = '')
{
	//static $prefix = '';
	
	if( !is_array($childs) || !count($childs) )
		return '';
	ob_start(); ?>
	<?php foreach($childs as $cat): ?>
	<?php if($args['type'] == 'list'): ?>
	<li class="menu-item <?php print $args['item_class']; ?>">
		<label>
			<?php if($args['checkbox']): ?>
			<input type="checkbox" name="<?php print $args['name']; ?>" value="<?php print $cat->category_id; ?>"	
					<?php print is_array($args['selected']) && in_array($cat->category_id, $args['selected']) ? 'checked' : ''; ?>/>
			<?php endif; ?>
			<?php if( $args['show_link']): ?><a href="<?php print $cat->link; ?>"><?php endif; ?>
			<?php print isset($args['before_name']) ? $args['before_name'] : ''; ?>
			<?php print $cat->name; ?>
			<?php print isset($args['after_name']) ? $args['after_name'] : ''; ?>
			<?php if( $args['show_link']): ?></a><?php endif; ?>
		</label>
		<?php if($_childs = __mb_dropdown_categories_get_childs($cat->category_id, $table)): ?>
		<ul class="<?php print $args['submenu_class']; ?>" style="margin:0 0 0 10px;">
			<?php print __mb_dropdown_categories_write_childs($_childs, $args, $table); ?>
		</ul>
		<?php endif; ?>
	</li>
	<?php elseif( $args['type'] == 'select' ): ?>
	<option value="<?php print $cat->category_id; ?>" <?php print $args['selected'] == $cat->category_id ? 'selected' : ''; ?>>
		<?php printf("%s %s", $prefix, $cat->name); ?>
	</option>
	<?php 
	if($_childs = __mb_dropdown_categories_get_childs($cat->category_id, $table))
	{
		$prefix .= '-';
		print __mb_dropdown_categories_write_childs($_childs, $args, $table, $prefix);
		$prefix = '';
	}
	?>
	<?php endif; ?>
	<?php endforeach; ?>
	<?php
	return ob_get_clean();
}
/**
 * Build a list or dropdown html of categories
 * 
 * @param array $args
 * @return string
 */
function mb_dropdown_categories($args = array())
{
	$def_args = array(
		'checkbox' 		=> false,
		'name'			=> 'cats[]',
		'selected'		=> null,
		'show_link'		=> false,
		'type'			=> 'list',
		'only_ops'		=> false,
		'store_id'		=> null,
		'class'			=> 'dropdown dropdown-categories',
		'submenu_class'	=> 'submenu',
		'item_class'	=> 'item'
	);
	$args 	= array_merge($def_args, $args);
	$dbh 	= SB_Factory::GetDbh();
	$table 	= SB_DbTable::GetTable('mb_categories', true);
	$conds 	= array('parent' => 0);
	if( $args['store_id'] )
	{
		$conds['store_id'] = $args['store_id'];
	}
	$parents = $table->GetRows(-1, 0, $conds, 'SB_MBCategory');
	
	if( $args['type'] == 'array' )
	{
		$cats = $parents;
		for($i = 0; $i < count($cats); $i++)
		{
			$cats[$i]->childs = __mb_dropdown_categories_get_childs($cats[$i]->category_id, $table);
		}
		return $cats;
	}
	ob_start();
	?>
	<?php if($args['type'] == 'list'): ?>
	<ul class="<?php print $args['class']; ?>">
	<?php elseif( $args['type'] == 'select' && !$args['only_ops'] ): ?>
	<select id="<?php print $args['name']; ?>" name="<?php print $args['name']; ?>" 
		class="<?php print $args['class']; ?> form-control">
		<option value="-1"><?php _e('-- categories --', 'mb'); ?></option>
	<?php endif; ?>
		<?php print __mb_dropdown_categories_write_childs($parents, $args, $table); ?>
	<?php if($args['type'] == 'list'): ?>
	</ul>
	<?php elseif( $args['type'] == 'select' && !$args['only_ops'] ): ?>
	</select>
	<?php endif; ?>
	<?php
	$list = ob_get_clean();
	return $list;
}
