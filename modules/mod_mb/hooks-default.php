<?php
//namespace SinticBolivia\SBFramework\Modules\Mb;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Factory;

class MB_Hooks
{
	public function __construct()
	{
		$this->AddActions();
	}
	public function AddActions()
	{
		if( lt_is_admin() )
		{
			//##add/edit product hooks
			SB_Module::add_action('mb_product_base_type_asm', array($this, 'mb_product_base_type_asm'));
			SB_Module::add_action('mb_save_product', array($this, 'mb_save_product'));
		}
	}
	public function mb_product_base_type_asm($product)
	{
		include 'views/admin/fragments/product-asm.php';
	}
	public function mb_save_product($id, $data)
	{
		$asm = SB_Request::getVar('asm');
		$dbh = SB_Factory::getDbh();
		$dbh->Query("DELETE FROM mb_assemblie2product WHERE assembly_id = $id");
		
		//##insert the assembly items
		if(is_array($asm) ):
		foreach($asm as $item)
		{
			$data = array(
					'assembly_id'	=> $id,
					'product_id'	=> $item['id'],
					'qty_required'	=> $item['qty'],
					'creation_date'	=> date('Y-m-d H:i:s')
			);
			$dbh->Insert('mb_assemblie2product', $data);
		}
		endif;
	}
}