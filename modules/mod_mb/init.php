<?php
//namespace SinticBolivia\SBFramework\Modules\Mb;

use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Modules\Mb\Entities\MB_Order;

//use SinticBolivia\SBFramework\Modules\Mb\Classes\SB_Warehouse;
define('MONOBUSINESS_VER', '1.0.0');
define('MOD_MB_DIR', dirname(__FILE__));
define('MOD_MB_URL', MODULES_URL . '/' . basename(MOD_MB_DIR));
define('MOD_MB_CLASSES_DIR', MOD_MB_DIR . SB_DS . 'classes');
define('MOD_MB_PROD_IMAGE_DIR', UPLOADS_DIR . SB_DS . 'products');
define('MOD_MB_CAT_IMAGE_DIR', UPLOADS_DIR . SB_DS . 'categories');
define('MOD_MB_PROD_IMAGE_URL', UPLOADS_URL . SB_DS . 'products');
define('MOD_MB_CAT_IMAGE_URL', UPLOADS_URL . SB_DS . 'categories');

define('K_TCPDF_THROW_EXCEPTION_ERROR', true);
//##set number format for decimal, by default the decimal separator is "."
setlocale(LC_NUMERIC, 'en_US');
if( !is_dir(MOD_MB_PROD_IMAGE_DIR) )
	mkdir(MOD_MB_PROD_IMAGE_DIR);
if( !is_dir(MOD_MB_CAT_IMAGE_DIR) )
	mkdir(MOD_MB_CAT_IMAGE_DIR);

require_once MOD_MB_DIR . SB_DS . 'functions.php';
require_once MOD_MB_DIR . SB_DS . 'hooks-default.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.tax.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.store.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.store-warehouse.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.category.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.product.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.transaction-type.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.order.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.purchase.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.transfer.php';
//require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.supplier.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.tweak.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.warehouse.php';
require_once MOD_MB_CLASSES_DIR . SB_DS . 'class.coupon.php';
//require_once MOD_MB_DIR . SB_DS . 'Models' . SB_DS . 'model.product.php';
//require_once MOD_MB_DIR . SB_DS . 'Models' . SB_DS . 'model.purchases.php';

class SB_ModMonoBusiness
{
	public $ops;
	protected $features = array(
			'point_of_sale'	=> true,
			'products'		=> true,
			'categories'	=> true,
			'coupons'		=> true,
			'purchases'		=> true,
			'transfers'		=> true,
			'import'		=> true,
			'stores'		=> true,
			'orders'		=> true,
			'measure_unit'	=> true,
			'types'			=> true,
			'taxes'			=> true
	);
    protected $dbh;
	public function __construct()
	{
		$this->dbh = SB_Factory::getDbh();
		//clearstatcache(MOD_MB_DIR . SB_DS . 'locale');
		SB_Language::loadLanguage(LANGUAGE, 'mb', MOD_MB_DIR . SB_DS . 'locale');
		$this->features = SB_Module::do_action('mb_features', $this->features);
		$this->AddActions();
		$this->ops = (object)sb_get_parameter('mb_settings', array());
		
		define('MB_CURRENCY_CODE', isset($this->ops->currency_code) ? $this->ops->currency_code : '$');
	}
	protected function AddActions()
	{
		SB_Module::add_action('modules_loaded', array($this, 'action_modules_loaded'));
		SB_Module::add_action('lt_body_class', array($this, 'action_lt_body_class'));
		if( lt_is_admin() )
		{
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
			SB_Module::add_action('admin_dashboard', array($this, 'action_admin_dashboard'));
			SB_Module::add_action('init', array($this, 'action_init'));
			SB_Module::add_action('lt_js_globals', array($this, 'lt_js_globals'));
			SB_Module::add_action('user_tabs', array($this, 'action_user_tabs'));
			SB_Module::add_action('user_tabs_content', array($this, 'action_user_tabs_content'));
			SB_Module::add_action('save_user', array($this, 'action_save_user'));
			SB_Module::add_action('view_template', array($this, 'action_view_template'));
			if( !sb_is_user_logged_in() )
			{
				SB_Module::add_action('lt_footer', array($this, 'action_lt_footer'));
			}
		}
		//##register order action hooks
		SB_Module::add_action('mb_order_action_send_receipt', array($this, 'mb_order_action_send_receipt'));
		new MB_Hooks();
	}
	public function action_lt_footer()
	{
		/*
		?>
		<div id="mono-business-log" style="position:fixed;left:10px;top:10px;width:250px;height:160px;">
			<img src="<?php print MOD_MB_URL; ?>/images/logo-monobusiness-250x157.png" alt="" style="max-width:100%;max-height:100%;" />
		</div>
		<?php
		*/
	}
	public function action_lt_body_class($classes)
	{
		if( lt_is_admin() )
		{
			if( strstr($_SERVER['SCRIPT_NAME'], 'login.php') )
			{
				$classes[] = 'mb_login';
			}
		}
		return $classes;
	}
	public function action_init()
	{
		$mod = SB_Request::getString('mod');
		if( !$mod || $mod == 'dashboard' )
		{
			//sb_add_script(BASEURL . '/js/Chart.js', 'chart', 0, true);
		}
	}
	public function action_admin_menu()
	{
        SB_Menu::addMenuPage('<span class="glyphicon glyphicon-signal"></span> ' . 
                                __('Transaction Records', 'mb'), 
                                SB_Route::_('index.php?mod=mb&view=transactions.default'), 
                                'mb-menu-transactions', 
                                'mb_menu_transactions');
		if( MB()->HasFeature('orders') )
		{
			SB_Menu::addMenuPage('<span class="glyphicon glyphicon-shopping-cart"></span> '.__('Orders', 'mb'), SB_Route::_('index.php?mod=mb&view=orders.default'), 'mb-menu-orders', 'mb_manage_orders');
			SB_Menu::addMenuChild('mb-menu-orders', __('Orders', 'mb'), SB_Route::_('index.php?mod=mb&view=orders.default'),
					'mb-menu-inventory-pos', 'mb_manage_orders');
				
		}
		if( MB()->HasFeature('point_of_sale') )
        {
            SB_Menu::addMenuPage('<span class="glyphicon glyphicon-record"></span> ' . 
                                __('Point of Sale', 'mb'), SB_Route::_('index.php?mod=mb&view=pos.default'),
                    'mb-menu-inventory-pos', 'mb_sell');
            SB_Menu::addMenuChild('mb-menu-orders', __('New Sale', 'mb'), SB_Route::_('index.php?mod=mb&view=pos.retail'),
                    'mb-menu-inventory-retail', 'mb_sell');
        }
		SB_Menu::addMenuPage('<span class="glyphicon glyphicon-barcode"></span> '.__('Inventory', 'mb'), 
                            'javascript:;', 
                            'mb-menu-inventory', 
							'mb_manage_products');
		if( MB()->HasFeature('stores') )
		{
			SB_Menu::addMenuChild('mb-menu-inventory', __('Stores', 'mb'), SB_Route::_('index.php?mod=mb&view=stores.default'), 'mb-menu-inventory-stores',
					'mb_manage_stores');
			SB_Menu::addMenuChild('mb-menu-inventory', __('Warehouse', 'mb'), SB_Route::_('index.php?mod=mb&view=warehouse.default'), 'mb-menu-inventory-warehouses',
					'mb_manage_warehouses');
			SB_Menu::addMenuChild('mb-menu-inventory', __('Batches', 'mb'), SB_Route::_('index.php?mod=mb&view=batch.default'), 'mb-menu-inventory-batches',
					'mb_manage_batches');
		}

		//if( MB()->HasFeature('types') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Transaction Types', 'mb'), SB_Route::_('index.php?mod=mb&view=ttypes.default'),
					'mb-menu-inventory-transaction-types', 'mb_manage_transaction_types');
		if( MB()->HasFeature('types') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Types', 'mb'), SB_Route::_('index.php?mod=mb&view=types.default'),
									'mb-menu-inventory-product-types', 'mb_manage_product_types');
		if( MB()->HasFeature('measure_unit') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Measurement units', 'mb'), SB_Route::_('index.php?mod=mb&view=mu.default'),
									'mb-menu-inventory-measurement-units', 'mb_manage_measurement_units');
		if( MB()->HasFeature('categories') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Categories', 'mb'), SB_Route::_('index.php?mod=mb&view=categories.default'), 
									'mb-menu-inventory-categories', 'mb_manage_categories');
		SB_Menu::addMenuChild('mb-menu-inventory', __('Products', 'mb'), SB_Route::_('index.php?mod=mb'),
								'mb-menu-inventory-products', 'mb_manage_products');
		if( MB()->HasFeature('taxes') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Taxes', 'mb'), SB_Route::_('index.php?mod=mb&view=taxes.default'),
									'mb-menu-inventory-product-taxes', 'mb_manage_product_taxes');
		if( MB()->HasFeature('coupons') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Coupons', 'mb'), SB_Route::_('index.php?mod=mb&view=coupons.default'),
									'mb-menu-inventory-coupons', 'mb_manage_coupons');
		if( MB()->HasFeature('purchases') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Purchases', 'mb'), SB_Route::_('index.php?mod=mb&view=purchases.default'),
									'mb-menu-inventory-purchases', 'mb_can_manage_purchases');
		if( MB()->HasFeature('transfers') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Transfers', 'mb'), SB_Route::_('index.php?mod=mb&view=transfers.default'),
					'mb-menu-inventory-transfers', 'mb_manage_transfers');
		SB_Menu::addMenuChild('mb-menu-inventory', 
						__('Tweaks', 'mb'),
							SB_Route::_('index.php?mod=mb&view=tweaks.default'),
							'mb-menu-tweaks',
							'mb_manage_products');
		SB_Menu::addMenuChild('mb-menu-inventory',
				__('Brands', 'mb'),
				SB_Route::_('index.php?mod=mb&view=brands.default'),
				'mb-menu-brands',
				'mb_menu_brands');
		
		SB_Menu::addMenuChild('mb-menu-inventory', __('Reports', 'mb'), SB_Route::_('index.php?mod=mb&view=reports.default'),
								'mb-menu-inventory-reports', 'mb_build_reports');
		if( MB()->HasFeature('import') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Import Products', 'mb'), SB_Route::_('index.php?mod=mb&view=import'),
									'mb-menu-inventory-import', 'mb_import');
		//if( MB()->HasFeature('export') )
			SB_Menu::addMenuChild('mb-menu-inventory', __('Export Products', 'mb'), 
									SB_Route::_('index.php?mod=mb&view=export'),
									'mb-menu-inventory-import', 'mb_export');
									
		SB_Menu::addMenuChild('mb-menu-inventory', 
								'<span class="glyphicon glyphicon-wrench"></span> ' . __('Settings', 'mb'), 
                                SB_Route::_('index.php?mod=mb&view=settings'),
								'mb-menu-inventory-settings', 'mb_settings');
	}
	public function action_admin_dashboard()
	{
        $user = sb_get_current_user();
        $where = "";
		if( !$user->can('mb_see_all_stores') && !$user->_store_id )
        {
            return false;
        }
        if( !$user->can('mb_see_all_stores') )
        {
            $where .= "AND store_id = {$user->_store_id} ";
        }
		//##get products without minimal stock assigned
		$query = "SELECT COUNT(*) FROM mb_products WHERE min_stock IS NULL $where";
		$null_products = $this->dbh->GetVar($query);
		//##get products with quantity lower or equal to minimal stock
		$query = "SELECT COUNT(*) 
                    FROM mb_products
					WHERE 1 = 1
					AND product_quantity <= min_stock 
					AND product_quantity > 0 
					AND min_stock IS NOT NULL
                    $where";
		$warning_products = $this->dbh->GetVar($query);
		//##get products with quantity lower or equal to cero
		$query = "SELECT COUNT(*) 
                    FROM mb_products
					WHERE 1 = 1
					AND (product_quantity <= 0)
					AND min_stock IS NOT NULL
                    $where";
		$zero_products = $this->dbh->GetVar($query);
		//##get sales history
		/*
		$query_sales = "SELECT order_id,store_id, SUM(total), DATE(order_date) AS date, count(order_id) AS sales ". 
						"FROM little_cms.mb_orders ".
						"where 1 = 1 ".
						"and (status = 'sold' OR status = 'complete') ".
						"and ( DATE(order_date) >= current_date() - interval 7 day ) ".
						"GROUP BY order_date " . 
						"ORDER BY order_date ASC";
		*/
		$query_sales = array();
		for($i = 7; $i >= 1; $i--)
		{
			//$query_sales[] = "(SELECT CONCAT(CAST(COUNT(order_id) AS char), ':', current_date() - interval $i day) ".
			if( $this->dbh->db_type == 'mysql' )
			{
				$query_sales[] = "(SELECT CONCAT(CAST(SUM(total) AS char), ':', current_date() - interval $i day) ".
								"FROM mb_orders ".
								"WHERE 1 = 1 ".
                                $where .
								"and (status = 'sold' OR UPPER(status) = '".MB_Order::STATUS_COMPLETED."') ".
								"and ( DATE(order_date) >= current_date() - interval $i day ) ) AS  sales$i";
			}
			elseif( $this->dbh->db_type == 'sqlite3' )
			{
				$query_sales[] = "(SELECT ( SUM(total) || ':' || date('now', 'localtime', '-$i day') ) ".
								"FROM mb_orders ".
								"WHERE 1 = 1 ".
                                $where .
								"and (status = 'sold' OR status = '".MB_Order::STATUS_COMPLETED."') ".
								"and ( DATE(order_date) >= date('now', 'localtime', '-$i day') ) ) AS  sales$i";
			}
		}
        //print_r($query_sales);
		$sales = $this->dbh->FetchRow("SELECT " . implode(',', $query_sales));
        
		//##get most 
		$query_most = "select (SELECT product_name FROM mb_products p where p.product_id = oi.product_id) as product, 
								oi.product_id, SUM(quantity) AS qty ".
						"from mb_orders o, mb_order_items oi " .
						"where 1 = 1 " .
						"and o.order_id = oi.order_id " .
						"and (o.status = 'sold' OR o.status = 'complete') ".
                        $where .
						"group by product_id ".
						"order by qty DESC ".
						"LIMIT 10";
		$mosts = $this->dbh->FetchResults($query_most);
        //sb_add_script(BASEURL . '/js/Chart.js', 'chart-js', 0, true);
		?>
		<script src="<?php print BASEURL; ?>/js/Chart.js"></script>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="widget panel panel-default">
				<div class="widget-header panel-heading">
					<i class="icon-list-alt"></i>
					<h3 class="panel-title"><?php _e('Inventory Warning', 'mb'); ?></h3>
				</div>
				<div class="widget-content panel-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="stat">
									<div class="value text-center"><?php print $null_products ?></div>
									<div class="text text-info">
										<?php _e('Products without minimal stock', 'mb'); ?>
									</div>
									<div class="text-center">
										<a class="btn btn-info" href="<?php print SB_Route::_('index.php?mod=mb&filter=null'); ?>">
											<?php _e('View list', 'mb'); ?>
										</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="stat">
									<div class="value text-center"><?php print $warning_products; ?></div>
									<div class="text text-warning">
										<?php _e('Products with quantity lower than minimal', 'mb'); ?>
									</div>
									<div class="text-center">
										<a class="btn btn-warning" href="<?php print SB_Route::_('index.php?mod=mb&filter=warning'); ?>">
											<?php _e('View list', 'mb'); ?>								
										</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="stat ">
									<div class="value text-center"><?php print $zero_products; ?></div>
									<div class="text text-danger"><?php _e('Products with quantity zero', 'mb'); ?></div>
									<div class="text-center">
										<a class="btn btn-danger" href="<?php print SB_Route::_('index.php?mod=mb&filter=zero'); ?>">
											<?php _e('View list', 'mb'); ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="widget panel panel-default">
				<div class="widget-header panel-heading" data-datetime="<?php print date('Y-m-d H:i:s') ?>">
                    <h3 class="panel-title"><?php _e('Sales', 'mb'); ?></h3>
                </div>
				<div class="widget-content panel-body">
					<?php
                    //print_r($sales);
					$labels = array();
					$_sales = array();
					if( is_array($sales) || is_object($sales) ) 
                    {
                        foreach(get_object_vars($sales) as $key => $val)
                        {
                            if( empty($val) ) continue;
                            list($count, $label) = explode(':', $val);
                            $labels[] = $label;
                            $_sales[] = number_format($count, 2, '.', '');
                        }
                    }
                    //print_r($_sales);
					$data = array(
							'labels' 	=> $labels,
							'datasets'	=> array(
									array(
											'label' 			=> __('Sales Statistics (past 7 days)', 'mb'),
											//'fillColor' 		=> "rgba(220,220,220,0.5)",
											//'fillColor'			=> 'rgba(151,187,205,0.5)',
											'fillColor'			=> 'rgba(80,185,229,1)',
											'strokeColor' 		=> "rgba(220,220,220,0.8)",
											'highlightFill'		=> "rgba(220,220,220,0.75)",
											'highlightStroke'	=> "rgba(220,220,220,1)",
											'data'				=> $_sales
									)
							)
					);
					?>
					<?php if( empty($_sales) ): ?>
					<h3 class="text-center"><?php _e('There are no sales yet', 'mb'); ?></h3>
					<?php else: ?>
					<canvas id="sales-chart" width="500" height="200"></canvas>
					<script>
					var data = <?php print json_encode($data); ?>;
					jQuery(function()
					{
						try
						{
							window.chart01_ctx = document.querySelector('#sales-chart').getContext('2d');
							window.chart01 = new Chart(chart01_ctx).Bar(data);
						}
						catch(e)
						{
							console.log(e);
						}
					});
					</script>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="widget panel panel-default">
				<div class="widget-header panel-heading">
					<h3 class="panel-title"><?php _e('Most selled products', 'mb'); ?></h3>
				</div>
				<div class="widget-content panel-body">
					<div style="width:100%;height:200px;overflow:auto;">
						<table class="table">
						<thead>
						<tr>
							<th><?php _e('Product', 'mb'); ?></th>
							<th><?php _e('Qty', 'mb'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($mosts as $p): ?>
						<tr>
							<td><?php print $p->product; ?></td>
							<td><?php print $p->qty; ?></td>
						</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php 
	}
	/**
	 * Send order receipt to customer
	 */
	public function mb_order_action_send_receipt($order)
	{
		$business = (array)sb_get_parameter('invoices_ops');
		$business_email = 'info@sinticbolivia.net';
		$business_website = BASEURL;
		extract($business);
		ob_start();
		include 'templates/email/tpl-receipt.php';
		$html = ob_get_clean();
		$headers = array(
				'Content-type: text/html',
				'From: no-reply@' . basename(HTTP_HOST)
		);
		lt_mail($order->customer->email, sprintf(__('%s - Your order is now processing', 'mb'), SITE_TITLE), $html, $headers);
	}
	public function HasFeature($feature)
	{
		return isset($this->features[$feature]) && $this->features[$feature] === true;
	}
	public function lt_js_globals($js)
	{
		$locale = array(
				'SOURCE_STORE_NEEDED'	=> __('You need to select a source store', 'mb'),
				'DESTINATION_STORE_NEEDED'	=> __('You need to select a destination store', 'mb'),
				'TRANSFER_ITEMS_REQUIRED'	=> __('You need to select items to transfer', 'mb')
		);
		if( isset($js['modules']['mb']['locale']) )
		{
			$js['modules']['mb']['locale'] = array_merge($js['modules']['mb']['locale'], $locale);
		}
		else
		{
			$js['modules']['mb']['locale'] = $locale;
		}
		return $js;
	}
	public function action_user_tabs($user)
	{
		?>
		<li class="">
			<a href="#store" data-toggle="tab"><?php _e('Store', 'mb'); ?></a>
		</li>
		<?php 
	}
	public function action_user_tabs_content($user)
	{
		$stores = SB_Warehouse::getStores();
		$warehouses 	= array();
		$u_warehouses 	= array();
		if( $user && $user->_store_id )
		{
			$warehouses = SB_DbTable::GetTable('mb_warehouse', 1)->GetRows(-1, 0, array('store_id' => (int)$user->_store_id));
			if( $list = json_decode($user->_warehouses) )
			{
				$u_warehouses = (array)$list;
			}
		}
		?>
		<div class="tab-pane" id="store">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label><?php _e('Store', 'mb'); ?></label>
						<select name="meta[_store_id]" class="form-control">
							<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
							<?php foreach($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>" 
								<?php print $user && sb_get_user_meta($user->user_id, '_store_id') == $store->store_id ? 'selected' : ''; ?>>
								<?php print $store->store_name; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label><?php _e('Warehouses', 'mb'); ?></label>
						<div class="form-control" style="min-height:100px;max-height:300px;overflow:auto;">
							<?php foreach($warehouses as $w): ?>
							<div><label>
								<input type="checkbox" name="meta[_warehouses][]" value="<?php print $w->id; ?>" 
									<?php print in_array($w->id, $u_warehouses) ? 'checked' : '';?>  />
								<?php print $w->name; ?>
							</label></div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>
		<?php 
	}
	public function action_save_user($user_id)
	{
		$meta = (array)SB_Request::getVar('meta', array());
		foreach($meta as $meta_key => $meta_value)
		{
			sb_update_user_meta($user_id, $meta_key, $meta_value);
		}
	}
	public function action_view_template($view_file, $mod)
	{
		$mod_view = SB_Request::getString('view');
		if( $mod == 'users' )
		{
			if( strstr($view_file, 'default.php') )
			{
				$view_file = MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'users.default.php';
			}
		}
		return $view_file;
	}
	public function action_modules_loaded()
	{
		$this->StartApiRest();
	}
	protected function StartApiRest()
	{
		//##set endpoints
		SB_Module::add_action('rewrite_routes', function($routes)
		{
            
			$routes['/^\/api\/monobusiness\/?$/'] = 'mod=mb&task=api.default';
            $routes['/^\/api\/monobusiness\/stores\/?$/'] = 'mod=mb&task=apistores.show';
            $routes['/^\/api\/monobusiness\/stores\/([0-9]+)\/?$/'] = 'mod=mb&task=apistores.get&id=$1';
            $routes['/^\/api\/monobusiness\/stores\/new\/?$/'] = 'mod=mb&task=apistores.create';
            $routes['/^\/api\/monobusiness\/stores\/update\/?$/'] = 'mod=mb&task=apistores.update';
            $routes['/^\/api\/monobusiness\/stores\/delete\/([0-9]+)\/?$/'] = 'mod=mb&task=apistores.delete';
            $routes['/^\/api\/monobusiness\/products\/?$/'] = 'mod=mb&task=apiproducts.show';
            $routes['/^\/api\/monobusiness\/products\/([0-9]+)\/?$/'] = 'mod=mb&task=apiproducts.get&id=$1';
			$routes['/^\/api\/monobusiness\/products\/([a-zA-Z-_]+)\/?$/'] = 'mod=mb&task=apiproducts.$1';
			$routes['/^\/api\/monobusiness\/products\/([a-zA-Z-_]+)\/([0-9]+)\/?$/'] = 'mod=mb&task=apiproducts.$1&id=$2';
            $routes['/^\/api\/monobusiness\/pos\/?$/'] = 'mod=mb&task=apipos.default';
            $routes['/^\/api\/monobusiness\/pos\/([0-9]+)\/?$/'] = 'mod=mb&task=apipos.get&id=$1';
			$routes['/^\/api\/monobusiness\/pos\/([a-zA-Z-_]+)\/?$/'] = 'mod=mb&task=apipos.$1';
			$routes['/^\/api\/monobusiness\/products\/([a-zA-Z-_]+)\/([0-9]+)\/?$/'] = 'mod=mb&task=apiproducts.$1&id=$2';
			
			return $routes;
		});
		//die(__METHOD__);
	}
}
function MB()
{
	static $mb;
	if( !$mb )
		$mb = new SB_ModMonoBusiness();
	return $mb;
}
MB();
