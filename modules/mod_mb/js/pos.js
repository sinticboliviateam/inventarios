/**
 * 
 */
function MBPos()
{
	var $this 			    = this;
	var form			    = jQuery('#form-pos');
	var _products_table     = jQuery('#products-table');
	var table 			    = jQuery('#table-order-items');
	var label_btn_delete    = window.mb && window.mb.locale.label_btn_delete_order_item ? window.mb.locale.label_btn_delete_order_item : 'Delete item';
	var current_keyword     = null;
	var current_view	    = 'list';
	var customer_blocked    = false;
    var interval            = null;
	var row_tpl = '<div class="trow" data-quantity={quantity}>'+
					'<div class="col col-qty">'+
						'<input type="hidden" min="1" name="products[{index}][id]" value="{id}" class="order-item-id" />'+
						'<input type="number" min="1" name="products[{index}][qty]" value="{qty}" class="order-item-qty" />'+
					'</div>'+
					'<div class="col col-product">\
						<input type="hidden" name="products[{index}][name]" value="{product_text}" class="order-item-id" />{product}</div>'+
					'<div class="col col-price"><input type="text" name="products[{index}][price]" value="{price}" class="order-item-price" /></div>'+
					'<div class="col col-total">{total}</div>'+
					'<div class="col col-delete"><a href="javascript:;" class="btn-delete-order-item" title="'+ label_btn_delete +'">&nbsp;</a></div>'+
				'</div>';
	
	this.ProductExists = function(id)
	{
		var result = false;
		table.find('.trow').each(function(i, row)
		{
			if( row.dataset.product_id == id )
			{
				result = i;
				return false;
			}
		});
		return result;
	};
	this.AddProduct = function(id, by, quantity)
	{
        var qty = quantity ? parseInt(quantity) : 1;
		var args = 'mod=mb&task=pos.ajax&action=get_product&id='+id;
		if( by )
		{
			args += '&by='+by;
		}
		jQuery.get('index.php?'+args, function(res)
		{
			if( res.status == 'ok' )
			{
				if( res.product.product_quantity > 0 )
				{
					if( (index = $this.ProductExists(parseInt(res.product.product_id))) === false )
					{
						var product_name = res.product.product_name.length > 50 ? 
											'<span title="'+res.product.product_name+'">' + res.product.product_name.substr(0, 50) + '...</span>' : 
											res.product.product_name;
						var rows 	= table.find('.trow').length;
						var html = row_tpl.replace(/{index}/g, rows)
											.replace(/{quantity}/g, parseInt(res.product.product_quantity) - qty)
											.replace('{id}', res.product.product_id)
											.replace('{qty}', qty)
											.replace(/{product_text}/g, res.product.product_name)
											.replace(/{product}/g, product_name)
											.replace('{price}', res.product.product_price)
											.replace('{total}', res.product.product_price * qty);
						var row = jQuery(html);
						row.get(0).dataset.product_id = res.product.product_id;
						table.append(row);
						var label = _products_table.find('.table-body .trow[data-id='+res.product.product_id+'] .column-qty .label');
						label.html(parseInt(res.product.product_quantity) - qty);
						if( (parseInt(res.product.product_quantity) - qty) == 0 )
						{
							jQuery(label).removeClass('label-info').addClass('label-danger')
						}
					}
					else
					{
						var row = table.find('.trow').eq(index).get(0);
						/*
						if( (parseInt(row.dataset.quantity) - 1) < 0 )
						{
							alert(lt.modules.mb.locale.ERROR_PRODUCT_WO_STOCK);
						}
						else
						{*/
							var _qty = parseInt(jQuery(row).find('.col-qty input[type=number]').val());
							_qty++;
							var price 		= parseFloat(jQuery(row).find('.col-price input:first').val());
							var total 	= (_qty * price).toFixed(2);
							jQuery(row).find('.col-qty input[type=number]').val(_qty);
							jQuery(row).find('.col-total').html(total);
							row.dataset.quantity = parseInt(row.dataset.quantity) - 1;
							var label = _products_table.find('.table-body .trow[data-id='+res.product.product_id+'] .column-qty .label');
							label.html(parseInt(res.product.product_quantity) - _qty);
							if( (parseInt(res.product.product_quantity) - _qty) == 0 )
							{
								jQuery(label).removeClass('label-info').addClass('label-danger')
							}
						//}
						
					}
					
					$this.CalculateTotals();
				}
				else
				{
					alert(lt.modules.mb.locale.ERROR_PRODUCT_WO_STOCK);
				}
				
			}
			else
			{
				alert(res.error);
			}
		});
	};
	this.CalculateTotals = function()
	{
		var tax_percent = window.pos_rate ? parseFloat(pos_rate / 100) : 0;
		var order_total = 0;
		var order_subtotal = 0;
		var order_tax_total = 0;
		
		table.find('.trow').each(function(i, $row)
		{
			var row 	= jQuery($row);
			var qty 	= parseInt(row.find('.order-item-qty').val());
			var price 	= parseFloat(row.find('.order-item-price').val());
			order_total += qty * price;
			
		});
		if( tax_percent > 0 )
		{
			//##calculate de tax amount
			order_tax_total = order_total * tax_percent;
			order_subtotal	= order_total - order_tax_total;
			//order_tax_total = order_subtotal * tax_percent;
			//order_total = order_subtotal + order_tax_total;
		}
		else
		{
			//order_total = order_subtotal;
		}
		jQuery('#subtotal').html(parseFloat(order_subtotal).toFixed(2));
		jQuery('#tax').html(parseFloat(order_tax_total).toFixed(2));
		jQuery('#total').html(parseFloat(order_total).toFixed(2));
	}
	this.CalculateRowTotals = function(row)
	{
		var qty 		= parseInt(row.find('.col-qty input[type=number]').val());
		var price 		= parseFloat(row.find('.col-price input:first').val());
		qty = isNaN(qty) ? 0 : qty;
		price = isNaN(price) ? 0 : price;
		var total		= (qty * price).toFixed(2);
		
		//row.find('.col-qty input:first').val(qty);
		row.find('.col-total').html(total);
		
		return true;
	};
	this.OnQtyChanged = function(e)
	{
		/*
		if( e.keyCode != 13 )
		{
			return true;
		}
		*/
		var row = jQuery(this).parents('.trow:first');
		$this.CalculateRowTotals(row);
		$this.CalculateTotals();
		return true;
	};
	this.ChangeLayout = function()
	{
		jQuery('#pos-views a').removeClass('active');
		var layout = this.dataset.layout;
		var store_id = jQuery('#store_id').val();
		current_view = layout;
		
		jQuery(this).addClass('active');
		var params = 'mod=mb&task=pos.get_layout&layout=' + layout + '&store_id=' + store_id;
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				jQuery('#products-container').html(res.html);
			}
			else
			{
				alert(res.error);
			}
			
			if( current_keyword && current_keyword.length > 0)
			{
				$this.SearchProduct(current_keyword);
			}
			
			_products_table = jQuery('#products-table');
		});
		
		
		return false;
	};
	this.DeleteOrderItem = function(row)
	{
		row 			= jQuery(row);
		var input_qty 	= row.find('.order-item-qty:first');
		var qty 		= parseInt(input_qty.val());
		if( qty == 1 )
		{
			row.remove();
			$this.CalculateTotals();
		}
		else
		{
			input_qty.val( qty - 1 ).trigger('change');
		}
	}
	this.FindCustomer = function(keyword)
	{
		var event = window.event || arguments.callee.caller.arguments[0];
		
		if( keyword.length <= 0 || isNaN(keyword) )
		{
			alert('Debe ingresar el NIT para el cliente');
			return false;
		}
		
		var params = 'mod=mb&task=pos.ajax&action=find_customer&keyword=' + keyword;
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				//##check if customer is blocked
				if( res.customer.blocked === 1 )
				{
					alert(lt.modules.mb.locale.CUSTOMER_IS_BLOCKED);
					jQuery('#customer_id').val('0');
					customer_blocked = true;
					return false;
				}
				//##check for pending payments
				if( res.customer.pending_payments && res.customer.pending_payments.length > 0 )
				{
					alert(lt.modules.mb.locale.CUSTOMER_HAS_PENDING_PAYMENTS);
				}
				var name = '';
				if( res.customer._last_sale_name )
				{
					name = res.customer._last_sale_name;
				}
				else
				{
					name = (res.customer.first_name || '') + ' ' + (res.customer.last_name || '');
				}
				jQuery('input[name=customer]').val(name);
				jQuery('#customer_id').val(res.customer.customer_id);
				jQuery(document).trigger('mb_pos_customer_selected', res.customer);
			}
			else
			{
				jQuery('#customer_id').val('0');
			}
			jQuery('input[name=customer]').focus();
		});
	}
	this.ResetOrder = function()
	{
		jQuery('#table-order-items').html('');
		jQuery('#subtotal, #tax, #total').html('0.00');
		jQuery('#nit_ruc_nif, #customer, #customer_id').val('');
		var index = 'sale_store_' + jQuery('#store_id').val();
		$this.ResetSession();
	}
	this.RegisterSale = function()
	{
		if( customer_blocked )
		{
			alert(lt.modules.mb.locale.CUSTOMER_IS_BLOCKED);
			return false;
		}
		var res = jQuery(document).trigger('mb_pos_before_register_sale', form.get(0));
		if( window.submit_pos && window.submit_pos.error )
		{
			alert(window.submit_pos.error);
			delete window.submit_pos;
			return false;
		}
		var params = form.serialize();
		jQuery('#modal-processing').modal('show', {backdrop: 'static', keyboard: false});
		jQuery.post('index.php', params, function(res)
		{
			jQuery('#modal-processing').modal('hide');
			jQuery('#btn-register-sale').attr('disabled', false);
			if( res.status == 'ok' )
			{
				if( res.eval )
				{
					eval(res.eval);
				}
				$this.ResetOrder();
				jQuery('#success-dialog').on('hidden.bs.modal', function (e) 
				{
					window.location.reload();
				});
                var silent = res.silent_print ? true : false;
                var size = res.receipt_type && res.receipt_type == 'ticket' ? ',width=300,height=600' : ',width=auto,height=600';
                var modal = window.open(res.print_url, '_blank', 'toolbar=no,location=no,resizable=no' + size);
                /*
				if( res.print_url )
				{
					var btn = '<a href="'+res.print_url+'" class="btn btn-warning" target="_blank">'+
								'<span class="glyphicon glyphicon-print"></span></a>';
					jQuery('#success-buttons').append(btn);
				}
                */
				jQuery('#success-dialog .message').html(res.message);
				jQuery('#success-dialog').modal('show');
				
			}
			else
			{
				alert(res.error);
			}
			
		});
		jQuery('#btn-register-sale').attr('disabled', true);
		
		return false;
	};
	this.SearchProduct = function(keyword, category_id)
	{
		current_keyword = keyword;
		jQuery('#search-spin').css('display', 'inline');
		var params = 'mod=mb&task=pos.search_product&store_id='+store_id+'&keyword='+keyword+'&layout='+current_view;
		if( category_id )
		{
			params += '&category_id='+category_id;
		}
		jQuery.post('index.php', params, function(res)
		{
			jQuery('#search-spin').css('display', 'none');
			if( res.status == 'ok' )
			{
				if( current_view == 'list' )
					_products_table.find('.table-body').html(res.items);
				else
					jQuery('.grid-layout').html(res.items);
			}
			else
			{
				alert(res.error);
			}
		});
	};
	this.CashCountTotal = function()
	{
		var total = 0;
		jQuery('input[data-cut]').each(function(i, obj)
		{
			total += parseInt(Number(obj.value)) * parseInt(obj.dataset.cut);
		});
		jQuery('.coins').each(function(i, input)
		{
			total += parseFloat(Number(input.value) * parseFloat(input.dataset.value));
		});
		
		var total_sales = parseFloat(jQuery('#cash_count_sales').val());
		var diff		= total_sales - total;
		
		document.getElementById('manual_balance').value 	= total.toFixed(2);
		document.getElementById('difference').value = parseFloat(document.getElementById('total_cash').value) - total.toFixed(2);
	};
	this.AddExtraCost = function(e)
	{
		jQuery('input[name=_extra_item]').val('').focus();
		jQuery('input[name=_extra_amount]').val('');
		//btn-add-extra-cost
		jQuery('#modal-extra-cost').modal('show');
		return false;
	};
	this.InsertExtraCost = function(e)
	{
		var item 	= jQuery('input[name=_extra_item]').val();
		var amount 	= parseFloat(jQuery('input[name=_extra_amount]').val());
		if( item.length <= 0 )
		{
			alert('Debe ingresar un nombre para el item');
			return false;
		}
		if( amount.length <= 0 )
		{
			alert('Debe ingresar un monto para el item');
			return false;
		}
		var rows 	= table.find('.trow').length;
		var html = row_tpl.replace(/{index}/g, rows)
							.replace('{id}', -1)
							.replace('{qty}', 1)
							.replace(/{product_text}/g, item)
							.replace(/{product}/g, item)
							.replace('{price}', amount.toFixed(2))
							.replace('{total}', amount.toFixed(2));
		table.append(html);
		$this.CalculateTotals();
		jQuery('#modal-extra-cost').modal('hide');
		return false;
	};
	this.RegisterSpending = function()
	{
		var form	= jQuery('#form-register-spending');
		var params 	= form.serialize();
		jQuery.post('index.php', params, function(res)
		{
			form.get(0).detail.value = '';
			form.get(0).amount.value = '';
			if( res.status == 'ok' )
			{
				jQuery('#modal-spending').modal('hide');
				alert(res.message);
			}
			else
			{
				alert(res.error);
			}
		});
		return false;
	};
	this.OpenCashbox	= function()
	{
		var amount = this.amount.value;
		if( isNaN(parseFloat(amount)) )
		{
			this.amount.value = '0.00';
		}
		var params = jQuery(this).serialize();
		jQuery.post('index.php', params, function(res)
		{
			jQuery('#modal-open-cashbox').modal('hide');
			if( res.status == 'ok' )
			{
				alert(res.message);
			}
			else
			{
				alert(res.error);
			}
		});
		
		return false;
	};
	this.SetEvents = function()
	{
		jQuery('.category-button').click(function()
		{
			$this.SearchProduct('', this.dataset.id);
			return false;
		});
		jQuery('#cat_id').change(function()
		{
			var cat_id 	= parseInt(this.value);
			cat_id 		= ( isNaN(cat_id) || cat_id <= 0 ) ? null : cat_id;
			
			$this.SearchProduct('', cat_id);
		});
		jQuery(document).on('click', '.btn-add-product', function()
		{
			var row          = jQuery(this).parents('.trow:first').get(0);
            if( !row )
                return false;
            var quantity    = parseInt(row.dataset.quantity);
            
            if( isNaN(quantity) || quantity <= 0 )
            {
                console.log('Product has no quantity');
                return false;
            }
            
			$this.AddProduct(row.dataset.id);
		});
		jQuery(document).on('keyup change', '.order-item-qty, .order-item-price', this.OnQtyChanged);
		jQuery(document).on('click', '.btn-delete-order-item', function()
		{
			$this.DeleteOrderItem(this.parentNode.parentNode);
			return false;
		});
		//##set search event
		jQuery('#keyword').keyup(function(e)
		{
			if( e.keyCode != 13 )
				return true;
			$this.SearchProduct(this.value)
			return true;
		});
		jQuery('#pos-views a').click(this.ChangeLayout);
		jQuery('#nit_ruc_nif').keydown(function(e)
		{
			if( e.keyCode == 13 )
			{
				e.preventDefault();
				return false;
			}
		});
		jQuery('#nit_ruc_nif').keyup(function(e)
		{
			if( e.keyCode == 13 )
			{
				$this.FindCustomer(this.value.trim());
			}
		});
		jQuery('#nit_ruc_nif').blur(function(e)
		{
			if( this.value.trim().length > 0 )
			{
				$this.FindCustomer(this.value.trim());
			}
		});
		jQuery('#customer').keyup(function(e){if(e.keyCode == 13){$this.RegisterSale()}});
		jQuery('#btn-add-extra-cost').click(this.AddExtraCost);
		jQuery('#btn-insert-extra-cost').click(this.InsertExtraCost);
		jQuery('#btn-void-sale').click(this.ResetOrder);
		jQuery('#btn-save-notes').click(function(){jQuery('#notes').val(jQuery('#order-notes').val());});
        jQuery('#btn-save-sale').click(function()
        {
            $this.SaveCurrent();
            alert(lt.modules.mb.locale.SALE_SAVED);
        });
		jQuery('#btn-register-sale').click(this.RegisterSale);
		//##set barcode event
		jQuery('#barcode').keydown(function(e)
		{
			if( e.keyCode == 13 )
			{
				$this.AddProduct(this.value, 'barcode');
				this.value = '';
				return false;
			}
		});
		//##set gallery
		jQuery(document).on('click', '.product-image', function(e)
		{
			jQuery('#modal-gallery .modal-title').html(this.dataset.title);
			jQuery('#modal-gallery .modal-body').html('');
			jQuery('#modal-gallery .modal-body').html('<div class="text-center"><img src="'+this.href+'" alt="" style="height:500px;" /></div>');
			jQuery('#modal-gallery').modal('show');
			return false;
		});
        //##view quick info
        jQuery(document).on('click', '.btn-view-product-info', function(e)
        {
            var product = jQuery(e.target).parents('.trow:first').get(0);
            
            jQuery('#_code').html(product.dataset.code);
            jQuery('#_name').html(e.target.textContent);
            jQuery('#_qty').html(product.dataset.quantity);
            jQuery('#_cost').html(product.dataset.cost);
            jQuery('#_price1').html(product.dataset.price);
            jQuery('#_price2').html(product.dataset.price_2);
            jQuery('#_price3').html(product.dataset.price_3);
            jQuery('#_price4').html(product.dataset.price_4);
            jQuery('#quick-view-dialog').modal('show');
        });
		//##set events for cash count
		jQuery('#btn-cash-count').click(function()
		{
			jQuery('input[data-cut], .coins,#manual_balance,#difference').val(0);
			jQuery.get('index.php?mod=mb&task=pos.ajax&action=cash_count&store_id=' + store_id, function(res)
			{
				if( res.status == 'ok' )
				{
					res.total_sales && jQuery('#cash_count_sales').val(res.total_sales.toFixed(2));
					res.balance && jQuery('#balance').val(parseFloat(res.balance).toFixed(2));
					jQuery('#total_spends').val(parseFloat(res.total_spends).toFixed(2));
					jQuery('#total_cash').val(parseFloat(res.total_cash).toFixed(2));
					jQuery('#modal-cash-count').modal('show');
					var table = '<table class="table table-condensed"><thead>\
									<tr>\
										<th>Detalle</th><th>Monto</th>\
									</tr></thead>';
					jQuery.each(res.expensed, function(i, item)
					{
						table += '<tr>\
							<td>'+item.detail+'</td>\
							<td class="text-right">'+item.amount+'</td>\
							</tr>';
					});
					table += '<tr><td><b>Total:</b></td><td class="text-right"><b>'+res.total_spends.toFixed(2)+'</b></td></tr></table>';
					jQuery('#spending').html(table);
				}
				else
				{
					alert(res.error);
				}
			});
			return false;
		});
		jQuery('input[data-cut]').focus(function()
		{
			this.select();
		});
		jQuery('input[data-cut], .coins').on('keyup change', function()
		{
			if( !isNaN(this.value) )
			{
				$this.CashCountTotal();
			}
		});
		jQuery('#btn-save-cash-count').click(function()
		{
			var sales 	= jQuery('#cash_count_sales').val();
			var balance = jQuery('#manual_balance').val();
			var params	= jQuery('#form-cash-count').serialize() + '&store_id=' + store_id;
			jQuery.post('index.php', params, function(res)
			{
				jQuery('#modal-cash-count').modal('hide');
				if( res.status == 'ok' )
				{
					alert(res.message);
					if( res.print_link )
					{
						jQuery('#js-dlg-alert .modal-footer').append('<a href="'+res.print_link+'" target="_blank" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span> Imprimir</a>');
					}
				}
				else
				{
					alert(res.error);
				}
				
			});
			return false;
		});
		jQuery('#btn-save-spending').click($this.RegisterSpending);
		jQuery('#btn-open-cashbox').click(function()
		{
			jQuery('#modal-open-cashbox input[name=amount]').val('');
			jQuery('#modal-open-cashbox').modal('show');
			return false;
		});
		jQuery('#form-opencashbox').submit($this.OpenCashbox);
        //set timer to save draft
        interval = setInterval(this.SaveDraft.bind(this), 120000);//every 3 mins
        window.onbeforeunload = function()
        {
            $this.SaveCurrent();
        };
        window.addEventListener('offline', $this.SaveCurrent);
        window.addEventListener('online', function(){});
	};
    this.SaveDraft = function()
    {
        var draftId = jQuery('#draft_id').length > 0 ? parseInt(jQuery('#draft_id').val()) : null;
        console.log('darftId: ' + draftId);
        //##clone current form
        var newForm             = document.createElement('form');
        newForm.innerHTML       = form.get(0).innerHTML;
        newForm.task.value      = 'pos.ajax';
       
        
        var params = jQuery(newForm).serialize();
        params += '&action=savedraft';
        //console.log(params);
        //return false;
        jQuery.post('index.php', params, function(res)
        {
            if( res && res.status )
            {
                if( res.status == 'ok' )
                {
                    if( draftId == null )
                    {
                        form.prepend('<input type="hidden" id="draft_id" name="draft_id" value="" />');
                    }
                    jQuery('#draft_id').val(res.draft_id || 0);
                }
                else if( res.status == 'error' )
                {
                    console.log(res.error);
                }
            }
            else
            {
                console.log('An unknow error has ocurred while trying to save the sale draft');
            }
        });
        
        
    };
    this.CheckSession = function()
    {
    };
    this.SaveCurrent = function()
    {  
        if( typeof(Storage) == 'undefined' )
        {
            alert('Your browser does not support session storage');
            return true;
        }
        var sale = {
            items: [], 
            customer: {
                id:     jQuery('#customer_id').val(),
                nit:    jQuery('#nit_ruc_nif').val(),
                name:   jQuery('#customer').val()
            }
        };
        if( jQuery('#table-order-items .trow').length <= 0 )
            return true;
            
        var $form = form.get(0);
        jQuery('#table-order-items .trow').each(function(i, row)
        {
            var item = {
                id:     parseInt($form['products['+i+'][id]'].value),
                qty:    parseInt($form['products['+i+'][qty]'].value),
                name:   $form['products['+i+'][name]'].value,
                price:  parseFloat($form['products['+i+'][price]'].value),
                total:  0,
                
            };
            item.total = item.qty * item.price;
            sale.items.push(item);
        });
        
        var $index = 'sale_store_' + jQuery('#store_id').val();
        var sales = {};
        sales[$index] = sale;
        localStorage.sales = JSON.stringify(sales);
        
        return false;
    };
    this.RestoreSession = function()
    {
		if( !localStorage.sales )
			return false;
		var sales = JSON.parse(localStorage.sales);
		if( !sales )
			return false;
		console.log(sales);
        var index = 'sale_store_' + jQuery('#store_id').val();
        if( !sales[index] )
            return false;
        var sale = sales[index];
		if( sale )
		{
			for(var i in sale.items)
			{
				this.AddProduct(sale.items[i].id, null, sale.items[i].qty);
			}
			//tru to restore customer
			jQuery('#customer_id').val(sale.customer.id ? sale.customer.id : '');
			jQuery('#nit_ruc_nif').val(sale.customer.nit ? sale.customer.nit : '');
			jQuery('#customer').val(sale.customer.name ? sale.customer.name : '');
		}
    };
    this.ResetSession = function()
    {
		localStorage.sales = null;
		return true;
	}
    //##check local storage
    if( !localStorage.sales )
		localStorage.sales = '';
}
function OnWindowResize()
{
	var ph = window.screen.height * 0.6;
	var h = window.screen.height * 0.4;
	jQuery('#pos-container').css('height', (window.innerHeight) + 'px');
	jQuery('#products-container').css('height', ph + 'px');
	//jQuery('#products-table .table-body').css('height', (h - 10) + 'px');
	jQuery('#pos-order-table-container').css('height', h + 'px');
	//jQuery('#pos-order-table-container .table-body').css('height', (h - 10) + 'px');
	//var th = jQuery('.pos-table').parent().height();
	//jQuery('.pos-table').css('height', th+'px');
}
var getting_results = false;
var p_page = 1;
jQuery(function()
{
	window.pos = new MBPos();
	pos.SetEvents();
	jQuery(window).resize(OnWindowResize);
	OnWindowResize();
    pos.RestoreSession();
});
