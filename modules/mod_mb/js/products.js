function onClickCell(index, field, html)
{
	var e = arguments.callee.caller.arguments[0];
	
	if(arguments[1] == 'action')
	{
		//console.log(jQuery(e.target).parent().find('.dropdown-menu').css('display', 'block'));
		var menu = jQuery(e.target).parent().find('.dropdown-menu:first');
		var cell = jQuery(e.target).parents('.datagrid-cell, .datagrid-view2, .datagrid-view, .datagrid-body, .datagrid-wrap, .datagrid');
		cell.css('overflow', 'visible');
		//console.log(menu.css('display'));
		if( menu.css('display') == 'block' )
			menu.css('display', 'none');
		else
			menu.css('display', 'block');
		
		e.stopPropagation();
		//jQuery(html).find('.dropdown-menu').css('display', 'block');
	}
}
var Products = 
{
	PrintCatalog: function(type)
	{
		type = type || 'pdf';
		var link = 'index.php?mod=mb&task=print_catalog&type=' + type;
		if( jQuery('.tcb-select:checked').length > 0 )
		{
			jQuery('.tcb-select:checked').each(function(i, input)
			{
				link += '&ids[]=' + input.value;
			});	
		}
		else
		{
			var store_id 	= jQuery('#filter-store-id').val();
			var cat_id 		= jQuery('#category_id').val();
			if( store_id > 0 )
			{
				link += '&store_id='+store_id;
			}
			if( cat_id > 0 )
			{
				link += '&cat_id='+cat_id;
			}
		}
		var popup = window.open(link);
	}
};
jQuery(function()
{
	jQuery('#btn-print-catalog').click(function()
	{
		jQuery('#modal-print-catalog').modal('show');
		return false;
	});
	jQuery('#btn-build-catalog').click(function()
	{
		jQuery('#modal-print-catalog').modal('hide');
		var type = jQuery('#catalog_type').val();
		Products.PrintCatalog(type);
		return false;
	});
	jQuery('#btn-print-labels').click(function()
	{
		var store_id 	= parseInt(jQuery('#filter-store-id').val());
		var list 		= jQuery('#print-labels-products');
		list.html('');
		if( isNaN(store_id) || store_id <= 0 )
		{
			alert(lt.modules.mb.locale.ERROR_SELECT_A_STORE);
			return false;
		}
		var items = sessionStorage.getItem('selection_products') ? 
						JSON.parse(sessionStorage.getItem('selection_products')) : 
						jQuery('.tcb-select:checked');
		if( items.length <= 0 )
		{
			alert(lt.modules.mb.locale.ALERT_SELECT_PRODUCTS);
			return false;
		}
		jQuery('#print_labels_store_id').val(store_id);
		jQuery.each(items, function(i, obj)
		{
			list.append('<input type="hidden" name="products[]" value="'+(obj.value || obj)+'" />');
		});
		jQuery('#modal-print-labels').modal('show');
		return false;
	});
	function FillCategories(items, space)
	{
		var dropdown = jQuery('#category_id');
		jQuery.each(items, function(i, cat)
		{
			dropdown.append('<option value="'+cat.category_id+'">' + ( space ? (space + ' ') : '') + cat.name+'</option>');
			
			if( cat.childs && cat.childs.length > 0 )
			{
				FillCategories(cat.childs, space + '-');
			}
		});
	}
	//##on store changed
	jQuery('#filter-store-id').change(function()
	{
		jQuery('#category_id').html('<option value="-1">-- categoria --</option>');
		if( this.value <= 0 )
			return true;
		var params = 'mod=mb&task=ajax&action=get_store_cats&store_id=' + this.value + '&time=' + (new Date().getTime());
		jQuery.get('index.php?' + params, function(res)
		{
			if( res.status == 'ok' )
			{
				FillCategories(res.categories, '');
			}
		});
		return false;
	});
	jQuery('.btn-quick-view, .product-name').click(function(e)
	{
		//console.log(e);
		var dataset = null;
		if( jQuery(e.target).hasClass('product-name') )
		{
			dataset = jQuery(e.target).parents('tr').find('.btn-quick-view:first').get(0).dataset;
		}
		else
		{
			dataset = this.dataset;
		}
		jQuery('#quick-view-dialog').find('#_code').html(dataset.code);
		jQuery('#quick-view-dialog').find('#_name').html(dataset.name);
		jQuery('#quick-view-dialog').find('#_qty').html(dataset.qty);
		jQuery('#quick-view-dialog').find('#_cost').html(dataset.cost);
		jQuery('#quick-view-dialog').find('#_price1').html(dataset.price1);
		jQuery('#quick-view-dialog').find('#_price2').html(dataset.price2);
		jQuery('#quick-view-dialog').find('#_price3').html(dataset.price3);
		jQuery('#quick-view-dialog').find('#_price4').html(dataset.price4);
		jQuery('#quick-view-dialog').modal('show');
		return false;
	});
	jQuery('#bulk_action_form').submit(function(e)
	{
		try
		{
			if( this.action.value == 'set_categories' )
			{
				if( jQuery('.tcb-select:checked').length <= 0 )
				{
					alert(lt.modules.mb.locale.ALERT_SELECT_PRODUCTS);
					return false;
				}
				jQuery('#categories-dialog').modal('show');
				return false;
			}
			else if( this.action.value == 'save_selection' )
			{
				if( jQuery('.tcb-select:checked').length <= 0 )
				{
					alert(lt.modules.mb.locale.ALERT_SELECT_PRODUCTS);
					return false;
				}
				var products = sessionStorage.getItem('selection_products');
				if( products )
					products = JSON.parse(products);
				else
					products = [];
					
				jQuery('.tcb-select:checked').each(function(i, input)
				{
					var id = isNaN(parseInt(input.value)) ? 0 : parseInt(input.value);
					
					if( id && products.indexOf(id) == -1 )
					{
						products.push(id);
					}
				});
				sessionStorage.setItem('selection_products', JSON.stringify(products));
				alert(lt.modules.mb.locale.MSG_SELECTION_SAVED);
				return false;
			}
			else if( this.action.value == 'clear_selection' )
			{
				sessionStorage.removeItem('selection_products');
				alert(lt.modules.mb.locale.MSG_SELECTION_WAS_DELETED);
				return false;
			}
			return true;
		}
		catch(e)
		{
			console.log(e);
			return false;
		}
	});
	jQuery('#form-bulk-categories').submit(function()
	{
		var cats = jQuery(this).find('input:checked');
		if( cats.length <= 0 )
		{
			alert(lt.modules.mb.locale.ALERT_SELECT_CATEGORIES);
			return false;
		}
		var params = jQuery(this).serialize();
		
		jQuery('.tcb-select:checked').each(function(i, chk)
		{
			params += '&products[]=' + chk.value;
		});
		jQuery.post('index.php', params, function(res)
		{
			console.log(res);
			if( res.status == 'ok' )
			{
				jQuery('#categories-dialog').modal('hide');
				alert(res.message, function(){window.location.reload();});
			}
		})
		return false;
	});
	jQuery('#btn-build-barcodes').click(function()
	{
		if( jQuery('.tcb-select:checked').length <= 0 )
		{
			alert(lt.modules.mb.locale.ALERT_SELECT_PRODUCTS);
			return false;
		}
		var ids = '';
		jQuery('.tcb-select:checked').each(function(i, checkbox)
		{
			ids += 'ids[]=' + checkbox.value + '&';
		});
		jQuery('#modal-processing').modal('show');
		jQuery.post('index.php?mod=mb&task=ajax&action=buildbarcodes', ids, function(res)
		{
			jQuery('#modal-processing').modal('hide');
			window.location.reload();
		});
		return false;
	});
});
var cint = null;
function sb_start_camera()
{
	//##initialize camera
	window.sb_camera = new SBCamera(document.getElementById('camera-video'), document.getElementById('camera-canvas'));
}
function __capture()
{
	cint = setInterval(function()
	{
		sb_camera.Capture();
		var barcode = getBarcodeFromCanvas('camera-canvas');
		if( !isNaN(barcode) )
		{
			alert(barcode);
		}
	}, 400);
	
}
function __stopcapture()
{
	clearInterval(cint);
}
