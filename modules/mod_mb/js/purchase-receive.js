/**
 * @module MonoBusiness - Purchases
 * @author Sintic Bolivia - Juan Marcelo Aviles Paco
 */
function SBReceivePurchase()
{
	var $this 	= this;
	var $form	= jQuery('#form-purchase');
	var table	= null;

	this.OnChange = function(item, prop)
	{
		if( prop == 'quantity' || prop == 'supply_price' )
		{
			item.supply_price 	= isNaN(parseFloat(item.supply_price)) ? 0 : parseFloat(item.supply_price);
			item.quantity 		= isNaN(parseInt(item.quantity)) ? 0 : parseInt(item.quantity);
			item.discount 		= isNaN(parseFloat(item.discount)) ? 0 : parseFloat(item.discount);
			item.total 			= ((item.supply_price * item.quantity) - item.discount).toFixed(2);
			$this.CalculateTotals();
		}
		else if( prop == 'discount' )
		{
			item.supply_price 	= isNaN(parseFloat(item.supply_price)) ? 0 : parseFloat(item.supply_price);
			item.quantity 		= isNaN(parseInt(item.quantity)) ? 0 : parseInt(item.quantity);
			item.discount 		= isNaN(parseFloat(item.discount)) ? 0 : parseFloat(item.discount);
			item.total 			= ((item.supply_price * item.quantity) - item.discount).toFixed(2);
			$this.CalculateTotals();
		}
	};
	this.CalculateTotals = function()
	{
		var items = table.GetItems();
		var subtotal = 0;
		var tax = 0;
		var total = 0;
		for(item of items)
		{
			subtotal += parseFloat(item.total);
		}
		total = subtotal;
		if( window.invoice_tax )
		{
			tax = subtotal * window.invoice_tax;
			total = subtotal + tax;
		}
		jQuery('#quote-subtotal').html(subtotal.toFixed(2));
		jQuery('#quote-tax').html(tax.toFixed(2));
		jQuery('#quote-total').html(total.toFixed(2));

		return total;
	}
	this.Save = function()
	{
		let form = $form.get(0);
		
		var $items = table.GetItems();
		if( $items.length <= 0 )
		{
			alert('Your purchase order has no items');
			return false;
		}
		var pass = true;
		var msg = '';
		//##validate purchase items
		for(p of $items)
		{
			if( !p.product_id )
			{
				pass = false;
				msg = 'Invalid product id';
				break;
			}
			if( !p.quantity || p.quantity <= 0 )
			{
				pass = false;
				msg = 'Invalid product quantity';
				break;
			}
			if( !p.supply_price || p.supply_price <= 0 )
			{
				pass = false;
				msg = 'Invalid product supply price';
				break;
			}
		}
		if( !pass )
		{
			alert(msg);
			return false;
		}
		if( $this.CalculateTotals() <= 0 )
		{
			alert('Your order has no prices and quantities, please review.');
			return false;
		}
		jQuery('#modal-processing').modal('show');
		var params = $form.serialize() + '&ajax=1&' + table.Serialize('form', ['product_hash']);
		jQuery.post('index.php', params, function(res)
		{
            jQuery('#modal-processing').modal('hide');
			if( res.status )
			{
				if( res.status == 'ok' )
				{
                    window.location = 'index.php?mod=mb&view=purchases.default';
                    /*
					alert(res.message, function()
                    {
                        form.reset();
                    });
                    
					//form.reset();
					if( res.items && items.length )
					{
						//delete table;
						table = new SBTableModel(model, document.getElementById('table-container'));
						for(item of res.items)
						{
							item.product_hash = item.product_id + ':' + item.product_code;
							item.received = item.quantity_received;
							item.quantity_received = 0;
							item.discount = 0;
							table.AddItem(item);
						}
					}
                    */
				}
				else if( res.status == 'error' )
				{
					alert(res.error);
				}
			}
			else
			{
				alert('Unknow error while trying to save the purchase order, contact with administrator');
			}
		});
		return true;
	};
	function setEvents()
	{
		jQuery('#btn-save').click(function(){$this.Save();});
		table.AddEvent('OnNodeAdded', function(item, node, value_key)
		{
			if( value_key == 'quantity_received' )
			{
				if( item.received == item.quantity )
				{
					node.disabled = true;
					jQuery(node).parents('tr').addClass('mb-item-completed');
				}
				return true;
			}
			if( value_key != 'expiration_date' )
				return false;

			jQuery(node).datepicker({
				format: lt.dateformat,
				weekStart: 0,
				autoclose: true,
				todayHighlight: true,
				language: lt.lang
			});
		});
	};
	this.GetTable = function()
	{
		return table;
	};
	table = new SBTableModel(model, document.getElementById('table-container'));
	table.Build();
	setEvents();
}
var mb_purchase = null;
jQuery(function()
{
	mb_purchase = new SBReceivePurchase();
	if( window.items && window.items.length )
	{
        jQuery('#modal-processing').modal('show');
		for(item of items)
		{
			item.product_hash       = item.product_id + ':' + item.product_code;
			item.received           = item.quantity_received;
			item.quantity_received  = item.quantity;
			item.discount           = 0;
			mb_purchase.GetTable().AddItem(item);
		}
        jQuery('#modal-processing').modal('hide');
	}
});
