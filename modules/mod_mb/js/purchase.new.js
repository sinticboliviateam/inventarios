/**
 * @module MonoBusiness - Purchases
 * @author Sintic Bolivia - Juan Marcelo Aviles Paco
 */
function SBPurchase()
{
	var $this 	            = this;
	var search 	            = jQuery('#search_product');
	var $form	            = jQuery('#form-purchase');
	var table	            = null;
	this.pending_product    = null;
    this.tax_rate           = jQuery('#tax_rate').val();
	this.AddItem = function(product)
	{
		var p = product || this.pending_product;
		//console.log(p);
		if( !p )
		{
			window.console && console.log('AddItem: Invalid product or empty');
			return false;
		}
		var hash = p.product_id + ':' + p.product_code;
		if( e_item = table.ItemExists(hash) )
		{
			e_item.quantity++;
			e_item.total = (e_item.quantity * e_item.supply_price).toFixed(2);
			item = e_item;
		}
		else
		{
			var qty = p.quantity || 1;
			var item = {
				quantity: qty,
				supply_price: isNaN(parseFloat(p.product_cost)) ? 0 : parseFloat(p.product_cost).toFixed(2),
				total: isNaN(parseFloat(p.product_cost)) ? 0 : (parseFloat(p.product_cost) * qty).toFixed(2),
				product_hash: hash
			};
			var purchase_id = isNaN(parseInt(jQuery('#purchase_id').val())) ? false : parseInt(jQuery('#purchase_id').val());
			if( !purchase_id )
			{
				item['stock'] = p.product_quantity;
			}
			for(col of model.columns)
			{
				if( typeof p[col.value_key] != 'undefined' )
				{
					item[col.value_key] =  p[col.value_key];
				}
			};		
			table.AddItem(item);
		}
	};
	this.OnAddItem = function(item, exists)
	{
		if( exists )
		{
			item.total = ((item.supply_price * item.quantity) - parseFloat(item.discount)).toFixed(2);
		}
		$this.CalculateTotals();
	};
	this.OnChange = function(item, prop)
	{
		if( prop == 'quantity' || prop == 'supply_price' )
		{
			item.supply_price 	= isNaN(parseFloat(item.supply_price)) ? 0 : parseFloat(item.supply_price);
			item.quantity 		= isNaN(parseInt(item.quantity)) ? 0 : parseInt(item.quantity);
			item.discount 		= isNaN(parseFloat(item.discount)) ? 0 : parseFloat(item.discount);
			item.total 			= ((item.supply_price * item.quantity) - item.discount).toFixed(2);
			$this.CalculateTotals();
		}
		else if( prop == 'discount' )
		{
			item.supply_price 	= isNaN(parseFloat(item.supply_price)) ? 0 : parseFloat(item.supply_price);
			item.quantity 		= isNaN(parseInt(item.quantity)) ? 0 : parseInt(item.quantity);
			item.discount 		= isNaN(parseFloat(item.discount)) ? 0 : parseFloat(item.discount);
			item.total 			= ((item.supply_price * item.quantity) - item.discount).toFixed(2);
			$this.CalculateTotals();
		}
	};
	this.DeleteItem = function()
	{
		table.DeleteItem(this.dataset.item_index);
		$this.CalculateTotals();
	};
	this.CalculateTotals = function()
	{
		var items       = table.GetItems();
		var subtotal    = 0;
		var tax         = 0;
		var total       = 0;
		var discount    = 0;
        
		for(item of items)
		{
			subtotal += parseFloat(item.total);
			discount += parseFloat(item.discount);
		}
        total = subtotal;
        if( $this.tax_rate > 0 )
        {
            tax     = subtotal * $this.tax_rate;
			total   = subtotal + tax;
			jQuery('#total_tax').val(tax);
        }
		/*if( window.invoice_tax )
		{
			tax = subtotal * window.invoice_tax;
			total = subtotal + tax;
		}*/
		jQuery('#quote-subtotal').html(subtotal.toFixed(2));
		jQuery('#quote-discount').html(discount.toFixed(2));
		jQuery('#quote-tax').html(tax.toFixed(2));
		jQuery('#quote-total').html(total.toFixed(2));

		return total;
	};
    this.OnTaxChange = function()
    {
        var op = jQuery(this).find('option:selected').get(0);
        if( op.title )
        {
            jQuery('#tax_id').attr('title', op.title);
        }
		if( !op.dataset.tax )
		{
			$this.tax_rate = 0;
			jQuery('#tax_rate').val($this.tax_rate);
			$this.CalculateTotals();
			return true;
		}
		$this.tax_rate = parseFloat(op.dataset.tax) / 100;
		jQuery('#tax_rate').val($this.tax_rate);
		$this.CalculateTotals();
    };
	this.Save = function()
	{
		let form = $form.get(0);
		if(form.supplier_id.value <= 0 )
		{
			alert('Necesita seleccionar un proveedor');
			return false;
		}
		if( !form.store_id.value || parseInt(form.store_id.value) <= 0 )
		{
			alert('Debe seleccionar una tienda');
			return false;
		}
		if( !form.warehouse_id.value || parseInt(form.warehouse_id.value) <= 0 )
		{
			alert('You need to select a warehouse');
			return false;
		}
		var $items = table.GetItems();
		if( $items.length <= 0 )
		{
			alert('Your purchase order has no items');
			return false;
		}
		var pass = true;
		var msg = '';
		//##validate purchase items
		for(p of $items)
		{
			if( !p.product_id )
			{
				pass = false;
				msg = 'Invalid product id';
				break;
			}
			if( !p.quantity || p.quantity <= 0 )
			{
				pass = false;
				msg = 'Invalid product quantity';
				break;
			}
			/*
			if( !p.supply_price || p.supply_price <= 0 )
			{
				pass = false;
				msg = 'Invalid product supply price';
				break;
			}
			*/
		}
		if( !pass )
		{
			alert(msg);
			return false;
		}
		
		if( $this.CalculateTotals() <= 0 )
		{
			alert('Su orden de pedido no tiene monto, revise los items, precios y catidades');
			return false;
		}
		
		var params = $form.serialize() + '&ajax=1&' + table.Serialize('form', ['product_hash']);
		jQuery.post('index.php', params, function(res)
		{
			if( res.status )
			{
				if( res.status == 'ok' )
				{
					if( res.updated )
					{
						alert(res.message);
					}
					else
					{
						alert(res.message, function(){form.reset();});
						$this.Reset();
					}
				}
				else if( res.status == 'error' )
				{
					alert(res.error);
				}
			}
			else
			{
				alert('Unknow error while trying to save the purchase order, contact with administrator');
			}
		});
		return true;
	};
	function setEvents()
	{
		jQuery('#btn-search-provider').click(function()
		{
			iframe = jQuery('#search-provider-modal iframe').get(0);
			iframe.contentDocument.location = iframe.dataset.src;
			jQuery('#search-provider-modal').modal('show');
			return false;
		});
		jQuery('#btn-add-provider').click(function()
		{
			iframe = jQuery('#create-provider-modal iframe').get(0);
			iframe.contentDocument.location = iframe.dataset.src;
			jQuery('#create-provider-modal').modal('show');
			return false;
		});
		jQuery('#store_id').change(function()
		{
			jQuery('#warehouse_id').html('');
			if( this.value == '-1' )
				return;
			jQuery('#search_product').get(0).dataset.query_data = 'store_id='+this.value;
			jQuery.get('index.php?mod=mb&task=ajax&action=get_warehouses&store_id=' +this.value, function(res)
			{
				jQuery('#warehouse_id').html('');
				jQuery('#warehouse_id').append('<option value="-1">-- almacen --</option>');
				jQuery.each(res.items, function(i, item)
				{
					jQuery('#warehouse_id').append('<option value="'+item.id+'">'+item.name+'</option>');
				});
			});
		});
		jQuery('#search_product').keyup(function(e)
		{
			if( e.keyCode != 13)
				return false;
			var $input			= this;
			var store_id 		= jQuery('#store_id').val();
			var warehouse_id 	= jQuery('#warehouse_id').val();
			if( !mb_purchase.pending_product && this.value.length > 0 )
			{
				//##get product by id/code/barcode
				var params = 'mod=mb&task=ajax&action=search_one_product'+
								`&store_id=${store_id}`+
								`&warehouse_id=${warehouse_id}`+
								`&keyword=${this.value}`;
				jQuery.get('index.php?' + params, function(res)
				{
					if( res.status == 'ok' )
					{
						mb_purchase.AddItem(res.product);
						$input.value = '';
						$input.focus();
						jQuery('.sb-suggestions').css('display', 'none');
					}
				});
			}
			else
			{
				$this.AddItem();
				this.value = '';
				this.focus();
			}
		});
        jQuery('#tax_id').change($this.OnTaxChange);
		jQuery('#btn-save').click(function(){$this.Save();});
		jQuery('#btn-add-item').click(function()
        {
            $this.AddItem();
            $this.pending_product = null;
            search.val('');
            search.focus();
        });
		table.AddEvent('OnNodeAdded', function(item, node, value_key)
		{
			if( value_key != 'expiration_date' )
				return false;
				
			jQuery(node).datepicker({
				format: lt.dateformat,
				weekStart: 0,
				autoclose: true,
				todayHighlight: true,
				language: "es"
			});
		});
		//##button to import from excel
		jQuery('#btn-import-excel').click($this.ImportFromExcel.bind($this));
		jQuery('#form-import').submit($this.DoImport);
	};
	this.GetTable = function()
	{
		return table;
	};
    this.Reset = function()
    {
        let form = $form.get(0);
        form.reset();
        jQuery('#tax_rate').html('0');
        jQuery('#quote-subtotal').html('0.00');
		jQuery('#quote-discount').html('0.00');
		jQuery('#quote-tax').html('0.00');
		jQuery('#quote-total').html('0.00');
        delete table;
        table = new SBTableModel(model, document.getElementById('table-container'));
        table.Build();
    };
    this.ImportFromExcel = function(e)
    {
		jQuery('#modal-import-excel').modal('show');
		return false;
	}
	this.DoImport = function(e)
	{
		var form = this;
		var store_id = parseInt(jQuery('#store_id').val());
		try
		{
			if( isNaN(store_id) || store_id <= 0  )
				throw 'You need to select a store';
			if( isNaN(parseInt(form.xls_code.value)) || parseInt(form.xls_code.value) < 0 )
				throw 'Invalid column code';
			if( isNaN(parseInt(form.xls_qty.value)) || parseInt(form.xls_qty.value) < 0 )
				throw 'Invalid column quantity';
			if( isNaN(parseInt(form.xls_cost.value)) || parseInt(form.xls_cost.value) < 0 )
				throw 'Invalid column cost';
			//var params = jQuery(this).serialize();
			//console.log(this.xls_file);
			if( this.xls_file.files.length <= 0 )
			{
				throw 'You need to select an excel file';
			}
			console.log(this.xls_file.files);
			var xls = this.xls_file.files[0];
			console.log('Name: ' + xls.name);
			console.log('Size: ' + xls.size);
			console.log('Mime: ' + xls.type);
			
			var reader = new FileReader();
			reader.onload = function(e) 
			{
				// handle onload
				//console.log(reader.result);
				var buffer 	= reader.result.split(',')[1];
				
				var data 	= {
					store_id:	store_id,
					code:		form.xls_code.value,
					barcode: 	form.xls_barcode.value,
					name:	 	form.xls_name.value,
					quantity:	form.xls_qty.value,
					cost:		form.xls_cost.value,
					row_start:	form.xls_row_start.value,
					filename:	xls.filename,
					mime:		xls.type,
					buffer:		buffer
				};
				jQuery('#modal-import-excel').modal('hide');
				jQuery('#modal-processing').modal('show');
				jQuery.post('index.php?mod=mb&task=purchases.importexcel', JSON.stringify(data), function(res)
				{
					jQuery('#modal-processing').modal('hide');
					if( res.status )
					{
						if( res.status == 'ok' )
						{
							$this.SetItems(res.items);
							if( res.noProducts && res.noProducts.length > 0 )
							{
								if( vconfirm(res.confirm_message) )
								{
									window.open(res.noProductsLink, '_blank', 'location=yes');
								}
							}							
						}
						else if( res.status == 'error' )
						{
							alert(res.error);
							jQuery('#modal-import-excel').modal('show');
						}
						else
						{
							jQuery('#modal-import-excel').modal('show');
						}
					}
					else
					{
						alert('Unknow error ocurred');
						jQuery('#modal-import-excel').modal('show');
					}
				});
			};
			reader.readAsDataURL(xls);
			//reader.readAsText(xls);
		}
		catch(e)
		{
			alert(e);
			console.log('ERROR: ' + e);
		}
		
		return false;
	};
	this.SetItems = function(items)
	{
		for(var i in items)
		{
			var product = items[i];
			$this.AddItem(product);
		}
	};
	table = new SBTableModel(model, document.getElementById('table-container'));
	table.Build();
	setEvents();
}
//##define the supplier search select callback
window.select_callback = function(obj)
{
	jQuery('#supplier_id').val(obj.id);
	jQuery('#supplier_name').val(obj.name);
	jQuery('#search-provider-modal').modal('hide');
};
var mb_purchase = null;
jQuery(function()
{
	mb_purchase = new SBPurchase();
	if( window.items && window.items.length )
	{
		for(item of items)
		{
			item.product_hash = item.product_id + ':' + item.product_code;
			mb_purchase.GetTable().AddItem(item);
		}
	}
	var completion = new SBCompletion({
		input: document.getElementById('search_product'),
		url: 'index.php?mod=mb&task=ajax&action=search_product',
        loading_gif: lt.baseurl + '/images/spin.gif',
		callback: function(sugesstion)
		{
			mb_purchase.pending_product = sugesstion.dataset;
			//console.log(mb_purchase.pending_product);
		}
	});
});
