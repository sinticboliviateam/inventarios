/**
 * 
 */
function MB_Retail()
{
	var $this = this;
	var row_tpl = '<tr data-product_id={id} data-batch={batch}>\
						<td><span class="counter">{num}</span><input type="hidden" name="products[{index}][id]" value="{id}" /></td>\
						<td><span class="item-code">{code}</span><input type="hidden" name="products[{index}][code]" value="{code}" /></td>\
						<td><span class="item-name">{product}</span><input type="hidden" name="products[{index}][name]" value="{product}" /></td>\
						<td><span class="item-batch">{batch}</span><input type="hidden" name="products[{index}][batch]" value="{batch}" /></td>\
						<td>{exp_date}<input type="hidden" name="products[{index}][exp_date]" value="{exp_date}" /></td>\
						<td><input type="number" min="1" name="products[{index}][qty]" value="{qty}" class="form-control item-qty text-center" /></td>\
						<td><div class="text-right">{price}</div><input type="hidden" name="products[{index}][price]" value="{price}" class="item-price" /></td>\
						<td>\
							<div class="item-total text-right">{total}</div>\
							<input type="hidden" name="items[{index}][total]" value="{total}" class="item-total" />\
						</td>\
						<td>\
							<a href="javascript:;" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></a>\
						</td>\
					</tr>';
	
	this.AddItem = function(item)
	{
		var total_items = jQuery('#order-items tbody tr').length;
		
		var exists = $this.ItemExists(item.product_id, item.batch);
		if( !exists )
		{
			var row = row_tpl
								.replace(/{id}/g, item.product_id)
								.replace(/{num}/g, total_items + 1)
								.replace(/{index}/g, total_items)
								.replace(/{code}/g, item.product_code)
								.replace(/{product}/g, item.product_name)
								.replace(/{batch}/g, item.batch)
								.replace(/{exp_date}/g, item.expiration_date)
								.replace(/{qty}/g, item.use_qty)
								.replace(/{price}/g, item.use_price)
								.replace(/{total}/g, item.use_qty * item.use_price);
			row = jQuery('#order-items tbody').append(row)
			$this.CalculateRowTotal(row);
		}
		else
		{
			var input_qty = jQuery(exists).find('.item-qty');
			var qty 	= parseInt(input_qty.val());
			var price 	= parseFloat(jQuery(exists).find('.item-price').val());
			input_qty.val(qty + parseInt(item.use_qty));
			var total = qty * price;
			jQuery('.item-total').html(total).val(total);
			$this.CalculateRowTotal(exists);
		}
		$this.CalculateTotals();
	};
	this.ItemExists = function(product_id, batch)
	{
		var row = false;
		jQuery('#order-items tbody tr').each(function(i, tr)
		{
			if( tr.dataset.product_id && tr.dataset.product_id == product_id && tr.dataset.batch == batch )
			{
				row = tr;
				return false;
			}
		});
		return row;
	};
	this.ShowProductsSelector = function(e)
	{
		var store_id = parseInt(jQuery('#store_id').val());
		if( store_id <= 0 )
		{
			alert(lt.modules.mb.locale.NEED_TO_SELECT_STORE);
			return false;
		}
		var url = search_product_url + store_id;
		jQuery('#iframe-search').prop('src', url);
		//jQuery('#iframe-search').get(0).contentDocument.location.reload();
		jQuery('#modal-search').modal('show');
		return false;
	};
	this.CalculateRowTotal = function(row)
	{
		var qty = parseInt(jQuery(row).find('.item-qty:first').val());
		var price = parseFloat(jQuery(row).find('.item-price:first').val());
		var tax = 0;//parseFloat(row.find('.item-tax:first').val());
		var total = (qty * price) + tax;
		jQuery(row).find('.item-total').html(total.toFixed(2)).val(total.toFixed(2));
		
		return total;
	};
	this.CalculateTotals = function()
	{
		var rows 		= jQuery('#order-items tbody tr');
		var subtotal 	= 0;
		var tax 		= 0;
		var total 		= 0;
		jQuery.each(rows, function(i, row)
		{
			subtotal += $this.CalculateRowTotal(jQuery(row));
		});
		total = subtotal;
		if( $this.tax_rate > 0 )
		{
			tax = subtotal * $this.tax_rate;
			total = subtotal + tax;
			jQuery('#total_tax').val(tax);
		}
		jQuery('#retail-subtotal').html(subtotal.toFixed(2));
		jQuery('#retail-tax').html(tax.toFixed(2));
		jQuery('#retail-total').html(total.toFixed(2));

		return total;
	};
	this.SearchCustomer	= function(keyword, by)
	{
		var params = 'mod=customers&ajax=1&task=get_customer&id='+keyword+'&by='+by;
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' && res.customer )
			{
				jQuery('#customer_id').val(res.customer.customer_id);
				jQuery('#customer, #invoice_name').val(res.customer.first_name + ' ' + res.customer.last_name);
			}
			else
			{
				jQuery('#customer_id').val(0);
				jQuery('#customer').val('');
			}
		});
		jQuery('#customer').focus();
	};
	this.Submit = function()
	{
		var form = this;
		if( this.nit_ruc_nif.value.trim().length <= 0 )
		{
			alert(lt.modules.mb.locale.NIT_RUC_NIF_EMPTY);
			return false;
		}
		//if( isNaN(parseInt(this.customer_id)) )
		if( this.customer.value.trim().length <= 0 )
		{
			alert(lt.modules.mb.locale.ERROR_CUSTOMER_EMPTY);
			return false;
		}
		if( this['meta[_billing_name]'].value.trim().length <= 0 )
		{
			alert(lt.modules.mb.locale.ERROR_INVOICE_NAME_EMPTY);
			return false;
		}
		if( this.store_id.value <= 0 )
		{
			alert(lt.modules.mb.locale.ERROR_SELECT_STORE);
			return false;
		}
		if( this['meta[_sale_type]'].value <= 0 )
		{
			alert(lt.modules.mb.locale.ERROR_SELECT_SALE_TYPE);
			return false;
		}
		//##disable submit button
		form.querySelector('button[type=submit]').disabled = true;
		var params = jQuery(this).serialize();
		jQuery.post('index.php', params, function(res)
		{
			form.querySelector('button[type=submit]').disabled = false;
			if( res && res.status == 'ok' )
			{
				if( res.eval )
				{
					eval(res.eval);
				}
				jQuery('#success-message').html(res.message);
				jQuery('#success-dialog').modal('show');
				jQuery('#success-dialog').on('hidden.bs.modal', function (e) 
				{
					window.location.reload();
				});
			}
			else
			{
				alert(res.error);
			}
		});
		return false;
	};
	this.DeleteItem = function(e)
	{
		jQuery(this).parents('tr:first').remove();
		$this.CalculateTotals();
		$this.ReorderItems();
		return false;
	};
	this.ReorderItems = function()
	{
		jQuery('#order-items tbody tr').each(function(i, row)
		{
			jQuery(row).find('td').each(function(ci, cell)
			{
				if( ci == 0 )
				{
					jQuery(cell).find('.counter').html(i + 1);
				}
				var input = jQuery(cell).find('input');
				if( input && typeof(input) != 'undefined' )
				{
					var name = input.prop('name');
					if( name && name.indexOf('id') != -1 )
					{
						input.prop('name', 'products['+i+'][id]');
					}
					if( name && name.indexOf('code') != -1 )
					{
						input.prop('name', 'products['+i+'][code]');
					}
					if( name && name.indexOf('name') != -1 )
					{
						input.prop('name', 'products['+i+'][name]');
					}
					if( name && name.indexOf('batch') != -1 )
					{
						input.prop('name', 'products['+i+'][batch]');
					}
					if( name && name.indexOf('exp_date') != -1 )
					{
						input.prop('name', 'products['+i+'][exp_date]');
					}
					if( name && name.indexOf('qty') != -1 )
					{
						input.prop('name', 'products['+i+'][qty]');
					}
					if( name && name.indexOf('price') != -1 )
					{
						input.prop('name', 'products['+i+'][price]');
					}
					if( name && name.indexOf('total') != -1 )
					{
						input.prop('name', 'products['+i+'][total]');
					}
					//console.log(input.attr('name'));
				}
				
			});
			
		});
	};
	function SetEvents()
	{
		jQuery('#btn-add-item').click($this.ShowProductsSelector);
		jQuery('#nit_ruc_nif').keydown(function(e)
		{
			if( e.keyCode == 13 )
			{
				$this.SearchCustomer(this.value, 'nit');
				return false;
			}
		});
		jQuery('#sale-type').change(function()
		{
			if( this.value == 'credit' )
			{
				jQuery('#payment_status').val('pending');
			}
			else
			{
				jQuery('#payment_status').val('complete');
			}
		});
		jQuery('#btn-search-customer').click(function()
		{
			jQuery('#search-customer-modal iframe').get(0).contentDocument.location.reload();
			jQuery('#search-customer-modal').modal('show');
			return false;
		});
		jQuery(document).on('change keydown keyup', '.item-qty', function(e)
		{
			var row = jQuery(this).parents('tr');
			if( e.type == 'keydown' && e.keyCode == 13 )
			{
				$this.CalculateRowTotal(row);
				$this.CalculateTotals();
				return false;
			}
			
			$this.CalculateRowTotal(row);	
			$this.CalculateTotals();
		});
		jQuery(document).on('click', '.btn-delete', $this.DeleteItem);
		jQuery('#form-sale').submit($this.Submit);
	}
	SetEvents();
}
function search_product_callback(item)
{
	if( item.use_price <= 0 )
	{
		alert(lt.modules.mb.locale.THE_PRODUCT_HAS_NO_PRICE);
		return false;
	}
	retail.AddItem(item);
}
//##define the customer search select callback
window.select_callback = function(obj)
{
	jQuery('#search-customer-modal').modal('hide');
	var params = 'mod=customers&ajax=1&task=get_customer&id=' + obj.id;
	jQuery.post('index.php', params, function(res)
	{
		if( res.status == 'ok' )
		{
			jQuery('#customer_id').val(res.customer.customer_id);
			jQuery('#customer').val(res.customer.first_name + ' ' + res.customer.last_name );
			//jQuery('#nit_ruc_nif').val(res.customer._nit_ruc_nif ? res.customer._nit_ruc_nif : '');
		}
		else
		{
			alert(res.error);
		}
	});
};
jQuery(function()
{
	window.retail = new MB_Retail();
});
