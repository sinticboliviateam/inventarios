/**
 * 
 */
function SBTransfer()
{
	var item_tpl = '<tr data-id="{id}">'+
						'<td>{number}<input type="hidden" name="items[{index}][id]" value="{id}" /></td>' +
						'<td>{code}<input type="hidden" name="items[{index}][code]" value="{code}" class="form-control" /></td>' +
						'<td class="column-product">{name}<input type="hidden" name="items[{index}][name]" value="{name}" class="form-control" /></td>' +
						'<td><input type="number" min="1" name="items[{index}][qty]" value="{qty}" class="form-control item-qty" /></td>' +
						'<td><a href="javascript:;" class="remove-item btn btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></a></td>' +
					'</tr>';
	var $this 	= this;
	var table	= jQuery('#purchase-table');
	var search 	= jQuery('#search_product');
	var form	= jQuery('#form-transfer');
	this.ItemExists = function(id)
	{
		var row = table.find('tbody tr[data-id='+id+']');
		return row.length > 0 ? row.get(0) : null;
	};
	this.AddItem = function(item)
	{
		if( !item )
			return false;
				
		if( row = $this.ItemExists(item.product_id) )
		{
			//##update product qty
			var qty = parseInt(jQuery(row).find('.item-qty:first').val());
			qty++;
			jQuery(row).find('.item-qty:first').val(qty);
			$this.CalculateRowTotal(jQuery(row));
		}
		else
		{
			var rows	= table.find('tbody tr').length;
			var row		= item_tpl.replace(/{index}/g, rows)
									.replace('{number}', rows + 1)
									.replace(/{id}/g, item.product_id)
									.replace(/{code}/g, item.product_code)
									.replace(/{name}/g, item.product_name)
									.replace(/{batch}/g, item.batch)
									.replace(/{qty}/g, 1)
									.replace(/{price}/g, item.product_cost)
									.replace(/{total}/g, item.product_cost);
			
			table.find('tbody').append(row);
		}
		search.val('').focus();
		$this.CalculateTotals();
	};
	this.RemoveItem = function(e)
	{
		this.parentNode.parentNode.remove();
		return false;
	};
	this.OnQtyChanged = function(e)
	{
		if( e.type == 'change' || e.type == 'keyup' )
		{
			if( e.type == 'keyup' && e.keyCode != 13 )
			{
				return false;
			}
			
			var row = jQuery(this).parents('tr:first');
			$this.CalculateRowTotal(row);
			$this.CalculateTotals();
		}
	};
	this.OnPriceChanged = function(e)
	{
		var row = jQuery(this).parents('tr:first');
		if( e.keyCode == 13 )
		{
			$this.CalculateRowTotal(row);
			$this.CalculateTotals();
			return false;
		}
		
		$this.CalculateRowTotal(row);
		$this.CalculateTotals();
		return true;
	};
	this.CalculateRowTotal = function(row)
	{
		var qty = parseInt(row.find('.item-qty:first').val());
		var price = parseFloat(row.find('.item-price:first').val());
		var tax = 0;//parseFloat(row.find('.item-tax:first').val());
		var total = (qty * price) + tax;
		row.find('.item-total:first').html(total.toFixed(2));
		
		return total;
	};
	this.CalculateTotals = function()
	{
		var rows = table.find('tbody tr');
		var subtotal = 0;
		var tax = 0;
		var total = 0;
		jQuery.each(rows, function(i, row)
		{
			subtotal += $this.CalculateRowTotal(jQuery(row));
		});
		total = subtotal;
		if( window.invoice_tax )
		{
			tax = subtotal * window.invoice_tax;
			total = subtotal + tax;
		}
		jQuery('#quote-subtotal').html(subtotal.toFixed(2));
		jQuery('#quote-tax').html(tax.toFixed(2));
		jQuery('#quote-total').html(total.toFixed(2));

		return total;
	}
	this.Save = function()
	{
	
		var from_store_id 		= this.from_store_id.value.trim();
		var from_warehouse_id 	= this.from_warehouse_id.value.trim();
		var to_store_id 		= this.to_store_id.value.trim();
		var to_warehouse_id		= this.to_warehouse_id.value.trim();
		
		if( from_store_id.length <= 0 || parseInt(from_store_id) <= 0 )
		{
			alert(lt.modules.mb.locale.SOURCE_STORE_NEEDED);
			return false;
		}
		if( from_warehouse_id.length <= 0 || parseInt(from_warehouse_id) <= 0 )
		{
			alert(lt.modules.mb.locale.SOURCE_WAREHOUSE_NEEDED);
			return false;
		}
		if( to_store_id.length <= 0 || to_store_id <= 0 )
		{
			alert(lt.modules.mb.locale.DESTINATION_STORE_NEEDED);
			return false;
		}
		if( to_warehouse_id.length <= 0 || to_warehouse_id <= 0 )
		{
			alert(lt.modules.mb.locale.DESTINATION_WAREHOUSE_NEEDED);
			return false;
		}
		if( jQuery('#purchase-table tbody tr').length <= 0 )
		{
			alert(lt.modules.mb.locale.TRANSFER_ITEMS_REQUIRED);
			return false;
		}
		var btn = this.querySelector('[type=submit]');
		btn.disabled = true;
		//var data 	= new FormData(this);
		var data = jQuery(this).serialize() + '&ajax=1';
		jQuery.post('index.php', data, function(res)
		{
			btn.disabled = false;
			if(res.status == 'ok')
			{
				alert(res.message, function(){window.location = res.redirect || window.location.reload();});
			}
			else if( res.status == 'error' )
			{
				alert(res.error);
			}
		});
		/*
		var req 	= new XMLHttpRequest();
		req.open('POST', 'index.php');
		req.send(data);
		*/
		return false;
	};
	function setEvents()
	{
		jQuery('.store-dropdown').change(function(e)
		{
			if( this.value.trim() == '' || this.value == -1 )
				return false;
			let $this = this;
			var target_dropdown = jQuery(`#${this.dataset.target_dropdown}`);
			//##get store warehouses
			jQuery.get('index.php?mod=mb&task=ajax&action=get_warehouses&store_id='+this.value, function(res)
			{
				if( res.status )
				{
					if( res.status == 'ok' )
					{
						res.items.forEach(function(item)
						{
							var op = document.createElement('option');
							op.value = item.id;
							op.innerHTML = item.name;
							target_dropdown.append(op);
						});
						
					}
					else
					{
						alert(res.error);
					}
				}
				if( $this.dataset.target_dropdown == 'from_warehouse_id' )
				{
					var warehouse_id = document.getElementById('from_warehouse_id').value;
					jQuery('#search_product').get(0).dataset.query_data = `store_id=${$this.value}&warehouse_id=${warehouse_id}`;
				}
			});
		});
		jQuery('#from_warehouse_id').change(function()
		{
			if( this.value.trim() == '' || this.value == -1 )
				return false;
			var store_id = document.getElementById('from_store_id').value;
			jQuery('#search_product').get(0).dataset.query_data = `store_id=${store_id}&warehouse_id=${this.value}`;
		});
		jQuery('#search_product').keyup(function(e)
		{
			if( e.keyCode != 13)
				return false;
			
			$this.AddItem(window.mb_product);
		});
		jQuery('#btn-add-item').click(function()
		{
			$this.AddItem(window.mb_product);
		});
		jQuery(document).on('click', '.remove-item', $this.RemoveItem);
		jQuery(document).on('keyup change keydown', '.item-qty', $this.OnQtyChanged);
		jQuery(document).on('keyup keydown', '.item-price', $this.OnPriceChanged);
		form.submit($this.Save);
	};
	setEvents();
}
jQuery(function()
{
	window.mb_transfer = new SBTransfer();
	var completion = new SBCompletion({
		input: document.getElementById('search_product'),
		url: 'index.php?mod=mb&task=ajax&action=search_product',
		callback: function(sugesstion)
		{
			//console.log(sugesstion);
			window.mb_product = sugesstion.dataset;
			//mb_transfer.AddItem(jQuery(sugesstion).data('obj'));
		}
	});
});