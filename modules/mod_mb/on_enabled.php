<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Text as SBText;
defined('BASEPATH') or die('Dont fuck with me buddy.');
SB_Language::loadLanguage(LANGUAGE, 'mb', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('mb');
$dbh = SB_Factory::getDbh();
$permissions = array(
		array('group' => 'mb', 'permission' => 'mb_manage_inventory', 'label'	=> __('Manage Inventory (MB)', 'mb')),
		//##stores permissions
		array('group' => 'mb', 'permission' => 'mb_manage_stores', 'label'	=> __('Manage Stores (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_create_store', 'label'	=> __('Create Store (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_edit_store', 'label'	=> __('Edit Store(MB)','mb')),
		array('group' => 'mb', 'permission' => 'mb_delete_store', 'label'	=> __('Delete Store(MB)','mb')),
		array('group' => 'mb', 'permission' => 'mb_see_all_stores', 'label'	=> __('Can see all stores','mb')),
		//##products permissions
		array('group' => 'mb', 'permission' => 'mb_manage_products', 'label' => __('Manage Products (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_create_product', 'label'	=> __('Create Product (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_edit_product', 'label'	=> __('Edit Product (MB)','mb')),
		array('group' => 'mb', 'permission' => 'mb_delete_product', 'label'	=> __('Delete Product (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_view_product', 'label'	=> __('View Product', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_view_products_into_its_store', 'label'	=> __('View products into its store', 'mb')),
		//array('group' => 'mb', 'permission' => 'mb_see_purchase_costs', 'label' => __('Can see product cost', 'mb')),
		//##categories permissions
		array('group' => 'mb', 'permission' => 'mb_manage_categories', 'label'	=> SB_Text::_('Manage Categories (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_create_category', 'label'	=> SB_Text::_('Create Category (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_edit_category', 'label'	=> SB_Text::_('Edit Category (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_delete_category', 'label'	=> SB_Text::_('Delete Category (MB)', 'mb')),
		//##orders permissions
		array('group' => 'mb', 'permission' => 'mb_manage_orders', 'label'	=> SB_Text::_('Manage Orders (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_see_its_orders', 'label'	=> SB_Text::_('See its orders (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_see_all_orders', 'label'	=> SB_Text::_('See all orders (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_create_order', 'label'	=> SB_Text::_('Create Order', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_edit_order', 'label'	=> SB_Text::_('Edit Order (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_delete_order', 'label'	=> SB_Text::_('Delete Order (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_see_deleted_orders', 'label'	=> SB_Text::_('Can see deleted orders (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_revert_sale', 'label'	=> __('Can revert sale', 'mb')),
		//##point of sale permissions
		array('group' => 'mb', 'permission' => 'mb_sell', 'label'	=> SB_Text::_('Sell Products (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_settings', 'label'	=> SB_Text::_('Mono Business Settings', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_see_cost', 'label'	=> SB_Text::_('Mono Business can see product cost', 'mb')),
		//##purchase orders
		array('group' => 'mb', 'permission' => 'mb_can_manage_purchases', 'label'=> SB_Text::_('Mono Business can manage purchases', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_create_purchase', 'label'	=> SB_Text::_('Can Create Purchases', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_edit_purchase', 'label'	=> SB_Text::_('Mono Business can edit purchase', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_delete_purchase', 'label'	=> SB_Text::_('Mono Business can delete purchase', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_receive_stock', 'label'	=> SB_Text::_('Mono Business can receive stock', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_revert_purchase', 'label'	=> SB_Text::_('Mono Business can revert purchase', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_can_print_purchase', 'label'	=> SB_Text::_('Mono Business can revert purchase', 'mb')),
		//##product type
		array('group' => 'mb', 'permission' => 'mb_manage_product_types', 'label' => __('Manage Product Types', 'mb')),
		//##measure units
		array('group' => 'mb', 'permission' => 'mb_manage_measurement_units', 'label' => __('Manage Measure Units', 'mb')),
		//##taxes
		array('group' => 'mb', 'permission' => 'mb_manage_product_taxes', 'label' => __('Manage Measure Units', 'mb')),
		//##coupons
		array('group' => 'mb', 'permission' => 'mb_manage_coupons', 'label' => __('Manage Coupons', 'mb')),
		//##transfers
		array('group' => 'mb', 'permission' => 'mb_manage_transfers', 'label' => __('Manage Transfers', 'mb')),
		array('group' => 'mb', 'label' => __('Create Transfer', 'mb'), 'permission' => 'mb_create_transfer'),
		array('group' => 'mb', 'label' => __('Receive Transfer', 'mb'), 'permission' => 'mb_receive_transfer'),
		array('group' => 'mb', 'label' => __('Revert Transfer', 'mb'), 'permission' => 'mb_revert_transfer'),
		array('group' => 'mb', 'label' => __('Void Transfer', 'mb'), 'permission' => 'mb_void_transfer'),
		array('group' => 'mb', 'label' => __('Edit Transfer', 'mb'), 'permission' => 'mb_edit_transfer'),
		array('group' => 'mb', 'label' => __('Delete Transfer', 'mb'), 'permission' => 'mb_delete_transfer'),
		array('group' => 'mb', 'label' => __('View All Transfers', 'mb'), 'permission' => 'mb_view_all_transfers'),
		array('group' => 'mb', 'label' => __('View Its Transfers', 'mb'), 'permission' => 'mb_view_its_transfers'),
		//##warehouses
		array('group' => 'mb', 'permission' => 'mb_manage_warehouses', 'label' => __('Manage Warehouses', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_manage_batches', 'label' => __('Manage Batches', 'mb')),
		//##transaction types
		array('group' => 'mb', 'permission' => 'mb_manage_transaction_types', 'label' => __('Manage Transaction Types', 'mb')),
		//##reports
		array('group' => 'mb', 'permission' => 'mb_build_reports', 'label' => __('Build Reports', 'mb')),
		//##import/export
		array('group' => 'mb', 'permission' => 'mb_import', 'label'	=> SB_Text::_('Import Products (MB)', 'mb')),
		array('group' => 'mb', 'permission' => 'mb_export', 'label'	=> SB_Text::_('Export Products', 'mb')),
        //##from version 1.0.0
        array('group' => 'mb', 'permission' => 'mb_menu_transactions', 'label'	=> __('Menu Transaction Records', 'mb')),
		['group' => 'mb', 'permission' => 'mb_menu_brands', 'label' => __('Brands Menu', 'mb')]
        
);
sb_add_permissions($permissions);
