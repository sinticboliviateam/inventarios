<div id="modal-print-labels" class="modal fade" data-backdrop="static">
	<form action="" method="" target="_blank" id="form-modal-print-label" class="modal-dialog modal-sm" role="document">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="print_labels" />
		<input type="hidden" name="store_id" value="0" id="print_labels_store_id" />
		<input type="hidden" name="category_id" value="0" />
		<div id="print-labels-products"></div>
		<div class="modal-content">
			<div class="modal-header"><h3 class="modal-title"><?php _e('Print Product Labels', 'mb'); ?></h3></div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label><?php _e('Columns', 'mb'); ?></label>
							<input type="number" min="4" name="columns" value="4" class="form-control" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
								<?php _e('Print Barcode', 'mb'); ?>
								<input type="checkbox" name="print_barcode" value="1" />
							</label>
							
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
								<?php _e('Print Price', 'mb'); ?>
								<input type="checkbox" name="print_price" value="1" />
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-danger" data-dismiss="modal"><?php _e('Cancelar', 'mb'); ?></a>
				<button type="submit" class="btn btn-success"><?php _e('Build', 'mb'); ?></button>
			</div>
		</div>
	</form>
</div>
