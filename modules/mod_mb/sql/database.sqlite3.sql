CREATE TABLE IF NOT EXISTS mb_stores ( 
    store_id                        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    code                            varchar(64),
    store_name                      varchar(128),
    store_address                   varchar(256),
    phone							varchar(128),
    fax								varchar(128),
    store_key                       VARCHAR( 150 )  UNIQUE,
    store_description               VARCHAR( 250 ),
    store_type                      VARCHAR( 100 ),
    main_store                      tinyint(1),
    tax_id                          INTEGER,
    sales_transaction_type_id       INTEGER,
    purchase_transaction_type_id    INTEGER,
    refund_transaction_type_id      INTEGER, 
    last_modification_date          DATETIME,
    creation_date                   DATETIME
);
CREATE TABLE IF NOT EXISTS mb_store_meta ( 
    meta_id       INTEGER         NOT NULL PRIMARY KEY AUTOINCREMENT,
    meta_key      VARCHAR( 128 ),
    meta_value    TEXT,
    store_id      INTEGER         NOT NULL,
    creation_date DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_warehouse(
	id				integer not null primary key autoincrement,
	store_id		integer not null,
	name			varchar(128),
	phone			varchar(64),
	address			varchar(128),
	location		varchar(256),
	creation_date	datetime
);
CREATE TABLE IF NOT EXISTS mb_batch(
	id				integer not null primary key autoincrement,
	warehouse_id	integer not null,
	name			varchar(128),
	creation_date	datetime
);
CREATE TABLE IF NOT EXISTS mb_categories ( 
    category_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name          TEXT,
    description   TEXT,
    parent        INTEGER,
    extern_id     INT,
    store_id      INT,
	slug			varchar(128),
    extern_type		varchar(64), 
	image_id		integer,
    creation_date DATETIME
);
CREATE TABLE IF NOT EXISTS mb_products ( 
    product_id				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    extern_id              	INTEGER,
    extern_type				varchar(64), 
    product_code           	VARCHAR( 100 ),
    product_number			VARCHAR(128),
    stocking_code			VARCHAR(128),
    product_name           	VARCHAR( 250 ),
    product_description    	TEXT,
    product_line_id        	INTEGER,
    type_id					integer,
    product_model          	VARCHAR( 250 ),
    product_barcode        	VARCHAR( 100 ),
    product_cost           	DECIMAL( 10, 2 ),
    product_price          	DECIMAL( 10, 2 ),
    product_price_2        	DECIMAL( 10, 2 ),
    product_price_3        	DECIMAL( 10, 2 ),
    product_price_4        	DECIMAL( 10, 2 ),
    product_quantity       	integer default 0,
    product_unit_measure   	INTEGER,
    store_id               	INTEGER,
	warehouse_id			INTEGER,
    user_id                	INTEGER,
    department_id			INTEGER,
    status                 	VARCHAR( 50 ),
    min_stock             	integer,
    product_internal_code  	VARCHAR( 100 ),
    shipping_weight			varchar(100),
    width					decimal(10,2),
    height					decimal(10,2),
    unit_pack				integer,
    base_type				varchar(128) default 'base',
	slug					varchar(128),
    for_sale        		tinyint(2),
	last_modification_date 	DATETIME,
    creation_date          	DATETIME
);
CREATE TABLE IF NOT EXISTS mb_product_quantity(
	id						integer not null primary key autoincrement,
	store_id				integer not null,
	warehouse_id			integer not null,
	batch_id				integer not null default 0,
	product_id				integer not null,
	quantity				integer,
	min_stock				integer,
	expiration_date			date
);
CREATE TABLE IF NOT EXISTS mb_product_meta ( 
    meta_id                INTEGER     NOT NULL    PRIMARY KEY AUTOINCREMENT,
    product_id             INTEGER  NOT NULL,
    meta_key               VARCHAR(128),
    meta_value             TEXT,
    last_modification_date DATETIME,
    creation_date          DATETIME 
);
create table if not exists mb_product_kardex(
		id 						integer not null primary key autoincrement,
		product_id	 			integer not null,
		in_out 					varchar(16),
		quantity 				integer not null,
		quantity_balance 		decimal(10, 2),
		unit_price 				decimal(10, 2),
		total_amount 			decimal(10, 2),
		monetary_balance 		decimal(10, 2),
		transaction_type_id 	integer,
		author_id 				integer not null,
		transaction_id			integer not null,
		batch_id				integer,
		batch_code				varchar(64),
		cost					decimal(10,2),
		cost_fixed				decimal(10,2),
		cost_weighted_average 	decimal(10,2),
		cost_fifo				decimal(10,2),
		creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_product_sn(
	sn_id					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	product_id				INTEGER NOT NULL,
	sn						varchar(256),
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_tags(
	tag_id					integer not null primary key autoincrement,
	tag						varchar(256),
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_product2tag(
	id						integer not null primary key autoincrement,
	product_id				integer not null,
	tag_id					integer not null,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_unit_measures(
	measure_id				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	name					varchar(128),
	code					varchar(64),
	quantity				integer,
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS `mb_product_lines`(
	line_id					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	name					VARCHAR(128),
	description				VARCHAR(512),
	store_id				INTEGER,
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_departments(
	department_id			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	store_id				integer,
	name					varchar(128),
	description				text,
	address					varchar(256),
	telephone				varchar(64),
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_purchase_orders(
	order_id 				integer not null primary key autoincrement,
	code					varchar(128),
	name					varchar(256),
	store_id 				integer not null,
	warehouse_id 			integer default 0,
	supplier_id				integer,
	transaction_type_id		integer default 0,
	tax_id                  integer default 0,
	sequence				integer not null,
	items					integer,
	tax_rate                decimal(10,2),
	subtotal				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	r_subtotal				decimal(10,2),
	r_total_tax				decimal(10,2),
	r_discount				decimal(10,2),
	r_total					decimal(10,2),
	details					text,
	status					varchar(128),
	user_id					integer,
	order_date				datetime,
	delivery_date			datetime,	
	last_modification_date 	datetime,
	creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_purchase_order_meta(
	id						integer not null primary key autoincrement,
	order_id				integer not null,
	meta_key				varchar(128),
	meta_value				text,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_purchase_order_items(
	item_id					integer not null primary key autoincrement,
	product_id				integer not null,
	order_id				integer not null,
	batch_id				integer,
	batch_code				varchar(64),
	name					varchar(250),
	quantity				integer,
	quantity_received		integer default 0,
	supply_price			decimal(10,2),
	subtotal				decimal(10,2),
	tax_rate				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	r_subtotal				decimal(10,2),
	r_total_tax				decimal(10,2),
	r_total					decimal(10,2),
	status					varchar(128),
	expiration_date			date,
	last_modification_date 	datetime,
	creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_purchase_order_deliveries(
	id						integer not null primary key autoincrement,
	order_id				integer not null,
	item_id					integer not null,
	user_id					integer not null,
	quantity_ordered		integer,
	quantity_delivered		integer,
	supply_price			decimal(10,2),
	sub_total				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_suppliers ( 
    supplier_id             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    store_id				integer,
    supplier_name           varchar(128),
    supplier_address        varchar(256),
    supplier_address_2      varchar(256),
    supplier_telephone_1    TEXT,
    supplier_telephone_2    TEXT,
    fax						varchar(64),
    supplier_details        TEXT,
    supplier_city           TEXT,
    supplier_email          TEXT,
    supplier_contact_person varchar(256),
    country					varchar(128),
    bank_name               VARCHAR( 250 ),
    bank_account            VARCHAR( 100 ),
    nit_ruc_nif             VARCHAR( 50 ),
    supplier_key            VARCHAR( 10 ),
    last_modification_date  DATETIME,
    creation_date           DATETIME
);
CREATE TABLE IF NOT EXISTS mb_supplier_categories(
	category_id				integer not null primary key autoincrement,
	name					varchar(256),
	parent					integer default 0,
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_transaction_types ( 
    transaction_type_id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    transaction_key         varchar(64),
    transaction_name        varchar(128),
    transaction_description TEXT,
    in_out                  varchar(64),
    store_id                INTEGER NOT NULL DEFAULT 0,
    last_modification_date  datetime,
    creation_date           datetime
);
CREATE TABLE IF NOT EXISTS mb_tax_rates(
	tax_id 					integer not null primary key autoincrement,
	code					varchar(256),
	name					varchar(256),
	rate					decimal(10, 2),
	is_default				tinyint(1) default 0,
	last_modification_date	datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_currencies(
	currency_id				integer not null primary key autoincrement,
	code					varchar(10),
	name					varchar(128),
	rate					decimal(10, 2),
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_item_types(
	item_type_id			integer not null primary key autoincrement,
	code					varchar(128),
	name					varchar(128),
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_product2suppliers(
	id						integer not null primary key autoincrement,
	product_id				integer not null,
	supplier_id				integer not null,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_product2category ( 
    id            INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
    product_id    INTEGER  NOT NULL,
    category_id   INTEGER  NOT NULL,
    creation_date DATETIME 
);
CREATE TABLE IF NOT EXISTS mb_assemblie2product(
	id						integer not null primary key autoincrement,
	assembly_id				integer not null,
	product_id				integer not null,
	qty_required			integer,
	unit_measure_id			integer not null,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_product_adjustments(
	adjustment_id 			integer not null primary key autoincrement,
	code					varchar(128),
	store_id				integer not null,
	product_id				integer not null,
	user_id					integer not null,
	note					text,
	old_qty					integer,
	new_qty					integer,
	difference				integer,
	status					varchar(128),
	adjustment_date			datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_product_types(
	type_id					integer not null primary key autoincrement,
	type					varchar(128),
	code					varchar(128),
	description				text,
	store_id				int,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_orders(
	order_id 				integer not null primary key autoincrement,
	code					varchar(128),
	name					varchar(256),
	store_id 				integer not null,
	supplier_id				integer,
	transaction_type_id		integer,
	sequence				integer not null,
	items					integer,
	subtotal				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	details					text,
	status					varchar(128),
	payment_status			varchar(64) default 'pending',
	user_id					integer,
	customer_id				integer,
	order_date				datetime,
	delivery_date			datetime,
	type					varchar(128),	
	last_modification_date 	datetime,
	creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_order_meta(
	meta_id					integer not null primary key autoincrement,
	order_id				integer not null,
	meta_key				varchar(128),
	meta_value				text,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_order_items(
	item_id					integer not null primary key autoincrement,
	product_id				integer not null,
	order_id				integer not null,
	name					varchar(250),
	quantity				integer,
	price					decimal(10,2),
	subtotal				decimal(10,2),
	tax_rate				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	status					varchar(128),
	last_modification_date 	datetime,
	creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_order_item_meta(
	id						integer not null primary key autoincrement,
	item_id					integer not null,
	meta_key				varchar(128),
	meta_value				text,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_coupons(
	coupon_id				integer not null primary key autoincrement,
	code					varchar(64),
	description				varchar(512),
	discount				decimal(10,2),
	usage_limit				integer,
	min_amount_required		decimal(10,2),
	start_date				datetime,
	end_date				datetime,
	status					varchar(64),
	creation_date			date
);
CREATE TABLE IF NOT EXISTS mb_transfers(
	id						integer not null primary key autoincrement,
	user_id					integer not null,
	from_store				integer not null,
	from_warehouse			integer not null,
	to_store				integer not null,
	to_warehouse			integer not null,
	to_sequence				integer not null,
	from_sequence			integer not null,
	from_lot				varchar(128),
	to_lot					varchar(128),
	sequence				integer,
	details					text,
	status					varchar(64),
	receiver_id				integer,
	reception_date			datetime,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_transfer_items(
	id						integer not null primary key autoincrement,
	transfer_id				integer not null,
	product_id				integer not null,
	batch 					varchar(64),
	quantity				integer,
	price					decimal(10,2),
	status					varchar(64) default 'pending',
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_cashbox(
	id						integer not null primary key autoincrement,
	cashier_id				integer not null,
	today					datetime,
	initial_amount			decimal(10,2),
	final_amount			decimal(10,2),
	notes					text,
	status					varchar(64)
);
CREATE TABLE IF NOT EXISTS mb_cashcount(
	id						integer not null primary key autoincrement,
	cashier_id				integer not null,
	store_id				integer not null,
	cashbox_id				integer not null,
	sales					decimal(10,2),
	spends					decimal(10,2),
	calculated				decimal(10,2),
	cash					decimal(10,2),
	diff					decimal(10,2),
	data					text,
	creation_date			datetime
);
create table if not exists mb_tweaks(
	id					integer not null primary key autoincrement,
	store_id			integer not null,
	transaction_type_id	integer not null,
	user_id				integer not null,
	`sequence`			integer not null,
	tipo				varchar(64),
	causa				varchar(64),
	details				varchar(512),
	subtotal			decimal(10,2),
	total				decimal(10,2),
	status				varchar(64),
	transaction_date	datetime,
	creation_date		datetime
);
create table if not exists mb_tweak_items(
	id					integer not null primary key autoincrement,
	tweak_id			integer not null,
	product_id			integer not null,
	product_code		varchar(128),
	quantity			integer not null,
	cost				decimal(10,2),
	total				decimal(10,2),
	creation_date		datetime
);
