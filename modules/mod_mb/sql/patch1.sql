alter table mb_tax_rates add column is_default				tinyint(1) default 0 after rate;
alter table mb_orders add column transaction_type_id		integer after supplier_id;
-- transfers
alter table mb_transfer_items add column status					varchar(64) default 'pending' after price;
