<?php
?>
<style>
.cashcount{font-size:11px;}
table{border-collapse: collapse;border:1px solid #000;}
table th, table td{border:1px solid #000;}
</style>
<div class="cashcount">
	<h2 style="text-align:center;">
		<?php _e('Conciliacion'); ?><br/>
		<span style="font-size:13px;"><?php print sb_format_datetime($cashcount->creation_date); ?></span>
	</h2>
	<p>
		<b><?php printf("%s %d", __('Saldo Anterior:'), $cashcount->data->balance); ?></b>
	</p>
	<h4 style="text-align:center;font-size:17px;"><?php _e('Ingresos'); ?></h4>
	<table style="width:100%;">
	<thead>
	<tr>
		<th>Item</th>
		<th>Precio</th>
	</tr>
	</thead>
	<tbody>
	<?php $total = 0; foreach($sales as $s): $total += $s->total; ?>
	<tr>
		<td><?php print mb_get_order_meta($s->order_id, '_fl_estilo'); ?></td>
		<td style="text-align:right;"><?php print number_format($s->total, 2, '.', ','); ?></td>
	</tr>
	<?php endforeach; ?>
	<tfoot>
		<td style="text-align:right;"><b><?php _e('Total'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
	</tfoot>
	</tbody>
	</table>
	<br/>
	<h4 style="text-align:center;font-size:17px;"><?php _e('Gastos'); ?></h4>
	<table style="width:100%;">
	<thead>
	<tr>
		<th>Detalle</th>
		<th>Monto</th>
	</tr>
	</thead>
	<tbody>
	<?php $total = 0; foreach($expensed as $e): $total += $e->amount; ?>
	<tr>
		<td><?php print $e->detail; ?></td>
		<td style="text-align:right;"><?php print number_format($e->amount, 2, '.', ','); ?></td>
	</tr>
	<?php endforeach; ?>
	<tfoot>
		<td style="text-align:right;"><b><?php _e('Total'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($total, 2, '.', ','); ?></b></td>
	</tfoot>
	</tbody>
	</table>
	<br/>
	<table>
	<tr>
		<td><b><?php _e('Efectivo en Caja:'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($cashcount->calculated, 2, '.', ','); ?></b></td>
	</tr>
	<tr>
		<td><b><?php _e('Efectivo Verificado en Caja:'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($cashcount->cash, 2, '.', ','); ?></b></td>
	</tr>
	<tr>
		<td><b><?php _e('Diferencia:'); ?></b></td>
		<td style="text-align:right;"><b><?php print number_format($cashcount->diff, 2, '.', ','); ?></b></td>
	</tr>
	</table>
</div>