<?php
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

$statuses = mb_get_order_statuses();
?>
<style>
*{font-size:10px;font-family:Helvetica,Verdana,Arial;}
.purchase-order{font-size:11px;font-family:Helvetica, Verdana, Arial;}
.text-center{text-align:center;}
.col-num,.col-code,.col-product,.col-price,.col-total,.col-qty{border:1px solid #000;padding:2px;}
.col-num{text-align:center;}
.col-price,.col-total{text-align:right;}
.col-qty{text-align:center;}
.bg-color{background-color:#bgbgbg;color:#000;}
.total{text-align:right;padding:2px;border:1px solid #000;font-weight:bold;}
table{width:100%;border-collapse:collapse;}
.table th, .table td{border:1px solid #000;padding:2px;}
.table th{background-color:#bcbcbc;color:#000;}
</style>
<div class="purchase-order">
	<table>
	<tr>
		<td style="width:33.33%;text-align:center;">
			<?php if( isset($ops->business_logo) && file_exists(UPLOADS_DIR . SB_DS . $ops->business_logo) ): ?>
			<img src="<?php print UPLOADS_URL . '/' . $ops->business_logo; ?>" alt="" style="width:200px;" />
			<?php endif; ?>
		</td>
		<td style="width:33.33%">
			<h2 style="text-align:center;font-size:20px;margin:0;padding:0;"><?php _e('Purchase Order', 'mb'); ?></h2>
			<div style="text-align:center;"><b><?php print $statuses[$purchase->status]; ?></b></div>
		</td>
		<td style="width:33.33%"></td>
	</tr>
	</table>
	<table style="width:100%;">
	<tr>
		<td style="width:15%;"><b><?php _e('Date:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print sb_format_date($purchase->order_date); ?></td>
		<td style="width:15%;text-align:right;"><b><?php _e('Order No.:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print sb_fill_zeros($purchase->sequence); ?></td>
	</tr>
	<tr>
		<td style="width:15%;"><b><?php _e('User:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php $user = new SB_User($purchase->user_id); print $user->username; ?></td>
		<td style="width:15%;text-align:right;"><b><?php _e('Store:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print $purchase->store->store_name; ?></td>
	</tr>
	<tr>
		<td style="width:15%;"><b><?php _e('Supplier:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print $purchase->supplier->supplier_name; ?></td>
		<td style="width:15%;text-align:right;"><b><?php _e('Warehouse:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print $purchase->warehouse->name; ?></td>
	</tr>
	<tr>
		<td style="width:15%;"><b><?php _e('Notes:', 'mb'); ?></b></td>
		<td colspan="3"><?php print $purchase->details; ?></td>
	</tr>
	</table>
	<h3 style="text-align:center;"><?php _e('Order Items', 'mb'); ?></h3>
	<table class="table">
	<thead>
	<tr>
		<th class="bg-color col-num"><?php _e('No.', 'mb'); ?></th>
		<th class="bg-color col-code"><?php _e('Code', 'mb'); ?></th>
		<th class="bg-color col-product"><?php _e('Product', 'mb'); ?></th>
		<th class="bg-color col-qty"><?php _e('Qty', 'mb'); ?></th>
		<th class="bg-color col-price"><?php _e('Price', 'mb'); ?></th>
		<th class="bg-color col-price"><?php _e('Discount', 'mb'); ?></th>
		<th class="bg-color col-total"><?php _e('Total', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($purchase->GetItems() as $item): ?>
	<tr>
		<td class="col-num"><?php print $i; ?></td>
		<td class="col-code"><?php print $item->product_code; ?></td>
		<td class="col-product"><?php print $item->product_name ? $item->product_name : $item->name; ?></td>
		<td class="col-qty"><?php print $item->quantity; ?></td>
		<td class="col-price"><?php print sb_number_format($item->supply_price); ?></td>
		<td class="col-price"><?php print sb_number_format($item->discount); ?></td>
		<td class="col-total"><?php print sb_number_format($item->total); ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="3" class=" total" style=""><?php _e('Total:', 'mb'); ?></td>
		<td></td>
		<td></td>
		<td><div style="text-align:right;"><?php print sb_number_format($purchase->discount); ?></div></td>
		<td class="total"><?php print sb_number_format($purchase->total); ?></td>
	</tr>
	</tfoot>
	</table>
	<br/><br/><br/><br/><br/>
	<table style="width:100%;">
	<tr>
		<td style="width:50%;text-align:center;font-weight:bold;">
			--------------------------------------------<br/>
			<?php _e('Authorization Signature', 'mb'); ?>
		</td>
		<td style="width:50%;text-align:center;font-weight:bold;">
			--------------------------------------------<br/>
			<?php _e('Supplier Signature', 'mb'); ?>
		</td>
	</tr>
	</table>
</div>
<script>
function mb_print()
{
	this.print({bUI: false, bSilent: true, bShrinkToFit: true});
}
</script>