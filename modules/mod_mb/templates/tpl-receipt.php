<?php
?>
<style>
*{font-family:Helvetica, Verdana,Arial;font-size:12px;}
table{border-collapse:collapse;width:100%;}
.col-qty, .col-num{text-align:center;width:15%;}
.col-price{text-align:right;width:20%;}
.col-product{width:46%;}
#table-items tbody td, #table-items thead th{border:1px solid #000;}
th{text-align:center;color:#fff;background-color:#6e9e07;font-weight:bold;}
</style>
<div id="header" style="position:fixed;top:-20px;right:0;left:0;">
	<table style="width:100%;">
	<tr>
		<td style="width:33.33%;">
			<img src="<?php print $logo_url; ?>" alt="" style="width:250px;" />
		</td>
		<td style="width:33.33%;">
			&nbsp;
		</td>
		<td style="width:33.33%;font-size:10px;text-align:center;">
			<?php print $settings->business_name; ?><br/>
			<?php print $settings->business_address; ?><br/>
			<?php print $settings->business_phone; ?><br/>
			<?php printf("%s - %s", $settings->business_city, $settings->business_country); ?><br/>
		</td>
	</tr>
	</table>
</div>
<br/><br/><br/><br/><br/>
<div style="text-align:center;font-size:19px;font-weight:bold;">
	<?php printf("%s Num. %s", __('Sale Receipt', 'mb'), sb_fill_zeros($order->sequence)); ?><br/>
	
</div>
<br/>
<div>
	<table>
	<tr>
		<td style="width:15%;"><b><?php _e('Customer:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php printf("%s %s", $order->customer->first_name, $order->customer->last_name) ?></td>
		<td style="width:15%;"><b><?php _e('Date:', 'mb'); ?></b></td>
		<td style="width:35%;"><?php print sb_format_date($order->creation_date); ?></td>
	</tr>
	</table>
	<h2 style="text-align:center;"><?php _e('Detail', 'mb'); ?></h2>
	<table id="table-items">
	<thead>
	<tr style="background-color:#5252C2;color:#fff;">
		<th class="col-num"><?php _e('No.', 'mb'); ?></th>
		<th class="col-product"><?php _e('Product', 'mb'); ?></th>
		<th class="col-qty"><?php _e('Qty', 'mb'); ?></th>
		<th class="col-price" style="text-align:center;"><?php _e('Price', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($order->GetItems() as $item): ?>
	<tr>
		<td class="col-num"><?php print $i; ?></td>
		<td class="col-product"><?php print $item->product_name ? $item->product_name : $item->name; ?></td>
		<td class="col-qty"><?php print $item->quantity; ?></td>
		<td class="col-price"><?php print $item->price; ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tfoot>
	<?php /*
	<tr>
		<td colspan="3" style="text-align:right;font-weight:bold;border: 1px solid #fff;"><?php _e('Tax', 'mb'); ?></td>
		<td style="text-align:right;font-weight:bold;"><?php print $order->total_tax; ?></td>
	</tr>
	*/?>
	<tr>
		<td colspan="3" style="text-align:right;font-weight:bold;border: 1px solid #fff;"><?php _e('Total', 'mb'); ?></td>
		<td style="text-align:right;font-weight:bold;"><?php print $order->total; ?></td>
	</tr>
	</tfoot>
	</table>
	<?php b_do_action('mb_order_receipt', $order); ?>
	<p>
		<?php _e('If you have some doubt of question, you can contact us at any moment to: ', 'mb'); ?>
	</p>
	<p>
		<b><?php _e('Telephone: ', 'mb'); ?></b><?php print @$business_phone; ?><br/>
	</p>
</div>