<?php
//print_r($order);
?>
<!doctype html>
<html>
<head>
    <title>xx</title>
    <style type="text/css">
    html,body{font-family:Courier;font-size:11px;width:270px;overflow:hidden;margin:0;padding:0;}
    table{border-collapse:collapse;}
    @media print
    {
        @page 
        {
            margin:0;
            padding:0;
        }
    }
    .text-center{text-align:center;}
    </style>
</head>
<body>
<div id="invoice-container">
<?php ?>
	<div class="text-center"><?php print $business_name ?></div>
	<div class="text-center"><?php print $business_branch_office ?></div>
	<div class="text-center"><?php print $business_address ?></div>
	<div class="text-center"><?php print $business_city ?> - <?php print @$business_country ?></div>
	<div class="text-center">SUCURSAL: <?php print $order->GetStore()->store_name; ?></div>
	<div class="text-center">NIT:<?php print @$business_nit_ruc_nif; ?></div>
    <div class="text-center"><?php print @$business_phone; ?></div>
	<div class="text-center"><?php _e('Sale Num.', 'mb'); ?> <?php print sb_fill_zeros($order->sequence); ?></div>
    <hr/>
    <div style="">
        <?php _e('Date:', 'mb'); ?> <?php print sb_format_datetime($order->order_date); ?>
    </div>
    <div>
        Se&ntilde;or(es): <?php printf("%s %s", $order->customer->first_name, $order->customer->last_name); ?>
    </div>
    <div>
        NIT/CI: <?php print $order->customer->_nit_ruc_nif; ?>
    </div>
    <div>
        Recibo Nro.: <?php print $order->_recibo_nro; ?>
    </div>
	<table id="invoice-table-items" style="width:100%;">
	<thead>
	<tr>
		<th>Cant.</th>
		<th>Cod.</th>
		<th>Detalle</th>
		<th>Precio</th>
		<th>Total</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($order->GetItems() as $item): ?>
	<tr>
		<td id="item-qty" style="text-align:center;"><?php print $item->quantity; ?></td>
		<td id="item-code"><?php print $item->product_code; ?></td>
		<td id="item-product"><?php print $item->product_name; ?></td>
		<td id="item-price" style="text-align:right;"><?php print number_format($item->price, 2); ?></td>
		<td id="item-total" style="text-align:right;"><?php print number_format($item->total, 2); ?></td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="4">Total:</td>
		<td style="text-align:right;"><?php print $order->total; ?></td>
	</tr>
	<tr>
		<td colspan="4">Cancelado:</td>
		<td style="text-align:right;">0.00</td>
	</tr>
	<tr>
		<td colspan="4">Cambio:</td>
		<td style="text-align:right;">0.00</td>
	</tr>
	</tfoot>
	</table>
    <hr/>
    <div>
        Notas: <?php print $order->details; ?>
    </div>
    <hr/>
    <div>
        <?php printf("%s %s", __('Username:', 'mb'), $order->GetUser()->username); ?>
    </div>
</div>
</body>
</html>
