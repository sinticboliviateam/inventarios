<?php
$s_store 	= new SB_MBStore($transfer->from_store);
$t_store 	= new SB_MBStore($transfer->to_store);
$tt 		= new SB_MBTransactionType($t_store->_transfer_tt_id);

?>
<style>
#note *{font-size:11px;font-family: Helvetica,Arial;}
#transfer-items{border:1px solid #000;}
#transfer-items th, #transfer-items td{border:1px solid #000;padding:2px;}
</style>
<div id="note">
	<table style="width:100%;">
	<tr>
		<td style="width:33.33%;">
			<?php if($logo_url): ?>
			<div>
				<img src="<?php print $logo_url; ?>" alt="" style="width:200px;" />
			</div>
			<?php endif; ?>
		</td>
		<td style="width:33.33%;">
			<h1 style="margin:0 0 8px 0;text-align:center;">
				<?php _e('Transfer Input Note', 'mb'); ?><br/>
				<?php printf("%d-%s-%d", $t_store->store_id, $tt->transaction_key, $transfer->to_sequence); ?>
			</h1>
		</td>
		<td style="width:33.33%;">
			<table style="float:right;">
			<tr>
				<td><b><?php _e('Date:'); ?></b></td><td><?php print sb_format_date(time()); ?></td>
			</tr>
			<tr>
				<td><b><?php _e('Time:'); ?></b></td><td><?php print sb_format_time(time()); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<table style="width:100%;">
	<tr>
		<td><b><?php _e('Source Store:', 'mb'); ?></b></td><td><?php print $s_store->store_name; ?></td>
		<td><b></b></td><td></td>
	</tr>
	<tr>
		<td><b><?php _e('Target Store:', 'mb'); ?></b></td><td><?php print $t_store->store_name; ?></td>
		<td><b></b></td><td></td>
	</tr>
	<tr>
		<td><b><?php _e('Date:', 'mb'); ?></b></td><td><?php print sb_format_date($transfer->creation_date); ?></td>
		<td><b><?php _e('Status:', 'mb'); ?></b></td><td></td>
	</tr>
	</table>
	<table id="transfer-items" style="width:100%;border-collapse:collapse;">
	<thead>
	<tr>
		<th><?php _e('Item Num.', 'mb'); ?></th>
		<th><?php _e('Code', 'mb'); ?></th>
		<th><?php _e('Product', 'mb'); ?></th>
		<th><?php _e('Quantity', 'mb'); ?></th>
		<th><?php _e('Price', 'mb'); ?></th>
		<th><?php _e('Total', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($transfer->GetItems() as $item): /*$batch = mb_ceass_obtener_lote($item->product_id, $item->batch);*/ ?>
	<tr>
		<td style="text-align:center;"><?php print $i; ?></td>
		<td><?php print $item->product_code; ?></td>
		<td><?php print $item->product_name; ?></td>
		<td><?php print $item->quantity; ?></td>
		<td><?php print number_format($item->price, 2, '.', ','); ?></td>
		<td><?php print number_format($item->price * $item->quantity, 2, '.', ','); ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table><br/>
	<div>
		<b><?php _e('Observations', 'mb'); ?></b><br/>
		<?php print $transfer->details; ?>
	</div><br/><br/>
	<table style="width:100%;">
	<tr>
		<td style="padding:8px;border:1px solid #000;width:50%;">
			<b><?php _e('I gave compliant', 'mb'); ?></b>
			<br/><br/><br/><br/><br/><br/>
			<div style="text-align:center;">-------------------------------------------------------</div>
		</td>
		<td style="padding:8px;border:1px solid #000;width:50%;">
			<b><?php _e('I received as', 'mb'); ?></b>
			<br/><br/><br/><br/><br/><br/>
			<div style="text-align:center;">-------------------------------------------------------</div>
		</td>
	</tr>
	</table>
</div>