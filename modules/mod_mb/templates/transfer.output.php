<?php
?>
<style>
#note *{font-size:11px;font-family: Helvetica,Arial;}
#transfer-items{border:1px solid #000;}
#transfer-items th, #transfer-items td{border:1px solid #000;padding:2px;}
</style>
<div id="note">
	<table style="width:100%;">
	<tr>
		<td style="width:33.33%;">
			<?php if($logo_url): ?>
			<div>
				<img src="<?php print $logo_url; ?>" alt="" style="width:200px;" />
			</div>
			<?php endif; ?>
		</td>
		<td style="width:33.33%;">
			<h1 style="margin:0 0 8px 0;text-align:center;">
				<?php _e('Transfer Output Note', 'mb'); ?>
			</h1>
		</td>
		<td style="width:33.33%;">
			<table style="float:right;">
			<tr>
				<td><b><?php _e('Date:'); ?></b></td><td><?php print sb_format_date(time()); ?></td>
			</tr>
			<tr>
				<td><b><?php _e('Time:'); ?></b></td><td><?php print sb_format_time(time()); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<table style="width:100%;">
	<tr>
		<td><b><?php _e('Source Store:', 'mb'); ?></b></td><td><?php print $transfer->source_store->store_name; ?></td>
		<td><b><?php _e('Source Warehouse', 'mb'); ?></b></td><td><?php print $transfer->source_warehouse->name; ?></td>
	</tr>
	<tr>
		<td><b><?php _e('Target Store:', 'mb'); ?></b></td><td><?php print $transfer->target_store->store_name; ?></td>
		<td><b><?php _e('Target Warehouse', 'mb'); ?></b></td><td><?php print $transfer->target_warehouse->name; ?></td>
	</tr>
	<tr>
		<td><b><?php _e('Date:', 'mb'); ?></b></td><td><?php print sb_format_date($transfer->creation_date); ?></td>
		<td><b><?php _e('Status:', 'mb'); ?></b></td><td><?php print $transfer->status; ?></td>
	</tr>
	</table>
	<table id="transfer-items" style="width:100%;border-collapse:collapse;">
	<thead>
	<tr>
		<th><?php _e('Item Num.', 'mb'); ?></th>
		<th><?php _e('Code', 'mb'); ?></th>
		<th><?php _e('Product', 'mb'); ?></th>
		<th><?php _e('Quantity', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($transfer->GetItems() as $item): ?>
	<tr>
		<td style="text-align:center;"><?php print $i; ?></td>
		<td><?php print $item->product_code; ?></td>
		<td><?php print $item->product_name; ?></td>
		<td style="text-align:center;"><?php print $item->quantity; ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table><br/>
	<div>
		<b><?php _e('Observations', 'mb'); ?></b><br/>
		<?php print $transfer->details; ?>
	</div><br/><br/>
	<table style="width:100%;">
	<tr>
		<td style="padding:8px;border:1px solid #000;width:50%;">
			<b><?php _e('I gave compliant', 'mb'); ?></b>
			<br/><br/><br/><br/><br/><br/>
			<div style="text-align:center;">-------------------------------------------------------</div>
		</td>
		<td style="padding:8px;border:1px solid #000;width:50%;">
			<b><?php _e('I received as', 'mb'); ?></b>
			<br/><br/><br/><br/><br/><br/>
			<div style="text-align:center;">-------------------------------------------------------</div>
		</td>
	</tr>
	</table>
</div>