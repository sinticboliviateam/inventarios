<?php
?>
<style>
.nota{font-size:11px;font-family:Helvetica,Verdana,Arial;}
.donacion-items{border-collapse: collapse;width:100%;}
.donacion-items td, .donacion-items th{border:1px solid #000;padding:2px;}
.donacion-items th{background:#bcbcbc;}
.totales{text-align:right;}
</style>
<div class="nota">
	<table style="width:100%;">
	<tr>
		<td style="width:33.33%;">
			<?php if( isset($logo_url) && $logo_url ): ?>
			<div style="text-align:center;">
				<img src="<?php print $logo_url; ?>" style="width:220px;" />
			</div>
			<?php endif; ?>
		</td>
		<td style="width:33.33%;text-align:center;font-size:13px;font-weight: bold;">
			<h2 style="margin:0 0 8px 0;padding:0;"><?php print $ajuste->tipo == 'negativo' ? __('Nota Salida de Almacenes') : __('Nota Ingreso a Almacenes'); ?></h2><br/>
			<span style=""><?php printf("%d-%s-%d", $ajuste->store->store_id, $ajuste->transaction->transaction_key, $ajuste->sequence); ?></span>
		</td>
		<td style="width:33.33%;">
			<table style="float:right;">
			<tr>
				<td><b>Fecha:</b></td><td><?php print sb_format_date(time(), 'd/m/Y'); ?></td>
			</tr>
			<tr>
				<td><b>Hora:</b></td><td><?php print sb_format_time(time(), 'H:i:s'); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<table style="width:100%;">
	<tr>
		<td><b><?php _e('Regional'); ?></b></td>
		<td style="width:37%;"><?php print $ajuste->store->store_name; ?></td>
		<td><b><?php _e('Fecha'); ?></b></td>
		<td style="width:37%;"><?php print sb_format_datetime($ajuste->transaction_date); ?></td>
	</tr>
	<tr>
		<td><b><?php _e('Tipo Transaccion'); ?></b></td>
		<td style="width:37%;"><?php print $ajuste->transaction->transaction_name; ?></td>
		<td><b><?php _e('Usuario'); ?></b></td>
		<td style="width:37%;"><?php print $ajuste->user->username; ?></td>
	</tr>
	<tr>
		<td><b><?php _e('Causa del Ajuste'); ?></b></td>
		<td style="width:37%;"><?php print mb_get_cause($ajuste->causa)->nombre; ?></td>
		<td><b></b></td>
		<td style="width:37%;"></td>
	</tr>
	</table>
	<table class="donacion-items">
	<thead>
	<tr>
		<th><?php _e('Nro'); ?></th>
		<th><?php _e('Producto'); ?></th>
		<th><?php _e('Cantidad'); ?></th>
		<th><?php _e('Total'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($ajuste->GetItems() as $item): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $item->product_name; ?></td>
		<td><?php print $item->quantity; ?></td>
		<td class="totales"><?php print number_format($item->total, 2, '.', ','); ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="5" style="text-align:right;"><b><?php _e('Total'); ?></b></td>
		<td class="totales"><b><?php print sb_number_format($ajuste->total); ?></b></td>
	</tr>
	</tfoot>
	</table>
	<p>
		<b>Observaciones:</b><br/>
		<?php print $ajuste->details; ?>
	</p>
	<table style="width:100%;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #000;padding:3px;width:48%;">
			<b>Entregue conforme:</b><br/><br/><br/><br/>
			<div style="text-align:center;">.......................................................</div>
		</td>
		<td style="border:1px solid #000;padding:3px;width:48%;">
			<b>Recibi conforme:</b><br/><br/><br/><br/>
			<div style="text-align:center;">.......................................................</div>
		</td>
	</tr>
	</table>
</div>