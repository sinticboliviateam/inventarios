<?php
?>
<div class="wrap">
	<h2>
		<?php _e('Batches', 'mb'); ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=batch.new'); ?>" class="btn btn-primary pull-right"><?php _e('New', 'mb'); ?></a>
	</h2>
	<form action="" method="get">
		<input type="hidden" name="mod" value="ceass" />
		<input type="hidden" name="view" value="codigos.default" />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<input type="text" name="" value="" placeholder="<?php _e('Buscar'); ?>" />
				</div>
			</div>
		</div>
	</form>
	<table class="table">
	<thead>
	<tr>
		<th><?php _e('No.'); ?></th>
		<th><?php _e('ID'); ?></th>
		<th><?php _e('Name', 'mb'); ?></th>
		<th><?php _e('Action', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($items as $item): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td class="text-center"><?php print $item->id; ?></td>
		<td><?php print $item->name; ?></td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=mb&view=batch.edit&id='.$item->id); ?>" class="btn btn-default">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=mb&task=batch.delete&id='.$item->id); ?>" class="btn btn-default confirm"
				data-message="<?php _e('Are you sure to delete the batch?', 'mb'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table>
</div>