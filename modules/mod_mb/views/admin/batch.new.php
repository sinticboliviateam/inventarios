<?php
?>
<div class="wrap">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h2><?php _e('New Batch', 'mb'); ?></h2>
		</div>
		<div class="col-xs-12 col-md-6">
			<a href="<?php print SB_Route::_('index.php?mod=mb&view=batch.default'); ?>" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
			<a href="javascript:;" onclick="jQuery('#wform').submit();" class="btn btn-success"><?php _e('Save', 'mb'); ?></a>
		</div>
	</div>
	<form id="wform" action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="batch.save" />
		<?php if( isset($b) ): ?>
		<input type="hidden" name="id" value="<?php print $b->id; ?>" />
		<?php endif; ?>
		<div class="form-group">
			<label><?php _e('Name', 'mb'); ?></label>
			<input type="text" name="name" value="<?php print isset($b) ? $b->name : ''; ?>" class="form-control" />
		</div>
	</form>
</div>