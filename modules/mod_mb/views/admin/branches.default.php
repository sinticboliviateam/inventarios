<?php
?>
<div class="wrap">
	<h2>
		<?php print $title; ?>
		<span class="pull-right">
			<a href="javascript:;" id="btn-new-bo" class="btn btn-success"><?php _e('New Branch Office', 'mb'); ?></a>
		</span>
	</h2>
	<table class="table">
	<thead>
	<tr>
		<th>ID</th>
		<th><?php _e('Branch Office', 'mb'); ?></th>
		<th><?php _e('Telephone', 'mb'); ?></th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($branches as $b): ?>
	<tr>
		<td><?php print $b->department_id; ?></td>
		<td><?php print $b->name; ?></td>
		<td><?php print $b->telephone; ?></td>
		<td class="col-action">
			<a href="javascript:;" class="btn btn-default edit-bo" title="<?php _e('Edit', 'mb'); ?>"
				data-id="<?php print $b->department_id; ?>" 
				data-name="<?php print $b->name; ?>" 
				data-address="<?php print $b->address; ?>"
				data-telephone="<?php print $b->telephone; ?>">
				<span class="glyphicon glyphicon-pencil"></span></a>
		</td>
		<td class="col-action">
			<a href="<?php print SB_Route::_('index.php?mod=mb&task=branches.delete&id='.$b->department_id); ?>" class="btn btn-default confirm"
				title="<?php _e('Delete', 'mb'); ?>" data-message="<?php _e('Are you sure to delete the branch office?', 'mb'); ?>">
				<span class="glyphicon glyphicon-trash"></span></a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="new-branch-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	    	<form id="form-branch-data" action="" method="post">
	    		<input type="hidden" name="mod" value="mb" />
	    		<input type="hidden" name="task" value="branches.save" />
	    		<input type="hidden" name="id" value="0" />
		    	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title" id="myModalLabel"><?php _e('Branch Office Data', 'mb')?></h4>
		      	</div>
		      	<div class="modal-body">
		        	<div class="row">
		        		<div class="col-xs-12 col-md-6">
		        			<div class="form-group">
			        			<label><?php _e('Branch Office Name', 'mb'); ?></label>
			        			<input type="text" name="bo_name" value="" class="form-control" />
			        		</div>
		        		</div>
		        		<div class="col-xs-12 col-md-6">
		        			<div class="form-group">
			        			<label><?php _e('Contact Telephone', 'mb'); ?></label>
			        			<input type="text" name="bo_telephone" value="" class="form-control" />
			        		</div>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-xs-12 col-md-8 col-md-offset-2">
		        			<div class="from-group">
		        				<label>DIRECCIÓN TAL Y COMO APARECERÁ EN EL TICKET</label>
		        				<textarea name="bo_address" class="form-control"></textarea>
		        			</div>
		        		</div>
		        	</div> 
		      	</div>
		      	<div class="modal-footerrr text-center">
		        	<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  -->
		        	<button type="submit" class="btn btn-dlg-submit">Save changes</button>
		        	<div></div>
		        	<br/>
		      	</div>
	      	</form>
	    </div>
  	</div>
</div>
<script>
jQuery(function()
{
	jQuery('#btn-new-bo').click(function()
	{
		var form = jQuery('#form-branch-data').get(0);
		form.id.value 			= 0;
		form.bo_name.value		= '';
		form.bo_telephone.value = '';
		form.bo_address.value 	= '';
		jQuery('#new-branch-modal').modal('show');
		return false;
	});
	jQuery('.edit-bo').click(function()
	{
		var form = jQuery('#form-branch-data').get(0);
		form.id.value = this.dataset.id;
		form.bo_name.value = this.dataset.name;
		form.bo_telephone.value = this.dataset.telephone;
		form.bo_address.value = this.dataset.address;
		jQuery('#new-branch-modal').modal('show');
	});
});
</script>