<?php
?>
<div class="wrap">
	<h2 id="page-title">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php print $title; ?></div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="text-right">
					<a href="<?php print $this->Route('index.php?mod=mb&view=brands.new'); ?>" class="btn btn-primary"><?php _e('New', 'mb'); ?></a>
				</div>
			</div>
		</div>
	</h2>
	<?php print $table->Show(); ?>
</div>