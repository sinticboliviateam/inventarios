<?php
?>
<div class="wrap">
	<h2 id="page-title">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php print $title; ?></div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="text-right">
					<a href="<?php print $this->Route('index.php?mod=mb&view=brands.default'); ?>" class="btn btn-success">
						<?php _e('Cancel', 'mb'); ?>
					</a>
					<a href="javascript:;" onclick="jQuery('#form-brand').submit();" class="btn btn-success"><?php _e('Save', 'mb'); ?></a>
				</div>
			</div>
	</h2>
	<form action="" method="post" enctype="multipart/form-data" id="form-brand">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="brands.save" />
		<?php if( isset($brand) ): ?>
		<input type="hidden" name="id" value="<?php print $brand->id; ?>" />
		<?php endif; ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<label><?php _e('Name', 'mb'); ?></label>
						<input type="text" name="name" value="<?php print $this->request->getString('name', isset($brand) ? $brand->name : '') ?>" 
							class="form-control" required />
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<label><?php _e('Image', 'mb'); ?></label>
						<input type="file" name="image" value="" class="form-control" accept="image/*" />
						<?php if( isset($brand) && $brand->image_id): ?>
						<figure>
							<img src="<?php print $brand->image->GetThumbnail('330x330')->GetUrl(); ?>" alt="" class="img-responsive" />
						</figure>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<a href="<?php print $this->Route('index.php?mod=mb&view=brands.default'); ?>" class="btn btn-danger">
						<?php _e('Cancel', 'mb'); ?>
					</a>
					<button type="submit" class="btn btn-success"><?php _e('Save', 'mb'); ?></button>
				</div>
			</div>
		</div>
	</form>
</div>