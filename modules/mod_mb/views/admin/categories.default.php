<?php
$cats = array();
?>
<div class="wrap">
	<h2 id="page-title">
		<?php print $this->__('Categories', 'mb'); ?>
		<a class="btn btn-primary pull-right" href="<?php print $this->Route('index.php?mod=mb&view=categories.new')?>">
			<?php print $this->__('Create new category', 'mb'); ?>
		</a>
	</h2>
	<form id="" class="form-search form-inline" action="" method="get">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="view" value="categories.default" />
		<div class="form-group">
			<input type="text" name="keyword" value="<?php print $this->request->getString('keyword'); ?>" class="form-control" placeholder="<?php print $this->__('Search...', 'mb'); ?>" />
			<select name="store_id" class="form-control">
				<option value=""><?php _e('-- store --', 'mb'); ?></option>
				<?php foreach($stores as $store): ?>
				<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == $storeId ? 'selected' : ''; ?>>
					<?php print $store->store_name; ?>
				</option>
				<?php endforeach; ?>
			</select>
			<select name="search_by" class="form-control">
				<option value="name"><?php print $this->__('Name', 'mb'); ?></option>
				<option value="identifier"><?php print $this->__('Identifier', 'mb'); ?></option>
			</select>
			<button type="submit" class="btn btn-primary"><?php print $this->__('Search', 'mb')?></button>
		</div>
	</form><br/>
	<table class="table">
	<thead>
	<tr>
		<th>#</th>
		<th><?php print $this->__('Name', 'mb'); ?></th>
		<th><?php print $this->__('Description', 'mb'); ?></th>
		<th><?php print $this->__('Store', 'mb'); ?></th>
		<th><?php print $this->__('Actions', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $prev_parent_id = 0; if( count($categories) ): $i = 1; foreach($categories as $cat): if( in_array($cat->category_id, $cats) ) continue;?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $cat->name; ?></td>
		<td><?php print $cat->description; ?></td>
		<td><?php print $cat->store_name; ?></td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=mb&view=categories.edit&id='.$cat->category_id)?>"
				class="btn btn-default btn-xs"
				title="<?php print $this->__('Edit', 'mb'); ?>">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=mb&task=categories.delete&id='.$cat->category_id); ?>"
				class="confirm btn btn-default btn-xs"
				data-message="<?php print $this->__('Are you sure to delete the category?', 'mb'); ?>"
				title="<?php print $this->__('Delete', 'mb'); ?>">
				<i class="glyphicon glyphicon-trash"></i>
			</a>
		</td>
		<?php $cats[] = $cat->category_id; ?>
        <?php foreach($categories as $_cat): $i++; if( $_cat->child_parent != $cat->category_id ) continue; ?>
			<tr>
				<td><?php print $i; ?></td>
				<td><?php printf("&nbsp;&nbsp;&nbsp;- %s", $_cat->child_name); ?></td>
				<td><?php print $_cat->child_description; ?></td>
                <td><?php print $cat->store_name; ?></td>
				<td>
					<a href="<?php print $this->Route('index.php?mod=mb&view=categories.edit&id='.$_cat->child_id)?>"
                        class="btn btn-default btn-xs"
                        title="<?php print $this->__('Edit', 'mb'); ?>">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?php print $this->Route('index.php?mod=mb&task=categories.delete&id='.$_cat->child_id); ?>"
                        class="confirm btn btn-default btn-xs"
                        data-message="<?php print $this->__('Are you sure to delete the category?', 'mb'); ?>"
                        title="<?php print $this->__('Delete', 'mb'); ?>">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
				</td>
			</tr>
		<?php endforeach; ?>
	</tr>
		
	<?php $i++; endforeach; else: ?>
	<tr><td colspan="4"><?php print $this->__('There are no categories yet.')?></td></tr>
	<?php endif; ?>
	</tbody>
	</table>
</div>
