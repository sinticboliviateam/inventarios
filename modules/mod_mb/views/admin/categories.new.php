<?php
?>
<div class="wrap">
	<h1 id="page-title"><?php print $page_title; ?></h1>
		<form action="" method="post" enctype="multipart/form-data">
			<input type="hidden" name="task" value="categories.save" />
			<input type="hidden" name="mod" value="mb" />
			<?php if( isset($the_cat) ): ?>
			<input type="hidden" name="cat_id" value="<?php print $the_cat->category_id; ?>" />
			<?php endif; ?>
			<div class="form-group">
				<label><?php print $this->__('Name', 'mb')?></label>
				<input type="text" name="category_name" value="<?php print isset($the_cat) ? $the_cat->name : ''; ?>" class="form-control" />
			</div>
			<div class="form-group">
				<label><?php print $this->__('Description', 'mb')?></label></span>
				<textarea name="description" class="form-control"><?php print isset($the_cat) ? $the_cat->description : ''; ?></textarea>
			</div>
			<div class="form-group">
				<label><?php _e('Image', 'mb'); ?></label>
				<input type="file" name="image" class="form-control" />
				<?php if( isset($the_cat) && $img = $the_cat->GetImage() ): ?>
				<img src="<?php print $the_cat->GetThumbnail()->GetUrl(); ?>" alt="" />
				<?php endif; ?>
			</div>
			<div class="form-group">
				<label><?php print $this->__('Store'); ?></label>
				<select id="store_id" name="store_id" class="form-control">
					<option value="-1"><?php print $this->__('-- store --'); ?></option>
					<?php foreach($stores as $store): ?>
					<option value="<?php print $store->store_id; ?>" <?php print (isset($the_cat) && $the_cat->store_id == $store->store_id) ? 'selected' : ''; ?>>
						<?php print $store->store_name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				<label><?php print $this->__('Parent Category', 'mb')?></label></span>
				<span class="span7">
					<select id="parent_id" name="parent_id" class="form-control">
						<option value="-1">-- <?php print $this->__('category', 'mb');?> --</option>
						<?php foreach($categories as $i => $cat): ?>
						<option value="<?php print $cat->category_id; ?>" 
								<?php print (isset($the_cat) && $the_cat->parent == $cat->category_id) ? 'selected' : ''?>>
							<?php print $cat->name; ?>
						</option>
						<?php if( $cat->childs ): foreach($cat->childs as $c): ?>
						<option value="<?php print $c->category_id; ?>" 
								<?php print (isset($the_cat) && $the_cat->parent == $c->category_id) ? 'selected' : ''?>>- <?php print $c->name; ?></option>
						<?php endforeach; endif; ?>
						<?php endforeach; ?>
					</select>
				</span>
			</div>
			<div class="form-actions">
				<a class="btn btn-danger" href="<?php print $this->Route('index.php?mod=mb&view=categories.default')?>">
					<?php _e('Cancel', 'mb'); ?>
				</a>
				<button type="submit" class="btn btn-success"><?php print $this->__('Save', 'mb')?></button>
			</div>
		</form>
	<script>
	jQuery(function()
	{
		jQuery('#store_id').change(function()
		{
			
			if( this.value <= 0 )
				return false;
			jQuery.get('index.php?mod=mb&task=categories.get_store_categories&store_id='+this.value, function(res)
			{
				//console.log(res);
				jQuery('#parent_id').html(res.ops);
			});
		});
	});
	</script>
</div>
