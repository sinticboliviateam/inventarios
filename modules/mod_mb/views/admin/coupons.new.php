<?php
?>
<div class="wrap">
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="coupons.save" />
		<?php if( isset($coupon) ): ?>
		<input type="hidden" name="id" value="<?php print $coupon->coupon_id; ?>" />
		<?php endif; ?>
		<h2>
			<?php print $title; ?>
			<button type="submit" class="btn btn-success pull-right"><?php _e('Add Coupon', 'mb'); ?></button>
		</h2>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><?php _e('Coupon Code', 'mb'); ?></label>
					<input type="text" name="code" value="<?php print isset($coupon) ? $coupon->code : ''; ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Discount', 'mb'); ?></label>
					<input type="text" name="discount" value="<?php print isset($coupon) ? $coupon->discount : ''; ?>" class="form-control" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="datetime-control">
					<div class="form-group">
						<div><label><?php _e('Start Date', 'mb'); ?></label></div>
						<div class="date">
							<input type="text" name="start_date" value="<?php print isset($coupon) ? $coupon->start_date : ''; ?>" class="form-control datepicker" />
						</div>
						<div class="time form-group">
							<input type="number" min="01" max="23" name="start_date_hour" value="<?php print isset($coupon) ? $coupon->start_date : ''; ?>" class="form-control" />
						</div>
						<div class="time-dots">:</div>
						<div class="time form-group">
							<input type="number" min="00" max="59" name="start_date_min" value="<?php print isset($coupon) ? $coupon->start_date : ''; ?>" class="form-control" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?php _e('Usage Limit', 'mb'); ?></label>
					<input type="number" name="usage_limit" value="<?php print isset($coupon) ? $coupon->usage_limit : ''; ?>" class="form-control" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-groups datetime-control">
					<label><?php _e('End Date', 'mb'); ?></label>
					<div class="date">
						<input type="text" name="end_date" value="<?php print isset($coupon) ? $coupon->end_date : ''; ?>" class="form-control datepicker" />
					</div>
					<div class="time form-group">
						<input type="number" min="01" max="23" name="end_date_hour" value="<?php print isset($coupon) ? $coupon->start_date : ''; ?>" class="form-control" />
					</div>
					<div class="time-dots">:</div>
					<div class="time form-group">
						<input type="number" min="00" max="59" name="end_date_min" value="<?php print isset($coupon) ? $coupon->start_date : ''; ?>" class="form-control" />
					</div>
				</div>
				<div class="form-groups">
					<label><?php _e('Minimal Amount Required', 'mb'); ?></label>
					<input type="text" name="min_amount_required" value="<?php print isset($coupon) ? $coupon->min_amount_required : ''; ?>" class="form-control" />	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label><?php _e('Coupon Description', 'mb'); ?></label>
					<textarea rows="" cols="" name="description" class="form-control"><?php print isset($coupon) ? $coupon->description : ''; ?></textarea>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><?php _e('Coupon Status', 'mb'); ?></label>
					<select name="status" class="form-control">
						<option value="active" <?php print isset($coupon) && $coupon->status == 'active' ? 'selected' : ''; ?>><?php _e('Active', 'mb'); ?></option>
						<option value="inactive" <?php print isset($coupon) && $coupon->status == 'inactive' ? 'selected' : ''; ?>><?php _e('Inactive', 'mb'); ?></option>
					</select>
				</div>
			</div>
		</div>
		
	</form>
</div>