<?php
//print_r(sb_get_current_user());
?>
<div class="wrap">
	<h2 id="page-title"><?php print $title ?></h2>
	<div class="row">
		<div class="col-md-12">
			<?php if( $user->can('mb_create_product') ): ?>
			<a href="<?php print b_route('index.php?mod=mb&view=new_product'); ?>" class="btn btn-primary"><?php _e('New product', 'mb'); ?></a>
			<a href="<?php print b_route('index.php?mod=mb&view=new_product&btype=asm'); ?>" class="btn btn-primary"><?php _e('New Assembly', 'mb'); ?></a>
			<?php endif; ?>
			
			<a href="javascript:;" id="btn-print-catalog" class="btn btn-warning">
				<span class="glyphicon glyphicon-print"></span> <?php _e('Print Catalog', 'mb'); ?>
			</a>
			<a href="javascript:;" id="btn-print-labels" class="btn btn-warning">
				<span class="glyphicon glyphicon-tag"></span> <?php _e('Print Labels', 'mb'); ?>
			</a>
            <a href="javascript:;" id="btn-build-barcodes" class="btn btn-info">
				<span class="glyphicon glyphicon-barcode"></span> <?php _e('Build barcodes', 'mb'); ?>
			</a>
			<a href="javascript:;" onclick="sb_start_camera();" class="btn btn-info" data-toggle="modal" data-target="#camera-dialog">
				<span class="glyphicon glyphicon-barcode"></span> <?php _e('Scan barcode', 'mb'); ?>
			</a>
			<?php b_do_action('mb_products_buttons'); ?>
		</div>
	</div>
	<br/>
	<form id="form-search" class="form-search form-group-sm" action="" method="get">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" id="search_by" name="search_by" value="<?php print isset($search_by) && !empty($search_by) ? $search_by : 'product_code'; ?>" />
		<div class="row">
			<div class="col-md-2 col-xs-12">
				<div class="form-group">
					<select id="filter-store-id" name="store_id" class="form-control input-sm">
						<option value="-1">-- <?php print __('Store', 'mb'); ?> --</option>
						<?php if( isset($stores) && is_array($stores) ): foreach($stores as $s): ?>
						<option value="<?php print $s->store_id; ?>" <?php print $store_id == $s->store_id ? 'selected' : ''; ?>>
							<?php print $s->store_name; ?>
						</option>
						<?php endforeach; endif; ?>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-md-2">
				<div class="form-group">
					<?php
					print mb_dropdown_categories(array('type' => 'select', 
														'name' => 'category_id', 
														'selected' => $this->request->getInt('category_id'),
														'store_id' => $store_id));
					?>
				</div>
			</div>
			<div class="col-xs-12 col-md-2">
				<div class="form-group">
					<select id="types" name="type_id" class="form-control input-sm">
						<option value="-1"><?php _e('-- type --', 'mb'); ?></option>
						<?php if(isset($types)): foreach($types as $t): ?>
						<option value="<?php print $t->type_id; ?>" <?php print $this->request->getInt('type_id') == $t->type_id ? 'selected' : ''; ?>>
							<?php print $t->type; ?>
						</option>
						<?php endforeach; endif; ?>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="input-group">
					<input type="text" name="keyword" value="<?php print $keyword; ?>" class="form-control input-sm" placeholder="<?php _e('Search...', 'mb'); ?>" />
					<div class="input-group-btn">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" >
						    	<span id="search-by-text"><?php print $def_search_text; ?></span> <span class="caret"></span>
						  	</button>
						  	<ul class="dropdown-menu">
							    <li>
							    	<a href="javascript:;" 
										onclick="jQuery('#search_by').val('product_name');jQuery('#search-by-text').html(this.innerHTML);">
							    		<?php _e('Name', 'mb'); ?>
							    	</a>
							    </li>
							    <li>
							    	<a href="javascript:;" 
										onclick="jQuery('#search_by').val('product_code');jQuery('#search-by-text').html(this.innerHTML);">
							    		<?php _e('Code', 'mb'); ?>
							    	</a>
							    </li>
							    <li role="separator" class="divider"></li>
						  	</ul>
						</div>
						<button type="submit" class="btn btn-default btn-sm"><?php _e('Search', 'mb')?></button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="clearfix">&nbsp;</div>
	<form id="bulk_action_form" action="" method="get">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="batch" />
		<div class="row">
			<div class="col-md-2">
				<select id="bulk_action" name="action" class="form-control input-sm">
					<option value="-1"><?php _e('-- batch action --', 'mb'); ?></option>
					<option value="delete"><?php _e('Delete', 'mb'); ?></option>
					<?php if( isset($categories) ): ?>
					<option value="set_categories"><?php _e('Set categories', 'mb'); ?></option>
					<?php endif; ?>
					<option value="save_selection"><?php _e('Save Selection', 'mb'); ?></option>
					<option value="clear_selection"><?php _e('Delete Selection', 'mb'); ?></option>
				</select>
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-primary btn-sm"><?php _e('Execute', 'mb'); ?></button>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-condensed">
			<thead>
			<tr>
				<th><input type="checkbox" name="selector" value="" class="tcb-select-all" /></th>
				<th><?php print 'ID'; ?></th>
				<th><?php _e('Code', 'mb'); ?></th>
				<th><?php _e('Image', 'mb'); ?></th>
				<th><?php _e('Product', 'mb'); ?></th>
                <th><?php _e('Barcode', 'mb'); ?></th>
				<th><?php _e('Store', 'mb'); ?></th>
				<th><?php _e('Category', 'mb'); ?></th>
				<th><?php _e('Stock', 'mb'); ?></th>
                <th><?php _e('Price', 'mb'); ?></th>
				<th><?php _e('Actions', 'mb'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php if( count($products) ): $i = 1;foreach($products as $p): ?>
			<?php
			$qty_title = __('Stock good', 'mb');
			$qty_class = 'label label-success';
			if($p->product_quantity > 0 && $p->product_quantity <= $p->min_stock  )
			{
				$qty_class = 'label label-warning';
				$qty_title = __('Minimal stock warning', 'mb');
			}
			if($p->product_quantity <= 0  )
			{
				$qty_class = 'label label-danger';
				$qty_title = __('Product stock critical danger', 'mb');
			}
				
			?>
			<tr>
				<td><input type="checkbox" name="ids[]" value="<?php print $p->product_id; ?>" class="tcb-select" /></td>
				<td class="col-id"><?php print $p->product_id; ?></td>
				<td class="col-code"><?php print $p->product_code; ?></td>
				<td class="col-image text-center">
					<?php printf("<a href=\"javascript:;\" class=\"thumb-container\"><img src=\"%s\" alt=\"\" /></a>", 
									$p->getFeaturedImage('55x55')->GetUrl()); ?>
				</td>
				<td class="col-name"><a href="javascript:;" class="product-name"><?php print $p->product_name; ?></a></td>
                <td class="col-barcode"><?php print $p->product_barcode; ?></td>
				<td class="col-store"><?php print $p->store_id ? $p->getStore()->store_name : __('No assigned', 'mb'); ?></td>
				<td class="col-category">
					<?php print $p->GetCategoriesName(); ?>
				</td>
				<td class="col-qty text-center">
					<h3 style="margin:0;cursor:pointer;" title="<?php print $qty_title; ?>">
						<span class="<?php print $qty_class; ?>"><?php print $p->product_quantity; ?></span>
					</h3>
				</td>
                <td class="text-right">
                    <h3 style="margin:0;cursor:pointer;">
                        <span class="label label-info"><?php print sb_number_format($p->product_price) ?></span>
                    </h3>
                </td>
				<td class="col-actions">
					<a href="<?php print b_route('index.php?mod=mb&view=edit&id='.$p->product_id); ?>" 
						title="<?php _e('Edit', 'mb')?>" class="btn btn-default btn-xs">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="javascript:;" class="btn-quick-view btn btn-default btn-xs" title="<?php _e('Quick view', 'mb'); ?>"
						data-code="<?php print $p->product_code; ?>"
						data-name="<?php print $p->product_name; ?>"
						data-qty="<?php print $p->product_quantity; ?>"
						data-cost="<?php print sb_get_current_user()->can('mb_can_see_cost') ? $p->product_cost : '0.00'; ?>"
						data-price1="<?php print number_format($p->product_price, 2, '.', ','); ?>"
						data-price2="<?php print number_format($p->product_price_2, 2, '.', ','); ?>"
						data-price3="<?php print number_format($p->product_price_3, 2, '.', ','); ?>"
						data-price4="<?php print number_format($p->product_price_4, 2, '.', ','); ?>">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					<?php if( $user->can('mb_delete_product') ): ?>
					<a class="confirm btn btn-default btn-xs" href="<?php print b_route('index.php?mod=mb&task=delete&id='.$p->product_id); ?>" title="<?php _e('Delete', 'mb')?>"
						data-message="<?php _e('Are you sure to delete the product?', 'mb'); ?>">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
					<?php endif; ?>
				</td>
			</tr>
			<?php $i++; endforeach; else: ?>
			<tr><td colspan="4"><?php _e('There are no products yet into database.'); ?></td></tr>
			<?php endif; ?>
			</tbody>
			</table>
		</div>
	</form>
	<?php lt_pagination(b_route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
</div>
<div id="quick-view-dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php _e('Product Details', 'mb'); ?></div>
			<div class="modal-body">
				<table>
				<tr>
					<td><b><?php _e('Code:', 'mb'); ?></b></td>
					<td><span id="_code">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Product Name:', 'mb'); ?></b></td>
					<td><span id="_name">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Quantity:', 'mb'); ?></b></td>
					<td><span id="_qty">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Costo:', 'mb'); ?></b></td>
					<td><span id="_cost">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 1:', 'mb'); ?></b></td>
					<td><span id="_price1">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 2:', 'mb'); ?></b></td>
					<td><span id="_price2">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 3:', 'mb'); ?></b></td>
					<td><span id="_price3">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 4:', 'mb'); ?></b></td>
					<td><span id="_price4">0.00</span></td>
				</tr>
				</table>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
			</div>
		</div>
	</div>
</div>
<div id="modal-print-catalog" class="modal fade" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php _e('Print Product Catalog', 'mb'); ?></div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php _e('Select catalog type', 'mb'); ?></label>
					<select id="catalog_type" name="catalog_type" class="form-control">
						<option value="pdf"><?php _e('PDF', 'mb'); ?></option>
						<option value="excel"><?php _e('Excel', 'mb'); ?></option>
					</select>
				</div>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
		        <a href="javascript:;" class="btn btn-success" id="btn-build-catalog">
					<?php _e('Build', 'mb'); ?>
				</a>
			</div>
		</div>
	</div>
</div>
<div id="camera-dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php _e('Capture Product Barcode', 'mb'); ?></div>
			<div class="modal-body">
				<video id="camera-video" width="250" height="250" autoplay></video>
				<canvas id="camera-canvas"></canvas>
				<a href="javascript:;" onclick="__capture();" class="btn btn-primary" style="width:100%;"><?php _e('Capture', 'mb'); ?></a>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
			</div>
		</div>
	</div>
</div>
<?php if( isset($categories) ): ?>
<div id="categories-dialog" class="modal fade">
	<form id="form-bulk-categories" action="" method="post" class="modal-dialog">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="set_bulk_categories" />
		<input type="hidden" name="ajax" value="1" />
		<div class="modal-content">
			<div class="modal-header"><?php _e('Set bulk categories', 'mb'); ?></div>
			<div class="modal-body">
				<div class="form-control" style="width:100%;height:250px;overflow:auto;">
					<?php print $categories; /*foreach($categories as $c): ?>
					<div>
						<label>
							<input type="checkbox" name="categories[]" value="<?php print $c->category_id; ?>" />
							<?php print $c->name; ?>
						</label>
					</div>
					<?php endforeach;*/ ?>
				</div>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
				<button type="submit" class="btn btn-primary"><?php _e('Save', 'mb'); ?></button>
			</div>
		</div>
	</form>
</div>
<?php endif; ?>
<style>
@media (max-width:768px)
{
	table, thead, tbody, th, td, tr { 
		display: block; 
		position:relative;
	}
	table thead{display:none;}
	table tbody{margin:0;padding:0;position:relative;width:100%;}
	table tbody tr{border:0;overflow:hidden;border-bottom:1px solid #ddd;margin:0 0 5px 0;}
	table tbody tr td{position:relative;border:0;display:block;border:0 !important;}
	/*.col-count,.col-image,.col-name{float:left;}*/
	.col-id{display:none !important;float:left !important;}
	.col-code,.col-image, .col-store,.col-qty{width:25%;float:left !important;}
	.col-name{width:50%;height:85px;float:left !important;overflow:hidden;}
	.col-store{}
}
</style>
<?php lt_include_partial('mb', 'modal-label.php'); ?>
<div id="modal-processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Processing, please wait...'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>

