<?php

?>
<div class="wrap">
	<h2><?php _e('Export Products', 'mb'); ?></h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="do_export" />
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="form-group">
					<label><?php _e('Store', 'mb'); ?></label>
					<select id="store_id" name="store_id" class="form-control" required>
						<option value=""><?php _e('-- store --', 'mb'); ?></option>
						<?php foreach(SB_Warehouse::GetUserStores($user) as $store): ?>
						<option value="<?php print $store->store_id; ?>"><?php print $store->store_name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="form-group">
					<label><?php _e('Category', 'mb'); ?></label>
					<?php
					print mb_dropdown_categories(array('type' => 'select', 
														'name' => 'category_id', 
														'selected' => -1,
														//'store_id' => $store_id
												));
					?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="form-group">
					<label><?php _e('Fields', 'mb'); ?></label>
					<div class="form-control" style="height:200px;overflow:auto;">
						<?php foreach($fields as $f => $fn): ?>
						<div>
							<label>
								<input type="checkbox" name="fields[]" value="<?php print $f; ?>" checked />
								<?php print $fn; ?>
							</label>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"></div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary"><?php _e('Export Now', 'mb'); ?></button>
		</div>
	</form>
</div>
<script>
function FillCategories(items, space)
{
	var dropdown = jQuery('#category_id');
	jQuery.each(items, function(i, cat)
	{
		dropdown.append('<option value="'+cat.category_id+'">' + ( space ? (space + ' ') : '') + cat.name+'</option>');
		
		if( cat.childs && cat.childs.length > 0 )
		{
			FillCategories(cat.childs, space + '-');
		}
	});
}
jQuery(function()
{
	console.log('__INIT__');
	//##on store changed
	jQuery('#store_id').change(function()
	{
		jQuery('#category_id').html('<option value="-1">-- categoria --</option>');
		if( this.value <= 0 )
			return true;
		var params = 'mod=mb&task=ajax&action=get_store_cats&store_id=' + this.value + '&time=' + (new Date().getTime());
		jQuery.get('index.php?' + params, function(res)
		{
			if( res.status == 'ok' )
			{
				FillCategories(res.categories, '');
			}
		});
		return false;
	});
});
</script>
