<?php
?>
<br/>
<fieldset>
	<legend><?php _e('Assembly Products', 'mb'); ?></legend>
	<div class="form-group">
		<input type="text" id="asm_search_product" name="asm_search_product" value="" placeholder="<?php _e('Search product...', 'mb'); ?>" class="form-control" />
	</div>
	<table id="assembly-items" class="table">
	<thead>
	<tr>
		<th><?php _e('No.', 'mb'); ?></th>
		<th><?php _e('Code', 'mb'); ?></th>
		<th><?php _e('Product', 'mb'); ?></th>
		<th><?php _e('Qty', 'mb'); ?></th>
		<th><?php _e('Price', 'mb'); ?></th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<?php if($product): $i = 1; ?>
	<?php foreach($product->GetAsmComponents() as $com): ?>
	<tr data-id="<?php print $com->product_id; ?>" data-qty="<?php print $com->qty_required; ?>">
		<td><?php print $i; ?></td>
		<td><?php print $com->product_code; ?></td>
		<td><?php print $com->product_name; ?></td>
		<td>
			<input type="number" name="asm_item_<?php print $com->product_id; ?>_qty" value="<?php print $com->qty_required; ?>" class="asm-item-qty form-control" />
		</td>
		<td><div class="text-right"><?php print number_format($com->product->product_price, 2, '.', ',') ?></div></td>
		<td>
			<a href="javascript:;" class="btn btn-default btn-xs remove-asm-item" title="<?php _e('Delete', 'mb'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
			<a href="<?php print b_route('index.php?mod=mb&view=product&id=' . $com->product_id); ?>" class="btn btn-default btn-xs" target="_blank" 
				title="<?php _e('View product', 'mb'); ?>">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	<?php endif;?>
	</tbody>
	</table>
</fieldset>
<script src="<?php print BASEURL; ?>/js/sb-completion.js"></script>
<script>
jQuery(function()
{
	window.asm_completion = new SBCompletion({
		input: document.getElementById('asm_search_product'),
		url: '<?php print b_route('index.php?mod=mb&task=ajax&action=search_product'); ?>',
		callback: function(item)
		{
			//console.log(item);
			window.mb_asm_product = item.dataset;
			console.log(window.mb_asm_product);
		}
	});
	jQuery('#asm_search_product').keyup(function(e)
	{
		if( e.keyCode != 13)
			return false;
		//##append the selected product to 
		var row_tpl = '<tr data-id="{id}" data-qty="{qty}">'+
						'<td>{num}</td>'+
						'<td>{code}</td>'+
						'<td>{product}</td>'+
						'<td><input type="number" name="asm_item_{id}_qty" value="{qty}" class="asm-item-qty form-control" /></td>'+
						'<td><div class="text-right">{price}</div></td>' +
						'<td><a href="javascript:;" class="btn btn-default btn-xs remove-asm-item" title="<?php _e('Delete', 'mb'); ?>"><span class="glyphicon glyphicon-trash"></span></a></td>'+
					'</tr>';
		var items = jQuery('#assembly-items tbody tr').length;
		var row = row_tpl.replace(/{num}/g, items + 1)
						.replace(/{id}/g, mb_asm_product.id)
						.replace(/{code}/g, mb_asm_product.product_code)
						.replace(/{product}/g, mb_asm_product.name)
						.replace(/{qty}/g, 1)
						.replace(/{price}/g, parseFloat(mb_asm_product.product_price).toFixed(2));
		jQuery('#assembly-items tbody').append(row);
		this.value = '';
	});
	jQuery(document).on('change keyup keydown', '.asm-item-qty', function(e)
	{
		if( e.type == 'change' || e.type == 'keyup' || e.type == 'keydown' )
		{
			jQuery(this).parents('tr:first').get(0).dataset.qty = isNaN(this.value) ? 0 : this.value;
			if( e.type == 'keydown' && e.keyCode == 13 )
			{
				return false;
			}
			
		}
	});
	//##add remove asm item event
	jQuery(document).on('click', '.remove-asm-item', function(e)
	{
		jQuery(this).parents('tr:first').remove();
		return false;
	});
	jQuery('#product-form').submit(function()
	{
		var form = jQuery(this);
		jQuery('#assembly-items tbody tr').each(function(i, tr)
		{
			form.append('<input type="hidden" name="asm['+i+'][id]" value="'+tr.dataset.id+'" />');
			form.append('<input type="hidden" name="asm['+i+'][qty]" value="'+tr.dataset.qty+'" />');
		})
	});
});
</script>