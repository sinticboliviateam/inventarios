<?php
?>
<a href="javascript:;" data-product_id="<?php print $p->product_id?>" class="product btn btn-default btn-add-product"
	title="<?php print $p->product_name; ?>">
	<span class="product-image">
		<img src="<?php print $p->getFeaturedImage(); ?>" alt="" width="90" />
	</span>
	<span class="product-name" style="display:block;width:100%;"><?php print $p->product_name; ?></span>
	<span class="product-price" style="display:block;width:100%;">$ <?php print $p->product_price; ?></span>
</a>