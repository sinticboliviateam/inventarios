<?php
?>
<div class="wrap">
	<h2><?php _e('Import Products', 'mb'); ?></h2>
	<form id="form-import" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="do_import" />
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label><?php _e('Store', 'mb')?></label>
					<select id="store_id" name="store_id" class="form-control">
						<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
						<?php foreach($stores as $s): ?>
						<option value="<?php print $s->store_id; ?>">
                            <?php print $s->store_name; ?>
                        </option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><?php _e('Category', 'mb'); ?></label>
					<select id="category_id" name="category_id" class="form-control">
						<option value="-1"><?php _e('-- category --', 'mb'); ?></option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label><?php _e('File:', 'mb');?></label>
					<input type="checkbox" id="from_path" name="from_path" value="1" /><?php _e('From manual path', 'mb'); ?><br/>
					<input type="file" id="the_file" name="the_file" value="" class="form-control" />
				</div>
				
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label><?php _e('Separator:', 'mb');?></label>
					<input type="text" name="separator" value=","  class="form-control" />
				</div>	
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="form-group">
					<label><?php _e('File Type', 'mb'); ?></label>
					<select name="file_type" class="form-control">
						<option value="-1" ><?php _e('-- file type --', 'mb'); ?></option>
						<option value="xls_file" <?php print $this->request->getString('file_type') == 'xls_file' ? 'selected' : ''; ?>><?php _e('Excel File', 'mb'); ?></option>
						<option value="csv_file" <?php print $this->request->getString('file_type') == 'csv_file' ? 'selected' : ''; ?>><?php _e('CSV File', 'mb'); ?></option>
						
						<option value="monobusiness_file" <?php print $this->request->getString('file_type') == 'monobusiness_file' ? 'selected' : ''; ?>>
                            <?php _e('Mono Business (SQLite)', 'mb'); ?></option>
						<option value="monobusiness_csv"
                            <?php print $this->request->getString('file_type') == 'monobusiness_csv' ? 'selected' : ''; ?>>
                            <?php _e('Mono Business (CSV)', 'mb'); ?></option>
						<option value="epos_file_sqlite" <?php print $this->request->getString('file_type') == 'epos_file_sqlite' ? 'selected' : ''; ?>><?php _e('EPoint of Sale (SQLite)', 'mb'); ?></option>
						<option value="epos_file_csv" 
                            <?php print $this->request->getString('file_type') == 'epos_file_csv' ? 'selected' : ''; ?>>
                            <?php _e('EPoint of Sale (CSV)', 'mb'); ?></option>
						
					</select>
				</div>
			</div>
		</div>
		<fieldset>
			<fieldset><a data-toggle="collapse" href="#adv-options" class="btn btn-default"><?php _e('Advanced Options', 'mb'); ?> <span class="caret"></span></a></fieldset>
			<div id="adv-options" class="collapse">
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Excel Sheet', 'mb'); ?></label>
							<input type="number" name="sheet_num" value="<?php print $this->request->getInt('sheet_num'); ?>" class="form-control" />
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Row Start:', 'mb'); ?></label>
							<input type="number" name="row_start" min="1" value="<?php print $this->request->getInt('row_start', 1); ?>" class="form-control" />
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Code Column:', 'mb'); ?></label>
							<select id="" name="code_column" class="form-control">
								<?php foreach(array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>" 
                                    <?php print $this->request->getInt('code_column', -1) == $i ? 'selected' : ''; ?>>
                                    <?php print $l; ?>
                                </option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
                    <div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Barcode:', 'mb'); ?></label>
							<select id="" name="barcode_column" class="form-control">
                                <option value="-1"><?php print $this->__('-- barcode --'); ?></option>
								<?php foreach(array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>" 
                                    <?php print $this->request->getInt('barcode_column', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Name Column:', 'mb'); ?></label>
							<select id="" name="name_column" class="form-control">
                                <option value="-1"><?php print $this->__('-- name --'); ?></option>
								<?php foreach(array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('name_column', -1) == $i ? 'selected' : ''; ?>>
                                    <?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Description Column:', 'mb'); ?></label>
							<select id="" name="desc_column" class="form-control">
								<?php foreach(array('-1' => __('-- description --', 'mb'), 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('desc_column', -1) == (int)$i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Qty Column:', 'mb'); ?></label>
							<select id="" name="qty_column" class="form-control">
								<?php foreach(array('-1' => __('-- quantity --', 'mb'), 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('qty_column', -1) == (int)$i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Cost Column:', 'mb'); ?></label>
							<select id="" name="cost_column" class="form-control">
								<?php foreach(array('-1' => '-- cost --', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('cost_column', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Price Column:', 'mb'); ?></label>
							<select id="" name="price_column" class="form-control">
								<?php foreach(array('-1' => '-- price --', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('price_column', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Price 2 Column:', 'mb'); ?></label>
							<select id="" name="price_column_2" class="form-control">
								<?php foreach(array('-1' => '-- price --', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('price_column_2', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Price 3 Column:', 'mb'); ?></label>
							<select id="" name="price_column_3" class="form-control">
								<?php foreach(array('-1' => '-- price --', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('price_column_3', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Price 4 Column:', 'mb'); ?></label>
							<select id="" name="price_column_4" class="form-control">
								<?php foreach(array('-1' => '-- price --', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('price_column_4', -1) == $i ? 'selected' : ''; ?>><?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label><?php _e('Image Column:', 'mb'); ?></label>
							<select id="" name="image_column" class="form-control">
								<?php foreach(array('-1' => __('-- image --', 'mb'), 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z') as $i => $l): ?>
								<option value="<?php print $i; ?>"
                                    <?php print $this->request->getInt('image_column', -1) == $i ? 'selected' : ''; ?>>
                                    <?php print $l; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label class="">
								<?php _e('Update just prices', 'mb'); ?>
								<input type="checkbox" name="update_prices_only" value="1" />
							</label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label class="">
								<?php _e('Mark is initial stock', 'mb'); ?>
								<input type="checkbox" name="initial_stock" value="1" />
							</label>
						</div>
					</div>
				</div>
			</div>
		</fieldset><br/>
		<div class="form-group">
			<a href="<?php print $this->Route('index.php?mod=mb'); ?>" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
			<button id="btn-import" type="submit" class="btn btn-success"><?php _e('Import Now', 'mb'); ?></button>
		</div>
	</form>
</div>
<div id="modal-processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Importing products, please wait...', 'mb'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>
<script>
jQuery(function()
{
	jQuery('#store_id').change(function()
	{
		if( this.value <= 0 )
			return false;
		jQuery('#category_id').html('<option value="-1"><?php _e('-- category --', 'mb'); ?></option>');
		jQuery.get('index.php?mod=mb&task=ajax&action=get_store_cats&store_id='+this.value, function(res)
		{
			if(res.status == 'ok')
			{
				jQuery.each(res.categories, function(i, cat)
				{
					var op = jQuery('<option value="'+cat.category_id+'">'+cat.name+'</option>');
					jQuery('#category_id').append(op);
					if( cat.childs && cat.childs.length > 0 )
					{
						jQuery.each(cat.childs, function(ii, subcat)
						{
							var sop = jQuery('<option value="'+subcat.category_id+'">- '+subcat.name+'</option>');
							jQuery('#category_id').append(sop);
						});
						
					}
				});
			}
			else
			{
				alert(res.error);
			}
		});
	});
	jQuery('#from_path').click(function()
	{
		if( jQuery(this).is(':checked') )
		{
			jQuery('#the_file').prop('type', 'text');
		}
		else
		{
			jQuery('#the_file').prop('type', 'file');
		}
	});
    jQuery('#form-import').submit(function()
    {
        //this.setAttribute('disabled', true);
        jQuery('#modal-processing').modal('show');
        return true;
    });
});
</script>
