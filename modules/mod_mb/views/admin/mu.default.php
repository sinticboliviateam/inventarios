<?php 
?>
<div class="wrap">
	<h2 class="col-xs-6 col-md-6"><?php _e('Measurent Units', 'mb') ?></h2>
	<div class="col-xs-6 col-md-6 text-right">
		<a href="javascript:;" id="btn-new-mu" class="btn btn-primary"><?php _e('Add New', 'mb'); ?></a>
	</div>
	<table class="table table-condensed">
	<thead>
	<tr>
		<th><?php _e('No.', 'mb'); ?></th>
		<th><?php _e('Code', 'mb'); ?></th>
		<th><?php _e('Unit', 'mb'); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($units as $u): ?>
	<tr>
		<td class="text-center"><?php print $i; ?></td>
		<td class="text-center"><?php print $u->code; ?></td>
		<td><?php print $u->name; ?></td>
		<td class="text-center">
			<a href="<?php print $this->Route('index.php?mod=mb&view=mu.edit&id='.$u->measure_id); ?>" class="edit-mu btn btn-default btn-sm"
				data-id="<?php print $u->measure_id; ?>" data-code="<?php print $u->code; ?>" 
				data-name="<?php print $u->name; ?>" title="<?php _e('Edit', 'mb'); ?>">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=mb&task=mu.delete&id='.$u->measure_id); ?>" title="<?php _e('Delete', 'mb'); ?>"
				class="btn btn-default btn-sm confirm" data-message="<?php _e('Are you sure to delete the measurement unit?', 'mb'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table>
</div>
<div id="new-mu-modal" class="modal fade">
	<div class="modal-dialog">
		<form action="" method="post" class="modal-content">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="task" value="mu.save" />
			<input type="hidden" name="id" value="" />
			<div class="modal-header"><h3 class="title"><?php _e('New Measurent Unit', 'mb'); ?></h3></div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php _e('Code', 'mb') ?></label>
					<input type="text" id="code" name="code" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Unit', 'mb') ?></label>
					<input type="text" id="unit" name="unit" value="" class="form-control" />
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-default" data-dismiss="modal"><?php _e('Cancel', 'mb'); ?></a>
				<button type="submit" class="btn btn-primary"><?php _e('Save', 'mb'); ?></button>
			</div>
		</form>
	</div>
</div>
<script>
jQuery(function()
{
	jQuery('#btn-new-mu').click(function()
	{
		var form = jQuery('#new-mu-modal form').get(0);
		form.id.value	= 0;
		form.code.value = '';
		form.unit.value = '';
		jQuery('#new-mu-modal').modal('show');
		return false;
	});
	jQuery('.edit-mu').click(function()
	{
		var form = jQuery('#new-mu-modal form').get(0);
		form.id.value	= this.dataset.id;
		form.code.value = this.dataset.code;
		form.unit.value = this.dataset.name;
		jQuery('#new-mu-modal').modal('show');
		return false;
	});
});
</script>
