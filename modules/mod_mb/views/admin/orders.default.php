<?php
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

$statuses   = mb_get_order_statuses();
$can_delete = $user->can('mb_delete_order');
$can_edit   = $user->can('mb_edit_order');
?>
<div class="wrap">
	<h2>
		<?php _e('Orders', 'mb'); ?>
		<a href="<?php print b_route('index.php?mod=mb&view=orders.new'); ?>" class="btn btn-primary pull-right"><?php _e('New Order', 'mb'); ?></a>
	</h2>
	<div class="form-group">
		<?php b_do_action('mb_orders_buttons'); ?>
	</div>
	<form action="" method="get">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="view" value="orders.default" />
		<div class="row">
			<div class="col-md-2">
				<select name="store_id" class="form-control">
					<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
					<?php foreach($stores as $s): ?>
					<option value="<?php print $s->store_id; ?>" <?php print $s->store_id == $this->request->getInt('store_id') ? 'selected' : ''; ?>>
                        <?php print $s->store_name; ?>
                    </option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md-2">
				<input type="text" name="date_from" value="<?php print sb_format_date($date_from); ?>" class="datepicker form-control">
			</div>
			<div class="col-md-2">
				<input type="text" name="date_to" value="<?php print sb_format_date(date($date_to)); ?>" class="datepicker form-control">
			</div>
			<div class="col-md-3">
				<input type="text" name="keyword" value="<?php print $keyword; ?>" class="form-control">
			</div>
			<div class="col-md-2">
				<select name="status" class="form-control">
					<option value="-1">-- estado --</option>
					<?php foreach($statuses as $key => $status): ?>
					<option value="<?php print $key; ?>" <?php print $this->request->getString('status') == $key ? 'selected' : ''; ?>>
                        <?php print $status; ?>
                    </option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			</div>
		</div>
	</form><br/>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" /> 
		<table id="table-orders" class="table table-condensed">
		<thead>
		<tr>
			<th><input type="checkbox" name="selector" value="1" class="tcb-select-all" /></th>
			<th>#</th>
			<th><?php _e('Store', 'mb'); ?></th>
			<th><?php _e('User', 'mb'); ?></th>
			<th><?php _e('Order', 'mb'); ?></th>
			<th><?php _e('Customer', 'mb'); ?></th>
			<th><?php _e('Date', 'mb'); ?></th>
			<th><?php _e('Status', 'mb'); ?></th>
			<th><?php _e('Total', 'mb'); ?></th>
			<th><?php _e('Actions', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($orders as $o): $user = new SB_User($o->user_id); ?>
		<tr>
			<td class="text-center"><input type="checkbox" name="ids[]" value="<?php print $o->order_id; ?>" class="tcb-select" /></td>
			<td class="text-center"><?php print $i; ?></td>
			<td style=""><?php print $o->store_name; ?></td>
			<td style=""><?php print $user->username; ?></td>
			<td class="text-center">
				<a href="<?php print $can_edit ? $this->Route('index.php?mod=mb&view=orders.edit&id='.$o->order_id) : 'javascript:;'; ?>">
					#<?php print $o->sequence; ?>
				</a>
			</td>
			<td style="">
				<?php if( $o->customer_id > 0 ): ?>
				<a href="<?php print b_route('index.php?mod=customers&view=edit&id='.$o->customer_id); ?>">
					<?php printf("%s %s", $o->customer->first_name, $o->customer->last_name); ?>
				</a>
				<?php else: ?>
					<?php _e('Guest', 'mb'); ?>
				<?php endif; ?>
			</td>
			<td class="text-right"><?php print sb_format_datetime($o->creation_date); ?></td>
			<td class="text-center" style="">
				<?php
				$status = __('Unknow', 'mb');
				$class = 'danger';
				if( strtolower($o->status) == 'sold' || strtolower($o->status) == 'complete' )
				{
					$status = __('Complete', 'mb');
					$class = 'success';
				}
				elseif( strtolower($o->status) == 'pending' )
				{
					$status = __('Pending', 'mb');
					$class = 'warning';
				}
				elseif( strtolower($o->status) == 'cancelled' )
				{
					$status = __('Cancelled', 'mb');
					$class = 'danger';
				}
				elseif( strtolower($o->status) == 'pre-sale' )
				{
					$status = $statuses[$o->status];
					$class = 'warning';
				}
				elseif( isset($statuses[$o->status]) )
				{
					$class = "order-status-{$o->status}";
					$status = $statuses[$o->status];
				}
				?>
				<span class="label label-<?php print $class; ?>"><?php print $status; ?></span>
			</td>
			<td class="text-right"><?php print $o->total; ?></td>
			<td class="text-right" style="">
				<a href="<?php print b_route('index.php?mod=mb&view=orders.view&id='.$o->order_id); ?>" title="<?php _e('View', 'mb'); ?>"
					class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>
				<?php if( $can_edit ):?>
				<a href="<?php print b_route('index.php?mod=mb&view=orders.edit&id='.$o->order_id); ?>" title="<?php _e('Edit', 'mb'); ?>"
					class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-edit"></span>
				</a>
				<?php endif; ?>
				<?php if( $can_delete ): ?>
				<a href="<?php print b_route('index.php?mod=mb&task=orders.delete&id='.$o->order_id); ?>" 
					class="confirm btn btn-default btn-sm" 
					data-message="<?php _e('Are you sure to delete this order?', 'mb'); ?>"
					title="<?php _e('Delete', 'mb'); ?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
				<?php endif; ?>
				<?php b_do_action('mb_order_action_btn', $o); ?>
			</td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		</table>
		<?php lt_pagination(b_route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
	</form>
</div>
