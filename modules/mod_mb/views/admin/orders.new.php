<?php
?>
<style>
table.order-totals{width:200px;float:right;}
table.order-totals th, table.order-totals td{text-align:right;font-size:17px;}
</style>
<div class="wrap">
	<h2><?php print $title; ?></h2>
	<form action="" method="post" id="order-form" class="form-group-sm">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="orders.save" />
		<?php if( isset($order) ): ?>
		<input type="hidden" name="id" value="<?php print $order->order_id; ?>" />
		<?php endif; ?>
		<div class="row">
			<div class="col-md-9">
				<div class="panel">
					<div class="panel-heading">
						<?php _e('Order Info', 'mb'); ?>
					</div>
					<div class="panel-body form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php _e('Order Date:', 'mb'); ?></label>
							<div class="col-sm-10">
								<input type="text" name="order_date" value="<?php print isset($order) ? sb_format_datetime($order->order_date) : ''; ?>" class="datepicker form-control" />
							</div>
						</div>
						<div class="form-inline form-group">
							<label class="col-sm-2 control-label"><?php _e('Store:', 'mb'); ?></label>
							<div class="col-sm-10">
								<select name="store_id" class="form-control">
									<option value="-1"><?php _e('-- store --', 'mb');  ?></option>
									<?php foreach($stores as $store): ?>
									<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == @$order->store_id ? 'selected' : '';?>>
										<?php print $store->store_name; ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php if( isset($order) ):?>
						<div class="form-inline form-group">
							<label class="col-sm-2 control-label"><?php _e('User:', 'mb'); ?></label>
							<div class="col-sm-10">
								<p class="form-control-static"><?php print isset($order) ? $user->username : ''; ?></p>
							</div>
						</div>
						<?php endif; ?>
						<fieldset>
							<legend><?php _e('Customer', 'mb'); ?></legend>
							<input type="hidden" id="customer_id" name="customer_id" value="<?php print isset($order) ? $order->customer_id : ''; ?>" />
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<label class="col-sm-2 control-label"><?php _e('Name:', 'mb'); ?></label>
										<div class="col-sm-10">
											<input type="text" id="customer_name" name="customer_name" value="<?php print isset($order) && $order->customer ? $order->customer->first_name . ' ' . $order->customer->last_name : ''; ?>" class="form-control" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<a href="javascript:;" id="btn-search-customer" class="btn btn-default btn-sm" title="<?php _e('Search customer', 'mb');?>">
										<span class="glyphicon glyphicon-search"></span>
									</a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<label class="col-sm-2 control-label"><?php _e('Email:', 'mb'); ?></label>
										<div class="col-sm-10">
											<input type="text" id="customer_email" name="customer_email" value="<?php print isset($order) && $order->customer ? $order->customer->email : ''; ?>" class="form-control" />
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend><?php _e('Shipping', 'mb'); ?></legend>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php _e('First name:', 'mb'); ?></label>
								<div class="col-sm-10">
									<input type="text" name="meta[_shipping_fname]" value="<?php print isset($order) ? $order->_shipping_fname : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php _e('Last name:', 'mb'); ?></label>
								<div class="col-sm-10">
									<input type="text" name="meta[_shipping_lname]" value="<?php print isset($order) ? $order->_shipping_lname : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php _e('Address:', 'mb'); ?></label>
								<div class="col-sm-10">
									<input type="text" name="meta[_shipping_address]" value="<?php print isset($order) ? $order->_shipping_address : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php _e('City:', 'mb'); ?></label>
								<div class="col-sm-10">
									<input type="text" name="meta[_shipping_city]" value="<?php print isset($order) ? $order->_shipping_city : ''; ?>" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php _e('Country:', 'mb'); ?></label>
								<div class="col-sm-10">
									<input type="text" name="meta[_shipping_country]" value="<?php print isset($order) ? $order->_shipping_country : ''; ?>" class="form-control" />
								</div>
							</div>
						</fieldset>
						<?php b_do_action('mb_order_info_fields', isset($order) ? $order : null); ?>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<?php _e('Order Items', 'mb'); ?>
					</div>
					<div class="panel-body">
						<table id="table-order-items" class="table">
						<thead>
						<tr>
							<th><input type="checkbox" class="tcb-select-all" name="selector" value="1" /></th>
							<th><?php _e('Code', 'mb'); ?></th>
							<th><?php _e('Item', 'mb'); ?></th>
							<th><?php _e('Price', 'mb'); ?></th>
							<th><?php _e('Qty', 'mb'); ?></th>
							<th><?php _e('Total', 'mb'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php if( isset($order) ): ?>
							<?php if( $order->status == 'complete' ): ?>
							<?php $num = 1 ;foreach($order->GetItems() as $i => $item): ?>
							<?php 
							$name = $item->product_name ? $item->product_name : $item->name;
							?>
							<tr data-id="<?php print $item->product_id; ?>">
								<td class="text-center">
									<input type="hidden" name="item[<?php print $i; ?>][product_id]" value="<?php print $item->product_id; ?>" />
									<input type="hidden" name="item[<?php print $i; ?>][code]" value="<?php print $item->product_code; ?>" />
									<input type="text" name="item[<?php print $i; ?>][name]" value="<?php print $name; ?>" style="display:none;" />
									<input type="hidden" name="item[<?php print $i; ?>][price]" value="<?php print $item->price; ?>" />
									<input type="hidden" name="item[<?php print $i; ?>][qty]" value="<?php print $item->quantity; ?>"  />
									<?php print $num; ?>
								</td>
								<td>
									<?php print $item->product_code; ?>
								</td>
								<td>
									<?php print $name; ?>
								</td>
								<td class="text-right">
									<?php print number_format($item->price, 2, '.', ','); ?>
								</td>
								<td class="text-center">
									<?php print $item->quantity; ?>
								</td>
								<td class="text-right">
									<span class="product-total"><?php print number_format($item->total, 2, '.', ','); ?></span>
								</td>
							</tr>
							<?php $num++; endforeach; ?>
							<?php else: ?>
							<?php foreach($order->GetItems() as $i => $item): ?>
							<tr data-id="<?php print $item->product_id; ?>">
								<td class="text-center">
									<input type="checkbox" name="item[<?php print $i; ?>][id]" value="<?php print $item->item_id; ?>" class="tcb-select" />
									<input type="hidden" name="item[<?php print $i; ?>][product_id]" value="<?php print $item->product_id; ?>" />
								</td>
								<td>
									<input type="text" name="item[<?php print $i; ?>][code]" value="<?php print $item->product_code; ?>" class="code form-control" />
								</td>
								<td>
									<?php 
									$name = $item->product_name ? $item->product_name : $item->name;
									print $name; 
									?>
									<input type="hidden" name="item[<?php print $i; ?>][name]" value="<?php print $name; ?>" class="name" /></td>
								<td class="text-right">
									<input type="text" class="form-control price" value="<?php print $item->price; ?>" name="item[<?php print $i; ?>][price]">
								</td>
								<td class="text-center">
									<input type="number" class="form-control qty" value="<?php print $item->quantity; ?>" name="item[<?php print $i; ?>][qty]" min="1">
								</td>
								<td class="text-right"><span class="product-total"><?php print $item->total; ?></span></td>
							</tr>
							<?php endforeach;?>
							<?php endif; ?>
						<?php endif; ?>
						</tbody>
						</table>
						<table class="order-totals">
						<tr>
							<th><?php _e('Discount:', 'mb'); ?></th>
							<td><span id="order-discount"><?php print isset($order) ? $order->discount : '0.00'; ?></span></td>
						</tr>
						<tr>
							<th><?php _e('Shipping:', 'mb'); ?></th>
							<td><span id="order-shipping">0.00</span></td>
						</tr>
						<tr>
							<th><?php _e('Tax:', 'mb'); ?></th>
							<td><span id="order-tax"><?php print isset($order) ? number_format((float)$order->total_tax, 2, '.', ',') : '0.00'; ?></span></td>
						</tr>
						<tr>
							<th><?php _e('Total:', 'mb'); ?></th>
							<td>
								<span id="order-total"><?php print isset($order) ? number_format((float)$order->total, 2, '.', ',') : '0.00'; ?></span>
							</td>
						</tr>
						</table>
						<div class="clearfix"></div>
						<?php if( !isset($order) || (isset($order) && $order->status != 'complete') ): ?>
						<div class="form-group">
							<a href="javascript:;" id="btn-add-product" class="btn btn-default btn-sm">
								<span class="glyphicon glyphicon-plus"></span> <?php _e('Add Product', 'mb'); ?></a>
							<a href="javascript:;" id="btn-remove-product" class="btn btn-default btn-sm">
								<span class="glyphicon glyphicon-trash"></span> <?php _e('Remove Product', 'mb'); ?></a>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php b_do_action('mb_order_panels', isset($order) ? $order : null); ?>
			</div>
			<div class="col-md-3">
				<div class="panel">
					<div class="panel-heading">
						<?php _e('Order Actions', 'mb'); ?>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label><?php _e('Status', 'mb'); ?></label>
							<select name="status" class="form-control">
								<option value="-1"><?php _e('-- order status --', 'mb'); ?></option>
								<?php foreach($order_status as $status => $label): ?>
								<option value="<?php print $status; ?>" <?php print (isset($order) && $status == $order->status) ? 'selected' : ''; ?>>
									<?php print $label; ?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label><?php _e('Payment Status', 'mb'); ?></label>
							<select name="payment_status" class="form-control">
								<option value="-1"><?php _e('-- payment status --', 'mb'); ?></option>
								<?php foreach($payment_status as $status => $label): ?>
								<option value="<?php print $status; ?>" <?php print (isset($order) && $status == $order->payment_status) ? 'selected' : ''; ?>>
									<?php print $label; ?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label><?php _e('Action', 'mb'); ?></label>
							<select name="action" class="form-control">
								<option value="-1"><?php _e('-- order action --', 'mb'); ?></option>
								<?php foreach($order_actions as $action => $label): ?>
								<option value="<?php print $action; ?>">
									<?php print $label; ?>
								</option>
								<?php endforeach; ?>
							</select>
							<a href="index.php?mod=mb&task=orders.print_receipt&id=<?php print $order->order_id; ?>" target="_blank" class="btn btn-warning">
								<span class="glyphicon glyphicon-print"></span>
								Imprimir
							</a>
						</div>
						<div class="form-group">
							<a href="<?php print b_route('index.php?mod=mb&view=orders.default'); ?>" class="btn btn-secondary">
								<?php _e('Cancel', 'mb'); ?></a>
							<button type="submit" class="btn btn-secondary"><?php _e('Save', 'mb'); ?></button>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<?php _e('Order Notes', 'mb'); ?>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<textarea class="form-control" name="notes"><?php print isset($order) ? $order->details : ''; ?></textarea>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<?php _e('Attachments', 'mb'); ?>
					</div>
					<div class="panel-body">
						<div id="order-files">
							<?php if( isset($order) && is_array($order->attachments) ): foreach($order->attachments as $a): ?>
							<div>
								<a href="<?php print UPLOADS_URL . '/' . $a->file; ?>" target="_blank"><?php print $a->title; ?></a>
                                <a href="<?php print b_route('index.php?mod=mb&task=orders.da&id='.$order->order_id . '&aid='.$a->attachment_id)?>" class="confirm"
									data-message="<?php _e('Are you sure to delete the order attachment?', 'mb'); ?>"
									title="<?php _e('Delete attachment', 'mb'); ?>">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
							</div>
							<?php endforeach; endif;?>
						</div>
						<div id="uploader" class="btn btn-primary center-block">
						<?php _e('Upload file', 'mb'); ?>
						</div>
						<span id="uploading" style="display:none;">
							<img src="<?php print BASEURL; ?>/js/fineuploader/loading.gif" alt=""  /><?php _e('Uploading file', 'mb'); ?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</form>
	<?php if( isset($order) ):?>
	<script>
	var uploader = new qq.FineUploaderBasic({
		//element: document.getElementById("uploader"),
		//template: 'qq-template-gallery',
		button: document.getElementById('uploader'),
		request: {
			endpoint: '<?php print b_route('index.php?mod=mb&task=orders.upload&id=' . $order->order_id); ?>'
		},
		callbacks: 
		{
			onSubmit: function(id, fileName) 
			{
			},
			onUpload: function(id, fileName) 
			{
				jQuery('#uploading').css('display', 'block');
			},
			onProgress: function(id, fileName, loaded, total) 
			{
				
			},
			onComplete: function(id, fileName, responseJSON) 
			{
				jQuery('#uploading').css('display', 'none');
				if (responseJSON.success) 
				{
					jQuery('#the-banner img:first').attr('src', responseJSON.image_url).css('display', 'inline');
					jQuery('#remove-banner').css('display', 'inline');
	            } 
	            else 
				{
					alert(responseJSON.error);
	            }
			}
		}
	});
	</script>
	<?php endif; ?>
	<script>
	/**
	* The callback for product selection
	**/
	function select_product_callback(data)
	{
		mb_order.AddProduct(data);
		jQuery(document).trigger('on_product_callback', data);
		jQuery('#modal-product-search').modal('hide');
	}
	/**
	* The callback for customer selection
	**/
	function select_callback(data)
	{
		jQuery('#customer_id').val(data.customer_id);
		jQuery('#customer_name').val(data.name);
		jQuery('#customer_email').val(data.email);
		jQuery('#search-customer-modal').modal('hide');
		jQuery(document).trigger('on_customer_callback', data);
	}
	var mb_order = 
	{
		RowTpl: '<tr data-id="{product_id}">\
					<td class="text-center">\
						<input type="checkbox" name="item[{index}][id]" value="" />\
						<input type="hidden" name="item[{index}][product_id]" value="{product_id}" />\
					</td>\
					<td><input type="text" name="item[{index}][code]" value="{code}" class="code" /></td>\
					<td>{name}<input type="hidden" name="item[{index}][name]" value="{name}" class="name" /></td>\
					<td class="text-right"><input type="text" name="item[{index}][price]" value="{price}" class="form-control price" /></td>\
					<td class="text-center"><input type="number" min="1" name="item[{index}][qty]" value="{qty}" class="form-control qty" /></td>\
					<td class="text-right"><span class="product-total">{total}</span></td>\
				</tr>',
		AddProduct: function(product)
		{
			var row = jQuery('#table-order-items tbody tr[data-product_id='+product.product_id+']');
			if( row.length > 0  )
			{
				var qty = row.find('.qty').val()
				var price = row.find('.price').val()
				row.find('.qty').val(++qty);
				this.CalculateRowTotal(row);
			}
			else
			{
				var index = jQuery('#table-order-items tbody tr').length;
				row = this.RowTpl.replace(/{index}/g, index)
								.replace(/{product_id}/g, product.product_id)
								.replace(/{code}/g, product.product_code)
								.replace(/{name}/g, product.product_name)
								.replace(/{price}/g, product.product_price)
								.replace(/{qty}/g, 1)
								.replace(/{total}/, product.product_price);
				jQuery('#table-order-items tbody').append(row)
			}
			this.CalculateTotals();
		},
		DeleteProduct: function()
		{
		},
		CalculateRowTotal: function(row)
		{
			var qty = parseInt(row.find('.qty').val());
			var price = parseFloat(row.find('.price').val());
			row.find('.product-total').html(qty * price);
		},
		CalculateTotals: function()
		{
			var totals = 0;
			jQuery('#table-order-items tbody tr').each(function(i, row)
			{
				var price = parseFloat(jQuery(row).find('.price:first').val());
				var qty = parseInt(jQuery(row).find('.qty:first').val());
				totals += price * qty;
			});
			jQuery('#order-discount').html('0.00');
			jQuery('#order-shipping').html('0.00');
			jQuery('#order-total').html(totals.toFixed(2));
		}
	};
	jQuery(function()
	{
		jQuery('#btn-search-customer').click(function()
		{
			jQuery('#search-customer-modal').modal('show');
		});
		jQuery('#btn-add-product').click(function()
		{
			jQuery('#modal-product-search').modal('show');
			return false;
		});
		jQuery('#btn-remove-product').click(function()
		{
			jQuery('#table-order-items tbody tr :checked').each(function(i, row)
			{
				var tr 				= jQuery(row).parents('tr');
				var current_index 	= tr.index();
				var new_index = current_index;
				tr.nextAll().each(function(i, ntr)
				{
					var inputs = jQuery(ntr).find('input');
					inputs.each(function(ii, input)
					{
						if(input.name.indexOf('item') != -1)
						{
							input.name = input.name.replace(/\[\d+\]/, '['+new_index+']')
						}
					});
					new_index++;
				});
				tr.remove();
			});
			mb_order.CalculateTotals();
			return false;
		});
		jQuery(document).on('keydown', '#order-form input[type=text]', function(e)
		{
			if( e.keyCode == 13 )
			{
				return false;
			}
		});
		jQuery(document).on('change keyup', 'tr td .qty', function(e)
		{
			mb_order.CalculateRowTotal(jQuery(this).parents('tr'));
			mb_order.CalculateTotals();
		});
		jQuery(document).on('change keyup', 'tr td .price', function(e)
		{
			mb_order.CalculateRowTotal(jQuery(this).parents('tr'));
			mb_order.CalculateTotals();
		});
	});
	</script>
</div>
<div id="search-customer-modal" class="modal fade in">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
	        	<h4 class="modal-title"><?php _e('Search Customer', 'mb'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe frameborder="0" style="width:100%;height:300px;" src="index.php?mod=customers&view=customers_list&tpl_file=module"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button data-dismiss="modal" class="btn btn-default" type="button"><?php _e('Close', 'mb'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<div id="modal-product-search" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title"><?php _e('Search Products', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<iframe style="width:100%;height:350px;" frameborder="0" src="<?php print b_route('index.php?mod=mb&view=products_search&tpl_file=module'); ?>"></iframe>
			</div>
		</div>
	</div>
</div>