<?php
$store = new SB_MBStore($order->store_id);
?>
<div class="wrap">
	<h2><?php _e('Oder Details', 'mb'); ?></h2>
	<div class="form-group">
		<a href="<?php print $this->Route('index.php?mod=mb&task=orders.print_receipt&id='.$order->order_id)?>" class="btn btn-warning btn-sm"
			target="_blank">
			<span class="glyphicon glyphicon-print"></span> <?php print _e('Print Receipt', 'mb'); ?>
		</a>
        <a href="<?php print $this->Route('index.php?mod=mb&task=pos.print&type=ticket&id='.$order->order_id)?>" 
            class="btn btn-warning btn-sm"
			target="_blank">
			<span class="glyphicon glyphicon-print"></span> <?php print _e('Print Ticket', 'mb'); ?>
		</a>
		<?php if( strtoupper($order->status) == 'COMPLETE' && $current_user->can('mb_revert_sale') ): ?>
		<a href="<?php print $this->Route('index.php?mod=mb&task=orders.revert&id='.$order->order_id)?>" 
            class="btn btn-danger btn-sm confirm"
			data-message="<?php _e('Are you sure to revert the order?', 'mb'); ?>"
			target="_blank">
			<span class="glyphicon glyphicon-transfer"></span> <?php print _e('Revert', 'mb'); ?>
		</a>
		<?php endif; ?>
		<?php b_do_action('mb_order_view_buttons', $order); ?>
	</div>
	<form action="" method="" onsubmit="return false;" class="form-group-sm">
		<div class="row">
			<div class="col-md-6">
				<fieldset class="form-horizontal">
					<legend><?php _e('Order', 'mb'); ?></legend>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php _e('Order Date', 'mb'); ?></label>
						<div class="col-sm-9">
							<span class="form-control-static"><?php print sb_format_date($order->order_date); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php _e('Store', 'mb'); ?></label>
						<div class="col-sm-9">
							<span class="form-control-static"><?php print $store->store_name ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php _e('User', 'mb'); ?></label>
						<div class="col-sm-9">
							<span class="form-control-static"><?php print $user->username; ?></span>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-md-6">
				<fieldset class="form-horizontal">
					<legend><?php _e('Customer', 'mb'); ?></legend>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php _e('Customer Name', 'mb'); ?></label>
						<div class="col-sm-9">
							<span class="form-control-static"><?php printf("%s %s", $order->customer->first_name, $order->customer->last_name) ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php _e('Email', 'mb'); ?></label>
						<div class="col-sm-9">
							<span class="form-control-static"><?php print $order->customer->email; ?></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<?php b_do_action('mb_order_info_fields', isset($order) ? $order : null); ?>
		<h3 class="text-center"><?php _e('Order Items', 'mb'); ?></h3>
		<table id="table-order-items" class="table table-condensed table-bordered">
		<thead>
		<tr>
			<th><input type="checkbox" class="tcb-select-all" name="selector" value="1" /></th>
			<th><?php _e('Code', 'mb'); ?></th>
			<th><?php _e('Item', 'mb'); ?></th>
			<th><?php _e('Price', 'mb'); ?></th>
			<th><?php _e('Qty', 'mb'); ?></th>
			<th><?php _e('Total', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $num = 1 ;foreach($order->GetItems() as $i => $item): ?>
		<tr data-id="<?php print $item->product_id; ?>">
			<td class="text-center">
				<?php print $num; ?>
			</td>
			<td>
				<?php print $item->product_code; ?>
			</td>
			<td>
				<?php 
				$name = $item->product_name ? $item->product_name : $item->name;
				print $name; 
				?>
				<?php print $name; ?>
			</td>
			<td class="text-right">
				<?php print number_format($item->price, 2, '.', ','); ?>
			</td>
			<td class="text-center">
				<?php print $item->quantity; ?>
			</td>
			<td class="text-right">
				<span class="product-total"><?php print number_format($item->total, 2, '.', ','); ?></span>
			</td>
		</tr>
		<?php $num++; endforeach; ?>
		</tbody>
		</table>
	</form>
</div>
