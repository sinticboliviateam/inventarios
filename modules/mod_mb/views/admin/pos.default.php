<?php
?>
<script>
var tb_pathToImage = "<?php print BASEURL; ?>/images/loadingAnimation.gif";
var store_id = <?php print $store->store_id; ?>;
jQuery(function()
{
	jQuery('#btn-expand-categories').click(function()
	{
		if( jQuery('#categories').hasClass('expanded') )
		{
			jQuery('#categories').css({height:'45px', overflow: 'hidden'});
			jQuery('#categories').removeClass('expanded');
		}
		else
		{
			jQuery('#categories').css({height:'150px', overflow: 'auto'});
			jQuery('#categories').addClass('expanded');
		}
		return false;
	});
    jQuery('#keyword').focus();
});
<?php b_do_action('mb_pos_javascript'); ?>
</script>
	<div id="pos-container" class="full-screen">
		<div id="pos-menu-container" class="dropdown">
			<div class="col-md-5">
				<ul id="pos-menu">
					<li>
						<a href="<?php print $this->Route('index.php?mod=mb&view=orders.default'); ?>" class="btn">
							<?php _e('Orders', 'mb'); ?>
						</a>
					</li>
					<li>
						<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
							<?php _e('Cashbox', 'mb'); ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="javascript:;" id="btn-open-cashbox"><?php _e('Open Cashbox', 'mb'); ?></a></li>
							<li><a href="javascript:;" id="btn-close-cashbox"><?php _e('Close Cashbox', 'mb'); ?></a></li>
							<li><a href="javascript:;" id="btn-cash-count"><?php _e('Cash Count', 'mb'); ?></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:;" class="btn" data-toggle="modal" data-target="#modal-spending"><?php _e('Register Spending', 'mb'); ?></a>
					</li>
				</ul>
			</div>
			<div id="the-store" class="col-md-2">
				<a href="javascript:;" ><b><?php print $store->store_name; ?></b></a>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-4">
						<b><?php _e('User:', 'mb'); ?></b>
						<span id="pos-current-user"><?php print sb_get_current_user()->username; ?></span>
					</div>
					<div class="col-md-4">
						<b><?php _e('Tax:', 'mb'); ?></b>
						<span id="pos-tax-rate">0.00</span>
					</div>
					<div class="col-md-4">
						<span id="powered"><?php _e('Mono Business', 'mb'); ?></span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<form id="form-pos" action="" method="post" class="form-pos-order">
			<input type="hidden" name="mod" value="mb" />
            <input type="hidden" name="ajax" value="1" />
			<input type="hidden" name="task" value="pos.register_sale" />
			<input type="hidden" id="store_id" name="store_id" value="<?php print $store->store_id; ?>" />
			<input type="hidden" id="notes" name="notes" value="" />
			<div class="col-md-7">
				<div class="form-group"></div>
				<div class="form-group" style="position:relative;height:47px;">
					<div id="categories" style="background:#fff;position:absolute;z-index:100;top:0;right:0;left:0;height:45px;padding:5px;border:1px solid #ececec;overflow:hidden;transition:height 0.3s;">
						<?php /*
						<ul>
							<?php if( isset($categories) ): foreach($categories as $cat): ?>
							<li style="margin:0 5px 5px 0;">
								<a href="javascript:;" class="category-button btn btn-default" data-id="<?php print $cat->category_id; ?>">
									<?php print $cat->name; ?>
								</a>
							</li>
							<?php endforeach; endif; ?>
						</ul>
						*/?>
						<?php print mb_dropdown_categories(['name' => 'cat_id', 'store_id' => $store->store_id, 'type' => 'select']); ?>
					</div>
					<a href="javascript:;" id="btn-expand-categories" style="position:absolute;top:0px;right:-20px;padding:3px;display:block;" title="<?php _e('Show categories', 'mb'); ?>">
						<span class="glyphicon glyphicon-chevron-down"></span>
					</a>
				</div>
				<div class="form-group">
					<div style="position:relative;">
						<input type="text" id="keyword" name="keyword" value="" placeholder="<?php _e('Search for products', 'mb'); ?>" 
							class="form-control" autocomplete="off" tabindex="1" />
						<img id="search-spin" src="<?php print BASEURL; ?>/images/spin.gif" alt="" style="display:none;position:absolute;top:10px;right:5px;" />
					</div>
				</div>
				<div id="products-container">
					<?php if( $layout == 'list' ): ?>
						<?php require_once MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'pos_list_layout.php'; ?>
					<?php elseif( $layout == 'grid' ): ?>
						<?php require_once MOD_MB_DIR . SB_DS . 'views' . SB_DS . 'admin' . SB_DS . 'pos_grid_layout.php'; ?>
					<?php else: b_do_action('mb_pos_layout_'.$layout); ?>
					<?php endif;?>
				</div><!-- end id="products-container" -->
				<div id="pos-views" class="btn-group">
					<a href="javascript:;" class="btn btn-default btn-xs <?php print $layout == 'list' ? 'active' : ''; ?>" data-layout="list">
						<?php _e('List', 'mb'); ?>
					</a>
					<a href="javascript:;" class="btn btn-default btn-xs <?php print $layout == 'grid' ? 'active' : ''; ?>" data-layout="grid">
						<?php _e('Grid', 'mb'); ?>
					</a>
				</div>
			</div>
			<div id="order" class="col-md-5">
				<div>&nbsp;</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
						<input type="text" id="barcode" name="barcode" value="" placeholder="<?php _e('Barcode', 'mb'); ?>" 
                            class="form-control" tabindex="2" />
					</div>
				</div>
				<div id="pos-order-table-container" >
					<div id="pos-order-table" class="pos-table">
						<div class="table-header">
							<div class="trow">
								<div class="col col-qty"><?php _e('Qty', 'mb'); ?></div>
								<div class="col col-product"><?php _e('Product', 'mb'); ?></div>
								<div class="col col-price"><?php _e('Price', 'mb'); ?></div>
								<div class="col col-total"><?php _e('Total', 'mb'); ?></div>
								<div class="col col-delete">&nbsp;</div>
							</div>
						</div>
						<div id="table-order-items" class="table-body">
							
						</div>
					</div>
				</div><!-- end class="pos-table-cotainer" -->
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label><?php _e('NIT/RUC/NIF:', 'mb'); ?></label>
							<input type="text" id="nit_ruc_nif" name="nit_ruc_nif" value="" autocomplete="off" placeholder="<?php _e('Customer NIT/RUC/NIF', 'mb'); ?>" 
								tabindex="3" class="form-control" />
						</div>
						<div class="form-group">
							<label><?php _e('Customer:', 'mb'); ?></label>
							<input type="text" id="customer" name="customer" value="" placeholder="<?php _e('Search customer', 'mb'); ?>" 
                                class="form-control" tabindex="4" />
							<input type="hidden" id="customer_id" name="customer_id" value="-1" />
						</div>
					</div><!-- end class="col-md-6" -->
					<div class="col-md-6">
						<table class="pos-table-totals">
						<tbody >
						<tr>
							<th><?php _e('Subtotal:', 'mb'); ?></th>
							<td><span id="currency-symbol"></span><span id="subtotal">0.00</span></td>
						</tr>
						<tr>
							<th><?php _e('Tax:', 'mb'); ?></th>
							<td><span id="currency-symbol"></span><span id="tax">0.00</span></td>
						</tr>
						<tr>
							<th><?php _e('Order Total', 'mb'); ?></th>
							<td><span id="currency-symbol"></span><span id="total">0.00</span></td>
						</tr>
						</tbody>
						</table>
					</div><!-- end class="col-md-6" -->
				</div>
				<div id="order-actions" class="">
					<?php b_do_action('mb_pos_buttons_before'); ?>
					<a href="javascript:;" id="btn-void-sale" class="btn btn-danger btn-lg" 
                        title="<?php _e('Void current sale', 'mb'); ?>" tabindex="5">
                        <?php _e('Void', 'mb'); ?>
                    </a>
					<a href="javascript:;" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modal-notes"
                        title="<?php _e('Add note to sale', 'mb'); ?>" tabindex="6">
						<?php _e('Note', 'mb'); ?>
					</a>
					<a href="javascript:;" id="btn-add-extra-cost" class="btn btn-warning btn-lg" 
                        title="<?php _e('Add extra cost to sale'); ?>" tabindex="7">
                        <?php _e('Add Extra Cost', 'mb'); ?>
                    </a>
					<a href="javascript:;" id="btn-save-sale" class="btn btn-primary btn-lg"
                        title="<?php _e('Save sale for later, the sale sale will be saved as draft', 'mb'); ?>"
                        tabindex="8">
                        <?php _e('Save', 'mb'); ?>
                    </a>
					<a href="javascript:;" id="btn-register-sale" class="btn btn-success btn-lg" title="<?php _e('Register the sale and print voucher', 'mb'); ?>"
                        tabindex="9">
                        <?php _e('Register Sale', 'mb'); ?></a>
					<?php b_do_action('mb_pos_buttons_after'); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
<div id="quick-view-dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php _e('Product Details', 'mb'); ?></div>
			<div class="modal-body">
				<table>
				<tr>
					<td><b><?php _e('Code:', 'mb'); ?></b></td>
					<td><span id="_code">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Product Name:', 'mb'); ?></b></td>
					<td><span id="_name">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Quantity:', 'mb'); ?></b></td>
					<td><span id="_qty">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Costo:', 'mb'); ?></b></td>
					<td><span id="_cost">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 1:', 'mb'); ?></b></td>
					<td><span id="_price1">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 2:', 'mb'); ?></b></td>
					<td><span id="_price2">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 3:', 'mb'); ?></b></td>
					<td><span id="_price3">0.00</span></td>
				</tr>
				<tr>
					<td><b><?php _e('Price 4:', 'mb'); ?></b></td>
					<td><span id="_price4">0.00</span></td>
				</tr>
				</table>
			</div>
			<div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-extra-cost" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php _e('Order Extra Cost', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row form-group-sm">
					<div class="col-md-7">
						<div class="form-group">
							<label><?php _e('Cost Name:', 'mb'); ?></label>
							<input type="text" name="_extra_item" value="" class="form-control" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label><?php _e('Amount:', 'mb'); ?></label>
							<input type="text" name="_extra_amount" value="0.00" class="form-control" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>&nbsp;</label><br/>
							<button type="button" id="btn-insert-extra-cost" class="btn btn-primary btn-sm"><?php _e('Add', 'mb'); ?></button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-notes"  role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php _e('Order Notes', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<textarea id="order-notes" class="form-control"></textarea>
			</div>
			<div class="modal-footer">
				<button id="btn-save-notes" type="button" class="btn btn-primary" data-dismiss="modal">
					<?php _e('Save', 'mb'); ?>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="success-dialog"  role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php _e('Sales Register Completed', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<p class="message"></p>
			</div>
			<div class="modal-footer">
				<div id="success-buttons">
					<button id="btn-save-notes" type="button" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-payment-methods" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php _e('Payment Method', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="">
					<?php foreach($payment_methods as $pm): ?>
					<a href="" id="btn-cash-payment" class="btn btn-default" style="width:150px;height:150px;text-align:center;float:left;margin:0 8px 8px 0;">
						<img src="<?php print $pm->image_url; ?>" alt="" title="<?php print $pm->name; ?>" /><br/>
						<span><?php print $pm->name; ?></span>
					</a>
					<?php endforeach; ?>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="btn-save-notes" type="button" class="btn btn-primary" data-dismiss="modal">
					<?php _e('Cancel', 'mb'); ?>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-gallery" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button id="btn-save-notes" type="button" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-cash-count" role="dialog">
	<div class="modal-dialog" role="document">
		<form id="form-cash-count" action="" method="post" class="modal-content">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="task" value="pos.ajax" />
			<input type="hidden" name="action" value="save_cashcount" />
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php _e('Cash Count', 'mb'); ?></h4>
			</div>
			<div class="modal-body form-horizontal form-group-sm">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#cashcount" data-toggle="tab"><?php _e('Cash Count', 'mb'); ?></a></li>
					<li><a href="#spending" data-toggle="tab"><?php _e('Spending', 'mb'); ?></a></li>
				</ul><!-- end class="nav nav-tabs" -->
				<div class="tab-content">
					<div id="cashcount" class="tab-pane active">
						<div class="form-group">
							<label class="control-label col-sm-3"><?php _e('Date:', 'mb');?></label>
							<div class="col-sm-9">
								<input type="text" name="creation_date" value="<?php print sb_format_date(time()); ?>" class="form-control datepicker" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('No. Custs 200:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" name="total_200" value="0" data-cut="200" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('No. Custs 100:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" name="total_100" value="0" data-cut="100" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('No. Custs 50:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" name="total_50" value="0" data-cut="50" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('No. Custs 20:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" name="total_20" value="0" data-cut="20" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('No. Custs 10:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" name="total_10" value="0" data-cut="10" class="form-control" />
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 5:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins" value="0" data-value="5" class="coins form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 2:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins[2]" value="0" data-value="2" class="coins form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 1:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins[1]" value="0" data-value="1" class="coins form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 0.50:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins[0.50]" value="0" data-value="0.50" class="coins form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 0.20:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins[0.20]" value="0" data-value="0.20" class="coins form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-7"><?php _e('Coins 0.10:', 'mb');?></label>
									<div class="col-sm-5">
										<input type="number" min="0" name="coins[0.10]" value="0" data-value="0.10" class="coins form-control" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<h4><?php _e('Calculated', 'mb'); ?></h4>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Cash Balance:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="balance" name="balance" value="" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Total Sales:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="cash_count_sales" name="sales" value="0" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Total Spends:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="total_spends" name="total_spends" value="0" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Cash:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="total_cash" name="total_cash" value="0" class="form-control" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<h4><?php _e('Manual', 'mb'); ?></h4>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Cash:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="manual_balance" name="manual_balance" value="" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php _e('Difference:', 'mb');?></label>
									<div class="col-sm-9">
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" id="difference" name="difference" value="0" class="form-control" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end id="cashcount" -->
					<div id="spending" class="tab-pane">
					</div><!-- end id="spending" -->
				</div><!-- end class="tab-content" -->
				
			</div>
			<div class="modal-footer">
				<a href="javascript:;" data-dismiss="modal" class="btn btn-danger"><?php _e('Close', 'mb'); ?></a>
				<button id="btn-save-cash-count" type="button" class="btn btn-primary" ><?php _e('Save', 'mb'); ?></button>
			</div>
		</form>
	</div>
</div>
<div class="modal fade" id="modal-spending" role="dialog">
	<div class="modal-dialog" role="document">
		<form id="form-register-spending" action="" method="post" class="modal-content form-group-sm">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="task" value="ajax" />
			<input type="hidden" name="action" value="register_spending" />
			<input type="hidden" name="store_id" value="<?php print $store->store_id; ?>" />
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php _e('Register Spending', 'mb'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php _e('Detail', 'mb'); ?></label>
					<input type="text" name="detail" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Amount', 'mb'); ?></label>
					<input type="text" name="amount" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Source', 'mb'); ?></label>
					<select name="source" class="form-control">
						<option value="cashbox"><?php _e('Cashbox', 'mb'); ?></option>
						<option value="prevision"><?php _e('Prevision', 'mb'); ?></option>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
				<button id="btn-save-spending" type="submit" class="btn btn-primary" data-dismiss="modal"><?php _e('Save', 'mb'); ?></button>
			</div>
		</form>
	</div>
</div>
<div class="modal fade" id="modal-open-cashbox" role="dialog">
	<div class="modal-dialog" role="document">
		<form id="form-opencashbox" action="" method="post" class="modal-content">
			<input type="hidden" name="mod" value="mb" />
			<input type="hidden" name="task" value="pos.ajax" />
			<input type="hidden" name="action" value="open_cashbox" />
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php _e('Open Cashbox', 'mb'); ?></h4>
			</div>
			<div class="modal-body form-group-sm">
				<div class="form-group">
					<label><?php _e('Initial Amount', 'mb'); ?></label>
					<input type="text" name="amount" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Notes', 'mb'); ?></label>
					<textarea name="notes" class="form-control"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
				<button type="submit" class="btn btn-primary"><?php _e('Save', 'mb'); ?></button>
			</div>
		</form>
	</div>
</div>
<div id="modal-processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Registering your sale, please wait...', 'mb'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>
<?php b_do_action('mb_pos_modals'); ?>
<style>footer{display:none;}</style>
