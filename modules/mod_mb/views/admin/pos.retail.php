<?php
$stores = $this->_stores;
if( !$user->IsRoot() )
{
	$stores = array(
			new SB_MBStore($user->_store_id)
	);
}
?>
<div class="wrap">
	<form action="" method="post" id="form-sale" class="container-fluid">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="pos.register_sale" />
		<input type="hidden" id="customer_id" name="customer_id" value="0" />
		<input type="hidden" id="payment_status" name="payment_status" value="complete" />
		<div class="row">
			<div class="col-md-6"><h2><span class="glyphicon glyphicon-qrcode"></span> <?php _e('Sales', 'mb'); ?></h2></div>
			<div class="col-md-6 text-right">
				<a href="javascript:;" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
				<button type="submit" class="btn btn-success"><?php _e('Register Sale', 'mb'); ?></button>
			</div>
		</div>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('Date', 'mb'); ?></label>
					<div class="col-sm-9">
						<input type="text" name="date" value="<?php print sb_format_date(time()); ?>" class="form-control" readonly="readonly"  />
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('NIT', 'mb'); ?></label>
					<div class="col-sm-9">
						<input type="text" id="nit_ruc_nif" name="nit_ruc_nif" value="" class="form-control" />
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('Customer', 'mb'); ?></label>
					<div class="col-sm-9">
						<div class="input-group">
							<input type="text" id="customer" name="customer" value="" class="form-control" />
							<span class="input-group-btn">
						        <button id="btn-search-customer" class="btn btn-default btn-sm" type="button" title="<?php _e('Search customer', 'mb'); ?>"><span class="glyphicon glyphicon-search"></span></button>
						    </span>
						</div>
					</div>
				</div>	
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('Invoice Name', 'mb'); ?></label>
					<div class="col-sm-9">
						<input type="text" id="invoice_name" name="meta[_billing_name]" value="" class="form-control" />
					</div>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('Stores', 'mb'); ?></label>
					<div class="col-sm-9">
						<select id="store_id" name="store_id" class="form-control">
							<?php foreach ($stores as $store): ?>
							<option value="<?php print $store->store_id; ?>"><?php print $store->store_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>	
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label"><?php _e('Sale Type', 'mb'); ?></label>
					<div class="col-sm-9">
						<select id="sale-type" name="meta[_sale_type]" class="form-control">
							<option value="-1"><?php _e('-- sale type --', 'mb'); ?></option>
							<option value="credit"><?php _e('Credit Sale', 'mb'); ?></option>
							<option value="cash"><?php _e('Cash Sale', 'mb'); ?></option>
						</select>
					</div>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="form-group">
				<a href="javascript:;" id="btn-add-item" class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-plus"></span> <?php _e('Add Item', 'mb'); ?>
				</a>
			</div>
			<table id="order-items" class="table table-condensed table-bordered">
			<thead>
			<tr>
				<th><?php _e('Num.', 'mb'); ?></th>
				<th><?php _e('Codigo', 'mb'); ?></th>
				<th><?php _e('Producto', 'mb'); ?></th>
				<th><?php _e('Batch', 'mb'); ?></th>
				<th><?php _e('Exp. Date', 'mb'); ?></th>
				<th><?php _e('Cant.', 'mb'); ?></th>
				<th><?php _e('Unit Price', 'mb'); ?></th>
				<th><?php _e('Total', 'mb'); ?></th>
				<th>&nbsp;</th>
			</tr>
			</thead>	
			<tbody>
			</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<table id="table-totals" class="totals-table" style="width:100%;">
				<tr>
					<th class="text-right"><?php _e('Sub Total:', 'quotes'); ?></th>
					<td class="text-right"><span id="retail-subtotal"><?php print isset($sale) ? $sale->subtotal : 0.00; ?></span></td>
				</tr>
				<tr id="row-total">
					<th class="text-right"><?php _e('Grand Total:', 'quotes'); ?></th>
					<td class="text-right"><span id="retail-total"><?php print isset($sale) ? $sale->total : 0.00; ?></span></td>
				</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label"><?php _e('Observations', 'mb'); ?></label>
					<textarea rows="" cols="" name="notes" class="form-control"></textarea>
				</div>
			</div>
			<div class="col-md-6">
			</div>
		</div>
	</form>
	<script>
	var search_product_url = '<?php print $this->Route('index.php?mod=ceass&view=search_product&tpl_file=module&store_id='); ?>';
	</script>
	<script src="<?php print MOD_MB_URL; ?>/js/retail.js"></script>
</div><!-- end class="wrap" -->
<div class="modal fade" id="search-customer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title" id="myModalLabel"><?php _e('Search Customer', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe src="<?php print $this->Route('index.php?mod=customers&view=customers_list&tpl_file=module'); ?>" style="width:100%;height:300px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'customers'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title"><?php _e('Buscar Producto', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe id="iframe-search" src="" style="width:100%;height:380px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Cerrar', 'ceass'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<div id="success-dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Factura Registrada', 'invoices'); ?></h4>
			</div>
			<div class="modal-body">
				<p id="success-message"><?php _e('The sale has been registered', 'mb'); ?></p>
			</div>
			<div id="success-buttons" class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'mb'); ?></button>
			</div>
		</div>
	</div>
</div>