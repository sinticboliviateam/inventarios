<?php
$see_cost = $user->can('mb_can_see_cost');
?>
	<div id="products" class="list-layout">
		<div id="products-table" class="pos-table">
			<div class="table-header">
				<div class="trow">
					<div class="col column-image"><?php _e('Image', 'mb'); ?></div>
					<div class="col column-id"><?php _e('ID', 'mb'); ?></div>
					<div class="col column-product"><?php _e('Product', 'mb'); ?></div>
					<div class="col column-qty"><?php _e('Qty', 'mb'); ?></div>
					<div class="col column-price"><?php _e('Price', 'mb'); ?></div>
					<div class="col column-add">&nbsp;<?php  ?></div>
				</div>
			</div>
			<div class="table-body">
				<?php if( isset($products) ): $i = 1; foreach($products as $p): ?>
				<?php include 'product_row.php'; ?>
				<?php $i++; endforeach; endif;?>
			</div>
		</div><!-- end class="pos-table" -->
	</div><!-- end id="products" -->
