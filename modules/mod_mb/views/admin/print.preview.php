<?php
?>
<style type="text/css">
#print-preview
{
	margin:10px auto;
	padding:10px;
	border:1px solid #000;
	box-shadow:5px 5px 0px 0px #000;
	background:#fff;
	position:relative;
	min-height:550px;
}
@media print
{
	body, #container, #content, .wrap{margin:0 !important;padding:0 !important;}
	#menu,#footer{display:none !important;}
	.no-print{display:none !important;}
	#print-preview{margin:0 !important;display:block !important;border:0;}
}
</style>
<div class="wrap">
	<div id="print-preview">
		<?php if( isset($iframe_url) ): ?>
		<iframe src="<?php print $iframe_url; ?>" frameborder="0" style="width:100%;height:600px;"></iframe>
		<?php elseif( isset($print_content) ): ?>
			<?php print $print_content; ?>
		<?php endif; ?>
	</div>
</div>
