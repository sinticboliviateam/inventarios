<?php
?>
<div class="trow <?php print (($i % 2) == 0) ? 'even' : 'odd'; ?>" 
	data-id="<?php print $p->product_id ?>"
	data-cost="<?php print isset($see_cost) && $see_cost ? $p->product_cost : 0 ?>"
	data-price="<?php print $p->product_price ?>"
	data-price_2="<?php print $p->product_price_2 ?>"
	data-price_3="<?php print $p->product_price_3 ?>"
	data-price_4="<?php print $p->product_price_4 ?>"
    data-quantity="<?php print $p->product_quantity ?>">
	<div class="col column-image">
		<a href="<?php print $p->getFeaturedImage('full')->GetUrl(); ?>" class="product-image" data-title="<?php print $p->product_name; ?>">
			<img src="<?php print $p->getFeaturedImage('150x150')->GetUrl(); ?>" alt="" style="max-width:100%;max-height:100%;" />
		</a>
	</div>
	<div class="col column-id text-center"><?php print $p->product_id; ?></div>
	<div class="col column-product">
		<a href="javascript:;" class="btn-view-product-info">
            <?php print $p->product_code; ?><br/>
            <?php print $p->product_name; ?>
        </a>
		<div>
			<?php if( $p->product_quantity <= 0 ): ?>
			<span class="label label-danger"><?php _e('No Stock', 'mb'); ?></span>
			<?php else: ?>
			<span class="label label-success"><?php _e('En Stock', 'mb'); ?></span>
			<?php endif; ?>
		</div>
	</div>
	<div class="col column-qty">
		<h4>
			<span class="label label-<?php print $p->product_quantity <= 0 ? 'danger' : 'success' ?>">
				<?php print $p->product_quantity; ?>
			</span>
		</h4>
	</div>
	<div class="col column-price">
		<?php print $p->product_price; ?>
	</div>
	<div class="col column-add">
		<a href="javascript:;" class="btn-add-product" data-product_id="<?php print $p->product_id; ?>">
			<img src="<?php print MOD_MB_URL; ?>/images/add-icon.png" alt="" />
		</a>
	</div>
</div>
