<?php
$store_id	= $this->request->getInt('store_id');
$keyword 	= $this->request->getString('keyword');
$search_by 	= $this->request->getString('search_by', 'name');
$products 	= array();
if( $store_id )
{
	$this->dbh->Select('p.*')
				->From('mb_products p')
				->Where(null);
	if( $search_by == 'name' )
	{
		$this->dbh->SqlAND(array('product_name' => $keyword), 'LIKE', '%', '%');
	}
	elseif( $search_by == 'code' )
	{
		$this->dbh->SqlAND(array('product_code' => $keyword));
	}
				
	$this->dbh->SqlAND(array('store_id' => $store_id));
	$this->dbh->Query(null);
	$products = $this->dbh->FetchResults();
}
?>
<style>
.products-search-container{width:98%;margin:0 auto;background:#fff;}
.products-list{border:1px solid #bcbcbc;height:100%;}
.products-list ul{list-style:none;}
.products-list ul li{list-style:none;float:left;padding:4px;}
.products-list .header{clear:both;background:linear-gradient(to bottom,#fff 0,#e0e0e0 100%);overflow:hidden;width:100%;}
.products-list .header li{text-align:center;border-bottom:1px solid #bcbcbc;border-right:1px solid #bcbcbc;}
.products-list .header li:last-child{border-right:0 !important;}
.products-list .body{clear:both;overflow:auto;width:100%;/*height:100%;*/background:#fff;min-height:auto;min-height:100%;}
.products-list .body .item{clear:both;overflow:hidden;border-bottom:1px solid #bcbcbc;}
.products-list .col-code{width:15%;text-align:center;}
.products-list .col-product{width:48%;}
.products-list .col-qty{width:15%;text-align: center;}
.products-list .col-cost{width:15%;text-align: right;}
.products-list .col-select{width:55px;text-align: center;}
</style>
<div class="products-search-container">
	<form action="" method="get">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="tpl_file" value="module" />
		<!-- 
		<input type="hidden" name="task" value="ajax" />
		<input type="hidden" name="action" value="search_product" />
		-->
		<input type="hidden" name="view" value="products_search" />
		<div class="row">
			<div class="col-xs-7 col-md-8">
				<div class="form-group">
					<input type="text" name="keyword" value="<?php print $keyword; ?>" placeholder="<?php _e('Search product...', 'mb'); ?>" class="form-control" />
				</div>
			</div>
			<div class="col-xs-3 col-md-2">
				<select name="store_id" class="form-control">
					<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
					<?php foreach(SB_Warehouse::getStores() as $store): ?>
					<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == $store_id ? 'selected' : ''; ?>>
						<?php print $store->store_name; ?>
					</option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-xs-2 col-md-2">
				<select name="search_by" class="form-control">
					<option value="name" <?php print $search_by == 'name' ? 'selected' : ''; ?>><?php _e('Name', 'mb'); ?></option>
					<option value="code" <?php print $search_by == 'code' ? 'selected' : ''; ?>><?php _e('Code', 'mb'); ?></option>
				</select>
			</div>
		</div>
		<div class="products-list">
			<ul class="header">
				<li class="col-code"><?php _e('Code', 'mb'); ?></li>
				<li class="col-product"><?php _e('Product', 'mb'); ?></li>
				<li class="col-qty"><?php _e('Qty', 'mb'); ?></li>
				<li class="col-cost"><?php _e('Cost', 'mb'); ?></li>
				<li class="col-select"><?php _e('Select', 'mb'); ?></li>
			</ul>
			<div class="body">
				<?php foreach($products as $p): ?>
				<ul class="item">
					<li class="col-code"><?php print $p->product_code; ?></li>
					<li class="col-product"><?php print $p->product_name; ?></li>
					<li class="col-qty"><span class="label label-success"><?php print $p->product_quantity; ?></span></li>
					<li class="col-cost"><?php print $p->product_cost; ?></li>
					<li class="col-select">
						<a class="btn btn-default btn-sm btn-select-product"
							<?php foreach($p as $prop => $val): ?>
							data-<?php print $prop; ?>="<?php print $val; ?>"
							<?php endforeach; ?>>
							<span class="glyphicon glyphicon-check"></span>
						</a>
					</li>
				</ul>
				<?php endforeach; ?>
			</div>
		</div>
	</form>
</div>
<script>
jQuery(function()
{
	jQuery('.btn-select-product').click(function()
	{
		var callback = window.select_product_callback || parent.select_product_callback;
		if( callback )
		{
			callback(this.dataset);
		}
		window.parent.jQuery(parent.document).trigger('product_selected', this.dataset);
		return false;
	});
});
</script>