<?php
?>
<table class="table">
<thead>
<tr>
	<th><?php _e('Code', 'mb'); ?></th>
	<th><?php _e('Provider', 'mb'); ?></th>
	<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php foreach($rows as $row): ?>
<tr>
	<td><?php print $row->supplier_id; ?></td>
	<td><?php print $row->supplier_name; ?></td>
	<td>
		<a href="javascript:;" class="select btn btn-default btn-xs" title="<?php _e('Select provider', 'mb'); ?>"
            data-id="<?php print $row->supplier_id; ?>" 
            data-name="<?php print $row->supplier_name; ?>">
			<span class="glyphicon glyphicon-ok"></span>
        </a>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<script>
jQuery(function()
{
	jQuery('.select').click(function()
	{
		if( parent.select_callback)
		{
			parent.select_callback(this.dataset);
		}
	});
	
});
</script>
