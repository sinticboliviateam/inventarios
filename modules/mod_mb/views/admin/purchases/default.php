<?php
?>
<div class="wrap">
	<h2 id="page-title">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <img src="<?php print MOD_MB_URL; ?>/images/76.png" alt="" /><?php _e('Purchases', 'mb'); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="text-right">
                    <a href="<?php print $this->Route('index.php?mod=mb&view=purchases.new'); ?>" class="btn btn-primary">
                        <?php _e('New Purchase', 'mb'); ?></a>
                </div>
            </div>
        </div>		
	</h2>
	<div class="row">
		<form action="" method="get">
			<input type="hidden" name="mod" value="quotes" />
			<div class="col-md-7">
				<div class="form-group">
					<input type="text" name="keyword" value="" class="form-control" />
				</div>
			</div>
			<button type="submit" id="btn-search-quote" class="btn btn-secondary" title="<?php _e('Search purchase', 'quotes'); ?>">
				<span class="glyphicon glyphicon-search"></span>
			</button>
		</form>
	</div>
	<?php $table->Show(); ?>
	<script>
	jQuery(function()
	{
		jQuery('.print-quote').click(function()
		{
			if( jQuery('#quote-iframe').length > 0 )
			{
				jQuery('#quote-iframe').remove();
			}
			var iframe = jQuery('<iframe id="quote-iframe" src="'+this.href+'" style="display:none;"></iframe>');
			//window.iframe = iframe;
			jQuery('body').append(iframe);
			try
			{
				iframe.load(function()
				{
					if(iframe.get(0).contentWindow.mb_print)
						iframe.get(0).contentWindow.mb_print();
				});
				
			}
			catch(e)
			{
				alert(e);
			}
			
			return false;
		});
	});
	</script>
</div>
