<?php

?>
<div class="wrap">
    <h2 id="page-title">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <img src="<?php print MOD_MB_URL; ?>/images/76.png" alt="" /> <?php print $title; ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <span class="page-buttons">
                        <a href="<?php print $this->Route('index.php?mod=mb&view=purchases.default'); ?>" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> <?php _e('Cancel', 'mb'); ?>
                        </a>
                        <?php if( isset($purchase) ): ?>
                        <a href="<?php print $this->Route('index.php?mod=mb&view=purchases.print&id='.$purchase->order_id); ?>" class="btn btn-warning" 
                            target="_blank">
                            <span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'mb'); ?>
                        </a>
                        <?php if( $purchase->status == 'complete' ): ?>
                        <a href="javascript:;" id="btn-print-labels" class="btn btn-warning">
                            <span class="glyphicon glyphicon-tag"></span> <?php _e('Print Labels', 'mb'); ?>
                        </a>
                        <?php endif; ?>
                        <?php if( $purchase->status != 'complete' ): ?>
                        <a href="<?php print $this->Route('index.php?mod=mb&task=purchases.receive&id='.$purchase->order_id)?>" class="btn btn-primary">
                            <span class="glyphicon glyphicon-cog"></span> <?php _e('Receive Stock', 'mb'); ?>
                        </a>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php if( (isset($purchase) && $purchase->status != 'complete') || !isset($purchase) ): ?>
                        <button type="button" id="btn-save" class="btn btn-success">
                            <span class="glyphicon glyphicon-save"></span> <?php _e('Save', 'mb'); ?>
                        </button>
                        <?php endif; ?>
                    </span>
                </div>
            </div>
        </div>
    </h2>
    <form id="form-purchase" class="form-group-sm">
        <input type="hidden" name="mod" value="mb" />
        <input type="hidden" name="task" value="purchases.save" />
        <?php if( isset($purchase) ): ?>
        <input type="hidden" name="purchase_id" value="<?php print $purchase->order_id; ?>" id="purchase_id" />
        <?php endif; ?>
        <input type="hidden" id="tax_rate" name="tax_rate" value="<?php print isset($purchase) ? $purchase->tax_rate : 0.00; ?>" />
        <fieldset>
            <legend><?php _e('General Details', 'mb'); ?></legend>
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label"><?php _e('ID', 'mb'); ?></label>
                                <input type="text" id="supplier_id" name="supplier_id" value="<?php print isset($purchase) ? $purchase->supplier_id : ''; ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label"><?php _e('Provider', 'mb'); ?></label>
                                <input type="text" id="supplier_name" name="supplier_name" 
                                    value="<?php print isset($purchase) ? $purchase->supplier->supplier_name : ''; ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div>
                                    <a href="javascript:;" id="btn-search-provider" class="btn btn-default" title="<?php _e('Search provider', 'mb'); ?>">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </a>
                                    <a href="javascript:;" id="btn-add-provider" class="btn btn-default" 
                                        title="<?php _e('Create provider', 'mb'); ?>">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <p id="customer-address"></p>
                        <div class="col-md-3"><span id="supplier-rfc"></span></div>
                        <div class="col-md-3"><span id="supplier-email"></span></div>
                        <div class="col-md-3"><span id="supplier-phone"></span></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label><?php _e('Store', 'mb'); ?></label>
                                <select id="store_id" name="store_id" class="form-control" <?php /*print isset($purchase) ? 'disabled' : '';*/ ?>>
                                    <option value="-1"><?php _e('-- store --', 'quotes'); ?></option>
                                    <?php foreach($stores as $s): ?>
                                    <option value="<?php print $s->store_id; ?>" <?php print isset($purchase) && $purchase->store_id == $s->store_id ? 'selected' : ''; ?>>
                                        <?php print $s->store_name; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label><?php _e('Warehouse', 'mb'); ?></label>
                                <select id="warehouse_id" name="warehouse_id" class="form-control" <?php /*print isset($purchase) ? 'disabled' : '';*/ ?>>
                                    <option value="0"><?php _e('-- warehouses --', 'mb'); ?></option>
                                    <?php if( isset($warehouses) && is_array($warehouses) ): foreach($warehouses as $w): ?>
                                    <option value="<?php print $w->id ?>" 
                                        <?php print isset($purchase) && $purchase->warehouse_id == $w->id ? 'selected' : ''; ?>>
                                        <?php print $w->name; ?>
                                    </option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>
                        </div>
                        <?php /*
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?php _e('Apply Promo', 'quotes'); ?></label>
                                <select name="coupon_id" class="form-control">
                                    <option value="-1"><?php _e('-- coupon --'); ?></option>
                                    <?php foreach($coupons as $c): ?>
                                    <option value="<?php print $c->coupon_id; ?>"><?php print $c->description; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        */?>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label><?php _e('Taxes', 'quotes'); ?></label>
                                <select id="tax_id" name="tax_id" class="form-control">
                                    <option value="-1"><?php _e('-- taxes --', 'quotes'); ?></option>
                                    <?php foreach($taxes as $t): ?>
                                    <option value="<?php print $t->tax_id; ?>"
                                        <?php print isset($purchase) && $purchase->tax_id == $t->tax_id ? 'selected' : ''; ?> data-tax="<?php print $t->rate; ?>"
                                        title="<?php printf("%s (%.2f%%)", $t->name, $t->rate); ?>">
                                        <?php printf("%s (%.2f%%)", $t->name, $t->rate); ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label><?php _e('Invoice Num.', 'mb'); ?></label>
                            <input type="text" name="meta[_invoice_num]" value="<?php print isset($purchase) ? $purchase->_invoice_num : ''; ?>" class="form-control" />
                        </div>
                        <div class="col-md-3">
                            <label><?php _e('Date', 'mb'); ?></label><br/>
                            <input type="text" name="order_date" value="<?php print isset($purchase) ? sb_format_date($purchase->order_date) : sb_format_date(time()); ?>" 
                                    class="form-control datepicker" />
                        </div>
                        <div class="col-md-3">
                            <label><?php _e('Delivery Date', 'mb'); ?></label>
                            <input type="text" name="delivery_date" value="<?php print isset($purchase) ? sb_format_date($purchase->delivery_date) : sb_format_date(time()); ?>" 
                                    class="form-control datepicker" />
                        </div>
                        <div class="col-md-3">
                            <label><?php _e('Payment Method', 'mb'); ?></label>
                            <select name="meta[_payment_method]" class="form-control">
                                <option value="cash"><?php _e('Cash', 'mb'); ?></option>
                                <option value="credit"><?php _e('Credit', 'mb'); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <!-- 
                    <div class="text-right">
                        <?php _e('Order No.', 'mb');?><br/>
                        <span id="order-number" class="text-red">
                            <b><?php print isset($purchase) ? sb_fill_zeros($purchase->order_id) : 'C-000??'; ?></b>
                        </span>
                    </div>
                     -->
                    <table id="table-totals" class="totals-table" style="width:100%;">
                    <tr>
                        <th class="text-right"><?php _e('Sub Total:', 'mb'); ?></th>
                        <td class="text-right"><span id="quote-subtotal"><?php print isset($purchase) ? $purchase->subtotal : 0.00; ?></span></td>
                    </tr>
                    <tr>
                        <th class="text-right"><?php _e('Discount:', 'mb'); ?></th>
                        <td class="text-right"><span id="quote-discount"><?php print isset($purchase) ? $purchase->discount : 0.00; ?></span></td>
                    </tr>
                    <tr>
                        <th class="text-right"><?php _e('Tax:', 'mb'); ?></th>
                        <td class="text-right"><span id="quote-tax"><?php print isset($purchase) ? $purchase->total_tax : 0.00; ?></span></td>
                    </tr>
                    <tr>
                        <th class="text-right"><b><?php _e('Advance:', 'mb'); ?></b></th>
                        <td class="text-right"><b><span id="invoice-total">0.00</span></b></td>
                    </tr>
                    <tr id="row-total">
                        <th class="text-right"><?php _e('Grand Total:', 'mb'); ?></th>
                        <td class="text-right"><span id="quote-total"><?php print isset($purchase) ? $purchase->total : 0.00; ?></span></td>
                    </tr>
                    </table>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <a href="javascript:;" id="btn-import-excel" class="btn btn-default bnt-xs">
                <span><img src="<?php print MOD_MB_URL; ?>/images/excel.png" alt="" style="height:20px;" /></span>
                <?php _e('Import from Excel', 'mb'); ?>
            </a>
        </div>
        <div class="form-group">
            <div class="input-group">
                <input type="text" id="search_product" name="search_product" value="" 
                    placeholder="<?php _e('Search product', 'mb'); ?>" class="form-control" />
                <span class="input-group-btn">
                    <button id="btn-add-item" class="btn btn-default" type="button">
                        <?php _e('Add item', 'mb'); ?>
                    </button>
                </span>
            </div>
        </div>
        <div id="table-container"></div>
        <script>
        var model = <?php print json_encode($model); ?>;
        var items = <?php print isset($purchase, $items) ? json_encode($items) : '[]'; ?>;
        //console.log(items);
        </script>
        <hr class="ht--"/>
        <div class="form-group">
            <label><?php _e('Notes', 'mb'); ?></label>
            <textarea name="notes" class="form-control"><?php print isset($purchase) ? $purchase->details : ''; ?></textarea>
        </div>
        <style>
        .inv-item-remove{width:5%;text-align:center;}
        .inv-item-remove img{width:25px;}
        .inv-item-number{width:7.5%;}
        .inv-item-name{width:50%;}
        .inv-item-qty{width:10%;}
        .inv-item-price{width:12.5%;}
        .inv-item-tax{width:12.5%;}
        .inv-item-total{width:15%;}
        .cool-table{background:#fff;}
        .cool-table .body{max-height:250px;}
        .cool-table .inv-item-number, .cool-table .inv-item-qty{text-align:center;}
        .cool-table .inv-item-qty input{text-align:center;}
        .cool-table .inv-item-price input, .cool-table .inv-item-tax input{text-align:right;}
        .cool-table .inv-item-total{text-align:right;}
        .table .column-product{width:45%;}
        .table .column-price input{text-align:right;}
        .sb-suggestions{max-height:200px;width:100%;}
        .sb-suggestions .the_suggestion{padding:5px;display:block;}
        .sb-suggestions .the_suggestion:focus,
        .sb-suggestions .the_suggestion:hover{background:#ececec;text-decoration:none;}, 
        </style>
    </form>
</div>
<!-- Modal -->
<div class="modal fade" id="search-provider-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php _e('Search Provider', 'customers'); ?></h4>
            </div>
            <div class="modal-body">
                <iframe data-src="<?php print $this->Route('index.php?mod=mb&view=providers_list&tpl_file=module'); ?>" style="width:100%;height:300px;" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'customers'); ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create-provider-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?php _e('Create Provider', 'customers'); ?></h4>
            </div>
            <div class="modal-body">
                <iframe data-src="<?php print $this->Route('index.php?mod=provider&view=new_prov&tpl_file=module'); ?>" style="width:100%;height:300px;" 
                        frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
            </div>
        </div>
    </div>
</div>
<?php if( isset($purchase) ): ?>
<div class="modal fade" id="send-quote-form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('Send Purchase Order', 'customers'); ?></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="mod" value="quotes" />
                <input type="hidden" name="task" value="send" />
                <?php if( isset($purchase) ): ?>
                <input type="hidden" name="id" value="<?php print $purchase->order_id; ?>" />
                <?php endif; ?>
                <div class="form-group">
                    <input type="text" name="email_to" value="<?php print $purchase->supplier->email; ?>" placeholder="<?php _e('Email', 'quotes'); ?>" class="form-control" />
                </div>
                <div class="form-group">
                    <input type="text" name="subject" value="" placeholder="<?php _e('Email subject', 'quotes'); ?>" class="form-control" />
                </div>
                <div class="form-group">
                    <textarea rows="" cols="" name="message" placeholder="<?php _e('Quote message', 'quoes'); ?>" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
                <button type="submit" class="btn btn-primary"><?php _e('Send Quote', 'quotes'); ?></button> 
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<div class="modal fade" id="modal-import-excel" role="dialog" data-keyboard="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <form action="" method="post" id="form-import" class="modal-content form-group-sm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?php _e('Import items from Excel', 'mb'); ?></h4>
            </div>
            <div class="modal-body">
                <?php 
                $options = '';
                foreach($excelColumns as $index => $col)
                    $options .= "<option value=\"$index\">$col</option>";
                ?>
                
                <input type="hidden" name="mod" value="mb" />
                <input type="hidden" name="task" value="purchases.importexcel" />
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label><?php _e('Code', 'mb'); ?></label>
                                <select id="xls_code" name="xls_code" class="form-control">
                                    <option value="-1"><?php _e('-- column --', 'mb'); ?></option>
                                    <?php print $options ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label><?php _e('Nombre', 'mb'); ?></label>
                                <select id="xls_name" name="xls_name" class="form-control">
                                    <option value="-1"><?php _e('-- name --', 'mb'); ?></option>
                                    <?php print $options ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label><?php _e('Barcode', 'mb'); ?></label>
                                <select id="xls_barcode" name="xls_barcode" class="form-control">
                                    <option value="-1"><?php _e('-- barcode --', 'mb'); ?></option>
                                    <?php print $options ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label><?php _e('Quantity', 'mb'); ?></label>
                                <select id="xls_qty" name="xls_qty" class="form-control">
                                    <option value="-1"><?php _e('-- quantity --', 'mb'); ?></option>
                                    <?php print $options ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label><?php _e('Cost', 'mb'); ?></label>
                                <select id="xls_cost" name="xls_cost" class="form-control">
                                    <option value="-1"><?php _e('-- cost --', 'mb'); ?></option>
                                    <?php print $options ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label><?php _e('Excel File', 'mb'); ?></label>
                            <input type="file" name="xls_file" class="form-control" />
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label><?php _e('Row Start', 'mb'); ?></label>
                                <input type="number" min="1" name="xls_row_start" value="1" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php _e('Close', 'customers'); ?></button>
                <button type="submit" class="btn btn-success">
                    <?php _e('Import', 'mb'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
<script>
jQuery(function()
{
    <?php if( isset($purchase) ): ?>
    if( items && items.length > 0 )
    {
        var form = jQuery('#form-modal-print-label').get(0);
        form.task.value = 'purchases.printlabels';
        if( jQuery(form).find('#order_id').length <= 0 )
        {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.id    = 'order_id';
            input.name = 'id';
            input.value = <?php print $purchase->order_id; ?>;
            form.appendChild(input);
        }
        jQuery('#btn-print-labels').click(function()
        {
            //$this->Route('index.php?mod=mb&task=purchases.printlabels&id='.$purchase->order_id);
            jQuery('#modal-print-labels').modal('show');
            return false;
        });
    }
    <?php endif; ?>
});
</script>
<?php lt_include_partial('mb', 'modal-label.php'); ?>
<div id="modal-processing" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="processing-message"><?php _e('Processing, please wait...', 'mb'); ?></div>
                <img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
            </div>
        </div>
    </div>
</div>
<script src="<?php print BASEURL ?>/js/table-model.js"></script>
