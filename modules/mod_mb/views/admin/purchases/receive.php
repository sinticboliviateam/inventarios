<div class="wrap">
	<h2 id="page-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<?php print $title; ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<span class="page-buttons">
						<a href="<?php print $this->Route('index.php?mod=mb&view=purchases.default'); ?>" class="btn btn-danger">
							<span class="glyphicon glyphicon-remove"></span> <?php _e('Cancel', 'mb'); ?>
						</a>
						<a href="<?php print $this->Route('index.php?mod=mb&view=purchases.print&id='.$purchase->order_id); ?>" class="btn btn-warning" 
							target="_blank">
							<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'mb'); ?>
						</a>
						<?php if( $purchase->status != 'complete' ): ?>
						<a href="javascript:;" id="btn-save" class="btn btn-success">
							<span class="glyphicon glyphicon-cog"></span> <?php _e('Receive Stock', 'mb'); ?>
						</a>
						<?php endif; ?>
					</span>
				</div>
			</div>
		</div>
	</h2>
	<form id="form-purchase" class="form-group-sm">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="purchases.do_receive" />
		<input type="hidden" name="purchase_id" value="<?php print $purchase->order_id; ?>" />
		<fieldset>
			<legend><?php _e('General Details', 'mb'); ?></legend>
			<div class="row">
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label class="control-label"><?php _e('ID', 'mb'); ?></label>
								<p class="control-static"><?php print $purchase->supplier_id; ?></p>
							</div>
						</div>
						<div class="col-md-7">
							<div class="form-group">
								<label class="control-label"><?php _e('Provider', 'mb'); ?></label>
								<p class="control-static"><?php print $purchase->supplier->supplier_name; ?></p>
							</div>
						</div>
					</div>
					<div class="row">
						<p id="customer-address"></p>
						<div class="col-md-3"><span id="supplier-rfc"></span></div>
						<div class="col-md-3"><span id="supplier-email"></span></div>
						<div class="col-md-3"><span id="supplier-phone"></span></div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label><?php _e('Store', 'mb'); ?></label>
								<p class="control-static">
									<?php print $purchase->store->store_name; ?>
								</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label><?php _e('Warehouse', 'mb'); ?></label>
								<p class="control-static">
									<?php print $purchase->warehouse->name; ?>
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label><?php _e('Invoice Num.', 'mb'); ?></label>
							<p class="control-static"><?php print $purchase->_invoice_num; ?></p>
						</div>
						<div class="col-md-3">
							<label><?php _e('Date', 'mb'); ?></label>
							<p class="control-static"><?php print sb_format_date($purchase->order_date) ?></p>
						</div>
						<div class="col-md-3">
							<label><?php _e('Delivery Date', 'mb'); ?></label>
							<p class="control-static"><?php print sb_format_date($purchase->delivery_date) ?></p>
						</div>
						<div class="col-md-3">
							<label><?php _e('Payment Method', 'mb'); ?></label>
							<p class="control-static"><?php print $purchase->_payment_method; ?></p>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="text-right">
						<?php _e('Order No.', 'mb');?><br/>
						<span id="order-number" class="text-red">
							<b><?php print sb_fill_zeros($purchase->order_id) ?></b>
						</span>
					</div>
					<table id="table-totals" class="totals-table" style="width:100%;">
					<tr>
						<th class="text-right"><?php _e('Sub Total:', 'mb'); ?></th>
						<td class="text-right">
							<span id="quote-subtotal"><?php print sb_number_format($purchase->subtotal); ?></span>
						</td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Discount:', 'mb'); ?></th>
						<td class="text-right">
							<span id="quote-discount"><?php print sb_number_format($purchase->discount);?></span>
						</td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Tax:', 'mb'); ?></th>
						<td class="text-right">
							<span id="quote-tax"><?php print sb_number_format($purchase->total_tax); ?></span>
						</td>
					</tr>
					<tr id="row-total">
						<th class="text-right"><?php _e('Grand Total:', 'mb'); ?></th>
						<td class="text-right">
							<span id="quote-total"><?php print sb_number_format($purchase->total); ?></span>
						</td>
					</tr>
					</table>
				</div>
			</div>
		</fieldset>
		<div id="table-container"></div>
		<script>
		var model = <?php print json_encode($model); ?>;
		var items = <?php print json_encode($items); ?>;
		//console.log(items);
		</script>
		<hr class="ht--"/>
		<div class="form-group">
			<label><?php _e('Notes', 'mb'); ?></label>
			<p><?php print $purchase->details ?></p>
		</div>
	</form>
</div>
<div id="modal-processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Processing, please wait...', 'mb'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>
