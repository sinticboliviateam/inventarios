<?php
?>
<div class="wrap">
	<h2 class="hidden-print"><?php _e('Reports', 'mb'); ?></h2>
	<ul class="hidden-print nav nav-tabs">
		<?php foreach( $this->instances as $key => $_obj): ?>
		<li <?php print $report == $key ? 'class="active"' : ''; ?>>
			<a href="<?php print $_obj->tabLink; ?>"><?php print $_obj->tabLabel; ?></a>
		</li>
		<?php endforeach; ?>
		<?php b_do_action('mb_reports_tabs'); ?>
	</ul>
	<div class="tab-content">
		<div id="" class="tab-pane active">
			<div><?php isset($obj) ? $obj->Form() : ''; ?></div>
			<div><?php isset($obj) ? $obj->Build() : ''; ?></div>
		</div>
	</div>
</div>