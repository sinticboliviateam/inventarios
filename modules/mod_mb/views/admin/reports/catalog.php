<?php
?>
<style type="text/css">
.text-center{text-align:center;}
.text-right{text-align:right;}
th{background-color:#337ab7;color:#fff;text-align:center;font-weight:bold;padding:4px;}
td{padding:4px;}
.col-no{width:30px;}
.col-product{width:190px;}
.col-qty{text-align:center;width:30px;}
.col-price1,.col-price2,.col-price3,.col-price4{width:60px;}
</style>
<h1 class="text-center"><?php print $title; ?></h1>
<table style="width:100%;">
<thead>
<tr>
	<th class="col-no"><?php _e('No.', 'mb'); ?></th>
	<th class="col-image"><?php _e('Image', 'mb'); ?></th>
	<th class="col-code"><?php _e('Code', 'mb'); ?></th>
	<th class="col-product"><?php _e('Product', 'mb'); ?></th>
	<th class="col-qty"><?php _e('Qty', 'mb'); ?></th>
	<th class="col-price1"><?php _e('Price 1', 'mb'); ?></th>
	<th class="col-price2"><?php _e('Price 2', 'mb'); ?></th>
	<th class="col-price3"><?php _e('Price 3', 'mb'); ?></th>
</tr>
</thead>
<tbody>
<?php $i = 1; foreach($products as $p): ?>
<tr>
	<td class="text-center col-no"><?php print $i; ?></td>
	<td class="text-center col-image"><img src="<?php print $p->getFeaturedImage(); ?>" width="150" /></td>
	<td class="text-center col-code"><?php print $p->product_code; ?></td>
	<td class="col-product"><?php print $p->product_name; ?></td>
	<td class="col-qty"><?php print $p->product_quantity; ?></td>
	<td class="col-price1 text-right"><?php print number_format($p->product_price, 2, '.', ','); ?></td>
	<td class="col-price2 text-right"><?php print number_format($p->product_price_2, 2, '.', ','); ?></td>
	<td class="col-price3 text-right"><?php print number_format($p->product_price_3, 2, '.', ','); ?></td>
</tr>
<?php $i++; endforeach; ?>
</tbody>
</table>