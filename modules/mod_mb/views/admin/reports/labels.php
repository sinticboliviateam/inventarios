<?php
?>
<style type="text/css">
.text-center{text-align:center;}
.text-right{text-align:right;}
th{background-color:#337ab7;color:#fff;text-align:center;font-weight:bold;padding:4px;}
td{padding:4px;}
.col-no{width:30px;}
.col-product{width:190px;}
.col-qty{text-align:center;width:30px;}
.col-price1,.col-price2,.col-price3,.col-price4{width:60px;}
</style>
<h1 class="text-center"><?php print $title; ?></h1>
<table style="width:100%;">
<?php $i = 0; foreach($products as $p): $barcode = '777231723897';?>
<?php if($i == 0 ): ?>
<tr>
<?php endif; ?>
	<td class="text-center">	
		<div >
			<img src="data:image/png;base64,
			<?php
			print base64_encode($generatorHTML->getBarcode($barcode, Picqer\Barcode\BarcodeGenerator::TYPE_EAN_13, 1, 60));
			?>" /><br/>
			<?php print $barcode; ?>
		</div>
		<div><?php print $p->product_code; ?></div>
	</td>
<?php $i++; if($i == 4): $i = 0; ?>
</tr>
<?php endif; ?>
<?php  endforeach; ?>
</table>