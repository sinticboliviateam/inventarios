<?php
?>
<div class="wrap">
	<div style="width:350px;margin:0 auto;">
		<style>
		.store-select{display:block;padding:5px 6px;}
		.store-select:hover{background:#ececec};
		</style>
		<div class="panel panel-default">
			<div class="panel-heading"><?php _e('Select Store', 'mb'); ?></div>
			<div class="panel-body">
				<div style="width:100%;height:350px;overflow:auto;">
					<?php foreach($stores as $store): ?>
					<a href="<?php print $this->Route('index.php?mod=mb&view=pos.default&store_id='.$store->store_id); ?>" class="store-select">
						<?php print $store->store_name; ?>
					</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>