<?php
?>
<div class="wrap">
	<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="save_settings" />
		<h2>
			<span class="glyphicon glyphicon-wrench"></span>
			<?php _e('Mono Business Settings', 'mb'); ?>
			<div class="pull-right">
				<button type="submit" class="btn btn-success"><?php _e('Save', 'mb'); ?></button>
			</div>
		</h2>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#mono-business" data-toggle="tab"><?php _e('General', 'mb'); ?></a></li>
			<li><a href="#barcode" data-toggle="tab"><?php _e('Barcode', 'mb'); ?></a></li>
			<?php b_do_action('mb_settings_tab', $ops); ?>
		</ul>
		<div class="tab-content">
			<div id="mono-business" class="tab-pane active">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<?php if( @$ops->business_logo ): ?>
								<div class="text-center">
									<img src="<?php print UPLOADS_URL; ?>/<?php print $ops->business_logo; ?>" alt="" />
								</div>
								<?php endif; ?>
								<label><?php _e('Business Logo:', 'mb'); ?></label>
								<input type="file" name="business_logo" value="" class="form-control" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label><?php _e('Business Name:', 'mb'); ?></label>
								<input type="text" name="ops[business_name]" value="<?php print @$ops->business_name; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('Business Address:', 'mb'); ?></label>
								<input type="text" name="ops[business_address]" value="<?php print @$ops->business_address; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('Business Telephone:', 'mb'); ?></label>
								<input type="text" name="ops[business_phone]" value="<?php print @$ops->business_phone; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('Business Mobile Telephone:', 'invoices'); ?></label>
								<input type="text" name="ops[business_mobile_telephone]" value="<?php print @$ops->business_mobile_telephone; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('City:', 'invoices'); ?></label>
								<input type="text" name="ops[business_city]" value="<?php print @$ops->business_city; ?>" class="form-control" />
							</div>
							<div class="form-group">
								<label><?php _e('Country:', 'invoices'); ?></label>
								<select name="ops[business_country]" class="form-control">
									<option value="-1"><?php _e('-- country --', 'invoices'); ?></option>
									<?php foreach( include INCLUDE_DIR . SB_DS . 'countries.php' as $code => $country): ?>
									<option value="<?php print $code; ?>" <?php print @$ops->business_country == $code ? 'selected' : ''; ?>><?php print $country; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label>
									<?php _e('Currency', 'emono'); ?>
								</label>
								<select name="ops[currency_code]" class="form-control">
									<option value="$">
										<?php _e('-- currency --', 'emono'); ?></option>
									<option value="USD" <?php print @$ops->currency_code == 'USD' ? 'selected' : ''; ?>>
										<?php _e('United State Dollar', 'emono'); ?>
									</option>
									<option value="&euro;" <?php print @$ops->currency_code == '&euro;' ? 'selected' : ''; ?>>
										<?php _e('Euro', 'emono'); ?>
									</option>
									<option value="Bs" <?php print @$ops->currency_code == 'Bs' ? 'selected' : ''; ?>>
										<?php _e('Boliviano', 'emono'); ?></option>
								</select>
								
							</div>
						</div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <fieldset>
                                <legend><?php _e('Point of Sale', 'mb'); ?></legend>
                                <div class="form-group">
                                    <label><?php _e('Receipt Type', 'mb'); ?></label>
                                    <select name="ops[pos][receipt_type]" class="form-control">
                                        <option value=""><?php _e('-- receipt type --', 'mb'); ?></option>
                                        <option value="default" <?php print @$ops->pos->receipt_type == 'default' ? 'selected' : ''; ?>>
                                            <?php _e('Default (A4 format)'); ?>
                                        </option>
                                        <option value="legal" <?php print @$ops->pos->receipt_type == 'legal' ? 'selected' : ''; ?>>
                                            <?php _e('Legal (Legal format)'); ?>
                                        </option>
                                        <option value="ticket" <?php print @$ops->pos->receipt_type == 'ticket' ? 'selected' : ''; ?>>
                                            <?php _e('Ticket (Thermal printers)', 'mb'); ?>
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Printing Type', 'mb'); ?></label>
                                    <select name="ops[pos][printing_type]" class="form-control">
                                        <option value=""><?php _e('-- printing type --', 'mb'); ?></option>
                                        <option value="browser" <?php print @$ops->pos->printing_type == 'browser' ? 'selected' : ''; ?>>
                                            <?php _e('Browser (Native Browser Printing)'); ?>
                                        </option>
                                        <option value="pdf" <?php print @$ops->pos->printing_type == 'pdf' ? 'selected' : ''; ?>>
                                            <?php _e('PDF (PDF document)'); ?>
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php _e('Silent print', 'mb'); ?></label>
                                    <input type="checkbox" name="ops[pos][silent_print]" value="1" 
                                        <?php print (int)@$ops->pos->silent_print == 1 ? 'checked' : ''; ?> />
                                    <span><?php _e('The document is sent to printer without preview', 'mb'); ?></span>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>				
			</div>
			<div id="barcode" class="tab-pane">
				<h3><?php _e('Barcode Generation', 'mb'); ?></h3>
				<div class="form-group">
					<label><?php _e('Country Code'); ?></label>
					<input type="text" name="ops[barcode_ean13_country]" value="<?php print @$ops->barcode_ean13_country; ?>" min="0" maxlength="3" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Company Code'); ?></label>
					<input type="text" name="ops[barcode_ean13_company]" value="<?php print @$ops->barcode_ean13_company; ?>" min="0" maxlength="5" class="form-control" />
				</div>
			</div><!-- end id="barcode" -->
			<?php b_do_action('mb_settings_pane', $ops); ?>
		</div>
	</form>
</div>
