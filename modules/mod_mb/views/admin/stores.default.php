<?php
?>
<div class="wrap">
	<h2 id="page-title"><?php print $this->__('Stores', 'mb'); ?></h2>
	<div>
		<a href="<?php print $this->Route('index.php?mod=mb&view=stores.new'); ?>" class="btn btn-primary">
			<?php print $this->__('New Store', 'mb'); ?>
		</a>
		<a href="<?php print $this->Route('index.php?mod=mb&view=warehouse.new'); ?>" class="btn btn-info">
			<?php print $this->__('New Warehouse', 'mb'); ?>
		</a>
		<a href="<?php print $this->Route('index.php?mod=mb&view=batch.new'); ?>" class="btn btn-warning">
			<?php print $this->__('New Batch', 'mb'); ?>
		</a>
	</div><br/>
	<table id="stores-datagrid" class="table">
	<thead>
	<tr>
		<th>#</th>
		<th><?php print $this->__('Name', 'mb'); ?></th>
		<th><?php print $this->__('Address', 'mb'); ?></th>
		<th><?php print $this->__('Total Products', 'mb'); ?></th>
		<th><?php print $this->__('Actions', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if(count($stores)): $i = 1;foreach($stores as $store): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $store->store_name; ?></td>
		<td><?php print $store->store_address; ?></td>
		<td>
			<?php print count($store->getProducts());?>
		</td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=mb&view=stores.edit&id='.$store->store_id)?>"
				title="<?php print $this->__('Edit', 'mb')?>" class="btn btn-default btn-xs">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=mb&task=stores.delete&id='.$store->store_id)?>" 
				class="confirm btn btn-default btn-xs"
				data-message="<?php print $this->__('Are you sure to delete the store?'); ?>"
				title="<?php print $this->__('Delete', 'mb')?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; else: ?>
	<tr><td colspan="4"><?php print $this->__('No stores found', 'mb'); ?></td></tr>
	<?php endif; ?>
	</tbody>
	</table>
</div>