<?php
?>
<div class="wrap">
	<h2 id="page-title"><?php print $page_title; ?></h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="stores.save" />
		<?php if( isset($the_store) ): ?>
		<input type="hidden" name="store_id" value="<?php print $the_store->store_id; ?>" />
		<?php endif; ?>
        <div class="form-group">
			<label><?php print $this->__('Code', 'mb'); ?></label>
			<input type="text" name="code" value="<?php print isset($the_store) ? $the_store->code : '';?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php print $this->__('Name', 'mb'); ?></label>
			<input type="text" name="store_name" value="<?php print isset($the_store) ? $the_store->store_name:'';?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php print $this->__('Address', 'mb'); ?></label>
			<textarea name="store_address" class="form-control"><?php print isset($the_store) ? $the_store->store_address:'';?></textarea>
		</div>
		<div class="form-group">
			<label><?php print $this->__('Phone', 'mb'); ?></label>
			<input type="text" name="store_phone" value="<?php print isset($the_store) ? $the_store->phone : ''; ?>" class="form-control" />
		</div>
		<h3><?php _e('Transaction Assignment', 'mb'); ?></h3>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Sale', 'mb'); ?></label>
					<select name="meta[_sale_tt_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): if($tt->in_out != 'out' ) continue;?>
						<option value="<?php print $tt->transaction_type_id; ?>" 
							<?php print isset($the_store) && $the_store->_sale_tt_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Sale Reversed', 'mb'); ?></label>
					<select name="meta[_revert_tt_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): if($tt->in_out != 'in' ) continue;?>
						<option value="<?php print $tt->transaction_type_id; ?>" 
							<?php print isset($the_store) && $the_store->_revert_tt_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Purchase', 'mb'); ?></label>
					<select name="meta[_purchase_tt_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): if($tt->in_out != 'in' ) continue;?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_purchase_tt_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Input Transfer', 'mb'); ?></label>
					<select name="meta[_transfer_tt_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): ?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_transfer_tt_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Output Transfer', 'mb'); ?></label>
					<select name="meta[_transfer_out_tt_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): ?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_transfer_out_tt_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Transfer Reverted', 'mb'); ?></label>
					<select name="meta[_tt_transfer_reverted]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): ?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_tt_transfer_reverted == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Positive Tweak', 'mb'); ?></label>
					<select name="meta[_ajuste_positivo_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): if( $tt->in_out == 'out' ) continue; ?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_ajuste_positivo_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Negative Tweak', 'mb'); ?></label>
					<select name="meta[_ajuste_negativo_id]" class="form-control">
						<option value="-1"><?php _e('-- transaction type --', 'mb'); ?></option>
						<?php foreach($ttypes as $tt): if( $tt->in_out == 'in' ) continue; ?>
						<option value="<?php print $tt->transaction_type_id; ?>"
							<?php print isset($the_store) && $the_store->_ajuste_negativo_id == $tt->transaction_type_id ? 'selected' : ''; ?>>
							<?php printf("%s - %s", $tt->transaction_key, $tt->transaction_name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<?php b_do_action('mb_store_form_transactions', isset($the_store) ? $the_store : null, $ttypes); ?>
		</div>
		<div class="form-group">
			<label>
				<input type="checkbox" name="meta[_allow_sales]" value="1" <?php print isset($the_store) && (int)$the_store->_allow_sales == 1 ? 'checked' : ''; ?> />
				<?php _e('Allow Sales', 'mb'); ?>
			</label>
			
		</div>
		<div class="form-group">
			<a class="btn btn-danger" href="<?php print $this->Route('index.php?mod=mb&view=stores.default')?>">
				<?php print $this->__('Cancel', 'mb'); ?>
			</a>
			<button type="submit" class="btn btn-success"><?php print $this->__('Save', 'mb')?></button>
		</div>
	</form>
</div>
