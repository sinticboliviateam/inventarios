<?php
?>
<div class="wrap">
	<h2 id="page-title">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg">
                <?php _e('Taxes', 'mb'); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg">
                <div class="text-right">
                    <a href="<?php print $this->Route('index.php?mod=mb&view=taxes.new'); ?>" class="btn btn-primary">
                        <?php _e('New', 'mb'); ?>
                    </a>
                </div>
            </div>
        </div>
    </h2>
	<div class="table-responsive">
        <table class="table">
        <thead>
        <tr>
            <th><?php _e('Num.', 'mb'); ?></th>
            <th><?php _e('Name', 'mb'); ?></th>
            <th><?php _e('Percent', 'mb'); ?></th>
            <th><?php _e('Default', 'mb'); ?></th>
            <th><?php _e('Action', 'mb'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; foreach($taxes as $tax): ?>
        <tr>
            <td class="text-center"><?php print $i; ?></td>
            <td><?php print $tax->name; ?></td>
            <td class="text-right"><?php print number_format($tax->rate, 2); ?></td>
            <td class="text-center">
                <?php if( $tax->is_default ): ?>
                <span class="glyphicon glyphicon-ok"></span>
                <?php endif; ?>
            </td>
            <td class="text-center">
                <a href="<?php print $this->Route('index.php?mod=mb&view=taxes.edit&id='.$tax->tax_id); ?>" title="<?php _e('Edit', 'mb'); ?>"
                    class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
                <a href="<?php print $this->Route('index.php?mod=mb&task=taxes.delete&id='.$tax->tax_id); ?>"
                    title="<?php _e('Delete', 'mb'); ?>"
                    class="btn btn-default btn-xs confirm"
                    data-message="<?php _e('Are you sure to delete the tax?', 'mb'); ?>">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
        </tr>
        <?php $i++; endforeach; ?>
        </tbody>
        </table>
    </div>
</div>
