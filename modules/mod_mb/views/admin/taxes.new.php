<?php
?>
<div class="wrap">
	<h2><?php print $title; ?></h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="taxes.save" />
		<?php if( isset($tax) ): ?>
		<input type="hidden" name="tax_id" value="<?php print $tax->tax_id; ?>" />
		<?php endif; ?>
		<div class="form-group">
			<label><?php _e('Code:', 'mb'); ?></label>
			<input type="text" name="code" value="<?php print isset($tax) ? $tax->code : ''; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Tax Name:', 'mb'); ?></label>
			<input type="text" name="tax_name" value="<?php print isset($tax) ? $tax->name : ''; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Percent:', 'mb'); ?></label>
			<input type="text" name="percent" value="<?php print isset($tax) ? $tax->rate : 0; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label>
				<?php _e('Mark this tax rate as default:', 'mb'); ?>
				<input type="checkbox" name="is_default" value="1" <?php print isset($tax) && $tax->is_default ? 'checked' : ''; ?> />
			</label>
		</div>
		<div class="form-group">
			<a href="<?php print $this->Route('index.php?mod=mb&view=taxes.default'); ?>" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
			<button type="submit" class="btn btn-success"><?php _e('Save', 'mb'); ?></button>
		</div>
	</form>
</div>
