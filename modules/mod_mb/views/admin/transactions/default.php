<div class="wrap">
    <h2 id="page-title">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <img src="<?php print MOD_MB_URL; ?>/images/transaction-icon-48x48.png" alt="" /><?php print $title; ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"></div>
        </div>
    </h2>
    <form action="" method="get" id="form-filter" class="form-group-sm">
        <input type="hidden" name="mod" value="mb" />
        <input type="hidden" name="task" value="transactions.ajax" />
        <input type="hidden" name="action" value="getrecords" />
        <div class="row">
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label><?php _e('Store', 'mb'); ?></label>
                    <select id="store_id" name="store_id" class="form-control">
                        <option value=""><?php _e('-- store --', 'mb'); ?></option>
                        <?php foreach($stores as $store): ?>
                        <option value="<?php print $store->store_id; ?>"><?php print $store->store_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label><?php _e('Transaction', 'mb'); ?></label>
                    <select id="transaction_type_id" name="transaction_type_id" class="form-control">
                        <option value=""><?php _e('-- transaction type --', 'mb'); ?></option>
                        <?php foreach($transaction_types as $tt): ?>
                        <option value="<?php print $tt->transaction_type_id; ?>"><?php print $tt->transaction_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label><?php _e('Customer/Supplier', 'mb'); ?></label>
                    <input type="text" name="customer" class="form-control" />
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label><?php _e('From Date', 'mb'); ?></label><br>
                    <input type="text" name="from_date" value="" class="form-control datepicker" />
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label><?php _e('To Date', 'mb'); ?></label><br>
                    <input type="text" name="to_date" value="" class="form-control datepicker" />
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                <div class="form-group">
                    <label></label><br/>
                    <button type="submit" class="btn btn-primary"><?php _e('Search', 'mb'); ?></button>
                </div>
            </div>
        </div>
    </form>
    <em><?php _e('Press right click to see more transaction options', 'mb'); ?></em>
    <div class="table-responsive">
        <table class="table table-condensed table-hover">
        <thead class="table-header" style="position:sticky;top:0;">
        <tr>
            <th><?php _e('#'); ?></th>
            <th><?php _e('Store', 'mb'); ?></th>
            <th><?php _e('Code', 'mb'); ?></th>
            <th><?php _e('Number', 'mb'); ?></th>
            <th><?php _e('Recibo', 'mb'); ?></th>
            <th><?php _e('Type', 'mb'); ?></th>
            <th><?php _e('Customer', 'mb'); ?></th>
            <th><?php _e('Details', 'mb'); ?></th>
            <th><?php _e('Total', 'mb'); ?></th>
            <th><?php _e('Status', 'mb'); ?></th>
            <th><?php _e('User', 'mb'); ?></th>
            <th><?php _e('Date Time', 'mb'); ?></th>
        </tr>
        </thead>
        <tbody id="transaction-items-body" >
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">
                <h4 title="<?php _e('Total Transactions:', 'mb'); ?>">
                    Total transacciones: <span id="total-transactions" class="label label-success">0</span>
                </h4>
            </td>
            
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <h4 title="<?php _e('Total Amount:', 'mb'); ?>" class="text-right">
                    <span id="total-amount" class="label label-success">0.00</span>
                </h4>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tfoot>
        </table>
    </div>
    <div id="pagination-container"></div>
</div>
<div id="contextual-modal" class="modal" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4><?php _e('Select an option', 'mb'); ?></h4>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item"><a href="javascript:;" id="btn-view"><?php _e('View transaction', 'mb'); ?></a></li>
                    <li class="list-group-item"><a href="javascript:;" id="btn-print-voucher"><?php _e('Print voucher', 'mb'); ?></a></li>
                    
                </ul>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary" data-dismiss="modal"><?php _e('Close', 'mb'); ?></a>
            </div>
        </div>
    </div>
</div>
<div id="modal-processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Processing, please wait...', 'mb'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>
<style>
.status{}
</style>
<script>
function SB_Transactions()
{
    var page    = 1;
    var $this   = this;
    this.GetItems = function(clear, extra_params, replace_params)
    {
        var transaction_type_id = jQuery('#transaction_type_id').val();
        var customer            = jQuery('#customer').val();
        var dateFrom            = jQuery('#from_date').val();
        var dateTo              = jQuery('#to_date').val();
         
        
        var params = jQuery('#form-filter').serialize();
        if( extra_params && !replace_params )
        {
			params += extra_params;
		}
		else if( extra_params && replace_params )
			params = extra_params;
			
        if( clear )
        {
             document.getElementById('transaction-items-body').innerHTML = '';
        }
        jQuery('#modal-processing').modal('show');
        jQuery.get('index.php?' + params, function(res)
        {
            jQuery('#modal-processing').modal('hide');
            if( res.status == 'ok' )
            {
                $this.AppendItems(res.items);
                jQuery('#pagination-container').html(res.pagination)
                jQuery('#total-amount').html(res.total_amount);
                jQuery('#total-transactions').html(res.total_rows);
            }
        });
    };
    this.AppendItems = function(items)
    {
        var list = document.getElementById('transaction-items-body');
        var index = 1;
        for(var i in items)
        {
            var item = items[i]
            var status = item.status == 'COMPLETE' ? 'COMPLETADO' : item.status;
            var row             = document.createElement('tr');
            var cellNum         = document.createElement('td');
            var cellStore       = document.createElement('td');
            var cellCode        = document.createElement('td');
            var cellSequence    = document.createElement('td');
            var cellNotes	    = document.createElement('td');
            var cellType        = document.createElement('td');
            var cellCustomer    = document.createElement('td');
            var cellDetails     = document.createElement('td');
            var cellTotal       = document.createElement('td');
            var cellStatus      = document.createElement('td');
            var cellUser        = document.createElement('td');
            var cellDateTime    = document.createElement('td');
            
            row.className           = 'trow';
            row.tabIndex            = parseInt(index) + 1;
            row.dataset.transaction_id = item.order_id;
            row.dataset.entity_type            = item.entity_type;
            
            cellNum.className       = 'col';
            cellStore.className     = 'col';
            cellCode.className      = 'col';
            cellSequence.className	= 'col';
            cellNotes.className		= 'col col-notes';
            cellType.className      = 'col';
            cellCustomer.className  = 'col';
            cellDetails.className   = 'col';
            cellTotal.className     = 'col col-total';
            cellStatus.className    = 'col';
            cellUser.className      = 'col';
            cellDateTime.className  = 'col';
            //##set values
            cellNum.innerHTML       = parseInt(index);
            cellStore.innerHTML     = item.store_name;
            cellCode.innerHTML      = item.code;
            cellSequence.innerHTML	= item.sequence;
            cellNotes.innerHTML		= '<span class="label label-primary">' + item.details + '</span>';
            cellType.innerHTML      = item.transaction_name;
            cellCustomer.innerHTML  = item.first_name + ' ' + (item.last_name || '');
            cellDetails.innerHTML   = '';
            cellTotal.innerHTML     = '<div class="text-right label-large"><span class="label label-success">' + item.total + '</span></div>';
            cellStatus.innerHTML    = '<div class="">'+status+'</div>';
            
            cellUser.innerHTML      = item.username;
            cellDateTime.innerHTML  = item.order_date;
            
            row.appendChild(cellNum);
            row.appendChild(cellStore);
            row.appendChild(cellCode);
            row.appendChild(cellSequence);
            row.appendChild(cellNotes);
            row.appendChild(cellType);
            row.appendChild(cellCustomer);
            row.appendChild(cellDetails);
            row.appendChild(cellTotal);
            row.appendChild(cellStatus);
            row.appendChild(cellUser);
            row.appendChild(cellDateTime);
            list.appendChild(row);
            index++;
        }        
    };
}
var transactions = null;
jQuery(function()
{
    transactions = new SB_Transactions();
    transactions.GetItems();
    /*
    var aux_header = jQuery('.float-table .table-header').clone();
    aux_header.css({postion: 'absolute', top: '10%'});
    jQuery('.float-table-wrap').append(aux_header);
    */
    /*
    jQuery('.float-table-wrap').scroll(function(e)
    {
        //console.log(e);
        var translate = "translate(0,"+this.scrollTop+"px)";
        jQuery(this).find('.table-header').get(0).style.transform = translate;
    });
    */
    jQuery('#form-filter').submit(function()
    {
        transactions.GetItems(true);
        return false;
    });
    jQuery('#store_id, #transaction_type_id').change(function(e)
    {
        jQuery('#form-filter').submit();
        //transactions.GetItems(true);
        /*
        if( !this.value || parseInt(this.value) <= 0 )
        {
            transactions.GetItems(true);
            return true;
        }
        */
    });
    //##right click contextual menu
    jQuery(document).on('contextmenu', '.trow', function(e) 
    {
        e.preventDefault();
        if( !this.dataset.transaction_id )
        {
            console.log('Invalid transaction identifier');
            return false;
        }
        
        jQuery('#contextual-modal').modal('show');
        var $link = this.dataset.entity_type == 'order' ? 
                    'index.php?mod=mb&task=pos.print&type=ticket&id=' + this.dataset.transaction_id : 
                    'index.php?mod=mb&task=purchases.print&id=' + this.dataset.transaction_id
        var $viewLink = 'index.php?mod=mb';
        if( this.dataset.entity_type == 'order' )
        {
            $viewLink += '&view=orders.view&id=' + this.dataset.transaction_id;
        }
        else if( this.dataset.entity_type == 'purchase_order' )
        {
            $viewLink += '&view=purchases.edit&id=' + this.dataset.transaction_id;
        }
        jQuery('#btn-print-voucher').attr('onclick', 
                                        'window.open(\''+$link+'\', \'_blank\', \'toolbar=no,location=no,resizable=no,width=400,height=500\')');
        jQuery('#btn-view').attr('href', $viewLink)
    });
    jQuery(document).on('click', '#pagination li a', function()
    {
		
		var params = this.href.split('?')[1];
		transactions.GetItems(true, params, true);
		return false;
	});
});
</script>
<style>
.col span.label{font-size:14px;min-width:90px;display:inline-block;}
.col-total span{text-align:right;}
</style>
