<?php
$revert_transfer = $user->can('mb_revert_transfer');
?>
<div class="wrap">
	<h1>
		<?php _e('Transfers Record', 'mb'); ?>
		<?php if( $user->can('mb_create_transfer') ): ?>
		<span class="pull-right">
			<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.new'); ?>" class="btn btn-primary">
				<?php _e('New Transfer', 'mb'); ?>
			</a>
		</span>
		<?php endif; ?>
	</h1>
	<div class="table-responsive">
		<table class="table table-condensed table-hover">
		<thead>
		<tr>
			<th><?php _e('No.', 'mb'); ?></th>
			<th><?php _e('ID', 'mb'); ?></th>
			<th><?php _e('From Store', 'mb'); ?></th>
			<th><?php _e('To Store', 'mb'); ?></th>
			<th><?php _e('From Warehouse', 'mb'); ?></th>
			<th><?php _e('To Warehouse', 'mb'); ?></th>
			<th><?php _e('Date', 'mb'); ?></th>
			<th><?php _e('Status', 'mb'); ?></th>
			<th><?php _e('Actions', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($this->transfers as $t): ?>
		<tr>
			<td><?php print $i; ?></td>
			<td><?php print $t->id; ?></td>
			<td><?php print $t->from_store_name; ?></td>
			<td><?php print $t->to_store_name; ?></td>
			<td><?php print $t->from_warehouse; ?></td>
			<td><?php print $t->to_warehouse; ?></td>
			<td><?php print sb_format_datetime($t->creation_date); ?></td>
			<td class="text-center">
				<?php
				$class = 'unknow';
				if( $t->status == 'complete' ) 
				{
					$class = 'success';
				}
				if( $t->status == 'on_the_way' )
				{
					$class = 'warning';
				}
				if( $t->status == 'cancelled' || $t->status == 'reverted' )
				{
					$class = 'danger';
				}
				
				?>
				<span class="label label-<?php print $class; ?>">
					<?php print isset($this->statuses[$t->status]) ? $this->statuses[$t->status] : __('Unknow', 'mb') ?>
				</span>
			</td>
			<td>
				<?php if( $revert_transfer ): ?>
				<a href="<?php print $this->Route('index.php?mod=mb&task=transfers.revert&id='.$t->id); ?>" 
					class="btn btn-default btn-xs confirm" title="<?php _e('Revert', 'mb'); ?>"
					data-message="<?php _e('Are you sure to revert the transfer?', 'mb'); ?>">
					<span class="glyphicon glyphicon-retweet"></span>
				</a>
				<?php endif; ?>
				<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.view&id='.$t->id); ?>" 
					class="btn btn-default btn-xs" title="<?php _e('View', 'mb'); ?>">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>
				<a href="<?php print $this->Route('index.php?mod=mb&task=transfers.delete&id='.$t->id); ?>" 
					class="btn btn-default btn-xs confirm" title="<?php _e('Delete', 'mb'); ?>"
                    data-message="<?php _e('Are you sure to delete the transfer? The items will not be reverted.'); ?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
			</td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		</table>
	</div>
</div>