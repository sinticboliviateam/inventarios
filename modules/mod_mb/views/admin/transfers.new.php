<?php
?>
<script src="<?php print MOD_MB_URL; ?>/js/transfer.new.js"></script>
<div class="wrap">
	<form action="" method="post" id="form-transfer" >
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="transfers.save" />
		<?php if( isset($transfer) ): ?>
		<input type="hidden" name="transfer_id" value="<?php print $transfer->id; ?>" />
		<?php endif; ?>
		<h2>
			<?php print $this->title; ?>
			<span class="pull-right">
				<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.default'); ?>" class="btn btn-danger">
					<span class="glyphicon glyphicon-remove"></span> <?php _e('Cancel', 'mb'); ?>
				</a>
				<?php if( isset($transfer) ): ?>
				<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.print&id='.$purchase->order_id); ?>" class="btn btn-warning" 
					target="_blank">
					<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'mb'); ?>
				</a>
				<?php endif; ?>
				<button type="submit" class="btn btn-success">
					<span class="glyphicon glyphicon-save"></span> <?php _e('Save', 'mb'); ?>
				</button>
			</span>
		</h2>
		<fieldset>
			<legend><?php _e('General Details', 'mb'); ?></legend>
			<div class="row">
				<div class="col-md-7">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label><?php _e('From Store', 'mb'); ?></label>
								<select id="from_store_id" name="from_store_id" class="form-control store-dropdown" data-target_dropdown="from_warehouse_id">
									<option value="-1"><?php _e('-- store --', 'quotes'); ?></option>
									<?php foreach($this->stores as $s): ?>
									<option value="<?php print $s->store_id; ?>" <?php print isset($purchase) && $purchase->store_id == $s->store_id ? 'selected' : ''; ?>>
										<?php print $s->store_name; ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label><?php _e('From Warehouse', 'mb'); ?></label>
								<select id="from_warehouse_id" name="from_warehouse_id" class="form-control">
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label><?php _e('To Store', 'mb'); ?></label>
								<select id="to_store_id" name="to_store_id" class="form-control store-dropdown" data-target_dropdown="to_warehouse_id">
									<option value="-1"><?php _e('-- store --', 'quotes'); ?></option>
									<?php foreach($this->stores as $s): ?>
									<option value="<?php print $s->store_id; ?>" <?php print isset($purchase) && $purchase->store_id == $s->store_id ? 'selected' : ''; ?>>
										<?php print $s->store_name; ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label><?php _e('To Warehouse', 'mb'); ?></label>
								<select id="to_warehouse_id" name="to_warehouse_id" class="form-control">
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<!-- 
					<table id="table-totals" class="totals-table" style="width:100%;">
					<tr>
						<th class="text-right"><?php _e('Sub Total:', 'mb'); ?></th>
						<td class="text-right"><span id="quote-subtotal"><?php print isset($purchase) ? $purchase->subtotal : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Discount:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-discount"><?php print isset($purchase) ? $purchase->discount : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Tax:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-tax"><?php print isset($purchase) ? $purchase->total_tax : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><b><?php _e('Advance:', 'quotes'); ?></b></th>
						<td class="text-right"><b><span id="invoice-total">0.00</span></b></td>
					</tr>
					<tr id="row-total">
						<th class="text-right"><?php _e('Grand Total:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-total"><?php print isset($purchase) ? $purchase->total : 0.00; ?></span></td>
					</tr>
					</table>
					 -->
				</div>
			</div>
		</fieldset><br/>
		<div class="form-group">
			<div class="input-group">
				<input type="text" id="search_product" name="search_product" value="" placeholder="<?php _e('Search product', 'invoice'); ?>" class="form-control" />
				<span class="input-group-btn">
		        	<button id="btn-add-item" class="btn btn-default" type="button"><?php _e('Add item', 'quotes'); ?></button>
		      	</span>
			</div>
		</div>
		<div class="table-responsive">
			<table id="purchase-table" class="table table-condensed table-striped">
			<thead>
			<tr>
				<th class="text-center"><?php _e('No.', 'quotes'); ?></th>
				<th class="text-center"><?php _e('Code', 'quotes'); ?></th>
				<th class="column-product"><?php _e('Product', 'quotes'); ?></th>
				<th class="column-qty"><?php _e('Quantity', 'quotes'); ?></th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php if( isset($purchase) ): $i = 1; foreach($purchase->GetItems() as $item): ?>
			<tr>
				<td><?php print $i; ?></td>
				<td><input type="text" name="items[<?php print $i - 1; ?>][code]" value="<?php print $item->product_code; ?>" class="form-control" /></td>
				<td class="column-product"><input type="text" name="items[<?php print $i - 1; ?>][name]" value="<?php print $item->name ?>" class="form-control" /></td>
				<td class="text-center">
					<input type="number" min="1" name="items[<?php print $i - 1; ?>][qty]" value="<?php print $item->quantity ?>" class="form-control item-qty" />
				</td>
				<?php /*
				<td class="column-price"><input type="text" name="items[<?php print $i - 1; ?>][price]" value="<?php print $item->supply_price ?>" class="form-control item-price" /></td>
				<td><?php print $item->total ?></td>
				*/?>
				<td>
					<a href="javascript:;" class="remove-item btn btn-default" title="<?php _e('Delete', 'mb'); ?>">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			<?php $i++; endforeach; endif; ?>
			</tbody>
			</table><!-- end id="quote-table" -->
		</div>
		
		<hr class="ht--"/>
		<div class="form-group">
			<label><?php _e('Notes', 'mb'); ?></label>
			<textarea name="notes" class="form-control"><?php print isset($purchase) ? $purchase->details : ''; ?></textarea>
		</div>
		<style>
		.inv-item-remove{width:5%;text-align:center;}
		.inv-item-remove img{width:25px;}
		.inv-item-number{width:7.5%;}
		.inv-item-name{width:50%;}
		.inv-item-qty{width:10%;}
		.inv-item-price{width:12.5%;}
		.inv-item-tax{width:12.5%;}
		.inv-item-total{width:15%;}
		.cool-table{background:#fff;}
		.cool-table .body{max-height:250px;}
		.cool-table .inv-item-number, .cool-table .inv-item-qty{text-align:center;}
		.cool-table .inv-item-qty input{text-align:center;}
		.cool-table .inv-item-price input, .cool-table .inv-item-tax input{text-align:right;}
		.cool-table .inv-item-total{text-align:right;}
		.table .column-product{width:45%;}
		.table .column-price input{text-align:right;}
		.sb-suggestions{max-height:200px;width:100%;}
		.sb-suggestions .the_suggestion{padding:5px;display:block;}
		.sb-suggestions .the_suggestion:focus,
		.sb-suggestions .the_suggestion:hover{background:#ececec;text-decoration:none;}, 
		</style>
	</form>
</div>
<!-- Modal -->
<div class="modal fade" id="search-provider-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-dialog-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title" id="myModalLabel"><?php _e('Search Provider', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe src="<?php print $this->Route('index.php?mod=mb&view=providers_list&tpl_file=module'); ?>" style="width:100%;height:300px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'customers'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<div class="modal fade" id="create-provider-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title"><?php _e('Create Provider', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe src="<?php print $this->Route('index.php?mod=provider&view=new_prov&tpl_file=module'); ?>" style="width:100%;height:300px;" 
	      				frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>