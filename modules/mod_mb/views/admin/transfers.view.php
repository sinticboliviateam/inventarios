<?php
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;

?>
<div class="wrap">
	<h2>
		<?php _e('Transfer Details', 'mb'); ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.default'); ?>" class="btn btn-danger pull-right">
			<?php _e('Back', 'mb'); ?>
		</a>
	</h2>
	<div>
		<?php if( $transfer->status != 'complete' ): ?>
		<a href="<?php print $this->Route('index.php?mod=mb&task=transfers.accept&id='.$transfer->id); ?>" class="btn btn-success btn-sm">
			<span class="glyphicon glyphicon-cog"></span> <?php _e('Accept Transfer', 'mb'); ?>
		</a>
		<a href="<?php print $this->Route('index.php?mod=mb&task=transfers.deny&id='.$transfer->id); ?>" class="btn btn-warning btn-sm">
			<span class="glyphicon glyphicon-remove"></span> <?php _e('Deny Transfer', 'mb'); ?>
		</a>
		<?php endif; ?>
		<?php if( $user->IsRoot() ): ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.printoutput&id='.$transfer->id); ?>" target="_blank" class="btn btn-warning btn-sm">
			<span class="glyphicon glyphicon-print"></span>
			<?php _e('Print Output Note', 'mb'); ?>
		</a>
		<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.printinput&id='.$transfer->id); ?>" target="_blank" class="btn btn-warning btn-sm">
			<span class="glyphicon glyphicon-print"></span>
			<?php _e('Print Input Note', 'mb'); ?>
		</a>
		<?php elseif( $transfer->status == 'on_the_way' || $transfer->status == 'complete' ): ?>
			<?php if($user->_store_id == $transfer->from_store ): ?>
			<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.printoutput&id='.$transfer->id); ?>" target="_blank" class="btn btn-warning btn-sm">
				<span class="glyphicon glyphicon-print"></span>
				<?php _e('Print Output Note', 'mb'); ?>
			</a>
			<?php endif; ?>
			<?php if($user->_store_id == $transfer->to_store ): ?>
			<a href="<?php print $this->Route('index.php?mod=mb&view=transfers.printinput&id='.$transfer->id); ?>" target="_blank" class="btn btn-warning btn-sm">
				<span class="glyphicon glyphicon-print"></span>
				<?php _e('Print Input Note', 'mb'); ?>
			</a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<table class="table table-condensed">
	<tr>
		<td><b><?php _e('Source Store: ', 'mb'); ?></b></td><td><?php print $transfer->source_store->store_name; ?></td>
		<td><b><?php _e('Source Warehouse: ', 'mb'); ?></b></td><td><?php print $transfer->source_warehouse->name; ?></td>
		<td><b><?php _e('Transfer Date: ', 'mb'); ?></b></td><td><?php print sb_format_datetime($transfer->creation_date); ?></td>
		<td><b><?php _e('Source User: ', 'mb'); ?></b></td><td><?php print (new SB_User($transfer->user_id))->username; ?></td>
	</tr>
	<tr>
		<td><b><?php _e('Destination Store: ', 'mb'); ?></b></td><td><?php print $transfer->target_store->store_name; ?></td>
		<td><b><?php _e('Destination Warehouse: ', 'mb'); ?></b></td><td><?php print $transfer->target_warehouse->name; ?></td>
		<td><b><?php _e('Reception Date: ', 'mb'); ?></b></td><td><?php print sb_format_datetime($transfer->reception_date); ?></td>
		<td><b><?php _e('Reception User: ', 'mb'); ?></b></td><td><?php print (new SB_User($transfer->receiver_id))->username; ?></td>
	</tr>
	</table>
	<h3><?php _e('Items'); ?></h3>
	<table class="table table-condensed table-bordered">
	<thead>
	<tr>
		<th><?php _e('Num.', 'mb'); ?></th>
		<th><?php _e('Code', 'mb'); ?></th>
		<th><?php _e('Product', 'mb'); ?></th>
		<?php if( function_exists('mb_ceass_obtener_lote') ): ?>
		<th><?php _e('Batch', 'mb'); ?></th>
		<th><?php _e('Expiration Date', 'mb'); ?></th>
		<?php endif; ?>
		<th><?php _e('Quantity', 'mb'); ?></th>
		<th><?php _e('Price', 'mb'); ?></th>
		<th><?php _e('Total', 'mb'); ?></th>
		<th><?php _e('Status', 'mb'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($transfer->GetItems() as $item): ?>
	<?php
	$batch = function_exists('mb_ceass_obtener_lote') ? mb_ceass_obtener_lote($item->product_id, $item->batch) : '';
	?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $item->product_code; ?></td>
		<td><?php print $item->product_name; ?></td>
		<?php if( function_exists('mb_ceass_obtener_lote') ): ?>
		<td><?php print $item->batch; ?></td>
		<td><?php print $batch ? sb_format_date($batch->expiration_date) : ''; ?></td>
		<?php endif; ?>
		<td><?php print $item->quantity; ?></td>
		<td><?php print number_format($item->price, 2, '.', ','); ?></td>
		<td><?php print number_format($item->price * $item->quantity, 2, '.', ','); ?></td>
		<td>
			<span class="label <?php print $item->item_status == 'complete' ? 'label-success' : 'label-danger'; ?>"><?php print $item->item_status; ?></span>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	</table>
</div>