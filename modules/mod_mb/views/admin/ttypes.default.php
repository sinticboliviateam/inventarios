<?php
?>
<div class="wrap">
	<h2>
		<?php _e('Transaction Types', 'mb'); ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=ttypes.new'); ?>" class="btn btn-primary pull-right">
			<?php _e('New', 'mb'); ?>
		</a>
	</h2>
	<div class="table-responsive">
		<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php _e('No', 'mb'); ?></th>
			<th><?php _e('ID', 'mb'); ?></th>
			<th><?php _e('Code', 'mb'); ?></th>
			<th><?php _e('Transaction', 'mb'); ?></th>
			<th><?php _e('Type', 'mb'); ?></th>
			<th><?php _e('Action', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1; foreach($this->items as $item): ?>
		<tr>
			<td class="text-center"><?php print $i; ?></td>
			<td class="text-center"><?php print $item->transaction_type_id; ?></td>
			<td class="text-center"><?php print $item->transaction_key; ?></td>
			<td><?php print $item->transaction_name; ?></td>
			<td class="text-center">
				<?php
				$class = $item->in_out == 'in' ? 'success' : 'danger';  
				?>
				<span class="label label-<?php print $class; ?>"><?php _e($item->in_out, 'mb'); ?></span>
			</td>
			<td>
				<a href="<?php print $this->Route('index.php?mod=mb&view=ttypes.edit&id='.$item->transaction_type_id); ?>" 
					title="<?php _e('Edit', 'mb'); ?>"
					class="btn btn-default btn-xs">
					<span class="glyphicon glyphicon-edit"></span>
				</a>
				<a href="<?php print $this->Route('index.php?mod=mb&task=ttypes.delete&id='.$item->transaction_type_id); ?>" 
					title="<?php _e('Delete', 'mb'); ?>"
					class="btn btn-default btn-xs confirm"
					data-message="<?php _e('Are you sure to delete the transaction type?'); ?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
			</td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
		</table>
	</div>
</div>