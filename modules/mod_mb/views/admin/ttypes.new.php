<?php
?>
<div class="wrap">
	<h2><?php print $this->title; ?></h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="ttypes.save" />
		<?php if( $this->tt ): ?>
		<input type="hidden" name="id" value="<?php print $this->tt->transaction_type_id; ?>" />
		<?php endif; ?>
		<div class="form-group">
			<label><?php _e('Code', 'mb'); ?></label>
			<input type="text" name="transaction_key" value="<?php print $this->tt ? $this->tt->transaction_key : '' ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Name', 'mb'); ?></label>
			<input type="text" name="name" value="<?php print $this->tt ? $this->tt->transaction_name : '' ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Type', 'mb'); ?></label>
			<select name="in_out" class="form-control">
				<option value="in" <?php print $this->tt && $this->tt->in_out == 'in' ? 'selected' : ''; ?>><?php _e('Input', 'mb'); ?></option>
				<option value="out" <?php print $this->tt && $this->tt->in_out == 'out' ? 'selected' : ''; ?>><?php _e('Output', 'mb'); ?></option>
				<option value="t_in" <?php print $this->tt && $this->tt->in_out == 't_in' ? 'selected' : ''; ?>><?php _e('Transfer Input', 'mb'); ?></option>
				<option value="t_out" <?php print $this->tt && $this->tt->in_out == 't_out' ? 'selected' : ''; ?>><?php _e('Transfer Output', 'mb'); ?></option>
			</select>
		</div>
		<div class="form-group">
			<a href="<?php print $this->Route('index.php?mod=mb&view=ttypes.default'); ?>" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
			<button type="submit" class="btn btn-success"><?php _e('Save', 'mb'); ?></button>
		</div>
	</form>
</div>