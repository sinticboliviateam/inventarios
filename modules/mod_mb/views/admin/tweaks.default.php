<?php
$store_id 	= $this->request->getInt('store_id');
$tipo 		= $this->request->getString('tipo_ajuste');

?>
<div class="wrap">
	<h2>
		<?php _e('Ajustes de Inventario', 'mb'); ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=tweaks.new')?>" class="pull-right btn btn-primary"><?php _e('Nuevo', 'ceass'); ?></a>
	</h2>
	<form action="" method="get" class="form-group-sm">
		<input type="hidden" name="mod" value="ceass" />
		<input type="hidden" name="view" value="ajustes.default" />
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Store', 'mb'); ?></label>
					<select name="store_id" class="form-control">
						<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
						<?php foreach($stores as $store): ?>
						<option value="<?php print $store->store_id; ?>" <?php print $store->store_id == $store_id ? 'selected' : ''; ?>>
							<?php print $store->store_name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Tipo de Ajuste', 'mb'); ?></label>
					<select name="tipo_ajuste" class="form-control">
						<option value="-1"><?php _e('-- tipo ajuste --', 'mb'); ?></option>
						<?php foreach($tipos as $t): ?>
						<option value="<?php print $t->key; ?>" <?php print $tipo == $t->key ? 'selected' : ''; ?>>
							<?php print $t->nombre; ?>
						</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><?php _e('Desde Fecha', 'mb'); ?></label>
					<input type="text" name="desde" value="<?php print $this->request->getString('desde', sb_format_date(time())); ?>" class="form-control datepicker" />
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><?php _e('Hasta Fecha', 'mb'); ?></label>
					<input type="text" name="hasta" value="<?php print $this->request->getString('hasta', sb_format_date(time())); ?>" class="form-control datepicker" />
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>&nbsp;</label><br/>
					<button type="submit" class="btn btn-default btn-sm">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</div>
			</div>
		</div>
	</form>
	<table class="table table-condensed table-hover">
	<thead>
	<tr>
		<th>Nro.</th>
		<th>Regional</th>
		<th>Tipo</th>
		<th>Fecha</th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php if( isset($items) ): $i = 1; foreach($items as $item): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print $item->store_name ?></td>
		<td><span class="label label-<?php print $item->tipo == 'positivo' ? 'success' : 'danger'; ?>"><?php print $item->tipo; ?></span></td>
		<td><?php print sb_format_datetime($item->transaction_date); ?></td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=ceass&view=ajustes.ver&id='.$item->id) ?>" class="btn btn-default btn-sm" 
				title="<?php _e('Ver Ajuste', 'ceass'); ?>">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=ceass&view=ajustes.edit&id='.$item->id) ?>" class="btn btn-default btn-sm" 
				title="<?php _e('Editar', 'ceass'); ?>">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=ceass&view=ajustes.delete&id='.$item->id) ?>" class="btn btn-default btn-sm confirm" 
				title="<?php _e('Editar', 'ceass'); ?>" data-message="<?php _e('Esta seguro de borrar el ajuste de inventario?', 'ceass'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; endif; ?>
	</tbody>
	</table>
	<?php lt_pagination($this->Route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
</div>