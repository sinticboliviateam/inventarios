<?php
?>
<script>
var search_product_url = '<?php print $this->Route('index.php?mod=mb&view=products_search&tpl_file=module&store_id='); ?>';
var causas = <?php print json_encode($causas); ?>; 
</script>
<script>
var ajuste = 
{
	AdicionarProducto: function(item)
	{
		var tpl = '<tr data-id="{id}" data-costo={costo} data-lote="{lote}">\
			<td><span class="product_nro">{nro}</span><input type="hidden" name="product[{index}][id]" value="{id}" class="product_id" /></td>\
			<td><span class="product_codigo">{codigo}</span><input type="hidden" name="product[{index}][codigo]" value="{codigo}" class="product_codigo" /></td>\
			<td><span class="product_nombre">{nombre}</span><input type="hidden" name="product[{index}][nombre]" value="{nombre}" class="product_nombre" /></td>\
			<td><input type="number" min="1" name="product[{index}][qty]" value="{qty}" class="product_qty form-control" /></td>\
			<td><span class="product_costo">{costo}</span><input type="hidden" name="product[{index}][costo]" value="{costo}" class="product_costo" /></td>\
			<td><span class="product_total">{total}</span><input type="hidden" name="product[{index}][total]" value="{total}" class="product_total" /></td>\
			<td><a href="javascript:;" class="btn-delete btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>\
		</tr>';
		var index 	= jQuery('#table-items tbody tr').length;
		var exists 	= ajuste.ProductExists(item.product_id);
		var qty = 1;
		var row = tpl.replace(/{nro}/g, 1)
						.replace(/{index}/g, index)
						.replace(/{id}/g, item.product_id)
						.replace(/{codigo}/g, item.product_code)
						.replace(/{nombre}/g, item.product_name)
						.replace(/{qty}/g, item.use_qty ? item.use_qty : 1)
						.replace(/{costo}/g, item.product_cost)
						.replace(/{total}/g, parseFloat(item.product_cost).toFixed(2));
		if( !exists )
		{
			jQuery('#table-items tbody').append(row);
		}
		else
		{
			var current_qty = parseInt(jQuery(exists).find('.product_qty').val());
			var qty = item.use_qty ? parseInt(item.use_qty) : 1;
			jQuery(exists).find('.product_qty').val(current_qty + qty);
			ajuste.CalculateRowTotal(exists);
		}
		ajuste.CalculateTotals();
	},
	ProductExists: function(id)
	{
		var exists = false;
		jQuery('#table-items tbody tr').each(function(i, row)
		{
			if( row.dataset.id == id )
			{
				exists = row;
				return false;
			}
		});
		return exists;
	},
	CalculateRowTotal: function(row)
	{
		var current_qty = parseInt(jQuery(row).find('.product_qty').val());
		var cost		= parseFloat(jQuery(row).find('input.product_costo').val());
		var total		= current_qty * cost;
		jQuery(row).find('.product_total').html(total.toFixed(2)).val(total.toFixed(2));
		
	},
	CalculateTotals: function()
	{
		var total = 0;
		jQuery('#table-items tbody tr').each(function(i, row)
		{
			var row_total = parseFloat(jQuery(row).find('input.product_total').val());
			total += row_total;
		});
		jQuery('#donacion-total').html(total.toFixed(2));
	},
	QuitarItem: function(e)
	{
		var tr 				= jQuery(this.parentNode.parentNode);
		var current_index 	= tr.index();
		var new_index 		= current_index;
		tr.nextAll().each(function(i, ntr)
		{
			var inputs = jQuery(ntr).find('input');
			inputs.each(function(ii, input)
			{
				if(input.name.indexOf('product') != -1)
				{
					input.name = input.name.replace(/\[\d+\]/, '['+new_index+']')
				}
			});
			new_index++;
		});
		tr.remove();
		ajuste.CalculateTotals();
		return false;
	},
	Registrar: function()
	{
		if( this.store_id.value <= 0 || isNaN(this.store_id.value) )
		{
			alert('Debe seleccionar una regional');
			return false;
		}
		if( parseInt(this.tipo_ajuste.value) <= 0 || this.tipo_ajuste.value.trim().length <= 0 )
		{
			alert('Debe seleccionar un tipo de ajuste');
			return false;
		}
		if( this.causa.value == '-1' || this.causa.value.trim().length <= 0 )
		{
			alert('Debe seleccionar una causa para el ajuste');
			return false;
		}
		if( jQuery('#table-items tbody tr').length <= 0 )
		{
			alert('Debe ingresar almenos un item para el ajuste de inventario');
			return false;
		}
		return true;
	},
	LlenarCausas: function()
	{
		var tipo = this.value;
		var sign = tipo == 'positivo' ? '+' : '-';
		var causa = jQuery('#causa');
		causa.html('<option value="-1">-- causa --</option>');
		jQuery.each(causas, function(i, item)
		{
			if( item.tipo == sign )
			{
				causa.append('<option value="'+item.key+'">'+item.nombre+'</option>');
			}
		});
	},
	OnProductSelected: function(e, product)
	{
		console.log(product);
		ajuste.AdicionarProducto(product);
	},
	SetEvents: function()
	{
		jQuery(document).on('change keydown keyup', '.product_qty', function(e)
		{
			var row 	= jQuery(this).parents('tr:first').get(0);
			var qty 	= isNaN(parseInt(this.value)) ? 1 : parseInt(this.value);
			var costo 	= parseFloat(row.dataset.costo);
			var total 	= qty * costo;
			jQuery(row).find('.product_total').html(total).val(total);
			if( e.type == 'keydown' && e.keyCode == 13 )
			{
				return false;
			}
		});
		jQuery(document).on('click', '.btn-delete', ajuste.QuitarItem);
		jQuery('#tipo_ajuste').change(ajuste.LlenarCausas);
		jQuery('#form-ajuste').submit(ajuste.Registrar);
		jQuery(document).on('product_selected', ajuste.OnProductSelected);
	}
};

jQuery(function()
{
	jQuery('#buscar_producto').click(function(e)
	{
		var store_id = jQuery('#store_id').val();
		var tipo = jQuery('#tipo_ajuste').val();
		if( store_id <= 0 )
		{
			alert('Debe seleccionar una regional');
			return false;
		}
		
		if( tipo == '-1' || tipo.trim().length <= 0 )
		{
			alert('Debe seleccionar un tipo de ajuste');
			return false;
		}
		var src = jQuery('#modal-search iframe').get(0).dataset.src;
		src += tipo == 'positivo' ? 'in_out=input&' : 'in_out=output&';
		jQuery('#modal-search iframe').prop('src', src + 'store_id='+store_id);
		jQuery('#modal-search').modal('show');
		return false;
	});
	ajuste.SetEvents();
});
</script>
<div class="wrap">
	<h2>
		<?php print $title; ?>
		<a href="javascript:;" onclick="jQuery('#form-ajuste').submit();" class="btn btn-success pull-right">Guardar</a>
	</h2>
	<form id="form-ajuste" action="" method="post" class="form-group-sm">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="tweaks.register" />
		<input type="hidden" id="customer_id" name="customer_id" value="<?php print isset($donacion) ? $donacion->customer_id : 0; ?>"  />
		<?php if( isset($donacion) ): ?>
		<input type="hidden" name="id" value="<?php print $donacion->id; ?>" />
		<?php endif; ?>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label><?php _e('Store', 'mb'); ?></label>
					<select id="store_id" name="store_id" class="form-control">
						<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
						<?php foreach($stores as $s): ?>
						<option value="<?php print $s->store_id; ?>" data-tt_id="<?php print $s->_donacion_output_tt_id; ?>">
							<?php print $s->store_name; ?>
						</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><?php _e('Tipo de Ajuste', 'mb'); ?></label>
					<select id="tipo_ajuste" name="tipo_ajuste" class="form-control">
						<option value="-1"><?php _e('-- tipo ajuste --', 'ceass'); ?></option>
						<?php foreach($tipos as $t): ?>
						<option value="<?php print $t->key; ?>"><?php print $t->nombre; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label><?php _e('Causa del Ajuste', 'ceass'); ?></label>
					<select id="causa" name="causa" class="form-control">
						<option value="-1"><?php _e('-- causa --', 'ceass'); ?></option>
						<?php foreach($causas as $c): ?>
						<option value="<?php print $c->key; ?>"><?php print $c->nombre; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-5">
				<table id="table-totals" class="totals-table" style="width:100%;">
				<tbody>
				<tr>
					<th class="text-right">Sub Total:</th>
					<td class="text-right"><span id="donacion-subtotal">0</span></td>
				</tr>
				<tr id="row-total">
					<th class="text-right">Total:</th>
					<td class="text-right"><span id="donacion-total">0</span></td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>
		<p>
			<a href="javascript:;" id="buscar_producto" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> Adicionar Producto</a>
		</p>
		<table id="table-items" class="table table-condensed table-hover table-bordered">
		<thead>
		<tr>
			<th>Nro.</th>
			<th>Codigo</th>
			<th>Producto</th>
			<th>Cantidad</th>
			<th>Costo</th>
			<th>Total</th>
			<th>&nbsp;</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
		<div class="form-group">
			<label>Notas</label>
			<textarea name="details" class="form-control"><?php print isset($donacion) ? $donacion->details : ''; ?></textarea>
		</div>
	</form>
</div>
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title"><?php _e('Buscar Producto', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe id="iframe-search" data-src="<?php print $this->Route('index.php?mod=mb&view=products_search&tpl_file=module&'); ?>" style="width:100%;height:380px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Cerrar', 'ceass'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>