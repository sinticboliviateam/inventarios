<?php
?>
<div class="wrap">
	<h2><?php _e('Product Types', 'mb'); ?></h2>
	<ul class="view-buttons">
		<li>
			<a href="<?php print $this->Route('index.php?mod=mb&view=types.new'); ?>" class="btn btn-default">
				<?php _e('New product type'); ?>
			</a>
		</li>
	</ul>
	<table class="table">
		<thead>
		<tr>
			<th>#</th>
			<th><?php _e('Type', 'mb'); ?></th>
			<th>
				<div>
				<select name="store_id">
					<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
					<?php foreach($stores as $s): ?>
					<option value="<?php print $s->store_id; ?>"><?php print $s->store_name; ?></option>
					<?php endforeach; ?>
				</select>
				</div>
				<?php //print SBText::_('Store', 'mb'); ?>
			</th>
			<th><?php _e('Actions', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php $i = 1;foreach($types as $t): ?>
		<tr>
			<td><?php print $i; ?></td>
			<td><?php print $t->type; ?></td>
			<td><?php print $t->store_name; ?></td>
			<td>
				<a href="<?php print $this->Route('index.php?mod=mb&view=types.edit&id='.$t->type_id); ?>">
					<?php _e('Editar', 'mb'); ?></a>
				|
				<a href="<?php print $this->Route('index.php?mod=mb&task=types.delete&id'.$t->type_id); ?>" class="confirm"
					data-message="<?php print $this->Route('Are you sure to delete the product type?'); ?>">
					<?php _e('Delete', 'mb'); ?></a>
			</td>
		</tr>
		<?php $i++; endforeach; ?>
		</tbody>
	</table>
</div>