<?php
?>
<div class="wrap">
	<h2><?php print $title; ?></h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="types.save" />
		<?php if( isset($type) ): ?>
		<input type="hidden" name="id" value="<?php print $type->type_id; ?>" />
		<?php endif; ?>
		<div class="control-group">
			<label><?php _e('Type', 'mb'); ?></label>
			<input type="text" name="type" value="<?php print $this->request->getString('type', isset($type) ? $type->type : ''); ?>" class="form-control" />
		</div>
		<div class="control-group">
			<label><?php _e('Code', 'mb'); ?></label>
			<input type="text" name="code" value="<?php print $this->request->getString('code', isset($type) ? $type->code : ''); ?>" class="form-control" />
		</div>
		<div class="control-group">
			<label><?php _e('Description', 'mb'); ?></label>
			<textarea name="description" class="form-control"><?php print $this->request->getString('description', isset($type) ? $type->description : ''); ?></textarea>
		</div>
		<div class="control-group">
			<label><?php _e('Store', 'mb'); ?></label>
			<select name="store_id" class="form-control">
				<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
				<?php foreach($stores as $s): ?>
				<option value="<?php print $s->store_id; ?>" <?php print (isset($type) && $type->store_id == $s->store_id) ? 'selected' : ''; ?>>
					<?php print $s->store_name; ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>
		<br/>
		<div>
			<a href="<?php print $this->Route('index.php?mod=mb&view=types.default'); ?>" class="btn btn-danger">
				<?php _e('Cancel', 'mb'); ?></a>
			<button type="submit" class="btn btn-success"><?php _e('Save', 'mb'); ?></button>
		</div>
	</form>
</div>