<?php
?>
<div class="wrap">
	<h2>
		<?php _e('Warehouse', 'mb'); ?>
		<a href="<?php print $this->Route('index.php?mod=mb&view=warehouse.new'); ?>" class="btn btn-primary pull-right">
			<?php _e('New', 'mb'); ?>
		</a>
	</h2>
	<div class="table-responsive">
		<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php _e('ID'); ?></th>
			<th><?php _e('Name', 'mb'); ?></th>
			<th><?php _e('Address', 'mb'); ?></th>
			<th><?php _e('Store', 'mb'); ?></th>
			<th><?php _e('Action', 'mb'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($items as $item): ?>
		<tr>
			<td class="text-center"><?php print $item->id; ?></td>
			<td><?php print $item->name; ?></td>
			<td><?php print $item->address; ?></td>
			<td><?php print $item->store_name; ?></td>
			<td>
				<a href="<?php print $this->Route('index.php?mod=mb&view=warehouse.edit&id='.$item->id); ?>" class="btn btn-default btn-xs">
					<span class="glyphicon glyphicon-edit"></span>
				</a>
				<a href="<?php print $this->Route('index.php?mod=mb&task=warehouse.delete&id='.$item->id); ?>" class="btn btn-default btn-xs confirm"
					data-message="<?php _e('Are you sure to delete the warehouse?', 'mb'); ?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
		</table>
	</div>
</div>