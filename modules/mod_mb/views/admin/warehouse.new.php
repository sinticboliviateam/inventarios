<?php
?>
<div class="wrap">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h2><?php _e('New Warehouse', 'mb'); ?></h2>
		</div>
		<div class="col-xs-12 col-md-6">
			<a href="<?php print $this->Route('index.php?mod=mb&view=warehouse.default'); ?>" class="btn btn-danger"><?php _e('Cancel', 'mb'); ?></a>
			<a href="javascript:;" onclick="jQuery('#wform').submit();" class="btn btn-success"><?php _e('Save', 'mb'); ?></a>
		</div>
	</div>
	<form id="wform" action="" method="post">
		<input type="hidden" name="mod" value="mb" />
		<input type="hidden" name="task" value="warehouse.save" />
		<?php if( isset($w) ): ?>
		<input type="hidden" name="id" value="<?php print $w->id; ?>" />
		<?php endif; ?>
		<?php b_do_action('mb_warehouse_before_fields', isset($w) ? $w : null); ?>
		<div class="form-group">
			<label><?php _e('Name', 'mb'); ?></label>
			<input type="text" name="name" value="<?php print isset($w) ? $w->name : ''; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Address', 'mb'); ?></label>
			<input type="text" name="address" value="<?php print isset($w) ? $w->address : ''; ?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?php _e('Store', 'mb'); ?></label>
			<select name="store_id" class="form-control">
				<option value="-1"><?php _e('-- store --', 'mb'); ?></option>
				<?php foreach($stores as $s): ?>
				<option value="<?php print $s->store_id; ?>" <?php print isset($w) && $w->store_id == $s->store_id ? 'selected' : ''; ?>>
					<?php print $s->store_name; ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>
		<?php b_do_action('mb_warehouse_after_fields', isset($w) ? $w : null); ?>
	</form>
</div>