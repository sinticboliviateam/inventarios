<?php
namespace SinticBolivia\SBFramework\Modules\Payments\Entities;
use SinticBolivia\SBFramework\Database\Classes\SB_DbEntity;

/**
 * @table mb_payments
 */
class Payment extends SB_DbEntity
{
    const STATUS_COMPLETED  = 'COMPLETED';
    const STATUS_VOID       = 'VOID';
    const STATUS_RECEIVED   = 'RECEIVED';
    /**
     * @primaryKey true
     * @var INTEGER
     */
    public  $id;
    /**
     * @var INTEGER
     */
    public  $document_id;
    /**
     * @var VARCHAR
     */
    public  $document_type;
    /**
     * @var VARCHAR
     */
    public  $document_number;
    /**
     * @var BIGINT
     */
    public  $sequence;
    /**
     * @var INTEGER
     */
    public  $store_id;
    /**
     * @var BIGINT
     */
    public  $user_id;
    /**
     * @var BIGINT
     */
    public  $customer_id;
    /**
     * @var VARCHAR
     */
    public  $customer;
    /**
     * @var BIGINT
     */
    public  $identity_document;
    /**
     * @var VARCHAR
     */
    public  $detail;
    /**
     * @var VARCHAR
     */
    public  $notes;
    /**
     * @digits 10
     * @precision 2
     * @var DECIMAL
     */
    public  $total_amount;
    /**
     * @digits 10
     * @presicion 2
     * @var DECIMAL
     */
    public  $amount;
    /**
     * @var VARCHAR
     */
    public  $source;
    /**
     * @digits 10
     * @presicion 2
     * @var DECIMAL
     */
    public  $balance;
    /**
     * @var VARHCAR
     */
    public  $payment_method;
    /**
     * @var VARCHAR
     */
    public  $payment_type;
    /**
     * @var VARCHAR
     */
    public  $check_number;
    /**
     * @var VARCHAR
     */
    public  $billing_number;
    /**
     * @var VARCHAR
     */
    public  $deposit_account_number;
    /**
     * @var VARCHAR
     */
    public  $status;
    /**
     * @var BIGINT
     */
    public  $parent;
    /**
     * @var DATETIME
     */
    public  $payment_date;
    /**
     * @var DATETIME
     */
    public  $last_modification_date;
    /**
     * @var DATETIME
     */
    public  $creation_date;
    /**
     * @var ARRAY
     */
    public      $childs         = null;
    protected   $lastPayment    = null;
    
    public function GetChilds()
    {
        if( $this->childs == null )
        {
            $query = "SELECT * FROM $this->table WHERE parent = {$this->id} ORDER BY payment_date ASC";
            $this->childs = $this->dbh->FetchResults($query, get_class($this));
        }
        return $this->childs;
    }
    public function GetLastSequence()
    {
        $sequence = $this->sequence;
        foreach($this->GetChilds() as $child)
            $sequence = $child->sequence;
        
        return $sequence;
    }
    public function GetChildSequence($id)
    {
    }
    public function GetLastPayment()
    {
        if( $this->lastPayment )
            return $this->lastPayment;
            
        if( !count($this->GetChilds()) )
        {
            $this->lastPayment = $this;
            return $this->lastPayment;
        }
        foreach($this->GetChilds() as $p)
            $this->lastPayment = $p;
        
        return $this->lastPayment;
    }
}
