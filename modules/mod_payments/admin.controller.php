<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;

class LT_AdminControllerPayments extends SB_Controller
{
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Payments\Models
	 * @var PaymentModel
	 */
	protected $paymentModel;
	
	public function task_default()
	{
		$user = sb_get_current_user();
		if( !$user->IsRoot() && !$user->_store_id )
		{
			lt_die(__('You dont have a store assigned', 'payments'));
		}
		$this->dbh->Select('*')
					->From('mb_payments')
					->Where(null);
		if( !$user->IsRoot() )
		{
			$store_id = (int)$user->_store_id;
			$this->dbh->SqlAND(array('store_id' => $store_id));
		}
		$this->dbh->OrderBy('payment_date');
		$this->dbh->Query(null);
		$items = $this->dbh->FetchResults();
		sb_set_view_var('items', $items);
		sb_set_view_var('payment_methods', mb_payments_methods());
		$this->SetVar('total_payments', $this->paymentModel->GetTotalPayments());
		$title = __('Payments Management', 'payments');
		$this->document->SetTitle($title);
	}
	public function task_new()
	{
		sb_add_script(BASEURL . '/js/sb-completion.js', 'completion-js');
		sb_add_script(MOD_PAYMENTS_URL . '/js/payments.js', 'payments-js');
		sb_add_js_global_var('payments', 'completion_customer_url', SB_Route::_('index.php?mod=customers&task=customers_list'));
		sb_add_js_global_var('payments', 'locale', array(
			'LABEL_CUSTOMER'	=> __('Customer', 'payments'),
			'LABEL_STORE'		=> __('Store', 'payments'),
			'LABEL_SALE_TYPE'	=> __('Sale Type', 'payments'),
			'LABEL_TOTAL_PAID'	=> __('Total Paid', 'payments'),
			'LABEL_TOTAL'		=> __('Total', 'payments'),
			'TOTAL_PAYMENTS'	=> __('Payments', 'payments')
		));
		sb_set_view_var('payment_methods', mb_payments_methods());
	}
	public function task_save()
	{
		if( !sb_is_user_logged_in() )
		{
			lt_die(__('You need to start a session', 'payments'));
		}
		$user = sb_get_current_user();
		$status				= SB_Request::getString('status', 'received');
		$data 	= SB_Request::getVars(array(
			'int:parent',
			'int:order_id',
			'int:customer_id',
			'customer',
			'int:identity_document',
			'payment_method',
			'document_number',
			'float:total_amount',
			'float:amount',
			'detail',
			'notes',
			'check_number',
			'billing_number',
			'int:deposit_account_number'
		));
		$payment_date   = SB_Request::getDate('payment_date');
		$order 			= new SB_MBOrder($data['order_id']);
		$cdate = $payment_date ? ($payment_date . ' ' . date('H:i:s') ) : date('Y-m-d H:i:s');
		$data['document_id']			= $data['order_id'];
		$data['document_type']			= 'order';
		$data['store_id']				= (int)$order->store_id;
		$data['sequence']				= mb_payments_get_sequence();
		$data['balance']				= $order->order_id ? ((float)$order->total - (float)$data['amount']) 
															: ((float)$data['total_amount'] - (float)$data['amount']);
		$data['payment_type']			= 'input';
		$data['user_id']				= sb_get_current_user()->user_id;
		$data['status'] 				= 'received';
		$data['payment_date']			= $cdate;
		$data['last_modification_date'] = $cdate;
		$data['creation_date'] 			= $cdate;
        if( $data['parent'] )
        {
            $mainPayment = $this->paymentModel->Get($data['parent']);
            $lastPayment = $mainPayment->GetLastPayment();
            $data['balance'] = $lastPayment->balance - $data['amount'];
            $mainPayment->balance = $data['balance'];
            if( $mainPayment->balance <= 0 )
                $mainPayment->status = Payment::STATUS_COMPLETED;
            $mainPayment->Save();
        }
		unset($data['order_id']);
		$this->dbh->Insert('mb_payments', $data);
		$total_paid = $order->order_id ? (float)mb_payments_get_total_paid($order->order_id) : 0;
		if( $total_paid >= (float)$order->total)
		{
			$this->dbh->Update('mb_orders', 
								array('payment_status' => 'complete'), 
								array('order_id' => $order->order_id)
			);
		}
		SB_MessagesStack::addMessage(__('The payment has been registered', 'payments'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=payments'));
	}
	public function task_void()
	{
		$id = $this->request->getInt('id');
		try
		{
			if( !$id )
				throw new Exception($this->__('Invalid payment identifier'));
			$this->paymentModel->UpdateStatus($id, 'void');
			SB_MessagesStack::AddSuccess($this->__('The payment has been voided'));
			sb_redirect($this->Route('index.php?mod=payments'));
		}
		catch(Exception $e)
		{
			SB_MessagesStack::AddError($e->getMessage());
			sb_redirect($this->Route('index.php?mod=payments'));
		}
	}
	public function task_search()
	{
		$user		= sb_get_current_user();
		$number 	= SB_Request::getString('number');
		$sale_type	= SB_Request::getString('sale_type');
		list($number, $year) = strstr($number, '/') ? array_map('intval', explode('/', $number)) : array($number, null);
		$res = null;
		if( $sale_type == 'invoice' )
		{
			$res = LT_MBInvoice::GetInvoiceByNumber($number, $year, $user->IsRoot() ? null : (int)$user->_store_id);
			
			for($i = 0; $i < count($res); $i++)
			{
				$subquery 	= "SELECT COUNT(*) FROM mb_payments p WHERE p.document_id = o.order_id";
				$subquery1 	= "SELECT SUM(amount) FROM mb_payments ps WHERE ps.document_id = o.order_id";
				$query = "select o.*,s.store_name, ($subquery) as total_payments, ($subquery1) as total_paid 
						from mb_orders o, mb_order_meta om, mb_stores s
						where 1 = 1
						and o.order_id = om.order_id
						and o.store_id = s.store_id
						and om.meta_key = '_invoice_id'
						and om.meta_value = '{$res[$i]->invoice_id}' ";
				//var_dump($query);
				if( !$user->IsRoot() )
				{
					$query .= "and o.store_id = {$user->_store_id} ";
				}
				$row = $this->dbh->FetchRow($query);
				if( $row )
				{
					$res[$i]->order 			= $row;
					$res[$i]->id				= $res[$i]->order->order_id;
					$res[$i]->store_name 		= $res[$i]->order->store_name;
					$res[$i]->invoice_date_time	= sb_format_date($res[$i]->invoice_date_time);
					$res[$i]->total_payments	= (int)$res[$i]->order->total_payments;
					$res[$i]->total_paid	= (float)$res[$i]->order->total_paid;
				}
				else
				{
					//$res = null;
				}
			}
			//var_dump(count($res));
		}
		elseif( $sale_type == 'order' )
		{
			$order = new SB_MBOrder();
		}
		
		if( !$res || !count($res) )
		{
			sb_response_json(array('status' => 'error', 'error' => __('The document does not exists', 'payments')));
		}
		sb_response_json(array('status' => 'ok', 'results' => $res));
	}
	public function task_get_document()
	{
		$id 		= SB_Request::getInt('id');
		$sale_type 	= SB_Request::getString('sale_type');
		
		$document = new SB_MBOrder($id);
		if( $sale_type == 'invoice' )
		{
			
		}
		if( !$document || !$document->order_id )
			sb_response_json(array('status' => 'error', 'error' => __('The document does not exists', 'payments')));
		$query = "SELECT * FROM mb_payments 
					WHERE document_id = $document->order_id 
					AND document_type = 'order'
					ORDER BY payment_date DESC";
		$document->total		= $document->total;
		$document->payments 	= $this->dbh->FetchResults($query);
		$document->total_paid	= $this->dbh->GetVar("SELECT SUM(amount) FROM mb_payments WHERE document_id = {$document->order_id} AND document_type = 'order'");
		$document->balance		= (float)$document->total - (float)$document->total_paid;
		sb_response_json(array('status' => 'ok', 'order' => $document));
	}
	public function task_print()
	{
		$id 	= SB_Request::getInt('id');
		$pdf	= SB_Request::getInt('pdf');
		$user	= sb_get_current_user();
		
		if( !$id )
		{
			lt_die(__('The payment identifier is invalid', 'payments'));
		}
        
		$payment = $this->paymentModel->GetMain($id);
        if( !$payment )
            throw new Exception("The payment does not exists");
        
		$tpl = SB_Module::do_action('payments_receipt_input', MOD_PAYMENTS_DIR . SB_DS . 'templates' . SB_DS . 'input.php');
		if( !$tpl || !file_exists($tpl) )
		{
			lt_die(__('The payment template does not exists', 'payments'));
		}
		
		$settings 	= sb_get_parameter('mb_settings');
		$payment_methods = mb_payments_methods();
		$logo 		= isset($settings->business_logo) ? UPLOADS_DIR . SB_DS . $settings->business_logo : null;
		$logo_url = null;
		if( file_exists($logo) )
			$logo_url = UPLOADS_URL . '/' . $settings->business_logo;
		if( $pdf )
		{
			$pdf = mb_get_pdf_instance('', '', 'dompdf');
			ob_start();
			include $tpl;
			$html = ob_get_clean();
			$pdf->loadHtml($html);
			$pdf->render();
			$pdf->stream(sprintf(__('payment-receipt-%s', 'payments'), sb_sanitize_title($payment->customer)),
				array('Attachment' => 0, 'Accept-Ranges' => 1));
			die();
		}
		sb_set_view_var('payment', $payment);
	}
	public function ajax_get_customer_payments()
	{
		$parent 		= SB_Request::getInt('parent');
		$customer_id 	= SB_Request::getInt('customer_id');
		
		$conds = array('parent' => $parent, 'customer_id' => $customer_id);
		$table = SB_DbTable::GetTable('mb_payments', 1);
		$payments = $table->GetRows(-1, 0, $conds);
		
		sb_response_json(array('status' => 'ok', 'payments' => $payments));
	}
}
