<?php
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;

function mb_payments_methods()
{
	return SB_Module::do_action('payments_methods', array(
		'cash'	=> array(
			'label'	=> __('Cash', 'payments'),
			'class'	=> 'success'
		),
		'credit_card'	=> array(
			'label'	=> __('Credit Card', 'payments'),
			'class'	=> 'warning'
		),
		'wire_transfer'	=> array(
			'label'	=> __('Wire Transfer', 'payments'),
			'class'	=> 'info'
		),
		'back_deposit'	=> array(
			'label'	=> __('Bank Deposit', 'payments'),
			'class'	=> 'info'
		),
		'check'	=> array(
			'label'	=> __('Check', 'payments'),
			'class'	=> 'danger'
		),
		'western_union'	=> array(
			'label'	=> __('Western Union', 'payments'),
			'class'	=> 'success'
		),
		'paypal'	=> array(
			'label'	=> __('Paypal', 'payments'),
			'class'	=> 'warning'
		),
	));
}
function mb_payments_get_sequence($store_id = null)
{
	$dbh = SB_Factory::GetDbh();
	$query = "SELECT MAX(sequence) FROM mb_payments WHERE 1 = 1 ";
	if( $store_id )
	{
		$query .= "AND store_id = $store_id ";
	}
	$query .= "LIMIT 1";
	$sequence = $dbh->GetVar($query);
	if( !$sequence )
		$sequence = 1;
	else
		$sequence++;
		
	return $sequence;	
}
function mb_payments_get_total_paid($id)
{
	$dbh 	= SB_Factory::getDbh();
	$query 	= "SELECT SUM(amount) FROM mb_payments ps WHERE ps.document_id = $id";
	
	return $dbh->GetVar($query);
}