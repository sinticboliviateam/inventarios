<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Route;

define('MOD_PAYMENTS_DIR', dirname(__FILE__));
define('MOD_PAYMENTS_URL', MODULES_URL . '/' . basename(MOD_PAYMENTS_DIR));
require_once MOD_PAYMENTS_DIR . SB_DS . 'functions.php';
class LT_ModPayments
{
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'payments', dirname(__FILE__) . SB_DS . 'locale');
		$this->AddActions();
	}
	protected function AddActions()
	{
		SB_Module::add_action('init', array($this, 'action_init'));
		SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
	}
	public function action_init()
	{
	}
	public function action_admin_menu()
	{
		if( sb_get_current_user()->IsRoot() || sb_get_current_user()->can('manage_backend') )
		{
			SB_Menu::addMenuChild('menu-management', 
								'<span class="glyphicon glyphicon-usd"></span> '.__('Payments', 'payments'), 
								SB_Route::_('index.php?mod=payments'), 
								'menu-payments', 
								'mb_payments_menu');
		}
		else
		{
			SB_Menu::addMenuPage('<span class="glyphicon glyphicon-usd"></span> '.__('Payments', 'payments'),
				SB_Route::_('index.php?mod=payments'),
				'menu-payments',
				'mb_payments_menu'
			);
		}
	}
}
new LT_ModPayments();
