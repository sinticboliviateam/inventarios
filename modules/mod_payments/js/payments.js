var payments = 
{
	Search: function()
	{
		var params = jQuery(this).serialize();
		jQuery.post('index.php', params, function(res)
		{
			jQuery('#transactions-found').html('');
			if( res.status == 'ok' )
			{
				jQuery.each(res.results, function(i, obj)
				{
					var _class = (obj.order && obj.order.payment_status == 'complete') ? 'label-success' : 'label-danger';
					var item = '<tr>'+
								'<td>' + obj.invoice_date_time + '</td>' +
								'<td>'+
									'<b>'+lt.modules.payments.locale.LABEL_CUSTOMER+':</b>' + obj.customer + '<br/>'+
									'<b>'+lt.modules.payments.locale.LABEL_STORE+':</b>'+obj.store_name+'</b><br/>'+
									'<b>'+ lt.modules.payments.locale.TOTAL_PAYMENTS + ':' + obj.total_payments +'</b>'+
								'</td>' +
								'<td class="total-paid">' + 
									'<b>' + lt.modules.payments.locale.LABEL_TOTAL_PAID + '</b><br/>'+
									obj.total_paid + 
								'</td>' +
								'<td>' + 
									'<b>' + lt.modules.payments.locale.LABEL_TOTAL + '</b><br/>'+
									obj.total + 
								'</td>' +
								'<td>' + obj._sale_type + '</td>' +
								'<td><span class="label '+_class+'">' + obj.order.payment_status + '</label></td>' +
								'<td>';
					if( obj.order && obj.order.payment_status != 'complete' )
					{
						item += '<a href="javascript:void('+obj.invoice_id+');" data-id="'+obj.id+'" class="btn btn-default btn-xs btn-select-document">'+
									'<span class="glyphicon glyphicon-ok"></span>'+
								'</a>';
					}
					
					item +=	'</td></tr>';
					jQuery('#transactions-found').append(item);
				});
				jQuery('#modal-transactions').modal('show');
			}
			else
			{
				alert(res.error);
			}
		});
		return false;
	},
	OnSelectDocument: function(e)
	{
		var sale_type = jQuery('#sale_type').val();
		var id = this.dataset.id;
		jQuery.get('index.php?mod=payments&task=get_document&id='+id+'&sale_type='+sale_type, function(res)
		{
			jQuery('#modal-transactions').modal('hide');
			if( res.status == 'ok' )
			{
				var customer_name = res.order.customer.company ? 
										res.order.customer.company : 
										res.order.customer.first_name + ' ' + res.order.customer.last_name;
				jQuery('#store_name').html(res.order.store_name);
				jQuery('#customer_id').val(res.order.customer_id);
				jQuery('#customer').val( customer_name );
				jQuery('#customer-document').html(customer_name);
				jQuery('#identity_document').val(res.order.customer._nit_ruc_nif);
				jQuery('#total_amount').val(res.order.total);
				jQuery('#total-payments').html(res.order.payments.length);
				jQuery('#total-amount').html(res.order.total);
				jQuery('#total-paid').html(res.order.total_paid);
				jQuery('#total_paid').val( !res.order.total_paid || isNaN(res.order.total_paid) ? 0 : res.order.total_paid);
				jQuery('#total-balance').html(res.order.balance);
				jQuery('#order_id').val(res.order.order_id);
			}
			else
			{
				alert(res.error);
			}
		});
		return false;
	},
	CalculateAmounts: function()
	{
		var total_amount 	= parseFloat(jQuery('#total_amount').val().trim());
		var total_paid 		= parseFloat(jQuery('#total_paid').val().trim());
		var amount 			= parseFloat(jQuery('#amount').val().trim());
		var restante		= (total_amount - total_paid);
		if( amount > restante )
		{
			amount = restante;
			jQuery('#amount').val(amount);
		}
		var diff			= restante - amount;
		if( diff <= 0 )
		{
			diff = 0;
		}
		jQuery('#balance').val(diff)
	},
	OnSelectParentPayment: function()
	{
		jQuery('#parent_id').val(this.dataset.id);
		jQuery('#total_amount').val(this.dataset.total);
		jQuery('#total_paid').val(parseFloat(this.dataset.total) - parseFloat(this.dataset.balance));
		jQuery('#balance').val(this.dataset.balance);
		jQuery('#modal-transactions').modal('hide');
		return false;
	},
	SetEvents: function ()
	{
		//##set customer search completion
		var completion = new SBCompletion({
			input: 			document.getElementById('customer'),
			url: 			lt.modules.payments.completion_customer_url,
			loading_gif: 	lt.baseurl + '/images/spin.gif',
			callback: function(sugesstion)
			{
				console.log(sugesstion.dataset);
				var obj = sugesstion.dataset;
				jQuery('#customer_id').val(obj.customer_id);
				jQuery('#identity_document').val(obj._nit_ruc_nif ? obj._nit_ruc_nif : '');
				//##get customer parent payments
				jQuery.get('index.php?mod=payments&task=ajax&action=get_customer_payments&parent=0&customer_id='+obj.customer_id, 
							function(res)
				{
					if( res.status == 'ok' && res.payments && res.payments.length > 0 )
					{
						var table = jQuery('#transactions-found');
						table.html('');
						jQuery('#modal-transactions').modal('show');
						jQuery.each(res.payments, function(i, payment)
						{
							var row = '<tr>\
										<td>'+payment.payment_date+'</td>\
										<td>'+payment.detail+'</td>\
										<td>Total: '+payment.total_amount+'</td>\
										<td>Payment: '+payment.amount+'</td>\
										<td>Balance: '+payment.balance+'</td>\
										<td>\
											<a href="javascript:;" title="select" \
												class="btn btn-default btn-xs btn-select-parent"\
												data-id="'+payment.id+'"\
												data-total="'+payment.total_amount+'"\
												data-balance="'+payment.balance+'">\
												<span class="glyphicon glyphicon-check"></span>\
											</a>\
										</td>\
									</tr>';
							table.append(jQuery(row));
						});
					}
					else if( res.error )
					{
						alert(res.error);
					}
				});
			}
		});
		
		jQuery('#total_amount').keyup(function(e)
		{
			//if( e.keyCode == 8 )
			//	return true;
				
			var number = parseFloat(this.value);
			this.value = this.value.replace(/[^0-9\.]/g, '');
			
			//if( jQuery('#amount').val().trim().length <= 0 )
			//{
				jQuery('#amount').val(this.value);
			//}
			payments.CalculateAmounts();
			return true;
		});
		jQuery('#amount').keyup(function(e)
		{
			var number = parseFloat(this.value);
			this.value = this.value.replace(/[^0-9\.]/g, '');
			
			payments.CalculateAmounts();
			return true;
		});
		jQuery('#btn-search-customer').click(function(e)
		{
			var modal = jQuery('#modal-customers');
			var iframe = '<iframe src="'+this.dataset.iframe_src+'" frameborder="0" style="width:100%;height:350px;"></iframe>';
			modal.find('#customers-iframe-wrapper').html('').html(iframe);
			modal.modal('show');
			return false;
		});
		jQuery(document).on('click', '.btn-select-parent', payments.OnSelectParentPayment);
		jQuery(document).on('click', '.btn-select-document', payments.OnSelectDocument);
	}
};
jQuery(function()
{
	try
	{
		jQuery('#form-search').submit(payments.Search);
		
		payments.SetEvents();
		jQuery('#payment-method').change(function(e)
		{
			jQuery('.payment-method-fields').css('display', 'none');
			if( this.value.length <= 0 )
				return true;
			var id = '#payment-'+this.value+'-fields'
			jQuery(id).css('display', 'block');
		});
	}
	catch(e)
	{
		console.log(e);
	}
});