��    H      \  a   �            !     )     0     8     E     T     [     `     f     s     y     �  	   �  	   �     �     �     �     �     �     �     �     �     �                         3     7     =     D     J     _     f     t     �     �     �     �     �     �     �     �     �            	   "     ,     1     8     E     K  
   R     ]     z     �  !   �  $   �     �     
	     	  
   1	     <	     O	     T	     [	     k	     ~	     �	     �	     �	    �	     �     �     �     �     �          
               +     2     E     M     V     _     e     l     s     �     �     �     �     �     �     �     �     �                         $     <     C     R     `     u     �     �     �     �     �     �     �     �     �  
        "     *     1     =     D     L     U     l     ~  $   �     �     �     �     �               6     ;     G     P     b     i     �     �                A          0   /   F   D   7   
   )                 C             ?      5      H   !       4       *   1   :                 +       E          9   <      -         8   $   2       B   .       3            ,             '                  	       ;              (   #       @           &      "      G      =   >   %                       6    Actions Amount Balance Bank Deposit Billing number Cancel Cash Check Check number Close Credit Card Customer Customer: Customers Date Date: Delete Deposit Account Number Detail Detail: Details Document Number: Document Type I received as Input Invoice NIT/Identity Document No. Notes Notes: Order Order/Invoice Number Output Pay according Payment Date Payment Information Payment Method Payment Method: Payment Num: Payment Receipt Payments Payments Management Print Receive Payment Sale Details Sale Information Sale Type Save Search Send Payment Store Store: Telephone: The document does not exists The payment does not exists The payment has been registered The payment identifier is invalid The payment template does not exists Total Amount Total Balance Total Number of Payments Total Paid Transactions Found Type Unknow Upfront Payment View Payments Menu Void Wire Transfer You need to start a session payment-receipt-%s Project-Id-Version: Mono Business Payments
POT-Creation-Date: 2017-03-21 20:12-0400
PO-Revision-Date: 2017-03-21 20:13-0400
Last-Translator: 
Language-Team: Sintic Bolivia <info@sinticbolivia.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_
X-Poedit-SearchPath-0: locale
X-Poedit-SearchPath-1: .
 Acciones Monto Saldo Deposito Bancario Numero boleta de pago Cancelar Efectivo Cheque Numero de cheque Cerrar Tarjeta de Credito Cliente Cliente: Clientes Fecha Fecha: Borrar Numero de Cuenta de Deposito Detalle Detalle: Detalles Numero de Documento: Tipo de Documento Recibi conforme Entrada Factura NIT/Documento de Identidad Nro. Notas Notas: Venta Numero de Venta/Factura Salida Pague conforme Fecha de Pago Informacion del Pago Forma de Pago Forma de Pago: Numero de Pago: Comprobante de Pago Pagos Administracion de Pagos Imprimir Recibir Pago Detalles de la Venta Informacion de la Venta Tipo Venta Guardar Buscar Enviar Pago Tienda Tienda: Telefono El documento no existe El pago no existe El pago ha sido registrado El identificador de pago es invalido La plantilla de pago no existe Monto Total Total Saldo Total Numero de Pagos Total Amortizado Transacciones Encontradas Tipo Desconocido Anticipo Ver Menu de Pagos Anular Transferencia Bancaria Necesita iniciar sesion comprobante-de-pago-%s 