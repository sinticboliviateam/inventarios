<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;

SB_Language::loadLanguage(LANGUAGE, 'payments', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('payments');
$permissions = array(
		array('group' => 'payments', 'permission' => 'mb_payments_menu', 'label'	=> __('View Payments Menu', 'users')),
		array('group' => 'payments', 'permission' => 'mb_receive_payment', 'label'	=> __('Receive Payment', 'users')),
		array('group' => 'payments', 'permission' => 'mb_send_payment', 'label'	=> __('Send Payment', 'users')),
);
sb_add_permissions($permissions);