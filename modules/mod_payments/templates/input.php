<style>
@page
{
	size: A4;
    margin: 0.2cm 0.2cm 0.2cm 0.2cm;
}
*{font-family:Helvetica,Verdana,Arial;font-size:10px;}
table{width:100%;}
</style>
<div id="receipt-container">
	<table style="width:100%;">
	<tr>
		<td style="width:50%;padding:0 20px 0 0;border-right:1px dashed #000;border-bottom:1px dashed #000;">
			<table>
			<tr>
				<td style="width:35%;">
					<img src="<?php print $logo_url; ?>" alt="" style="width:120px;" />
				</td>
				<td style="width:65%;">
					<div style="text-align:center;font-size:10px;">
						<?php print $settings->business_name; ?><br/>
						<?php print $settings->business_address; ?><br/>
						<?php print _e('Telephone:', 'payments'); ?> <?php print $settings->business_phone; ?><br/>
						<?php printf("%s - %s", $settings->business_city, $settings->business_country); ?>
					</div>
					<div style="text-align:center;font-size:15px;margin:8px 0;">
						<b><?php _e('Payment Num:', 'payments'); ?></b> <?php print sb_fill_zeros($payment->GetLastPayment()->sequence); ?>
					</div>
				</td>
			</tr>
			</table>
			<h3 style="text-align:center;font-size:18px;margin:0 0 8px 0;">
				<?php _e('Payment Receipt', 'payments'); ?><br/>
				<span style="font-size:14px;"><?php _e('Original', 'payments'); ?></span>
			</h3>
			<table>
			<tr>
				<td style="width:15%;"><b><?php _e('Customer:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment->customer; ?></td>
				<td style="width:15%;"><b><?php _e('Date:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print sb_format_datetime($payment->payment_date); ?></td>
			</tr>
			<tr>
				<td style="width:15%;"><b><?php _e('Payment Method:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment_methods[$payment->payment_method]['label']; ?></td>
				<td style="width:15%;"><b><?php _e('Document Number:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment->document_number; ?></td>
			</tr>
			</table>
			<p>
				<b><?php _e('Detail:', 'payments'); ?></b>
				<?php print $payment->GetLastPayment()->detail; ?>
			</p>
			<table>
			<tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Total', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($payment->total_amount); ?>
                </td>
			</tr>
            <tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Upfront Payment', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"><?php print sb_format_datetime($payment->payment_date) ?></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($payment->amount); ?>
                </td>
			</tr>
            <?php if( $childs = $payment->GetChilds() ): foreach($childs as $child): ?>
            <tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Upfront Payment', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"><?php print sb_format_datetime($child->payment_date) ?></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($child->amount); ?>
                </td>
			</tr>
            <?php endforeach; endif; ?>
			<tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Balance', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;"></td>
				<td style="border-bottom:1px solid #000;text-align:right;"><?php print sb_number_format($payment->GetLastPayment()->balance); ?></td>
			</tr>
			</table>
			<br/><br/>
			<p>
				<b><?php _e('Notes:', 'payments'); ?></b><br/>
				<?php print $payment->GetLastPayment()->notes; ?>
			</p>
			<table>
			<tr>
				<td style="width:50%;text-align:center;padding:10px;">
					<br/><br/><br/><br/><br/><br/>
					<?php _e('Pay according', 'payments'); ?><br/>
					<?php print $payment->customer; ?><br/>
					<?php print $payment->identity_document; ?>
				</td>
				<td style="width:50%;text-align:center;padding:10px;">
					<br/><br/><br/><br/><br/><br/>
					<?php _e('I received as', 'payments'); ?><br/>
					<?php print $settings->business_name; ?><br/>
					<?php print $user->first_name . ' ' . $user->last_name; ?>
				</td>
			</tr>
			</table>
		</td>
		<td style="width:50%;padding:0 0px 0 20px;border-bottom:1px dashed #000;">
			<table>
			<tr>
				<td style="width:35%">
					<img src="<?php print $logo_url; ?>" alt="" style="width:120px;" />
				</td>
				<td style="width:65%">
					<div style="text-align:center;font-size:10px;">
						<?php print $settings->business_name; ?><br/>
						<?php print $settings->business_address; ?><br/>
						<?php print _e('Telephone:', 'payments'); ?> <?php print $settings->business_phone; ?><br/>
						<?php printf("%s - %s", $settings->business_city, $settings->business_country); ?>
					</div>
					<div style="text-align:center;font-size:15px;margin:8px 0;">
						<b><?php _e('Payment Num:', 'payments'); ?></b> <?php print sb_fill_zeros($payment->GetLastPayment()->sequence); ?>
					</div>
				</td>
			</tr>
			</table>
			<h3 style="text-align:center;font-size:18px;margin:0 0 8px 0;">
				<?php _e('Payment Receipt', 'payments'); ?><br/>
				<span style="font-size:14px;"><?php _e('Copy', 'payments'); ?></span>
			</h3>
			<table>
			<tr>
				<td style="width:15%;"><b><?php _e('Customer:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment->customer; ?></td>
				<td style="width:15%;"><b><?php _e('Date:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print sb_format_datetime($payment->payment_date); ?></td>
			</tr>
			<tr>
				<td style="width:15%;"><b><?php _e('Payment Method:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment_methods[$payment->payment_method]['label']; ?></td>
				<td style="width:15%;"><b><?php _e('Document Number:', 'payments'); ?></b></td>
				<td style="width:35%;"><?php print $payment->document_number; ?></td>
			</tr>
			</table>
			<p>
				<b><?php _e('Detail:', 'payments'); ?></b>
				<?php print $payment->detail; ?>
			</p>
			<table>
			<tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Total', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"></td>
				<td style="border-bottom:1px solid #000;text-align:right;"><?php print sb_number_format($payment->total_amount); ?></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Upfront Payment', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"><?php print sb_format_datetime($payment->payment_date) ?></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($payment->amount); ?>
                </td>
			</tr>
            <?php if( $childs = $payment->GetChilds() ): foreach($childs as $child): ?>
            <tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Upfront Payment', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;text-align:center;"><?php print sb_format_datetime($child->payment_date) ?></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($child->amount); ?>
                </td>
			</tr>
            <?php endforeach; endif; ?>
			<tr>
				<td style="border-bottom:1px solid #000;"><b><?php _e('Balance', 'payments'); ?></b></td>
                <td style="border-bottom:1px solid #000;"></td>
				<td style="border-bottom:1px solid #000;text-align:right;">
                    <?php print sb_number_format($payment->GetLastPayment()->balance); ?>
                </td>
			</tr>
			</table>
			<br/><br/>
			<p>
				<b><?php _e('Notes:', 'payments'); ?></b><br/>
				<?php print $payment->GetLastPayment()->notes; ?>
			</p>
			<table>
			<tr>
				<td style="width:50%;text-align:center;padding:10px;">
					<br/><br/><br/><br/><br/><br/>
					<?php _e('Pay according', 'payments'); ?><br/>
					<?php print $payment->customer; ?><br/>
					<?php print $payment->identity_document; ?>
				</td>
				<td style="width:50%;text-align:center;padding:10px;">
					<br/><br/><br/><br/><br/><br/>
					<?php _e('I received as', 'payments'); ?><br/>
					<?php print $settings->business_name; ?><br/>
					<?php print $user->first_name . ' ' . $user->last_name; ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</div>
<?php
