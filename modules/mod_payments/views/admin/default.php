<?php
$can_delete		= sb_get_current_user()->can('payments_delete');
$can_void 		= sb_get_current_user()->can('payments_void');

?>
<div class="wrap">
	<div class="row">
		<h2 class="col-xs-6 col-md-6"><?php _e('Payments'); ?></h2>
		<div class="col-xs-6 col-md-6 text-right">
			<a href="<?php print $this->Route('index.php?mod=payments&view=new&type=input'); ?>" class="btn btn-primary">
				<?php _e('Receive Payment', 'payments'); ?>
			</a>
			<a href="<?php print $this->Route('index.php?mod=payments&view=new&type=output'); ?>" class="btn btn-primary">
				<?php _e('Send Payment', 'payments'); ?>
			</a>
		</div>
	</div>
	<table class="table">
	<thead>
	<tr>
		<th><?php _e('No.', 'payments'); ?></th>
		<th><?php _e('Date', 'payments'); ?></th>
		<th><?php _e('Customer', 'payments'); ?></th>
		<th><?php _e('Amount', 'payments'); ?></th>
		<th><?php _e('Payment Method', 'payments'); ?></th>
		<th><?php _e('Details', 'payments'); ?></th>
		<th><?php _e('Store', 'payments'); ?></th>
		<th><?php _e('Type', 'payments'); ?></th>
		<th><?php _e('Status', 'payments'); ?></th>
		<th><?php _e('Actions', 'payments'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($items as $item): ?>
	<tr>
		<td><?php print $i; ?></td>
		<td><?php print sb_format_date($item->payment_date); ?></td>
		<td><?php print $item->customer; ?></td>
		<td><div class="text-right"><?php print sb_number_format($item->amount); ?></div></td>
		<td>
			<?php if( isset($payment_methods[$item->payment_method]) ): ?>
			<label class="label label-<?php print $payment_methods[$item->payment_method]['class']; ?>">
				<?php print $payment_methods[$item->payment_method]['label']; ?>
			</label>
			<?php else: ?>
			<label class="label label-danger">
				<?php _e('Unknow', 'payments'); ?>
			</label>
			<?php endif; ?>
		</td>
		<td><?php print $item->detail; ?></td>
		<td>
			<?php 
			$store = new SB_MBStore($item->store_id);
			print $store->store_name;
			?>
		</td>
		<td>
			<label class="label label-<?php print $item->payment_type == 'input' ? 'success' : 'danger'; ?>">
				<?php print $item->payment_type == 'input' ? __('Input', 'payments') : __('Output', 'payments'); ?>
			</label>
		</td>
		<td>
			<label class="label label-<?php print $item->status != 'void' ? 'success' : 'danger'; ?>">
				<?php print $this->__("payment_status_{$item->status}"); ?>
			</label>
		</td>
		<td>
			<?php if( $can_delete ): ?>
			<a href="" class="btn btn-default btn-xs"
				title="<?php _e('Delete', 'payments'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
			<?php endif; ?>
			<?php if( $can_void && $item->status != 'void' ): ?>
			<a href="<?php print $this->Route('index.php?mod=payments&task=void&id=' . $item->id) ?>" class="btn btn-default btn-xs confirm"
				title="<?php _e('Void', 'payments'); ?>" data-message="<?php print $this->__('Are you sure to void the payment?'); ?>">
				<span class="glyphicon glyphicon-remove"></span></a>
			<?php endif; ?>
			<a href="<?php print $this->Route('index.php?mod=payments&view=print&id='.$item->id); ?>" target="_blank"
				class="btn btn-default btn-xs"
				title="<?php _e('Print', 'payments'); ?>">
				<span class="glyphicon glyphicon-print"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tfoot>
	<tr>
		<th colspan="3"><?php print $this->__('Total Payments Amount'); ?></th>
		<th><div class="text-right"><?php print sb_number_format($total_payments) ?></div></th>
	</tr>
	</tfoot>
	</table>
</div>
