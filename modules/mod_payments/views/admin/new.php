<?php
$type = 'input';
?>
<style>
.payment-method-fields{display:none;}
</style>
<div class="wrap">
	<h2><?php _e('Receive Payment', 'payments') ?></h2>
	<div class="col-md-6">
		<form action="" method="post" class="form-group-sm">
			<input type="hidden" name="mod" value="payments" />
			<input type="hidden" name="task" value="save" />
			<input type="hidden" name="type" value="<?php print $type ?>" />
			<input type="hidden" name="customer_id" value="" id="customer_id" />
			<input type="hidden" name="order_id" value="0" id="order_id" />
			<input type="hidden" name="parent" value="0" id="parent_id" />
			<h3><?php _e('Payment Information', 'payments'); ?></h3>
			<div class="form-group">
				<label><?php _e('Customer', 'payments'); ?></label>
				<div class="input-group">
					<input type="text" id="customer" name="customer" value="" class="form-control" />
					<span class="input-group-btn">
						<a href="javascript:;" id="btn-search-customer" class="btn btn-sm btn-default"
							data-iframe_src="<?php print $this->Route('index.php?mod=customers&view=customers_list&tpl_file=module'); ?>">
							<span class="glyphicon glyphicon-search"></span>
						</a>
					</span>
				</div>
			</div>
			<div class="form-group">
				<label><?php _e('NIT/Identity Document', 'payments'); ?></label>
				<input type="text" id="identity_document" name="identity_document" value="" class="form-control" />
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label><?php _e('Payment Method', 'payments'); ?></label>
						<select id="payment-method" name="payment_method" class="form-control">
							<?php foreach($payment_methods as $key => $pm): ?>
							<option value="<?php print $key; ?>"><?php print $pm['label']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label><?php _e('Payment Date', 'payments'); ?></label>
						<input id="payment-date" name="payment_date" 
								value="<?php print sb_format_date(time()); ?>" class="form-control datepicker" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label><?php _e('Deposit Account Number', 'payments'); ?></label>
				<input id="deposit-account-number" name="deposit_account_number" 
						value="" class="form-control" />
			</div>
			<div id="payment-fields">
				<div id="payment-check-fields" class="payment-method-fields">
					
				</div>
				<?php b_do_action('payments_method_fields'); ?>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="form-group">
						<label><?php _e('Check number', 'payments'); ?></label>
						<input type="text" name="check_number" value="" class="form-control" />
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="form-group">
						<label><?php _e('Billing number', 'payments'); ?></label>
						<input type="text" name="billing_number" value="" class="form-control" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="form-group">
						<label><?php _e('Total Amount', 'payments'); ?></label>
						<input type="text" id="total_amount" name="total_amount" value="" class="form-control"
							autocomplete="off" />
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group">
						<label><?php _e('Total Paid', 'payments'); ?></label>
						<input type="text" id="total_paid" name="total_paid" value="" class="form-control"
							autocomplete="off"  />
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group">
						<label><?php _e('Upfront Payment', 'payments'); ?></label>
						<input type="text" id="amount" name="amount" value="" class="form-control" autocomplete="off" />
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group">
						<label><?php _e('Balance', 'payments'); ?></label>
						<input type="text" id="balance" name="balance" value="" class="form-control" autocomplete="off" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label><?php _e('Detail', 'payments'); ?></label>
				<input type="text" name="detail" value="" class="form-control" />
			</div>
			<div class="form-group">
				<label><?php _e('Notes', 'payments'); ?></label>
				<textarea name="notes" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<a href="<?php print $this->Route('index.php?mod=payments'); ?>" class="btn btn-danger">
					<?php _e('Cancel', 'payments'); ?>
				</a>
				<button class="btn btn-primary"><?php _e('Save', 'payments'); ?></button>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		<form id="form-search" action="" method="post">
			<input type="hidden" name="mod" value="payments" />
			<input type="hidden" name="task" value="search" />
			<h3><?php _e('Sale Information', 'payments'); ?></h3>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5">
					<label><?php _e('Order/Invoice Number', 'payments'); ?></label>
					<input type="text" name="number" value="" class="form-control" 
						placeholder="<?php _e('Order/Invoice Number'); ?>" autocomplete="off" />
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5">
					<label><?php _e('Document Type', 'payments'); ?></label>
					<select name="sale_type" class="form-control">
						<option value="invoice"><?php _e('Invoice', 'payments'); ?></option>
						<option value="order"><?php _e('Order', 'payments'); ?></option>
					</select>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2">
					<label>&nbsp;</label>
					<a href="javascript:;" id="btn-get-info" class="btn btn-primary"><?php _e('Search', 'payments'); ?></a>
				</div>
			</div>
		</form>
		<div class="row">
			<div class="col-md-12">
				<fieldset>
					<legend><b><?php _e('Sale Details', 'payments'); ?></b></legend>
					<table class="table table-condensed">
					<tr>
						<td><b><?php _e('Store:', 'payments'); ?></b></td>
						<td><span id="store_name"></span></td>
					</tr>
					<tr>
						<td><b><?php _e('Customer:', 'payments'); ?></b></td>
						<td><span id="customer-document"></span></td>
					</tr>
					</table>
					<table class="table table-condensed">
					<tbody>
					<tr>
						<td><b><?php _e('Total Number of Payments', 'payments'); ?></b></td>
						<td class="text-right"><span id="total-payments">0</span></td>
					</tr>
					<tr>
						<td><b><?php _e('Total Amount', 'payments'); ?></b></td>
						<td class="text-right"><span id="total-amount">0.00</span></td>
					</tr>
					<tr>
						<td><b><?php _e('Total Paid', 'payments'); ?></b></td>
						<td class="text-right"><span id="total-paid">0.00</span></td>
					</tr>
					<tr>
						<td><b><?php _e('Total Balance', 'payments'); ?></b></td>
						<td class="text-right"><span id="total-balance">0.00</span></td>
					</tr>
					</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>
<div id="modal-customers" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Customers', 'payments'); ?></h4>
			</div>
			<div class="modal-body">
				<div id="customers-iframe-wrapper">
					
				</div><!-- end id="customers-iframe-wrapper" -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<?php _e('Close', 'payments'); ?>
				</button>
			</div>
		</div>
	</div>
</div><!-- class="modal" -->
<div id="modal-transactions" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php _e('Transactions Found', 'payments'); ?></h4>
			</div>
			<div class="modal-body">
				<div style="height:300px;overflow:auto;font-size:12px;">
					<table id="transactions-found" class="table table-hover table-condensed">
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<?php _e('Close', 'payments'); ?>
				</button>
			</div>
		</div>
	</div>
</div><!-- class="modal" -->