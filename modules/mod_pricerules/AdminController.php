<?php
namespace SinticBolivia\SBFramework\Modules\Pricerules;
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SB_Warehouse;

class AdminController extends SB_Controller
{
	/**
	 * @namespace SinticBolivia\SBFramework\Modules\Pricerules\Models
	 * @var PriceRulesModel
	 */
	protected $priceRulesModel;
	
	public function TaskDefault()
	{
		$title = __('Price Rules', 'pricerules');
		$stores = SB_Warehouse::getStores();
		$this->SetVars(get_defined_vars());
		
		$this->document->SetTitle($title);
	}
	public function TaskSave()
	{
		$data = $this->request->ToJSON();
		try
		{
			if( !$data->store_id )
				throw new Exception(__('Invalid store', 'pricerules'));
			if( !$data->category_id )
				throw new Exception(__('Invalid category', 'pricerules'));
			$this->dbh->BeginTransaction();
			$this->dbh->Delete('mb_pricerules', array('category_id' => $data->category_id));
			foreach($data->rules as $rule)
			{
				$newRule = (object)array(
					//'store_id' 		=> (int)$data->store_id,
					'category_id' 	=> (int)$data->category_id,
					'price'			=> $rule->price,
					'from'			=> (float)$rule->from,
					'to'			=> (float)$rule->to,
					'percent'		=> (float)$rule->percent,
					'type'			=> $data->type,
					//'creation_date'	=> date('Y-m-d H:i:s')
				);
				$this->dbh->Insert('mb_pricerules', $newRule);
			}
			$this->dbh->EndTransaction();
			sb_response_json(array('status' => 'ok', 'message' => __('The price rules has been saved', 'pricerules')));
		}
		catch(Exception $e)
		{
			$this->dbh->Rollback();
			sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
		}
	}
	public function ajax_getrules()
	{
		$category_id = $this->request->getInt('category_id');
		$query = "SELECT * FROM mb_pricerules WHERE category_id = $category_id ORDER BY id ASC";
		$items = $this->dbh->FetchResults($query);
		
		sb_response_json(array('status' => 'ok', 'items' => $items));
	}
	public function task_apply()
	{
		$category_id = $this->request->getInt('category_id');
		$type		 = $this->request->getString('type');
		try
		{
			if( !$category_id )
				throw new Exception(__('Invalid category identifier', 'mb'));
			$this->dbh->BeginTransaction();
			$this->priceRulesModel->Apply($category_id, $type);
			$this->dbh->EndTransaction();
			sb_response_json(array('status' => 'ok', 'message' => __('The price rules has been applied', 'pricerules')));
		}
		catch(Exception $e)
		{
			$this->dbh->Rollback();
			sb_response_json(array('status' => 'error', 'error' => $e->getMessage()));
		}
	}
}
