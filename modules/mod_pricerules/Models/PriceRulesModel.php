<?php
namespace SinticBolivia\SBFramework\Modules\Pricerules\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Factory;

class PriceRulesModel extends SB_Model
{
	public function Apply($category_id, $type)
	{
		$app = SB_Factory::getApplication();
		set_time_limit(0);
		$query = "SELECT * FROM mb_pricerules WHERE category_id = $category_id AND type = '$type'";
		$app->Log($query);
		$rules = $this->dbh->FetchResults($query);
		
		$query = "SELECT * FROM mb_products p, mb_product2category p2c ".
					"WHERE 1= 1 ".
					"AND p.product_id = p2c.product_id ".
					"AND p2c.category_id = $category_id ";
		
		
		$products = $this->dbh->FetchResults($query);
		//$app->Log("Total products: " . count($products));
		
		for($i = 0; $i < count($products); $i++)
		{
			$p = $products[$i];
			$update = false;
			foreach($rules as $rule)
			{
				
				if( $rule->price == 'price_1' && 
					((float)$p->product_cost >= (float)$rule->from && (float)$p->product_cost <= (float)$rule->to) 
					&& (float)$rule->percent > 0 )
				{
					
					$newPrice = ceil($p->product_cost + ((float)$p->product_cost * ((float)$rule->percent/100)));
					if ($newPrice > 100 /*&& rounding == "on"*/ ) 
					{
						$price_str 	= (int)$newPrice;
						$last_digit = (int)substr($price_str, -1);
						if ($last_digit > 0) 
						{
							//$p->product_price += (10 - $last_digit);
							$newPrice += $last_digit < 5 ? (5 - $last_digit) : (10 - $last_digit);
						}
						//$p->product_price = (int)$p->product_price;
						
					} 
					else 
					{
						$newPrice = ceil($newPrice);
					}
					$p->product_price = $newPrice;
					$update = true;
				}
				elseif( $rule->price == 'price_2' && 
					((float)$p->product_cost >= (float)$rule->from && (float)$p->product_cost <= (float)$rule->to) 
					&& (float)$rule->percent > 0 )
				{
					//SB_Factory::getApplication()->Log("applying for price 2\n product_code: {$p->product_code}");
					$p->product_price_2 = ceil($p->product_cost + ((float)$p->product_cost * ((float)$rule->percent/100)));
					if ($p->product_price_2 > 100 /*&& rounding == "on"*/ ) 
					{
						$price_str 	= (int)$p->product_price_2;
						$last_digit = (int)substr($price_str, -1);
						if ($last_digit > 0) 
						{
							//~ $p->product_price_2 += (10 - $last_digit);
							$p->product_price_2 += $last_digit < 5 ? (5 - $last_digit) : (10 - $last_digit);
						}
						$p->product_price_2 = (int)$p->product_price_2;
						
					} 
					else 
					{
						$p->product_price_2 = ceil($p->product_price_2);
					}
					//SB_Factory::getApplication()->Log("new price: $p->product_price");
					$update = true;
				}
				elseif( $rule->price == 'price_3' && 
					((float)$p->product_cost >= (float)$rule->from && (float)$p->product_cost <= (float)$rule->to) 
					&& (float)$rule->percent > 0 )
				{
					$p->product_price_3 = $p->product_cost + ((float)$p->product_cost * ((float)$rule->percent/100));
					$update = true;
				}
				elseif( $rule->price == 'price_4' 
					&& ((float)$p->product_cost >= (float)$price4_rule->from && (float)$p->product_cost <= (float)$rule->to) 
					&& (float)$rule->percent > 0 )
				{
					$p->product_price_4 = $p->product_cost + ((float)$p->product_cost * (float)$price4_rule->percent);
					$update = true;
				}
			}
			
			if( $update )
			{
				$prices = array(
					'product_price' 	=> $p->product_price,
					'product_price_2'	=> $p->product_price_2,
					'product_price_3' 	=> $p->product_price_3,
					'product_price_4' 	=> $p->product_price_4,
				);
				$this->dbh->Update('mb_products', 
					$prices,
					array('product_id' => $p->product_id));
			}
		}
	}
}
