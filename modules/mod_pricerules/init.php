<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Route;

class SinticBolivia_SBFramework_Modules_PriceRules
{
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'pricerules', dirname(__FILE__) . SB_DS . 'locale');
		$this->AddActions();
	}
	protected function AddActions()
	{
		if( lt_is_admin() )
		{
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
		}
	}
	public function action_admin_menu()
	{
		SB_Menu::addMenuChild('menu-management', 
								'<span class="glyphicon glyphicon-sort"></span> ' . __('Price Rules', 'pricerules'), 
								SB_Route::_('index.php?mod=pricerules'), 
								'mb-pricerules', 
								'mbpc_build_rules');
	}
}
$mod_pr = new SinticBolivia_SBFramework_Modules_PriceRules();
