<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;

SB_Language::loadLanguage(LANGUAGE, 'pricerules', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('pricerules');
$perms = array(
	array('group' => 'pricerules', 'permission' => 'mbpc_build_rules', 'label' => __('Build Price Rules', 'pricerules')),
	array('group' => 'pricerules', 'permission' => 'mbpc_apply_rules', 'label' => __('Apply Price Rules', 'pricerules'))
);
sb_add_permissions($perms);
