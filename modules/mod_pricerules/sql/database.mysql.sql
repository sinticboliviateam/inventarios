CREATE TABLE IF NOT EXISTS mb_pricerules(
	id			bigint not null primary key auto_increment,
	category_id		bigint not null,
	price			varchar(64),
	`from`			decimal(10, 2),
	`to`			decimal(10,2),
	percent			decimal(10,2),
	type			varchar(64),
	creation_date	datetime
)engine=InnoDB;
