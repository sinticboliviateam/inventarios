<div class="wrap">
	<h2 id="page-title">
		<?php print $title; ?>
	</h2>
	<div>
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#cost-rules" data-toggle="tab"><?php _e('By Cost', 'pricerules'); ?></a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="cost-rules" class="tabpane active">
				<form action="" method="post" id="form-cost-rules" class="form-group-sm" data-type="cost">
					<input type="hidden" name="mod" value="pricerules" />
					<input type="hidden" name="task" value="save_cost_rules" />
					
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<button type="submit" class="btn btn-primary">
								<?php _e('Save Price Rules', 'pricerules'); ?>
							</button>
							<a href="javascript:;" id="btn-apply-cost-rules" class="btn btn-success">
								<?php _e('Save & Apply Price Rules', 'pricerules'); ?>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label><?php _e('Store', 'pricerules'); ?></label>
								<select id="cost_store_id" name="store_id" class="form-control" data-target="#cost_categories">
									<option value=""><?php _e('-- store --', 'mb'); ?></option>
									<?php foreach($stores as $store): ?>
									<option value="<?php print $store->store_id ?>"><?php print $store->store_name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label><?php _e('Category', 'pricerules'); ?></label>
								<select id="cost_categories" name="cat_id" class="form-control">
									<option value=""><?php _e('-- categories --', 'mb'); ?></option>
								</select>
								<?php //print mb_dropdown_categories(array('name' => 'cat_id', 'type' => 'select')); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<fieldset>
								<legend><?php _e('Price 1', 'pricerules'); ?></legend>
								<div>
									<a href="javascript:;" class="btn-add-rule btn btn-primary btn-xs" data-price="1">
										<span class="glyphicon glyphicon-plus-sign"></span> <?php _e('New Rule', 'pricerules'); ?>
									</a>
									<a href="javascript:;" class="btn-delete-rule btn btn-danger btn-xs" data-price="1">
										<span class="glyphicon glyphicon-minus-sign"></span>  <?php _e('Delete Rule', 'pricerules'); ?>
									</a>
								</div>
								<div class="table-responsive">
									<table id="cost_table_1" class="table table-condensed  table-hover table-bordered">
									<thead>
									<tr>
										<th>#</th>
										<th><?php _e('From', 'pricerules'); ?></th>
										<th><?php _e('To', 'pricerules'); ?></th>
										<th><?php _e('Percent(%)', 'pricerules'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?php if( isset($rules1) ): $i = 1; foreach($rules1 as $rule): ?>
									<tr>
										<td><?php print $i; ?></td>
										<td><?php print sb_number_format($rule->from); ?></td>
										<td><?php print sb_number_format($rule->to); ?></td>
										<td><?php print sb_number_format($rule->percent); ?></td>
									</tr>
									<?php $i++; endforeach;endif; ?>
									</tbody>
									</table>
								</div>
							</fieldset>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<fieldset>
								<legend><?php _e('Price 2', 'pricerules'); ?></legend>
								<div>
									<a href="javascript:;" class="btn-add-rule btn btn-primary btn-xs" data-price="2">
										<span class="glyphicon glyphicon-plus-sign"></span> <?php _e('New Rule', 'pricerules'); ?>
									</a>
									<a href="javascript:;" class="btn-delete-rule btn btn-danger btn-xs" data-price="2">
										<span class="glyphicon glyphicon-minus-sign"></span>  <?php _e('Delete Rule', 'pricerules'); ?>
									</a>
								</div>
								<div class="table-responsive">
									<table id="cost_table_2" class="table table-condensed table-hover table-bordered">
									<thead>
									<tr>
										<th>#</th>
										<th><?php _e('From', 'pricerules'); ?></th>
										<th><?php _e('To', 'pricerules'); ?></th>
										<th><?php _e('Percent(%)', 'pricerules'); ?></th>
									</tr>
									</thead>
									<tbody>
									<?php if( isset($rules1) ): $i = 1; foreach($rules1 as $rule): ?>
									<tr>
										<td><?php print $i; ?></td>
										<td><?php print sb_number_format($rule->from); ?></td>
										<td><?php print sb_number_format($rule->to); ?></td>
										<td><?php print sb_number_format($rule->percent); ?></td>
									</tr>
									<?php $i++; endforeach;endif; ?>
									</tbody>
									</table>
								</div>
							</fieldset>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>
.cell-editable{margin:2px;border:1px solid #ececec;padding:2px;}
.cell-editable:focus{background:#FFFF77;}
</style>
<script>

function OnCellKeydown(e)
{
	if( e.keyCode == 9  || e.keyCode == 13 )
	{
		e.preventDefault();
		this.setAttribute('contenteditable', false);
		var row 	= jQuery(this).parents('tr').get(0);
		var cells 	= jQuery(row).find('td');
		row.dataset.from 	= isNaN(parseFloat(cells[1].innerText)) ? 0 : parseFloat(cells[1].innerText);
		row.dataset.to 		= isNaN(parseFloat(cells[2].innerText)) ? 0 : parseFloat(cells[2].innerText);
		row.dataset.percent = isNaN(parseFloat(cells[3].innerText)) ? 0 : parseFloat(cells[3].innerText);
		jQuery(this).removeClass('cell-editable');
		return false;
	}
	return true;
}
var currentRow = null;
function SetCellEditable(e)
{
	currentRow = jQuery(this).parents('tr:first');
	jQuery(this).addClass('cell-editable');
	this.setAttribute('contenteditable', true);
	this.focus();
}
function AddPriceRule(table, data)
{
	var tableBody = jQuery(table).find('tbody');
	var totalRows = tableBody.find('tr').length;
	var newIndex	= totalRows + 1;
	
	var row			= document.createElement('tr');
	var col1		= document.createElement('td');
	var col2		= document.createElement('td');
	var col3		= document.createElement('td');
	var col4		= document.createElement('td');
	
	col1.innerText	= newIndex;
	col2.innerText	= data.from || ''; 
	col3.innerText	= data.to || '';
	col4.innerText	= data.percent || '';
	//col1.addEventListener('click', SetCellEditable);
	col2.addEventListener('click', SetCellEditable);
	col3.addEventListener('click', SetCellEditable);
	col4.addEventListener('click', SetCellEditable);
	
	col2.addEventListener('blur', function(e)
	{
		console.log(this.innerHTML);
		var row 	= jQuery(this).parents('tr').get(0);
		var cells 	= jQuery(row).find('td');
		//row.dataset.from 	= isNaN(parseFloat(cells[1].innerText)) ? 0 : parseFloat(cells[1].innerText);
	});
	//col3.addEventListener('click', SetCellEditable);
	//col4.addEventListener('click', SetCellEditable);
	//col1.addEventListener('keydown', OnCellKeydown);
	col2.addEventListener('keydown', OnCellKeydown);
	col3.addEventListener('keydown', OnCellKeydown);
	col4.addEventListener('keydown', OnCellKeydown);
	
	row.tabIndex 		= 10 + newIndex;
	row.dataset.price 	= 'price_' + (data.price.replace('price_', '') || '');
	row.dataset.from	= data.from || '';
	row.dataset.to		= data.to || '';
	row.dataset.percent	= data.percent || '';
	row.appendChild(col1);
	row.appendChild(col2);
	row.appendChild(col3);
	row.appendChild(col4);
	tableBody.append(row);
	col2.click();
	return false;
}
function DeletePriceRule(e)
{
	if( !currentRow )
		return true;
	currentRow.remove();
}
function SaveRules()
{
	try
	{
		var store_id 	= parseInt(this.store_id.value);
		var category_id = parseInt(this.cat_id.value);
		
		if(  isNaN(store_id) || store_id.value <= 0 )
			throw '<?php _e('You need to select a store'); ?>';
		if( isNaN(category_id) || category_id.value <= 0 )
			throw '<?php _e('You need to select a category'); ?>';
		jQuery('#modal-processing').modal('show');
		var data = {store_id: store_id, category_id: category_id, type: this.dataset.type, rules: []};
		var tables = jQuery(this).find('table')
		tables.each(function(i, table)
		{
			var body = jQuery(table).find('tbody');
			var rows = body.find('tr');
			
			rows.each(function(i, row)
			{
				data.rules.push(row.dataset);
			});
		});
		jQuery.post('index.php?mod=pricerules&task=save', JSON.stringify(data), function(res)
		{
			jQuery('#modal-processing').modal('hide');
			if( res.status == 'ok' )
			{
				alert(res.message);
			}
		})
	}
	catch(e)
	{
		alert(e);
	}
	return false;
}
function GetCategories()
{
	var dropdown = jQuery(this.dataset.target);
	if( isNaN(parseInt(this.value)) || this.value <= 0 )
	{
		dropdown.html('<option value="-1"><?php _e('-- category --', 'mb'); ?></option>');
		return false;
	}
	dropdown.html('<option value="-1"><?php _e('-- category --', 'mb'); ?></option>');
	jQuery.get('index.php?mod=mb&task=ajax&action=get_store_cats&store_id='+this.value, function(res)
	{
		if(res.status == 'ok')
		{
			jQuery.each(res.categories, function(i, cat)
			{
				var op = jQuery('<option value="'+cat.category_id+'">'+cat.name+'</option>');
				dropdown.append(op);
				if( cat.childs && cat.childs.length > 0 )
				{
					jQuery.each(cat.childs, function(ii, subcat)
					{
						var sop = jQuery('<option value="'+subcat.category_id+'">- '+subcat.name+'</option>');
						dropdown.append(sop);
					});
					
				}
			});
		}
		else
		{
			alert(res.error);
		}
	});
}
function ResetRules(table)
{
	jQuery(table).find('tbody').html('');
}
function GetRules()
{
	ResetRules('#cost_table_1');
	ResetRules('#cost_table_2');
	if( this.value <= 0 )
	{
		
		return false;
	}
	jQuery('#modal-processing').modal('show');
	jQuery.get('index.php?mod=pricerules&task=ajax&action=getrules&category_id=' + this.value + '&type=cost', function(res)
	{
		jQuery('#modal-processing').modal('hide');
		if( res.status == 'ok' )
		{
			jQuery.each(res.items, function(i, rule)
			{
				var tableId = '#'+ rule.type +'_table_' + rule.price.replace('price_', '');
				console.log(tableId);
				AddPriceRule(tableId, rule);
				
			});
		}
	});
}
function SaveAndApply()
{
	var category_id = jQuery('#cost_categories').val();
	if( category_id <= 0 )
	{
		alert('<?php _e('You need to select a category'); ?>');
		return false;
	}
	jQuery('#modal-processing').modal('show');
	var params = 'mod=pricerules&task=apply&category_id=' + category_id + '&type=cost';
	jQuery.post('index.php', params, function(res)
	{
		jQuery('#modal-processing').modal('hide');
		if( res.status == 'ok' )
			alert(res.message);
		else
			alert(res.error || 'Unknow error');
	});
	return false;
}
jQuery(function()
{
	jQuery('#cost_store_id').change(GetCategories);
	jQuery('#cost_categories').change(GetRules);
	jQuery('.btn-add-rule').click(function(e)
	{
		
		var tableId = '#cost_table_' + this.dataset.price;
		AddPriceRule(tableId, {price: this.dataset.price});
	});
	jQuery('.btn-delete-rule').click(DeletePriceRule);
	jQuery('#form-cost-rules').submit(SaveRules);
	jQuery('#btn-apply-cost-rules').click(SaveAndApply);
});
</script>
<div id="modal-processing" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="processing-message"><?php _e('Processing, please wait...', 'mb'); ?></div>
				<img src="<?php print BASEURL; ?>/images/loadingAnimation.gif" alt="" />
			</div>
		</div>
	</div>
</div>
