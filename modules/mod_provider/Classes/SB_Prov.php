<?php
namespace SinticBolivia\SBFramework\Modules\Provider\Classes;
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use \SB_Warehouse;

/**
 * 
 * @author marcelo
 * 
 *@property int $supplier_id
 *@property int $store_id
 *@property	string $supplier_key
 *@property string $supplier_name
 *@property string $supplier_address
 *@property string $nit_ruc_nif
 *@property string $creation_date
 */
class SB_Prov extends SB_ORMObject
{
	//protected $_dbData = null;
	//protected $_permissions = array();
	//protected $_meta = array();
	
	public function __construct($supplier_id = null)
	{
		parent::__construct();
		if( $supplier_id)
			$this->GetDbData($supplier_id);
	}
	public function GetDbData($supplier_id)
	{
		$dbh = SB_Factory::getDbh();
		//$query = "SELECT * FROM users u, user_roles r WHERE u.user_id = $user_id AND u.role_id = r.role_id";
		$query = "SELECT s.*, t.store_name FROM mb_suppliers as s LEFT JOIN mb_stores as t on s.store_id = t.store_id WHERE supplier_id = $supplier_id";
		$res = $dbh->Query($query);
		if( !$res ){
			return false; 
		}
		$this->_dbData = $dbh->FetchRow();
		$this->GetDbMeta();
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_supplier_meta WHERE supplier_id = $this->supplier_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->meta[$row->meta_key] = trim($row->meta_value);
		}
		
	}
}