<?php
namespace SinticBolivia\MonoBusiness\Modules\Provider\Classes;
use SinticBolivia\SBFramework\Classes\SB_Object;
/**
 *
 * @author marcelo
 *
 *@property int $store_id
 *@property int $store_name
 *@property	string $store_key
 *@property string $store_address
 *@property string $store_type
 *@property string $creation_date
 */
class SB_Stores extends SB_Object
{
	protected $_dbData = null;
	//protected $_permissions = array();
	protected $_meta = array();

	public function __construct($store_id = null)
	{
		if( $store_id)
			$this->getData($store_id);
	}
	public function getData($store_id)
	{
		$dbh = SB_Factory::getDbh();
		//$query = "SELECT * FROM users u, user_roles r WHERE u.user_id = $user_id AND u.role_id = r.role_id";
		$query = "SELECT * FROM mb_stores WHERE store_id = $store_id LIMIT 1";
		$res = $dbh->Query($query);
		if( !$res ){
			return false;
		}
		$this->_dbData = $dbh->FetchRow();
	}
}