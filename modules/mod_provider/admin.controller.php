<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;
use SinticBolivia\SBFramework\Classes\SB_Text;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Modules\Provider\Classes\SB_Prov;

class LT_AdminControllerProvider extends SB_Controller
{
	public function task_default()
	{	
		
		$dbh = SB_Factory::getDbh();
		$columns = array(
				's.*',
				't.store_name'
		);
		$tables = array(
				'mb_suppliers as s LEFT JOIN mb_stores as t on s.store_id = t.store_id'
		);
		$order = SB_Request::getString('order', 'desc');
		$order_by = SB_Request::getString('order_by', 'creation_date');
		$query = sprintf("SELECT %s FROM %s ORDER BY %s $order",
				implode(',', $columns),
				implode(',', $tables),
				$order_by
				);
		//print $query;
		$res = $dbh->Query($query);//revisar
		$provider = $dbh->FetchResults();
		/*$new_order = $order == 'desc' ? 'asc' : 'desc';
		$code_order_link 		= 'index.php?mod=provider&order_by=conde&order='.$new_order;
		$name_order_link 		= 'index.php?mod=provider&order_by=name&order='.$new_order;
		$address_order_link		= 'index.php?mod=provider&order_by=address&order='.$new_order;
		$nit_order_link			= 'index.php?mod=provider&order_by=nit&order='.$new_order;
		$stores_order_link			= 'index.php?mod=provider&order_by=store&order='.$new_order;
		sb_set_view_var('code_order_link', SB_Route::_($code_order_link));
		sb_set_view_var('name_order_link', SB_Route::_($name_order_link));
		sb_set_view_var('address_order_link', SB_Route::_($address_order_link));
		sb_set_view_var('nit_order_link', SB_Route::_($nit_order_link));
		sb_set_view_var('stores_order_link', SB_Route::_($stores_order_link));*/
		sb_set_view_var('provider', $provider);
	}
	
	public function task_new_prov()
	{
		sb_include_module_helper('provider');
		$stores = array();
		foreach (LT_HelperProvider::GetStore() as $r){
			$stores[] = $r;
		}
		sb_set_view('edit_prov');
		sb_add_script(BASEURL.'/js/fineuploader/all.fine-uploader.min.js', 'fineuploader');
		sb_set_view_var('stores', $stores);
		sb_set_view_var('title', __('Create New Supplier','provider'));
		SB_Module::do_action('on_create_new_provider');
	}
	
	public function task_save_provider()
	{
		if( !sb_get_current_user()->can('provider_save') ){
			die('You dont have enough permissions');
		}
		$supplier_id 		= SB_Request::getInt('supplier_id');
		$store_id 			= SB_Request::getInt('store_id');
		$supplier_key 		= SB_Request::getString('supplier_key');
		$supplier_name 		= SB_Request::getString('supplier_name');
		$supplier_address 	= SB_Request::getString('supplier_address');
		$nit_ruc_nif 		= SB_Request::getString('nit_ruc_nif');
		$supplier_contact_person = SB_Request::getString('supplier_contact_person');
		$meta				= SB_Request::getVar('meta', array());
		
		if(empty($supplier_key)){
 			SB_MessagesStack::AddMessage('Debe ingresar el codigo del proveedor','error');
 			$this->task_new_prov();
 			return false;
		}
		if(empty($supplier_name)){
			SB_MessagesStack::AddMessage('Debe ingresar nombres y apellidos','error');
			$this->task_new_prov();
			return false;
		}
		/*
		if(empty($supplier_address)){
			SB_MessagesStack::AddMessage('Debe ingresar los direccion','error');
			$this->task_new_prov();
			return false;
		}
		if(empty($nit_ruc_nif)){
			SB_MessagesStack::AddMessage('Debe ingresar la nit','error');
			$this->task_new_prov();
			return false;
		}
		*/
		/*
		$stores = new SB_Stores($store_id);
		if( $stores->store_id )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('La tienda de proveedor no existe','provider'),'error');
			return false;
		}
		*/
		$dbh = SB_Factory::getDbh();//devuelve la base de datos
		$query = "select * from mb_suppliers where supplier_key = '$supplier_key' LIMIT 1";
		$key = $dbh->Query($query);
		if( !$supplier_id && $key )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('The supplier code already exists.','provider'),'error');
			$this->task_new_prov();
			return false;
		}
		/*$query = "select * from mb_suppliers where supplier_name = '$supplier_name' LIMIT 1";
		$key = $dbh->Query($query);
		if($supplier_name && $key){
			SB_MessagesStack::AddMessage(SB_Text::_('El nombre del proveedor ya existe.','provider'),'error');
			$this->task_new_prov();
			return false;
		}*/
		$provider = null;
		if( $supplier_id )
		{
			$provider = new SB_Prov($supplier_id);
		}
		$cdate = date('Y-m-d H:i:s');
		$data = array(
				'store_id' => $store_id,
				'supplier_key' => $supplier_key,
				'supplier_name' => $supplier_name,
				'supplier_address' => $supplier_address,
				'nit_ruc_nif' => $nit_ruc_nif,
				'supplier_contact_person' => $supplier_contact_person
		);//direcciona al campos de la tabla y le ponemos el valor
		SB_Module::do_action('before_save_provider', $supplier_id);
		$msg = $link = null;
		if(!$supplier_id)
		{
			$data['creation_date'] = SB_Request::getString('creation_date')?
										sb_format_datetime(SB_Request::getString('creation_date'),'Y-m-d H:i:s'):$cdate;
			//sacamos el identificador de la BD mediante una consulta
			$supplier_id = $dbh->Insert('mb_suppliers', $data);//insertamos datos en la tabla mb_suppliers
			SB_Module::do_action('save_provider',$supplier_id);
			$msg = __('The supplier has been created', 'provider');
			$link = SB_Route::_('index.php?mod=provider');
		}
		else
		{
			if(!$provider->supplier_id){
				SB_MessagesStack::AddMessage(SB_Text::_('El identificador de proveedor no existe.','provider'),'error');
				return false;
			}
			$data['creation_date'] = sb_format_datetime(SB_Request::getString('creation_date'), 'Y-m-d H:i:s');
			$dbh->Update('mb_suppliers', $data, array('supplier_id' => $supplier_id));
			SB_Module::do_action('save_provider', $supplier_id);
			$msg = __('The supplier has been updated', 'provider');
			$link = SB_Route::_('index.php?mod=provider&view=edit_prov&id='.$supplier_id);
		}
		//##update meta
		foreach($meta as $meta_key => $meta_value)
		{
			SB_Meta::updateMeta('mb_supplier_meta', $meta_key, $meta_value, 'supplier_id', $supplier_id);
		}
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect($link);
	}
	public function task_edit_prov()
	{
		if(!sb_get_current_user()->can('provider_edit')){
			die('You dont have enough permissions');
		}
		$supplier_id = SB_Request::getInt('id');
		if(!$supplier_id){
			SB_MessagesStack::AddMessage(__('The supplier identifier is invalid'),'provider');
			sb_redirect(SB_Route::_('index.php?mod=provider'));
		}
		sb_include_module_helper('provider');
		$provider = new SB_Prov($supplier_id);
		if(!$provider->supplier_id){
			SB_MessagesStack::AddMessage(__('The supplier does not exists'),'provider');
			sb_redirect(SB_Route::_('index.php?mod=provider'));
		}
		//$c_user = sb_get_current_user();
		$stores = array();
		$stores = LT_HelperProvider::GetStore();
		sb_add_script(BASEURL . '/js/fineuploader/all.fine-uploader.min.js', 'fineuploader');
		sb_set_view_var('stores', $stores);
		sb_set_view_var('supplier_id', $supplier_id);
		sb_set_view_var('provider', $provider);
		sb_set_view_var('title', __('Edit Supplier', 'provider'));
	}
	public function task_delete_prov()
	{
		$supplier_id = SB_Request::getInt('id');
		if( !$supplier_id )
		{
			SB_MessagesStack::AddMessage(SB_Text::_('Identificador de usuario no valido','provider'),'error');
			sb_redirect(SB_Route::_('index.php?mod=provider'));
		}
		$provider = new SB_Prov($supplier_id);
		if( !$provider->supplier_id)
		{
			SB_MessagesStack::AddMessage(SB_Text::_('El proveedor no existe','provider'),'error');
			sb_redirect(SB_Route::_('index.php?mod=provider'));
		}
		//##deletee provider folder
		$this->dbh->Delete('mb_suppliers', array('supplier_id' => $provider->supplier_id));
		SB_MessagesStack::AddMessage(__('The supplier has been deleted.'),'success');
		sb_redirect(SB_Route::_('index.php?mod=provider'));
	}
}