<?php
	function sb_get_provider_meta($user_id, $meta_key)
	{
		return SB_Meta::getMeta('user_meta', $meta_key, 'user_id', $user_id);
	}
