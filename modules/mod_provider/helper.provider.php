<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;
class LT_HelperProvider
{
	public  static function GetProvider($supplier_id){
		$supplier_id = (int)$supplier_id;
		$dbh = SB_Factory::getDbh();
		$query = "select * from mb_suppliers where supplier_id =  $supplier_id LIMIT 1";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
	public  static function GetStore(){
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_stores ORDER BY store_name ASC";
		$dbh->Query($query);
		return $dbh->FetchResults();
	}
}