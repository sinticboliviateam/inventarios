<?php
namespace SinticBolivia\MonoBusiness\Modules\Provider;
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Route;

define('MOD_PROVIDER_DIR', dirname(__FILE__));
define('MOD_PROVIDER_URL', MODULES_URL . '/' . basename(MOD_PROVIDER_DIR));
require_once MOD_PROVIDER_DIR . SB_DS . 'functions.php';
//require_once MOD_PROVIDER_DIR . SB_DS . 'classes' . SB_DS . 'class.prov.php';
require_once MOD_PROVIDER_DIR . SB_DS . 'Classes' . SB_DS . 'class.store.php';

class SB_ModProviders
{
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'provider', MOD_PROVIDER_DIR . SB_DS . 'locale');
		$this->AddActions();
	}
	protected function AddActions()
	{
		if( lt_is_admin() )
		{
			SB_Module::add_action('init', array($this, 'action_init'));
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'), 1);
			SB_Module::add_action('admin_dashboard', array($this,'action_admin_dashboard'));
		}
	}
	public function action_init()
	{
		
	}
	public function action_admin_menu()
	{
		SB_Menu::addMenuChild('menu-management',
				'<span class="glyphicon glyphicon-user"></span> ' . __('Suppliers','provider'),
				SB_Route::_('index.php?mod=provider'),
				'menu_providers_x',
				'mb_manage_provider');
		SB_Menu::addMenuChild('mb-menu-inventory',
				'<span class="glyphicon glyphicon-user"></span> ' . __('Suppliers','provider'),
				SB_Route::_('index.php?mod=provider'),
				'menu_providers_x',
				'mb_manage_provider');
	}
	
	public function action_admin_dashboard()
	{
	}
}
new SB_ModProviders();