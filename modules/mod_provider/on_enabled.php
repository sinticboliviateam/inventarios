<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;

$dbh = SB_Factory::getDbh();
SB_Language::loadLanguage(LANGUAGE, 'provider', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('provider');
$permissions = array(
		array('group' => 'provider', 'permission' => 'mb_manage_provider', 'label'	=> __('Manage Providers (MB)', 'provider')),
		array('group' => 'provider', 'permission' => 'provider_save', 'label' => __('Save Provider', 'provider')),
		array('group' => 'provider', 'permission' => 'provider_edit', 'label' => __('Edit Provider', 'provider')),
		array('group' => 'provider', 'permission' => 'provider_delete', 'label' => __('Delete Provider', 'provider'))
);
sb_add_permissions($permissions);