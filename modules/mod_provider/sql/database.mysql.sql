CREATE TABLE IF NOT EXISTS `mb_suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(128) DEFAULT NULL,
  `supplier_address` varchar(256) DEFAULT NULL,
  `supplier_address_2` varchar(256) DEFAULT NULL,
  `supplier_telephone_1` text,
  `supplier_telephone_2` text,
  `fax` varchar(64) DEFAULT NULL,
  `supplier_details` text,
  `supplier_city` text,
  `supplier_email` text,
  `supplier_contact_person` varchar(256) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `bank_account` varchar(100) DEFAULT NULL,
  `nit_ruc_nif` varchar(50) DEFAULT NULL,
  `supplier_key` varchar(10) DEFAULT NULL,
  `last_modification_date` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
);
CREATE TABLE IF NOT EXISTS mb_supplier_meta(
	id				bigint not null auto_increment primary key,
	supplier_id 	integer not null,
	meta_key		varchar(512),
	meta_value		text,
	creation_date	datetime
);
