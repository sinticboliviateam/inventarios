<?php
?>
<div class="wrap">
	<h2 id="page-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php _e('Suppliers', 'provider'); ?></div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="page-buttons">
						<a class="btn btn-primary" href="index.php?mod=provider&view=new_prov"
							data-content="<?php _e('PROVIDER_BUTTON_NEW');?>">
							<?php _e('New','provider');?>
						</a>	
					</div>
				</div>
			</div>
		</div>
	</h2>
	<div class="table-responsive">
		<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>
					<a href="<?php print $code_order_link;?>" class="has-popover" data-content="<?php _e('PROVIDER_TH_CODE');?>">
						<?php _e('Codigo','provider');?>
						<span class = "glyphicon glyphicon-triangle-<?php print ($this->request->getString('order_by') == 'code' && $this->request->getString('order','asc') == 'asc') ? 'bottom' : 'top';?>">
						</span>
					</a>
				</th>
				<th>
					<a href="<?php print $name_order_link; ?>" class="has-popover" data-content="<?php _e('PROVIDER_TH_NAME');?>">
						<?php _e('Nombre','provider');?>
						<span class = "glyphicon glyphicon-triangle-<?php print ($this->request->getString('order_by') == 'name' && $this->request->getString('order','asc') == 'asc') ? 'bottom' : 'top';?>">
						</span>
					</a>
				</th>
				<th>
					<a href="<?php print $address_order_link; ?>" class="has-popover" data-content="<?php _e('PROVIDER_TH_ADDRESS');?>">
						<?php _e('Direccion','provider');?>
						<span class = "glyphicon glyphicon-triangle-<?php print ($this->request->getString('order_by') == 'address' && $this->request->getString('order','asc') == 'asc') ? 'bottom' : 'top';?>">
						</span>
					</a>
				</th>
				<th>
					<a href="<?php print $nit_order_link; ?>" class="has-popover" data-content="<?php _e('PROVIDER_TH_NIT');?>">
						<?php _e('Nit','provider');?>
						<span class = "glyphicon glyphicon-triangle-<?php print ($this->request->getString('order_by') == 'nit' && $this->request->getString('order','asc') == 'asc') ? 'bottom' : 'top';?>">
						</span>
					</a>
				</th>
				<th>
					<a href = "<?php print $stores_order_link; ?>" class = "has-popover" data-content = "<?php _e('PROVIDEER_TH_STORE')?>">
						<?php _e('Tienda','provider');?>
						<span class="glyphicon glyphicon-triangle-<?php print ($this->request->getString('order_by') == 'store' && $this->request->getString('order','asc') == 'asc') ? 'bottom'  : 'top';?>">
						</span>
					</a>
				</th>
				<th>
					<a href="#" class="has-popover" data-content="<?php _e('PROVIDER_TH_ACTIONS');?>">
						<?php _e('Acciones','provider');?>
					</a>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php $i = 1;foreach ($provider as $prov):?>
		<tr>
			<td><?php print $i; ?></td>
			<td><?php print $prov->supplier_key;//hace referencias a los nombres de las columnas de la tablas mb_supplier?></td>
			<td><?php print $prov->supplier_name;?></td>
			<td><?php print $prov->supplier_address;?></td>
			<td><?php print $prov->nit_ruc_nif;?></td>
			<td><?php print $prov->store_name;?></td>
			<td>
				<a href="index.php?mod=provider&view=edit_prov&id=<?php print $prov->supplier_id; ?>" 
					title="<?php _e('Edit', 'provider'); ?>" class="btn btn-default btn-xs">
					<span class="glyphicon glyphicon-pencil"></span>
				</a>
				<a href="index.php?mod=provider&task=delete_prov&id=<?php print $prov->supplier_id; ?>" 
					title="<?php _e('Delete', 'provider'); ?>" class="btn btn-default btn-xs confirm" 
					data-message="<?php _e('Are you sure to delete the supplier?', 'provider'); ?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
			</td>
		</tr>
		<?php $i++; endforeach;?>			
		</tbody>
		</table>
	</div>
</div><!-- end class="container" -->