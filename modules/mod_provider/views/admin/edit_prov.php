<?php 
$selected_store = $this->request->getInt('store_id', isset($provider) ? $provider->store_id : -1);
?>
<div class="wrap">
	<h2 id="page-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php print $title; ?></div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="page-buttons">
						<a href="index.php?mod=provider" class="btn btn-danger">
							<?php _e('Cancel', 'mbp');?>
						</a>
						<a href="javascript:;" class="btn btn-success" onclick="document.getElementById('form-provider').submit();">
							<?php _e('Save','provider');?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</h2>
	
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-general" data-toggle="tab"><?php _e('General Information', 'provider'); ?></a>
		</li>
		<li>
			<a href="#tab-contact" data-toggle="tab"><?php _e('Contact Information', 'provider'); ?></a>
		</li>
	</ul>
	<form id="form-provider" action="" method="post" class="tab-content">
		<input type="hidden" name="mod" value="provider"/>
		<input type="hidden" name="task" value="save_provider" />
		<?php if(isset($provider)):?>
		<input type="hidden" name="supplier_id" value="<?php print $supplier_id?> "/>
		<?php endif;?>
		<div id="tab-general" class="tab-pane active">
			<h3><?php _e('General Information', 'provider');?></h3>
				<div class="form-group">
					<label class = "has-popover" data-content="<?php _e('PROVIDER_CODE');?>">
						<?php _e('Codigo:','provider'); ?></label>
					<input type="text" name="supplier_key" value="<?php print $this->request->getString('supplier_key', isset($provider) ? $provider->supplier_key : '');?>" class = "form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Nombre:','provider'); ?></label>
					<input type="text" name="supplier_name" value="<?php print $this->request->getString('supplier_name', isset($provider) ? $provider->supplier_name : '');?>" class = "form-control"/>
				</div>			
				<div class="form-group">
					<label><?php _e('Direccion:','provider'); ?></label>
					<input type="text" name="supplier_address" value="<?php print $this->request->getString('supplier_address', isset($provider) ? $provider->supplier_address : '');?>" class = "form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Nit:','provider'); ?></label>
					<input type="text" name="nit_ruc_nif" value ="<?php print $this->request->getString('nit_ruc_nif', isset($provider) ? $provider->nit_ruc_nif : '');?>" class = "form-control" />
				</div>
				<div class="form-group">
					<label><?php _e('Tiendas','provider');?></label>
					<select id="store_id" name="store_id" class="form-control">
						<option value="-1"><?php _e('-- Stores --','provider'); ?></option>
							<?php foreach ($stores as & $store):?>
						<option value="<?php  print $store->store_id;?>" <?php print ($selected_store == $store->store_id ) ? 'selected' : ''; ?>>
							<?php print $store->store_name; ?>
						</option>
						<?php endforeach;?>
					</select>
				</div>
		</div><!-- end id="tab-general" -->
		<div id="tab-contact" class="tab-pane">
			<div class="form-group">
				<label><?php _e('Name:', 'provider'); ?></label>
				<input type="text" name="supplier_contact_person" value="<?php print isset($provider) ? $provider->supplier_contact_person: ''; ?>" class="form-control" />
			</div>
			<div class="form-group">
				<label><?php _e('Telephone:', 'provider'); ?></label>
				<input type="text" name="meta[_contact_telephone]" value="<?php print isset($provider) ? $provider->_contact_telephone : ''; ?>" class="form-control" />
			</div>
			<div class="form-group">
				<label><?php _e('Notes:', 'provider'); ?></label>
				<textarea name="meta[_contact_notes]" class="form-control"><?php print isset($provider) ? $provider->_contact_notes : ''; ?></textarea>
			</div>
		</div><!-- end id="tab-general" -->
	</form>
</div>
