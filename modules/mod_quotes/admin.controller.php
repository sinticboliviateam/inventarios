<?php
use SinticBolivia\SBFramework\Classes\SB_Controller;
use SinticBolivia\SBFramework\Classes\SB_Request;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Meta;
use SinticBolivia\SBFramework\Classes\SB_MessagesStack;

class LT_AdminControllerQuotes extends SB_Controller
{
	public function task_default()
	{
		$user = sb_get_current_user();
		$keyword = SB_Request::getString('keyword');
		$limit		= SB_Request::getInt('limit', defined('ITEMS_PER_PAGE') ? ITEMS_PER_PAGE : 25);
		$page		= SB_Request::getInt('page', 1);
		if( $page <= 0 )
			$page = 1;
		$concat = "CONCAT(c.first_name, ' ', c.last_name)";
		if( $this->dbh->db_type == 'sqlite' || $this->dbh->db_type == 'sqlite3' )
		{
			$concat = "c.first_name || ' ' || c.last_name";
		}
		$subquery = "SELECT $concat ".
					"FROM mb_customers c ".
					"WHERE c.customer_id = q.customer_id";
		
		$columns = "q.*, (SELECT store_name FROM mb_stores s WHERE s.store_id = q.store_id) AS store_name, ($subquery) AS customer";
		
		$query = "SELECT {columns} " .
					"FROM mb_quotes q " .
					"WHERE 1 = 1 " . 
					"AND status <> 'deleted' ";
		if( !$user->IsRoot() )
		{
			if( !$user->can('quote_view_all') )
			{
				if( !$user->_store_id )
				{
					lt_die(__('You dont have any store assigned', 'quotes'));
				} 
				$query .= "AND q.store_id = {$user->_store_id} ";
			}
			
		}
		if( is_numeric($keyword) )
		{
			$id = (int)$keyword;
			$query .= "AND quote_id = $id ";
		}
		elseif( !empty($keyword) )
		{
			$query .= "AND ($subquery) LIKE '%$keyword%' ";
		}
		$total_rows 	= $this->dbh->GetVar(str_replace('{columns}', 'COUNT(*)', $query));
		$total_pages	= ceil($total_rows / $limit);
		$offset			= $page == 1 ? 0 : ($page - 1) * $limit; 
		$query .= "ORDER BY q.quote_date DESC ";
		$query .= "LIMIT $offset, $limit";
		$this->dbh->Query(str_replace('{columns}', $columns, $query));
		$invoices = $this->dbh->FetchResults();
		sb_set_view_var('quotes', $invoices);
		sb_set_view_var('total_pages', $total_pages);
		sb_set_view_var('current_page', $page);
		$this->document->SetTitle(__('Quotes', 'quotes'));
	}
	public function task_new()
	{
		$user = sb_get_current_user();
		if( !$user->can('create_quote') )
		{
			lt_die(__('You dont have permission to complete this action', 'quotes'));
		}
		if( $user->role_id !== 0 && !$user->_store_id )
		{
			lt_die(__('You dont have an store assigned to sell', 'mb'));
		}
		$stores = SB_Warehouse::GetUserStores(sb_get_current_user());
		$query = "SELECT * FROM mb_tax_rates ORDER BY name ASC";
		$taxes = $this->dbh->FetchResults($query);
		$coupons = $this->dbh->FetchResults("SELECT * FROM mb_coupons ORDER BY creation_date ASC");
		$title = __('New Quote', 'quotes');
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_QUOTES_URL . '/js/new.js', 'quotes-js');
		sb_add_js_global_var('quotes', 'completion_product_url', SB_Route::_('index.php?mod=quotes&task=search_product'));
		sb_add_js_global_var('quotes', 'completion_customer_url', SB_Route::_('index.php?mod=quotes&task=search_customer'));
		sb_add_js_global_var('quotes', 'locale', array(
			'INVALID_CUSTOMER_NAME' => __('You need to enter a customer name', 'quotes'),
			'INVALID_CUSTOMER_NIT'	=> __('You need to enter a customer nit/ruc/nif', 'quotes'),
			'INVALID_QUOTE_AMOUNT'	=> __('Your quote has no amount, please review your items prices and quantities', 'quotes'),
			'NEED_SELECT_CUSTOMER'	=> __('You need to select a customer', 'quotes')
		));
		lt_add_tinymce();
		sb_set_view_var('stores', $stores);
		sb_set_view_var('taxes', $taxes);
		sb_set_view_var('coupons', $coupons);
		sb_set_view_var('title', $title);
		$this->document->SetTitle($title);
	}
	public function task_edit()
	{
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The quote identifier is invalid', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			SB_MessagesStack::AddMessage(__('The quote does not exists', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$stores = SB_Warehouse::getStores();
		$query = "SELECT * FROM mb_tax_rates ORDER BY name ASC";
		$taxes = $this->dbh->FetchResults($query);
		$coupons = $this->dbh->FetchResults("SELECT * FROM mb_coupons ORDER BY creation_date ASC");
		
		$title = __('Edit Quote', 'quotes');
		sb_add_script(BASEURL . '/js/sb-completion.js', 'sb-completion');
		sb_add_script(MOD_QUOTES_URL . '/js/new.js', 'quotes-js');
		sb_add_js_global_var('quotes', 'completion_product_url', SB_Route::_('index.php?mod=quotes&task=search_product'));
		sb_add_js_global_var('quotes', 'completion_customer_url', SB_Route::_('index.php?mod=quotes&task=search_customer'));
		sb_add_js_global_var('quotes', 'locale', array(
			'INVALID_CUSTOMER_NAME' => __('You need to enter a customer name', 'quotes'),
			'INVALID_CUSTOMER_NIT'	=> __('You need to enter a customer nit/ruc/nif', 'quotes'),
			'INVALID_QUOTE_AMOUNT'	=> __('Your quote has no amount, please review your items prices and quantities', 'quotes'),
			'NEED_SELECT_CUSTOMER'	=> __('You need to select a customer', 'quotes')
		));
		lt_add_tinymce();
		sb_set_view('new');
		sb_set_view_var('stores', $stores);
		sb_set_view_var('taxes', $taxes);
		sb_set_view_var('coupons', $coupons);
		sb_set_view_var('title', $title);
		sb_set_view_var('quote', $quote);
		$this->document->SetTitle($title);
	}
	public function task_save()
	{
		if( !sb_is_user_logged_in() )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You need to start a session', 'invoices')));
		}
		if( !sb_get_current_user()->can('create_quote') )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You dont have enough permissions for the operation.', 'invoices')));
		}
		$dbh 			= SB_Factory::getDbh();
		$quote_id		= SB_Request::getInt('quote_id');
		$name			= SB_Request::getString('name');
		$customer_id 	= SB_Request::getInt('customer_id');
		$customer_name	= SB_Request::getString('customer');
		$nit			= SB_Request::getInt('nit_ruc_nif');
		$store_id		= SB_Request::getInt('store_id', 0);
		$coupon_id		= SB_Request::getInt('coupon_id');
		$tax_id			= SB_Request::getInt('tax_id');
		//$tax_rate		= SB_Request::getFloat('tax_rate');
		$total_tax		= SB_Request::getFloat('total_tax');
		$tax_percent 	= SB_Request::getFloat('tax_rate');
		$items			= SB_Request::getVar('items');
		$notes			= htmlentities(SB_Request::getString('notes'));
		$meta			= SB_Request::getVar('meta');
		if( $customer_id <= 0 )
		{
			SB_MessagesStack::AddMessage(__('You need to select a customer', 'quotes'));
			$quote_id ? $this->task_edit() : $this->task_new();
			return false;
		}
		if( empty($customer_name) )
		{
			sb_response_json(array('status' => 'error', 'error' => SBText::_('You must enter a customer name', 'invoices')));
		}
		//##create the quote
		$total 		= 0;
		$tax 		= 0;
		$subtotal 	= 0;
		$total_items = 0;
		//print_r($_POST);die();
		for($i = 0; $i < count($items) ; $i++)
		{
			$items[$i]['subtotal'] = $items[$i]['qty'] * $items[$i]['price'];
			$items[$i]['total'] = $items[$i]['subtotal'];// + $items[$i]['tax'];
			$subtotal 	+= $items[$i]['subtotal'];
			//$tax 		+= $items[$i]['tax'];
			//$total 		+= $subtotal + $tax;
			$total_items += $items[$i]['qty'];
		}
		$total_tax = $tax_percent > 0 ? $subtotal * $tax_percent : 0;
		$total = $subtotal + $total_tax;
		
		$cdate = date('Y-m-d H:i:s');
		$data = array(
				'name'					=> $name,
				'customer_id' 			=> $customer_id,
				'user_id'				=> sb_get_current_user()->user_id,
				'store_id'				=> $store_id,
				'items'					=> $total_items,
				'tax_id'				=> $tax_id,
				'tax_rate'				=> $tax_percent,
				'subtotal'				=> $subtotal,
				'total_tax'				=> number_format($total_tax, 2, '.', ''),
				'discount'				=> 0,
				'total'					=> $total,
				'notes'					=> $notes,
				'status'				=> 'created',
				'quote_date'			=> $cdate,
				'last_modification_date'=> $cdate,
		);
		$msg = '';
		if( !$quote_id )
		{
			$data['sequence']		= mb_quote_get_next_sequence($store_id);
			$data['creation_date'] 	= $cdate;
			$quote_id = $this->dbh->Insert('mb_quotes', $data);
			$msg = __('The quote has been created', 'quotes');
		}
		else 
		{
			$this->dbh->Update('mb_quotes', $data, array('quote_id' => $quote_id));
			$this->dbh->Delete('mb_quote_items', array('quote_id' => $quote_id));
			$msg = __('The quote has been updated', 'quotes');
		}
		$db_items = array();
		foreach($items as $item)
		{
			$db_items[] = array(
					'product_id'				=> (isset($item['id']) && (int)$item['id'] > 0) ? $item['id'] : 0,
					'quote_id'					=> $quote_id,
					'item_code'					=> trim($item['code']),
					'name'						=> htmlentities($item['name']),
					'quantity'					=> $item['qty'],
					'price'						=> $item['price'],
					'subtotal'					=> $item['subtotal'],
					'tax_rate'					=> 0,
					'total_tax'					=> 0,
					'discount'					=> 0,
					'total'						=> $item['total'],
					'status'					=> 'created',
					'last_modification_date'	=> $cdate,
					'creation_date'				=> $cdate
			);
		}
		$dbh->InsertBulk('mb_quote_items', $db_items);
		//##add/update quote meta
		foreach($meta as $meta_key => $meta_value)
		{
			SB_Meta::updateMeta('mb_quote_meta', $meta_key, htmlentities(trim($meta_value)), 'quote_id', $quote_id);
		}
		SB_Module::do_action('quote_save', $quote_id, $data, $items);
		SB_MessagesStack::AddMessage($msg, 'success');
		sb_redirect(SB_Route::_('index.php?mod=quotes&view=edit&id=' . $quote_id));
	}
	public function task_delete()
	{
		if( !sb_get_current_user()->can('delete_quote') )
		{
			lt_die(__('You dont have enough permissions to complete this operation', 'quotes'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The quote identifier is invalid', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			SB_MessagesStack::AddMessage(__('The quote does not exists', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$this->dbh->Update('mb_quotes', array('status' => 'deleted'), array('quote_id' => $quote->quote_id));
		SB_MessagesStack::AddMessage(__('The quote has been deleted', 'quotes'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=quotes'));
	}
	public function task_void()
	{
		if( !sb_is_user_logged_in() )
		{
			SB_MessagesStack::AddMessage(SBText::_('You need to start a session'), 'error');
			sb_redirect(SB_Route::_('login.php'));
		}
		if( !sb_get_current_user()->can('void_invoice') )
		{
			SB_MessagesStack::AddMessage(SBText::_('You dont have enough permission to complete this operation', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The invoice identifier is invalid', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$dbh = SB_Factory::getDbh();
		$query = "SELECT * FROM mb_invoices WHERE invoice_id = $id LIMIT 1";
		if( !$dbh->Query($query) )
		{
			SB_MessagesStack::AddMessage(SBText::_('The invoice does not exists', 'invoices'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=invoices'));
		}
		$invoice = $dbh->FetchRow();
		$dbh->Update('mb_invoices', array('status' => 'void'), array('invoice_id' => $invoice->invoice_id));
		SB_MessagesStack::AddMessage(SBText::_('The invoice is now void', 'invoices'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=invoices'));
	}
	public function task_view()
	{
		if( !sb_get_current_user()->can('quote_view') )
		{
			die(SBText::_('You dont have enough permissions to print quotes.', 'quotes'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The quote identifier is invalid', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
	
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The quote does not exists.', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$tpl = SB_Module::do_action('quote_tpl', MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php');
		//##get monobusiness settings
		$business = (object)sb_get_parameter('mb_settings');
		//sb_set_view_var('tpl', $tpl);
		sb_set_view_var('quote', $quote);
		sb_set_view_var('business', $business);
		//sb_add_style('quote-preview', MOD_QUOTES_URL . '/css/quote-preview.css');
		//sb_add_style('quote-preview', MOD_QUOTES_URL . '/css/quote-print.css');
		$this->document->SetTitle(__('Quote Preview', 'quotes'));
	}
	public function task_print()
	{
		if( !sb_get_current_user()->can('print_quote') )
		{
			die(SBText::_('You dont have enough permissions to print quotes.', 'quotes'));
		}
		$id = SB_Request::getInt('id');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The quote identifier is invalid', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			SB_MessagesStack::AddMessage(SBText::_('The quote does not exists.', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		sb_set_view('print');
		//##get monobusiness settings
		$business 	= (object)sb_get_parameter('mb_settings');
		//$_dompdf_show_warnings = true;
		ini_set('display_errors', 1);error_reporting(E_ALL);
		$pdf 		= mb_get_pdf_instance('', '', 'dompdf');
				
		ob_start();
		include SB_Module::do_action('quote_tpl', MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php');
		$quote_html = ob_get_clean();
		$pdf->loadHtml($quote_html);
		$pdf->render();
		$pdf->stream(sprintf(__('quote-%d-%d', 'quotes'), $quote->store_id, $quote->sequence),
				array('Attachment' => 0, 'Accept-Ranges' => 1));
	}
	public function task_send()
	{
		$id = SB_Request::getInt('id');
		$email_to = SB_Request::getString('email_to');
		$subject = SB_Request::getString('subject');
		$message = SB_Request::getString('message');
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('Invalid quote identifier', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		if( empty($email_to) )
		{
			SB_MessagesStack::AddMessage(__('Invalid destination email.', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes&view=edit&id='.$id));
		}
		
		$quote = new SB_MBQuote($id);
		if( empty($subject) )
		{
			$subject = sprintf("%s for %s %s", html_entity_decode(__('Quotation', 'quotes')), $quote->customer->first_name, $quote->customer->last_name);
		}
		//##get monobusiness settings
		$business = (object)sb_get_parameter('mb_settings');
		//sb_set_view('print');
		$quote_filename = MOD_QUOTES_DIR . SB_DS . sprintf(__('quotation_%s.pdf', 'quotes'), 
															sb_fill_zeros($quote->sequence));
		ob_start();
		include MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php';
		$quote_html = ob_get_clean();
		sb_include_lib('dompdf/dompdf.php');
		
		$pdf = new Dompdf\Dompdf();
		$pdf->set_option('defaultFont', 'Helvetica');
		$pdf->set_option('isRemoteEnabled', true);
		$pdf->set_option('isPhpEnabled', true);
		//$pdf->set_option('defaultPaperSize', 'Legal');
		// (Optional) Setup the paper size and orientation
		$pdf->setPaper('A4'/*, 'landscape'*/);
		$pdf->loadHtml($quote_html);
		$pdf->render();
		//$pdf->stream($quote_filename, array('Attachment' => 0));
		file_put_contents($quote_filename, $pdf->output());
		/*
		//##convert html quote to pdf
		sb_include_lib('tcpdf/tcpdf.php');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator('MonoBusiness - Sintic Bolivia');
		$pdf->SetAuthor('J. Marcelo Aviles Paco');
		$pdf->SetTitle(sprintf(__('Quotation %s', 'quotes'), sb_fill_zeros($quote->quote_id)));
		$pdf->SetSubject(__('Quotation', 'quotes'));
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->SetFont('times', '', 11, '', true);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();
		$pdf->writeHTML($quote_html, true, false, true, false, '');
		$pdf->Output($quote_filename, 'F');
		//die($quote_html);
		*/
		$email_from = 'info@sinticbolivia.net';
		$headers = array(
				'from' 		=> sprintf("From: \"%s\" <%s>", $business->business_name, $email_from),
				'type'		=> "Content-type: text/html",
				'reply'		=> "Reply-To: $email_from",
				'confirm'	=> "Return-Receipt-To: $email_from"
		);
		lt_mail($email_to, $subject, $message, $headers, $quote_filename);
		unlink($quote_filename);
		//##change quote status
		$this->dbh->Update('mb_quotes', array('status' => 'sent'), array('quote_id' => $quote->quote_id));
		SB_MessagesStack::AddMessage(__('The quote has been sent.', 'quotes'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=quotes&view=edit&id='.$id));
	}
	public function task_search_customer()
	{
		if( !SB_Module::moduleExists('customers') )
		{
			die(__('Customer module does not exists', 'quotes'));
		}
		$keyword = SB_Request::getString('keyword');
		$query = "SELECT * FROM mb_customers 
					WHERE 1 = 1
					AND (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR company LIKE '%$keyword%') 
					ORDER BY last_name ASC";
		$results = $this->dbh->FetchResults($query);
		if( SB_Request::getInt('completion') )
		{
			for($i = 0; $i < count($results); $i++)
			{
				$results[$i]->id = $results[$i]->customer_id;
				$results[$i]->name = $results[$i]->first_name . ' ' . $results[$i]->last_name;
				$results[$i]->label = $results[$i]->first_name . ' ' . $results[$i]->last_name;
			}
		}
		sb_response_json(array('status' => 'ok', 'results' => $results));
	}
	public function task_search_product()
	{
		$keyword = SB_Request::getString('keyword');
		if( !SB_Module::moduleExists('mb') )
		{
			die();
		}
		$query = "SELECT p.* ".
					"FROM mb_products p ".
					"WHERE 1 = 1 " .
					//"AND p.product_id = b.product_id ".
					"AND p.product_name LIKE '%$keyword%' ".
					"ORDER BY p.product_name ASC";
		$items = array();
		$items = $this->dbh->FetchResults($query);
		for($i = 0; $i < count($items); $i++)
		{
			$row = $items[$i];
			$items[$i]->id = $row->product_id;
			$items[$i]->name = $row->product_name;
			$items[$i]->label = sprintf("%s", $row->product_name);
		}
		sb_response_json(array('status' => 'ok', 'results' => $items));
	}
	public function task_sell()
	{
		if( !sb_is_user_logged_in() )
		{
			lt_die(__('You need to start session', 'quotes'));
		}
		if( !sb_get_current_user()->can('quote_sell') )
		{
			lt_die(__('You cant do this operation', 'quotes'));
		}
		$id = SB_Request::getInt('id');
		$billing_name 	= SB_Request::getString('billing_name');
		$billing_number	= SB_Request::getString('billing_number');
		$sale_type 		= SB_Request::getInt('sale_type');
		
		if( !$id )
		{
			SB_MessagesStack::AddMessage(__('The quote identifier is invalid', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			SB_MessagesStack::AddMessage(__('The quote does not exists', 'quotes'), 'error');
			sb_redirect(SB_Route::_('index.php?mod=quotes'));
		}
		$store = new SB_MBStore($quote->store_id);
		$odata = array(
				'code'						=> '',
				'store_id'					=> $quote->store_id,
				'transaction_type_id'		=> (int)$store->_sale_tt_id,
				'items'						=> 0,
				'subtotal'					=> $quote->subtotal,
				'total_tax'					=> $quote->total_tax,
				'discount'					=> $quote->discount,
				'total'						=> $quote->total,
				'details'					=> '',
				'status'					=> 'complete',
				'payment_status'			=> 'complete',
				'user_id'					=> sb_get_current_user()->user_id,
				'customer_id'				=> $quote->customer_id,
				'order_date'				=> date('Y-m-d H:i:s'),
				'type'						=> 'quote',
				'last_modification_date'	=> date('Y-m-d H:i:s'),
				'creation_date'				=> date('Y-m-d H:i:s'),
		);
		$oitems = array();
		foreach($quote->GetItems() as $item)
		{
			$_item = array(
					'product_id'				=> $item->product_id,
					'order_id'					=> null,
					'name'						=> $item->name,
					'quantity'					=> $item->quantity,
					'price'						=> $item->price,
					'subtotal'					=> $item->subtotal,
					'total_tax'					=> $item->total_tax,
					'discount'					=> $item->discount,
					'total'						=> $item->total,
					'status'					=> 'complete',
					'last_modification_date'	=> date('Y-m-d H:i:s'),
					'creation_date'				=> date('Y-m-d H:i:s')
			);
			$_item = SB_Module::do_action('mb_build_order_item', $_item, $item);
			$oitems[] = $_item;
		}
		//##insert the order
		$order_id = mb_insert_sale_order($odata, $oitems);
		//##set order meta
		mb_add_order_meta($order_id, '_sale_type', $sale_type);
		$the_order = new SB_MBOrder($order_id);
		$the_order->Complete();
		//##update customer metadata for invoice
		sb_update_customer_meta($quote->customer_id, '_billing_name', $billing_name);
		sb_update_customer_meta($quote->customer_id, '_nit_ruc_nif', $billing_number);
		//##call hooks
		SB_Module::do_action('mb_pos_register_sale', $order_id, $odata, $oitems);
		//##get invoice id
		$invoice_id = (int)mb_get_order_meta($order_id, '_invoice_id');
		//##set order quote_id
		SB_Meta::addMeta('mb_order_meta', '_quote_id', $quote->quote_id, 'order_id', $order_id);
		//##set quote order id
		SB_Meta::addMeta('mb_quote_meta', '_order_id', $order_id, 'quote_id', $quote->quote_id);
		//##set quote invoice id
		SB_Meta::addMeta('mb_quote_meta', '_invoice_id', $invoice_id, 'quote_id', $quote->quote_id);
		//##update quote status
		$this->dbh->Update('mb_quotes', array('status' => 'sold'), array('quote_id' => $quote->quote_id));
		SB_Module::do_action('quote_sold', $quote);
		SB_MessagesStack::AddMessage(__('The quote has been sold', 'quotes'), 'success');
		sb_redirect(SB_Route::_('index.php?mod=quotes&view=edit&id='.$quote->quote_id));
	}
	public function task_downloadpdf()
	{
		sb_set_view('print');
		$id = SB_Request::getInt('id');
		$quote = new SB_MBQuote($id);
		//##get monobusiness settings
		$business = (object)sb_get_parameter('mb_settings');
		//sb_set_view('print');
		ob_start();
		include SB_Module::do_action('quote_tpl', MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php');
		$quote_html = ob_get_clean();
		
		$quote_filename = MOD_QUOTES_DIR . SB_DS . sprintf(__('quotation_%s.pdf', 'quotes'), 
															sb_fill_zeros($quote->sequence));
		//##convert html quote to pdf
		sb_include_lib('dompdf/dompdf.php');
		
		$pdf = new Dompdf\Dompdf();
		$pdf->set_option('defaultFont', 'Helvetica');
		$pdf->set_option('isRemoteEnabled', true);
		$pdf->set_option('isPhpEnabled', true);
		//$pdf->set_option('defaultPaperSize', 'Legal');
		// (Optional) Setup the paper size and orientation
		$pdf->setPaper('A4'/*, 'landscape'*/);
		$pdf->loadHtml($quote_html);
		$pdf->render();
		$pdf->stream(sprintf(__('quote-%d-%d.pdf', 'mb'), $quote->quote_id, $quote->sequence),
				array('Attachment' => 0, 'Accept-Ranges' => 1));
		die();
		
		/*
		sb_include_lib('tcpdf/tcpdf.php');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator('MonoBusiness - Sintic Bolivia');
		$pdf->SetAuthor('J. Marcelo Aviles Paco');
		$pdf->SetTitle(sprintf(__('Quotation %s', 'quotes'), sb_fill_zeros($quote->quote_id)));
		$pdf->SetSubject(__('Quotation', 'quotes'));
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->SetFont('times', '', 11, '', true);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();
		$pdf->writeHTML($quote_html, true, false, true, false, '');
		$pdf->Output($quote_filename, 'I');
		*/
	}
}
