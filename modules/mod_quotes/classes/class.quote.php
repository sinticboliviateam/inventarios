<?php
use SinticBolivia\SBFramework\Classes\SB_ORMObject;
use SinticBolivia\SBFramework\Modules\Customers\Classes\SB_MBCustomer;

/**
 * 
 * @author marcelo
 * 
 * @property int quote_id
 * @property string code
 * The quote code
 * @property int store_id
 * @property int items
 * @property float subtotal
 * @property float total_tax
 * @property float discount
 * @property float total
 * @property string notes
 * @property string status
 * @property int user_id
 * The user who create/registered the quote
 * @property int customer_id
 * The customer quote for
 * @property datetime quote_date
 * The quote date
 * @property datetime last_modification_date
 * @property datetime creation_date
 * The quote creation date
 */
class SB_MBQuote extends SB_ORMObject
{
	protected 	$items 		= array();
	public		$customer;
	
	public function __construct($id = null)
	{
		parent::__construct();
		if( $id )
			$this->GetDbData($id);
	}
	public function GetDbData($id)
	{
		$query = "SELECT * FROM mb_quotes WHERE quote_id = $id LIMIT 1";
		if( !$this->dbh->Query($query) )
			return false;
		$this->_dbData = $this->dbh->FetchRow();
		$this->GetDbMeta();
		if( $this->customer_id )
			$this->customer = new SB_MBCustomer($this->customer_id); 
	}
	public function SetDbData($data)
	{
		$this->_dbData = $data;
	}
	public function GetDbMeta()
	{
		$query = "SELECT * FROM mb_quote_meta WHERE quote_id = $this->quote_id";
		foreach($this->dbh->FetchResults($query) as $meta)
		{
			$this->meta[$meta->meta_key] = $meta->meta_value;
		}
	}
	public function GetDbItems()
	{
		$query = "SELECT * FROM mb_quote_items WHERE quote_id = $this->quote_id";
		foreach($this->dbh->FetchResults($query) as $row)
		{
			$this->items[] = $row;
		}
	}
	/**
	 * Get quote items
	 * 
	 * @return array <multitype:, object>
	 */
	public function GetItems()
	{
		if( count($this->items) <= 0 )
			$this->GetDbItems();
		return $this->items;
	}
}
