<?php
class LT_ControllerQuotes
{
	public function task_download()
	{
		$id = SB_Session::getVar('to_download_quote_id');
		if( !$id )
		{
			lt_die(__('The quote identifier is invalid', 'quotes'));
		}
		$quote = new SB_MBQuote($id);
		if( !$quote->quote_id )
		{
			lt_die(__('The quote does not exists', 'quotes'));
		}
		SB_Session::unsetVar('to_download_quote_id');
		$business 	= (object)sb_get_parameter('mb_settings');
		//$_dompdf_show_warnings = true;
		ini_set('display_errors', 1);error_reporting(E_ALL);
		$pdf 		= mb_get_pdf_instance('', '', 'dompdf');
				
		ob_start();
		include SB_Module::do_action('quote_tpl', MOD_QUOTES_DIR . SB_DS . 'templates' . SB_DS . 'quote.php');
		$quote_html = ob_get_clean();
		$pdf->loadHtml($quote_html);
		$pdf->render();
		$pdf->stream(sprintf(__('quote-%d-%d', 'quotes'), $quote->store_id, $quote->sequence),
				array('Attachment' => 1, 'Accept-Ranges' => 1));
	}
}