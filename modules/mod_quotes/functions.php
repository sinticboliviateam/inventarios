<?php
use SinticBolivia\SBFramework\Classes\SB_Factory;
/**
 * Get the next sequence for a new quote
 * @param int $store_id
 */
function mb_quote_get_next_sequence($store_id)
{
	$dbh = SB_Factory::getDbh();
	$sequence = $dbh->GetVar("SELECT MAX(sequence) + 1 FROM mb_quotes WHERE store_id = $store_id LIMIT 1");
	if( !$sequence )
		$sequence = 1;
	
	return (int)$sequence;
}
function mb_quote_insert($data, $items)
{
	$dbh = SB_Factory::GetDbh();
	$id = null;
	if( !isset($data['quote_id']) || !$data['quote_id'] )
	{
		$data['sequence']		= mb_quote_get_next_sequence($data['store_id']);
		$data['creation_date'] 	= date('Y-m-d H:i:s');
		$id = $dbh->Insert('mb_quotes', $data);
		
	}
	else 
	{
		$id = $data['quote_id'];
		$dbh->Update('mb_quotes', $data, array('quote_id' => $quote_id));
		$dbh->Delete('mb_quote_items', array('quote_id' => $quote_id));
	}
	
	for($i = 0; $i < count($items); $i++) 
	{
		$items[$i]['quote_id'] = $id;
	}
	$dbh->InsertBulk('mb_quote_items', $items);
	return $id;
}