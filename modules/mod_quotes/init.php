<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Menu;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Request;

define('MOD_QUOTES_DIR', dirname(__FILE__));
define('MOD_QUOTES_URL', MODULES_URL . '/' . basename(MOD_QUOTES_DIR));
//require_once MOD_QUOTES_DIR . SB_DS . 'functions.php';
require_once MOD_QUOTES_DIR . SB_DS . 'classes' . SB_DS . 'class.quote.php';
require_once MOD_QUOTES_DIR . SB_DS . 'functions.php';

class LT_ModuleQuotes
{
	public function __construct()
	{
		SB_Language::loadLanguage(LANGUAGE, 'quotes', MOD_QUOTES_DIR . SB_DS . 'locale');
		$this->AddActions();
	}
	protected function AddActions()
	{
		if( lt_is_admin() )
		{
			SB_Module::add_action('admin_menu', array($this, 'action_admin_menu'));
			SB_Module::add_action('invoice_new', array($this, 'action_invoice_new'));
		}
	}
	public function action_admin_menu()
	{
		SB_Menu::addMenuPage('<span class="glyphicon glyphicon-folder-close"></span> '.__('Quotes', 'quotes'), SB_Route::_('index.php?mod=quotes'), 'menu-quotes', 'manage_quotes');
	}
	public function action_invoice_new()
	{
		if( !($quote_id = SB_Request::getInt('quote_id')) )
		{
			return false;
		}
		if( !SB_Module::moduleExists('invoices') )
		{
			lt_die(__('The invoices module does not exists, please install it'));
		}
		$quote 		= new SB_MBQuote($quote_id);
		$customer 	= new SB_MBCustomer($quote->customer_id);
		$invoice 	= new LT_MBInvoice();
		
		$invoice->tax	= $quote->total_tax;
		$invoice->total = $quote->total;
		$invoice->subtotal = $quote->subtotal;
		$invoice->customer_id = $customer->customer_id;
		$invoice->customer		= sprintf("%s %s", $customer->first_name, $customer->last_name);
		foreach($quote->GetItems() as $i)
		{
			$item = array(
					'product_id'	=> $i->product_id,
					'product_name'	=> $i->name,
					'price'			=> $i->price,
					'quantity'		=> $i->quantity,
					'subtotal'		=> $i->subtotal,
					'total'			=> $i->total
			);
			$invoice->AddItem((object)$item);
		}
		sb_set_view_var('invoice', $invoice);
	}
} 
new LT_ModuleQuotes();