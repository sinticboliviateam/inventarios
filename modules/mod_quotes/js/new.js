function SBQuote()
{
	var item_tpl = '<tr data-id="{id}" data-realqty="{real_qty}">'+
						'<td>{number}<input type="hidden" name="items[{index}][id]" value="{id}" /></td>' +
						'<td><input type="text" name="items[{index}][code]" value="{code}" class="form-control" /></td>' +
						'<td class="column-product"><input type="text" name="items[{index}][name]" value="{name}" class="form-control" /></td>' +
						'<td><input type="number" min="1" name="items[{index}][qty]" value="1" class="form-control item-qty" /></td>' +
						'<td class="column-price">\
							<div class="input-group">\
								<input type="text" name="items[{index}][price]" value="{price}" class="form-control item-price text-right" />\
								<span class="input-group-addon">\
									<a href="javascript:;" class="price-selector" title="Seleccionar precio">\
										<span class="glyphicon glyphicon-equalizer"></span>\
									</a>\
								</span>\
							</div>\
						</td>' +
						'<td class="text-right"><span class="item-total">{total}</span></td>' +
						'<td><a href="javascript:;" class="remove-item"><span class="glyphicon glyphicon-trash"></span></a></td>' +
					'</tr>';
	var $this 		= this;
	var table		= jQuery('#quote-table');
	var search 		= jQuery('#search_product');
	var form		= jQuery('#form-quote');
	this.tax_rate 	= jQuery('#tax_rate').val();
	
	this.ItemExists = function(id)
	{
		var result = false;
		table.find('tbody tr').each(function(i, row)
		{
			if( row.dataset.id == id )
			{
				result = row;
				return false;
			}
		});
		return result;
	};
	this.AddItem = function()
	{
		if( search.val().trim().length <= 0 )
		{
			return false;
		}
		
		var rows = table.find('tbody').find('tr').length;
		var row = item_tpl.replace(/{index}/g, rows)
							.replace('{number}', rows + 1)
							.replace(/{name}/g, search.val());
		var exists = false;
		if( window.mb_product )
		{
			exists 	= $this.ItemExists(mb_product.product_id);
			row 	= row.replace(/{id}/g, mb_product.product_id)
							.replace('{code}', mb_product.product_code)
							.replace('{price}', mb_product.product_price)
							.replace('{total}', mb_product.product_price)
							.replace('{real_qty}', mb_product.product_quantity);
		}
		else
		{
			row = row.replace(/{id}/g, '')
						.replace('{code}', '')
						.replace('{price}', '0.00')
						.replace('{total}', '0.00');
		}
		
		if( exists )
		{
			//##update product qty
			var qty = jQuery(exists).find('.item-qty').val();
			qty++;
			jQuery(exists).find('.item-qty').val(qty);
			$this.CalculateRowTotal(jQuery(exists));
		}
		else
		{
			if( window.mb_product )
			{
				row = jQuery(row);
				row.get(0).dataset.qty = mb_product.product_quantity;
				var ps = row.find('.price-selector').get(0);
				ps.dataset.price_1 	= mb_product.product_price;
				ps.dataset.price_2 	= mb_product.product_price_2;
				ps.dataset.price_3 	= mb_product.product_price_3;
				ps.dataset.price_4 	= mb_product.product_price_4;
				ps.dataset.cost		= mb_product.product_cost;
			}
			table.find('tbody').append(row);
		}
		search.val('').focus();
		$this.CalculateTotals();
	};
	this.RemoveItem = function(e)
	{
		this.parentNode.parentNode.remove();
		return false;
	};
	this.OnQtyChanged = function(e)
	{
		if( e.type == 'change' || e.type == 'keyup' )
		{
			if( e.type == 'keyup' && e.keyCode != 13 )
			{
				return false;
			}
			
			var row = jQuery(this).parents('tr:first');
			$this.CalculateRowTotal(row);
			$this.CalculateTotals();
		}
	};
	this.OnPriceChanged = function(e)
	{
		var row = jQuery(this).parents('tr:first');
		if( e.keyCode == 13 )
		{
			$this.CalculateRowTotal(row);
			$this.CalculateTotals();
			return false;
		}
		
		$this.CalculateRowTotal(row);
		$this.CalculateTotals();
		return true;
	};
	this.CalculateRowTotal = function(row)
	{
		var qty = parseInt(row.find('.item-qty:first').val());
		var price = parseFloat(row.find('.item-price:first').val());
		var tax = 0;//parseFloat(row.find('.item-tax:first').val());
		var total = (qty * price) + tax;
		row.find('.item-total:first').html(total.toFixed(2));
		
		return total;
	};
	this.CalculateTotals = function()
	{
		var rows = jQuery('#quote-table tbody tr');
		var subtotal = 0;
		var tax = 0;
		var total = 0;
		jQuery.each(rows, function(i, row)
		{
			subtotal += $this.CalculateRowTotal(jQuery(row));
		});
		total = subtotal;
		if( $this.tax_rate > 0 )
		{
			tax = subtotal * $this.tax_rate;
			total = subtotal + tax;
			jQuery('#total_tax').val(tax);
		}
		jQuery('#quote-subtotal').html(subtotal.toFixed(2));
		jQuery('#quote-tax').html(tax.toFixed(2));
		jQuery('#quote-total').html(total.toFixed(2));

		return total;
	}
	this.Save = function()
	{
		var customer = jQuery('#customer').val().trim();
		var nit		= parseInt(jQuery('#nit_ruc_nif').val().trim());
		if( customer.length <= 0 )
		{
			alert(lt.modules.quotes.locale.INVALID_CUSTOMER_NAME);
			return false;
		}
		if( nit.length <= 0 )
		{
			alert(lt.modules.quotes.locale.INVALID_CUSTOMER_NIT);
			return false;
		}
		if( isNaN(nit) )
		{
			alert(lt.modules.quotes.locale.INVALID_CUSTOMER_NIT);
			return false;
		}
		var params = form.serialize();
		jQuery.post('index.php', params, function(res)
		{
			if( res.status == 'ok' )
			{
				alert(res.message);
				window.location.reload();
			}
			else
			{
				alert(res.error);
			}
		});
	};
	this.OnTaxChange = function(e)
	{
		var op = jQuery(this).find('option:selected').get(0);
		if( !op.dataset.tax )
		{
			$this.tax_rate = 0;
			jQuery('#tax_rate').val($this.tax_rate);
			$this.CalculateTotals();
			return true;
		}
		$this.tax_rate = parseFloat(op.dataset.tax) / 100;
		jQuery('#tax_rate').val($this.tax_rate);
		$this.CalculateTotals();
	};
	function SetPrice(e)
	{
		var price = this.rel;
		var td = jQuery(this).parents('td');
		var input_price = td.find('.item-price');
		input_price.val(price);
		$this.OnPriceChanged.call(input_price.get(0), e);
		td.find('.price-selector').popover('hide');
	}
	this.OnSelectPriceClicked = function()
	{
		if( !this.dataset.originalTitle )
		{
			var ul = jQuery('<div class="item-prices" />');
			var a = null;
			if( this.dataset.cost )
			{
				a = jQuery('<a href="javascript:;" rel="'+this.dataset.cost+'" class="">0.- '+this.dataset.cost+' (costo)</a><br/>');
				ul.append(a);
			}
			if( this.dataset.price_1 )
			{
				a = jQuery('<a href="javascript:;" rel="'+this.dataset.price_1+'" class="">1.- '+this.dataset.price_2+'</a><br/>');
				ul.append(a);
			}
			if( this.dataset.price_2 )
			{
				a = jQuery('<a href="javascript:;" rel="'+this.dataset.price_2+'" class="">2.- '+this.dataset.price_2+'</a><br/>');
				ul.append(a);
			}
			if( this.dataset.price_3 )
			{
				a = jQuery('<a href="javascript:;" rel="'+this.dataset.price_3+'" class="">3.- '+this.dataset.price_3+'</a><br/>');
				ul.append(a);
			}
			if( this.dataset.price_4 )
			{
				a = jQuery('<a href="javascript:;" rel="'+this.dataset.price_4+'" class="">4.- '+this.dataset.price_4+'</a><br/>');
				ul.append(a);
			}
			jQuery(this).popover({
				html: true, 
				title: 'Precios', 
				content: ul.get(0).outerHTML
			});
		}
		
		jQuery(this).popover('show');
		return false;
	};
	function setEvents()
	{
		jQuery('#search_product').keyup(function(e)
		{
			if( e.keyCode != 13)
				return false;
			
			$this.AddItem();
		});
		jQuery('#btn-add-item').click($this.AddItem);
		jQuery(document).on('click', '.remove-item', $this.RemoveItem);
		jQuery(document).on('keyup change keydown', '.item-qty', $this.OnQtyChanged);
		jQuery(document).on('keyup keydown', '.item-price', $this.OnPriceChanged);
		jQuery(document).on('click', '.price-selector', $this.OnSelectPriceClicked);
		jQuery(document).on('click', '.item-prices a', SetPrice);
		form.submit(function()
		{
			if(this.customer_id.value <= 0 )
			{
				alert(lt.modules.quotes.locale.NEED_SELECT_CUSTOMER);
				return false;
			}
			if( $this.CalculateTotals() <= 0 )
			{
				alert(lt.modules.quotes.locale.INVALID_QUOTE_AMOUNT);
				return false;
			}
			return true;
		});
		//##add event to apply taxes
		jQuery('#tax_id').change($this.OnTaxChange);
	};
	setEvents();
}
//##define the customer search select callback
window.select_callback = function(obj)
{
	jQuery('#search-customer-modal').modal('hide');
	var params = 'mod=customers&ajax=1&task=get_customer&id=' + obj.id;
	jQuery.post('index.php', params, function(res)
	{
		//console.log(res.customer.customer_id);
		if( res.status == 'ok' )
		{
			jQuery('#customer_id').val(res.customer.customer_id);
			jQuery('#customer').val(res.customer.first_name + ' ' + res.customer.last_name );
		}
		else
		{
			alert(res.error);
		}
	});
};
jQuery(function()
{
	window.mb_quote = new SBQuote();
	window.completion = new SBCompletion({
		input: document.getElementById('search_product'),
		url: lt.modules.quotes.completion_product_url,
		loading_gif: lt.baseurl + '/images/spin.gif',
		callback: function(sugesstion)
		{
			//window.mb_product = jQuery(sugesstion).data('obj');
            window.mb_product = sugesstion.dataset;
		}
	});
	window.completion_1 = new SBCompletion({
		input: document.getElementById('customer'),
		url: lt.modules.quotes.completion_customer_url,
		loading_gif: lt.baseurl + '/images/spin.gif',
		callback: function(sugesstion)
		{
			//var data = jQuery(sugesstion).data('obj');
            var data = sugesstion.dataset;
			jQuery('#customer_id').val(data.customer_id);
		}
	});
	jQuery('#btn-search-customer').click(function()
	{
		jQuery('#search-customer-modal iframe').get(0).contentDocument.location.reload();
		jQuery('#search-customer-modal').modal('show');
		return false;
	});
});