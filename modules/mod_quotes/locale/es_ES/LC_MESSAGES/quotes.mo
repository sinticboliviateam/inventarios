��    |      �  �   �      x
     y
     �
     �
     �
     �
     �
     �
     �
     �
  !   �
      �
          #     ,     @     M  	   T     ^     d     j     o          �     �     �     �     �     �  	   �     �     �     �       	             '  
   ,     7     =     K     [     h     �     �  	   �     �     �     �     �     �     �     �     �     �     �  	   �            	   "  
   ,  
   7     B     P     ^  	   e     o  	   }     �     �     �     �     �     �  
   �     �  
   �                      
        $     )  
   /  !   :  6   \     �     �     �     �     �          /     J     j     p     w     ~     �     �  
   �     �  
   �     �     �  '   �        :   $  3   _  ;   �  1   �  0        2  !   Q     s     �     �  H   �          '     <     M     Y  	  i     s          �     �     �     �  	   �     �     �  %   �  &        7     >  (   M     v     �     �     �     �     �     �     �     �     �     �               #     C     L     R     Y     `  
   r     }     �     �     �     �     �     �      �  $   �          ,     =     @     D     K     R     [     l     �     �     �     �     �  	   �     �     �     �     �            
        '     7     E     ]     e     t     �     �     �     �     �     �     �     �        
     	     	        &  *   7  >   b     �     �     �     �          )     G  +   f     �     �     �     �     �     �     �     �     �            @   #      d  B   �  A   �  B   
  9   M  ;   �  +   �  +   �          ;     S  D   k     �     �     �     �     �     7       g   l       M          T   X   W   n   !   f   ?   ]       h       a         /       -   ;       )   t   C       u       B       c       8      #      R   D      5       N          F   x         J   ,   &   $       :       ^   i   V   1   >   P   Y   %   |   y   @       p       o   	       `   *      L               w       G   s      <   0   I          
      q      Q                E          e      b      d           H               4              '       k       3   m   (   9                  S          U          O      2   .      K   A   {   v   z   Z   =   +   _   \       [   "           6       r             j    -- coupon -- -- sale type -- -- store -- -- taxes -- Actions Add item Advance: Amount Apply Promo Are you sure to delete the quote? Are you sure to void this quote? Back Bill Now Billing Information Billing Name Cancel Cash Sale City: Close Code Create Customer Create Quote Create customer Created Credit Sale Customer Customer Name Customer module does not exists Customer: Date Date: Delete Delete Quote Discount: Download PDF Edit Edit Quote Email Email subject General Details Grand Total: Invalid destination email. Invalid quote identifier Manage Quote New Quote No No. Notes: Price Print Print Prices Print Quote Product Qty Quantity Quotation Quote Comments Quote Content Quote No. Quote No.: Quote Num. Quote Preview Quote message Quotes Reference Register Sale Sale Type Sales Representative Save Search Customer Search customer Search product Search quote Sell Quote Send Quotation Send Quote Sent Sold Status Store Sub Total: Tax: Taxes Telephone: The invoice identifier is invalid The invoices module does not exists, please install it The quote does not exists The quote does not exists. The quote has been created The quote has been deleted The quote has been sent. The quote has been sold The quote has been updated The quote identifier is invalid Total Total: Unknow Validity Time View View All Quotes View Quote Void Void Quote Yes You cant do this operation You dont have an store assigned to sell You dont have any store assigned You dont have enough permission to complete this operation You dont have enough permissions for the operation. You dont have enough permissions to complete this operation You dont have enough permissions to print quotes. You dont have permission to complete this action You must enter a customer name You need to enter a customer name You need to select a customer You need to start a session You need to start session Your quote has no amount, please review your items prices and quantities index.php?mod=invoices index.php?mod=quotes quotation_%s.pdf quote-%d-%d quote-%d-%d.pdf Project-Id-Version: Modulo de Cotizaciones - Mono Business
POT-Creation-Date: 2018-04-03 11:04-0400
PO-Revision-Date: 2018-04-03 11:04-0400
Last-Translator: J. Marcelo Aviles Paco <maviles@sinticbolivia.net>
Language-Team: Sintic Bolivia <info@sinticbolivia.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _;__;_e
X-Poedit-SearchPath-0: ../../..
 -- cupon -- -- tipo de venta -- -- tienda -- -- impuestos -- Acciones Adicionar item Adelanto: Monto Aplicar promocion Estas seguro de borrar la cotizacion? Esta seguro de anular esta cotizacion? Volver Facturar Ahora Informaci&oacute;n de Facturaci&oacute;n Nombre Facturaci&oacute;n Cancelar Venta en Efectivo Ciudad: Cerrar C&oacute;digo Crear Cliente Crear Cotizacion Crear cliente Creado Venta a Credito Cliente Nombre Cliente El modulo de clientes no existe Cliente: Fecha Fecha: Borrar Borrar Cotizacion Descuento: Descargar PDF Editar Editar Cotizacion Correo Asunto: Detalles Generales Total: Destinatario de correo invalido. Identificador de cotizacion invalido Administrar Cotizaciones Nueva Cotizacion No No. Notas: Precio Imprimir Imprimir Precios Imprimir Cotizacion Producto Cant Cantidad Cotizaci&oacute;n Comentarios Contenido Cotizacion No. Cotizacion No.: Cotizacion Num. Vista Previa Cotizacion Mensaje: Cotizaciones Referencia Registrar Venta Tipo de Venta Representante de Ventas Guardar Buscar Cliente Buscar cliente Buscar producto Buscar cotizacion Vender Cotizaci&oacute;n Enviar Cotizaci&oacute;n Enviar Cotizacion Enviado Vendido Estado Tienda Sub Total: Impuesto: Impuestos Tel&eacute;fono: El identificador de la factura es invalido El modulo de facturacion no existe, porfavor instale el modulo La cotizacion no existe La cotizacion no existe La cotizacion a sido creado La cotizacion ha sido borrada La cotizacion a sido enviada. La cotizacion ha sido vendida La cotizacion se a actualizado El identificar de la cotizacion es invalido Total Total: Desconocido Tiempo de Validez Ver Ver todas las cotizaciones Ver Cotizaci&oacute;n Anular Anular Cotizacion Si No puedes hacer esto No tiene asignada ninguna tienda o regional asignada para vender No tiene asignada ninguna tienda No tiene suficientes permisos para completar esta operaci&oacute;n No tiene suficientes permisos para realizar esta operaci&oacute;n No tiene suficientes permisos para completar esta operaci&oacute;n No tiene suficientes permisos para imprimir cotizaciones. No tiene permisos suficientes para completar esta operacion Necesita ingresar un nombre para el cliente Necesita ingresar un nombre para el cliente Necesita seleccionar un cliente Necesita iniciar sesion Necesita iniciar sesion La cotizacion no tiene monto, porfavor revise sus items y cantidades index.php?mod=invoices index.php?mod=quotes cotizacion_%s.pdf cotizacion-%d-%d cotizacion-%d-%d.pdf 