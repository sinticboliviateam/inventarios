<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Classes\SB_Factory;

SB_Language::loadLanguage(LANGUAGE, 'quotes', dirname(__FILE__) . SB_DS . 'locale');
SB_Module::RunSQL('quotes');
$dbh = SB_Factory::getDbh();
$permissions = array(
		array('group' => 'quotes', 'permission' => 'manage_quotes', 'label'	=> __('Manage Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'create_quote', 'label'	=> __('Create Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'edit_quote', 'label'	=> __('Edit Quote','quotes')),
		array('group' => 'quotes', 'permission' => 'delete_quote', 'label'	=> __('Delete Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'print_quote', 'label'	=> __('Print Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'void_quote', 'label'	=> __('Void Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'quote_sell', 'label'	=> __('Sell Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'quote_view', 'label'	=> __('View Quote', 'quotes')),
		array('group' => 'quotes', 'permission' => 'quote_view_all', 'label'	=> __('View All Quotes', 'quotes')),
);
sb_add_permissions($permissions);
