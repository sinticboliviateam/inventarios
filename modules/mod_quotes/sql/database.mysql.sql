CREATE TABLE IF NOT EXISTS mb_quotes(
	quote_id 				integer not null primary key auto_increment,
	code					varchar(128),
	name					varchar(256),
	store_id 				integer not null,
	sequence				bigint not null,
	items					integer,
	tax_id					integer,
	tax_rate				decimal(10,2),
	subtotal				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	notes					text,
	status					varchar(128),
	user_id					integer,
	customer_id				integer,
	quote_date				datetime,
	last_modification_date 	datetime,
	creation_date 			datetime
);
CREATE TABLE IF NOT EXISTS mb_quote_meta(
	meta_id					integer not null auto_increment primary key,
	quote_id				integer not null,
	meta_key				varchar(128),
	meta_value				text,
	creation_date			datetime
);
CREATE TABLE IF NOT EXISTS mb_quote_items(
	item_id					integer not null primary key auto_increment,
	quote_id				integer not null,
	product_id				integer not null,
	item_code				varchar(64),
	name					varchar(250),
	quantity				integer,
	batch					varchar(64),
	expiration_date			date,
	price					decimal(10,2),
	subtotal				decimal(10,2),
	tax_rate				decimal(10,2),
	total_tax				decimal(10,2),
	discount				decimal(10,2),
	total					decimal(10,2),
	status					varchar(128),
	last_modification_date 	datetime,
	creation_date 			datetime
);
