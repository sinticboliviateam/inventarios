<?php
use SinticBolivia\SBFramework\Classes\SB_Request;

$print_prices = (int)$quote->_print_prices;
?>
<?php if( SB_Request::getVar('view') == 'print' ): ?>
<html>
<head>
	<link rel="stylesheet" href="<?php print MOD_QUOTES_URL ?>/css/quote-print.css" />
</head>
<body>
<?php else: ?>
<?php endif;?>
<style>
@page 
{
    size: Letter/*A4*/;
    margin: 2cm 2cm 2cm 2cm;
}
#header{position:fixed;top:-1.8cm;height:50px;overflow:hidden;border:0px solid #000;font-size:12px;}
#footer{height:50px;text-align:center;position:fixed;bottom:-1.8cm;font-size:10px;border-top:1px solid #000;}
#quote-container{font-family: Arial, Verdana, Helvetica;font-size:12px;margin:0;}
#quote-table{font-size:12px;border-collapse: collapse;width:100%;}
.col{border:1px solid #000;padding:2px;}
th.col{background-color:#579d1c;color:#fff;text-align:center;}
.col-num{width:8%;text-align:center;}
.col-code{width:10%;text-align:center;}
.col-name{width:51%;}
.col-qty{width:8%;text-align:center;}
.col-price{width:11%;text-align:right;}
.col-total{width:11%;text-align:right;}

/*
#quote-table tbody tr:nth-child(2n) {
    background: #f9f9f9 none repeat scroll 0 0;
}
#quote-table tbody tr:nth-child(2n+1) {
    background: #fff none repeat scroll 0 0;
}
*/
</style>
<?php
$logo 		= isset($business->business_logo) ? UPLOADS_DIR . SB_DS . $business->business_logo : null;
$logo_url 	= isset($business->business_logo) ? UPLOADS_URL . '/' . $business->business_logo : null;

//die(file_get_contents($logo_url));
//print_r(getimagesize($logo));
?>
<div id="header">
	<?php if( $logo && is_file($logo) ): ?>
	<table style="width:100%;">
	<tr>
		<td style="width:50%;">
			<img src="<?php print $logo; ?>" style="height:48px;" />
		</td>
		<td style="width:50%;"></td>
	</tr>
	</table>
	<?php endif; ?>
</div>
<div id="quote-container">
	<h1 style="text-align:center;font-size:25px;"><?php _e('Quotation', 'quotes'); ?></h1>
	<table style="width:100%;">
	<tr>
		<td style="width:65%;">
			<table style="width:100%;">
			<tr>
				<th style="width:100px;text-align:left;"><?php _e('Customer:', 'quotes'); ?></th>
				<td style="width:auto;">
					<?php 
					if( $quote->customer->company )
					{
						print $quote->customer->company; 
					} 
					else
					{
						printf("%s %s", $quote->customer->first_name, $quote->customer->last_name);
					}
					?>
				</td>
			</tr>
			<tr>
				<th style="width:100px;text-align:left;"><?php _e('NIT/RUC/NIF:', 'quotes'); ?></th>
				<td style="width:auto;">
					<?php print $quote->customer->_nit_ruc_nif?>
				</td>
			</tr>
			</table>
		</td>
		<td style="width:35%;">
			<table style="width:100%;">
			<tr>
				<th style="width:120px;text-align:left;"><?php _e('Quote No.:', 'quotes'); ?></th>
				<td style="width:130px;"><?php print sb_fill_zeros($quote->sequence); ?></td>
			</tr>
			<tr>
				<th style="width:120px;text-align:left;"><?php _e('Date:', 'quotes'); ?></th>
				<td style="width:130px;"><?php print sb_format_date(time()); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<table id="quote-table">
	<thead>
	<tr>
		<th class="col col-num"><?php _e('No.', 'quotes'); ?></th>
		<th class="col col-code"><?php _e('Code', 'quotes'); ?></th>
		<th class="col col-name"><?php _e('Product', 'quotes'); ?></th>
		<th class="col col-qty"><?php _e('Qty', 'quotes'); ?></th>
		<th class="col col-price"><?php _e('Price', 'quotes'); ?></th>
		<th class="col col-total"><?php _e('Total', 'quotes'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1; foreach($quote->GetItems() as $item): ?>
	<tr>
		<td class="col col-num"><?php print $i; ?></td>
		<td class="col col-code"><?php print $item->item_code; ?></td>
		<td class="col col-name"><?php print ($item->name); ?></td>
		<td class="col col-qty"><?php print $item->quantity; ?></td>
		<td class="col col-price"><?php print $print_prices ? sb_number_format($item->price) : '0.00'; ?></td>
		<td class="col col-total"><?php print $print_prices ? sb_number_format($item->total) : '0.00'; ?></td>
	</tr>
	<?php $i++; endforeach; ?>
	</tbody>
	<tfoot>
	<?php /*
	<tr>
		<td colspan="4" style="border:1px solid #fff;border-right:1px solid #000;">&nbsp;</td>
		<td style="text-align:right;font-weight:bold;" class="col"><?php _e('Tax:', 'quotes'); ?></td>
		<td style="text-align:right;" class="col"><?php print $quote->total_tax; ?></td>
	</tr>
	*/?>
	<tr>
		<td colspan="4" style="border:1px solid #fff;border-right:1px solid #000;">&nbsp;</td>
		<td style="text-align:right;font-weight:bold;" class="col"><?php _e('Total:', 'quotes'); ?></td>
		<td style="text-align:right;" class="col"><?php print sb_number_format($quote->total); ?></td>
	</tr>
	</tfoot>
	</table>
	<p>
		<b><?php _e('Notes:', 'quotes'); ?></b> <?php print html_entity_decode($quote->notes); ?>
	</p>
	<br/>
	<div><?php print SB_Request::getString('view') == 'print' ? html_entity_decode($quote->_content) : html_entity_decode($quote->_content); ?></div>
	<br/><br/>
	<table style="width:100%;">
	<tr>
		<td style="width:60%;">&nbsp;</td>
		<td style="width:40%;">
			<p style="border-top:1px solid #000;text-align:center;">
				<?php _e('Sales Representative', 'quotes'); ?>
			</p>
		</td>
	</tr>
	</table><br/>
</div>
<div style="display:none;">
	<script type="text/javascript">
	function mb_print()
	{
		this.print();
	}
	</script>
</div>
<div id="footer">
	<?php print @$business->business_address; ?>, 
	<?php _e('Telephone:', 'quotes'); ?> <?php print @$business->business_phone; ?>, 
	<?php print @$business->business_mobile_telephone; ?><br/>,
	<?php _e('City:', 'quotes'); ?> <?php print @$business->business_city; ?>
</div>
<?php if(SB_Request::getString('view') == 'print' ): ?>
</body>
</html>
<?php endif; ?>
