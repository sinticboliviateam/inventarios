<?php
?>
<div class="wrap">
	<h2>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php _e('Quotes', 'quotes'); ?></div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="page-buttons">
					<a href="<?php print $this->Route('index.php?mod=quotes&view=new'); ?>" class="btn btn-primary">
						<?php _e('New Quote', 'quotes'); ?>
					</a>
				</div>
			</div>
		</div>
	</h2>
	<div class="row">
		<form action="" method="get">
			<input type="hidden" name="mod" value="quotes" />
			<div class="col-md-7">
				<div class="form-group">
					<input type="text" name="keyword" value="" class="form-control" />
				</div>
			</div>
			<button type="submit" id="btn-search-quote" class="btn btn-secondary" title="<?php _e('Search quote', 'quotes'); ?>">
				<span class="glyphicon glyphicon-search"></span>
			</button>
		</form>
	</div>
	<table class="table table-condensed">
	<thead>
	<tr>
		<th>#</th>
		<th><?php _e('Quote Num.', 'quotes'); ?></th>
		<th><?php _e('Date', 'quotes'); ?></th>
		<th><?php _e('Store', 'quotes'); ?></th>
		<th><?php _e('Reference', 'quotes'); ?></th>
		<th><?php _e('Amount', 'quotes'); ?></th>
		<th><?php _e('Customer', 'quotes'); ?></th>
		<th><?php _e('Status', 'quotes'); ?></th>
		<th><?php _e('Actions', 'quotes'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if( is_array($quotes) ): $i = 1;foreach($quotes as $q): ?>
	<tr>
		<td class="text-center"><?php print $i; ?></td>
		<td class="text-center"><?php print $q->sequence; ?></td>
		<td class="text-right"><?php print sb_format_datetime($q->quote_date); ?></td>
		<td><?php print $q->store_name; ?></td>
		<td><?php print $q->name; ?></td>
		<td class="text-right"><?php print $q->total; ?></td>
		<td><?php print $q->customer; ?></td>
		<td class="text-center">
			<?php
			$class = 'danger';
			$label = __('Unknow', 'quotes');
			if( $q->status == 'void' )
			{
				$class = 'danger';
				$label = __('Void', 'quotes');
			}
			if( $q->status == 'created' )
			{
				$class = 'info';
				$label = __('Created', 'quotes');
			}
			if( $q->status == 'sent' )
			{
				$class = 'primary';
				$label = __('Sent', 'quotes');
			}
			if( $q->status == 'sold' )
			{
				$class = 'success';
				$label = __('Sold', 'quotes');
			}
			?>
			<label class="label label-<?php print $class; ?>"><?php print $label; ?></label>
		</td>
		<td>
			<a href="<?php print $this->Route('index.php?mod=quotes&task=void&id='.$q->quote_id); ?>" 
				class="confirm btn btn-default btn-xs" 
				data-message="<?php _e('Are you sure to void this quote?', 'invoices'); ?>" title="<?php _e('Void', 'quotes'); ?>">
				<span class="glyphicon glyphicon-remove"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=quotes&view=view&id='.$q->quote_id); ?>" title="<?php _e('View', 'quotes'); ?>"
				class="btn btn-default btn-xs">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=quotes&view=edit&id='.$q->quote_id); ?>" title="<?php _e('Edit', 'quotes'); ?>"
				class="btn btn-default btn-xs">
				<span class="glyphicon glyphicon-pencil"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=quotes&task=print&id='.$q->quote_id); ?>" 
				class="print-quote btn btn-default btn-xs" target="_blank" 
				title="<?php _e('Print', 'quotes'); ?>">
				<span class="glyphicon glyphicon-print"></span>
			</a>
			<a href="<?php print $this->Route('index.php?mod=quotes&task=delete&id='.$q->quote_id); ?>" 
				class="confirm btn btn-default btn-xs"
				data-message="<?php _e('Are you sure to delete the quote?', 'quotes'); ?>" title="<?php _e('Delete', 'quotes'); ?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
	<?php $i++; endforeach; endif; ?>
	</tbody>
	</table>
	<p>
		<?php lt_pagination($this->Route('index.php?'.$_SERVER['QUERY_STRING']), $total_pages, $current_page); ?>
	</p>
	<script>
	jQuery(function()
	{
		/*
		jQuery('.print-quote').click(function()
		{
			if( jQuery('#quote-iframe').length > 0 )
			{
				jQuery('#quote-iframe').remove();
			}
			var iframe = jQuery('<iframe id="quote-iframe" src="'+this.href+'" style="display:none;"></iframe>');
			//window.iframe = iframe;
			jQuery('body').append(iframe);
			try
			{
				iframe.load(function()
				{
					if(iframe.get(0).contentWindow.mb_print)
						iframe.get(0).contentWindow.mb_print();
				});
				
			}
			catch(e)
			{
				alert(e);
			}
			
			return false;
		});
		*/
	});
	</script>
</div>