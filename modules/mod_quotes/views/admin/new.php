<?php
//print_r($quote);
?>
<div class="wrap">
	<h2 id="page-title">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><?php print $title; ?></div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="page-buttons">
					<a href="<?php print $this->Route('index.php?mod=quotes'); ?>" class="btn btn-danger">
						<span class="glyphicon glyphicon-remove"></span> <?php _e('Cancel', 'quotes'); ?>
					</a>
					<?php if( !isset($quote) || ( isset($quote) && $quote->status != 'sold' ) ): ?>
					<button type="button" class="btn btn-success" onclick="jQuery('#form-quote').submit();">
						<span class="glyphicon glyphicon-save"></span> <?php _e('Save', 'quotes'); ?>
					</button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</h2>
	<form id="form-quote" action="" method="post">
		<input type="hidden" name="mod" value="quotes" />
		<input type="hidden" name="task" value="save" />
		<input type="hidden" id="tax_rate" name="tax_rate" value="<?php print isset($quote) ? $quote->tax_rate : 0.00; ?>" />
		<input type="hidden" id="total_tax" name="total_tax" value="<?php print isset($quote) ? $quote->total_tax : 0.00; ?>" />
		<?php if( isset($quote) ): ?>
		<input type="hidden" name="quote_id" value="<?php print $quote->quote_id; ?>" />
		<?php endif; ?>
		<div>
			<?php if( isset($quote) ): ?>
			<a href="<?php print $this->Route('index.php?mod=quotes&view=print&id='.$quote->quote_id); ?>" class="btn btn-warning" target="_blank">
				<span class="glyphicon glyphicon-print"></span> <?php _e('Print', 'quotes'); ?>
			</a>
			<a href="javascript:;" class="btn btn-success" data-toggle="modal" data-target="#send-quote-form">
				<span class="glyphicon glyphicon-envelope"></span> <?php _e('Send Quote', 'quotes'); ?>
			</a>
			<a href="<?php print $this->Route('index.php?mod=quotes&task=downloadpdf&id='.$quote->quote_id); ?>" class="btn btn-primary">
				<span class="glyphicon glyphicon-download"></span> <?php _e('Download PDF', 'quotes'); ?>
			</a>
			<?php endif; ?>
			<?php if( isset($quote) && $quote->status != 'sold' ): ?>
			<a href="javascript:;" class="btn btn-info" data-toggle="modal" data-target="#billing-info">
				<span class="glyphicon glyphicon-shopping-cart"></span> <?php _e('Register Sale', 'quotes'); ?>
			</a>
			<?php endif; ?>
			<?php b_do_action('quote_buttons', isset($quote) ? $quote : null); ?>
		</div>
		<div class="clearfix"></div>
		<fieldset>
			<legend><?php _e('General Details', 'quotes'); ?></legend>
			<div class="rows">
				<div class="col-xs-12 col-md-7">
					<div class="row">
						<div class="col-md-12">
							<label><?php _e('Reference', 'quotes'); ?></label>
							<input type="text" name="name" value="<?php print isset($quote) ? $quote->name : ''; ?>" 
								class="form-control" />
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="control-label"><?php _e('ID', 'quotes'); ?></label>
								<input type="text" id="customer_id" name="customer_id" value="<?php print isset($quote) ? $quote->customer_id : ''; ?>" class="form-control" />
							</div>
						</div>
						<div class="col-xs-10 col-md-7">
							<div class="form-group">
								<label class="control-label">
									<?php _e('Customer Name', 'quotes'); ?>
								</label>
								<input type="text" id="customer" name="customer" 
									value="<?php print isset($quote) ? $quote->customer->first_name . ' ' . $quote->customer->last_name : ''; ?>" 
									class="form-control" autocomplete="false" />
							</div>
						</div>
						<div class="">
							<div class="form-group">
								<label>&nbsp;</label>
								<div>
									<a href="javascript:;" id="btn-search-customer" class="btn btn-default" title="<?php _e('Search customer', 'quotes'); ?>">
										<span class="glyphicon glyphicon-search"></span>
									</a>
									<a href="javascript:;" id="btn-add-customer" class="btn btn-default" title="<?php _e('Create customer', 'quotes'); ?>"
										data-toggle="modal" data-target="#create-customer-modal">
										<span class="glyphicon glyphicon-user"></span>
									</a>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<p id="customer-address"></p>
						<div class="col-md-3"><span id="customer-rfc"></span></div>
						<div class="col-md-3"><span id="customer-email"></span></div>
						<div class="col-md-3"><span id="customer-phone"></span></div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label><?php _e('Store', 'quotes'); ?></label>
								<select name="store_id" class="form-control">
									<option value="-1"><?php _e('-- store --', 'quotes'); ?></option>
									<?php foreach($stores as $s): ?>
									<option value="<?php print $s->store_id; ?>" <?php print isset($quote) && $quote->store_id == $s->store_id ? 'selected' : ''; ?>>
										<?php print $s->store_name; ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php if( MB()->HasFeature('coupons') ): ?>
						<div class="col-md-4">
							<div class="form-group">
								<label><?php _e('Apply Promo', 'quotes'); ?></label>
								<select name="coupon_id" class="form-control">
									<option value="-1"><?php _e('-- coupon --'); ?></option>
									<?php foreach($coupons as $c): ?>
									<option value="<?php print $c->coupon_id; ?>"><?php print $c->description; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php endif; ?>
						<?php if( MB()->HasFeature('taxes') ): ?>
						<div class="col-md-4">
							<div class="form-group">
								<label><?php _e('Taxes', 'quotes'); ?></label>
								<select id="tax_id" name="tax_id" class="form-control">
									<option value="-1"><?php _e('-- taxes --', 'quotes'); ?></option>
									<?php foreach($taxes as $t): ?>
									<option value="<?php print $t->tax_id; ?>" <?php print isset($quote) && $quote->tax_id == $t->tax_id ? 'selected' : ''; ?> data-tax="<?php print $t->rate; ?>">
										<?php printf("%s (%.2f)", $t->name, $t->rate); ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php endif; ?>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label><?php _e('Validity Time', 'quotes'); ?></label>
							<input type="number" name="meta[_validity_time]" value="5" class="form-control" />
						</div>
						<div class="col-md-4">
							<label><?php _e('Print Prices', 'quotes'); ?></label>
							<label>
								<input type="radio" name="meta[_print_prices]" value="1" 
									<?php print isset($quote) && $quote->_print_prices ? 'checked' : ''; ?> />
								<?php _e('Yes', 'quotes'); ?>
							</label>
							<label>
								<input type="radio" name="meta[_print_prices]" value="0" 
									<?php print isset($quote) && !$quote->_print_prices ? 'checked' : ''; ?> />
								<?php _e('No', 'quotes'); ?>
							</label>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="text-right">
						<?php _e('Quote No.', 'quotes');?><br/>
						<span id="quote-number" class="text-red">
							<b><?php print isset($quote) ? sb_fill_zeros($quote->sequence) : 'C-000??'; ?></b>
						</span>
					</div>
					<table id="table-totals" class="totals-table" style="width:100%;">
					<tr>
						<th class="text-right"><?php _e('Sub Total:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-subtotal"><?php print isset($quote) ? $quote->subtotal : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Discount:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-discount"><?php print isset($quote) ? $quote->discount : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><?php _e('Tax:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-tax"><?php print isset($quote) ? $quote->total_tax : 0.00; ?></span></td>
					</tr>
					<tr>
						<th class="text-right"><b><?php _e('Advance:', 'quotes'); ?></b></th>
						<td class="text-right"><b><span id="invoice-total">0.00</span></b></td>
					</tr>
					<tr id="row-total">
						<th class="text-right"><?php _e('Grand Total:', 'quotes'); ?></th>
						<td class="text-right"><span id="quote-total"><?php print isset($quote) ? $quote->total : 0.00; ?></span></td>
					</tr>
					</table>
				</div>
			</div>
		</fieldset><br/>
		<div class="form-group">
			<div class="input-group">
				<input type="text" id="search_product" name="search_product" value="" placeholder="<?php _e('Search product', 'invoice'); ?>" class="form-control" />
				<span class="input-group-btn">
		        	<button id="btn-add-item" class="btn btn-default" type="button"><?php _e('Add item', 'quotes'); ?></button>
		      	</span>
			</div>
		</div>
		<div class="table-responsive">
			<table id="quote-table" class="table table-condensed table-striped">
			<thead>
			<tr>
				<th class="text-center"><?php _e('No.', 'quotes'); ?></th>
				<th class="text-center"><?php _e('Code', 'quotes'); ?></th>
				<th class="column-product"><?php _e('Product', 'quotes'); ?></th>
				<th class="column-qty"><?php _e('Quantity', 'quotes'); ?></th>
				<th class="column-price text-right"><?php _e('Price', 'quotes'); ?></th>
				<th class="column-total text-right"><?php _e('Total', 'quotes'); ?></th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php if( isset($quote) ): $i = 1; foreach($quote->GetItems() as $item): ?>
			<tr>
				<td><?php print $i; ?></td>
				<td><input type="text" name="items[<?php print $i - 1; ?>][code]" value="<?php print $item->item_code; ?>" class="form-control" /></td>
				<td class="column-product"><input type="text" name="items[<?php print $i - 1; ?>][name]" value="<?php print html_entity_decode($item->name); ?>" class="form-control" /></td>
				<td class="text-center">
					<input type="number" min="1" name="items[<?php print $i - 1; ?>][qty]" value="<?php print $item->quantity ?>" class="form-control item-qty" />
				</td>
				<td class="column-price">
					<input type="text" name="items[<?php print $i - 1; ?>][price]" value="<?php print $item->price ?>" class="form-control item-price" /></td>
				<td class="text-right"><span class="item-total"><?php print $item->total ?></span></td>
				<td><a href="javascript:;" class="remove-item"><span class="glyphicon glyphicon-trash"></span></span></a></td>
			</tr>
			<?php $i++; endforeach; endif; ?>
			</tbody>
			</table><!-- end id="quote-table" -->
		</div>
		<br/>
		<hr class="ht--"/>
		<div class="form-group">
			<label><?php _e('Quote Comments', 'quotes'); ?></label>
			<textarea name="notes" class="form-control"><?php print isset($quote) ? $quote->notes : ''; ?></textarea>
		</div>
		<div class="form-group">
			<label><?php _e('Quote Content', 'quotes'); ?></label>
			<textarea rows="" cols="" name="meta[_content]"><?php print isset($quote) ? $quote->_content : ''; ?></textarea>
		</div>
		<style>
		.inv-item-remove{width:5%;text-align:center;}
		.inv-item-remove img{width:25px;}
		.inv-item-number{width:7.5%;}
		.inv-item-name{width:50%;}
		.inv-item-qty{width:10%;}
		.inv-item-price{width:12.5%;}
		.inv-item-tax{width:12.5%;}
		.inv-item-total{width:15%;}
		.cool-table{background:#fff;}
		.cool-table .body{max-height:250px;}
		.cool-table .inv-item-number, .cool-table .inv-item-qty{text-align:center;}
		.cool-table .inv-item-qty input{text-align:center;}
		.cool-table .inv-item-price input, .cool-table .inv-item-tax input{text-align:right;}
		.cool-table .inv-item-total{text-align:right;}
		.table .column-product{width:45%;}
		.table .column-price input{text-align:right;}
		.sb-suggestions{max-height:200px;width:100%;}
		.sb-suggestions .the_suggestion{padding:5px;display:block;}
		.sb-suggestions .the_suggestion:focus,
		.sb-suggestions .the_suggestion:hover{background:#ececec;text-decoration:none;}, 
		</style>
	</form>
</div>
<!-- Modal -->
<div class="modal fade" id="search-customer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-dialog-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title" id="myModalLabel"><?php _e('Search Customer', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe src="<?php print $this->Route('index.php?mod=customers&view=customers_list&tpl_file=module'); ?>" style="width:100%;height:300px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'customers'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<div class="modal fade" id="create-customer-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-lg" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title"><?php _e('Create Customer', 'customers'); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<iframe src="<?php print $this->Route('index.php?mod=customers&view=new&tpl_file=module'); ?>" style="width:100%;height:300px;" frameborder="0"></iframe>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
	      	</div>
	    </div>
  	</div>
</div>
<?php if( isset($quote) ): ?>
<div class="modal fade" id="send-quote-form" tabindex="-1" role="dialog"><form action="" method="post">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title"><?php _e('Send Quotation', 'quotes'); ?></h4>
	      	</div>
	      	<div class="modal-body">
      			<input type="hidden" name="mod" value="quotes" />
      			<input type="hidden" name="task" value="send" />
      			<?php if( isset($quote) ): ?>
      			<input type="hidden" name="id" value="<?php print $quote->quote_id; ?>" />
      			<?php endif; ?>
      			<div class="form-group">
      				<input type="text" name="email_to" value="<?php print $quote->customer->email; ?>" placeholder="<?php _e('Email', 'quotes'); ?>" class="form-control" />
      			</div>
      			<div class="form-group">
      				<input type="text" name="subject" value="" placeholder="<?php _e('Email subject', 'quotes'); ?>" class="form-control" />
      			</div>
      			<div class="form-group">
      				<textarea rows="" cols="" name="message" placeholder="<?php _e('Quote message', 'quoes'); ?>" class="form-control"></textarea>
      			</div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
	        	<button type="submit" class="btn btn-primary"><?php _e('Send Quote', 'quotes'); ?></button> 
	      	</div>
	    </div>
  	</form></div>
</div>
<div class="modal fade" id="billing-info" tabindex="-1" role="dialog">
	<form action="" method="post">
		<input type="hidden" name="mod" value="quotes" />
		<input type="hidden" name="task" value="sell" />
		<input type="hidden" name="id" value="<?php print $quote->quote_id; ?>" />
		<div class="modal-dialog modal-dialog-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title"><?php _e('Billing Information', 'quotes'); ?></h4>
				</div>
				<div class="modal-body">
					<div class="form-controll">
						<label><?php _e('NIT/RUC/NIF', 'quotes'); ?></label>
						<input type="text" name="billing_number" 
							value="<?php print $quote->customer->_nit_ruc_nif; ?>" class="form-control" required />
					</div>
					<div class="form-controll">
						<label><?php _e('Billing Name', 'quotes'); ?></label>
						<input type="text" name="billing_name" 
							value="<?php print $quote->customer->GetBillingName(); ?>" class="form-control" required />
					</div>
					<div class="form-controll">
						<label><?php _e('Sale Type', 'quotes'); ?></label>
						<select id="sale-type" name="sale_type" class="form-control">
							<option value="-1"><?php _e('-- sale type --', 'mb'); ?></option>
							<option value="credit"><?php _e('Credit Sale', 'mb'); ?></option>
							<option value="cash"><?php _e('Cash Sale', 'mb'); ?></option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'quotes'); ?></button>
					<button type="submit" class="btn btn-primary"><?php _e('Bill Now', 'quotes'); ?></button>
				</div>
			</div>
		</div>
	</form>
</div>
<?php endif; ?>