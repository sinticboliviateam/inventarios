<?php
$link = b_route('index.php?mod=quotes&view=print&id='.$quote->quote_id);
?>
<div class="wrap">
	<p>
		<a href="<?php print b_route('index.php?mod=quotes'); ?>" class="btn btn-danger">
			<?php _e('Back', 'quotes'); ?>
		</a>
	</p>
	<iframe src="<?php print $link; ?>" style="width:100%;height:600px;"></iframe>
	<?php /*
	<div id="print-preview">
		<?php include $tpl; ?>
	</div>
	<div class="no-print" style="text-align:center;">
		<a href="<?php print SB_Route::_('index.php?mod=quotes'); ?>" class="btn btn-danger">
			<span class="glyphicon glyphicon-remove"></span> <?php _e('Back', 'invoices'); ?></a>
		<a href="<?php print SB_Route::_('index.php?mod=quotes&view=print&id='.$quote->quote_id); ?>" target="_blank" class="btn btn-success">
			<span class="glyphicon glyphicon-print"></span> <?php print SBText::_('Print', 'invoices'); ?></a>
	</div>
	*/?>
</div>
