<?php
namespace SinticBolivia\SBFramework\Modules\Storage\Models
{
	use SinticBolivia\SBFramework\Classes\SB_Model;
	use SinticBolivia\SBFramework\Classes\SB_Attachment;
	use SinticBolivia\SBFramework\Classes\SB_AttachmentImage;
	
		
	class AttachmentModel extends SB_Model
	{
		public function Obj2Attachment($obj)
		{
			if( !$obj )
				return null;
	
			$attachment = new SB_Attachment();
			$attachment->SetDbData($obj);
	
			return $attachment;
		}
		/**
		 * Create an attachment image from attachment object
		 *
		 * @param SB_Attachment $obj
		 * @return NULL|\SinticBolivia\SBFramework\Classes\SB_AttachmentImage
		 */
		public function Attachment2Image($obj)
		{
			if( !$obj )
				return null;
	
			$img = new SB_AttachmentImage();
			$img->SetDbData($obj->_dbData);
	
			return $img;
		}
		/**
		 * Get an attachment
		 * 
		 * @param int $id
		 * @return NULL|\SinticBolivia\SBFramework\Classes\SB_Attachment
		 */
		public function GetAttachment($id)
		{
			$obj = new SB_Attachment($id);
			if( !$obj->attachment_id )
				return null;
			return $obj;
		}
	}	
}
