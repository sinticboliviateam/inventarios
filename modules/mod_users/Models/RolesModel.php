<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Modules\Users\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_Role;

/**
 * Description of RolesModel
 *
 * @author marcelo
 */
class RolesModel extends SB_Model
{
    public function Get($id)
    {
        if( !$id )
            throw new Exception(__('Invalid role identifier', 'users'));
        $role = new SB_Role($id);
        if( !$role->role_id )
            throw new Exception(__('The role does not exists', 'users'));
        return $role;
    }
    public function Create(SB_Role $role)
    {
        //##create a new user role
        $data                           = $role->_dbData;
        $data->last_modification_date   = date('Y-m-d H:i:s');
        $data->creation_date            = date('Y-m-d H:i:s');
        $role_id = $this->dbh->Insert('user_roles', $data);
        //TODO: add role as permission
        $data = array(
                'permission' 				=> 'create_role_' . sb_build_slug($role->role_name),
                'label'						=> sprintf($this->__('Assign Role %s'), $role->role_name),
                'last_modification_date'	=> $cdate,
                'creation_date'				=> $cdate
        );
        $this->dbh->Insert('permissions', $data);
        return $role_id;
    }
    public function Update(SB_Role $role)
    {
        $data = $role->_dbData;
       
        //##update the user role
		$this->dbh->Update('user_roles', $data, array('role_id' => $role->role_id));
        
        return $role->role_id;
    }
    public function Save(SB_Role $role)
    {
        if( !$role->role_id )
		{
            return $this->Create($role);
		}
		else
		{
			return $this->Update($role);
		}
        
    }
    public function Delete($roleId)
    {
        $query = "SELECT * FROM user_roles WHERE role_id = $roleId LIMIT 1";
		if( !$this->dbh->Query($query) )
		{
            throw new Exception($this->__('El role does not exists', 'users'));
		}
		$role = $this->dbh->FetchRow();
		$slug = sb_build_slug($role->role_name);
		$this->dbh->Query("DELETE FROM role2permission WHERE role_id = {$role->role_id}");
		$this->dbh->Query("DELETE FROM permissions WHERE (permission = 'create_role_$slug' OR permission = '$slug')");
		$this->dbh->Query("DELETE FROM user_roles WHERE role_id = {$role->role_id} LIMIT 1");
    }
    /**
     * Check if a role name already exists
     * 
     * @param string $roleName
     * @return bool
     */
    public function RoleNameExists($roleName)
    {
        //##check for duplicated role name
        $query = "SELECT role_id, role_name FROM user_roles WHERE role_name = '$roleName' LIMIT 1";
        return $this->dbh->Query($query) > 0 ? true : false;
    }
    public function SetPermissions($roleId, $perms)
    {
        $cdate  = date('Y-m-d H:i:s');
        $roleId = (int)$roleId;
        $this->dbh->Query("DELETE FROM role2permission WHERE role_id = $roleId");
        if( count($perms) )
        {
            $query = "INSERT INTO role2permission(role_id,permission_id) VALUES";
            foreach($perms as $perm_id)
            {
                $query .= "($roleId, $perm_id),";
            }
            $this->dbh->Query(rtrim($query, ','));
        }
        return true;
    }
    /**
     * Get user role by key
     * @param string $key
     * @return Object|null The role
     */
    public function GetByKey($key)
    {
        return $this->dbh->FetchRow(sprintf("SELECT * FROM user_roles WHERE role_key = '%s' LIMIT 1", $key));
    }
}
