<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace SinticBolivia\SBFramework\Modules\Users\Models;
use SinticBolivia\SBFramework\Classes\SB_Model;
use SinticBolivia\SBFramework\Classes\SB_Route;
use SinticBolivia\SBFramework\Classes\SB_Module;
use SinticBolivia\SBFramework\Database\Classes\SB_DbTable;
use SinticBolivia\SBFramework\Modules\Users\Classes\SB_User;
use Exception;

/**
 * Description of UsersModel
 *
 * @author marcelo
 */
class UsersModel extends SB_Model
{
    /**
     * @namespace SinticBolivia\SBFramework\Modules\Users\Models
     * @var RolesModel
     */
    protected $rolesModel;
    
	public function GetAllUsers()
	{
		$table = SB_DbTable::GetTable('users', 1);
		
		return $table->GetRows(-1);
	}
	public function Get($id)
	{
		$user = new SB_User($id);
		if( !$user->user_id )
			throw new Exception($this->__('The user does not exists', 'users'));
			
		return $user;
		
	}
	/**
	 * Get a user by field
	 * 
	 * @return  
	 */
	public function GetBy($column, $keyword)
	{
		return sb_get_user_by($column, $keyword);
	}
    //put your code here
    public function Create($data, $send_email = true, $password_is_plain = true)
    {
        $currentUser = sb_get_current_user();
        $data = !is_object($data) ? (object)$data : $data;
        
        if( !isset($data->username) || empty($data->username) )
            throw new Exception(__('The username is empty', 'users'));
        /*
        if( !isset($data->email) || empty($data->email) )
            throw new Exception(__('The user email is empty', 'users'));
        */
      
        if( $this->GetBy('username', $data->username) )
            throw new Exception(__('The username already exists', 'users'));
        if( !isset($data->pwd) || empty($data->pwd) )
            throw new Exception(__('You need to enter a password', 'users'));
        if( $data->email && $user_by_email = $this->GetBy('email', $data->email) )
            throw new Exception(__('The email already exists', 'users'));
        
        //##set default user role to user
        if( !isset($data->role_id) || !$data->role_id )
            $data->role_id = $this->rolesModel->GetByKey('user')->role_id;
        if( isset($data->status) || !$data->status )
            $data->status = 'enabled';
        
        $data->creation_date            = date('Y-m-d H:i:s');
        $data->last_modification_date	= date('Y-m-d H:i:s');
        //##check for user meta data
        $meta = (array)isset($data->meta) ? $data->meta : array();
        unset($data->meta);
        
        $pass = '';
        if( !isset($data->pwd) || empty($data->pwd)  )
        {
            $pass       = $this->GenRandomPassword();
            $data->pwd  = md5($pass);
        }
        else
        {
            $pass = $data->pwd;
            if( $password_is_plain )
                $data->pwd = md5($data->pwd);
        }
       
        $data->last_modification_date	= date('Y-m-d H:i:s');
        $data->creation_date			= date('Y-m-d H:i:s');
        SB_Module::do_action_ref('before_insert_user', $data);
        SB_Module::do_action_ref('users_before_insert_user', $data);
        $this->dbh->BeginTransaction();
        //##insert the user to database
        $id 		= $this->dbh->Insert('users', $data);
        SB_Module::do_action('after_insert_user', $id, $data); //for old versions
        SB_Module::do_action('users_after_insert_user', $id, $data);
        //##insert user meta data
        foreach($meta as $metaKey => $metaValue)
        {
            sb_add_user_meta($id, $metaKey, $metaValue);
        }
        $this->dbh->EndTransaction();
        $user 		= new SB_User($id);
        $user_dir	= UPLOADS_DIR . SB_DS . sb_build_slug($user->username);
        if( !is_dir($user_dir) )
            mkdir($user_dir);
        if( $send_email && $user->email )
        {
            $url = parse_url(BASEURL);
            //##send user email
            $body       = $this->GetNewUserEmail($user, $pass);
            $subject 	= SB_Module::do_action('register_user_email_subject', sprintf(__('%s - User Registration', 'users'), SITE_TITLE));
            $headers    = array(
                    'Content-Type: text/html; charset=utf-8',
                    sprintf("From: %s <no-reply@%s>", SITE_TITLE, $url['host'])
            );
            $subject 	= SB_Module::do_action('users_new_email_subject', $subject);
            $body 		= SB_Module::do_action('users_new_email_body', 
                                                $body, 
                                                $data->username, 
                                                $data->email, 
                                                $pass, 
                                                $data);
            $headers	= SB_Module::do_action('users_new_email_headers', $headers);
            
            lt_mail($user->email, $subject, $body, $headers);
        }
       
        return $user;
    }
    /**
     * Update a user
     * @param $user SB_User|array|object
     */
    public function Update($user)
    {
        $user = is_a($user, 'SB_User') ? (object)$user->_dbData : (object)$user;
        if( !isset($user->user_id) || !$user->user_id )
            throw new Exception(__('Invalid user identifier', 'users'));
        
        $oldUser = $this->Get($user->user_id);
       
        if( !$oldUser || !$oldUser->user_id )
            throw new Exception(__('The user does not exists', 'users'));
        //##validate email
        if( $oldUser->email != $user->email && $this->GetBy('email', $user->email) )
            throw new Exception(__('The email already exists', 'users'));
        //##avoid to update the username
        unset($user->username);
        //##try to update the user password
        if( isset($user->pwd) && !empty($user->pwd) && $oldUser->pwd != md5($user->pwd) )
        {
            //##hash the new password
            $user->pwd = md5($user->pwd);
        }
        else
        {
			//##keep the old user password
			$user->pwd = $oldUser->pwd;
		}
        $meta = array();
        //##check for user meta
        if( isset($user->meta) )
        {
            $meta = $user->meta;
            unset($user->meta);
        }
        $user->last_modification_date = date('Y-m-d H:i:s');
        
        SB_Module::do_action_ref('users_before_update_user', $user);

        //##update the user
        $this->dbh->BeginTransaction();
        $this->dbh->Update('users', $user, array('user_id' => $user->user_id));
        foreach($meta as $metaKey => $metaValue)
        {
            sb_update_user_meta($user->user_id, $metaKey, $metaValue);
        }
        $this->dbh->EndTransaction();
        SB_Module::do_action_ref('users_after_update_user', $user->id, $user);
        //##return the updated user
        return $this->Get($user->user_id);
    }
    /**
     * Insert or update a user
     * 
     * @param   SB_User|object|array
     * @return  SB_User The user
     */
    public function Save($data)
    {
        $data = is_object($data) ? $data : (object)$data;
        SB_Module::do_action_ref('before_save_user', $data->user_id);
        SB_Module::do_action_ref('users_before_save_user', $data);
        $user = null;
        if( isset($data->user_id) && $data->user_id )
            $user = $this->Update ($data);
        else
            $user = $this->Create($data);
        
        SB_Module::do_action('save_user', $user->user_id);
        SB_Module::do_action('user_after_save_user', $user);
        return $user;
    }
    public function Delete($userId)
    {
        $user = new SB_User($userId);
		if( !$user->user_id )
			throw new \Exception($this->__('The user does not exists', 'users'));
        $this->dbh->BeginTransaction();
		$query = "DELETE FROM user_meta WHERE user_id = {$user->user_id}";
		$this->dbh->Query($query);
		$this->dbh->Query("DELETE FROM users WHERE user_id = {$user->user_id}");
        $this->dbh->EndTransaction();
        sb_delete_dir($user->GetDirectory());
        return true;
    }
    
    /**
     * Build a random password
     * 
     * @param int $length
     * @param string $type the charactes to use to build the password, set null to use whole characteres
     * 						letter|number|special
     * @return string
     */
    public function GenRandomPassword($length = 8, $type = null) 
    {
        $numbers = '1234567890';
        $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $special = '*[]{}-_$';
        $alphabet = '';
        if( $type == 'letter' )
        {
            $alphabet = $letters;
        }
        elseif( $type == 'number' )
        {
            $alphabet = $numbers;
        }
        elseif( $type == 'letter|number' )
        {
            $alphabet = $letters . $numbers;
        }
        elseif( $type == 'special' )
        {
            $alphabet = $special;
        }
        else
        {
            $alphabet = $numbers . $letters . $special;
        }
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) 
        {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    /**
     * Get the email body for new users
     * @param $user SB_User
     * @param $plainPassword The new user text plain password
     */
    public function GetNewUserEmail($user, $plainPassword = null)
    {
        $emailBody = sprintf(__("Hello %s<br/><br/>", 'users'), $user->username) .
                sprintf(__('Thanks for register into our website, you account details are below.<br/><br/>', 'users')).
                sprintf(__('Username: %s<br/>', 'users'), $user->username).
                sprintf(__('Password: %s<br/>', 'users'), $plainPassword ? $plainPassword : __('[Your password]', 'users')).
                '<br/>'.
                __('Follow the next link in order to start a session.<br/><br/>', 'users').
                sprintf(__('<a href="%s">Login</a><br/><br/>'), SB_Route::_('index.php?mod=users')).
                sprintf(__('Greetings<br/><br/>%s', 'users'), SITE_TITLE);
                
        return SB_Module::do_action('register_user_email_body', $emailBody, $user, $pass);;
    }
}
