<?php
use SinticBolivia\SBFramework\Classes\SB_Language;
use SinticBolivia\SBFramework\Classes\SB_Factory;
use SinticBolivia\SBFramework\Classes\SB_Module;

SB_Language::loadLanguage(LANGUAGE, 'users', dirname(__FILE__) . SB_DS . 'locale');
$dbh = SB_Factory::getDbh();
SB_Module::RunSQL('users');
$permissions = array(
		array('group' => 'users', 'permission' => 'manage_settings', 'label'	=> __('Gestionar configuracion', 'users')),
		array('group' => 'users', 'permission' => 'manage_general_settings', 'label'	=> __('Configuraci&oacute;n General', 'users')),
		//array('permission' => 'manage_design_settings', 'label'	=> SB_Text::_('Configuraci&oacute;n de Est&eacute;tica', 'users')),
		array(
				'group' => 'users',
				'permission' 	=> 'manage_limit_settings', 
				'label'			=> __('Configuraci&oacute;n de Aplicaci&oacute;n', 'users'),
				'attributes'	=> json_encode(array('only_root' => 'yes')),
		),
		array('group' => 'users','permission' => 'manage_roles', 'label'	=> __('Gestionar roles', 'users')),
		array('group' => 'users','permission' => 'create_role', 'label'	=> __('Crear rol', 'users')),
		array('group' => 'users','permission' => 'edit_role', 'label'		=> __('Editar rol', 'users')),
		array('group' => 'users','permission' => 'delete_role', 'label'	=> __('Borrar rol', 'users')),
		array('group' => 'users','permission' => 'manage_users', 'label'	=> __('Gestionar usuarios', 'users')),
		array('group' => 'users','permission' => 'create_user', 'label'	=> __('Crear usuario', 'users')),
		array('group' => 'users','permission' => 'edit_user', 'label'		=> __('Editar usuario', 'users')),
		array('group' => 'users','permission' => 'delete_user', 'label'	=> __('Borrar usuario', 'users')),
		array('group' => 'users', 'permission' => 'see_all_users', 'label'	=> __('See all users', 'users')),
		array('group' => 'users', 'permission' => 'users_see_sessions', 'label'	=> __('See user sessions', 'users')),
);
sb_add_permissions($permissions);
//##insert roles
$roles = array(
		array(
				'role_name'					=> __('Posible', 'users'),
				'role_key'					=> 'possible',
				'last_modification_date'	=> date('Y-m-d H:i:s'),
				'creation_date'				=> date('Y-m-d H:i:s')
		),
		array(
				'role_name'					=> __('Bloqueado', 'users'),
				'role_key'					=> 'bloqued',
				'last_modification_date'	=> date('Y-m-d H:i:s'),
				'creation_date'				=> date('Y-m-d H:i:s')
		),
);
foreach( $roles as $role )
{
	$query = "SELECT role_id FROM user_roles WHERE role_key = '{$role['role_key']}' LIMIT 1";
	if( !$dbh->Query($query) )
	{
		$dbh->Insert('user_roles', $role);
	}
}
